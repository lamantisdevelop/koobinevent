<?php
/* Funcions auxiliars per mòdul */
$path_top = $path_modul . '/function_top.php';
if (is_file($path_top)) {
    include($path_top);
}
?>

<!DOCTYPE html>
<html lang='<?php echo $language_code; ?>' xml:lang='<?php echo $language_code; ?>' xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title><?php echo (tep_not_null($title)) ? $title : _('META Title'); ?></title>

        <base href = "<?php echo HTTP_SERVER; ?>" target="_self" />

        <meta name="author" content="<?php echo AUTHOR; ?>"/>
        <meta name="keywords" content="<?php echo (tep_not_null($keywords)) ? $keywords : _('META Keywords'); ?>" />
        <meta name="description" content="<?php echo (tep_not_null($description)) ? $description : _('META Description'); ?>" />


        <meta charset="utf-8" />

        <meta name="robots" content="all" />
        <meta name="revisit-after" content="30 days" />

        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700,200|Arimo:400,700,400italic' rel='stylesheet' type='text/css'/>

        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="js/rs-plugin/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/rs-plugin.css" media="screen" />

        <link href="favicon.png" type="image/png" rel="shortcut icon" />
        <link href="favicon.png" type="image/png" rel="icon" />

        <?php
        if ($canonical) {
            ?>
            <link rel="canonical" href="<?php echo $canonical; ?>"/>
            <?php
        }
        ?>
        <meta name="viewport" content="initial-scale=1, minimum-scale=1"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" title="default" />
        <link rel="stylesheet" type="text/css" href="css/structure.css" media="screen" title="default" />
        <?php
        /* CSS auxiliars per mòdul */
        $path_css = $path_modul . '/styles.css';
        if (is_file($path_css))
            echo '<link rel="stylesheet" type="text/css" href="' . HTTP_SERVER . $path_css . '" media="screen" title="default" />' . "\n";
        ?>
        <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 4900px)" href="css/structure_mobile.css" />
        <!--[if gte IE 9]>
            <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <![endif]-->

        <!--[if gte IE 8]>
            <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <![endif]-->
        <!-- Piwik 
        <script type="text/javascript">
            var pkBaseURL = (("https:" == document.location.protocol) ? "https://dinastats.com/" : "http://dinastats.com/");
            document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
        </script><script type="text/javascript">
            try {
                var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 1171);
                piwikTracker.trackPageView();
                piwikTracker.enableLinkTracking();
            } catch( err ) {}
        </script><noscript><p><img src="http://dinastats.com/piwik.php?idsite=1171" style="border:0" alt="" /></p></noscript>
      End Piwik Tracking Code -->

    </head>

    <body >
        <div>
            <div  id="menu7">
                <?php
// Menú Pagines
                echo tep_print_menu_by_id(6, 'menu-top', 0, true); // Menú Catàleg
                ?>   
            </div>
        </div>
        <div class="container">
            <div id="pagina" >
                <div class="row-fluid">
                    <div id="header" class="span16">
                        <?php include(DIR_WS_MODULS . 'header.php'); ?>
                    </div>

                    <div id="sub_header" class="span16">
                        <?php include(DIR_WS_MODULS . 'sub_header.php'); ?>
                    </div>

                    <div id="contingut" class="span16">
                        <?php
                        $path_content = $path_modul . '/content.php';
                        if (is_file($path_content)) {
                            include($path_content);
                        } else {
                            echo('Error al carregar el mòdul <b>' . $_GET ['modul'] . '</b>.<br/>Contingut no definit.');
                        }
                        ?>
                        <div class="clear"></div>
                        <?php
                        //mostrar GALERIA si existeix 
                        if ($page['fotos_groups_id']) {//CAS té una galeria assignada
                            ?>
                            <div class="galeria">
                                <?php
                                $galeriaQuery = tep_get_fotos_galeria($page['fotos_groups_id']);
                                $galeria_numrows = tep_db_num_rows($galeriaQuery);
                                if ($galeria_numrows > 0) {//Mostrar galeria de fotos
                                    while ($galeria = tep_db_fetch_array($galeriaQuery)) {
                                        $fotoInfo = new objectInfo($galeria);
                                        ?>
                                        <a <?php echo($i == 0 ? 'id="firstInGallery"' : '') ?> class="foto <?php echo (($i % 4 == 0) ? 'fi_linia' : ''); ?>" href="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs4/' . $fotoInfo->image; ?>" rel="shadowbox[<?php echo $page['fotos_groups_id']; ?>]" title="<?php echo $fotoInfo->title . ($fotoInfo->autor ? ' | &copy; ' . $fotoInfo->autor : '' ); ?>">
                                            <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $fotoInfo->image; ?>" alt="<?php echo $fotoInfo->title; ?>"/>
                                        </a>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div id="pre_footer" class="row">
            </div>

            <div id="footer" class="row">
                <?php //include(DIR_WS_MODULS . 'footer.php'); ?>
            </div>
        </div>

        <!--<script type="text/javascript" src="js/jquery.js"></script>-->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.plugins.js"></script>
        <script type="text/javascript" src="js/_plugins/jquery.anythingslider.js"></script>
        <!--jQuery REVOLUTION Slider  -->
        <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>


        <?php
        //Javascrips inicials
        include('jquery.init.php');

        /* Javascript auxiliars per mòdul */
        $path_js = $path_modul . '/js_script.php';
        if (is_file($path_js))
            include($path_js);
        ?>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', '<?php echo GOOGLE_ANALYTICS_CODE; ?>']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>

    </body>
</html>
