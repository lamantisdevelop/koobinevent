<?php
/* CRIDEM AL FUNCTION_TOP DE CADA MODUL -> Funcions auxiliars per mòdul */
$path_top = $path_modul . '/function_top.php';
if (is_file($path_top)) {
    include($path_top);

    //AQUI POSAREM ELS AFEGITS ALS METAS, I NO PAS A CADA FUNCTION TOP
    //The Open Graph protocol -> http://ogp.me/
    //Twitter no fa servir og tags, fa servir els seus. -> https://dev.twitter.com/docs/cards/markup-reference
    //PRIIORITATS:
    //1 - Meta TITOL de la pagina
    //2 - Titol de la pagina
    //3 - Titol Generic del po mo
    //4 - Afegim el que vulguem al titol aqui sota, en aquest cas afegim el titol generic.

    (tep_not_null($title)) ? $title .= ' | ' . _('META Title') : $title = _('META Title');
    (tep_not_null($keywords)) ? $keywords .= ' , ' . _('META Keywords') : $keywords = _('META Keywords');
    (tep_not_null($description)) ? $description .= ' ' . _('META Description') : $description = _('META Description');

    //OG TAGS
    (tep_not_null($ogTitle)) ? $ogTitle .= ' | ' . _('META Title') : $ogTitle = _('META Title');
    (tep_not_null($ogDescription)) ? $ogDescription .= ' ' . _('META Description') : $ogDescription = _('META Description');
    //$ogImage -> li donem valor al function_top
    //TWITTER TAGS
    (tep_not_null($twitterTitle)) ? $twitterTitle .= ' | ' . _('META Title') : $twitterTitle = _('META Title');
    (tep_not_null($twitterDescription)) ? $twitterDescription .= ' ' . _('META Description') : $twitterDescription = _('META Description');
    //$twitterImage -> li donem valor al function_top
}
?>
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang='<?php echo $language_code; ?>' xml:lang='<?php echo $language_code; ?>' xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang='<?php echo $language_code; ?>' xml:lang='<?php echo $language_code; ?>' xmlns="http://www.w3.org/1999/xhtml"> <!--<![endif]-->
    <head>
        <title><?php echo $title; ?></title>
        
        <meta charset="utf-8" />
        <meta property="" content="" />
       
        <base href = "<?php echo HTTP_SERVER; ?>" target="_self" />

        <meta name="author" content="<?php echo WEB_OWNER; ?>"/>
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <meta name="description" content="<?php echo $description; ?>" />

        <?php //FACEBOOK  ?>
        <?php echo (isset($ogTitle) && tep_not_null($ogTitle)) ? '<meta property="og:title" content="' . $ogTitle . '" />' : ''; ?> 
        <?php echo (isset($ogDescription) && tep_not_null($ogDescription)) ? '<meta property="og:description" content="' . $ogDescription . '" />' : ''; ?> 
        <?php echo (isset($ogImage) && tep_not_null($ogImage)) ? '<meta property="og:image" content="' . $ogImage . '" />' : ''; ?> 

        <?php //TWITTER ?>
        <?php echo (isset($twitterTitle) && tep_not_null($twitterTitle)) ? '<meta property="twitter:title" content="' . $twitterTitle . '" />' : ''; ?> 
        <?php echo (isset($twitterDescription) && tep_not_null($twitterDescription)) ? '<meta property="twitter:description" content="' . $twitterDescription . '" />' : ''; ?> 
        <?php echo (isset($twitterImage) && tep_not_null($twitterImage)) ? '<meta property="twitter:image:src" content="' . $twitterImage . '" />' : ''; ?> 

        <!-- Set the viewport width to device width for mobile -->
        <meta name="viewport" content="width=device-width" />
        <meta name="robots" content="all" />
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

        <link href='//fonts.googleapis.com/css?family=Roboto:400,900,300,300italic,400italic,900italic' rel='stylesheet' type='text/css'>

        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="js/rs-plugin/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/rs-plugin.css" media="screen" />

        <link href="css/jquery.pixelentity.flare.min.css" rel="stylesheet"/>

        <link href="image/favicon.png" type="image/png" rel="shortcut icon" />
        <link href="image/favicon.ico" type="image/ico" rel="icon" />

        <?php
        if ($canonical) {
            ?>
            <link rel="canonical" href="<?php echo $canonical; ?>"/>
            <?php
        }
        ?>

        <link rel="stylesheet" type="text/css" href="css/scss/app.css?v=2" media="screen" title="default" />
        <link rel="stylesheet" type="text/css" href="css/foundation_icons/foundation-icons.css" media="screen" title="default" />
        <?php
        /* CSS auxiliars per mòdul */
        $path_css = $path_modul . '/styles.css';
        if (is_file($path_css)) {
            echo '<link rel="stylesheet" type="text/css" href="' . HTTP_SERVER . $path_css . '" media="screen" title="default" />' . "\n";
        }
        ?>

        <script type="text/javascript" src="js/modernizr.min.js"></script>

        <!--[if gte IE 9]>
            <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <![endif]-->

        <!--[if gte IE 8]>
            <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <![endif]-->

        <!-- Piwik -->
        <script type="text/javascript">
            var pkBaseURL = (("https:" == document.location.protocol) ? "https://dinastats.com/" : "http://dinastats.com/");
            document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try
            {
                var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", <?php echo PIWIK_TRACK_CODE; ?>);
                piwikTracker.trackPageView();
                piwikTracker.enableLinkTracking();
            }
            catch (err) {
            }
        </script>


<!-- <noscript><p><img src="http://dinastats.com/piwik.php?idsite=<?php // echo PIWIK_TRACK_CODE;      ?>" style="border:0" alt="" /></p></noscript> -->
        <!-- End Piwik Tracking Code -->

    </head>

    <body class="page_<?php echo $_GET['pageID']; ?>" >

        <?php
        if (AVIS_COOKIES_ACTIU && !isset($_COOKIE['cookies_acceptades'])) {
            ?>
            <div id="avis_cookies" >
                <div class="row">
                    <div class="contingut large-12 columns">
                        <?php echo _('Text avis cookies'); ?> <a href="<?php echo tep_friendly_url('', tep_get_page_title(PAGINA_COOKIES)); ?>"><?php echo _('Mes info'); ?></a>
                        <span class="tancar_cookie">x</span>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

        <div class="container" >
            <div id="pagina" >
                
                <div id="header" >
                    <div class="header_content" >
                        <?php include(DIR_WS_MODULS . 'header.php'); ?>
                    </div>
                </div>
                <div class="aguantaMenu">
                    <div class="row">
                        <div class="menu-principal large-12 columns">
                            <div class="panellMenu">
                                <?php include(DIR_WS_MODULS . 'menu_top_bar.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="fons_body"></div>
                
                <div id="sub_header" class="">
                    <?php include(DIR_WS_MODULS . 'sub_header.php'); ?>
                </div>
                
                <?php 
                 if ($_GET['pageID'] == DEFAULT_PAGE) //nomes es mostra a la pgina d' inici
                {
                    ?>
                    <div class="aguantaBanners">
                        <div class="row">
                            <div class="large-12 columns">
                                <ul class="large-block-grid-2 medium-block-grid-2 small-block-grid-1">
                                    <?php echo banners_list_group_foundation(2); //banners home ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php 
                }
                ?>
                
                <div class="body_content">
                    <div id="contingut" class="">
                        <?php
                        $path_content = $path_modul . '/content.php';
                        if (is_file($path_content)) {
                            include($path_content);
                        } else {
                            echo('Error al carregar el mòdul <b>' . $_GET ['modul'] . '</b>.<br/>Contingut no definit.');
                        }
                        ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            
            <div id="footer">
                
                <?php //BANNER PEU ?>
                <?php 
                if ($_GET['pageID'] == DEFAULT_PAGE && $language_code=='ca') //nomes es mostra a la pgina d' inici
                {
                    ?>
                    <div class="bannerPeu">
                        <div class="row">
                            <div class="columns large-12">
                                <ul class="large-block-grid-1 medium-block-grid-1 small-block-grid-1">
                                    <?php echo banners_list_group_peu_foundation(4); //banners home ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php 
                }
                ?>

                <?php //DIRECCIO PEU ?>
                <div class="infoPeu">
                    <div class="row">
                        <div class="columns large-12 medium-12 small-12">
                            <div class="aguantaInfoPeu">
                                <p>
                                    <img class="logoPeu" src="image/koobin_event_footer.png" alt="Koobin Event">
                                </p>
                                <?php 
                                    echo _('Text de contacte peu de pagina');
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php //ICONES SOCIALS ?>
                <div class="iconsPeu">
                    <div class="row">
                        <div class="columns large-12 medium-12 small-12">
                            <dl>
                                <dt></dt>
                                <dd><a href="<?php echo SOCIAL_FACEBOOK; ?>" target="_blank" title="Facebook" class="facebook"><i class="fi-social-facebook"></i></a></dd>
                                <dd><a href="<?php echo SOCIAL_TWITTER; ?>" target="_blank" title="Twitter" class="twitter"><i class="fi-social-twitter"></i></a></dd>
                                <dd><a href="<?php echo SOCIAL_INSTAGRAM; ?>" target="_blank" title="Instagram" class="instagram"><i class="fi-social-instagram"></i></a></dd>
                                <dd><a href="<?php echo SOCIAL_LINKEDIN; ?>" target="_blank" title="LinkedIN" class="youtube"><i class="fi-social-linkedin"></i></a></dd>
                                <dd><a href="<?php echo SOCIAL_GOOGLE_PLUS; ?>" target="_blank" title="Google+" class="youtube"><i class="fi-social-google-plus"></i></a></dd>
                                
                            </dl>
                        </div>
                    </div>
                </div>

                <?php //MENU PEU ?>                
                <div class="menuPeu">
                    <?php include(DIR_WS_MODULS . 'footer.php'); ?>
                </div>
            </div>
            
        </div>

        <!--<script type="text/javascript" src="js/jquery.js"></script>-->
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.plugins.js"></script>

        <!--jQuery REVOLUTION Slider  -->
        <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="js/foundation.min.js"></script>

        <?php
//Javascrips inicials
        include('jquery.init.php');

        /* Javascript auxiliars per mòdul */
        $path_js = $path_modul . '/js_script.php';
        if (is_file($path_js)) {
            include($path_js);
        }
        ?>


        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '<?php echo GOOGLE_ANALYTICS_CODE; ?>', '<?php echo GOOGLE_ANALYTICS_HOST; ?>');
            ga('send', 'pageview');
        </script>

    </body>
</html>
