<?php

require('includes/application_top_min.php');

// Definim la classe utiitzada per crear el calendari
class CreateQCalendarArray {

    var $daysInMonth;
    var $weeksInMonth;
    var $firstDay;
    var $week;
    var $month;
    var $year;

    function CreateQCalendarArray($month, $year) {
        $this->month = $month;
        $this->year = $year;
        $this->week = array();
        $this->daysInMonth = date("t", mktime(0, 0, 0, $month, 1, $year));
        // get first day of the month
        $this->firstDay = date("w", mktime(0, 0, 0, $month, 1, $year));
        $this->lastDay = date("w", mktime(0, 0, 0, $month, $this->daysInMonth, $year));
        $tempDays = $this->firstDay + $this->daysInMonth;
        $this->weeksInMonth = ceil($tempDays / 7);
        // Per començar dilluns en comptes de diumenge
        if ($this->firstDay == 0) {
            $this->firstDay = 7;
            $this->weeksInMonth = $this->weeksInMonth + 1;
        }
        if ($this->lastDay == 0) {
            $this->weeksInMonth = $this->weeksInMonth - 1;
        }
        $this->fillArray();
    }

    function fillArray() {
        // create a 2-d array
        for ($j = 0; $j < $this->weeksInMonth; $j++) {
            for ($i = 0; $i < 7; $i++) {
                $counter++;
                $this->week[$j][$i] = $counter;
                // offset the days
                $this->week[$j][$i] -= ($this->firstDay - 1); // Per començar dilluns en comptes de diumenge
                if (($this->week[$j][$i] < 1) || ($this->week[$j][$i] > $this->daysInMonth)) {
                    $this->week[$j][$i] = "";
                }
            }
        }
    }

}

class QCalendar {

    var $html;
    var $weeksInMonth;
    var $week;
    var $month;
    var $year;
    var $today;
    var $links;

    function QCalendar($cArray, $today, &$links) {
        $this->month = $cArray->month;
        $this->year = $cArray->year;
        $this->weeksInMonth = $cArray->weeksInMonth;
        $this->week = $cArray->week;
        $this->today = $today;
        $this->links = $links;
        $this->createCalendar();
    }

    function createCalendar() {
        $cal_day = array('Dg', 'Dl', 'Dm', 'Dx', 'Dj', 'Dv', 'Ds');
        $day_text = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
        $month_text = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');
        $header = $month_text[$this->month - 1] . ' ' . $this->year;
        $nextMonth = $this->month + 1;
        $prevMonth = $this->month - 1;

        switch ($this->month) {
            case 1:
                $lYear = $this->year;
                $pYear = $this->year - 1;
                $nextMonth = 2;
                $prevMonth = 12;
                break;

            case 12:
                $lYear = $this->year + 1;
                $pYear = $this->year;
                $nextMonth = 1;
                $prevMonth = 11;
                break;

            default:
                $lYear = $this->year;
                $pYear = $this->year;
                break;
        }

        $this->html .= '<div class="header_botons">'
                //. '<a href="javascript:void(0);" rel=”nofollow” rev="' . HTTP_SERVER . 'calendar.php?m=' . $this->month . '&amp;y=' . ($this->year-1) . '" class="calendar_button prev_year"><&nbsp;</a>'
                . '<a href="javascript:void(0);" rel=”nofollow” rev="' . HTTP_SERVER . 'calendar.php?m=' . $prevMonth . '&amp;y=' . $pYear . '" class="calendar_button prev_month"><<&nbsp;</a>'
                . '<a href="javascript:void(0);" rel=”nofollow” rev="' . HTTP_SERVER . 'calendar.php" class="calendar_button">' . htmlentities($header, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</a>'
                . '<a href="javascript:void(0);" rel=”nofollow” rev="' . HTTP_SERVER . 'calendar.php?m=' . $nextMonth . '&amp;y=' . $lYear . '" class="calendar_button next_month">&nbsp;>></a>'
                //. '<a href="javascript:void(0);" rel=”nofollow” rev="' . HTTP_SERVER . 'calendar.php?m=' . $this->month . '&amp;y=' . ($this->year+1) . '"  class="calendar_button next_year">&nbsp;></a>'
                . '</div>';

        // Creem el cos
        $this->html.= '<div class="header_dia">'
                . '<span class="primer">' . $cal_day[1] . '</span>'
                . '<span>' . $cal_day[2] . '</span>'
                . '<span>' . $cal_day[3] . '</span>'
                . '<span>' . $cal_day[4] . '</span>'
                . '<span>' . $cal_day[5] . '</span>'
                . '<span>' . $cal_day[6] . '</span>'
                . '<span>' . $cal_day[0] . '</span>'
                . '</div>';

        $this->html.= '<div class="calendar_dies">';
        for ($j = 0; $j < $this->weeksInMonth; $j++) {
            for ($i = 0; $i < 7; $i++) {
                $cellValue = $this->week[$j][$i];
                // if today
                if (($this->today['day'] == $cellValue) && ($this->today['month'] == $this->month) && ($this->today['year'] == $this->year)) {
                    $cell = '<span class="today ' . (($i == 0) ? 'primer' : '') . '">' . $cellValue . '</span>';
                } else {
                    // else normal day
                    if (tep_not_null($cellValue)) {
                        $cell = '<span class="day ' . (($i == 0) ? 'primer' : '') . '">' . $cellValue . '</span>';
                    } else {
                        $cell = '<span class="' . (($i == 0) ? 'primer' : '') . '">' . $cellValue . '</span>';
                    }
                }

                // if days with link
                $link_title = '';
                $link = 0;
                foreach ($this->links as $val) {
                    if (($val['day'] == $cellValue) && (($val['month'] == $this->month) || ($val['month'] == '*')) && (($val['year'] == $this->year) || ($val['year'] == '*'))) {
                        $link++;
                        if ($link > 1)
                            $link_title .= '<hr/>';
                        $link_title .= $val['desc'] . '<br/>';
                    }
                }
                if (tep_not_null($link_title)) {
                    $cell = '<span class="link' . (($i == 0) ? ' primer' : '') . ((($this->today['day'] == $cellValue) && ($this->today['month'] == $this->month) && ($this->today['year'] == $this->year)) ? ' link_today' : '') . '">'                                               // . '&dia=' . $cellValue
                            . '<a href="' . tep_friendly_url('', tep_get_page_title(DEFAULT_AGENDA_PAGE), 'data=' . $this->year . '-' . $this->month) . '" class="prova" title="' . $link_title . '">' . $cellValue . '</a>'
                            . '</span>';
                }
                $this->html .= $cell;
            }
        }
        $this->html .= "</div>";
    }

    function render() {
        echo $this->html;
    }

}

// Busquem la data d'avui
$y = isset($_REQUEST['any']) ? $_REQUEST['any'] : date('Y');
$m = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : date('n');
$d = date('j');

$today = array('day' => $d, 'month' => date('n'), 'year' => date('Y'));

// Si li passem variables, canviem la data
if (isset($_GET['m'])) {
    $y = $_GET['y'];
    $m = $_GET['m'];
}

// Busquem dates segons mes i any per data inici i/o data final
$links = array();
$ini_mes = (int) $y . '-' . (int) $m . '-1';
$fi_mes = (int) $y . '-' . (int) ($m + 1) . '-1';
// Búsqueda
$calendar_query = tep_db_query("select a.id, a.lloc, ad.nom, a.data_inici, a.data_final from " . TABLE_ACTIVITATS . " a left join " . TABLE_ACTIVITATS_DESCRIPTION . " ad on a.id = ad.activitats_id and ad.language_id = '" . $language_id . "' where a.active = '1' and ("
        . "( TO_DAYS(a.data_inici) >= TO_DAYS('" . $ini_mes . "') and TO_DAYS(a.data_inici) <= TO_DAYS('" . $fi_mes . "'))"  // Data d'inici en el mes
        . " or ( a.data_final IS NOT NULL and TO_DAYS(a.data_final) >= TO_DAYS('" . $ini_mes . "') and TO_DAYS(a.data_final) <= TO_DAYS('" . $fi_mes . "'))" // Data final en el mes
        . " or ( a.data_final IS NOT NULL and TO_DAYS(a.data_inici) <= TO_DAYS('" . $ini_mes . "') and TO_DAYS(a.data_final) >= TO_DAYS('" . $fi_mes . "'))"
        . ")"); // Si el mes està en l'interval
// Recorrem dates
while ($calendar_link = tep_db_fetch_array($calendar_query)) {
    $itemInfo = new objectInfo($calendar_link);
    // Si és un interval recorrem tots els dies
    if (tep_not_null($itemInfo->data_final) && (comparar_dates($itemInfo->data_inici, $itemInfo->data_final) < 0)) {
        // Recorrem dies interval
        $data_inici_array = explode("-", $itemInfo->data_inici);
        $data_final_array = explode("-", $itemInfo->data_final);
        // Convertim dates i busquem diferència
        $data_inici_marc = mktime(0, 0, 0, $data_inici_array[1], $data_inici_array[2], $data_inici_array[0]);
        $data_final_marc = mktime(0, 0, 0, $data_final_array[1], $data_final_array[2], $data_final_array[0]);
        $oneday = (60 * 60 * 24);
        $cantidad_dias = ($data_final_marc - $data_inici_marc) / $oneday;
        // Recorrem dies interval
        for ($i = 0; $i < $cantidad_dias + 1; $i++) {
            $dias_de_adelanto = $oneday * $i;
            $fecha = $data_inici_marc + $dias_de_adelanto;
            // Info calendar
            $links[] = array('day' => date("d", $fecha), 'month' => date("m", $fecha), 'year' => date("Y", $fecha), 'link' => '', 'desc' => htmlentities($itemInfo->nom . ' ' . (tep_not_null($calendar_link['lloc']) ? '<br/>(' . $calendar_link['lloc'] . ')' : '')));
        }
    } else {
        $data_inici_array = explode("-", $itemInfo->data_inici);
        // Info calendar
        $links[] = array('day' => $data_inici_array[2], 'month' => $data_inici_array[1], 'year' => $data_inici_array[0], 'link' => '', 'desc' => htmlentities($itemInfo->nom . ' ' . (tep_not_null($itemInfo->lloc) ? '<br/>(' . $itemInfo->lloc . ')' : '')));
    }
}

// Creem el calendari
$cArray = new CreateQCalendarArray($m, $y);
$cal = new QCalendar($cArray, $today, $links);
$cal->render();
?>
