/*!
 * jQuery Transit - CSS3 transitions and transformations
 * Copyright(c) 2011 Rico Sta. Cruz <rico@ricostacruz.com>
 * MIT Licensed.
 *
 * http://ricostacruz.com/jquery.transit
 * http://github.com/rstacruz/jquery.transit
 */
(function($) {
	"use strict";
	$.transit = {
		version: "0.1.3",
		propertyMap: {
			marginLeft: 'margin',
			marginRight: 'margin',
			marginBottom: 'margin',
			marginTop: 'margin',
			paddingLeft: 'padding',
			paddingRight: 'padding',
			paddingBottom: 'padding',
			paddingTop: 'padding'
		},
		enabled: true,
		useTransitionEnd: false
	};
	var div = document.createElement('div');
	var support = {};
	function getVendorPropertyName(prop) {
		var prefixes = ['Moz', 'Webkit', 'O', 'ms'];
		var prop_ = prop.charAt(0).toUpperCase() + prop.substr(1);
		if (prop in div.style) {
			return prop;
		}
		for (var i = 0; i < prefixes.length; ++i) {
			var vendorProp = prefixes[i] + prop_;
			if (vendorProp in div.style) {
				return vendorProp;
			}
		}
	}

	function checkTransform3dSupport() {
		div.style[support.transform] = '';
		div.style[support.transform] = 'rotateY(90deg)';
		return div.style[support.transform] !== '';
	}
	var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	support.transition = getVendorPropertyName('transition');
	support.transitionDelay = getVendorPropertyName('transitionDelay');
	support.transform = getVendorPropertyName('transform');
	support.transformOrigin = getVendorPropertyName('transformOrigin');
	support.transform3d = checkTransform3dSupport();
	$.extend($.support, support);
	var eventNames = {
		'MozTransition': 'transitionend',
		'OTransition': 'oTransitionEnd',
		'WebkitTransition': 'webkitTransitionEnd',
		'msTransition': 'MSTransitionEnd'
	};
	var transitionEnd = support.transitionEnd = eventNames[support.transition] || null;
	div = null;
	$.cssEase = {
		'_default': 'ease',
		'in': 'ease-in',
		'out': 'ease-out',
		'in-out': 'ease-in-out',
		'snap': 'cubic-bezier(0,1,.5,1)'
	};
	$.cssHooks.transform = {
		get: function(elem) {
			return $(elem).data('transform');
		},
		set: function(elem, v) {
			var value = v;
			if (!(value instanceof Transform)) {
				value = new Transform(value);
			}
			if (support.transform === 'WebkitTransform' && !isChrome) {
				elem.style[support.transform] = value.toString(true);
			} else {
				elem.style[support.transform] = value.toString();
			}
			$(elem).data('transform', value);
		}
	};
	$.cssHooks.transformOrigin = {
		get: function(elem) {
			return elem.style[support.transformOrigin];
		},
		set: function(elem, value) {
			elem.style[support.transformOrigin] = value;
		}
	};
	$.cssHooks.transition = {
		get: function(elem) {
			return elem.style[support.transition];
		},
		set: function(elem, value) {
			elem.style[support.transition] = value;
		}
	};
	registerCssHook('scale');
	registerCssHook('translate');
	registerCssHook('rotate');
	registerCssHook('rotateX');
	registerCssHook('rotateY');
	registerCssHook('rotate3d');
	registerCssHook('perspective');
	registerCssHook('skewX');
	registerCssHook('skewY');
	registerCssHook('x', true);
	registerCssHook('y', true);
	function Transform(str) {
		if (typeof str === 'string') {
			this.parse(str);
		}
		return this;
	}
	Transform.prototype = {
		setFromString: function(prop, val) {
			var args = (typeof val === 'string') ? val.split(',') : (val.constructor === Array) ? val : [val];
			args.unshift(prop);
			Transform.prototype.set.apply(this, args);
		},
		set: function(prop) {
			var args = Array.prototype.slice.apply(arguments, [1]);
			if (this.setter[prop]) {
				this.setter[prop].apply(this, args);
			} else {
				this[prop] = args.join(',');
			}
		},
		get: function(prop) {
			if (this.getter[prop]) {
				return this.getter[prop].apply(this);
			} else {
				return this[prop] || 0;
			}
		},
		setter: {
			rotate: function(theta) {
				this.rotate = unit(theta, 'deg');
			},
			rotateX: function(theta) {
				this.rotateX = unit(theta, 'deg');
			},
			rotateY: function(theta) {
				this.rotateY = unit(theta, 'deg');
			},
			scale: function(x, y) {
				if (y === undefined) {
					y = x;
				}
				this.scale = x + "," + y;
			},
			skewX: function(x) {
				this.skewX = unit(x, 'deg');
			},
			skewY: function(y) {
				this.skewY = unit(y, 'deg');
			},
			perspective: function(dist) {
				this.perspective = unit(dist, 'px');
			},
			x: function(x) {
				this.set('translate', x, null);
			},
			y: function(y) {
				this.set('translate', null, y);
			},
			translate: function(x, y) {
				if (this._translateX === undefined) {
					this._translateX = 0;
				}
				if (this._translateY === undefined) {
					this._translateY = 0;
				}
				if (x !== null) {
					this._translateX = unit(x, 'px');
				}
				if (y !== null) {
					this._translateY = unit(y, 'px');
				}
				this.translate = this._translateX + "," + this._translateY;
			}
		},
		getter: {
			x: function() {
				return this._translateX || 0;
			},
			y: function() {
				return this._translateY || 0;
			},
			scale: function() {
				var s = (this.scale || "1,1").split(',');
				if (s[0]) {
					s[0] = parseFloat(s[0]);
				}
				if (s[1]) {
					s[1] = parseFloat(s[1]);
				}
				return (s[0] === s[1]) ? s[0] : s;
			},
			rotate3d: function() {
				var s = (this.rotate3d || "0,0,0,0deg").split(',');
				for (var i = 0; i <= 3; ++i) {
					if (s[i]) {
						s[i] = parseFloat(s[i]);
					}
				}
				if (s[3]) {
					s[3] = unit(s[3], 'deg');
				}
				return s;
			}
		},
		parse: function(str) {
			var self = this;
			str.replace(/([a-zA-Z0-9]+)\((.*?)\)/g, function(x, prop, val) {
				self.setFromString(prop, val);
			});
		},
		toString: function(use3d) {
			var re = [];
			for (var i in this) {
				if (this.hasOwnProperty(i)) {
					if ((!support.transform3d) && ((i === 'rotateX') || (i === 'rotateY') || (i === 'perspective') || (i === 'transformOrigin'))) {
						continue;
					}
					if (i[0] !== '_') {
						if (use3d && (i === 'scale')) {
							re.push(i + "3d(" + this[i] + ",1)");
						} else if (use3d && (i === 'translate')) {
							re.push(i + "3d(" + this[i] + ",0)");
						} else {
							re.push(i + "(" + this[i] + ")");
						}
					}
				}
			}
			return re.join(" ");
		}
	};
	function callOrQueue(self, queue, fn) {
		if (queue === true) {
			self.queue(fn);
		} else if (queue) {
			self.queue(queue, fn);
		} else {
			fn();
		}
	}

	function getProperties(props) {
		var re = [];
		$.each(props, function(key) {
			key = $.camelCase(key);
			key = $.transit.propertyMap[key] || key;
			key = uncamel(key);
			if ($.inArray(key, re) === -1) {
				re.push(key);
			}
		});
		return re;
	}

	function getTransition(properties, duration, easing, delay) {
		var props = getProperties(properties);
		if ($.cssEase[easing]) {
			easing = $.cssEase[easing];
		}
		var attribs = '' + toMS(duration) + ' ' + easing;
		if (parseInt(delay, 10) > 0) {
			attribs += ' ' + toMS(delay);
		}
		var transitions = [];
		$.each(props, function(i, name) {
			transitions.push(name + ' ' + attribs);
		});
		return transitions.join(', ');
	}
	$.fn.transition = $.fn.transit = function(properties, duration, easing, callback) {
		var self = this;
		var delay = 0;
		var queue = true;
		if (typeof duration === 'function') {
			callback = duration;
			duration = undefined;
		}
		if (typeof easing === 'function') {
			callback = easing;
			easing = undefined;
		}
		if (typeof properties.easing !== 'undefined') {
			easing = properties.easing;
			delete properties.easing;
		}
		if (typeof properties.duration !== 'undefined') {
			duration = properties.duration;
			delete properties.duration;
		}
		if (typeof properties.complete !== 'undefined') {
			callback = properties.complete;
			delete properties.complete;
		}
		if (typeof properties.queue !== 'undefined') {
			queue = properties.queue;
			delete properties.queue;
		}
		if (typeof properties.delay !== 'undefined') {
			delay = properties.delay;
			delete properties.delay;
		}
		if (typeof duration === 'undefined') {
			duration = $.fx.speeds._default;
		}
		if (typeof easing === 'undefined') {
			easing = $.cssEase._default;
		}
		duration = toMS(duration);
		var transitionValue = getTransition(properties, duration, easing, delay);
		var work = $.transit.enabled && support.transition;
		var i = work ? (parseInt(duration, 10) + parseInt(delay, 10)) : 0;
		if (i === 0) {
			var fn = function(next) {
					self.css(properties);
					if (callback) {
						callback.apply(self);
					}
					if (next) {
						next();
					}
				};
			callOrQueue(self, queue, fn);
			return self;
		}
		var oldTransitions = {};
		var run = function(nextCall) {
				var bound = false;
				var cb = function() {
						if (bound) {
							self.unbind(transitionEnd, cb);
						}
						if (i > 0) {
							self.each(function() {
								this.style[support.transition] = (oldTransitions[this] || null);
							});
						}
						if (typeof callback === 'function') {
							callback.apply(self);
						}
						if (typeof nextCall === 'function') {
							nextCall();
						}
					};
				if ((i > 0) && (transitionEnd) && ($.transit.useTransitionEnd)) {
					bound = true;
					self.bind(transitionEnd, cb);
				} else {
					window.setTimeout(cb, i);
				}
				self.each(function() {
					if (i > 0) {
						this.style[support.transition] = transitionValue;
					}
					$(this).css(properties);
				});
			};
		var deferredRun = function(next) {
				var i = 0;
				if ((support.transition === 'MozTransition') && (i < 25)) {
					i = 25;
				}
				window.setTimeout(function() {
					run(next);
				}, i);
			};
		callOrQueue(self, queue, deferredRun);
		return this;
	};
	function registerCssHook(prop, isPixels) {
		if (!isPixels) {
			$.cssNumber[prop] = true;
		}
		$.transit.propertyMap[prop] = support.transform;
		/*var version = $.fn.jquery.split('.'),
			versionMinor = parseFloat(version[1]),
			versionIncrement = parseFloat(version[2] || '0');
			//alert("1."+versionMinor+"."+versionIncrement);
		if (versionMinor < 8) {*/

			$.cssHooks[prop] = {
				get: function(elem) {
					var et = $(elem).css('transform');
					var t = (et && et != 'none') ? et : new Transform();
					try {
						return t.get(prop);
					} catch (e) {}
				},
				set: function(elem, value) {
					var et = $(elem).css('transform');
					var t = (et && et != 'none') ? et : new Transform();
					try {
						t.setFromString(prop, value);
						$(elem).css({
							transform: t
						});
					} catch (e) {}
				}
			};
		/*} else {
			$.cssHooks[prop] = {
				get: function(elem) {
					var t = $(elem).css('transform');
					if (t === 'none') {
						t = new Transform();
					}
					try {
						return t.get(prop);
					} catch (e) {}
				},
				set: function(elem, value) {
					var t = $(elem).css('transform');
					if (t === 'none') {
						t = new Transform();
					}
					try {
						t.setFromString(prop, value);
						$(elem).css({
							transform: t
						});
					} catch (e) {}
				}
			};
		}*/
	}

	function uncamel(str) {
		return str.replace(/([A-Z])/g, function(letter) {
			return '-' + letter.toLowerCase();
		});
	}

	function unit(i, units) {
		if ((typeof i === "string") && (!i.match(/^[\-0-9\.]+$/))) {
			return i;
		} else {
			return "" + i + units;
		}
	}

	function toMS(duration) {
		var i = duration;
		if ($.fx.speeds[i]) {
			i = $.fx.speeds[i];
		}
		return unit(i, 'ms');
	}
	$.transit.getTransitionValue = getTransition;
})(jQuery);
(function(a, b) {
	jQuery.easing["jswing"] = jQuery.easing["swing"];
	jQuery.extend(jQuery.easing, {
		def: "easeOutQuad",
		swing: function(a, b, c, d, e) {
			return jQuery.easing[jQuery.easing.def](a, b, c, d, e)
		},
		easeInQuad: function(a, b, c, d, e) {
			return d * (b /= e) * b + c
		},
		easeOutQuad: function(a, b, c, d, e) {
			return -d * (b /= e) * (b - 2) + c
		},
		easeInOutQuad: function(a, b, c, d, e) {
			if ((b /= e / 2) < 1) return d / 2 * b * b + c;
			return -d / 2 * (--b * (b - 2) - 1) + c
		},
		easeInCubic: function(a, b, c, d, e) {
			return d * (b /= e) * b * b + c
		},
		easeOutCubic: function(a, b, c, d, e) {
			return d * ((b = b / e - 1) * b * b + 1) + c
		},
		easeInOutCubic: function(a, b, c, d, e) {
			if ((b /= e / 2) < 1) return d / 2 * b * b * b + c;
			return d / 2 * ((b -= 2) * b * b + 2) + c
		},
		easeInQuart: function(a, b, c, d, e) {
			return d * (b /= e) * b * b * b + c
		},
		easeOutQuart: function(a, b, c, d, e) {
			return -d * ((b = b / e - 1) * b * b * b - 1) + c
		},
		easeInOutQuart: function(a, b, c, d, e) {
			if ((b /= e / 2) < 1) return d / 2 * b * b * b * b + c;
			return -d / 2 * ((b -= 2) * b * b * b - 2) + c
		},
		easeInQuint: function(a, b, c, d, e) {
			return d * (b /= e) * b * b * b * b + c
		},
		easeOutQuint: function(a, b, c, d, e) {
			return d * ((b = b / e - 1) * b * b * b * b + 1) + c
		},
		easeInOutQuint: function(a, b, c, d, e) {
			if ((b /= e / 2) < 1) return d / 2 * b * b * b * b * b + c;
			return d / 2 * ((b -= 2) * b * b * b * b + 2) + c
		},
		easeInSine: function(a, b, c, d, e) {
			return -d * Math.cos(b / e * (Math.PI / 2)) + d + c
		},
		easeOutSine: function(a, b, c, d, e) {
			return d * Math.sin(b / e * (Math.PI / 2)) + c
		},
		easeInOutSine: function(a, b, c, d, e) {
			return -d / 2 * (Math.cos(Math.PI * b / e) - 1) + c
		},
		easeInExpo: function(a, b, c, d, e) {
			return b == 0 ? c : d * Math.pow(2, 10 * (b / e - 1)) + c
		},
		easeOutExpo: function(a, b, c, d, e) {
			return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c
		},
		easeInOutExpo: function(a, b, c, d, e) {
			if (b == 0) return c;
			if (b == e) return c + d;
			if ((b /= e / 2) < 1) return d / 2 * Math.pow(2, 10 * (b - 1)) + c;
			return d / 2 * (-Math.pow(2, -10 * --b) + 2) + c
		},
		easeInCirc: function(a, b, c, d, e) {
			return -d * (Math.sqrt(1 - (b /= e) * b) - 1) + c
		},
		easeOutCirc: function(a, b, c, d, e) {
			return d * Math.sqrt(1 - (b = b / e - 1) * b) + c
		},
		easeInOutCirc: function(a, b, c, d, e) {
			if ((b /= e / 2) < 1) return -d / 2 * (Math.sqrt(1 - b * b) - 1) + c;
			return d / 2 * (Math.sqrt(1 - (b -= 2) * b) + 1) + c
		},
		easeInElastic: function(a, b, c, d, e) {
			var f = 1.70158;
			var g = 0;
			var h = d;
			if (b == 0) return c;
			if ((b /= e) == 1) return c + d;
			if (!g) g = e * .3;
			if (h < Math.abs(d)) {
				h = d;
				var f = g / 4
			} else var f = g / (2 * Math.PI) * Math.asin(d / h);
			return -(h * Math.pow(2, 10 * (b -= 1)) * Math.sin((b * e - f) * 2 * Math.PI / g)) + c
		},
		easeOutElastic: function(a, b, c, d, e) {
			var f = 1.70158;
			var g = 0;
			var h = d;
			if (b == 0) return c;
			if ((b /= e) == 1) return c + d;
			if (!g) g = e * .3;
			if (h < Math.abs(d)) {
				h = d;
				var f = g / 4
			} else var f = g / (2 * Math.PI) * Math.asin(d / h);
			return h * Math.pow(2, -10 * b) * Math.sin((b * e - f) * 2 * Math.PI / g) + d + c
		},
		easeInOutElastic: function(a, b, c, d, e) {
			var f = 1.70158;
			var g = 0;
			var h = d;
			if (b == 0) return c;
			if ((b /= e / 2) == 2) return c + d;
			if (!g) g = e * .3 * 1.5;
			if (h < Math.abs(d)) {
				h = d;
				var f = g / 4
			} else var f = g / (2 * Math.PI) * Math.asin(d / h);
			if (b < 1) return -.5 * h * Math.pow(2, 10 * (b -= 1)) * Math.sin((b * e - f) * 2 * Math.PI / g) + c;
			return h * Math.pow(2, -10 * (b -= 1)) * Math.sin((b * e - f) * 2 * Math.PI / g) * .5 + d + c
		},
		easeInBack: function(a, b, c, d, e, f) {
			if (f == undefined) f = 1.70158;
			return d * (b /= e) * b * ((f + 1) * b - f) + c
		},
		easeOutBack: function(a, b, c, d, e, f) {
			if (f == undefined) f = 1.70158;
			return d * ((b = b / e - 1) * b * ((f + 1) * b + f) + 1) + c
		},
		easeInOutBack: function(a, b, c, d, e, f) {
			if (f == undefined) f = 1.70158;
			if ((b /= e / 2) < 1) return d / 2 * b * b * (((f *= 1.525) + 1) * b - f) + c;
			return d / 2 * ((b -= 2) * b * (((f *= 1.525) + 1) * b + f) + 2) + c
		},
		easeInBounce: function(a, b, c, d, e) {
			return d - jQuery.easing.easeOutBounce(a, e - b, 0, d, e) + c
		},
		easeOutBounce: function(a, b, c, d, e) {
			if ((b /= e) < 1 / 2.75) {
				return d * 7.5625 * b * b + c
			} else if (b < 2 / 2.75) {
				return d * (7.5625 * (b -= 1.5 / 2.75) * b + .75) + c
			} else if (b < 2.5 / 2.75) {
				return d * (7.5625 * (b -= 2.25 / 2.75) * b + .9375) + c
			} else {
				return d * (7.5625 * (b -= 2.625 / 2.75) * b + .984375) + c
			}
		},
		easeInOutBounce: function(a, b, c, d, e) {
			if (b < e / 2) return jQuery.easing.easeInBounce(a, b * 2, 0, d, e) * .5 + c;
			return jQuery.easing.easeOutBounce(a, b * 2 - e, 0, d, e) * .5 + d * .5 + c
		}
	})
	a.waitForImages = {
		hasImageProperties: ["backgroundImage", "listStyleImage", "borderImage", "borderCornerImage"]
	};
	a.expr[":"].uncached = function(b) {
		var c = document.createElement("img");
		c.src = b.src;
		return a(b).is('img[src!=""]') && !c.complete
	};
	a.fn.waitForImages = function(b, c, d) {
		if (a.isPlainObject(arguments[0])) {
			c = b.each;
			d = b.waitForAll;
			b = b.finished
		}
		b = b || a.noop;
		c = c || a.noop;
		d = !! d;
		if (!a.isFunction(b) || !a.isFunction(c)) {
			throw new TypeError("An invalid callback was supplied.")
		}
		return this.each(function() {
			var e = a(this),
				f = [];
			if (d) {
				var g = a.waitForImages.hasImageProperties || [],
					h = /url\((['"]?)(.*?)\1\)/g;
				e.find("*").each(function() {
					var b = a(this);
					if (b.is("img:uncached")) {
						f.push({
							src: b.attr("src"),
							element: b[0]
						})
					}
					a.each(g, function(a, c) {
						var d = b.css(c);
						if (!d) {
							return true
						}
						var e;
						while (e = h.exec(d)) {
							f.push({
								src: e[2],
								element: b[0]
							})
						}
					})
				})
			} else {
				e.find("img:uncached").each(function() {
					f.push({
						src: this.src,
						element: this
					})
				})
			}
			var i = f.length,
				j = 0;
			if (i == 0) {
				b.call(e[0])
			}
			a.each(f, function(d, f) {
				var g = new Image;
				a(g).bind("load error", function(a) {
					j++;
					c.call(f.element, j, i, a.type == "load");
					if (j == i) {
						b.call(e[0]);
						return false
					}
				});
				g.src = f.src
			})
		})
	}
	a.fn.swipe = function(b) {
		if (!this) return false;
		var c = {
			fingers: 1,
			threshold: 75,
			swipe: null,
			swipeLeft: null,
			swipeRight: null,
			swipeUp: null,
			swipeDown: null,
			swipeStatus: null,
			click: null,
			triggerOnTouchEnd: true,
			allowPageScroll: "auto"
		};
		var d = "left";
		var e = "right";
		var f = "up";
		var g = "down";
		var h = "none";
		var i = "horizontal";
		var j = "vertical";
		var k = "auto";
		var l = "start";
		var m = "move";
		var n = "end";
		var o = "cancel";
		var p = "ontouchstart" in window,
			q = p ? "touchstart" : "mousedown",
			r = p ? "touchmove" : "mousemove",
			s = p ? "touchend" : "mouseup",
			t = "touchcancel";
		var u = "start";
		if (b.allowPageScroll == undefined && (b.swipe != undefined || b.swipeStatus != undefined)) b.allowPageScroll = h;
		if (b) a.extend(c, b);
		return this.each(function() {
			function J() {
				var a = I();
				if (a <= 45 && a >= 0) return d;
				else if (a <= 360 && a >= 315) return d;
				else if (a >= 135 && a <= 225) return e;
				else if (a > 45 && a < 135) return g;
				else return f
			}

			function I() {
				var a = y.x - z.x;
				var b = z.y - y.y;
				var c = Math.atan2(b, a);
				var d = Math.round(c * 180 / Math.PI);
				if (d < 0) d = 360 - Math.abs(d);
				return d
			}

			function H() {
				return Math.round(Math.sqrt(Math.pow(z.x - y.x, 2) + Math.pow(z.y - y.y, 2)))
			}

			function G(a, b) {
				if (c.allowPageScroll == h) {
					a.preventDefault()
				} else {
					var l = c.allowPageScroll == k;
					switch (b) {
					case d:
						if (c.swipeLeft && l || !l && c.allowPageScroll != i) a.preventDefault();
						break;
					case e:
						if (c.swipeRight && l || !l && c.allowPageScroll != i) a.preventDefault();
						break;
					case f:
						if (c.swipeUp && l || !l && c.allowPageScroll != j) a.preventDefault();
						break;
					case g:
						if (c.swipeDown && l || !l && c.allowPageScroll != j) a.preventDefault();
						break
					}
				}
			}

			function F(a, b) {
				if (c.swipeStatus) c.swipeStatus.call(v, a, b, direction || null, distance || 0);
				if (b == o) {
					if (c.click && (x == 1 || !p) && (isNaN(distance) || distance == 0)) c.click.call(v, a, a.target)
				}
				if (b == n) {
					if (c.swipe) {
						c.swipe.call(v, a, direction, distance)
					}
					switch (direction) {
					case d:
						if (c.swipeLeft) c.swipeLeft.call(v, a, direction, distance);
						break;
					case e:
						if (c.swipeRight) c.swipeRight.call(v, a, direction, distance);
						break;
					case f:
						if (c.swipeUp) c.swipeUp.call(v, a, direction, distance);
						break;
					case g:
						if (c.swipeDown) c.swipeDown.call(v, a, direction, distance);
						break
					}
				}
			}

			function E(a) {
				x = 0;
				y.x = 0;
				y.y = 0;
				z.x = 0;
				z.y = 0;
				A.x = 0;
				A.y = 0
			}

			function D(a) {
				a.preventDefault();
				distance = H();
				direction = J();
				if (c.triggerOnTouchEnd) {
					u = n;
					if ((x == c.fingers || !p) && z.x != 0) {
						if (distance >= c.threshold) {
							F(a, u);
							E(a)
						} else {
							u = o;
							F(a, u);
							E(a)
						}
					} else {
						u = o;
						F(a, u);
						E(a)
					}
				} else if (u == m) {
					u = o;
					F(a, u);
					E(a)
				}
				b.removeEventListener(r, C, false);
				b.removeEventListener(s, D, false)
			}

			function C(a) {
				if (u == n || u == o) return;
				var b = p ? a.touches[0] : a;
				z.x = b.pageX;
				z.y = b.pageY;
				direction = J();
				if (p) {
					x = a.touches.length
				}
				u = m;
				G(a, direction);
				if (x == c.fingers || !p) {
					distance = H();
					if (c.swipeStatus) F(a, u, direction, distance);
					if (!c.triggerOnTouchEnd) {
						if (distance >= c.threshold) {
							u = n;
							F(a, u);
							E(a)
						}
					}
				} else {
					u = o;
					F(a, u);
					E(a)
				}
			}

			function B(a) {
				var d = p ? a.touches[0] : a;
				u = l;
				if (p) {
					x = a.touches.length
				}
				distance = 0;
				direction = null;
				if (x == c.fingers || !p) {
					y.x = z.x = d.pageX;
					y.y = z.y = d.pageY;
					if (c.swipeStatus) F(a, u)
				} else {
					E(a)
				}
				b.addEventListener(r, C, false);
				b.addEventListener(s, D, false)
			}
			var b = this;
			var v = a(this);
			var w = null;
			var x = 0;
			var y = {
				x: 0,
				y: 0
			};
			var z = {
				x: 0,
				y: 0
			};
			var A = {
				x: 0,
				y: 0
			};
			try {
				this.addEventListener(q, B, false);
				this.addEventListener(t, E)
			} catch (K) {}
		})
	}
})(jQuery)