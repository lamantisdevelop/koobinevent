<?php
//posem la cookie manteniment per a 10 dies de durada
setcookie("manteniment_koobin_glwmvkso5t0wo9e0o00_mantis", "1", time() + 8640000, '', 'http://koobinevent.d359.dinaserver.com/');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='<?php echo LANGUAGE_CONTENT; ?>' xml:lang='<?php echo LANGUAGE_CONTENT; ?>' xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="css/foundation.min.css" media="screen" title="default" />
    </head>
    <body>
        <div class="row">
            <img src="/admin/image/logo_admin.png" alt="La Mantis"/>
            <div class="large-12 columns">
                <h1>Mode Desenvolupament</h1>
                <p class="panel">Acabes d'activar una <a href="http://es.wikipedia.org/wiki/Cookie_%28inform%C3%A1tica%29" target="_blank"> cookie</a> per el navegador i ordinador actual <em>(<?php echo $_SERVER['HTTP_USER_AGENT']; ?>)</em></p>
                <p>En cas de voler accedir als continguts per a altres navegadors i ordinadors, hauràs de tornar a entrar, primer, a aquesta pàgina</p>

                <a href="/index.php" class="button radius large success" >Entrar al web</a>
            </div>
        </div>
    </body>
</html>
