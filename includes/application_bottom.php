<?php
// Counter
$counter_query = tep_db_query("select startdate, counter from " . TABLE_COUNTER);

if (!tep_db_num_rows($counter_query)) {
  $date_now = date('Ymd');
  tep_db_query("insert into " . TABLE_COUNTER . " (startdate, counter) values ('" . $date_now . "', '1')");
} else {
  $counter = tep_db_fetch_array($counter_query);       
  $counter_now = ($counter['counter'] + 1);
  tep_db_query("update " . TABLE_COUNTER . " set counter = '" . $counter_now . "'");
}

// Close session
tep_session_close();
?>
