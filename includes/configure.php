<?php
/*
Definim variables per al servidor
FS = Filesystem (physical)
WS = Webserver (virtual)
*/

// Busquem cookie domain
if (substr($_SERVER["SERVER_NAME"],0,4) == "www.") {
  $cookie_domain = '.' . substr($_SERVER["SERVER_NAME"],4,strlen($_SERVER["SERVER_NAME"]));
} else {
  $cookie_domain = '.' . $_SERVER['SERVER_NAME'];
}

define('HTTP_SERVER','https://' . $_SERVER['SERVER_NAME'] . '/');   // Buides si estan a l'arrel
//define('HTTP_SERVER', '/');   // Buides si estan a l'arrel
define('DIR_WS_DOCUMENT_ROOT','');  // ROOT !!!
define('DIR_FS_DOCUMENT_ROOT', str_replace("\\","/",realpath(dirname(__FILE__)."/..").'/'));  // ROOT !!!
define('ENABLE_SSL', true); // secure webserver
define('HTTP_COOKIE_DOMAIN',$cookie_domain);
define('HTTPS_COOKIE_DOMAIN',$cookie_domain);
define('HTTP_COOKIE_PATH', '/');
define('HTTPS_COOKIE_PATH', '/');
define('BOTIGA', FALSE); // Define que ens diu si la plana que estem muntant es una botiga o no.

// Directori admin
define('DIR_WS_ADMIN', 'admin/');
define('DIR_FS_ADMIN', DIR_FS_DOCUMENT_ROOT . DIR_WS_ADMIN);

define('DIR_WS_INCLUDES', DIR_WS_ADMIN . 'includes/');  // php's inclosos
define('DIR_WS_MODULES', DIR_WS_INCLUDES . 'modules/');  // php's inclosos
define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');

// Directori portal: paths relatius i absoluts
define('DIR_WS_PORTAL', '');
define('DIR_FS_PORTAL', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL);

define('DIR_WS_BOXES', DIR_WS_PORTAL . 'moduls/boxes/');

define('DIR_WS_PORTAL_TEXTS', DIR_WS_PORTAL . 'texts/');
define('DIR_FS_PORTAL_TEXTS', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL_TEXTS);

define('DIR_WS_PORTAL_BANNERS', DIR_WS_PORTAL . 'image/banners/');
define('DIR_FS_PORTAL_BANNERS', DIR_FS_PORTAL . DIR_WS_PORTAL_BANNERS);

define('DIR_WS_PORTAL_DOWNLOADS', DIR_WS_PORTAL . 'downloads/');
define('DIR_FS_PORTAL_DOWNLOADS', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL_DOWNLOADS);

define('DIR_WS_PORTAL_GALERIES', DIR_WS_PORTAL . 'image/galeries/');
define('DIR_FS_PORTAL_GALERIES', DIR_FS_PORTAL . 'image/galeries/');

define('DIR_WS_PORTAL_FESTIVALS', DIR_WS_PORTAL . 'image/festivals/');
define('DIR_FS_PORTAL_FESTIVALS', DIR_FS_PORTAL . 'image/festivals/');

define('DIR_WS_PORTAL_DOCUMENTS', DIR_WS_PORTAL . 'image/documents/');
define('DIR_FS_PORTAL_DOCUMENTS', DIR_FS_PORTAL . 'image/documents/');

define('DIR_WS_PORTAL_IMAGE_NEWS', DIR_WS_PORTAL . 'image/noticies/');
define('DIR_FS_PORTAL_IMAGE_NEWS', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL_IMAGE_NEWS);

define('DIR_WS_PORTAL_IMAGE_CATALEG', DIR_WS_PORTAL . 'image/cataleg/');
define('DIR_FS_PORTAL_IMAGE_CATALEG', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL_IMAGE_CATALEG);

define('DIR_WS_PORTAL_IMAGE_AGENDA', DIR_WS_PORTAL . 'image/agenda/');
define('DIR_FS_PORTAL_IMAGE_AGENDA', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL_IMAGE_AGENDA);

define('DIR_WS_PORTAL_IMAGE_LLISTATS', DIR_WS_PORTAL . 'image/llistats/');
define('DIR_FS_PORTAL_IMAGE_LLISTATS', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL_IMAGE_LLISTATS);

define('DIR_WS_PORTAL_IMAGE_FOTOS', DIR_WS_PORTAL . 'image/fotos/');
define('DIR_FS_PORTAL_IMAGE_FOTOS', DIR_FS_PORTAL . 'image/fotos/');

define('DIR_WS_PORTAL_IMAGE_PROJECTES', DIR_WS_PORTAL . 'image/projectes/');
define('DIR_FS_PORTAL_IMAGE_PROJECTES', DIR_FS_PORTAL . 'image/projectes/');


/*
Definim mòduls
*/
define ('DIR_WS_MODULS','moduls/');
define ('DIR_WS_LAYOUTS','layouts/');
define ('MODUL_DEFECTE','inici');
define ('LAYOUT_DEFECTE','layout_simple.php');

/*
Definim connexió amb la Base de Dades
*/
define('DB_SERVER', '127.0.0.1');
define('DB_SERVER_USERNAME','koobinevent_adm');
define('DB_SERVER_PASSWORD','FnKrHetNTfPioQjJXtd8');
define('DB_DATABASE','koobinevent');
define('USE_PCONNECT', 'false');
define('STORE_SESSIONS', 'mysql');
?>
