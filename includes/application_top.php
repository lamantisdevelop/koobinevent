<?php
//iniciem sessió aquí dalt per tal de que funcioni el CAPCHA (sinó dona fail sempre)
session_start();

// Iniciem temporitzador
define('PAGE_PARSE_START_TIME', microtime());

// Definim nivell d'error
error_reporting(E_ALL & ~E_NOTICE);

//forcem charset a utf
header('Content-type: text/html; charset=utf-8');

// Incluim configuració
if (file_exists('includes/configure.php'))
    require('includes/configure.php');

// set the type of request (secure or not)
$request_type = (getenv('HTTPS') == 'on') ? 'SSL' : 'NONSSL';

// Definim php_self i php_url
if (!isset($PHP_SELF))
    $PHP_SELF = $_SERVER['PHP_SELF'];
if (!isset($PHP_URL))
    $PHP_URL = $_SERVER['REQUEST_URI'];
// Base de dades: Taules, funcions i connexió
require(DIR_WS_INCLUDES . 'database_tables.php');
require(DIR_WS_FUNCTIONS . 'database.php');
tep_db_connect() or die('Unable to connect to database server!');
tep_db_query("SET NAMES 'utf8'");

//mirem si estem navegant a través d'un dispositiu mòbil
//require(DIR_WS_INCLUDES . 'mobile.php');
// set application wide parameters
$configuration_query = tep_db_query('select item, value from ' . TABLE_CONFIG_VALUES . '');
while ($configuration = tep_db_fetch_array($configuration_query)) {
    //CAS mobil la constant DEFAULT_PAGE la definim al mobile.php TOTA LA RESTA ES DEFINEIXEN AQUI
    if (!(($configuration['item'] == 'DEFAULT_PAGE') && $dispositiu_mobil)) {
        //definim totes les CONSTANTS entrades a ADMIN
        define($configuration['item'], $configuration['value']);
    } else {
        define($configuration['item'], $configuration['value']);
    }
}



// define general functions used application-wide
require(DIR_WS_FUNCTIONS . 'general.php');
require(DIR_WS_FUNCTIONS . 'admin.php');
require(DIR_WS_FUNCTIONS . 'html_output.php');
//require(DIR_WS_FUNCTIONS . 'catalogue.php');
require(DIR_WS_FUNCTIONS . 'fotos.php');
require(DIR_WS_FUNCTIONS . 'llistats.php');
require(DIR_WS_FUNCTIONS . 'pages.php');
require(DIR_WS_FUNCTIONS . 'menus.php');
//require(DIR_WS_FUNCTIONS . 'edicions.php');
require(DIR_WS_FUNCTIONS . 'noticies.php');
require(DIR_WS_FUNCTIONS . 'agenda.php');


if (BOTIGA) {
    // Cistella de la compra
    require(DIR_WS_CLASSES . 'shopping_cart.php');
}

// include navigation history class
require(DIR_WS_CLASSES . 'navigation_history.php');

// define how the session functions will be used
require(DIR_WS_FUNCTIONS . 'sessions_db.php');

// Initialize session:
tep_session_name('mantisID');
tep_session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
$expireTime = 60 * 60 * 24 * 0; // 0 days
$cookie_domain = (($request_type == 'NONSSL') ? HTTP_COOKIE_DOMAIN : HTTPS_COOKIE_DOMAIN);
$cookie_path = (($request_type == 'NONSSL') ? HTTP_COOKIE_PATH : HTTPS_COOKIE_PATH);
session_set_cookie_params($expireTime, $cookie_path, $cookie_domain);

$session_started = false;
tep_session_start();
$session_started = true;

// set SID once, even if empty
$SID = (defined('SID') ? SID : '');

if (BOTIGA) {

    // create the shopping cart & fix the cart if necesary
    if (tep_session_is_registered('cart') && is_object($cart)) {
        if (PHP_VERSION < 4) {
            $broken_cart = $cart;
            $cart = new shoppingCart;
            $cart->unserialize($broken_cart);
        }
    } else {
        tep_session_register('cart');
        $cart = new shoppingCart;
    }

    // Zona botiga x EEUU
    if (!tep_session_is_registered('price_iva')) {
        tep_session_register('price_iva');
        // Eina Paisos
        require_once(DIR_WS_INCLUDES . 'geoip.inc');
        $gi = geoip_open(DIR_WS_INCLUDES . 'GeoIP.dat', GEOIP_STANDARD);
        $ip = getenv('REMOTE_ADDR');

        $aux_code = geoip_country_code_by_addr($gi, $ip);
        $ue_array = array('AS', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

        // Busquem país
        if (!in_array($aux_code, $ue_array)) {
            $price_iva = false;
            // Definim idioma i moneda per defecte
            // $_GET['language'] = 'en';
            // $_POST['currency'] = 'USD';
        } else {
            $price_iva = true;
        }
    }
}

// language
require(DIR_WS_FUNCTIONS . 'languages.php');

// set the language
if (!tep_session_is_registered('language') || isset($_GET['language'])) {
    if (!tep_session_is_registered('language')) {
        tep_session_register('language');
        tep_session_register('language_id');
        tep_session_register('language_code');
        tep_session_register('i18n_code');
    }

    include(DIR_WS_CLASSES . 'language.php');
    $lng = new language();

    if (isset($_GET['language']) && tep_not_null($_GET['language'])) {
        $lng->set_language($_GET['language']);
    } else {
        //$lng->get_browser_language();
        $lng->set_language(DEFAULT_LANGUAGE);
    }

    $language = $lng->language['directory'];
    $language_id = $lng->language['id'];
    $language_code = $lng->language['code'];
    $language_i18n_code = $lng->language['i18n_code'];
}

//INICIALITZEM ARXIUS IDIOMES (arxius .po)
$language_i18n_code = tep_get_languages_i18n_code($language_id) . '.utf8';

putenv('LC_ALL=' . $language_i18n_code);
setlocale(LC_ALL, $language_i18n_code);
bindtextdomain("default", "./texts");
bind_textdomain_codeset("default", 'UTF-8');
textdomain("default");

//Parametres inicials de funció DATE
@setlocale(LC_TIME, 'es_ES');
@date_default_timezone_set('Europe/Madrid');
define('DATE_FORMAT_SHORT', 'd/m/Y');  // this is used for date()
define('DATE_FORMAT_LONG', 'd/m/Y H:i:s'); // this is used for date()
define('PHP_DATE_TIME_FORMAT', 'd/m/Y H:i:s'); // this is used for date()
//echo($language_i18n_code);
// navigation history
if (!tep_session_is_registered('navigation')) {
    tep_session_register('navigation');
    $navigation = new navigationHistory;
}
//$navigation->add_current_page();
// include the who's online functions
require(DIR_WS_FUNCTIONS . 'whos_online.php');
tep_update_whos_online();

// entry/item info classes
require(DIR_WS_CLASSES . 'object_info.php');

// Breadcrumb
require(DIR_WS_CLASSES . 'breadcrumb.php');
$breadcrumb = new breadcrumb;
$breadcrumb->add(_('Inici'), tep_friendly_url('', tep_get_page_title(22), ''));

if (BOTIGA) {
    // Include currencies class and create an instance
    require(DIR_WS_CLASSES . 'currencies.php');
    $currencies = new currencies();

    // Currency
    if (!tep_session_is_registered('currency') || isset($_POST['currency'])) {

        if (!tep_session_is_registered('currency')) {
            tep_session_register('currency');
        }

        if (isset($_POST['currency']) && tep_not_null($_POST['currency'])) {
            $currency = $_POST['currency'];
            // Redirecció
            tep_redirect(tep_friendly_url('', $_GET['page_title'], tep_get_friendly_params()));
        } else {
            $currency = DEFAULT_CURRENCY;
        }
    }
}


// Calculate category path
/*
  if (isset($_GET['cPath'])) {
  $cPath = $_GET['cPath'];
  } else if (isset($_GET['itemID'])) {
  $cPath = tep_get_product_path($_GET['itemID']);
  } else {
  $cPath = 0;  // top
  $_GET['cPath'] = 0;
  }
 */

if (tep_not_null($cPath)) {
    $cPath_array = tep_parse_category_path($cPath);
    $cPath = implode('-', $cPath_array);
    $current_category_id = $cPath_array[(sizeof($cPath_array) - 1)];
} else {
    $cPath_array[] = '0';
    $current_category_id = 0;  // top
}


// Accions generals
if (isset($_GET['action'])) {
    // Segons les diferents accions
    switch ($_GET['action']) {

        // Shopping cart actions
        case 'update-product' :
            for ($i = 0, $n = sizeof($_POST['products_id']); $i < $n; $i++) {
                if (in_array($_POST['products_id'][$i], (is_array($_POST['cart_delete']) ? $_POST['cart_delete'] : array()))) {
                    $cart->remove($_POST['products_id'][$i]);
                } else {
                    $attributes = ($_POST['id'][$_POST['products_id'][$i]]) ? $_POST['id'][$_POST['products_id'][$i]] : '';
                    $cart->add_cart($_POST['products_id'][$i], $_POST['cart_quantity'][$i], $attributes, false);
                }
            }
            tep_redirect(tep_friendly_url('', tep_get_page_title(29), ''));
            break;

        case 'add-product' :

            if (isset($_POST['products_id']) && is_numeric($_POST['products_id'])) {
                //Debemos mirar si el producto es agrupado o no.
                if (tep_product_grouped($_POST['products_id'])) {
                    //Buscamos los productos hijos
                    $products = teb_get_all_grouped_products($_POST['products_id'], $language_id);
                    if ($products) {

                        foreach (array_keys($products) as $prod_id) {
                            if (isset($_POST['qty_' . $prod_id])) {
                                $cart->add_cart($prod_id, $_POST['qty_' . $prod_id]);
                            }
                        }
                    }
                } else {
                    $cart->add_cart($_POST['products_id'], $cart->get_quantity(tep_get_uprid($_POST['products_id'], $_POST['option_id'])) + 1, $_POST['option_id']);
                }
            }
            tep_redirect(tep_friendly_url('', tep_get_page_title(29), ''));
            break;

        // Accions usuaris
        case 'logout' :
            tep_session_unregister('aux_customer');

            tep_session_unregister('customer_id');
            tep_session_unregister('customer_id_rol');
            tep_session_unregister('customer_name');
            tep_session_unregister('customer_mail');
            tep_session_unregister('customer_activitats_id');

            if (BOTIGA) {
                tep_session_unregister('customer_default_address_id');
                tep_session_unregister('customer_country_id');
                tep_session_unregister('customer_zone_id');
                tep_session_unregister('comments');
                tep_session_unregister('intents');

                tep_session_unregister('shipping');
                tep_session_unregister('payment');
                tep_session_unregister('sendto');
                tep_session_unregister('billto');

                $cart->reset();
            }

            tep_session_close();

            tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
            break;
    }
}


/********LLISTATS********/
//busquem arbre de categories per als llistats

$group_id = 0;//inicialment estem a grup 0
$group_path = array();//array on es guardara globalment el path del grup actual

if (isset($_GET['itemID'])) {//CAS fitxa producte
    $producte = tep_db_fetch_array(tep_get_llistat_by_id($_GET['itemID']));
    $group_id = $producte['group_id'];
} else if (isset($_GET['groupID'])){//CAS llistat
    $group_id = $_GET['groupID'];
}

if ($group_id) {
    tep_get_llistats_group_path($group_path, $group_id);
}

?>
