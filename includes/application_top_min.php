<?php
// Iniciem temporitzador
define('PAGE_PARSE_START_TIME', microtime());

// Definim nivell d'error
error_reporting(E_ALL & ~E_NOTICE);

// Incluim configuració
if (file_exists('includes/configure.php')) require('includes/configure.php');

// set the type of request (secure or not)
$request_type = (getenv('HTTPS') == 'on') ? 'SSL' : 'NONSSL';

// Definim php_self i php_url
if (!isset($PHP_SELF)) $PHP_SELF = $_SERVER['PHP_SELF'];
if (!isset($PHP_URL)) $PHP_URL = $_SERVER['REQUEST_URI'];

// Base de dades: Taules, funcions i connexió
require(DIR_WS_INCLUDES . 'database_tables.php');
require(DIR_WS_FUNCTIONS . 'database.php');
tep_db_connect() or die('Unable to connect to database server!');

// set application wide parameters
$configuration_query = tep_db_query('select item, value from ' . TABLE_CONFIG_VALUES . '');
while ($configuration = tep_db_fetch_array($configuration_query)) {
  define($configuration['item'], $configuration['value']);
}

// define general functions used application-wide
require(DIR_WS_FUNCTIONS . 'general.php');
require(DIR_WS_FUNCTIONS . 'admin.php');
require(DIR_WS_FUNCTIONS . 'html_output.php');
require(DIR_WS_FUNCTIONS . 'catalogue.php');
require(DIR_WS_FUNCTIONS . 'fotos.php');
require(DIR_WS_FUNCTIONS . 'llistats.php');
require(DIR_WS_FUNCTIONS . 'pages.php');
require(DIR_WS_FUNCTIONS . 'menus.php');
require(DIR_WS_FUNCTIONS . 'edicions.php');
require(DIR_WS_FUNCTIONS . 'noticies.php');
require(DIR_WS_FUNCTIONS . 'agenda.php');


// define how the session functions will be used
require(DIR_WS_FUNCTIONS . 'sessions_db.php');

// Initialize session:
tep_session_name('mantisID');
tep_session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
$expireTime = 60*60*24*0; // 0 days
$cookie_domain = (($request_type == 'NONSSL') ? HTTP_COOKIE_DOMAIN : HTTPS_COOKIE_DOMAIN);
$cookie_path = (($request_type == 'NONSSL') ? HTTP_COOKIE_PATH : HTTPS_COOKIE_PATH);
session_set_cookie_params($expireTime,$cookie_path,$cookie_domain);

$session_started = false;
tep_session_start();
$session_started = true;

// set SID once, even if empty
$SID = (defined('SID') ? SID : '');

// language
require(DIR_WS_FUNCTIONS . 'languages.php');

// set the language
if (!tep_session_is_registered('language') || isset($_GET['language'])) {
    if (!tep_session_is_registered('language')) {
        tep_session_register('language');
        tep_session_register('language_id');
        tep_session_register('language_code');
        tep_session_register('i18n_code');
    }

    include(DIR_WS_CLASSES . 'language.php');
    $lng = new language();

    if (isset($_GET['language']) && tep_not_null($_GET['language'])) {
        $lng->set_language($_GET['language']);
    } else {
        //$lng->get_browser_language();
        $lng->set_language(DEFAULT_LANGUAGE);
    }

    $language = $lng->language['directory'];
    $language_id = $lng->language['id'];
    $language_code = $lng->language['code'];
    $language_i18n_code = $lng->language['i18n_code'];
}

//INICIALITZEM ARXIUS IDIOMES (arxius .po)
$language_i18n_code = tep_get_languages_i18n_code($language_id).'.utf8';

putenv('LC_ALL=' . $language_i18n_code);
setlocale(LC_ALL, $language_i18n_code);
bindtextdomain("default", "./texts");
bind_textdomain_codeset("default", 'UTF-8');
textdomain("default");



// entry/item info classes
require(DIR_WS_CLASSES . 'object_info.php');
?>
