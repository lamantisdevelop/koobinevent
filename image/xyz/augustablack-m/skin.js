// Garden Gnome Software - Skin
// Object2VR 3.0beta4/10565
// Filename: carregamobil.ggsk
// Generated jue abr 3 12:31:48 2014

function object2vrSkin(player,base) {
	var me=this;
	var flag=false;
	var nodeMarker=new Array();
	var activeNodeMarker=new Array();
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=new Array();
	this.elementMouseOver=new Array();
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	for(i=0;i<prefixes.length;i++) {
		if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
			cssPrefix='-' + prefixes[i].toLowerCase() + '-';
			domTransition=prefixes[i] + 'Transition';
			domTransform=prefixes[i] + 'Transform';
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=new Array();
		stack.push(startElement);
		while(stack.length>0) {
			e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=new Array();
		var stack=new Array();
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		this._loader=document.createElement('div');
		this._loader.ggId="loader";
		this._loader.ggParameter={ rx:0,ry:0,a:0,sx:0.5,sy:0.5 };
		this._loader.ggVisible=true;
		this._loader.className='ggskin ggskin_container';
		this._loader.ggType='container';
		this._loader.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				w=this.parentNode.offsetWidth;
				this.style.left=(-465 + w/2) + 'px';
				h=this.parentNode.offsetHeight;
				this.style.top=(-125 + h/2) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -465px;';
		hs+='top:  -125px;';
		hs+='width: 960px;';
		hs+='height: 40px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._loader.ggParameter) + ';';
		hs+='opacity: 0.7;';
		hs+='visibility: inherit;';
		this._loader.setAttribute('style',hs);
		this._loaderbg=document.createElement('div');
		this._loaderbg.ggId="loaderbg";
		this._loaderbg.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loaderbg.ggVisible=true;
		this._loaderbg.className='ggskin ggskin_rectangle';
		this._loaderbg.ggType='rectangle';
		hs ='position:absolute;';
		hs+='left: -1520px;';
		hs+='top:  -980px;';
		hs+='width: 4000px;';
		hs+='height: 2000px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.6;';
		hs+='visibility: inherit;';
		hs+='background: #000000;';
		hs+='border: 0px solid #000000;';
		this._loaderbg.setAttribute('style',hs);
		this._loader.appendChild(this._loaderbg);
		this._loaderbgimage=document.createElement('div');
		this._loaderbgimage.ggId="loaderbgimage";
		this._loaderbgimage.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loaderbgimage.ggVisible=true;
		this._loaderbgimage.className='ggskin ggskin_svg';
		this._loaderbgimage.ggType='svg';
		hs ='position:absolute;';
		hs+='left: 325px;';
		hs+='top:  -50px;';
		hs+='width: 150px;';
		hs+='height: 150px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loaderbgimage.setAttribute('style',hs);
		this._loaderbgimage__img=document.createElement('img');
		this._loaderbgimage__img.className='ggskin ggskin_svg';
		this._loaderbgimage__img.setAttribute('src',basePath + 'images/loaderbgimage.svg');
		this._loaderbgimage__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 150px;height: 150px;-webkit-user-drag:none;');
		this._loaderbgimage__img['ondragstart']=function() { return false; };
		this._loaderbgimage.appendChild(this._loaderbgimage__img);
		this._loader.appendChild(this._loaderbgimage);
		this._image_56=document.createElement('div');
		this._image_56.ggId="Image 56";
		this._image_56.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._image_56.ggVisible=true;
		this._image_56.className='ggskin ggskin_image';
		this._image_56.ggType='image';
		hs ='position:absolute;';
		hs+='left: 363px;';
		hs+='top:  9px;';
		hs+='width: 75px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._image_56.setAttribute('style',hs);
		this._image_56__img=document.createElement('img');
		this._image_56__img.className='ggskin ggskin_image';
		this._image_56__img.setAttribute('src',basePath + 'images/image_56.png');
		this._image_56__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._image_56__img.className='ggskin ggskin_image';
		this._image_56__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._image_56__img);
		this._image_56.appendChild(this._image_56__img);
		this._loader.appendChild(this._image_56);
		this._loadingtext=document.createElement('div');
		this._loadingtext__text=document.createElement('div');
		this._loadingtext.className='ggskin ggskin_textdiv';
		this._loadingtext.ggTextDiv=this._loadingtext__text;
		this._loadingtext.ggId="loadingtext";
		this._loadingtext.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loadingtext.ggVisible=true;
		this._loadingtext.className='ggskin ggskin_text';
		this._loadingtext.ggType='text';
		this._loadingtext.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.style.left=(387 + (31-this.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 387px;';
		hs+='top:  31px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loadingtext.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._loadingtext__text.setAttribute('style',hs);
		this._loadingtext.ggUpdateText=function() {
			var hs=(me.player.getPercentLoaded()*100.0).toFixed(0)+"%";
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
			}
		this.ggUpdatePosition();
		}
		this._loadingtext.ggUpdateText();
		this._loadingtext.appendChild(this._loadingtext__text);
		this._loader.appendChild(this._loadingtext);
		this.divSkin.appendChild(this._loader);
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
		}
		this.divSkin.ggLoaded=function() {
			if (me.player.transitionsDisabled) {
				me._loader.style[domTransition]='none';
			} else {
				me._loader.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._loader.style.opacity='0';
			me._loader.style.visibility='hidden';
		}
		this.divSkin.ggReLoaded=function() {
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
		}
		this.divSkin.ggExitFullscreen=function() {
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.changeActiveNode=function(id) {
		var newMarker=new Array();
		var i,j;
		var tags=me.player.userdata.tags;
		for (i=0;i<nodeMarker.length;i++) {
			var match=false;
			if ((nodeMarker[i].ggMarkerNodeId==id) && (id!='')) match=true;
			for(j=0;j<tags.length;j++) {
				if (nodeMarker[i].ggMarkerNodeId==tags[j]) match=true;
			}
			if (match) {
				newMarker.push(nodeMarker[i]);
			}
		}
		for(i=0;i<activeNodeMarker.length;i++) {
			if (newMarker.indexOf(activeNodeMarker[i])<0) {
				if (activeNodeMarker[i].ggMarkerNormal) {
					activeNodeMarker[i].ggMarkerNormal.style.visibility='inherit';
				}
				if (activeNodeMarker[i].ggMarkerActive) {
					activeNodeMarker[i].ggMarkerActive.style.visibility='hidden';
				}
				if (activeNodeMarker[i].ggDeactivate) {
					activeNodeMarker[i].ggDeactivate();
				}
			}
		}
		for(i=0;i<newMarker.length;i++) {
			if (activeNodeMarker.indexOf(newMarker[i])<0) {
				if (newMarker[i].ggMarkerNormal) {
					newMarker[i].ggMarkerNormal.style.visibility='hidden';
				}
				if (newMarker[i].ggMarkerActive) {
					newMarker[i].ggMarkerActive.style.visibility='inherit';
				}
				if (newMarker[i].ggActivate) {
					newMarker[i].ggActivate();
				}
			}
		}
		activeNodeMarker=newMarker;
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		var hs='';
		if (me._loaderbgimage.ggParameter) {
			hs+=parameterToTransform(me._loaderbgimage.ggParameter) + ' ';
		}
		hs+='rotate(' + (-1.0*(-800 * me.player.getPercentLoaded() + 0)) + 'deg) ';
		me._loaderbgimage.style[domTransform]=hs;
		this._loadingtext.ggUpdateText();
	};
	this.addSkin();
};