// Garden Gnome Software - Skin
// Object2VR 3.0beta4/10565
// Filename: SpinnerHTML5objetnologo.ggsk
// Generated jue abr 3 12:09:27 2014

function object2vrSkin(player,base) {
	var me=this;
	var flag=false;
	var nodeMarker=new Array();
	var activeNodeMarker=new Array();
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=new Array();
	this.elementMouseOver=new Array();
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	for(i=0;i<prefixes.length;i++) {
		if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
			cssPrefix='-' + prefixes[i].toLowerCase() + '-';
			domTransition=prefixes[i] + 'Transition';
			domTransform=prefixes[i] + 'Transform';
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=new Array();
		stack.push(startElement);
		while(stack.length>0) {
			e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=new Array();
		var stack=new Array();
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.preloadImages=function() {
		var preLoadImg=new Image();
		preLoadImg.src=basePath + 'images/rotate__o.png';
		preLoadImg.src=basePath + 'images/rotate__a.png';
		preLoadImg.src=basePath + 'images/plus__o.png';
		preLoadImg.src=basePath + 'images/plus__a.png';
		preLoadImg.src=basePath + 'images/minus__o.png';
		preLoadImg.src=basePath + 'images/minus__a.png';
		preLoadImg.src=basePath + 'images/left__o.png';
		preLoadImg.src=basePath + 'images/left__a.png';
		preLoadImg.src=basePath + 'images/up__o.png';
		preLoadImg.src=basePath + 'images/up__a.png';
		preLoadImg.src=basePath + 'images/right__o.png';
		preLoadImg.src=basePath + 'images/right__a.png';
		preLoadImg.src=basePath + 'images/down__o.png';
		preLoadImg.src=basePath + 'images/down__a.png';
		preLoadImg.src=basePath + 'images/menux__o.png';
		preLoadImg.src=basePath + 'images/menux__a.png';
		preLoadImg.src=basePath + 'images/menubtn__o.png';
		preLoadImg.src=basePath + 'images/menubtn__a.png';
	}
	
	this.addSkin=function() {
		this._menu=document.createElement('div');
		this._menu.ggId="MENU";
		this._menu.ggParameter={ rx:0,ry:0,a:0,sx:0.6,sy:0.6 };
		this._menu.ggVisible=true;
		this._menu.className='ggskin ggskin_container';
		this._menu.ggType='container';
		this._menu.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				w=this.parentNode.offsetWidth;
				this.style.left=(-14 + w/2) + 'px';
				h=this.parentNode.offsetHeight;
				this.style.top=(-33 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -14px;';
		hs+='top:  -33px;';
		hs+='width: 30px;';
		hs+='height: 30px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._menu.ggParameter) + ';';
		hs+='opacity: 0.7;';
		hs+='visibility: inherit;';
		this._menu.setAttribute('style',hs);
		this._rotate=document.createElement('div');
		this._rotate.ggId="rotate";
		this._rotate.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._rotate.ggVisible=true;
		this._rotate.className='ggskin ggskin_button';
		this._rotate.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._rotate.ggParameter) + ';';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._rotate.setAttribute('style',hs);
		this._rotate__img=document.createElement('img');
		this._rotate__img.className='ggskin ggskin_button';
		this._rotate__img.setAttribute('src',basePath + 'images/rotate.png');
		this._rotate__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._rotate__img.className='ggskin ggskin_button';
		this._rotate__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._rotate__img);
		this._rotate.appendChild(this._rotate__img);
		this._rotate.onclick=function () {
			me.player.toggleAutorotate();
		}
		this._rotate.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._rotatetip.style[domTransition]='none';
			} else {
				me._rotatetip.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._rotatetip.ggParameter.sx=1;me._rotatetip.ggParameter.sy=1;
			me._rotatetip.style[domTransform]=parameterToTransform(me._rotatetip.ggParameter);
			me._rotate__img.src=basePath + 'images/rotate__o.png';
			me._rotate.ggIsOver=true;
		}
		this._rotate.onmouseout=function () {
			me._rotatetip.style[domTransition]='none';
			me._rotatetip.ggParameter.sx=0;me._rotatetip.ggParameter.sy=0;
			me._rotatetip.style[domTransform]=parameterToTransform(me._rotatetip.ggParameter);
			me._rotate__img.src=basePath + 'images/rotate.png';
			me._rotate.ggIsOver=false;
		}
		this._rotate.onmousedown=function () {
			me._rotate__img.src=basePath + 'images/rotate__a.png';
		}
		this._rotate.onmouseup=function () {
			if (me._rotate.ggIsOver) {
				me._rotate__img.src=basePath + 'images/rotate__o.png';
			} else {
				me._rotate__img.src=basePath + 'images/rotate.png';
			}
		}
		this._rotatetip=document.createElement('div');
		this._rotatetip.ggId="rotatetip";
		this._rotatetip.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._rotatetip.ggVisible=true;
		this._rotatetip.className='ggskin ggskin_image';
		this._rotatetip.ggType='image';
		hs ='position:absolute;';
		hs+='left: -29px;';
		hs+='top:  -28px;';
		hs+='width: 90px;';
		hs+='height: 26px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._rotatetip.ggParameter) + ';';
		hs+='opacity: 0.8;';
		hs+='visibility: inherit;';
		this._rotatetip.setAttribute('style',hs);
		this._rotatetip__img=document.createElement('img');
		this._rotatetip__img.className='ggskin ggskin_image';
		this._rotatetip__img.setAttribute('src',basePath + 'images/rotatetip.png');
		this._rotatetip__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._rotatetip__img.className='ggskin ggskin_image';
		this._rotatetip__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._rotatetip__img);
		this._rotatetip.appendChild(this._rotatetip__img);
		this._rotate.appendChild(this._rotatetip);
		this._menu.appendChild(this._rotate);
		this._plus=document.createElement('div');
		this._plus.ggId="plus";
		this._plus.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._plus.ggVisible=true;
		this._plus.className='ggskin ggskin_button';
		this._plus.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._plus.ggParameter) + ';';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._plus.setAttribute('style',hs);
		this._plus__img=document.createElement('img');
		this._plus__img.className='ggskin ggskin_button';
		this._plus__img.setAttribute('src',basePath + 'images/plus.png');
		this._plus__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._plus__img.className='ggskin ggskin_button';
		this._plus__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._plus__img);
		this._plus.appendChild(this._plus__img);
		this._plus.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._zoomtip0.style[domTransition]='none';
			} else {
				me._zoomtip0.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._zoomtip0.ggParameter.sx=1;me._zoomtip0.ggParameter.sy=1;
			me._zoomtip0.style[domTransform]=parameterToTransform(me._zoomtip0.ggParameter);
			me._plus__img.src=basePath + 'images/plus__o.png';
			me._plus.ggIsOver=true;
		}
		this._plus.onmouseout=function () {
			me._zoomtip0.style[domTransition]='none';
			me._zoomtip0.ggParameter.sx=0;me._zoomtip0.ggParameter.sy=0;
			me._zoomtip0.style[domTransform]=parameterToTransform(me._zoomtip0.ggParameter);
			me._plus__img.src=basePath + 'images/plus.png';
			me._plus.ggIsOver=false;
			me.elementMouseDown['plus']=false;
		}
		this._plus.onmousedown=function () {
			me._plus__img.src=basePath + 'images/plus__a.png';
			me.elementMouseDown['plus']=true;
		}
		this._plus.onmouseup=function () {
			if (me._plus.ggIsOver) {
				me._plus__img.src=basePath + 'images/plus__o.png';
			} else {
				me._plus__img.src=basePath + 'images/plus.png';
			}
			me.elementMouseDown['plus']=false;
		}
		this._plus.ontouchend=function () {
			me.elementMouseDown['plus']=false;
		}
		this._zoomtip0=document.createElement('div');
		this._zoomtip0.ggId="zoom+tip";
		this._zoomtip0.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._zoomtip0.ggVisible=true;
		this._zoomtip0.className='ggskin ggskin_image';
		this._zoomtip0.ggType='image';
		hs ='position:absolute;';
		hs+='left: -19px;';
		hs+='top:  -28px;';
		hs+='width: 70px;';
		hs+='height: 26px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._zoomtip0.ggParameter) + ';';
		hs+='opacity: 0.8;';
		hs+='visibility: inherit;';
		this._zoomtip0.setAttribute('style',hs);
		this._zoomtip0__img=document.createElement('img');
		this._zoomtip0__img.className='ggskin ggskin_image';
		this._zoomtip0__img.setAttribute('src',basePath + 'images/zoomtip0.png');
		this._zoomtip0__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._zoomtip0__img.className='ggskin ggskin_image';
		this._zoomtip0__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._zoomtip0__img);
		this._zoomtip0.appendChild(this._zoomtip0__img);
		this._plus.appendChild(this._zoomtip0);
		this._menu.appendChild(this._plus);
		this._minus=document.createElement('div');
		this._minus.ggId="minus";
		this._minus.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._minus.ggVisible=true;
		this._minus.className='ggskin ggskin_button';
		this._minus.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._minus.ggParameter) + ';';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._minus.setAttribute('style',hs);
		this._minus__img=document.createElement('img');
		this._minus__img.className='ggskin ggskin_button';
		this._minus__img.setAttribute('src',basePath + 'images/minus.png');
		this._minus__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._minus__img.className='ggskin ggskin_button';
		this._minus__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._minus__img);
		this._minus.appendChild(this._minus__img);
		this._minus.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._zoomtip.style[domTransition]='none';
			} else {
				me._zoomtip.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._zoomtip.ggParameter.sx=1;me._zoomtip.ggParameter.sy=1;
			me._zoomtip.style[domTransform]=parameterToTransform(me._zoomtip.ggParameter);
			me._minus__img.src=basePath + 'images/minus__o.png';
			me._minus.ggIsOver=true;
		}
		this._minus.onmouseout=function () {
			me._zoomtip.style[domTransition]='none';
			me._zoomtip.ggParameter.sx=0;me._zoomtip.ggParameter.sy=0;
			me._zoomtip.style[domTransform]=parameterToTransform(me._zoomtip.ggParameter);
			me._minus__img.src=basePath + 'images/minus.png';
			me._minus.ggIsOver=false;
			me.elementMouseDown['minus']=false;
		}
		this._minus.onmousedown=function () {
			me._minus__img.src=basePath + 'images/minus__a.png';
			me.elementMouseDown['minus']=true;
		}
		this._minus.onmouseup=function () {
			if (me._minus.ggIsOver) {
				me._minus__img.src=basePath + 'images/minus__o.png';
			} else {
				me._minus__img.src=basePath + 'images/minus.png';
			}
			me.elementMouseDown['minus']=false;
		}
		this._minus.ontouchend=function () {
			me.elementMouseDown['minus']=false;
		}
		this._zoomtip=document.createElement('div');
		this._zoomtip.ggId="zoom-tip";
		this._zoomtip.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._zoomtip.ggVisible=true;
		this._zoomtip.className='ggskin ggskin_image';
		this._zoomtip.ggType='image';
		hs ='position:absolute;';
		hs+='left: -19px;';
		hs+='top:  -28px;';
		hs+='width: 70px;';
		hs+='height: 26px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._zoomtip.ggParameter) + ';';
		hs+='opacity: 0.8;';
		hs+='visibility: inherit;';
		this._zoomtip.setAttribute('style',hs);
		this._zoomtip__img=document.createElement('img');
		this._zoomtip__img.className='ggskin ggskin_image';
		this._zoomtip__img.setAttribute('src',basePath + 'images/zoomtip.png');
		this._zoomtip__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._zoomtip__img.className='ggskin ggskin_image';
		this._zoomtip__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._zoomtip__img);
		this._zoomtip.appendChild(this._zoomtip__img);
		this._minus.appendChild(this._zoomtip);
		this._menu.appendChild(this._minus);
		this._left=document.createElement('div');
		this._left.ggId="left";
		this._left.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._left.ggVisible=true;
		this._left.className='ggskin ggskin_button';
		this._left.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._left.ggParameter) + ';';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._left.setAttribute('style',hs);
		this._left__img=document.createElement('img');
		this._left__img.className='ggskin ggskin_button';
		this._left__img.setAttribute('src',basePath + 'images/left.png');
		this._left__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._left__img.className='ggskin ggskin_button';
		this._left__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._left__img);
		this._left.appendChild(this._left__img);
		this._left.onmouseover=function () {
			me._left__img.src=basePath + 'images/left__o.png';
			me._left.ggIsOver=true;
		}
		this._left.onmouseout=function () {
			me._left__img.src=basePath + 'images/left.png';
			me._left.ggIsOver=false;
			me.elementMouseDown['left']=false;
		}
		this._left.onmousedown=function () {
			me._left__img.src=basePath + 'images/left__a.png';
			me.elementMouseDown['left']=true;
		}
		this._left.onmouseup=function () {
			if (me._left.ggIsOver) {
				me._left__img.src=basePath + 'images/left__o.png';
			} else {
				me._left__img.src=basePath + 'images/left.png';
			}
			me.elementMouseDown['left']=false;
		}
		this._left.ontouchend=function () {
			me.elementMouseDown['left']=false;
		}
		this._menu.appendChild(this._left);
		this._up=document.createElement('div');
		this._up.ggId="up";
		this._up.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._up.ggVisible=false;
		this._up.className='ggskin ggskin_button';
		this._up.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._up.ggParameter) + ';';
		hs+='visibility: hidden;';
		this._up.setAttribute('style',hs);
		this._up__img=document.createElement('img');
		this._up__img.className='ggskin ggskin_button';
		this._up__img.setAttribute('src',basePath + 'images/up.png');
		this._up__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._up__img.className='ggskin ggskin_button';
		this._up__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._up__img);
		this._up.appendChild(this._up__img);
		this._up.onmouseover=function () {
			me._up__img.src=basePath + 'images/up__o.png';
			me._up.ggIsOver=true;
		}
		this._up.onmouseout=function () {
			me._up__img.src=basePath + 'images/up.png';
			me._up.ggIsOver=false;
			me.elementMouseDown['up']=false;
		}
		this._up.onmousedown=function () {
			me._up__img.src=basePath + 'images/up__a.png';
			me.elementMouseDown['up']=true;
		}
		this._up.onmouseup=function () {
			if (me._up.ggIsOver) {
				me._up__img.src=basePath + 'images/up__o.png';
			} else {
				me._up__img.src=basePath + 'images/up.png';
			}
			me.elementMouseDown['up']=false;
		}
		this._up.ontouchend=function () {
			me.elementMouseDown['up']=false;
		}
		this._menu.appendChild(this._up);
		this._right=document.createElement('div');
		this._right.ggId="right";
		this._right.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._right.ggVisible=true;
		this._right.className='ggskin ggskin_button';
		this._right.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._right.ggParameter) + ';';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._right.setAttribute('style',hs);
		this._right__img=document.createElement('img');
		this._right__img.className='ggskin ggskin_button';
		this._right__img.setAttribute('src',basePath + 'images/right.png');
		this._right__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._right__img.className='ggskin ggskin_button';
		this._right__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._right__img);
		this._right.appendChild(this._right__img);
		this._right.onmouseover=function () {
			me._right__img.src=basePath + 'images/right__o.png';
			me._right.ggIsOver=true;
		}
		this._right.onmouseout=function () {
			me._right__img.src=basePath + 'images/right.png';
			me._right.ggIsOver=false;
			me.elementMouseDown['right']=false;
		}
		this._right.onmousedown=function () {
			me._right__img.src=basePath + 'images/right__a.png';
			me.elementMouseDown['right']=true;
		}
		this._right.onmouseup=function () {
			if (me._right.ggIsOver) {
				me._right__img.src=basePath + 'images/right__o.png';
			} else {
				me._right__img.src=basePath + 'images/right.png';
			}
			me.elementMouseDown['right']=false;
		}
		this._right.ontouchend=function () {
			me.elementMouseDown['right']=false;
		}
		this._menu.appendChild(this._right);
		this._down=document.createElement('div');
		this._down.ggId="down";
		this._down.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._down.ggVisible=false;
		this._down.className='ggskin ggskin_button';
		this._down.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._down.ggParameter) + ';';
		hs+='visibility: hidden;';
		this._down.setAttribute('style',hs);
		this._down__img=document.createElement('img');
		this._down__img.className='ggskin ggskin_button';
		this._down__img.setAttribute('src',basePath + 'images/down.png');
		this._down__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._down__img.className='ggskin ggskin_button';
		this._down__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._down__img);
		this._down.appendChild(this._down__img);
		this._down.onmouseover=function () {
			me._down__img.src=basePath + 'images/down__o.png';
			me._down.ggIsOver=true;
		}
		this._down.onmouseout=function () {
			me._down__img.src=basePath + 'images/down.png';
			me._down.ggIsOver=false;
			me.elementMouseDown['down']=false;
		}
		this._down.onmousedown=function () {
			me._down__img.src=basePath + 'images/down__a.png';
			me.elementMouseDown['down']=true;
		}
		this._down.onmouseup=function () {
			if (me._down.ggIsOver) {
				me._down__img.src=basePath + 'images/down__o.png';
			} else {
				me._down__img.src=basePath + 'images/down.png';
			}
			me.elementMouseDown['down']=false;
		}
		this._down.ontouchend=function () {
			me.elementMouseDown['down']=false;
		}
		this._menu.appendChild(this._down);
		this._menux=document.createElement('div');
		this._menux.ggId="menux";
		this._menux.ggParameter={ rx:0,ry:0,a:-45,sx:1,sy:1 };
		this._menux.ggVisible=true;
		this._menux.className='ggskin ggskin_button';
		this._menux.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._menux.ggParameter) + ';';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._menux.setAttribute('style',hs);
		this._menux__img=document.createElement('img');
		this._menux__img.className='ggskin ggskin_button';
		this._menux__img.setAttribute('src',basePath + 'images/menux.png');
		this._menux__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._menux__img.className='ggskin ggskin_button';
		this._menux__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._menux__img);
		this._menux.appendChild(this._menux__img);
		this._menux.onclick=function () {
			me._menubtn.onclick();
		}
		this._menux.onmouseover=function () {
			me._menux__img.src=basePath + 'images/menux__o.png';
			me._menux.ggIsOver=true;
		}
		this._menux.onmouseout=function () {
			me._menux__img.src=basePath + 'images/menux.png';
			me._menux.ggIsOver=false;
		}
		this._menux.onmousedown=function () {
			me._menux__img.src=basePath + 'images/menux__a.png';
		}
		this._menux.onmouseup=function () {
			if (me._menux.ggIsOver) {
				me._menux__img.src=basePath + 'images/menux__o.png';
			} else {
				me._menux__img.src=basePath + 'images/menux.png';
			}
		}
		this._menu.appendChild(this._menux);
		this._menubtn=document.createElement('div');
		this._menubtn.ggId="menubtn";
		this._menubtn.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._menubtn.ggVisible=true;
		this._menubtn.className='ggskin ggskin_button';
		this._menubtn.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 31px;';
		hs+='height: 31px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._menubtn.setAttribute('style',hs);
		this._menubtn__img=document.createElement('img');
		this._menubtn__img.className='ggskin ggskin_button';
		this._menubtn__img.setAttribute('src',basePath + 'images/menubtn.png');
		this._menubtn__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._menubtn__img.className='ggskin ggskin_button';
		this._menubtn__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._menubtn__img);
		this._menubtn.appendChild(this._menubtn__img);
		this._menubtn.onclick=function () {
			flag=me._menubtn.ggAngleActive;
			if (me.player.transitionsDisabled) {
				me._menubtn.style[domTransition]='none';
			} else {
				me._menubtn.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._menubtn.ggParameter.a=0;
				me._menubtn.style[domTransform]=parameterToTransform(me._menubtn.ggParameter);
			} else {
				me._menubtn.ggParameter.a="45";
				me._menubtn.style[domTransform]=parameterToTransform(me._menubtn.ggParameter);
			}
			me._menubtn.ggAngleActive=!flag;
			flag=me._menubtn.ggOpacitiyActive;
			if (me.player.transitionsDisabled) {
				me._menubtn.style[domTransition]='none';
			} else {
				me._menubtn.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._menubtn.style.opacity='1';
				me._menubtn.style.visibility=me._menubtn.ggVisible?'inherit':'hidden';
			} else {
				me._menubtn.style.opacity='0';
				me._menubtn.style.visibility='hidden';
			}
			me._menubtn.ggOpacitiyActive=!flag;
			flag=me._left.ggScaleActive;
			if (me.player.transitionsDisabled) {
				me._left.style[domTransition]='none';
			} else {
				me._left.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._left.ggParameter.sx=0;me._left.ggParameter.sy=0;
				me._left.style[domTransform]=parameterToTransform(me._left.ggParameter);
			} else {
				me._left.ggParameter.sx=1;me._left.ggParameter.sy=1;
				me._left.style[domTransform]=parameterToTransform(me._left.ggParameter);
			}
			me._left.ggScaleActive=!flag;
			flag=me._left.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._left.style[domTransition]='none';
			} else {
				me._left.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._left.ggParameter.rx=0;me._left.ggParameter.ry=0;
				me._left.style[domTransform]=parameterToTransform(me._left.ggParameter);
			} else {
				me._left.ggParameter.rx=-40;me._left.ggParameter.ry=0;
				me._left.style[domTransform]=parameterToTransform(me._left.ggParameter);
			}
			me._left.ggPositonActive=!flag;
			flag=me._right.ggScaleActive;
			if (me.player.transitionsDisabled) {
				me._right.style[domTransition]='none';
			} else {
				me._right.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._right.ggParameter.sx=0;me._right.ggParameter.sy=0;
				me._right.style[domTransform]=parameterToTransform(me._right.ggParameter);
			} else {
				me._right.ggParameter.sx=1;me._right.ggParameter.sy=1;
				me._right.style[domTransform]=parameterToTransform(me._right.ggParameter);
			}
			me._right.ggScaleActive=!flag;
			flag=me._right.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._right.style[domTransition]='none';
			} else {
				me._right.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._right.ggParameter.rx=0;me._right.ggParameter.ry=0;
				me._right.style[domTransform]=parameterToTransform(me._right.ggParameter);
			} else {
				me._right.ggParameter.rx=40;me._right.ggParameter.ry=0;
				me._right.style[domTransform]=parameterToTransform(me._right.ggParameter);
			}
			me._right.ggPositonActive=!flag;
			flag=me._up.ggScaleActive;
			if (me.player.transitionsDisabled) {
				me._up.style[domTransition]='none';
			} else {
				me._up.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._up.ggParameter.sx=0;me._up.ggParameter.sy=0;
				me._up.style[domTransform]=parameterToTransform(me._up.ggParameter);
			} else {
				me._up.ggParameter.sx=1;me._up.ggParameter.sy=1;
				me._up.style[domTransform]=parameterToTransform(me._up.ggParameter);
			}
			me._up.ggScaleActive=!flag;
			flag=me._up.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._up.style[domTransition]='none';
			} else {
				me._up.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._up.ggParameter.rx=0;me._up.ggParameter.ry=0;
				me._up.style[domTransform]=parameterToTransform(me._up.ggParameter);
			} else {
				me._up.ggParameter.rx=0;me._up.ggParameter.ry=-40;
				me._up.style[domTransform]=parameterToTransform(me._up.ggParameter);
			}
			me._up.ggPositonActive=!flag;
			flag=me._down.ggScaleActive;
			if (me.player.transitionsDisabled) {
				me._down.style[domTransition]='none';
			} else {
				me._down.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._down.ggParameter.sx=0;me._down.ggParameter.sy=0;
				me._down.style[domTransform]=parameterToTransform(me._down.ggParameter);
			} else {
				me._down.ggParameter.sx=1;me._down.ggParameter.sy=1;
				me._down.style[domTransform]=parameterToTransform(me._down.ggParameter);
			}
			me._down.ggScaleActive=!flag;
			flag=me._down.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._down.style[domTransition]='none';
			} else {
				me._down.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._down.ggParameter.rx=0;me._down.ggParameter.ry=0;
				me._down.style[domTransform]=parameterToTransform(me._down.ggParameter);
			} else {
				me._down.ggParameter.rx=0;me._down.ggParameter.ry=40;
				me._down.style[domTransform]=parameterToTransform(me._down.ggParameter);
			}
			me._down.ggPositonActive=!flag;
			flag=me._menux.ggAngleActive;
			if (me.player.transitionsDisabled) {
				me._menux.style[domTransition]='none';
			} else {
				me._menux.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._menux.ggParameter.a=-45;
				me._menux.style[domTransform]=parameterToTransform(me._menux.ggParameter);
			} else {
				me._menux.ggParameter.a="0";
				me._menux.style[domTransform]=parameterToTransform(me._menux.ggParameter);
			}
			me._menux.ggAngleActive=!flag;
			flag=me._plus.ggScaleActive;
			if (me.player.transitionsDisabled) {
				me._plus.style[domTransition]='none';
			} else {
				me._plus.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._plus.ggParameter.sx=0;me._plus.ggParameter.sy=0;
				me._plus.style[domTransform]=parameterToTransform(me._plus.ggParameter);
			} else {
				me._plus.ggParameter.sx=1;me._plus.ggParameter.sy=1;
				me._plus.style[domTransform]=parameterToTransform(me._plus.ggParameter);
			}
			me._plus.ggScaleActive=!flag;
			flag=me._plus.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._plus.style[domTransition]='none';
			} else {
				me._plus.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._plus.ggParameter.rx=0;me._plus.ggParameter.ry=0;
				me._plus.style[domTransform]=parameterToTransform(me._plus.ggParameter);
			} else {
				me._plus.ggParameter.rx=-120;me._plus.ggParameter.ry=0;
				me._plus.style[domTransform]=parameterToTransform(me._plus.ggParameter);
			}
			me._plus.ggPositonActive=!flag;
			flag=me._minus.ggScaleActive;
			if (me.player.transitionsDisabled) {
				me._minus.style[domTransition]='none';
			} else {
				me._minus.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._minus.ggParameter.sx=0;me._minus.ggParameter.sy=0;
				me._minus.style[domTransform]=parameterToTransform(me._minus.ggParameter);
			} else {
				me._minus.ggParameter.sx=1;me._minus.ggParameter.sy=1;
				me._minus.style[domTransform]=parameterToTransform(me._minus.ggParameter);
			}
			me._minus.ggScaleActive=!flag;
			flag=me._minus.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._minus.style[domTransition]='none';
			} else {
				me._minus.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._minus.ggParameter.rx=0;me._minus.ggParameter.ry=0;
				me._minus.style[domTransform]=parameterToTransform(me._minus.ggParameter);
			} else {
				me._minus.ggParameter.rx=-80;me._minus.ggParameter.ry=0;
				me._minus.style[domTransform]=parameterToTransform(me._minus.ggParameter);
			}
			me._minus.ggPositonActive=!flag;
			flag=me._rotate.ggScaleActive;
			if (me.player.transitionsDisabled) {
				me._rotate.style[domTransition]='none';
			} else {
				me._rotate.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._rotate.ggParameter.sx=0;me._rotate.ggParameter.sy=0;
				me._rotate.style[domTransform]=parameterToTransform(me._rotate.ggParameter);
			} else {
				me._rotate.ggParameter.sx=1;me._rotate.ggParameter.sy=1;
				me._rotate.style[domTransform]=parameterToTransform(me._rotate.ggParameter);
			}
			me._rotate.ggScaleActive=!flag;
			flag=me._rotate.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._rotate.style[domTransition]='none';
			} else {
				me._rotate.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._rotate.ggParameter.rx=0;me._rotate.ggParameter.ry=0;
				me._rotate.style[domTransform]=parameterToTransform(me._rotate.ggParameter);
			} else {
				me._rotate.ggParameter.rx=80;me._rotate.ggParameter.ry=0;
				me._rotate.style[domTransform]=parameterToTransform(me._rotate.ggParameter);
			}
			me._rotate.ggPositonActive=!flag;
		}
		this._menubtn.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._image_64.style[domTransition]='none';
			} else {
				me._image_64.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._image_64.style.opacity='0';
			me._image_64.style.visibility='hidden';
			me._menubtn__img.src=basePath + 'images/menubtn__o.png';
			me._menubtn.ggIsOver=true;
		}
		this._menubtn.onmouseout=function () {
			me._menubtn__img.src=basePath + 'images/menubtn.png';
			me._menubtn.ggIsOver=false;
		}
		this._menubtn.onmousedown=function () {
			me._menubtn__img.src=basePath + 'images/menubtn__a.png';
		}
		this._menubtn.onmouseup=function () {
			if (me._menubtn.ggIsOver) {
				me._menubtn__img.src=basePath + 'images/menubtn__o.png';
			} else {
				me._menubtn__img.src=basePath + 'images/menubtn.png';
			}
		}
		this._menu.appendChild(this._menubtn);
		this._image_64=document.createElement('div');
		this._image_64.ggId="Image 64";
		this._image_64.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._image_64.ggVisible=true;
		this._image_64.className='ggskin ggskin_image';
		this._image_64.ggType='image';
		hs ='position:absolute;';
		hs+='left: -14px;';
		hs+='top:  -30px;';
		hs+='width: 60px;';
		hs+='height: 26px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._image_64.ggParameter) + ';';
		hs+='visibility: inherit;';
		this._image_64.setAttribute('style',hs);
		this._image_64__img=document.createElement('img');
		this._image_64__img.className='ggskin ggskin_image';
		this._image_64__img.setAttribute('src',basePath + 'images/image_64.png');
		this._image_64__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._image_64__img.className='ggskin ggskin_image';
		this._image_64__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._image_64__img);
		this._image_64.appendChild(this._image_64__img);
		this._menu.appendChild(this._image_64);
		this.divSkin.appendChild(this._menu);
		this._loader=document.createElement('div');
		this._loader.ggId="loader";
		this._loader.ggParameter={ rx:0,ry:0,a:0,sx:0.5,sy:0.5 };
		this._loader.ggVisible=true;
		this._loader.className='ggskin ggskin_container';
		this._loader.ggType='container';
		this._loader.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				w=this.parentNode.offsetWidth;
				this.style.left=(-444 + w/2) + 'px';
				h=this.parentNode.offsetHeight;
				this.style.top=(-35 + h/2) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -444px;';
		hs+='top:  -35px;';
		hs+='width: 960px;';
		hs+='height: 40px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+=cssPrefix + 'transform: ' + parameterToTransform(this._loader.ggParameter) + ';';
		hs+='opacity: 0.7;';
		hs+='visibility: inherit;';
		this._loader.setAttribute('style',hs);
		this._loaderbg=document.createElement('div');
		this._loaderbg.ggId="loaderbg";
		this._loaderbg.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loaderbg.ggVisible=true;
		this._loaderbg.className='ggskin ggskin_rectangle';
		this._loaderbg.ggType='rectangle';
		hs ='position:absolute;';
		hs+='left: -1520px;';
		hs+='top:  -980px;';
		hs+='width: 4000px;';
		hs+='height: 2000px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.6;';
		hs+='visibility: inherit;';
		hs+='background: #000000;';
		hs+='border: 0px solid #000000;';
		this._loaderbg.setAttribute('style',hs);
		this._loader.appendChild(this._loaderbg);
		this._loaderbgimage=document.createElement('div');
		this._loaderbgimage.ggId="loaderbgimage";
		this._loaderbgimage.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loaderbgimage.ggVisible=true;
		this._loaderbgimage.className='ggskin ggskin_svg';
		this._loaderbgimage.ggType='svg';
		hs ='position:absolute;';
		hs+='left: 325px;';
		hs+='top:  -50px;';
		hs+='width: 150px;';
		hs+='height: 150px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loaderbgimage.setAttribute('style',hs);
		this._loaderbgimage__img=document.createElement('img');
		this._loaderbgimage__img.className='ggskin ggskin_svg';
		this._loaderbgimage__img.setAttribute('src',basePath + 'images/loaderbgimage.svg');
		this._loaderbgimage__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 150px;height: 150px;-webkit-user-drag:none;');
		this._loaderbgimage__img['ondragstart']=function() { return false; };
		this._loaderbgimage.appendChild(this._loaderbgimage__img);
		this._loader.appendChild(this._loaderbgimage);
		this._image_56=document.createElement('div');
		this._image_56.ggId="Image 56";
		this._image_56.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._image_56.ggVisible=true;
		this._image_56.className='ggskin ggskin_image';
		this._image_56.ggType='image';
		hs ='position:absolute;';
		hs+='left: 363px;';
		hs+='top:  9px;';
		hs+='width: 75px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._image_56.setAttribute('style',hs);
		this._image_56__img=document.createElement('img');
		this._image_56__img.className='ggskin ggskin_image';
		this._image_56__img.setAttribute('src',basePath + 'images/image_56.png');
		this._image_56__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._image_56__img.className='ggskin ggskin_image';
		this._image_56__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._image_56__img);
		this._image_56.appendChild(this._image_56__img);
		this._loader.appendChild(this._image_56);
		this._loadingtext=document.createElement('div');
		this._loadingtext__text=document.createElement('div');
		this._loadingtext.className='ggskin ggskin_textdiv';
		this._loadingtext.ggTextDiv=this._loadingtext__text;
		this._loadingtext.ggId="loadingtext";
		this._loadingtext.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loadingtext.ggVisible=true;
		this._loadingtext.className='ggskin ggskin_text';
		this._loadingtext.ggType='text';
		this._loadingtext.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.style.left=(387 + (31-this.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 387px;';
		hs+='top:  31px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loadingtext.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._loadingtext__text.setAttribute('style',hs);
		this._loadingtext.ggUpdateText=function() {
			var hs=(me.player.getPercentLoaded()*100.0).toFixed(0)+"%";
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
			}
		this.ggUpdatePosition();
		}
		this._loadingtext.ggUpdateText();
		this._loadingtext.appendChild(this._loadingtext__text);
		this._loader.appendChild(this._loadingtext);
		this.divSkin.appendChild(this._loader);
		this.preloadImages();
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
		}
		this.divSkin.ggLoaded=function() {
			if (me.player.transitionsDisabled) {
				me._loader.style[domTransition]='none';
			} else {
				me._loader.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._loader.style.opacity='0';
			me._loader.style.visibility='hidden';
			if (me.player.transitionsDisabled) {
				me._image_64.style[domTransition]='none';
			} else {
				me._image_64.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._image_64.ggParameter.sx=1;me._image_64.ggParameter.sy=1;
			me._image_64.style[domTransform]=parameterToTransform(me._image_64.ggParameter);
		}
		this.divSkin.ggReLoaded=function() {
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
		}
		this.divSkin.ggExitFullscreen=function() {
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.changeActiveNode=function(id) {
		var newMarker=new Array();
		var i,j;
		var tags=me.player.userdata.tags;
		for (i=0;i<nodeMarker.length;i++) {
			var match=false;
			if ((nodeMarker[i].ggMarkerNodeId==id) && (id!='')) match=true;
			for(j=0;j<tags.length;j++) {
				if (nodeMarker[i].ggMarkerNodeId==tags[j]) match=true;
			}
			if (match) {
				newMarker.push(nodeMarker[i]);
			}
		}
		for(i=0;i<activeNodeMarker.length;i++) {
			if (newMarker.indexOf(activeNodeMarker[i])<0) {
				if (activeNodeMarker[i].ggMarkerNormal) {
					activeNodeMarker[i].ggMarkerNormal.style.visibility='inherit';
				}
				if (activeNodeMarker[i].ggMarkerActive) {
					activeNodeMarker[i].ggMarkerActive.style.visibility='hidden';
				}
				if (activeNodeMarker[i].ggDeactivate) {
					activeNodeMarker[i].ggDeactivate();
				}
			}
		}
		for(i=0;i<newMarker.length;i++) {
			if (activeNodeMarker.indexOf(newMarker[i])<0) {
				if (newMarker[i].ggMarkerNormal) {
					newMarker[i].ggMarkerNormal.style.visibility='hidden';
				}
				if (newMarker[i].ggMarkerActive) {
					newMarker[i].ggMarkerActive.style.visibility='inherit';
				}
				if (newMarker[i].ggActivate) {
					newMarker[i].ggActivate();
				}
			}
		}
		activeNodeMarker=newMarker;
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		if (me.elementMouseDown['plus']) {
			me.player.changeFovLog(-0.3,true);
		}
		if (me.elementMouseDown['minus']) {
			me.player.changeFovLog(0.3,true);
		}
		if (me.elementMouseDown['left']) {
			me.player.changePanLog(0.3,true);
		}
		if (me.elementMouseDown['up']) {
			me.player.changeTiltLog(0.3,true);
		}
		if (me.elementMouseDown['right']) {
			me.player.changePanLog(-0.3,true);
		}
		if (me.elementMouseDown['down']) {
			me.player.changeTiltLog(-0.3,true);
		}
		var hs='';
		if (me._loaderbgimage.ggParameter) {
			hs+=parameterToTransform(me._loaderbgimage.ggParameter) + ' ';
		}
		hs+='rotate(' + (-1.0*(-800 * me.player.getPercentLoaded() + 0)) + 'deg) ';
		me._loaderbgimage.style[domTransform]=hs;
		this._loadingtext.ggUpdateText();
	};
	this.addSkin();
};