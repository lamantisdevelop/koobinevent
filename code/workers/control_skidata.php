<?php

chdir(dirname(__FILE__));
chdir("..");

include("include/GENERIC/generic.small.php");

define("DBTYPE", "dbmysqli");

include("include/GENERIC/db.funciones.php");

include("include/DB/" . DBTYPE . ".php");

spl_autoload_register('carga_clases');

define("USA_AMAZON", "true");
define("PATH", str_replace("\\", "/", realpath(getcwd())));
define("TMP_PATH", sys_get_temp_dir() . "/koobin");
//define("__DESARROLLO", true);

error_reporting(E_ALL);
ini_set("display_errors", "Off");
ini_set("log_errors", true);
if (!is_dir(sys_get_temp_dir() . "/koobin/"))
{
	mkdir(sys_get_temp_dir() . "/koobin/");
}
ini_set("error_log", sys_get_temp_dir() . "/koobin/php-error.log");

define("NOMBRE_COLA", "control_skidata");

// Guardamos el timestamp de última modificación del worker, por si hay que matarlo para que se reinicie
clearstatcache(true, __FILE__);
$ultima_modificacion = @filemtime(__FILE__);

while (true)
{
	list($header, $mensaje) = cola::recibir(NOMBRE_COLA);

	if ($header)
	{
		// Comprobamos que tengamos una acción (acc) que realizar
		if ($mensaje->accion)
		{
			$datos = Array();
			$datos["host"] = convert($mensaje->__MYSQL_HOST);
			$datos["username"] = convert($mensaje->__MYSQL_USERNAME);
			$datos["passwd"] = convert($mensaje->__MYSQL_PASSWD);
			$datos["database"] = convert($mensaje->__MYSQL_DATABASE);

			$carpeta_ftp = convert($mensaje->__CARPETA_CLIENTE);

			if (desarrollo())
			{
				$ruta_ftp_skidata = TMP_PATH . "/ftp" . $carpeta_ftp;
			}
			else
			{
				$ruta_ftp_skidata = "/var/www/ftp/" . $carpeta_ftp;
			}

			if (is_dir($ruta_ftp_skidata))
			{
				$con = db_connect($datos["host"], $datos["username"], $datos["passwd"], $datos["database"]);

				$archivo_zip = str_replace("\\", "/", $ruta_ftp_skidata . "/codigos_skidata.zip");
				$directorio_zip = dirname($archivo_zip);

				if ($mensaje->accion == 'EVENTO' && $mensaje->id_evento)
				{
					// si la acción es "EVENTO" (nuevo evento a controlar) y recibimos correctamente el "id_evento",
					// quiere decir que se ha añadido el evento al control y hay que generar todos sus códigos
					$id_evento = $mensaje->id_evento;

					$zip = false;

					if (is_dir($ruta_ftp_skidata))
					{
						$ruta_tornos = str_replace("\\", "/", $ruta_ftp_skidata);

						// Borramos todos los códigos generados hasta ahora
						if (is_dir($ruta_tornos))
						{
							$objects = scandir($ruta_tornos);
							foreach ($objects as $object)
							{
								if ($object != "." && $object != "..")
								{
									if (filetype($ruta_tornos . "/" . $object) != "dir")
										unlink($ruta_tornos . "/" . $object);

								}
							}
						}

						// Preparamos el directorio para el ZIP
						if (!is_dir($directorio_zip))
						{
							// No existe el directorio.
							$dir = explode("/", $directorio_zip);

							$ruta = "";
							for ($cont = 0; $cont < count($dir); $cont++)
							{
								$ruta .= $dir[$cont] . "/";
								if (!is_dir($ruta))
								{
									mkdir($ruta);
									chmod($ruta, 0775);
								}
							}
						}

						$zip = new ZipArchive();

						// comprobamos que el ZIP no esté bloqueado
						$fichero_lock = $directorio_zip . "/.lock_" . str_replace($directorio_zip . "/", "", $archivo_zip);

						$bloqueado = false;
						$forzado = false;
						$cont = 0;
						$flock = fopen($fichero_lock, "w");
						while (!flock($flock, LOCK_EX|LOCK_NB))
						{
							// Ya esta bloqueado!
							$bloqueado = true;
							// esperamos 1/4 de segundo
							usleep(250000);
							$cont++;
							if ($cont >= 20)
							{
								// habrán pasado 5 segundos (cada 1/4 seg, cont suma 1)
								$forzado = true;
								break;
							}
						}

						if (!$forzado)
						{
							// Abrimos el ZIP
							$zip->open($archivo_zip, ZIPARCHIVE::OVERWRITE);
						}

						flock($flock, LOCK_UN);
						fclose($flock);
						@unlink($fichero_lock);
					}

					$codigos = array();

					// Obtenemos la fecha del evento
					$query = "SELECT Ev_fecha_evento FROM D_eventos WHERE Ev_id = '" . db_real_escape_string($id_evento) . "'";
					$doquery = query($query, __FILE__, __LINE__);
					if ($row = fetch($doquery))
					{
						$fecha_evento = $row['Ev_fecha_evento'];
					}
					else
					{
						$fecha_evento = time();	// en el remoto caso en que el evento no tuviera fecha, cogemos la fecha de hoy
					}

					$id_prefijo = date("Ymd", $fecha_evento);
					$fecha_validez_ini = date("Y-m-d", $fecha_evento) . "T00:00:01";
					$fecha_validez_fin = date("Y-m-d", $fecha_evento) . "T23:59:59";

					// Obtenemos todos los códigos válidos y escribimos los ficheros de control
					$query = "SELECT EN_id_intern, barcode, Ev_id, tipo_codigo FROM EN_control_ev_detalle WHERE Ev_id = '" . db_real_escape_string($id_evento) . "'";
					$doquery = query($query, __FILE__, __LINE__);
					while ($row = fetch($doquery))
					{
						if ($row['barcode'] != "")
						{
							if (!isset($codigos[$row['barcode']]))
							{
								genera_permiso($carpeta_ftp, $ruta_ftp_skidata, $row['tipo_codigo'], $row['barcode'], $id_prefijo, $fecha_validez_ini, $fecha_validez_fin, $zip);
								$codigos[$row['barcode']] = true;
							}
						}
					}

					// Cerramos y guardamos el ZIP
					if ($zip)
					{
						$zip->close();
					}
					@chmod($archivo_zip, 0664);
				}
				elseif ($mensaje->accion == 'LOCALIDAD' && $mensaje->id_evento && $mensaje->barcode)
				{
					// si la acción es "loc" (nuevo localidad añadida o anulada),
					// tenemos que generar el código de la localidad correspondiente
					$id_evento = $mensaje->id_evento;
					$barcode = $mensaje->barcode;
					$tipo_codigo = $mensaje->tipo_codigo;
					$anulacion = $mensaje->anulacion;

					$zip = false;

					if (is_dir($ruta_ftp_skidata))
					{
						$ruta_tornos = str_replace("\\", "/", $ruta_ftp_skidata);

						if (!is_dir($directorio_zip))
						{
							// No existe el directorio.
							$dir = explode("/", $directorio_zip);

							$ruta = "";
							for ($cont = 0; $cont < count($dir); $cont++)
							{
								$ruta .= $dir[$cont] . "/";
								if (!is_dir($ruta))
								{
									mkdir($ruta);
									chmod($ruta, 0775);
								}
							}
						}

						$zip = new ZipArchive();

						// comprobamos que el ZIP no esté bloqueado
						$fichero_lock = $directorio_zip . "/.lock_" . str_replace($directorio_zip . "/", "", $archivo_zip);

						$bloqueado = false;
						$forzado = false;
						$cont = 0;
						$flock = fopen($fichero_lock, "w");
						while (!flock($flock, LOCK_EX|LOCK_NB))
						{
							// Ya esta bloqueado!
							$bloqueado = true;
							// esperamos 1/4 de segundo
							usleep(250000);
							$cont++;
							if ($cont >= 20)
							{
								// habrán pasado 5 segundos (cada 1/4 seg, cont suma 1)
								$forzado = true;
								break;
							}
						}

						if (!$forzado)
						{
							// Abrimos el ZIP
							$zip->open($archivo_zip, ZIPARCHIVE::OVERWRITE);
						}

						flock($flock, LOCK_UN);
						fclose($flock);
						@unlink($fichero_lock);

						// Abrimos el ZIP
						$zip->open($archivo_zip, ZIPARCHIVE::CREATE);	// Lo crea si no existe. Si existe, lo abre para añadir ficheros
					}

					// Obtenemos la fecha del evento
					$query = "SELECT Ev_fecha_evento FROM D_eventos WHERE Ev_id = '" . db_real_escape_string($id_evento) . "'";
					$doquery = query($query, __FILE__, __LINE__);
					if ($row = fetch($doquery))
					{
						$fecha_evento = $row['Ev_fecha_evento'];
					}
					else
					{
						$fecha_evento = time();	// en el remoto caso en que el evento no tuviera fecha, cogemos la fecha de hoy
					}

					$id_prefijo = date("Ymd", $fecha_evento);
					$fecha_validez_ini = date("Y-m-d", $fecha_evento) . "T00:00:01";
					$fecha_validez_fin = date("Y-m-d", $fecha_evento) . "T23:59:59";

					if ($barcode != "")
					{
						genera_permiso($carpeta_ftp, $ruta_ftp_skidata, $tipo_codigo, $barcode, $id_prefijo, $fecha_validez_ini, $fecha_validez_fin, $zip, $anulacion);
					}

					// Cerramos y guardamos el ZIP
					if ($zip)
					{
						$zip->close();
					}
					@chmod($archivo_zip, 0664);

				}

				// Cerramos la conexión con la base de datos
				db_close($con);
			}
		}

		cola::desencola(NOMBRE_COLA, $header['ReceiptHandle']);
	}

	// Comparamos el timestamp de última modificación del worker con cuando se cargó, por si hay que matarlo para que se reinicie.
	clearstatcache(true, __FILE__);
	if ($ultima_modificacion != @filemtime(__FILE__))
	{
		die;
	}

}


/**
 * Función que genera los archivos de permisos y los coloca en la carpeta correspondiente. Además mantiene actualizado el archivo ZIP que también los contiene
 * @param string $tipo_xml
 * @param string $ruta_ftp_skidata
 * @param string $tipo_codigo
 * @param string $barcode
 * @param string $id_prefijo
 * @param string $fecha_validez_ini
 * @param string $fecha_validez_fin
 * @param object $zip
 * @param bool $anulacion
 */
function genera_permiso($tipo_xml, $ruta_ftp_skidata, $tipo_codigo, $barcode, $id_prefijo, $fecha_validez_ini, $fecha_validez_fin, &$zip, $anulacion = false)
{
	$id_sufijo = "";
	$xml = false;
	$funcion_obtener_xml = "obtener_xml_" . strtolower($tipo_xml);

	// Localizamos y llamamos a la función que obtiene el XML concreto según el cliente
	if (function_exists($funcion_obtener_xml))
	{
		$xml = $funcion_obtener_xml($tipo_codigo, $barcode, $id_prefijo, $fecha_validez_ini, $fecha_validez_fin, $id_sufijo, $anulacion);
	}
	
	if (is_dir($ruta_ftp_skidata) && $xml)
	{
		$filename_original = $id_prefijo . $barcode . $id_sufijo . '.imp';
		
		$nombre = "tmp_" . rand(100000000, 999999999);
		
		$fp = fopen($ruta_ftp_skidata . "/" . $nombre . ".tmp", "a");
		
		fwrite($fp, $xml);
		
		fclose($fp);
		
		$archivo = str_replace("\\", "/", $ruta_ftp_skidata . "/" . $filename_original);
		$directorio = dirname($archivo);
		
		if (!is_dir($directorio))
		{
			// No existe el directorio.
			$dir = explode("/", $directorio);

			$ruta = "";
			for ($cont = 0; $cont < count($dir); $cont++)
			{
				$ruta .= $dir[$cont] . "/";
				if (!is_dir($ruta))
				{
					mkdir($ruta);
					chmod($ruta, 0775);
				}
			}
		}
		
		if (is_file($archivo))
		{
			unlink($archivo);
		}
		rename($ruta_ftp_skidata . "/" . $nombre . ".tmp", $archivo);
		chmod($archivo, 0664);
		
		//Añadimos el archivo al Zip
		if ($zip)
		{
			$zip->addFile($archivo, str_replace($ruta_ftp_skidata . "/", $id_prefijo . "/", $archivo));
		}
	}
}


/**
 * Función que obtiene el XML de los permisos de control de acceso Skidata para Bilbao Basket
 * @param string $tipo_codigo
 * @param string $barcode
 * @param string $id_prefijo
 * @param string $fecha_validez_ini
 * @param string $fecha_validez_fin
 * @param string $id_sufijo
 * @param bool $anulacion
 * @return string
 */
function obtener_xml_bilbao($tipo_codigo, $barcode, $id_prefijo, $fecha_validez_ini, $fecha_validez_fin, &$id_sufijo, $anulacion = false)
{
	$xml = "";

	if ($anulacion)
	{
		$id_sufijo = "_A";
		$action = "D";
	}
	else
	{
		// Nos aseguramos de que el código tiene fijado correctamente su tipo (ENTRADA o ABONO) y si no, lo saltamos
		if (($tipo_codigo != 'ENTRADA') && ($tipo_codigo != 'ABONO'))
			return $xml;

		$id_sufijo = "";
		$action = "U";
	}

	$xml  = '';
	$xml .= '<TSData>' . "\n";
	$xml .= '<Header>' . "\n";
	$xml .= '<Version>HSHIF25</Version>' . "\n";
	$xml .= '<Issuer>5</Issuer>' . "\n";
	$xml .= '<Receiver>1</Receiver>' . "\n";
	$xml .= '<ID>' . $id_prefijo . $barcode . $id_sufijo . '</ID>' . "\n";
	$xml .= '</Header>' . "\n";
	$xml .= '<WhitelistRecord Expire="' . $fecha_validez_fin . '" >' . "\n";
	$xml .= '<Action>' . $action . '</Action>' . "\n";
	$xml .= '<UTID>' . $barcode . '</UTID>' . "\n";
	$xml .= '<Coding>260</Coding>' . "\n";
	$xml .= '<Permission>' . "\n";
	$xml .= '<UPID>' . $id_prefijo . $barcode . '</UPID>' . "\n";
	if (!$anulacion)
	{
		$xml .= '<TSProperty Type="VALIDITY" From="' . $fecha_validez_ini . '" To="' . $fecha_validez_fin . '"/>' . "\n";
		$xml .= '<TSProperty Type="INTER_TYPE">' . "\n";
		if ($tipo_codigo == 'ENTRADA')
		{
			$xml .= '<ID>1</ID>' . "\n";
		}
		elseif ($tipo_codigo == 'ABONO')
		{
			$xml .= '<ID>2</ID>' . "\n";
		}
		$xml .= '</TSProperty>' . "\n";
		$xml .= '<TSProperty Type="PUNTOS">' . "\n";
		$xml .= '<ID>1</ID>' . "\n";
		$xml .= '</TSProperty>' . "\n";
		$xml .= '<TSProperty Type="DAYS">' . "\n";
		$xml .= '<ID>1</ID>' . "\n";
		$xml .= '</TSProperty>' . "\n";
		$xml .= '<TSProperty Type="TIMETICKETPERIOD">' . "\n";
		$xml .= '<ID>1</ID>' . "\n";
		$xml .= '</TSProperty>' . "\n";
	}
	$xml .= '</Permission>' . "\n";
	$xml .= '</WhitelistRecord>' . "\n";
	$xml .= '</TSData>';

	return $xml;
}


/**
 * Función que obtiene el XML de los permisos de control de acceso Skidata para CB Gran Canaria
 * @param string $tipo_codigo
 * @param string $barcode
 * @param string $id_prefijo
 * @param string $fecha_validez_ini
 * @param string $fecha_validez_fin
 * @param string $id_sufijo
 * @param bool $anulacion
 * @return string
 */
function obtener_xml_cbgrancanaria($tipo_codigo, $barcode, $id_prefijo, $fecha_validez_ini, $fecha_validez_fin, &$id_sufijo, $anulacion = false)
{
	$xml = "";

	if ($anulacion)
	{
		$id_sufijo = "_A";
		$action = "D";
	}
	else
	{
		// Nos aseguramos de que el código tiene fijado correctamente su tipo (ENTRADA o ABONO) y si no, lo saltamos
		if (($tipo_codigo != 'ENTRADA') && ($tipo_codigo != 'ABONO'))
			return $xml;

		$id_sufijo = "";
		$action = "U";
	}

	$xml  = '';
	$xml .= '<TSData>' . "\n";
	$xml .= '<Header>' . "\n";
	$xml .= '<Version>HSHIF25</Version>' . "\n";
	$xml .= '<Issuer>5</Issuer>' . "\n";
	$xml .= '<Receiver>1</Receiver>' . "\n";
	$xml .= '<ID>' . $id_prefijo . $barcode . $id_sufijo . '</ID>' . "\n";
	$xml .= '</Header>' . "\n";
	$xml .= '<WhitelistRecord Expire="' . $fecha_validez_fin . '" >' . "\n";
	$xml .= '<Action>' . $action . '</Action>' . "\n";
	$xml .= '<UTID>' . $barcode . '</UTID>' . "\n";
	$xml .= '<Coding>260</Coding>' . "\n";
	$xml .= '<Permission>' . "\n";
	$xml .= '<UPID>' . $id_prefijo . $barcode . '</UPID>' . "\n";
	if (!$anulacion)
	{
		$xml .= '<TSProperty Type="VALIDITY" From="' . $fecha_validez_ini . '" To="' . $fecha_validez_fin . '"/>' . "\n";
		$xml .= '<TSProperty Type="INTER_TYPE">' . "\n";
		if ($tipo_codigo == 'ENTRADA')
		{
			$xml .= '<ID>1</ID>' . "\n";
		}
		elseif ($tipo_codigo == 'ABONO')
		{
			$xml .= '<ID>2</ID>' . "\n";
		}
		$xml .= '</TSProperty>' . "\n";
		$xml .= '<TSProperty Type="PUNTOS">' . "\n";
		$xml .= '<ID>1</ID>' . "\n";
		$xml .= '</TSProperty>' . "\n";
		$xml .= '<TSProperty Type="DAYS">' . "\n";
		$xml .= '<ID>1</ID>' . "\n";
		$xml .= '</TSProperty>' . "\n";
		$xml .= '<TSProperty Type="TIMETICKETPERIOD">' . "\n";
		$xml .= '<ID>1</ID>' . "\n";
		$xml .= '</TSProperty>' . "\n";
	}
	$xml .= '</Permission>' . "\n";
	$xml .= '</WhitelistRecord>' . "\n";
	$xml .= '</TSData>';

	return $xml;
}