<?php

require_once 'AWSSDKforPHP/aws.phar';
if (isset($_ENV['desarrollo'])
	&& (strcasecmp($_ENV['desarrollo'], "true") == 0))
{
	include_once("include/DEV/dev.php");
}
use Aws\Sqs\SqsClient;
use Aws\Common\Enum\Region;

/**
 * Description of cola
 *
 * @author Christian
 */
class cola {
	
	static $usuarios = Array(
		"correos" => Array(
			'url' => "https://sqs.eu-west-1.amazonaws.com/144734154142/correos"
		),
		"thumbs" => Array(
			"url" => "https://sqs.eu-west-1.amazonaws.com/144734154142/thumbs"
		),
		"control_skidata" => Array(
			"url" => "https://sqs.eu-west-1.amazonaws.com/144734154142/control_skidata"
		)
	);

	static function conectarCola($cola)
	{
		if (!isset($GLOBALS['sqsClient']['actual'])
			|| $GLOBALS['sqsClient']['actual'] != $cola)
		{
			$GLOBALS['sqsClient']['actual'] = $cola;
			
			if (isset($GLOBALS['sqsClient'][$cola]))
			{
				$GLOBALS['sqs'] = $GLOBALS['sqsClient'][$cola];
			}
			else
			{
				if (isset($_ENV['desarrollo'])
					&& (strcasecmp($_ENV['desarrollo'], "true") == 0))
				{
					$GLOBALS['sqsClient'][$GLOBALS['sqsClient']['actual']] = SqsClient::factory(array(
						'key'    => $GLOBALS['dev']['aws']['sqs'][$GLOBALS['sqsClient']['actual']]['key'],
						'secret' => $GLOBALS['dev']['aws']['sqs'][$GLOBALS['sqsClient']['actual']]['secret'],
						'region' => Region::EU_WEST_1
					));
				}
				else
				{
					$GLOBALS['sqsClient'][$GLOBALS['sqsClient']['actual']] = SqsClient::factory(array(
						'region' => Region::EU_WEST_1
					));
				}

				if (isset($GLOBALS['sqsClient'][$cola]))
				{
					$GLOBALS['sqs'] = $GLOBALS['sqsClient'][$cola];
				}
				else
				{
					return false;
				}
			}
		}
	}
	
	/**
	 * Vamos a meter un mensaje en una cola
	 * 
	 * @param $cola: En que cola vamos a poner el mensaje
	 * @param $body: Mensaje que hay que encodar.
	 * @return id mensaje
	 */
	static function encola($cola, $body)
	{
		self::conectarCola($cola);
		
		if (!$GLOBALS['sqs'])
		{
			return false;
		}
		
		$mensaje = Array(
				"QueueUrl" => self::$usuarios[$cola]['url'],
				"MessageBody" => json_encode($body)
		);
		
		$result = $GLOBALS['sqs']->sendMessage($mensaje);

		return $result->get("MessageId");
	}
	
	/**
	 * Recibimos un mensaje de una cola especificada
	 * 
	 * @param $cola: Cola que queremos mirar.
	 * @return $body: Mensaje de la cola. False si está vacia.
	 */
	static function recibir($cola)
	{
		self::conectarCola($cola);

		if (!$GLOBALS['sqs'])
		{
			return Array(false, false);
		}
		
		$result = $GLOBALS['sqs']->receiveMessage(array(
			'QueueUrl' => self::$usuarios[$cola]['url'],
			'WaitTimeSeconds' => 20
		));
		
		if ($result->get("Messages"))
		{
			foreach ($result->get("Messages") as $message)
			{
				$resultado = Array();

				$header = Array();
				$body = "";
				foreach ($message as $key => $value)
				{
					if ($key != "Body")
					{
						$header[$key] =& $message[$key];
					}
					else
					{
						$body =& $message['Body'];
					}
				}

				$resultado[] = $header;
				$resultado[] = json_decode($body);

				return $resultado;
			}
		}
		else
		{
			// No hay mensajes en cola
			return Array(false, false);
		}
	}
	
	/** 
	 * Elimina de la cola un mensaje
	 * 
	 * @param $cola: Cola que queremos mirar.
	 * @param $ReceiptHandle: Identificador del mensaje que devuelve amazon
	 * @return boolean
	 */
	static function desencola($cola, $ReceiptHandle)
	{
		self::conectarCola($cola);
		
		if (!$GLOBALS['sqs'])
		{
			return false;
		}
		
		$GLOBALS['sqs']->deleteMessage(Array(
				'QueueUrl' => self::$usuarios[$cola]['url'],
				'ReceiptHandle' => $ReceiptHandle
		));
		
		return true;
	}
}
