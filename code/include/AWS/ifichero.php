<?php
require_once 'AWSSDKforPHP/aws.phar';

use Aws\Common\Aws;
use Aws\Common\Enum\Region;
use Doctrine\Common\Cache\FilesystemCache;
use Guzzle\Cache\DoctrineCacheAdapter;

if (!is_dir(sys_get_temp_dir() . '/koobin/cache'))
{
	mkdir(sys_get_temp_dir() . '/koobin/cache');
}

$cacheAdapter = new DoctrineCacheAdapter(new FilesystemCache(sys_get_temp_dir() . '/koobin/cache'));

if (isset($_ENV['desarrollo'])
	&& (strcasecmp($_ENV['desarrollo'], "true") == 0))
{
	include_once("include/DEV/dev.php");
	$GLOBALS['aws'] = Aws::factory(Array(
		'key'	=> $GLOBALS['dev']['aws']['s3']['key'],
		'secret'	=> $GLOBALS['dev']['aws']['s3']['secret'],
		'region' => Region::EU_WEST_1,
		'ssl.certificate_authority' => ini_get('curl.cainfo'),
		'credentials.cache' => $cacheAdapter
	));
}
else
{
	$GLOBALS['aws'] = Aws::factory(Array(
		'region' => Region::EU_WEST_1,
		'credentials.cache' => $cacheAdapter
	));
}

if (substr($GLOBALS['workConstantes']->MYSQL_DATABASE, -4) == "_dev"
	|| substr($GLOBALS['workConstantes']->MYSQL_DATABASE, -4) == "_pre"
	|| $GLOBALS['workConstantes']->MYSQL_DATABASE == "ticketing_koobin")
{
	define("BUCKET", "koobin-staticpre");
}
else
{
	define("BUCKET", "koobin-static");
}

/**
 * Description of ficheros
 *
 * @author Christian
 */
class ifichero extends abs_ifichero
{
	const bucket = BUCKET;
	
	static function getUrl($fichero)
	{
		if ($fichero != "")
		{
			if (substr($fichero, 0, 4) != "http")
			{
				if (substr($fichero, 0, 1) != "/")
				{
					$fichero = "/" . $fichero;
				}

				$fichero = rawurlencode($fichero);

				return "https://" . self::bucket .".s3.amazonaws.com" . str_replace("%2F", "/", $fichero);
			}
			else
			{
				return $fichero;
			}
		}
		else
		{
			return "";
		}
	}
	
	static function fileExists($fichero, $usa_cache = true, $solo_cache = false)
	{
		if ($usa_cache === true
			|| $solo_cache === true)
		{
			$cache = $GLOBALS['memcache']->get("cache_" . self::pathLimpio($fichero));
		}
		else
		{
			$cache = false;
		}
		if ($cache !== false)
		{
			if ($cache == "Y")
			{
				return true;
			}
			else
			{
//				$fp = fopen(sys_get_temp_dir() . "/cache.txt", "a");
//				fwrite($fp, "cache_" . self::pathLimpio($fichero) . "\n");
//				fclose($fp);
				return false;
			}
		}
		else
		{
			if ($solo_cache === false)
			{
				$s3 = $GLOBALS['aws']->get('s3');

				$tmp_prefix = self::pathLimpio($fichero);
				if (substr($tmp_prefix, 0, 1) == "/") $tmp_prefix = substr($tmp_prefix, 1);

				$tmp_prefix = str_replace("%20", " ", $tmp_prefix);

				$iterator = $s3->getIterator('ListObjects', array(
					'Bucket' => self::bucket,
					'Prefix' => $tmp_prefix
				));

				$iterator->set('names_only', true);

				$existe = false;
				foreach ($iterator as $object)
				{
					$existe = true;
				}

				if ($existe)
				{
					$valor = "Y";
				}
				else
				{
					$valor = "N";
				}
			}
			else
			{
				$valor = "N";
				$existe = false;
			}

			if ($GLOBALS['memcache']->get("cache_" . self::pathLimpio($fichero)) !== false)
			{
				$GLOBALS['memcache']->replace("cache_" . self::pathLimpio($fichero), $valor);
			}
			else
			{
				$GLOBALS['memcache']->add("cache_" . self::pathLimpio($fichero), $valor);
			}
//			if ($valor == "Y")
//			{
//				echo "Marcamos como cacheada cache_" . self::pathLimpio($fichero) . "\n";
//			}
//			else
//			{
//				echo "Marcamos como DESCACHEADA cache_" . self::pathLimpio($fichero) . "\n";
//			}

			return $existe;
		}
	}
	
	static function dirExists($carpeta)
	{
		if (substr($carpeta, -1) != "/")
		{
			$carpeta .= "/";
		}
		
		return self::fileExists($carpeta);
	}
	
	static function crearCarpeta($ruta, $carpeta)
	{
		$s3 = $GLOBALS['aws']->get('s3');

		if (substr($ruta, -1) != "/") $ruta .= "/";
		if (substr($carpeta, -1) != "/") $carpeta .= "/";
		
		$s3->createObject(array(
			'Bucket' => self::bucket,
			'Key'    =>	$ruta . RemoveAccents($carpeta),
			'Body'   => "")
		);
	}
	
	static function eliminarArchivo($ruta, $fichero)
	{
		$s3 = $GLOBALS['aws']->get('s3');

		if (substr($ruta, -1) != "/") $ruta .= "/";
		
		if (substr($fichero, 0, 1) == "/") $fichero = substr($fichero, 1);
		
		$s3->deleteObject(array(
			'Bucket' => self::bucket,
			'Key'    => $ruta . RemoveAccents($fichero)
		));
		
		if ($GLOBALS['memcache']->get("cache_" . $ruta . RemoveAccents($fichero)) !== false)
		{
			$GLOBALS['memcache']->replace("cache_" . $ruta . RemoveAccents($fichero), "N");
		}
	}
	
	static function eliminarCarpeta($ruta, $carpeta)
	{
		$s3 = $GLOBALS['aws']->get('s3');

		if (substr($carpeta, -1) != "/") $carpeta .= "/";
		
		$iterator = $s3->getIterator('ListObjects', array(
			'Bucket' => self::bucket,
			'Prefix' => $ruta . RemoveAccents($carpeta)
		));

		$iterator->set('names_only', true);
		
		foreach ($iterator as $object)
		{
			// En S3 los archivos y carpetas se borran igual
			self::eliminarArchivo($ruta . $carpeta, str_replace($ruta . $carpeta, "", $object));
		}
	}
	
	static function subirFichero($carpeta, $fichero, $nombre = false)
	{
		$s3 = $GLOBALS['aws']->get('s3');
		
		if (substr($carpeta, -1) != "/") $carpeta .= "/";
		
		if (!$nombre) $nombre = $fichero['name'];

		$nombre = RemoveAccents($nombre);
		$nombre = RemoveAccents(str_replace("&","", str_replace("$", "", str_replace(" ", "", str_replace("@", "", str_replace("+", "", str_replace("%", "", str_replace("'", "", str_replace("\"", "", str_replace("`", "", str_replace("´", "", str_replace("¨", "", str_replace("!", "", str_replace("?", "", str_replace("¿", "", str_replace(";", "", str_replace(":", "", str_replace("\\", "", str_replace(",", "", str_replace("|", "", str_replace("=", "", str_replace("[", "", str_replace("]", "", str_replace("{", "", str_replace("}", "", str_replace("*", "", str_replace("#", "", str_replace("¬", "", str_replace("^", "", str_replace("º", "", str_replace("ª", "", str_replace("<", "", str_replace(">", "", str_replace("~", "", $nombre))))))))))))))))))))))))))))))))));

		if (substr($nombre, 0, 1) == "/") $nombre = substr($nombre, 1);

		if (isset($fichero['contenido']))
		{
			$s3->putObject(array(
				'Bucket' => self::bucket,
				'Key'    => str_replace("%20", " ", $carpeta . $nombre),
				'Body'   => $fichero['contenido']
			));
		}
		else
		{
			clearstatcache();
			$s3->putObject(array(
				'Bucket' => self::bucket,
				'Key'    => str_replace("%20", " ", $carpeta . $nombre),
				'Body'   => fopen($fichero['tmp_name'], 'r+')
			));
		}

		$GLOBALS['memcache']->set("cache_" . self::pathLimpio($carpeta . $nombre), "Y");
		
		return $carpeta . $nombre;
	}
	
	protected function procesaRuta($ruta)
	{
		return $ruta;
	}
	
	public function listaFicheros($carpeta)
	{
		$s3 = $GLOBALS['aws']->get('s3');

		$carpeta = str_replace("//", "/", $carpeta);
		
		if (substr($carpeta, -1) != "/") $carpeta .= "/";
		
		$prefix = $carpeta;
		
//		$s3->createObject(array(
//			'Bucket' => $bucket,
//			'Key'    =>	$prefix,
//			'Body'   => "")
//		);
//		
//		$s3->createObject(array(
//			'Bucket' => $bucket,
//			'Key'    =>	$prefix . "fotos/",
//			'Body'   => "")
//		);
//		
//		$s3->createObject(array(
//			'Bucket' => $bucket,
//			'Key'    =>	$prefix . "panoramicas/",
//			'Body'   => "")
//		);
//		
//		$s3->createObject(array(
//			'Bucket' => $bucket,
//			'Key'    =>	$prefix . "plantillas/",
//			'Body'   => "")
//		);
//		
		$iterator = $s3->getIterator('ListObjects', array(
			'Bucket' => self::bucket,
			'Prefix' => $prefix,
			'Delimiter' => "/"
		));
		
		$iterator->set('names_only', true);
		$iterator->set('return_prefixes', true);
		
		$ficheros = Array();
		foreach ($iterator as $object)
		{
			if (str_replace($prefix, "", $object) != "")
			{
				$ficheros[] = str_replace($prefix, "", $object);
			}
//			$tmp_array = $this->devuelveContenido($object['Key']);
//			$ficheros = array_merge_recursive($ficheros, $tmp_array);
		}
		
		return $this->ordenaFicheros($ficheros);
	}
	
	static function getTMPUrl($fichero, $tipo, $public = false)
	{
		$s3 = $GLOBALS['aws']->get('s3');
		
		$s3->putObject(array(
			'Bucket' => self::bucket,
			'Key'     => $GLOBALS['workConstantes']->CARPETA_PRINT . $fichero,
			'Body'    => fopen($GLOBALS['workConstantes']->TMP_PATH . "/" . $fichero, 'r+'),
			'Expires' => "+15 minutes"
		));

		return self::getSignedUrl($GLOBALS['workConstantes']->CARPETA_PRINT . $fichero);
	}

	static function getSignedUrl($fichero, $tipo = false, $public = false, $attachment = false)
	{
		$s3 = $GLOBALS['aws']->get('s3');

		if ($attachment)
		{
			$disp = "?response-content-disposition=attachment; filename=\"" . basename($fichero) . "\"";
		}
		else
		{
			$disp = "?response-content-disposition=inline; filename=\"" . basename($fichero) . "\"";
		}

		$request = $s3->get(self::bucket . "/" . $fichero . $disp);

		$url = $s3->createPresignedUrl($request, '+15 minutes');

		return $url;
	}
	
	static function getPathCache($fichero)
	{
		return self::getUrl($GLOBALS['workConstantes']->CARPETA_CACHE . $fichero);
	}
	
	static function limpiaCache($fichero)
	{
		$fichero = self::getPathCache($fichero);
		if (self::fileExists($fichero))
		{
			self::eliminarArchivo("", self::pathLimpio($fichero));
			$GLOBALS['memcache']->delete("cache_" . self::pathLimpio($fichero));
		}
	}
	
	static function pathLimpio($fichero)
	{
		return str_replace("https://" . self::bucket .".s3.amazonaws.com/", "", $fichero);
	}

	static function renombrarArchivo($nombre_ori, $nuevo_nombre)
	{
		$s3 = $GLOBALS['aws']->get('s3');

		$s3->copyObject(
			array(
				'Bucket' => self::bucket,
				'Key' => $nuevo_nombre,
				'CopySource' => self::bucket . '/' . $nombre_ori
			)
		);

		self::eliminarArchivo("", $nombre_ori);
	}
}
