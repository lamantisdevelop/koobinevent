<?php

function carga_clases($clase)
{
	if (substr($clase, 0, 1) == "i")
	{
		$carpeta = mb_strtoupper(substr($clase, 1));
	}
	else
	{
		$carpeta = mb_strtoupper($clase);
	}
	
	if (is_file("include/ABSTRACT/" . $clase . ".php"))
	{
		include("include/ABSTRACT/" . $clase . ".php");
	}
	elseif (defined("USA_AMAZON") && USA_AMAZON == "true" && is_file("include/AWS/" . $clase . ".php"))
	{
		include("include/AWS/" . $clase . ".php");
	}
	elseif (is_file("include/" . $carpeta . "/" . $clase . ".php"))
	{
		include("include/" . $carpeta . "/" . $clase . ".php");
	}
	elseif (is_file("include/" . $clase . "/" . $clase . ".php"))
	{
		include_once("include/" . $clase . "/" . $clase . ".php");
	}
	elseif (is_file("include/" . mb_strtoupper($clase) . "/" . $clase . ".php"))
	{
		include_once("include/" . mb_strtoupper($clase) . "/" . $clase . ".php");
	}
	elseif (is_file("include/" . $clase . "s/" . $clase . ".php"))
	{
		include_once("include/" . $clase . "s/" . $clase . ".php");
	}
	elseif (is_file("include/" . mb_strtoupper($clase . "s") . "/" . $clase . ".php"))
	{
		include_once("include/" . mb_strtoupper($clase . "s") . "/" . $clase . ".php");
	}
	elseif (is_file("include/" . $clase . "es/" . $clase . ".php"))
	{
		include_once("include/" . $clase . "es/" . $clase . ".php");
	}
	elseif (is_file("include/" . mb_strtoupper($clase . "es") . "/" . $clase . ".php"))
	{
		include_once("include/" . mb_strtoupper($clase . "es") . "/" . $clase . ".php");
	}
	elseif ($clase == "formapago")
	{
		include_once("include/FORMASPAGO/" . $clase . ".php");
	}
	elseif ($clase == "categoriasextras")
	{
		include_once("include/EXTRAS/" . $clase . ".php");
	}
	elseif ($clase == "categoriaextras")
	{
		include_once("include/EXTRAS/" . $clase . ".php");
	}
}

function genera_imagen($nombre_foto, $extension, $max, $base)
{
	if ($array_img = @getimagesize($nombre_foto))
	{
		$tipo = $array_img['mime'];

		$path_limpio = ifichero::pathLimpio($nombre_foto);
		
		$nombre_fichero = substr(str_replace("/", "_", str_replace(":", "_", $path_limpio)), 0, strpos($path_limpio, $extension)) . "_" . $base . "_" . $max . $extension;
		
		$tmp_path = TMP_PATH . "/" . $GLOBALS['workConstantes']->MYSQL_DATABASE . "/" . $nombre_fichero;
		$fichero_lock = TMP_PATH . "/" . $GLOBALS['workConstantes']->MYSQL_DATABASE . "/.lock_" . $nombre_fichero;

		$flock = fopen($fichero_lock, "w");
		$bloqueado = false;
		$forzado = false;
		$cont = 0;
		while (!flock($flock, LOCK_EX|LOCK_NB))
		{
			// Ya esta bloqueado!
			$bloqueado = true;
			sleep(1);
			$cont++;
			if ($cont >= 5)
			{
				$forzado = true;
				break;
			}
		}

		if ($bloqueado)
		{
			// Hemos salido porque el fichero estaba bloqueado
			if ($forzado)
			{
				// Ademas ha devuelto timeout! Devolvemos la ruta de una imagen 1x1 en blanco
				return $tmp_path;
			}
			else
			{
				// Ha finalizado el bloqueo.
				fclose($flock);
				unlink($fichero_lock);
			}
		}

		// Primero creamos una imagen en blanco para que no se genere varias veces la misma imagen
		if ($tipo == "image/jpeg")
		{
			$im = ImageCreateFromJPEG($nombre_foto);
		}
		else if ($tipo == "image/gif")
		{
			$im = ImageCreateFromGif($nombre_foto);
		}
		else if ($tipo == "image/png")
		{
			$im = ImageCreateFromPng($nombre_foto);
		}

		if ($im) // Miramos si la foto se ha cargado
		{
			$alto = ImageSY($im);
			$ancho = ImageSX($im);
			if($alto > $ancho) $proporcion = (100 / $base);
			else $proporcion = (100 / $base);
			if ($max != "max")
			{
				if (($ancho / $proporcion) > $max) $proporcion = $ancho / $max;
				if (($alto / $proporcion) > $max) $proporcion = $alto / $max;
			}
			if ($proporcion != 1)
			{
				if ($tipo == "image/jpeg")
				{
					$im2 = ImageCreateTrueColor($ancho / $proporcion, $alto / $proporcion);
					ImageCopyResampled($im2, $im, 0, 0, 0, 0, $ancho / $proporcion, $alto / $proporcion,  $ancho, $alto);
					ImageJpeg($im2, $tmp_path, 100); // Guardamos la imagen en el directorio caché
				}
				else if ($tipo == "image/gif")
				{
					$im2 = ImageCreateTrueColor($ancho / $proporcion, $alto / $proporcion);
					ImageCopyResampled($im2, $im, 0, 0, 0, 0, $ancho / $proporcion, $alto / $proporcion,  $ancho, $alto);
					ImageGif($im2, $tmp_path); // Guardamos la imagen en el directorio caché
				}
				else if ($tipo == "image/png")
				{
					$im2 = ImageCreate($ancho / $proporcion, $alto / $proporcion);
					ImageCopyResampled($im2, $im, 0, 0, 0, 0, $ancho / $proporcion, $alto / $proporcion,  $ancho, $alto);
					imagealphablending($im2, false);
					imagesavealpha($im2, true);
					ImagePng($im2, $tmp_path, 0); // Guardamos la imagen en el directorio caché
				}
				ImageDestroy($im2);
			}
			else
			{
				if ($tipo == "image/jpeg")
				{
					ImageJpeg($im, $tmp_path); // Guardamos la imagen en el directorio caché
				}
				else if ($tipo == "image/gif")
				{
					ImageGif($im, $tmp_path); // Guardamos la imagen en el directorio caché
				}
				else if ($tipo == "image/png")
				{
					imagealphablending($im, false);
					imagesavealpha($im, true);
					ImagePng($im, $tmp_path); // Guardamos la imagen en el directorio caché
				}
				ImageDestroy($im);
			}
			
			fclose($flock);
			unlink($fichero_lock);
			
			return $tmp_path;

//					return str_replace(TMP_PATH . "/", "", $tmp_path);
		}
	}
	else
	{
		return false;
	}
}

function debug($v, $l = "", $retorna = false, $die = false)
{
	if (is_debug())
	{
		if ($retorna)
		{
			$resultado = "";
		}
		if (is_object($v))
		{
			if (!$retorna)
			{
				echo "<pre>";
				var_dump($v);
				echo "</pre>";
			} else
			{
				ob_start();
				var_dump($v);
				$resultado .= ob_get_contents();
				ob_clean();
				ob_end_flush();
			}
		} else if (is_array($v))
		{
			if (!$retorna)
			{
				echo "<pre>";
				print_r($v);
				echo "</pre>";
			} else
			{
				$resultado .= print_r($v, true);
			}
		} else if (is_bool($v))
		{
			if ($v)
				$estat = 'True';
			else
				$estat = 'False';

			if (!$retorna)
			{
				echo "<br><span>### L: $l [" . $estat . "]</span><br>";
			} else
			{
				$resultado .= "### L: $l [" . $estat . "]\n";
			}
		} else
		{
			if (!$retorna)
			{
				echo "<br><span>### L: $l [" . $v . "]</span><br>";
			} else
			{
				$resultado .= "### L: $l [" . $v . "]\n";
			}
		}
		
		if (!$retorna && $die)
		{
			die();
		}
	}

	if ($retorna)
	{
		return $resultado;
	}
}

function debug_and_die($v, $l = "")
{
	debug($v, $l, false, true);
}

function is_debug()
{
	$ips_debug = Array();
	$ips_debug[] = "84.89.61.129";
	
	if ((desarrollo() || in_array($_SERVER['REMOTE_ADDR'], $ips_debug)) || 
			(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && in_array($_SERVER['HTTP_X_FORWARDED_FOR'], $ips_debug)))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/** Esta función comprueba si una IP está dentro de un rango dado en notación CIDR
 *
 * @param type $ip
 * @param type $cidr
 * @return boolean
 */
function cidr_match($ip, $cidr)
{
	list($subnet, $mask) = explode('/', $cidr);

	if ((ip2long($ip) & ~((1 << (32 - $mask)) - 1) ) == ip2long($subnet))
	{
		return true;
	}

	return false;
}


function desarrollo()
{
	if ((defined("__DESARROLLO") && __DESARROLLO == true)
		|| isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == "127.0.0.1" || $_SERVER['SERVER_ADDR'] == '::1' || cidr_match($_SERVER['SERVER_ADDR'], "192.168.30.0/24")))
	{
		return true;
	} else
	{
		return false;
	}
}

function dev()
{
	if (substr($GLOBALS['workConstantes']->MYSQL_DATABASE, -4) == "_dev")
	{
		return true;
	}
	else
	{
		return false;
	}
}

function pre()
{
	if (substr($GLOBALS['workConstantes']->MYSQL_DATABASE, -4) == "_pre")
	{
		return true;
	}
	else
	{
		return false;
	}
}

function total_consultas()
{
	return $GLOBALS['total_consultas'];
}

// String EnCrypt + DeCrypt function
// Author: halojoy, July 2006
function convert($str, $key = 'tnc+uvyr')
{
	if ($key == '')
		return $str;

	$key = str_replace(chr(32), '', $key);

	if (strlen($key) < 8)
		exit('key error');

	$key_length = strlen($key) < 32 ? strlen($key) : 32;

	$k = array();
	for ($i = 0; $i < $key_length; $i++)
	{
		$k[$i] = ord($key{$i}) & 0x1F;
	}

	$j = 0;
	for ($i = 0; $i < strlen($str); $i++)
	{
		$e = ord($str{$i});
		$str{$i} = $e & 0xE0 ? chr($e ^ $k[$j]) : chr($e);
		$j++;
		$j = $j == $key_length ? 0 : $j;
	}

	return $str;

}

function RemoveAccents($string)
{
	$from = Array(
		"Š", "Œ", "Ž", "š", "œ", "ž", "Ÿ", "¥", "µ", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ",
		"Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö",
		"Ø", "Ù", "Ú", "Û", "Ü", "Ý", "ß", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è",
		"é", "ê", "ë", "ì", "í", "î", "ï", "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù",
		"ú", "û", "ü", "ý", "ÿ");
	$to = Array(
		"S", "O", "Z", "s", "o", "z", "Y", "Y", "u", "A", "A", "A", "A", "A", "A", "A",
		"C", "E", "E", "E", "E", "I", "I", "I", "I", "D", "N", "O",	"O", "O", "O", "O",
		"O", "U", "U", "U", "U", "Y", "s", "a", "a", "a", "a", "a", "a", "a", "c", "e",
		"e", "e", "e", "i", "i", "i", "i", "o", "n", "o", "o", "o", "o", "o", "o", "u",
		"u", "u", "u", "y", "y");

	return str_replace($from, $to, $string);
}

function start_output()
{
	// buffer all upcoming output
	if (!ob_start("ob_gzhandler")) {
		define('NO_GZ_BUFFER', true);
		ob_start();
	}
}

function end_output()
{
	//Flush here before getting content length if ob_gzhandler was used.
	if (!defined('NO_GZ_BUFFER')) {
		ob_end_flush();
	}

	// get the size of the output
	$size = ob_get_length();

	// send headers to tell the browser to close the connection
	header("Content-Length: $size");
	header('Connection: close');

	// flush all output
	ob_end_flush();
	ob_flush();
	flush();
}

/*
 * @param $tabla Tabla
 * @param $campo
 * @param $id
 * @param $id_idioma
 * @param $ahora
 * @return La descripción que hemos pedido
 *
 */

function get_descripcion($tabla, $campo = false, $id = false, $id_idioma = false, $ahora = false, $like = false)
{
	$tabla = strtolower($tabla);
	$campo = strtolower($campo);
	$id = strtolower($id);
	if (!$id_idioma || $id_idioma == "")
		$id_idioma = session::get('idioma');

	if (($campo !== false) && ($id !== false))
	{
		$indice = $tabla . "-" . $campo . "-" . $id;
	}
	elseif ($campo !== false)
	{
		$indice = $tabla . "-" . $campo;
	}
	else
	{
		$indice = $tabla;
	}

	$cadena = $GLOBALS['memcache']->get("multiidioma[" . $indice . "][" . $id_idioma . "]");

	if ($cadena === false)
	{
		if ($like)
		{
			$query = "SELECT id_cadena, descripcion FROM multiidioma " .
				"WHERE id_cadena LIKE '" . db_real_escape_string($indice) . "-%' " .
				"AND id_idioma = '" . db_real_escape_string($id_idioma) . "'";
			$result = query($query, __FILE__, __LINE__);

			$array_traducciones = Array();

			if (db_num_rows($result))
			{
				while ($row = fetch($result))
				{
					$GLOBALS['memcache']->add("multiidioma[" . strtolower($row['id_cadena']) . "][" . $id_idioma . "]", $row['descripcion']);
					$array_traducciones[strtolower($row['id_cadena'])][$id_idioma] = $row['descripcion'];
				}
			}

			return $array_traducciones;
		}
		else
		{
			$query = "SELECT id_cadena, descripcion FROM multiidioma " .
				"WHERE id_cadena = '" . db_real_escape_string($indice) . "' " .
				"AND id_idioma = '" . db_real_escape_string($id_idioma) . "'";
			$result = query($query, __FILE__, __LINE__);
			if (db_num_rows($result))
			{
				while ($row = fetch($result))
				{
					$GLOBALS['memcache']->add("multiidioma[" . strtolower($row['id_cadena']) . "][" . $id_idioma . "]", $row['descripcion']);
					$cadena = $row['descripcion'];
				}
			}
			else
			{
				$GLOBALS['memcache']->add("multiidioma[" . $indice . "][" . $id_idioma . "]", "");
				$cadena = "";
			}
		}

		return $cadena;
	}
	else
	{
		return $cadena;
	}
}

function inicializa_smarty($idioma, $carpeta = false)
{
	$smarty = new Smarty();
	$smarty->idioma = $idioma;
	$smarty->compile_dir = $GLOBALS['workConstantes']->TMP_PATH . '/smarty/templates_c';
	$smarty->cache_dir = $GLOBALS['workConstantes']->TMP_PATH . '/smarty/cache';
	$smarty->config_dir = $GLOBALS['workConstantes']->TMP_PATH . '/smarty/configs';

	if (!$carpeta)
	{
		if (version_movil())
		{
			$carpeta = "public_mobile";
		}
		else
		{
			$carpeta = "public2";
		}
	}

	$smarty->template_dir = $GLOBALS['workConstantes']->TMP_PATH . '/cache_plantillas/' . $idioma . '/templates/' . $carpeta . '/';
	$smarty->root_url = './templates/' . $carpeta;
	$smarty->root_url_limpio = '/templates/' . $carpeta;

	$smarty->assign("GESTION_ROOT_URL", $GLOBALS['workConstantes']->GESTION_ROOT_URL);
	$smarty->assign("PUBLIC_ROOT_URL", $GLOBALS['workConstantes']->PUBLIC_ROOT_URL);

	$smarty->assign("STATIC_URL", substr(ifichero::getUrl($GLOBALS['workConstantes']->CARPETA_STATIC), 0, -1));
	$smarty->assign("ZONA_UNICA", $GLOBALS['workConstantes']->ZONA_UNICA);

	$smarty->assign("CARPETA_CLIENTE", $GLOBALS['workConstantes']->CARPETA_CLIENTE);

	if ($GLOBALS['workConstantes']->CARPETA_CUSTOM_CLIENTE)
		$smarty->assign("CARPETA_CUSTOM_CLIENTE", $GLOBALS['workConstantes']->CARPETA_CUSTOM_CLIENTE);
	else
		$smarty->assign("CARPETA_CUSTOM_CLIENTE", $GLOBALS['workConstantes']->CARPETA_CLIENTE);

	if ($GLOBALS['workConstantes']->CARPETA_DISENYO)
		$smarty->assign("CARPETA_DISENYO", $GLOBALS['workConstantes']->CARPETA_DISENYO);
	else
		$smarty->assign("CARPETA_DISENYO", $GLOBALS['workConstantes']->CARPETA_CLIENTE);

	$smarty->assign("HTTP_URL", $GLOBALS['workConstantes']->HTTP_URL);
	$smarty->assign("GESTION_ROOT_URL_NO_SCRIPT", $GLOBALS['workConstantes']->GESTION_ROOT_URL_NO_SCRIPT);
	$smarty->assign("PUBLIC_ROOT_URL_NO_SCRIPT", $GLOBALS['workConstantes']->PUBLIC_ROOT_URL_NO_SCRIPT);
	$smarty->assign("URL_TO_IMAGES", $GLOBALS['workConstantes']->URL_TO_IMAGES);
	$smarty->assign("MONEDA",$GLOBALS['workConstantes']-> MONEDA);
	$smarty->assign("PATH", $GLOBALS['workConstantes']->PATH);
	$smarty->assign("PLANTILLA_CLIENTE", $GLOBALS['workConstantes']->PLANTILLA_CLIENTE);
	$smarty->assign("CARPETA_CLIENTE", $GLOBALS['workConstantes']->CARPETA_CLIENTE);
	$smarty->assign("PAPEL_TAMANYO_X", $GLOBALS['workConstantes']->PAPEL_TAMANYO_X);
	$smarty->assign("PAPEL_TAMANYO_Y", $GLOBALS['workConstantes']->PAPEL_TAMANYO_Y);

	if (session::get('sesion_sucursal') == "Y")
	{
		$smarty->assign("SUCURSAL", true);
	}
	else
	{
		$smarty->assign("SUCURSAL", false);
	}

	if ($GLOBALS['workConstantes']->PAPEL_DPI)
	{
		$smarty->assign("PAPEL_DPI", $GLOBALS['workConstantes']->PAPEL_DPI);
	}
	else
	{
		$smarty->assign("PAPEL_DPI", "300");
	}

	if ($GLOBALS['workConstantes']->CONFIG_IMPRESORA)
	{
		$smarty->assign("CONFIG_IMPRESORA", $GLOBALS['workConstantes']->CONFIG_IMPRESORA);
	}
	else
	{
		$smarty->assign("CONFIG_IMPRESORA", "");
	}

	if ($GLOBALS['workConstantes']->WEB_MOSTRAR_AFORO == "Y")
	{
		$smarty->assign("WEB_MOSTRAR_AFORO", true);
	}
	else
	{
		$smarty->assign("WEB_MOSTRAR_AFORO", false);
	}

	$smarty->assign("VERSION", $GLOBALS['workConstantes']->VERSION . (desarrollo() ? ("_" . genera_aleatorio()) : ""));
	$smarty->assign("PREPRODUCCION", pre());

	return $smarty;

}

function notifica_error($asunto, $error)
{
	$mail = new html_mime_mail;

	if (isset($GLOBALS['id_registro_acceso']) && $GLOBALS['id_registro_acceso'])
	{
		$error .= "<br />\nID Registro: " . $GLOBALS['id_registro_acceso'];
	}

	$mail->add_html($error, $error);
	$mail->build_message();

	if ($GLOBALS['workConstantes']->METODO_EMAIL)
	{
		$metode_enviament_email = $GLOBALS['workConstantes']->METODO_EMAIL;
	} else
	{
		$metode_enviament_email = "mail";
	}

	if (desarrollo())
	{
		$server_admin = isset($_SERVER['SERVER_ADMIN']) ? ": " . $_SERVER['SERVER_ADMIN'] : "";
		$asunto = "[DEV" . $server_admin . "] " . $asunto;
	}
	elseif (pre())
	{
		$asunto = "[PRE] " . $asunto;
	}
	else
	{
		$asunto = "[PRO] " . $asunto;
	}

	$mails[] = "monitor@koobin.com";
	$mail->send("Monitor Koobin", $mails, "Koobinevent SL", "monitor@koobin.com", $asunto, false, $metode_enviament_email);

}

function quotreplace($cadena)
{
	return str_replace("\"", "&quot;", $cadena);
}

function ean_key($barcode)
{
	$keyFamily = array(
		0 => 1,
		1 => 3,
		2 => 1,
		3 => 3,
		4 => 1,
		5 => 3,
		6 => 1,
		7 => 3,
		8 => 1,
		9 => 3,
		10 => 1,
		11 => 3,
		12 => 0,
	);
	settype($barcode,'string');
	//Transformation de la chaine EAN en tableau
	for($i=0;$i<13;$i++)
	{
		$EAN13[$i] = substr($barcode,$i,1);
	}
	$total = 0;
	for($i=0;$i<13;$i++)
	{
		$total += $EAN13[$i] * $keyFamily[$i];
	}
	if ($total==0)
		return -1;
	else
	{
		return ((10-($total % 10))%10);
	}
}

function date_to_timestamp($fecha, $fin = false)
{
	$arr_fecha = explode(" ", $fecha);

	if (isset($arr_fecha[1]))
	{
		$hora = explode(":", $arr_fecha[1]);
	}
	else
	{
		if ($fin)
		{
			$hora[0] = 23;
			$hora[1] = 59;
		}
		else
		{
			$hora[0] = 0;
			$hora[1] = 0;
		}
	}
	$dia = explode("/", $arr_fecha[0]);

	if ($fin) $segundos = 59;
	else $segundos = 0;

	$fecha_timestamp = mktime($hora[0], $hora[1], $segundos, $dia[1], $dia[0], $dia[2]);

	return $fecha_timestamp;
}

/*** Genera un número aleatorio entre un mínimo y un máximo
 * @param $min
 * @param $max
 * @return int Número aleatorio entre $min y $max
 */
function genera_aleatorio($min = 0, $max = 0)
{
	if ($max == 0)
		$max = mt_getrandmax();

	if ($max >= $min)
	{
		list($usec, $sec) = explode(' ', microtime());
		mt_srand( (float) $sec + ((float) $usec * 100000) );

		return mt_rand($min, $max);
	}
	else
	{
		trigger_error("El límite máximo no puede ser inferior al mínimo", E_USER_WARNING);
		return FALSE;
	}
}