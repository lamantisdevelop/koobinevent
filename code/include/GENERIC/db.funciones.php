<?php
$n = 0;

/**
 * Conecta con una BD y establece la codificación a UTF-7
 *
 * @param $host
 * @param $username
 * @param $passwd
 * @param $database (optional)
 * @param $replica (optional)
 *
 * @return conexion
 */
function db_connect($host, $username, $passwd, $database = false, $replica = false)
{
	$dbtype = DBTYPE;
	$con = $dbtype::connect($host, $username, $passwd, $database);

	if ($con)
	{
		db_set_charset("utf8", $con);

		if(defined("ZONA_HORARIA") && in_array(ZONA_HORARIA, DateTimeZone::listIdentifiers()))
		{
			$GLOBALS['conex'][db_thread_id($con)]['zona_horaria'] = ZONA_HORARIA;
		}
		else
		{
			$GLOBALS['conex'][db_thread_id($con)]['zona_horaria'] = false;
		}

		$GLOBALS['conex'][db_thread_id($con)]['username'] = $username;
		$GLOBALS['conex'][db_thread_id($con)]['database'] = $database;
		$GLOBALS['conex'][db_thread_id($con)]['host'] = $host;
		$GLOBALS['conex'][db_thread_id($con)]['con'] =& $con;
		$GLOBALS['conex'][db_thread_id($con)]['replica'] = $replica;

		return $con;
	}
	else
	{
		return false;
	}
}

/**
 * @param      $database
 * @param      $tcon (optional) Conexión de la BD
 *
 * @return bool
 */
function db_select_db($database, $tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	
	$GLOBALS['conex'][db_thread_id($con)]['database'] = $database;
	
	$dbtype = DBTYPE;
	return $dbtype::select_db($database, $con);
	
	
}

/**
 * Ejecuta una consulta y devuelve el resultado.
 *
 * @param      $query
 * @param      $file
 * @param      $line
 * @param 	   $con (optional) Conexión de la BD.
 * @param bool $debug (optional) Si hay un duplicado no da error.
 * @param bool $stop_on_error (optional) Se interrumple la ejecución si hay un error en la consulta.
 * @param bool $envia_mail (optional) Envía un email si hay un error.
 *
 * @return result El resultado de la consulta
 */
function query($query, $file, $line, $con = false, $debug = true, $stop_on_error = false, $envia_mail = true)
{
	$dbtype = DBTYPE;
	
	global $n;

	if (!$con)
	{
		global $con;		//variable de conexió MySql
	}

	if ($GLOBALS['conex'][db_thread_id($con)]['replica'])
	{
		if (strcasecmp(trim($query), "SHOW SLAVE STATUS") !== 0)
		{
			// Es una réplica
			if (slave_status($con) === NULL)
			{
				$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $GLOBALS['conex'][db_thread_id($con)]['database'];

				$mail = new html_mime_mail;

				$asunto = "Ocurrido error en " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " (" . $maquina . ")";

				$body = "La réplica está rota.</b><br>\n";
				$mail->add_html($body, $body);
				$mails[] = "monitor@koobin.com";
				$mail->send("monitor@koobin.com", $mails, "monitor@koobin.com", "monitor@koobin.com", $asunto);
				die;
			}
			else if (slave_status($con) != 0)
			{
				// Está más de un segundo detrás del master. Que espere un momento.
				$cont = 0;
				do
				{
					sleep(1);
					$cont++;
				}
				while ($cont < 10 && slave_status($con) != 0);

				if ($cont >= 10)
				{
					/// Ha salido con la replica retrasada
					$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $GLOBALS['conex'][db_thread_id($con)]['database'];

					$mail = new html_mime_mail;

					$asunto = "Ocurrido error en " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " (" . $maquina . ")";

					$body = "La réplica está " . slave_status($con) . " segundos atrás.</b><br>\n";
					$mail->add_html($body, $body);
					$mails[] = "monitor@koobin.com";
					$mail->send("monitor@koobin.com", $mails, "monitor@koobin.com", "monitor@koobin.com", $asunto);
					die;

				}
			}
		}
	}

	$n++;
	$query = trim($query);

	
//	if (MYSQL_DATABASE == "ticketing_ibercamera"
//		|| MYSQL_DATABASE == "ticketing_ibercamera_dev")
//	if (true)
//	if (MYSQL_DATABASE != "ticketing_acb" && MYSQL_DATABASE != "ticketing_liceu_dev")
	if (false)
	{
		if (substr($query, 0, strlen("SELECT")) != "SELECT")
		{
			$logfile = fopen(TMP_PATH . "/" . $GLOBALS['conex'][db_thread_id($con)]['database'] . "_query.log", "a");
			fwrite($logfile, "[" . date("d-M-Y H:i:s") . "] #" . db_thread_id($con) . "    " . $query . ";   File: " . $file . " Line: " . $line . "\n");
			fclose($logfile);
		}
	}

	if (db_errno($con) != 2006)
	{
		if (function_exists("desarrollo") && desarrollo()
			&& isset($_SERVER) && isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != "192.168.30.251")
		{
			if (!isset($GLOBALS['consultas_realizadas'][md5($query)]))
			{
				$GLOBALS['consultas_realizadas'][md5($query)]['consulta'] = $query;
				$GLOBALS['consultas_realizadas'][md5($query)]['veces'] = 1;
			}
			else
			{
				$GLOBALS['consultas_realizadas'][md5($query)]['veces']++;
			}
		}
		$result = $dbtype::query($query, $con);
	}
	else
	{
		die;
	}

	if (!isset($GLOBALS['total_consultas']))
	{
		$GLOBALS['total_consultas'] = 0;
	}
	$GLOBALS['total_consultas']++;
	
	$errno = db_errno($con);
	$error = db_error($con);

	if ($errno != 0)
	{
		if ($errno == 2006)
		{
			// El MySQL está parado! Paramos la ejecución para que no entre en bucle!
			$stop_on_error = true;
		}
		
		if ($debug) // Control d'errors - mostrem tot
		{
			$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $GLOBALS['conex'][db_thread_id($con)]['database'];

			if (function_exists("desarrollo")
				&& !desarrollo()
				&& $envia_mail
				&& class_exists("html_mime_mail"))
			{
				$mail = new html_mime_mail;

				$asunto = "Ocurrido error en " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " (" . $maquina . ")";

				$body = "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>\n";
				$body .= $query . "<br>\n";

				if (isset($GLOBALS['id_registro_acceso']))
				{
					$body .= "<br />\nID Registro: " . $GLOBALS['id_registro_acceso'];
				}

				if (desarrollo())
				{
					$server_admin = isset($_SERVER['SERVER_ADMIN']) ? ": " . $_SERVER['SERVER_ADMIN'] : "";
					$asunto = "[DEV" . $server_admin . "] " . $asunto;
				}
				elseif (pre())
				{
					$asunto = "[PRE] " . $asunto;
				}
				else
				{
					$asunto = "[PRO] " . $asunto;
				}
				
				$mail->add_html($body, $body);
				$mails[] = "monitor@koobin.com";
				$mail->send("monitor@koobin.com", $mails, "monitor@koobin.com", "monitor@koobin.com", $asunto);
			}

			if ((function_exists("desarrollo") && desarrollo()) || (isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == "127.0.0.1" || $_SERVER['SERVER_ADDR'] == "::1")))
			{
				echo ($query . "<br>" . "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>");
			}
			else
			{
				if (ini_get("log_errors") == true)
				{
					$logfile = fopen(sys_get_temp_dir() . "/koobin/php-error.log", "a");
					fwrite($logfile, "[" . date("d-M-Y H:i:s") . "] MySQL error " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " - " . $GLOBALS['id_registro_acceso'] . " (" . $maquina . ") Error en Sentencia Mysql (" . $errno . ")" . "\n" . $error . ", File: " . $file . " Line: " . $line . "\n");
					fclose($logfile);
				}
			}
			
			if ($stop_on_error)
			{
				die;
			}
		}
		else // filtrem errors
		{
			if ($errno != db_duplicate_code()) // si no es el codi de duplicate entry, mostrem
			{
				$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $GLOBALS['conex'][db_thread_id($con)]['database'];

				if (function_exists("desarrollo")
					&& !desarrollo()
					&& $envia_mail
					&& class_exists("html_mime_mail"))
				{
					$mail = new html_mime_mail;

					$asunto = "Ocurrido error en " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " (" . $maquina . ")";

					$body = "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>\n";
					$body .= $query . "<br>\n";

					if ($GLOBALS['id_registro_acceso'])
					{
						$body .= "<br />\nID Registro: " . $GLOBALS['id_registro_acceso'];
					}
					
					if (desarrollo())
					{
						$server_admin = isset($_SERVER['SERVER_ADMIN']) ? ": " . $_SERVER['SERVER_ADMIN'] : "";
						$asunto = "[DEV" . $server_admin . "] " . $asunto;
					}
					elseif (pre())
					{
						$asunto = "[PRE] " . $asunto;
					}
					else
					{
						$asunto = "[PRO] " . $asunto;
					}

					$mail->add_html($body, $body);
					$mails[] = "monitor@koobin.com";
					$mail->send("monitor@koobin.com", $mails, "monitor@koobin.com", "monitor@koobin.com", $asunto);
				}

				if ((function_exists("desarrollo") && desarrollo()) || (isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == "127.0.0.1" || $_SERVER['SERVER_ADDR'] == "::1")))
				{
					echo ($query . "<br>" . "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>");
				}
				else
				{
					if (ini_get("log_errors") == true)
					{
						$logfile = fopen(sys_get_temp_dir() . "/koobin/php-error.log", "a");
						fwrite($logfile, "[" . date("d-M-Y H:i:s") . "] MySQL error " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " - " . $GLOBALS['id_registro_acceso'] . " (" . $maquina . ") Error en Sentencia Mysql (" . $errno . ")" . "\n" . $error . ", File: " . $file . " Line: " . $line . "\n");
						fclose($logfile);
					}
				}
				
				if ($stop_on_error)
				{
					die;
				}
			}
		}
	}

	return $result;
}

//Funcions adicionals mysql

//Fetch
//
//	Rep les variables	$result:	Resultat MySql
//						$file		Nom del fitxer des d'on és cridat
//						$line		Linia del fitxer des de on és cridat
//	Retorna un row de Mysql
/**
 * Recorre el result y devuelve el row correspondiente.
 *
 * @param        $result
 * @param string $parametros (optional)
 *
 * @return row Row de recorrer el result
 */
function fetch($result, $parametros = "ASSOC")
{
	$dbtype = DBTYPE;
	return $dbtype::fetch($result, $parametros);
}

/**
 * Devuelve el número de lineas de un result.
 *
 * @param $result
 *
 * @return int
 */
function db_num_rows($result)
{
	$dbtype = DBTYPE;
	return (int) $dbtype::num_rows($result);
}

/**
 * Devuelve el ID insertado en la última consulta
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return ID
 */
function db_insert_id($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::insert_id($con);
}

/**
 * Devuelve el número de lineas afectadas en la última consulta
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return int número de lineas
 */
function db_affected_rows($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::affected_rows($con);
}

/**
 * Devuelve el código de error de la última consulta
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return int Código de error
 */
function db_errno($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::errno($con);
}

/**
 * Devuelve el código de error al intentar conectar con la BD.
 *
 * @return int Código de error
 */
function db_connect_errno()
{
	$dbtype = DBTYPE;
	
	return $dbtype::connect_errno();
}

/**
 * Devuelve la descripción del error de la última consulta.
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return string Descripción del error
 */
function db_error($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::error($con);
}

/**
 * Devuelve la descripción del error del último intento de conexión con la BD.
 *
 * @return string Descripción del error
 */
function db_connect_error()
{
	$dbtype = DBTYPE;
	
	return $dbtype::connect_error();
}

/**
 * Cierra la conexión con la BD.
 *
 * @param $tcon Identificador de la conexión
 *
 * @return bool
 */
function db_close($tcon)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;

	if (isset($GLOBALS['conex'][db_thread_id($con)]))
	{
		unset($GLOBALS['conex'][db_thread_id($con)]);
	}

	return $dbtype::close($con);
}

/**
 * Establece la codificación para la conexión con la BD.
 * @param string    $charset
 * @param $tcon (optional) Identificador de conexión con la BD.
 *
 * @return bool
 */
function db_set_charset($charset, $tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::set_charset($charset, $con);
}

/**
 * Hace un Ping a la BD para ver si la conexión está activa.
 * @param $tcon (optional) Identificador de la conexión
 *
 * @return bool
 */
function db_ping($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::ping($con);
}

/**
 * Mueve la posición del resultado a la linea que se le diga.
 * @param $result
 * @param $row_number
 *
 * @return bool
 */
function db_data_seek($result, $row_number)
{
	$dbtype = DBTYPE;
	return $dbtype::data_seek($result, $row_number);
}

/**
 * Devuelve el número de campos que tiene el resultado de una consulta.
 * @param $result
 *
 * @return bool
 */
function db_num_fields($result)
{
	$dbtype = DBTYPE;
	return $dbtype::num_fields($result);
}

function db_fetch_row($result)
{
	$dbtype = DBTYPE;
	return $dbtype::fetch_row($result);
}

function db_field_name($result, $field_offset)
{
	$dbtype = DBTYPE;
	return $dbtype::field_name($result, $field_offset);
}

function db_duplicate_code()
{
	$dbtype = DBTYPE;
	return $dbtype::duplicateCode;
}

function db_free_result($result)
{
	$dbtype = DBTYPE;
	return $dbtype::free_result($result);
}

function db_thread_id($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::thread_id($con);
}

function db_real_escape_string($escapestr, $tcon = false)
{
	if (!is_bool($escapestr))
	{
		if (!$tcon)
		{
			global $con;
		}
		else
		{
			$con = $tcon;
		}
		$dbtype = DBTYPE;
		return $dbtype::real_escape_string($escapestr, $con);
	}
	else
	{
		return "";
	}
}