<?php

/**
 * Description of mysql
 *
 * @author Christian
 */
class dbmysql
{
	const duplicateCode = 1062;
	
	static function connect($host, $username, $passwd, $database)
	{
		$con = mysql_connect($host, $username, $passwd);
		
		if ($con)
		{
			if (self::select_db($database, $con))
			{
				return $con;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	static function select_db($database, $con = false)
	{
		if ($con)
		{
			return mysql_select_db($database, $con);
		}
		else
		{
			return mysql_select_db($database);
		}
	}
	
	static function query($query, $con = false)
	{
		if ($con)
		{
			return mysql_query($query, $con);
		}
		else
		{
			return mysql_query($query);
		}
	}
	
	static function fetch($result, $parametros = "ASSOC")
	{
		switch ($parametros)
		{
			case "ASSOC":
				$parametros = MYSQL_ASSOC;
				break;
			
			case "NUM":
				$parametros = MYSQL_NUM;
				break;
			
			case "BOTH":
			default:
				$parametros = MYSQL_BOTH;
				break;
		}
		
		return mysql_fetch_array($result, $parametros);
	}
	
	static function errno($con = false)
	{
		if ($con)
		{
			return mysql_errno($con);
		}
		else
		{
			return mysql_errno();
		}
	}
	
	static function connect_errno()
	{
		return mysql_errno();
	}
	
	static function error($con = false)
	{
		if ($con)
		{
			return mysql_error($con);
		}
		else
		{
			return mysql_error();
		}
	}
	
	static function connect_error()
	{
		return mysql_error();
	}
	
	static function num_rows($result)
	{
		return mysql_num_rows($result);
	}
	
	static function insert_id($con = false)
	{
		if ($con)
		{
			return mysql_insert_id($con);
		}
		else
		{
			return mysql_insert_id();
		}
	}
	
	static function affected_rows($con = false)
	{
		if ($con)
		{
			return mysql_affected_rows($con);
		}
		else
		{
			return mysql_affected_rows();
		}
	}
	
	static function close($con)
	{
		return mysql_close($con);
	}
	
	static function set_charset($charset, $con = false)
	{
		if ($con)
		{
			return mysql_set_charset($charset, $con);
		}
		else
		{
			return mysql_set_charset($charset);
		}
	}
	
	static function ping($con = false)
	{
		if ($con)
		{
			return mysql_ping($con);
		}
		else
		{
			return mysql_ping();
		}
	}
	
	static function data_seek($result, $row_number)
	{
		return mysql_data_seek($result, $row_number);
	}
	
	static function num_fields($result)
	{
		return mysql_num_fields($result);
	}
	
	static function fetch_row($result)
	{
		return mysql_fetch_row($result);
	}
	
	static function field_name($result, $field_offset)
	{
		return mysql_field_name($result, $field_offset);
	}
	
	static function free_result($result)
	{
		return mysql_free_result($result);
	}
	
	static function thread_id($con = false)
	{
		if ($con)
		{
			return mysql_thread_id($con);
		}
		else
		{
			return mysql_thread_id();
		}
	}
	
	static function real_escape_string($escapestr, $con = false)
	{
		if ($con)
		{
			return real_escape_string($escapestr, $con);
		}
		else
		{
			return real_escape_string($escapestr);
		}
	}
}
