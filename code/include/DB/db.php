<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 19/03/14
 * Time: 17.56
 */

class db
{
	static function buscar($campos, $tablas, $where, $group_by = false, $devolver_query = false, $query_union = false, $group_by_global = false, $having = false)
	{
		include_once("./include/GENERIC/db_replica.php");

		$joins = Array();
		$joins['car_ventas']['car_detalle_ventas'] = "V.Car_sid = D.Car_sid";
		$joins['car_ventas']['ab_clientes'] = "V.Cliente_id = C.id_cliente AND V.tabla = 'AB_clientes'";
		$joins['car_ventas']['en_clientes'] = "V.Cliente_id = C.id_cliente AND V.tabla = 'EN_clientes'";
		$joins['car_ventas']['clientes'] = "V.Cliente_id = C.id_cliente AND V.tabla = '{\$cliente}'";
		$joins['clientes']['car_ventas'] = "V.Cliente_id = C.id_cliente AND V.tabla = '{\$cliente}'";
		$joins['clientes']['ab_abonos'] = "A.id_cliente = C.id_cliente AND A.tabla = '{\$cliente}'";
		$joins['clientes']['us_campos'] = "U.id_cliente = C.id_cliente AND U.tabla = '{\$cliente}'";
		$joins['clientes_rel']['car_ventas'] = "";
		$joins['clientes_rel']['car_detalle_ventas'] = "D.id_cliente_assoc = C.id_cliente AND D.tabla_assoc = '{\$cliente}'";
		$joins['clientes_rel']['ab_abonos'] = "A.id_cliente = C.id_cliente AND A.tabla = '{\$cliente}'";
		$joins['clientes_rel']['us_campos'] = "U.id_cliente = C.id_cliente AND U.tabla = '{\$cliente}'";
		$joins['car_detalle_ventas']['car_ventas'] = "V.Car_sid = D.Car_sid";
		$joins['car_detalle_ventas']['us_campos'] = "U.id_cliente = C.id_cliente AND U.tabla = '{\$cliente}'";
		$joins['ab_abonos']['ab_clientes'] = "A.id_cliente = C.id_cliente AND A.tabla = '{\$cliente}'";
		$joins['ab_abonos']['car_ventas'] = "A.id_cliente = C.id_cliente";
		$joins['ab_clientes']['ab_abonos'] = "A.id_cliente = C.id_cliente AND A.tabla = '{\$cliente}'";

		$leftjoins = Array();
		$leftjoins['car_ventas']['car_detalle_ventas'] = "V.Car_sid = D.Car_sid AND D.cancelada = 'N' AND V.reserva = 'N'";
		$leftjoins['car_ventas']['ab_clientes'] = "V.Cliente_id = C.id_cliente AND V.tabla = 'AB_clientes'";
		$leftjoins['car_ventas']['en_clientes'] = "V.Cliente_id = C.id_cliente AND V.tabla = 'EN_clientes'";
		$leftjoins['car_ventas']['clientes'] = "V.Cliente_id = C.id_cliente AND V.tabla = '{\$cliente}'";
		$leftjoins['clientes']['car_ventas'] = "V.Cliente_id = C.id_cliente AND V.tabla = '{\$cliente}'";
		$leftjoins['clientes']['ab_abonos'] = "A.id_cliente = C.id_cliente AND A.tabla = '{\$cliente}'";
		$leftjoins['clientes']['us_campos'] = "U.id_cliente = C.id_cliente AND U.tabla = '{\$cliente}'";
		$leftjoins['clientes_rel']['car_ventas'] = "";
		$leftjoins['clientes_rel']['car_detalle_ventas'] = "D.id_cliente_assoc = C.id_cliente AND D.tabla_assoc = '{\$cliente}'";
		$leftjoins['clientes_rel']['ab_abonos'] = "A.id_cliente = C.id_cliente AND A.tabla = '{\$cliente}'";
		$leftjoins['clientes_rel']['us_campos'] = "U.id_cliente = C.id_cliente AND U.tabla = '{\$cliente}'";
		$leftjoins['car_detalle_ventas']['car_ventas'] = "V.Car_sid = D.Car_sid AND D.cancelada = 'N' AND V.reserva = 'N'";
		$leftjoins['car_detalle_ventas']['us_campos'] = "U.id_cliente = C.id_cliente AND U.tabla = '{\$cliente}'";
		$leftjoins['ab_abonos']['ab_clientes'] = "A.id_cliente = C.id_cliente AND A.tabla = '{\$cliente}'";
		$leftjoins['ab_abonos']['car_ventas'] = "A.id_cliente = C.id_cliente";
		$leftjoins['ab_clientes']['ab_abonos'] = "A.id_cliente = C.id_cliente";

		$union = false;
		$hay_where_us_campos = false;
		foreach ($tablas as $abreviatura => $tabla)
		{
			if ($tabla == "clientes" || $tabla == "clientes_rel")
			{
				$campos[] = Array('tabla' => '', 'campo' => "tabla", "calculo" => "", "calculo_campo" => "'{\$cliente}'");
				$union = true;
			}
		}

		$query = "SELECT ";
		$campos_query_global = "";

		$left_join_campos = Array();

		if ($campos && is_array($campos))
		{
			foreach ($campos as $det_campo)
			{
				if (substr($det_campo['campo'], 0, 15) == "cpersonalizado_")
				{
					$valor_campo_per = substr($det_campo['campo'], 15);
					$det_campo['campo'] = "valor AS " . $det_campo['campo'];

					$left_join_campos[$det_campo['tabla']][$valor_campo_per] = "id_campo";
				}

				if (isset($det_campo['calculo']))
				{
					if ($det_campo['calculo']) $query .= $det_campo['calculo'] . "(";

					if ($det_campo['tabla']) $query .= $det_campo['tabla'] . ".";

					$query .= $det_campo['calculo_campo'];

					if ($det_campo['calculo']) $query .= ")";

					$query .= " AS " . $det_campo['campo'] . ", ";

					if ($det_campo['calculo']) $campos_query_global .= "SUM(" . $det_campo['campo'] . ") AS " . $det_campo['campo'] . ", ";
					else $campos_query_global .= $det_campo['campo'] . ", ";
				}
				else
				{
					if ($det_campo['tabla']) $query .= $det_campo['tabla'] . ".";

					$query .= $det_campo['campo'] . ", ";
					$campos_query_global .= $det_campo['campo'] . ", ";
				}
			}

			$query = substr($query, 0, -2);
			$campos_query_global = substr($campos_query_global, 0, -2);
		}
		else
		{
			$query .= "* ";
		}

		$query .= " FROM ";

		$array_where = Array();
		if ($where)
		{
			$where_tmp['pre'] = "WHERE (";
			array_push($array_where, $where_tmp);

			$siguiente_operador = "";
			$grupo_anterior = "";

			foreach ($where as $det_where)
			{
				$query_tmp = "";

				if ($grupo_anterior === "") $grupo_anterior = $det_where['agrupacion'];

				if ($siguiente_operador != "")
				{
					// Si no es la primera pasada ya tenemos operador.
					if (isset($det_where['operador']))
					{
						if ($grupo_anterior != $det_where['agrupacion'])
						{
							$query_tmp .= ")";

							if ($siguiente_operador == "AND")
							{
								$query_tmp .= " OR (";
							}
							else
							{
								$query_tmp .= " AND (";
							}
						}
						else
						{
							// Si existe el del where lo agregamos a la query
							$query_tmp .= " " . $det_where['operador'] . " ";
						}
					}
					else
					{
						if ($grupo_anterior != $det_where['agrupacion'])
						{
							$query_tmp .= ")";
						}

						// Si no existe por defecto agregamos a la query un AND
						$query_tmp .= " AND ";
					}
				}

				$where_tmp = Array();

				$where_tmp['pre'] = $query_tmp;
				$where_tmp['cli'] = "";
				$where_tmp['abo'] = "";
				$query_tmp = "";
				$query_tmp_cli = "";
				$query_tmp_abo = "";

				// Si tenemos el campo extra significa que en una condición nos llegan dos. Por ejemplo: (campo IS NULL OR campo = '')
				if (isset($det_where['extra']) && is_array($det_where['extra']))
				{
					$query_tmp .= "(" . $det_where['tabla'] . "." . $det_where['campo'] . " " . $det_where['criterio'] . " " . $det_where['valor'];
					$query_tmp .= " OR " . $det_where['extra']['tabla'] . "." . $det_where['extra']['campo'] . " " . $det_where['extra']['criterio'] . " " . $det_where['extra']['valor'] . ")";
				}
				else
				{
					if ((isset($det_where['cliente_in']) && $det_where['cliente_in']) || (isset($det_where['abonados_in']) && $det_where['abonados_in']))
					{
						if ($det_where['cliente_in'])
						{
							$query_tmp_cli = $query_tmp;
							$query_tmp_cli .= $det_where['tabla'] . "." . $det_where['campo'] . " " . $det_where['criterio'] . " (" . $det_where['cliente_in'] . ")";
						}
						else
						{
							$query_tmp_cli = $query_tmp;
							$query_tmp_cli .= $det_where['tabla'] . "." . $det_where['campo'] . " " . $det_where['criterio'] . " ('-1')";
						}

						if ($det_where['abonados_in'])
						{
							$query_tmp_abo = $query_tmp;
							$query_tmp_abo .= $det_where['tabla'] . "." . $det_where['campo'] . " " . $det_where['criterio'] . " (" . $det_where['abonados_in'] . ")";
						}
						else
						{
							$query_tmp_abo = $query_tmp;
							$query_tmp_abo .= $det_where['tabla'] . "." . $det_where['campo'] . " " . $det_where['criterio'] . " ('-1')";
						}
					}

					if (isset($det_where['campobd']) && $det_where['campobd'] == "textdate")
					{
						$tmp_campo = "CONCAT(SUBSTRING_INDEX(" . $det_where['tabla'] . "." . $det_where['campo'] . ", '/', -1), '-', " .
							"SUBSTRING_INDEX(SUBSTRING_INDEX(" . $det_where['tabla'] . "." . $det_where['campo'] . ", '/', 2), '/', -1), '-', " .
							"SUBSTRING_INDEX(" . $det_where['tabla'] . "." . $det_where['campo'] . ", '/', 1))";
						$query_tmp .= $tmp_campo . " " . $det_where['criterio'] . " " . $det_where['valor'];
					}
					else
					{
						$query_tmp .= $det_where['tabla'] . "." . $det_where['campo'] . " " . $det_where['criterio'] . " " . $det_where['valor'];
					}
				}

				if ($det_where['tabla'] == "U")
				{
					$hay_where_us_campos = true;
				}

				$where_tmp['common'] = $query_tmp;
				$where_tmp['cli'] = $query_tmp_cli;
				$where_tmp['abo'] = $query_tmp_abo;

				if (isset($det_where['operador'])) $siguiente_operador =  " " . $det_where['operador'] . " ";
				else $siguiente_operador = " AND ";

				$grupo_anterior = $det_where['agrupacion'];
				array_push($array_where, $where_tmp);
			}

			$where_tmp = Array();
			$where_tmp['pre'] = ")";
			array_push($array_where, $where_tmp);
		}

		$contador = 1;
		$tabla_anterior = "";

		$hay_clientes = false;
		$hay_ventas = false;

		foreach ($tablas as $abreviatura => $tabla)
		{
			$tabla = strtolower($tabla);
			if ($tabla == "car_ventas") $hay_ventas = true;
			if ($tabla == "ab_abonos") $hay_ventas = true;
			if ($tabla == "clientes" || $tabla == "clientes_rel" || $tabla == "ab_clientes" || $tabla == "en_clientes") $hay_clientes = true;
		}

		foreach ($tablas as $abreviatura => $tabla)
		{
			$tabla_para_joins = $tabla;
			if ($tabla == "clientes" || $tabla == "clientes_rel")
			{
				$tabla = "{\$cliente}";
				$union = true;
			}

			if ($contador == 1)
			{
				$query .= $tabla . " " . $abreviatura . " ";
			}
			else
			{
				if ((strtolower($tabla_anterior) == "car_detalle_ventas" || strtolower($tabla_anterior) == "car_ventas")
					&& strtolower($tabla) == "ab_abonos")
				{
					// Si se está haciendo una join entre detalle de ventas y abonos, miramos si se ha incluido la tabla de clientes
					// para hacer la join a través de la tabla de clientes

					if ($key = array_search("clientes", $tablas))
					{
						$tabla_anterior = $tablas[$key];
					}
					else
					{
						if ($key = array_search("clientes_rel", $tablas))
						{
							$tabla_anterior = $tablas[$key];
						}
					}
				}

				// Si la tabla es la US_campos pero no se está filtrando por ella en el where, hacemos un left join, ya que sino solo aparecerían los clientes
				// que tuvieran registro en la tabla us_campos
				if (($abreviatura == "U" && !$hay_where_us_campos)
					|| ($hay_ventas && $hay_clientes))
				{
					$query .= " LEFT JOIN " . $tabla . " " . $abreviatura . " ON ";
					$query .= $leftjoins[strtolower($tabla_anterior)][strtolower($tabla)] . " ";
				}
				else
				{
					$query .= " JOIN " . $tabla . " " . $abreviatura . " ON ";
					$query .= $joins[strtolower($tabla_anterior)][strtolower($tabla)] . " ";
				}

				// TODO En el caso de un left join con la tabla US_campos hay que añadir contra que campos personalizables se están filtrando
				if ($abreviatura == "U" && !$hay_where_us_campos)
				{
					if ($left_join_campos)
					{
						$query .= "AND (";
						foreach ($left_join_campos as $abreviatura_tabla_left_join => $det_campos_left_join)
						{
							foreach ($det_campos_left_join as $id_campo_left_join => $campo_left_join)
							{
								$query .= $abreviatura_tabla_left_join . "." . $campo_left_join . " = '" . db_real_escape_string($id_campo_left_join) . "' OR ";
							}
						}
						$query = substr($query, 0, -3) . ") ";
					}
				}
			}

			$tabla_anterior = $tabla_para_joins;

			$contador++;
		}

		if ($union)
		{
			$query_abo = "";
			foreach ($array_where as $det_where)
			{
				$query_abo .= $det_where['pre'];

				if (isset($det_where['abo']) && $det_where['abo'])
				{
					$query_abo .= $det_where['abo'];
				}
				else
				{
					if (isset($det_where['common']))
					{
						$query_abo .= $det_where['common'];
					}
				}
			}

			$query_abo = $query . $query_abo;
			$query_abo = str_replace("{\$cliente}", "ab_clientes", $query_abo);

			$query_cli = "";
			foreach ($array_where as $det_where)
			{
				$query_cli .= $det_where['pre'];

				if (isset($det_where['cli']) && $det_where['cli'])
				{
					$query_cli .= $det_where['cli'];
				}
				else
				{
					if (isset($det_where['common']))
					{
						$query_cli .= $det_where['common'];
					}
				}
			}

			$query_cli = $query . $query_cli;
			$query_cli = str_replace("{\$cliente}", "en_clientes", $query_cli);

			if ($group_by)
			{
				$query = $query_abo . " GROUP BY " . $group_by . " UNION " . $query_cli . " GROUP BY " . $group_by;
			}
			else
			{
				$query = $query_abo . " UNION " . $query_cli;
			}
		}
		else
		{
			foreach ($array_where as $det_where)
			{
				$query .= $det_where['pre'];

				if (isset($det_where['common']))
				{
					$query .= $det_where['common'];
				}
			}
		}

		if ($query_union)
		{
			$query .= " UNION " . $query_union;
		}

		if ($devolver_query)
		{
			return $query;
		}
		else
		{
			if ($group_by_global)
			{
				$query = "SELECT " . $campos_query_global . " FROM (" . $query . ") t1 GROUP BY " . $group_by_global;

				if ($having)
				{
					$query .= " HAVING ";

					$operador_anterior = false;

					foreach ($having as $det_having)
					{
						if ($operador_anterior)
						{
							$query .= " " .$det_having['operador'] . " ";
						}

						$query .= $det_having['campo'] . " " . $det_having['criterio'] . " " . $det_having['valor'];

						$operador_anterior = true;
					}
				}
			}

//			debug($query);//die;
			$result = query($query, __FILE__, __LINE__, $GLOBALS['con_replica']);

			return $result;
		}
	}
}