<?php

/**
 * Description of mysqli
 *
 * @author Christian
 */
class dbmysqli
{
	const duplicateCode = 1062;
	
	static function connect($host, $username, $passwd, $database)
	{
		return mysqli_connect($host, $username, $passwd, $database);
	}
	
	static function select_db($database, $con)
	{
		return mysqli_select_db($con, $database);
	}
	
	static function query($query, $con = false)
	{
		return mysqli_query($con, $query);
	}
	
	static function fetch($result, $parametros = "ASSOC")
	{
		switch ($parametros)
		{
			case "ASSOC":
				$parametros = MYSQLI_ASSOC;
				break;
			
			case "NUM":
				$parametros = MYSQLI_NUM;
				break;
			
			case "BOTH":
			default:
				$parametros = MYSQLI_BOTH;
				break;
		}
		
		$row = mysqli_fetch_array($result, $parametros);
		
		if ($row == NULL)
		{
			return false;
		}
		else
		{
			return $row;
		}
	}
	
	static function errno($con = false)
	{
		return mysqli_errno($con);
	}
	
	static function connect_errno()
	{
		return mysqli_connect_errno();
	}
	
	static function error($con = false)
	{
		return mysqli_error($con);
	}
	
	static function connect_error()
	{
		return mysqli_connect_error();
	}
	
	static function num_rows($result)
	{
		return $result->num_rows;
	}
	
	static function insert_id($con = false)
	{
		return mysqli_insert_id($con);
	}
	
	static function affected_rows($con = false)
	{
		return mysqli_affected_rows($con);
	}
	
	static function close($con)
	{
		return mysqli_close($con);
	}
	
	static function set_charset($charset, $con = false)
	{
		return mysqli_set_charset($con, $charset);
	}
	
	static function ping($con = false)
	{
		return mysqli_ping($con);
	}
	
	static function data_seek($result, $row_number)
	{
		return mysqli_data_seek($result, $row_number);
	}
	
	static function num_fields($result = false)
	{
		return mysqli_num_fields($result);
	}
	
	static function fetch_row($result)
	{
		return mysqli_fetch_row($result);
	}
	
	static function field_name($result, $field_offset)
	{
		$campo = mysqli_fetch_field_direct($result, $field_offset);
		return $campo->name;
	}
	
	static function free_result($result)
	{
		return mysqli_free_result($result);
	}
	
	static function thread_id($con = false)
	{
		return mysqli_thread_id($con);
	}

	static function real_escape_string($escapestr, $con = false)
	{
		return mysqli_real_escape_string($con, $escapestr);
	}
}
