<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<div class="hide-for-small ">
<?php
$page = tep_db_fetch_array(tep_get_page_by_id($_GET['pageID']));
//BANNERS
if ($page['banners_id']) 
{
    //CAS la pagina te un slide associat
    if ($_GET['pageID'] == DEFAULT_PAGE) 
    { 
        //CAS pagina INICI 
        ?>
        <div class="">
            <?php
            
            banners_slide_revolution_by_galery($page['banners_id'], 0);
            
            ?>
        </div>
        <?php
    } 
    else 
    {
        //cas pagines interiors (height menys gran)
        banners_slide_revolution_by_galery($page['banners_id'], 0, 'interior');
    }
}
?>
</div>
