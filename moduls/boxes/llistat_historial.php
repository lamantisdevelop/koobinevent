<?php

$day_text = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
$month_text = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');

if (isset($_GET['groupID']))
{
    $group = " AND l.group_id = " . $_GET['groupID'];
}
else if (isset($_GET['llistat']))
{
    $group = " AND lg.parent_id = " . $_GET['llistat'];
}

// Busquem ultimes llistats
$listing = tep_db_query(" SELECT count(l.id) as num, MONTH(l.published) as mes, YEAR(l.published) as any "
                      . " FROM " . TABLE_LLISTATS . " l "
                      . " LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ld on l.id = ld.llistats_id and ld.language_id = '" . (int) $language_id . "' "
                      . " LEFT JOIN " . TABLE_LLISTATS_GROUPS . " lg ON l.group_id = lg.id"
                      . " WHERE l.active='1'"
                      . " AND ld.titol<>'' "
                      . $group
                      . " GROUP BY mes, any "
                      . " ORDER BY l.published desc ");

if (tep_db_num_rows($listing) > 0) 
{
    ?>
    <div class="bloc separat">
        <h3 class="color1"><?php echo _('Historial'); ?></h3>
        <ul id="historial_lat">
            <?php
            
            $any_aux = date("Y", time());
            
            while ($item = tep_db_fetch_array($listing)) 
            {
                if (!isset($any_act))
                {
                    $any_act = $item['any'];
                }
                if ($item['any'] != $any_act && $item['any'] < $any_aux) 
                {
                    $any++;
                    if ($any > 1)
                    {
                        echo '</ul></li>';
                    }
                    
                    // Links javascript amagar si no any $_GET
                    echo '<li><a href="javascript:void(0);" rel="any_' . $item['any'] . '" class="history" >' . $item['any'] . '</a><ul id="any_' . $item['any'] . '" style="' . (($_GET['any'] != $item['any']) ? "display:none;" : "display:block;") . '">';
                    
                }
                
                echo '<li><a href="' . tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'data=' . $item['any'] . '-' . $item['mes']) . '">' . $month_text[$item['mes'] - 1] . ' ' . $item['any'] . '  (' . $item['num'] . ')</a></li>';
                
                $any_act = $item['any'];
            }
            if ($any > 0)
            {
                echo '</ul></li>';
            }
            ?>
        </ul>
    </div>
    <?php
}
?>
