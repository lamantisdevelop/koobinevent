<!-- Inici Box Noticies Search -->
<div class="bloc">
    <h3 class="color1"><?php echo _('Cercador'); ?></h3>
    <form name="llistat_search_form" id="news_search_form" action="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'action=search'); ?>" method="post">
        <?php
        if (isset($_GET['news-search']) && tep_not_null($_GET['news-search'])) {
            echo '<input type="text" name="llistat_search" id="news_search" value="' . htmlentities(utf8_encode($_GET['news-search']), ENT_QUOTES | ENT_IGNORE, "UTF-8") . '" />';
        } else {
            echo '<input type="text" name="llistat_search" id="news_search" placeholder="' . _('Text a cercar') . '" class="blank" />';
        }
        ?>
        <a class="boto_input send_form" href="javascript:void(0);"></a>
    </form>
</div>
