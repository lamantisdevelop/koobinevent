<?php

//if($language_id == 2)//es
//{
//    $day_text = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
//    $month_text = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');
//}
//else if ($language_id == 3)//en
//{
//    $day_text = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
//    $month_text = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');
//}
//else if ($language_id == 4)//fr
//{
//    $day_text = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
//    $month_text = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');
//}
//else //cat
//{
//    $day_text = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
//    $month_text = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');
//}

$day_text[1] = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
$month_text[1] = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');

$day_text[2] = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$month_text[2] = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$day_text[3] = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
$month_text[3] = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

$day_text[4] = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
$month_text[4] = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");






// Busquem ultimes noticies
$listing = tep_db_query("select count(n.id) as num, MONTH(n.published) as mes, YEAR(n.published) as any from " . TABLE_NOTICIES . " n left join " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.language_id = '" . (int) $language_id . "' where ( nd.destacat !='' or (nd.text != '' and nd.text != '<br />')) AND n.active='1' group by mes, any order by n.published desc");
if (tep_db_num_rows($listing) > 0) {
    ?>
    <!-- Inici Box Noticies Historial -->
    <div class="bloc separat">
        <h3 class="color1"><?php echo _('Historial'); ?></h3>
        <ul id="historial_lat">
            <?php
            $any_aux = date("Y", time());
            while ($item = tep_db_fetch_array($listing)) {
                if (!isset($any_act))
                    $any_act = $item['any'];
                if ($item['any'] != $any_act && $item['any'] < $any_aux) {
                    $any++;
                    if ($any > 1)
                        echo '</ul></li>';
                    // Links javascript amagar si no any $_GET
                    echo '<li><a href="javascript:void(0);" rel="any_' . $item['any'] . '" class="history" >' . $item['any'] . '</a><ul id="any_' . $item['any'] . '" style="' . (($_GET['any'] != $item['any']) ? "display:none;" : "display:block;") . '">';
                }
                echo '<li><a href="' . tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'data=' . $item['any'] . '-' . $item['mes']) . '">' . $month_text[$language_id][$item['mes'] - 1] . ' ' . $item['any'] . '  (' . $item['num'] . ')</a></li>';
                $any_act = $item['any'];
            }
            if ($any > 0)
                echo '</ul></li>';
            ?>
        </ul>
    </div>
    <!-- Fi Box Noticies Historial -->
    <?php
}
?>
