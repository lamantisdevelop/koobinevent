<?php
// Banners
$banners_sql = "select b.id, b.name, bd.image, bd.link, bd.target from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on  b.id = bd.banners_id and bd.language_id = '" . (int)$language_id . "' where b.active = '1' and b.group_id = '2' order by b.listorder limit 3";
$banners = tep_db_query($banners_sql);
$banners_numrows = tep_db_num_rows($banners);

if ($banners_numrows>0) {
  echo '<ul id="banners">';
  $i=0;
  while ($image = tep_db_fetch_array($banners)) {
    $i++;
    $imageInfo = new objectInfo($image);
    echo '<li>' //'<li class="' . (($i==1)?'primer':'') . '">'
     . '<a href="' . ((tep_not_null($imageInfo->link) && $imageInfo->link!='http://')?$imageInfo->link:'javascript:void(0);') . '" target="' . $imageInfo->target . '">' . "\n"
     . '<img src="' . DIR_WS_PORTAL_BANNERS . $imageInfo->image . '" alt="' . $imageInfo->name . '" />' . "\n"
     . '</a>' . "\n"
     . '</li>';
   }
  echo '</ul>';
}
?>
