<!-- Inici Menu Lateral -->
<?php
// Obtenir arbre categories
function tep_get_pages_menu($parent_id = '0', $spacing = '', $expanded = 'true', $pages_list = '') {
  global $language_id, $_GET;

  $pages_query = tep_db_query("select p.id, pd.titol, pd.page_title, pd.link, pd.target, pd.modul, pd.vars, p.parent_id, p.action, p.active, p.listorder from " . TABLE_PAGES . " p left join  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . $language_id . "'where p.parent_id = '" . $parent_id . "' and p.active = '1' order by p.listorder, pd.titol");
  $pages_num = tep_db_num_rows($pages_query);
  if ($pages_num>0) {
    $pages_list .= '<ul ' . (($parent_id==0)?'id="menu_header"':'') . ' ' . (($expanded=='true')?'class="block"':'class="none"') . '>';

    while ($pages = tep_db_fetch_array($pages_query)) {
      $itemInfo = new objectInfo($pages);

      // Busquem quins son fills de la categoria actual
      $childs_array = array();
      $childs_query = tep_db_query("select distinct p.id, p.parent_id, p.listorder from " . TABLE_PAGES . " p where p.parent_id = '" . $itemInfo->id . "' and p.active = '1' order by p.listorder");
	  $childs_num = tep_db_num_rows($childs_query);
	  if ($childs_num>0) {
        while ($childs = tep_db_fetch_array($childs_query)) {
          $childs_array[] = $childs['id'];
        }
      }

      // Definim classe per enllaç, normal/actual + fills:expanded/collapsed
      $expanded = 'false';
      if ($childs_num>0) {
        if (in_array($_GET['pageID'],$childs_array)) {
          $css_class = 'class="expanded ' . (($_GET['pageID']==$itemInfo->id)?'actual':'') . '"';
          $expanded = 'true';
        }
      } else {
        $css_class = 'class="' . (($_GET['pageID']==$itemInfo->id)?'actual':'') . '"';
      }

      switch ($itemInfo->action) {
        case 'link':
  	      $pages_list .= '<li class="' . (($parent_id==0)?' level_top':'') .' elementMenu'.$itemInfo->id.' '. (($_GET['pageID']==$itemInfo->id || in_array($_GET['pageID'],$childs_array))?' selected':'') . '"><span><a href="' . $itemInfo->link . '" target="' . ((tep_not_null($itemInfo->target)) ? $itemInfo->target : '_blank') . '" ' . $css_class . '>' . $spacing . $itemInfo->titol . '</a></span>';
          break;

        case 'modul':
  	      $pages_list .= '<li class="' . (($parent_id==0)?' level_top':'') .' elementMenu'.$itemInfo->id.' '. (($_GET['pageID']==$itemInfo->id || in_array($_GET['pageID'],$childs_array))?' selected':'') . '"><span><a href="' . tep_friendly_url('',$itemInfo->page_title,$itemInfo->vars ) . '" ' . $css_class . '>' . $spacing . $itemInfo->titol . '</a></span>';
          break;

        case 'content':
  	      $pages_list .= '<li class="' . (($parent_id==0)?' level_top':'') .' elementMenu'.$itemInfo->id.' '. (($_GET['pageID']==$itemInfo->id || in_array($_GET['pageID'],$childs_array))?' selected':'') . '"><div><a href="' . tep_friendly_url('',$itemInfo->page_title,'') . '" ' . $css_class . '>' . $spacing . $itemInfo->titol . '</a></div>';
          break;

        default:
  	      $pages_list .= '<li class="' . (($parent_id==0)?' level_top':'') .' elementMenu'.$itemInfo->id.' '. (($_GET['pageID']==$itemInfo->id || in_array($_GET['pageID'],$childs_array))?' selected':'') . '"><div><a href="javascript:void(0);" ' . $css_class . ' rel="nofollow">' . $spacing . $itemInfo->titol . '</a></div>';
          break;
      }
      //$pages_list .= tep_get_pages_menu($pages['id'], $spacing . $spacing, $expanded) . '</li>';
      // Només 1 nivell !!!
      $pages_list .= '</li>';
    }
    $pages_list .= '</ul>';
  }
  return $pages_list;
}

echo tep_get_pages_menu('0');
?>
<!-- Fi Menu Lateral -->
