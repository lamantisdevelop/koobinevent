<?php
$day_text = array('Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte');
$month_text = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre');
// Busquem variables auxiliars: inici i final x buscar mesos amb activitat (inici, fi i interval)
$aux_dates = tep_db_fetch_array(tep_db_query("select  min(a.data_inici)as min_ini, max(a.data_inici) as max_ini, max(a.data_final) as max_final from " . TABLE_ACTIVITATS . " a where a.active = '1'"));
$min_data_array = explode("-", $aux_dates['min_ini']);
$max_data_array = explode("-", ((comparar_dates($aux_dates['max_ini'],$aux_dates['max_final'])>0)?$aux_dates['max_ini']:$aux_dates['max_final']));
// Aux_values
$min_mes = $act_mes = (int)$min_data_array[1];
$min_any = $act_any = (int)$min_data_array[0];
$max_mes = (int)$max_data_array[1];
$max_any = (int)$max_data_array[0];
// Array per mesos
$values_array = array();
// Recorrem mesos
while ($act_any<$max_any || ($act_mes<=$max_mes && $act_any==$max_any)) {
  // Variables auxiliars
  $ini_mes = (int)$act_any . '-' . (int)$act_mes . '-1';
  $fi_mes = (int)(($act_mes!=12)?$act_any:$act_any+1) . '-' . (int)(($act_mes!=12)?$act_mes+1:1) . '-1'; 
  $count = tep_db_fetch_array(tep_db_query(" select count(a.id) as num "
                                         . " from " . TABLE_ACTIVITATS . " a "
                                         . " where a.active = '1' "
                                         . " and a.data_inici IS NOT NULL "
                                         . " and (( TO_DAYS(a.data_inici) >= TO_DAYS('" . $ini_mes . "') "
                                         . " and TO_DAYS(a.data_inici) < TO_DAYS('" . $fi_mes . "')) or ( a.data_final IS NOT NULL and TO_DAYS(a.data_final) >= TO_DAYS('" . $ini_mes . "') "
                                         . " and TO_DAYS(a.data_final) < TO_DAYS('" . $fi_mes . "')) or ( a.data_final IS NOT NULL and TO_DAYS(a.data_inici) <= TO_DAYS('" . $ini_mes . "') "
                                         . " and TO_DAYS(a.data_final) >= TO_DAYS('" . $fi_mes . "'))) "
                                         . " order by a.data_inici asc, a.data_final desc"));
  
  
  
if ($count['num']>0) 
{
    $values_array[] = $act_any . '-' . $act_mes .'-'. $count['num'];
    
}
  
  $act_mes++;
  if ($act_mes>12){
    $act_mes = 1;
    $act_any++;
  }
}
// Netejem i ordenem array
$values_array = array_unique($values_array);
//ordenem array invers segons key (mes)
krsort(($values_array));
// Recorrem array dates
if (sizeof($values_array)>0) {
?>
<!-- Inici Box Noticies Historial -->
<div class="agendaHistorialBloc bloc no_padding">
  <h3 class="color1"><?php echo _('Historial'); ?></h3>
  <ul id="historial_lat">
<?php
$any_aux = date(Y);
foreach ($values_array as $key => $value) 
{
  //
  $val = explode("-", $value);
  //
  if (!isset($any_act)) $any_act=$val[0];
  if ( $val[0]!=$any_act && $item['any']<$any_aux ) {
    $any++;
    if ($any>1) echo '</ul></li>';
    // Links javascript amagar si no any $_GET
    echo '<li><a href="javascript:void(0);" rel="any_' . $val[0] . '" class="history" >' . $val[0] . '</a><ul id="any_' . $val[0] . '" style="' . (($_GET['any']!=$val[0])?"display:none;":"display:block;") . '">';
  }
  echo '<li><a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),'data=' . $val[0] . '-' . $val[1]) . '">' . $month_text[$val[1]-1] . ' ' . $val[0] . ' (' . $val[2] . ')</a></li>';
  $any_act = $val[0];
}
if ($any>0) echo '</ul></li>';
?>
  </ul>
</div>
<!-- Fi Box Noticies Historial -->
<?php
}
?>
