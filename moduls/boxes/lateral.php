<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<div id="lateral_dret" class="lateral">


    <?php
    //Cerquem ID del menu general que te associat aquesta pagina...aixi podem saber si aquest menu te fills o no i mostrar-los on ens interesa
    $id_menu = tep_id_menu_actual_page();
    $parent_menu_id = tep_get_menu_parent_id($id_menu); //obtenim el pare de l'item del menú
    $id_menu = ($parent_menu_id ? $parent_menu_id : $id_menu); //si el pare es diferent de 0 vol dir que som dins un submenú
    $submenu = tep_get_menus_pages_list(4, 'submenu_header', $id_menu, true);
    if ($submenu) {
        ?>

        <div class="bloc no_padding" id="sub_pages">
            <ul class="side-nav">
                <?php tep_print_menu_foundation_sidenav(4, $id_menu, true) ?>
            </ul>
        </div>
    <?php } ?>

    <?php if ($_GET['pageID'] != 9 && LATERAL_BLOC_NOTICIES) { //CAS pagina NOTICIES?>
        <div  class="bloc bottom_9 separat">
            <h2 class="bottom_9 "><?php echo _('Noticies'); ?></h2>

            <?php
// Busquem les ultimes noticies
            $date_aux = date("Y-m-d", time());
            $query_raw = "select n.id, nd.titular, nd.text,n.fotos_groups_id, n.active, n.published, n.finished from " . TABLE_NOTICIES . " n left join " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.language_id = '" . (int) $language_id . "' where (nd.titular!='' and nd.text !='') and n.active = '1' and (n.published IS NULL or TO_DAYS(n.published) <= TO_DAYS('" . $date_aux . "')) and (n.finished IS NULL or TO_DAYS(n.finished) > TO_DAYS('" . $date_aux . "')) and portada=1 order by n.published desc, n.entered desc limit " . MAX_DESTACATS;
            $listing = tep_db_query($query_raw);
            $aux_numrows = tep_db_num_rows($listing);
// Llistat segons busqueda
            while ($item = tep_db_fetch_array($listing)) {
                $itemInfo = new objectInfo($item);
                //busquem foto de portada de galeria
                $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                // Data
                $aux_data = explode('-', $itemInfo->published);
                //traiem hora
                $aux_data2 = explode(' ', $aux_data[2]);
                $text_data = $day_text[date("w", strtotime($itemInfo->published))] . ' ' . $aux_data2[0] . ' ' . $month_text[$aux_data[1] - 1] . ' ' . date("Y", strtotime($itemInfo->published));
                ?>
                <div class="noticia_llistat">
                    <a href="<?php echo tep_friendly_url('', tep_get_page_title('9'), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular); ?>" class="box_noticia">
                        <?php //echo tep_image(DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image'], '', '', '') . "\n"; ?>
                        <span class="titol "><?php echo $itemInfo->titular; ?> </span>
                        <span class="data color2"><?php echo tep_date_short($itemInfo->published); ?> </span>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
    
    
     //***************LATERLAS LLISTATS***************
    //if ($llistatID == 3 || $llistatID == 2) //3=publicacions //Aqui podem filtrer per alguns llistats i altres no
    //if (isset($_GET['llistat']))
    //{
    //    include_once (DIR_WS_BOXES . 'llistat_search.php');
    //    include_once (DIR_WS_BOXES . 'llistat_historial.php');
    //}
    
    
    ?>
    
    
    
    <div class="show-for-medium-up">
        <?php if ($_GET['pageID'] == DEFAULT_PAGE) { //CAS pagina INICI ?>
            <?php
            // banners
            banners_lateral_list(2);
        } else {//resta de pàgines interiors
            // banners
            banners_lateral_list(4);
        }
        ?>
    </div>
    <?php
    if ($llistatID == 2) {//CAS modul llistats productes i dins un grup
        ?>
        <div class="bloc no_padding panel blanc" id="sub_pages">
            <ul class="side-nav">
                <?php echo tep_print_llistats_groups_fills_tree($llistatID); ?>
            </ul>
        </div>
        <?php
        if ($groupID) {//mostrem productes
            $llistats = tep_get_llistats_by_group($groupID);
            if (tep_db_num_rows($llistats)) {
                ?>
                <div class="bloc no_padding panel blanc" id="sub_pages">
                    <ul class="side-nav">
                        <?php
                        $llistats = tep_get_llistats_by_group($groupID);

                        while ($llistat = tep_db_fetch_array($llistats)) 
                        {
                            ?>
                        <li <?php echo($_GET['itemID'] == $llistat['id'] ? 'class="active"' : ''); ?>><a <?php echo($_GET['itemID'] == $llistat['id'] ? 'class="actual"' : ''); ?> href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), tep_get_friendly_params(). 'itemID=' . $llistat['id'] . '&item_title=' . $llistat['titol'] ); ?>"><?php echo $llistat['titol']; ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <?php
            }
        }
        ?>
    
    <?php
}
?>
</div>
