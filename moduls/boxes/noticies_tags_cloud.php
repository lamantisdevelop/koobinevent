<?php
// Busqueda Etiquetes
$tags_query = tep_db_query("select distinct n.id, nd.tags from " . TABLE_NOTICIES . " n left join " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.language_id = '" . $language_id . "' where n.active = 1 order by n.published desc");
$tags_numrows = tep_db_num_rows($tags_query);

// Separem i comptem etiquetes
while ($row = tep_db_fetch_array($tags_query)) {
    $temp_tags = explode(',', $row['tags']);

    foreach ($temp_tags as $tag) {
        // Eliminem espais i passem a min�scules
        $tag = strtolower(trim($tag));
        if (isset($tags[$tag])) {
            $tags[$tag]++;
        } else {
            if (tep_not_null($tag)) {
                $tags[$tag] = 1;
            }
        }
    }
}

if (sizeof($tags) > 0) {

// M�xim i m�nim parcentatge per font en %
    $max_size = 200;
    $min_size = 100;

// Obtenir els valors de repeticions m�xim i m�nim i la seva difer�ncia
    $max_qty = max(array_values($tags));
    $min_qty = min(array_values($tags));
    $spread = $max_qty - $min_qty;
    if ($spread == 0)
        $spread = 1; // No volem dividir per zero

        
// Determinem increment per numero repeticions
    $step = ($max_size - $min_size) / ($spread);

// Si hi ha etiquetes per mostrar creem cadena text
    if (sizeof($tags) > 0) {
        // Ordenem etiquetes per valor (repeticions)
        arsort($tags);
        // Agafem els valors que ens interessa mostrar
        reset($tags);
        $tags = array_slice($tags, 0, MAX_BOX_TAGS);
        // Ordenem etiquetes alfab�ticament
        ksort($tags);

        $content_tags = '';
        foreach ($tags as $key => $value) {
            // Calculem CSS font-size
            $size = $min_size + (($value - $min_qty) * $step);
            // Titol link
            $titol = $value . ' things tagged with ' . $key;
            // Creem link
            $content_tags .= ' <a href="' . tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'tag=' . $key) . '" style="font-size: ' . $size . '%" title="' . $titol . '">' . $key . '</a>&nbsp;';
        }
        ?>
        <!-- Inici Box Tags Cloud -->
        <div class="bloc">
            <h3 class="color1"><?php echo _('Etiquetes'); ?></h3>
            <p class="tags_cloud"><?php echo $content_tags; ?></p>
        </div>
        <!-- Fi Box Tags Cloud -->
        <?php
    }
}
?>
