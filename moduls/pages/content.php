
    <div class="row">

        <?php
        $span_central_home = tep_get_span_central(SITE_LATERAL_ESQUERRE, SITE_LATERAL_DRET);
        ?>

        <div class="<?php echo $span_central_home; ?> columns page page_id_<?php echo $_GET['pageID']; ?>">
            
            <div class="page_content">
                <?php
                /* if (substr($pageInfo->titol, 0, 2) != '00') {//SI EL TITOL DE LA PAGINA COMENÇA AMB 00 NO ES MOSTRARÀ
                  ?>
                  <h1><?php echo($pageInfo->titol); ?></h1>
                  <?php
                  } */
                ?>

                <div class="resum">
                    <?php
                    if (isset($_GET['pageID']) && tep_not_null($_GET['pageID'])) {
                        echo $pageInfo->content;
                    } else {
                        echo TEXT_INTRO;
                    }
                    ?>
                </div>
            </div>
            
            <div class="row">

                <div class="large-12  columns">
                    <?php
                    //mostrar GALERIA si existeix
                    if ($pageInfo->fotos_groups_id) {//CAS té una galeria assignada
                        ?>
                        <div class="">                
                            <ul class="medium-block-grid-3">
                                <?php tep_print_gallery_blocks_flare($pageInfo->fotos_groups_id, 'galeria' . $pageInfo->fotos_groups_id) ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div> 
            
           

        </div>
    </div>
</div>

<?php
//les pàgines només tindran una columna (dreta o esquerra)
if (SITE_LATERAL_ESQUERRE) {
    ?>
    <div class="large-3 large-pull-9 medium-3 medium-pull-9 columns">
        <?php
        include(DIR_WS_BOXES . 'lateral.php');
        ?>
    </div>
    <?php
}

if (SITE_LATERAL_DRET) {
    ?>
    <div class="large-3 columns">
        <?php include(DIR_WS_BOXES . 'lateral.php'); ?>
    </div>
    <?php
}
?>


