<?php
if (isset($_GET['pageID']) && tep_not_null($_GET['pageID'])) 
{
    $meta = tep_get_page_metas($_GET['pageID']);
    $id = $_GET['pageID'];
    $pages_query = tep_db_query(" SELECT p.id, pd.titol, pd.content, pd.title, pd.keywords, pd.description,p.fotos_groups_id  "
                              . " FROM " . TABLE_PAGES . " p "
                              . " LEFT JOIN  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . (int) $language_id . "' "
                              . " WHERE p.id = '" . (int) $id . "' "
                              . " AND p.active=1 "
                              . " LIMIT 1");

    if ($pages = tep_db_fetch_array($pages_query)) 
    {
        $pageInfo = new objectInfo($pages);
        
        //Breadcrumb
        $breadcrumb->add($pageInfo->titol, '');
        
        //METAS
        $title = isset($meta['title']) ? $meta['title'] : $pageInfo->titol;
        $keywords = isset($meta['keywords']) ? $meta['keywords'] : $pageInfo->keywords;
        $description = isset($meta['description']) ? $meta['description'] : $pageInfo->description;
        
        //OG
        $ogTitle = $title;
        $ogDescription  = $description;
        
        
        //TWITTER
        $twitterDescription = $description;
        $twitterTitle = $title;
        
        //META IAMGE -> d emoment nomes la primera, e spot fer el bucle sencer i posarles totes
        $galeriaQuery = tep_get_fotos_galeria($pageInfo->fotos_groups_id);
        if($galeria = tep_db_fetch_array($galeriaQuery))
        {
            $ogImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
            $twitterImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
        }
    } 
    else 
    {
        // Redirecció
        tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
    }
} 
else 
{
    // Redirecció
    tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
}
