<div class="row">
<!--    <div class="large-12 columns">
        <ul class="breadcrumbs"><?php //echo $breadcrumb->trail(); ?></ul>
    </div>-->
    <?php
//les pàgines només tindran una columna (dreta o esquerra)
    $span_central_home = SITE_LATERAL_DRET || SITE_LATERAL_ESQUERRE ? "large-9" : "large-12";
    if (SITE_LATERAL_ESQUERRE) {
        ?>
        <div class="large-3 columns">
            <?php include(DIR_WS_BOXES . 'lateral.php'); ?>
        </div>
        <?php
    }
    ?>

    <div class="<?php echo $span_central_home; ?> columns page page_id_<?php echo $_GET['pageID']; ?>">
        <div class="page_content">

            <h1><?php echo($pageInfo->titol); ?></h1>
            <?php
            echo $pageInfo->content;

// Obtenir arbre categories
            function tep_get_pages_list($parent_id = '0', $spacing = '', $exclude = '', $pages_list = '') {
                global $language_id;

                $pages_query = tep_db_query("select p.id, pd.titol, pd.page_title, pd.link, pd.target, pd.modul, pd.vars, p.parent_id, p.action, p.active,  p.listorder from " . TABLE_PAGES . " p left join  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . $language_id . "'where p.parent_id = '" . $parent_id . "' AND p.active=1 order by p.listorder, pd.titol");
                $pages_num = tep_db_num_rows($pages_query);
                if ($pages_num > 0) {
                    $pages_list .= ( ($parent_id != 0) ? '<ul>' : '<ul id="sitemap">');

                    while ($pages = tep_db_fetch_array($pages_query)) {
                        $itemInfo = new objectInfo($pages);

                        switch ($itemInfo->action) {
                            case 'link':
                                $pages_list .= '<li><a href="' . DIR_WS_DOCUMENT_ROOT . $itemInfo->link . '" target="' . ((tep_not_null($itemInfo->target)) ? $itemInfo->target : '_blank') . '">' . $spacing . $itemInfo->titol . '</a>' . "\n";
                                break;

                            case 'modul':
                                $pages_list .= '<li><a href="' . tep_friendly_url('', $itemInfo->page_title, $itemInfo->vars) . '">' . $spacing . $itemInfo->titol . '</a>';
                                break;

                            case 'content':
                                if ($itemInfo->id != 125)
                                {
                                    $pages_list .= '<li><a href="' . tep_friendly_url('', $itemInfo->page_title, '') . '">' . $spacing . $itemInfo->titol . '</a>' . "\n";
                                }
                                else
                                {
                                    $pages_list .= '<li>' . $spacing . $itemInfo->titol . "\n";
                                }
                                break;

                            default:
                                $pages_list .= '<li>' . $spacing . $itemInfo->titol . "\n";
                                break;
                        }

                        $pages_list .= tep_get_pages_list($pages['id'], $spacing . $spacing) . '</li>';
                    }
                    $pages_list .= '</ul>';
                }
                return $pages_list;
            }

            echo tep_get_pages_list('0');
            ?>

        </div>
    </div>

    <?php if (SITE_LATERAL_DRET) { ?>
        <div class="large-3 columns">
            <?php include(DIR_WS_BOXES . 'lateral.php'); ?>
        </div>
    <?php } ?>
</div>
