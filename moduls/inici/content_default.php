<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */

$span_central_home = tep_get_span_central(HOME_LATERAL_ESQUERRE, HOME_LATERAL_DRET);
?>



        <div class="row">
            <div class="large-12 medium-12 hide-for-small columns">
                <ul class="orbit" data-orbit>
                    <?php echo clients_aleatori_foundation(); //banners home aleaotris ?>
                </ul>
            </div>
            <div class="small-12 hide-for-medium-up columns">
                <ul class="orbit" data-orbit>
                    <?php echo clients_aleatori_foundation(2); //banners home aleaotris ?>
                </ul>
            </div>
        </div>
        
        <div class="separadorBanners"></div>
        
        <div class="row">
            <div class="columns large-8 medium-8 small-12">
                <div  id="actualitat" class="noticies show-for-medium-up">
                    <h2 class="actualitat"><?php echo _('Darreres Notícies de Koobin'); ?></h2>
                    <?php echo tep_print_noticies_llistat_ultimes_by_date_row_inici(date("Y-m-d", time()),"100",true,true); ?> 
                </div>
                <div  id="actualitat2" class="noticies show-for-small">
                    <h2 class="actualitat"><?php echo _('Darreres Notícies de Koobin'); ?></h2>
                    <ul class="small-block-grid-1">
                        <?php echo tep_print_noticies_llistat_ultimes_by_date_block(date("Y-m-d", time()),"150",true); ?> 
                    </ul>
                </div>
            </div>
            <div class="columns large-4 medium-4 small-12">
                <h2 class="actualitat"><?php echo _('Koobin Tweets'); ?></h2>
                <div clasS="twitterTimlineContent">
                    <a class="twitter-timeline"  href="https://twitter.com/koobinevent"  data-widget-id="482051832399659008">Tweets por @koobinevent</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
        </div>
        


