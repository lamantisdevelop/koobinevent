
<div id="footer_content" class="row">
    <div class="large-11 medium-11 small-12 columns">
        <div class="row">
            <div class="columns medium-12">
                <ul>
                    <?php tep_print_menu_foundation_sidenav(6); ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="columns medium-12">
                <img src="/image/IndustriaEnergiaYTurismo.jpg" title="Ministerio de industria, energía y turismo" style="max-width: 250px;margin-top: 1rem;" />
            </div>
        </div>


        
<!--        <div class="panel transparent">
            <div class="row">
                <div id="text" class="large-6 columns">
                    <div class="direccio">
                        <?php
                        //echo _('Text de contacte peu de pagina');
                        ?>
                    </div>
                </div>
                <div class="large-6 columns">
                    <div id="links_footer" class="right">
                        <ul>
                            
                        </ul>
                    </div>                        
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <div id="credits" class="text-right">

                    </div>
                </div>
            </div>
        </div>-->
    </div>
    
    <div class="large-1 medium-1 small-12 columns">
        <a href="http://www.mantis.cat" target="_blank" title="La Mantis - Pàgines web a Girona"><img class="logoMantisPeu" alt="La Mantis - Pàgines web a Girona" src="image/lamantis_logo.png"></a>
    </div>
    
    <div class="clear"></div>

    <div class="hide-for-small-only">
        <div id="toTop" >^</div>
    </div>
    
</div>

