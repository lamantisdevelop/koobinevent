
<div class="row">
    <div id="link_inici" class="large-8 medium-8 small-12  columns">
        <a class="fade" href="<?php echo HTTP_SERVER; ?>" title="<?php echo WEB_NAME; ?>">
            <img src="image/koobin_event.png" alt="<?php echo WEB_NAME; ?>"/>
        </a>
        <div class="aguantaComentariTitol">
            <p class="comentariTitol">SPORT & CULTURAL EVENTS</p><p class="comentariTitolNegre">&nbsp;MANAGEMENT</p>
        </div>
        
    </div>

    <div class="barra_superior large-4 medium-4 columns hide-for-small">

        <?php
        //echo tep_print_menu_foundation_sub_nav(5, 'menu_superior', 0, true); // Menú Catàleg
        ?>

        
        
        <dl id="idiomes" class="hide-for-small"> 
            <dt></dt>
            <?php
            // Idiomes Actius
            $active_languages = tep_get_active_languages();

            for ($i = 0, $n = sizeof($active_languages); $i < $n; $i++) 
            {
                //if ($active_languages[$i]['code'] == "ca" || $active_languages[$i]['code'] == "en")
                {
                    ?>

                    <dd class="<?php echo (($active_languages[$i]['id'] == $language_id) ? 'active' : '') ?>">
                        <a href="<?php echo tep_friendly_url($active_languages[$i]['code'], tep_get_page_title($_GET['pageID'], $active_languages[$i]['id']), tep_get_friendly_params(), 1) ?>" class="<?php (($active_languages[$i]['id'] == $language_id) ? 'active' : '')?>">
                            <?php echo $active_languages[$i]['code']; ?>
                        </a>
                    </dd>
                    <?php
                }
            }
            ?>
        </dl> 
<!--        
       <dl class="socials">
            <dt></dt>
            

            <dd><a href="<?php echo SOCIAL_GOOGLE_PLUS; ?>" title="Google+" target="_blank" class="youtube"><i class="fi-social-google-plus"></i></a></dd>
            <dd><a href="<?php echo SOCIAL_LINKEDIN; ?>" title="LinkedIN" target="_blank" class="youtube"><i class="fi-social-linkedin"></i></a></dd>
            <dd><a href="<?php echo SOCIAL_FACEBOOK; ?>" title="Facebook" target="_blank" class="facebook"><i class="fi-social-facebook"></i></a></dd>
        </dl>-->
    </div>
</div>



