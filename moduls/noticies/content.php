
<?php 
if (!tep_not_null($_GET['itemID'])) 
{
    ?>
    <div class="row">
        <div class="columns large-12 medium-12 small-12">
            <h1 class="titolActualitat"><?php echo(tep_get_page_titol($_GET['pageID'])); ?></h1> 
        </div>
    </div>
    <?php 
}
else
{
    ?>
    <div class="row">
        <div class="columns large-12 medium-12 small-12">
            &nbsp;
        </div>
    </div>
    <?php 
}
?>

<div class="row">
    
    <?php 
    $span_central_home = tep_get_span_central(true, false);
    ?>
    
    <div class="<?php echo $span_central_home; ?> columns page page_id_<?php echo $_GET['pageID']; ?>">
        
        <?php 
        //mostrem la fila de fitxa si cal
        if (tep_not_null($_GET['itemID'])) 
        {
            if (isset($fitxaInfo)) 
            {
                ?>
                <div class="row">
                    <div class="columns large-12">
                        <?php tep_print_noticia_fitxa($fitxaInfo) ?>
                    </div>
                </div>
                <?php 
            }
            ?><h2 class="actualitat"><?php echo _('Altres Notícies de Koobin'); ?></h2><?php
        }
        
        
        echo tep_print_noticies_llistat_ultimes_by_date_row_inici(date("Y-m-d", time()),"150",false,false);
        ?>
    </div>
    
    <?php
    //les pàgines només tindran una columna (dreta o esquerra)
    
    if (true) 
    {
        ?>
        <div class="large-3 large-pull-9 medium-3 medium-pull-9 columns">
            <?php 
            //include(DIR_WS_BOXES . 'lateral.php'); 
            require_once(DIR_WS_BOXES . 'noticies_search.php');
            require_once(DIR_WS_BOXES . 'noticies_historial.php');
            //require_once(DIR_WS_BOXES . 'noticies_tags_cloud.php'); //per defecte de moment fora
            ?>
        </div>
        <?php
    }
    ?>
    
    <?php 
    if (false) 
    { 
        ?>
        <div class="large-3 medium-3 columns">
            <?php 
            //include(DIR_WS_BOXES . 'lateral.php'); 
            require_once(DIR_WS_BOXES . 'noticies_search.php');
            require_once(DIR_WS_BOXES . 'noticies_historial.php');
            //require_once(DIR_WS_BOXES . 'noticies_tags_cloud.php'); //per defecte de moment fora
            ?>
            
        </div>
        <?php 
    } 
    ?>
    
</div>


