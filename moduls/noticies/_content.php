<div class="row"> 
    <div class="large-12 columns">
        <ul class="breadcrumbs"><?php echo $breadcrumb->trail(); ?></ul>
    </div>
    <?php
    //les pàgines només tindran una columna (dreta o esquerra)
    $span_central_home = SITE_LATERAL_DRET || SITE_LATERAL_ESQUERRE ? "large-9" : "large-12";

    if (SITE_LATERAL_ESQUERRE) {
        ?>
        <div class="large-3 columns">
            <?php
            include(DIR_WS_BOXES . 'lateral.php');
            require_once(DIR_WS_BOXES . 'noticies_search.php');
            require_once(DIR_WS_BOXES . 'noticies_historial.php');
            require_once(DIR_WS_BOXES . 'noticies_tags_cloud.php');
            ?>
        </div>
        <?php
    }
    ?>
    <div class="<?php echo $span_central_home; ?> columns noticies">

        <?php
        /* Breadcrumb */
        //echo '<div id="breadcrumb">' . $breadcrumb->trail(BREADCRUMB_SEP) . '</div>';
// Content
        if (tep_not_null($_GET['itemID'])) {

            if (isset($fitxaInfo)) {

                //mostrar GALERIA si existeix 
                if ($fitxaInfo->fotos_groups_id) {//CAS té una galeria assignada
                    $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($fitxaInfo->fotos_groups_id));

                    $image = tep_get_fotos_galeria($fitxaInfo->fotos_groups_id);
                    $gal_img = '';
                    //$image = tep_db_query("select ni.id, ni.image, nid.titol from " . TABLE_NOTICIES_IMAGES . " ni left join " . TABLE_NOTICIES_IMAGES_DESCRIPTION . " nid on ni.id = nid.images_id and nid.language_id = '" . (int) $language_id . "' where ni.noticies_id ='" . (int) $fitxaInfo->id . "' order by ni.listorder");
                    $image_numrows = tep_db_num_rows($image);
                    if ($image_numrows > 0) {
                        $i = 0;
                        $gal_img = '<div class="row" ><ul class="clearing-thumbs clearing-feature" data-clearing>';
                        while ($item = tep_db_fetch_array($image)) {
                            $gal_img .= '<li  ' . (($i == 0) ? 'class="clearing-featured-img"' : '') . '><a href="' . DIR_WS_PORTAL_IMAGE_FOTOS . $item['image'] . '" title="' . $item['titol'] . '"><img class="large-12 columns thumb" src="' . DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $item['image'] . '" data-caption="' . $item['titol'] . ($item['autor'] ? ' | &copy; ' . $item['autor'] : '' ) . '" /></a></li>' . "\n";
                            $i++;
                        }
                        $gal_img .= '</ul></div>';
                    }
                }
                ?>
                <div class="noticia fitxa">
                    <div class="img_gal">
                        <?php echo $gal_img; ?>
                    </div>
                    <div class="info_aux">
                        <h1 class="titol capital"><?php echo $fitxaInfo->titular; ?></h1>
                        <p class="data color0"><?php echo tep_date_short($fitxaInfo->published); ?></p>
                        <div class="resum">
                            <?php echo $fitxaInfo->text; ?>
                        </div>
                        <?php
                        echo ((tep_not_null($fitxaInfo->download) && is_file(DIR_WS_PORTAL_DOWNLOADS . $fitxaInfo->download)) ? '<p id="download">' . TEXT_DOWNLOAD . ' <a href="' . DIR_WS_PORTAL_DOWNLOADS . $fitxaInfo->download . '" target="_blank">' . $fitxaInfo->download . ' (' . formatBytes(filesize(DIR_WS_PORTAL_DOWNLOADS . $fitxaInfo->download)) . ')</a></p>' : '');

                        //galeria de fotos
                        //SELECT i.id,i.image, i.listorder, id.titol from noticies_images i, noticies_images_description id where id.images_id=i.id and i.noticies_id=1 and id.language_id=1
                        //$galeriaQuery = tep_db_query("SELECT i.id,i.image, i.listorder, id.titol FROM " . TABLE_NOTICIES_IMAGES . " i ," . TABLE_NOTICIES_IMAGES_DESCRIPTION . " id where id.images_id=i.id and id.language_id= '" . (int) $language_id . "' and i.noticies_id= '" . (int) $fitxaInfo->id . "'");
                        $galeriaQuery = tep_get_fotos_galeria($fitxaInfo->fotos_groups_id);
                        $galeria_numrows = tep_db_num_rows($galeriaQuery);
                        $next_aux = '';
                        if ($galeria_numrows == -1234) {//Mostrar galeria de fotos al final de la noticia ACTUALMENT DESACTIVAT
                            //$next_aux = '<p class="next"><a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),'itemID=' . $next['id'] . '&item_title=' . $next['titular'] ) . '" ><b>' . TEXT_NEXT . '</b><span class="data">' . tep_date_short($next['published']) . '</span><span class="titol">'  . $next['titular'] . '</span></a></p>';
                            while ($galeria = tep_db_fetch_array($galeriaQuery)) {
                                $fotoInfo = new objectInfo($galeria);
                                ?>
                                <a href="<?php echo (DIR_WS_PORTAL_IMAGE_FOTOS . $fotoInfo->image); ?>" title="<?php echo($fotoInfo->titol); ?>" rel="shadowbox[galeria]">
                                    <img class="fotos_noticia" alt="<?php echo($fotoInfo->name); ?>" src="<?php echo (DIR_WS_PORTAL_IMAGE_FOTOS . $fotoInfo->image); ?>"/>
                                </a>
                                <?php
                            }
                        }
                        ?>
                        <div class="clear"></div>
                        <div class="social">
                            <iframe id="facebook" src="http://www.facebook.com/plugins/like.php?href=<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $fitxaInfo->id . '&item_title=' . $fitxaInfo->titular); ?>&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=recommend&amp;colorscheme=light&amp;font&amp;height=35" scrolling="no" frameborder="0"></iframe>
                            <a id="twitter" href="https://twitter.com/share" class="twitter-share-button">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
                        </div>
                        <?php
                    } else {
                        // No Item
                        echo '<div class="no_items">' . _('No hi ha elements') . '</div>';
                    }
                    ?>
                </div>
                <?php
            }
            //LLISTAT DE NOTÍCIES (es mostra sempre, o sol o a sota de la fitxa

            if (is_null($_GET['itemID'])) {//cas llistat sol (NO FITXA)
                $mostrar_primer_destacat = FALSE;
                ?>
                <h1 class="actualitat"><? echo(tep_get_page_titol($_GET['pageID'])); ?></h1>
                <?php
            }

            $listing_sql = "select n.id,n.fotos_groups_id, nd.titular, nd.destacat, nd.text, n.active, n.published, n.entered from " . TABLE_NOTICIES . " n left join " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.titular<>'' and nd.language_id = '" . (int) $language_id . "' ";
            if (tep_not_null($_GET['mes']) && tep_not_null($_GET['any'])) {
                $where_str = " where (TO_DAYS(NOW()) - TO_DAYS(n.published) >= 0) and (n.finished IS NULL or TO_DAYS(n.finished) - TO_DAYS(NOW()) >= 0) and n.active='1'  and nd.text !='' and MONTH(n.published) = '" . (int) $_GET['mes'] . "' and YEAR(n.published) = '" . (int) $_GET['any'] . "' order by n.published desc, n.id desc";
                $mostrar_primer_destacat = false;
            } else if (tep_not_null($_GET['tag'])) {
                // Canviem guions per espais
                $_GET['tag'] = tep_db_input(str_replace("-", " ", $_GET['tag']));
                $where_str = " where ( LOWER(nd.tags) REGEXP '[\,]{1}[ \n]*" . $_GET['tag'] . "$' or  LOWER(nd.tags) REGEXP '^" . $_GET['tag'] . "[ \n]*[\,]{1}' or  LOWER(nd.tags) REGEXP '[\,]{1}[ \n]*" . $_GET['tag'] . "[ \n]*[\,]{1}' ) and n.active='1'  and nd.text !='' order by n.published desc, n.id desc";
                $mostrar_primer_destacat = false;
            } else if (tep_not_null($_GET['news-search'])) {
                $mostrar_primer_destacat = false;
                // Condicions
                $keyword_str = "";
                if (tep_parse_search_string(stripslashes($_GET['news-search']), $search_keyword)) {
                    $keyword_str .= " and (";
                    $size = sizeof($search_keyword);
                    $aux_search = 0;
                    for ($i = 0; $i < $size; $i++) { //Per cada paraula creem la cadena
                        $keyword = $search_keyword[$i];
                        switch ($keyword) {
                            case '(':
                            case ')':
                            case 'and':
                            case 'or':
                                $keyword_str .= " " . $keyword . " ";
                                $aux_search = 0;
                                break;
                            default:
                                $aux_search++;
                                if ($aux_search > 1)
                                    $keyword_str .= " and ";
                                $keyword_str .= "(nd.titular LIKE '%" . addslashes($keyword) . "%' or nd.titular REGEXP '[^&]" . addslashes($keyword) . "' or nd.text LIKE '%" . addslashes($keyword) . "%' or nd.text REGEXP '[^&]" . addslashes($keyword) . "'  )";
                                break;
                        }
                    }
                    $keyword_str .= ") ";
                    $where_str = " where nd.text !='' " . $keyword_str . " and (TO_DAYS(NOW()) - TO_DAYS(n.published) >= 0) and (n.finished IS NULL or TO_DAYS(n.finished) - TO_DAYS(NOW()) >= 0) and n.active='1'  and nd.text !='' order by n.published desc, n.id desc";
                }
            } else {
                $where_str = " where (TO_DAYS(NOW()) - TO_DAYS(n.published) >= 0) and (n.finished IS NULL or TO_DAYS(n.finished) - TO_DAYS(NOW()) >= 0) and n.active='1'  and nd.text !='' order by n.published desc, n.id desc";
            }
            $listing_sql = $listing_sql . $where_str;
            $listing = tep_db_query($listing_sql);
            $listing_numrows = tep_db_num_rows($listing);
            $page_numrows = tep_db_num_rows($listing);

            if ($listing_numrows > 0) {
                // Llistat segons búsqueda
                $i = 0;
                while ($item = tep_db_fetch_array($listing)) {
                    if ($item['id'] != $_GET['itemID']) {
                        $i++;
                        $aux = ($aux % 2) + 1;
                        $itemInfo = new objectInfo($item);

                        // Ressaltar resultats búsuqeda
                        // Text busqueda titol
                        $fldname = $itemInfo->titular;

                        // Text busqueda text
                        $fldtext = tep_text_curt($itemInfo->text, ($mostrar_primer_destacat && $i == 1) ? '150' : '150'); //$itemInfo->descripcio;
                        if (isset($_GET['news-search']) && tep_not_null($_GET['news-search'])) {

                            if ($i == 1) {//mostrem text buscat només una vegada
                                $_GET['news-search'] = urldecode($_GET['news-search']);
                                echo('<h2>' . $_GET['news-search'] . '</h2>');
                            }
                            $fldtext = html_entity_decode($fldtext);

                            $string_to_explode = $_GET['news-search']; // is result LOWERCASE ?
                            $tok = preg_split('~ ~', $string_to_explode);
                            for ($k = 0; $k < sizeof($tok); $k++) {
                                // minuscules
                                $fldtext = preg_replace("~" . strtolower($tok[$k]) . "(?!([^<]+)?>)~", '<span class="search_str">' . strtolower($tok[$k]) . '</span>', $fldtext);
                                // majúscules
                                $fldtext = preg_replace("~" . strtoupper($tok[$k]) . "(?!([^<]+)?>)~", '<span class="search_str">' . strtoupper($tok[$k]) . '</span>', $fldtext);
                                // 1º majuscula
                                $fldtext = preg_replace("~" . ucwords($tok[$k]) . "(?!([^<]+)?>)~", '<span class="search_str">' . ucwords($tok[$k]) . '</span>', $fldtext);
                            }
                        }
                        $fldtext;

                        $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                        ?>
                       <div class="noticia row <?php echo (($mostrar_primer_destacat && $i == 1) ? ' primer ' : ''); ?>">
                        <div class="large-12 columns">
                            <div class="fons">
                                <div class="row">
                                    <a class="large-4 columns" href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular); ?>">
                                        <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs2' . (($mostrar_primer_destacat && $i == 1) ? '3' : '') . '/' . $portada['image']; ?>" alt="<?php echo $fldname; ?>"/>
                                    </a>
                                    <div class="sumari large-8 columns">
                                        <div class="">
                                            <h3>
                                                <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular); ?>">
                                                    <?php echo $fldname; ?>
                                                </a>
                                            </h3>
                                            <span class="data"><?php echo tep_date_short($itemInfo->published); ?></span>
                                            <p class="desc "><?php echo $fldtext; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                }
            } else {
                // No Items
                echo '<div class="no_items fons_opac">' . _('No hi ha elements') . '</div>';
            }
            ?>

        </div>
        <?php if (SITE_LATERAL_DRET) { ?>
            <div class="large-4">
                <?php
                include(DIR_WS_BOXES . 'lateral.php');
                require_once(DIR_WS_BOXES . 'noticies_search.php');
                require_once(DIR_WS_BOXES . 'noticies_historial.php');
                require_once(DIR_WS_BOXES . 'noticies_tags_cloud.php');
                ?>
            </div>
        <?php } ?>

    </div>
</div>