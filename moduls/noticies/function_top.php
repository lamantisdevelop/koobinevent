<?php
// Accions
if ($_GET['action']) 
{
    switch ($_GET['action']) 
    {
        case 'send':
            // Substituim caràcters
            //$search = stripslashes($_POST['news_search']);
            $search = utf8_decode($_POST['news_search']);  
            //$search =str_replace('&','',$search);
            //$search =str_replace('[','',$search);
            //$search =str_replace(']','',$search);
            // Redirecció
            tep_redirect(tep_friendly_url('',tep_get_page_title($_GET['pageID']),'news-search=' . $search));
        break;
    }
}

// Si tenim ID pàgina busquem info pagina de noticia
if (isset($_GET['pageID']) && tep_not_null($_GET['pageID'])) 
{
    $meta = tep_get_page_metas($_GET['pageID']);
    $id = $_GET['pageID'];
    $pages_query = tep_db_query(" SELECT p.id, pd.titol, pd.content, pd.title, pd.keywords, pd.description,p.fotos_groups_id  "
                              . " FROM " . TABLE_PAGES . " p "
                              . " LEFT JOIN  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . (int) $language_id . "' "
                              . " WHERE p.id = '" . (int) $id . "' "
                              . " AND p.active=1 "
                              . " LIMIT 1");
    
    if ($pages = tep_db_fetch_array($pages_query)) 
    {
        $pageInfo = new objectInfo($pages);

        //METAS
        $title = isset($meta['title']) ? $meta['title'] : $pageInfo->titol;
        $keywords = '';//isset($meta['keywords']) ? $meta['keywords'] : $pageInfo->keywords;
        $description = isset($meta['description']) ? $meta['description'] : $pageInfo->description;
        
        //en aquest cas donem valor despres del itemID
        //OG
        //TWITTER
        
        //META IAMGE -> d emoment nomes la primera, e spot fer el bucle sencer i posarles totes
        $galeriaQuery = tep_get_fotos_galeria($pageInfo->fotos_groups_id);
        if($galeria = tep_db_fetch_array($galeriaQuery))
        {
            $ogImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
            $twitterImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
        }
    } 
    else 
    {
        // Redirecció
        tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
    }

    //PER LA FITXA: en alguns casos sobreescrivim les meta dades de la pagina
    if (tep_not_null($_GET['itemID'])) 
    {
        $breadcrumb->add($meta['titol'], tep_friendly_url('',tep_get_page_title($_GET['pageID'])));
        
        // Augmentem visualitzacions noticia
        tep_db_query("update " . TABLE_NOTICIES_DESCRIPTION . " set viewed = viewed+1 where noticies_id = '" . (int)$_GET['itemID'] . "' and language_id = '" . $language_id . "'");

        // Obtenim informació x fitxa i altres: mapa!!!
        $fitxa = tep_db_query("SELECT n.id,n.fotos_groups_id, nd.titular, nd.destacat, nd.text, n.active, n.published, n.finished, n.modified "
                            . "FROM " . TABLE_NOTICIES . " n "
                            . "LEFT JOIN " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.language_id = '" . (int)$language_id .  "' "
                            . "WHERE n.id ='" . (int)$_GET['itemID'] . "' "
                            . "AND nd.text != '' "
                            . "AND n.active = 1 "
                            . "ORDER BY n.entered desc");

        $fitxa_numrows = tep_db_num_rows($fitxa);

        if ($fitxa_numrows>0)
        {
            $fitxa = tep_db_fetch_array($fitxa);
            $fitxaInfo = new objectInfo($fitxa);   

            // Titol, desc i keys
            $title = $fitxaInfo->titular;
            $description = tep_text_curt($fitxaInfo->text, 160);
            $keywords = '';//tep_text_curt($fitxaInfo->text, 160);
            
            $galeriaQuery = tep_get_fotos_galeria($fitxaInfo->fotos_groups_id);
            if($galeria = tep_db_fetch_array($galeriaQuery))
            {
                $ogImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
                $twitterImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
            }

            //Canonical
            //$canonical = tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $fitxaInfo->id . '&item_title=' . $fitxaInfo->titular);

            // Breadcrumb
            $breadcrumb->add($fitxaInfo->titular, '');
        }
    }  
    else if (tep_not_null($_GET['tag'])) 
    {
        $title = $meta['title'] . ' ' . str_replace("-", " ", $_GET['tag']);
    } 
    else if (tep_not_null($_GET['news-search'])) 
    {
        $title = $meta['title'] . ' - ' . $_GET['news-search']; //. ': ' . utf8_encode(str_replace("-", " ", $_GET['news-search']));
    } 
    else if (tep_not_null($_GET['any']) && tep_not_null($_GET['mes'])) 
    {
        $title = $meta['title'] . ' ' . $month_text[$_GET['mes']-1] . ' ' . $_GET['any'];
    } 
    else 
    {
        $breadcrumb->add($pageInfo->titol, '');
        $title = $meta['title'];
    }
    
    //OG
    $ogTitle = $title;
    $ogDescription  = $description;
    
    //TWITTER
    $twitterDescription = $description;
    $twitterTitle = $title;
} 
else 
{
    // Redirecció
    tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
}


// Metas
//$meta = tep_get_page_metas($_GET['pageID']);
//$keywords = $meta['keywords'] . (tep_not_null(_('META Keywords')) ? ',' . _('META Keywords') : '');
//$description = $meta['description'] . ' ' . (tep_not_null(_('META Description')) ? ' ' . _('META Description') : '');

//Canonical
//$canonical = tep_friendly_url('', tep_get_page_title($_GET['pageID']), '');



?>
