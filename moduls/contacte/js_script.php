<?php
/*
  JavascriptScript específic pel mòdul
 */
?>

<script type="text/javascript" >
    //<![CDATA[

    function strip_tags(str, allowed_tags) {
        var key = '', tag = '', allowed = false;
        var matches = allowed_array = [];

        var replacer = function(search, replace, str) {
            return str.split(search).join(replace);
        };
        // Build allowes tags associative array
        if (allowed_tags) {
            allowed_array = allowed_tags.match(/([a-zA-Z]+)/gi);
        }
        str += '';
        matches = str.match(/(<\/?[^>]+>)/gi); // Match tags

        for (key in matches) {
            if (isNaN(key)) { // IE7 Hack
                continue;
            }
            html = matches[key].toString();// Save HTML tag
            allowed = false;// Is tag not in allowed list? Remove from str!

            for (k in allowed_array) {
                allowed_tag = allowed_array[k];
                i = -1;

                if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+'>');}
                if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+' ');}
                if (i != 0) { i = html.toLowerCase().indexOf('</'+allowed_tag)   ;}

                if (i == 0) {
                    allowed = true;
                    break;
                }
            }
            if (!allowed) {
                str = replacer(html, "", str); // Custom replace. No regexing
            }
        }
        return str;
    }

    function trim(string) {
        string = strip_tags(string ,''); // Eliminem etiquetes html
        return string.replace(/\t/g, '').replace(/\r/g, '').replace(/\n/g, ''); // Eliminem salts de linea
        return string.replace(/^[\s]+|[\s]+$/g, ''); // Eliminem espais a principi i final
    }

    function check_form() {

            return true;
        
    }

    function delete_form() {
        document.form_data.name.value = '';
        document.form_data.mail.value = '';
        document.form_data.subject.value = '';
        document.form_data.message.value = '';
    }
    
    
    function EnviaContacte()
    {
        $(".avisEnviant").css( "display", "block" );
        $(".avisEnviant").css( "visibility", "visible" );
        $("#btnSend").attr("disabled", true);
        $("#btnReset").attr("disabled", true);
        $("#form_data").submit();
    }
    
    function load() {

        var latlng = new google.maps.LatLng(<?php echo GOOGLE_LAT; ?>,<?php echo GOOGLE_LON; ?>);
        var myOptions = {
            zoom: <?php echo GOOGLE_ZOOM; ?>,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.<?php echo GOOGLE_MAP_TYPE; ?>
        };
        var map = new google.maps.Map(document.getElementById("google_map"),
        myOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo GOOGLE_LAT; ?>,<?php echo GOOGLE_LON; ?>),
            map: map
        });


    }
    
    

    load(); // carreguem Google Maps
    
    
    //]]>
</script>

<script>
        
    </script>
