<?php

// Metas
//$meta = tep_get_page_metas($_GET['pageID']);
//$title = $meta['title'] . ' - ' . _('META Title');
//$keywords = $meta['keywords'] . _('META Keywords');
//$description = $meta['description'] . ' ' . _('META Description');


if (isset($_GET['pageID']) && tep_not_null($_GET['pageID']))
{
    $meta = tep_get_page_metas($_GET['pageID']);
    $id = $_GET['pageID'];
    $pages_query = tep_db_query(" SELECT p.id, pd.titol, pd.content, pd.title, pd.keywords, pd.description,p.fotos_groups_id  "
            . " FROM " . TABLE_PAGES . " p "
            . " LEFT JOIN  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . (int) $language_id . "' "
            . " WHERE p.id = '" . (int) $id . "' "
            . " AND p.active=1 "
            . " LIMIT 1");

    if ($pages = tep_db_fetch_array($pages_query))
    {
        $pageInfo = new objectInfo($pages);

        //Breadcrumb
        $breadcrumb->add($pageInfo->titol, '');

        //METAS
        $title = isset($meta['title']) ? $meta['title'] : $pageInfo->titol;
        $keywords = isset($meta['keywords']) ? $meta['keywords'] : $pageInfo->keywords;
        $description = isset($meta['description']) ? $meta['description'] : $pageInfo->description;

        //OG
        $ogTitle = $title;
        $ogDescription = $description;


        //TWITTER
        $twitterDescription = $description;
        $twitterTitle = $title;

        //META IAMGE -> d emoment nomes la primera, e spot fer el bucle sencer i posarles totes
        $galeriaQuery = tep_get_fotos_galeria($pageInfo->fotos_groups_id);
        if ($galeria = tep_db_fetch_array($galeriaQuery))
        {
            $ogImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
            $twitterImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
        }
    }
    else
    {
        // Redirecció
        tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
    }
}
else
{
    // Redirecció
    tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
}



// Breadcrumb
//$breadcrumb->add($meta['titol'], '');
// Classes per mail
require(DIR_WS_CLASSES . 'class.phpmailer.php');
require(DIR_WS_CLASSES . 'class.smtp.php');
require('missatge.php');

// Breadcrumb
//$breadcrumb->add($meta['titol'], tep_friendly_url('', tep_get_page_title($_GET['pageID'])));
// Accions
if ($_GET['action'])
{
    switch ($_GET['action'])
    {
        case 'send':

            //inicialitzem capcha
            include_once DIR_WS_CLASSES . 'captcha/securimage.php';
            $securimage = new Securimage();
            //$securimage->session_name = 'comsubID';
            if ($securimage->check($_POST['captcha_code']) == false)
            {
                // the code was incorrect
                // Creem variable sessió
                if (!tep_session_is_registered('contact'))
                {
                    tep_session_register('contact');
                }
                $contact = array('name' => tep_db_prepare_input($_POST['name']),
                                 'mail' => tep_db_prepare_input($_POST['mail']),
                                 'tel' => tep_db_prepare_input($_POST['tel']),
                                 'message' => tep_db_prepare_input($_POST['message']),
                                 'carrec' => tep_db_prepare_input($_POST['empresa']),
                                 'send' => 'now()');
                //tep_session_close(); // Guardar variables sessió
                
                // Redirecció
                $url = tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'action=errorcaptcha');
                if (($_GET['cPath']))
                {
                    //cas formulari incrustat dins de categories de cataleg modifiquem url perquè torni a la categoria actual
                    $cPath_new = tep_get_category_name($_GET['cPath']) . "-" . $_GET['cPath'];
                    $url = tep_friendly_url('', tep_get_page_title(4), 'cPath=' . $cPath_new . '&action=errorcaptcha');
                }
                tep_redirect($url);
            }

            if ($_POST)
            {
                // Assumpte
                $subject = WEB_NAME . ' - Formulari Web (' . date('d-m-Y, H:m:s') . ')';
                
                // Creem missatge
                // Tenim la plantilla del missatge a missatge.php
                
                //$message += pintaMissatge();
                
                //$message = file_get_contents('http://cms.lamantisproves.net/moduls/contacte/missatge.php');

                $message = '<p>FORMULARI CONTACTE ENVIAT DES DEL WEB ' . WEB_NAME . '</p>'
                         . '<b>' . _("Nom i Cognoms") . '</b> ' . stripslashes($_POST['name']) . '<br/>'
                         . '<b>' . _("Empresa") . '</b> ' . stripslashes($_POST['empresa']) . '<br/>'
                         . '<b>' . _("Email") . '</b> ' . stripslashes($_POST['mail']) . '<br/>'
                         . '<b>' . _("Telefon") . '</b> ' . stripslashes($_POST['tel']) . '<br/>'
                         . '<b>' . _('Missatge') . '</b><br/>' . nl2br(stripslashes($_POST['message']));

                // Guardem dades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                                        'mail' => tep_db_prepare_input($_POST['mail']),
                                        'carrec' => tep_db_prepare_input($_POST['empresa']),
                                        'tel' => tep_db_prepare_input($_POST['tel']),
                                        'message' => nl2br(stripslashes($_POST['message'])),
                                        'send' => 'now()');

                tep_db_perform(TABLE_MAIL_PREMSA, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Enviem mail
                $mail = new PHPMailer();
                $mail->CharSet = "UTF-8";
                $mail->IsHTML(true);

                $mail->SetLanguage('ca', 'admin/includes/classes/language/');
                $mail->PluginDir = 'admin/includes/classes/';
                $mail->IsSMTP();
                $mail->Mailer = "smtp";
                $mail->Host = SMTP_SERVER;
                $mail->Port = 25;
                $mail->SMTPAuth = true;
                $mail->Username = SMTP_USERNAME;
                $mail->Password = SMTP_PASSWORD;
                $mail->From = MAIL_FROM; // Mail
                $mail->FromName = MAIL_FROM_NAME; // Nom
                $mail->AddReplyTo($_POST['mail'], $_POST['name']);
                //$mail->AddAddress(MAIL_FROM, MAIL_FROM_NAME); // A qui l'enviem
                //$mail->AddAddress("xavi@mantis.cat", MAIL_FROM_NAME); // A qui l'enviem
                $mail->AddAddress("jaume@mantis.cat", MAIL_FROM_NAME); // A qui l'enviem

                $mail->Subject = $subject; // Titol
                $mail->Body = $message;
                $mail->WordWrap = 100;

                if ($mail->Send())
                {
                    // Eliminem variable sessió
                    tep_session_unregister('contact');
                    
                    // Vector entrades
                    $sql_data_array = array('result' => 'success');
                    
                    // Crida base dades
                    tep_db_perform(TABLE_MAIL_PREMSA, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
                    
                    // Redirecció
                    //$url =  HTTP_SERVER . tep_friendly_url('',tep_get_page_title($_GET['pageID']),'action=success');
                    $url = tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'action=success');
                    if (($_GET['cPath']))
                    {
                        //cas formulari incrustat dins de categories de cataleg modifiquem url perquè torni a la categoria actual
                        $cPath_new = tep_get_category_name($_GET['cPath']) . "-" . $_GET['cPath'];
                        $url = tep_friendly_url('', tep_get_page_title(4), 'cPath=' . $cPath_new . '&action=success');
                    }
                    tep_redirect($url);
                }
                else
                {   
                    //die($mail->ErrorInfo);
                    // Creem variable sessió
                    if (!tep_session_is_registered('contact'))
                    {
                        tep_session_register('contact');
                    }
                    
                    $contact = array('name' => tep_db_prepare_input($_POST['name'] . ' ' . $_POST['surname']),
                                     'mail' => tep_db_prepare_input($_POST['mail']),
                                     'message' => tep_db_prepare_input($_POST['message']));
                    
                    // Vector entrades
                    $sql_data_array = array('result' => 'error');
                    
                    // Crida base dades
                    tep_db_perform(TABLE_MAIL_PREMSA, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
                    
                    // Redirecció
                    tep_session_close(); // Guardar variables sessió
                    $url = HTTP_SERVER . tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'action=error');
                    tep_redirect($url);
                }
            }
            break;
    }
}
?>
