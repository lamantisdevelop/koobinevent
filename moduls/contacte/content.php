<div class="row contacte formulari">
    
    <?php 
    $span_central_home = tep_get_span_central(SITE_LATERAL_ESQUERRE, SITE_LATERAL_DRET);
    ?>
    
    <div class="<?php echo $span_central_home; ?> columns">

        <div class="page_content">
            <h1><?php echo(tep_get_page_titol($_GET['pageID'])); ?></h1>
            
            <?php pintaMissatgeResultatEnviar(); ?>
            
<!--            <div class="row">
                <div class="columns large-12 medium-12 small-12">
                    <img class="imgContact" src="../../image/koobin_contact.jpg">
                </div>
            </div> -->
            
            <div class="row">
                <div class="columns large-12 medium-12 small-12">
                    <p class="introFormulari">
                        <?php 
                            echo _('Intro formulari');
                        ?>
                    </p>
                </div>
            </div>
            
            <div class="row">
                
                <div class="columns large-7 medium-7 small-12">
                    <div class="aguantaAdreca">
                        <?php echo $pageInfo->content; ?>
                        
                        
                    </div>
                </div>
                
                <div class="columns large-5 medium-5 small-12">
                    <div class="aguantaAdreca">
                     <!--   <a class="btnContacte button" href="mailto:info@koobin.com" target="_blank"><?php echo _("Envia un correu") ?> </a> -->
                        <a class="btnContacte button" href="https://www.google.es/maps/place/KoobinEvent,+S.L./@41.967128,2.836183,17z/data=!3m1!4b1!4m2!3m1!1s0x12bae6b58ac9d52d:0x8565704a8bbecc50" target="_blank"><?php echo _("Localització") ?></a>
<!--                        <a class="btnContacte button" href="https://www.google.es/maps/dir//KoobinEvent,+S.L.,+Parc+Cient%C3%ADfic+i+Tecnol%C3%B2gic+UdG,+edifici+Centre+d%27Empreses+-+Pic+de+Peguera,+11,+17003+Gerona/@41.967128,2.836183,17z/data=!4m13!1m4!3m3!1s0x12bae6b58ac9d52d:0x8565704a8bbecc50!2sKoobinEvent,+S.L.!3b1!4m7!1m0!1m5!1m1!1s0x12bae6b58ac9d52d:0x8565704a8bbecc50!2m2!1d2.836183!2d41.967128" target="_blank"><?php echo _("Com Arribar a Koobin en cotxe"); ?></a>
                    </div> -->
<!--                    <form name="form_data" id="form_data" class="contacte" action="<?php //echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'action=send' . (($current_category_id) ? '&cPath=' . tep_get_category_name($current_category_id) . "-" . $current_category_id : '')); ?>" method="post" accept-charset="UTF-8">-->

                        <!--                <div class="contaceIntro">
                        <?php //?>
                                        </div>

                                        <div class="contaceAvisProtect">
                        <?php //echo _('avis formulari contacte protect');?>
                                        </div>

                                        <div class="contaceAvis">
                        <?php //echo _('avis formulari contacte');?>
                                        </div>-->

<!--                        <fieldset>
                            <div class="row">
                                <div class="large-6 columns">
                                    <input type="text" name="name" id="name" required class="span6" value="<?php //echo $contact['name']; ?>" placeholder="<?php //echo _('Nom i cognoms'); ?>..."/>
                                    <input type="text" name="empresa" required class=" span6" id="empresa" value="<?php //echo $contact['carrec']; ?>"  placeholder="<?php //echo _('Empresa'); ?>..." />
                                </div>
                                <div class="large-6 columns">
                                    <input type="email" name="mail" required class="span6" id="mail" value="<?php //echo $contact['mail']; ?>" placeholder="<?php //echo _('Email'); ?>..." />
                                    <input type="number" name="tel" required class=" span6" id="tel" value="<?php //echo $contact['tel']; ?>" placeholder="<?php //echo _('Telefon'); ?>..." />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-6 columns">
                                    <textarea name="message" id="comments"class="content span6" placeholder="<?php //echo _('Missatge'); ?>..."><?php //echo $contact['message']; ?></textarea>
                                </div>
                                <div class="large-6 columns">
                                    <img id="captcha" src="<?php //echo DIR_WS_CLASSES; ?>captcha/securimage_show.php" alt="CAPTCHA Image" />	
                                    <a id="nova_imatge" class="button tiny no-fade" href="#" onclick="document.getElementById('captcha').src = '<?php //echo DIR_WS_CLASSES; ?>captcha/securimage_show.php?' + Math.random();
                                            return false"><?php //echo _("Nova imatge"); ?></a>	
                                    <input type="text" class="span6" required name="captcha_code" size="10" maxlength="5" placeholder="<?php //echo _('Escriu el codi de la imatge'); ?>..."/>
                                    <?php
                                    //if ($_GET['action'] == 'errorcaptcha')
                                    //{
                                    //    echo '<div class="error">' . _('Error formulari captcha') . '</div>';
                                    //}
                                    ?>
                                </div>
                            </div>
                        </fieldset>-->

<!--                        <p class="botonera top_10">
                            <button id="btnSend" class="btn btn-danger" type="submit" ><?php //echo _('Enviar'); ?></button>
                            <button id="btnReset" class="btn btn-danger" type="reset" ><?php //echo _('Esborrar'); ?></button>
                        </p>-->

                        <div class='avisEnviant'>
                            <img src="../../image/loading_linia.gif" alt="carregant"></img><?php echo _("El formulari s'està enviant, un moment si us plau..."); ?> 
                        </div>

                    </form>
                    
                </div>
            </div>
            
<!--            <div class="row">
                <div class="columns large-5 medium-5 small-12">
                    
                </div>
                
                <div class="columns large-7 medium-7 small-12">
                    <div class="contacteAvisProtect">
                        <?php //echo _('avis formulari contacte protect'); ?>
                    </div>
                </div>
            </div>-->

            <?php pintaMissatgeResultatEnviar(); ?>
            
        </div>
    </div>
    
    
    
    <?php
    //les pàgines només tindran una columna (dreta o esquerra)
    if (SITE_LATERAL_ESQUERRE) 
    {
        ?>
        <div class="large-3 large-pull-9 medium-3 medium-pull-9 columns">
            <div class="panel blanc ombra">
                <?php echo $pageInfo->content; ?>
            </div>
            <?php 
            include(DIR_WS_BOXES . 'lateral.php'); 
            ?>
        </div>
        <?php
    }
    
    if (SITE_LATERAL_DRET) 
    { 
        ?>
        <div class="large-3 columns">
            <?php include(DIR_WS_BOXES . 'lateral.php'); ?>
        </div>
        <?php 
    } 
    ?>
    
</div>
