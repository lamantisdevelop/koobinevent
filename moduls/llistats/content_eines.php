<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */


/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * FITXA-----------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
if (tep_not_null($_GET['itemID'])) {
    ?>
    <div class="large-9 large-push-3 columns fitxa_llistat llistat_<?php echo $_GET['llistat']; ?>">
        <?php
        tep_llistats_augmentar_visualitzacions((int) $_GET['itemID']);

        // Fitxa

        $listing = tep_get_llistat_by_id($_GET['itemID'], false); //obtenim info del llistat

        $query_numrows = tep_db_num_rows($listing);
        $te_mapa_simple = true; //indiquem a js que ha de generar google maps
        if ($query_numrows > 0) {
            $item = tep_db_fetch_array($listing);
            $itemInfo = new objectInfo($item);
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));

            if ($itemInfo->titol) {
                ?>
                <h1 class = "titol"><?php echo $itemInfo->titol; ?></h1>
                <?php
            }
            ?>
            <?php
            if ($itemInfo->titol2) {
                ?>
                <h2 class = "titol2 subheader"><?php echo $itemInfo->titol2; ?></h2>
                <?php
            }
            ?>
            <div class = "continent_fitxa">
                <?php if (!is_null($portada)) {
                    ?>
                    <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" class = "imatge" />
                <?php } ?>
                <div class = "camps">
                    <div class="titulars">
                        <?php
                        if ($itemInfo->titol3) {
                            ?>
                            <h3 class ="titol3"><?php echo $itemInfo->titol3; ?></h3>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="info_extra">
                        <?php
                        if ($itemInfo->autor) {
                            ?>
                            <div class="autor"><?php echo $itemInfo->autor; ?></div>
                            <?php
                        }

                        if ($itemInfo->published != '0000-00-00 00:00:00') {
                            ?>
                            <div class="data"><?php echo date('d/m/Y', strtotime($itemInfo->published)); ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="descripcions">
                        <div class="desc"><?php echo $itemInfo->descripcio; ?></div>
                        <div class="desc2"><?php echo $itemInfo->descripcio2; ?></div>
                        <div class="desc3"><?php echo $itemInfo->descripcio3; ?></div>
                    </div>
                    <div class="generics">
                        <?php
                        if ($itemInfo->generic1) {
                            ?>
                            <div class="generic1"><?php echo $itemInfo->generic1; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic2) {
                            ?>
                            <div class="generic2"><?php echo $itemInfo->generic2; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic3) {
                            ?>
                            <div class="generic3"><?php echo $itemInfo->generic3; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic4) {
                            ?>
                            <div class="generic4"><?php echo $itemInfo->generic4; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic5) {
                            ?>
                            <div class="generic5"><?php echo $itemInfo->generic5; ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if ($itemInfo->link) {
                        ?>
                        <a class="boto link" target="_blank" href="<?php echo $itemInfo->link; ?>"><?php echo $itemInfo->link; ?></a>
                        <?php
                    }
                    ?>
                </div>
                <?php
            } else {
                echo '<p class="no_item">' . _('Element no trobat') . '</p>';
            }
            //obtenim tots els projectes relacionats amb l'eina
            $llistats = tep_get_llistats_relacionats_by_group_and_related($_GET['itemID'], 5, false);
            $llistat_numrows = tep_db_num_rows($llistats);
            if ($llistat_numrows) {//si hi ha algun projecte relacionat
                ?>
                <h3><?php echo _('Projectes relacionats'); ?></h3>
                <ul class="grid small-block-grid-2 medium-block-grid-2 large-block-grid-4">
                    <?php
                    //Recorrem tots els ids de projectes relacionats
                    while ($llistat = tep_db_fetch_array($llistats)) {
                        $llistat = tep_get_llistat_by_id($llistat['llistats_id']); //obtenim info del projecte
                        $itemInfo = new objectInfo(tep_db_fetch_array($llistat));
                        $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                        $pagina_desti = ($itemInfo->group_id=='7')?'97':'98';//mirem a pagina a redirigir depenent si grafic o web
                        ?>
                        <li class="item_llistat llistat_7">
                            <?php if (is_array($portada)) { ?>
                                <a href="<?php echo tep_friendly_url('', tep_get_page_title($pagina_desti), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->titol; ?>">
                                    <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" class = "imatge" />
                                </a>
                            <?php } ?>

                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            }
            ?>
        </div>

        <?php
    } else {

        echo($pageInfo->content);
    }
    ?>
</div>

<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * LLISTAT---------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
$llistats = tep_get_llistats_by_group(5);
?>
<div class="llistat large-3 large-pull-9 columns">
    <ul class="large-block-grid-1">
        <?php
        //Recorrem tots els elements del llistat
        while ($llistat = tep_db_fetch_array($llistats)) {
            $itemInfo = new objectInfo($llistat);
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
            ?>
            <li class="item_llistat llistat_5">
                <?php if (is_array($portada)) { ?>
                    <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->titol; ?>">
                        <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" class = "imatge" />
                    </a>
                <?php } ?>

            </li>
            <?php
        }
        ?>
    </ul>
</div>
<?php
/* } else {
  // No Items
  echo NO_RESULTS;
  } */
?>
