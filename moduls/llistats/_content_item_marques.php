<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
//echo($_GET['pageID']);
?>

<!--<h2><?php //echo(tep_get_page_titol($_GET['pageID'])); ?></h2>-->
<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * FITXA-----------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
if (tep_not_null($_GET['itemID'])) {
    ?>
    <div class="fitxa_llistat llistat_<?php echo $_GET['llistat']; ?>">
        <?php
        tep_llistats_augmentar_visualitzacions((int) $_GET['itemID']);

        // Fitxa

        $listing = tep_get_llistat_by_id($_GET['itemID'], false); //obtenim info del llistat

        $query_numrows = tep_db_num_rows($listing);
        $te_mapa_simple = true; //indiquem a js que ha de generar google maps
        if ($query_numrows > 0) {
            $item = tep_db_fetch_array($listing);
            $itemInfo = new objectInfo($item);
            
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
            $active = false;
            $active_content =false;
            ?>
            <div class="continent_fitxa">

                
                <?php banners_slide_orbit_by_galery_primeraNO($itemInfo->fotos_groups_id,0); ?>
                
                
                <div class="camps">
                    
                    <div class="pestanyes">
                        
                        <dl class="tabs" data-tab> 
                            <?php 
                            if (!empty($itemInfo->descripcio))
                            {
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-1"><?php echo(_('Paquet')) ?></a></dd> 
                                <?php 
                                $active = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio2))
                            { 
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-2"><?php echo(_('Granel')) ?></a></dd> 
                                <?php 
                                $active = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio3))
                            { 
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-3">1X1</a></dd> 
                                <?php 
                                $active = true;
                            } 
                             
                            if (!empty($itemInfo->descripcio4))
                            { 
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-4">TUPPER</a></dd> 
                                <?php 
                                $active = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio7))
                            {
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-5"><?php echo $itemInfo->descTitol1 ?></a></dd> 
                                <?php 
                                $active = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio8))
                            { 
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-6"><?php echo $itemInfo->descTitol2 ?></a></dd> 
                                <?php 
                                $active = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio9))
                            { 
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-7"><?php echo $itemInfo->descTitol3 ?></a></dd> 
                                <?php 
                                $active = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio10))
                            { 
                                ?>
                                <dd class="<?php echo (!$active ? 'active':'') ?>"><a href="#panel2-8"><?php echo $itemInfo->descTitol4 ?></a></dd> 
                                <?php 
                                $active = true;
                            } 
                            
                           
                            ?>
                        </dl> 
                        
                        <div class="tabs-content tabs-content-pfp"> 
                            
                            <?php 
                             //paquet
                            if (!empty($itemInfo->descripcio))
                            { 
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-1"> 
                                    <p><?php echo $itemInfo->descripcio; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            } 
                            
                            //granel
                            if (!empty($itemInfo->descripcio2))
                            {
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-2"> 
                                    <p><?php echo $itemInfo->descripcio2; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            } 
                            
                            //1x1
                            if (!empty($itemInfo->descripcio3))
                            {
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-3"> 
                                    <p><?php echo $itemInfo->descripcio3; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            } 
                            
                            //tupper
                            if (!empty($itemInfo->descripcio4))
                            {
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-4"> 
                                    <p><?php echo $itemInfo->descripcio4; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            }
                            
                            if (!empty($itemInfo->descripcio7))
                            {
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-5"> 
                                    <p><?php echo $itemInfo->descripcio7; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio8))
                            {
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-6"> 
                                    <p><?php echo $itemInfo->descripcio8; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio9))
                            {
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-7"> 
                                    <p><?php echo $itemInfo->descripcio9; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            } 
                            
                            if (!empty($itemInfo->descripcio10))
                            {
                                ?>
                                <div class="content <?php echo (!$active_content ? 'active':'') ?>" id="panel2-8"> 
                                    <p><?php echo $itemInfo->descripcio10; ?></p> 
                                </div> 
                                <?php 
                                $active_content = true;
                            } 
                            
                           
                            ?>
                            
                        </div>
                        
                    </div>
                    
                    <div class="descripcions">
                        
                        <?php if (!empty($itemInfo->descripcio5)){?>
                            <div class="desc resumFitxa">
                                <?php echo $itemInfo->descripcio5; ?>
                            </div>
                        <?php } ?>
                        
                        <?php if (!empty($itemInfo->descripcio6)){?>
                            <div class="foliTOP"></div>
                            <div class="resum2">
                                <div class="foli">
                                    <?php echo $itemInfo->descripcio6; ?>
                                </div>
                            </div>
                            <div class="foliBottom"></div>
                        <?php } ?>
                        
                    </div>
                    
                </div>
            <?php
            } 
            else 
            {
                echo '<p class="no_item">' . _('Element no trobat') . '</p>';
            }
            ?>
        </div>

    <?php 
    
            }
    ?>


<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * LLISTAT AMB ---------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
?>

<div class="llistat">
<?php

    //$llistats = tep_get_llistats_by_group($_GET['llistat']);
    if (isset($_GET['groupID'])) $llistats = tep_get_llistats_by_group($_GET['groupID']);
    else if (isset($_GET['llistat'])) $llistats = tep_get_llistats_by_group($_GET['llistat']);

    $llistat_numrows = tep_db_num_rows($llistats);

    if ($llistat_numrows > 0)
    {
        //Recorrem tots els elements del llistat NO ESPECIALITATS
        $i = 0;
        ?><div class='row'><?php
        while ($llistat = tep_db_fetch_array($llistats)) 
        {
            $itemInfo = new objectInfo($llistat);
            if ($itemInfo->inscripcions == 0 )
            {

                if ($itemInfo->id != $_GET['itemID']) 
                {
                    ?>
                        <div class="columns large-6 medium-6">
                            <?php
                                pintaBannerLlista($itemInfo);
                            ?>
                        </div>
                     
                        <?php 
                        if ($i%2 == 1)
                        {
                            ?>
                            </div>
                            <div class='row'>
                            <?php
                        } 
                        ?>
                    <?php
                    $i++;
                }
            }
        }
        
        echo '</div>';
        
        //posem l' index de la query a 0
        tep_db_data_seek($llistats, 0);

        //ESPECIALITATS
        $i = 0;
        $z = 0;
        $grup = new objectInfo(tep_get_llistat_group_by_id($_GET['groupID']));
        while ($llistat = tep_db_fetch_array($llistats)) 
        {
            $itemInfo = new objectInfo($llistat);
            if ($itemInfo->inscripcions == 1 )
            {
                //$portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                
                if ($z == 0)
                {
                    $z++;
                    ?>
                    <div class='row'>
                        <div class="columns large-12">
                            <div class="llistaEspecialitats">
                                
                                <?php
                                
                                if(strpos($grup->title,_('Especialitats')) !== false )echo (str_replace(_('Especialitats'),'',$grup->title ));
                                else echo (_('Especialitats'));
                                
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class='row'>     
                    <?php
                }
                
                if ($itemInfo->id != $_GET['itemID']) 
                {
                    
                    ?>
                    
                        <div class="columns large-6 medium-6">
                            <?php
                                pintaBannerLlista($itemInfo);
                            ?>
                        </div>
                     
                        <?php 
                        if ($i%2 == 1)
                        {
                            ?>
                            </div>
                            <div class='row'>
                            <?php
                        } 
                        ?>
                    <?php
                    $i++;
                }
            }
        }
        ?></div><?php
    }
    
    function pintaBannerLlista($itemInfo)
    {
        ?>
        <div class="bannerInici">
            <div class="capceleraBanner2">
                <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->title; ?>">
                    <?php echo($itemInfo->titol); ?>
                </a>
            </div>
            <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->title; ?>">
                <?php 
                $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                if (!is_null($portada)) { ?>
                    <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" class = "imatge large-12 medium-12 small-12" />
                <?php } ?>
            </a>
        </div>    
        <?php
    }
    ?>
</div>

<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * LLISTAT GRUPS---------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */

if (!isset($_GET['itemID']))   
{
    if (isset($_GET['groupID'])) $result = tep_get_llistat_groups_by_parent_id($_GET['groupID']);
    else if (isset($_GET['llistat'])) $result = tep_get_llistat_groups_by_parent_id($_GET['llistat']);

    if (tep_db_num_rows($result))
    {
        
        ?>
        <ul class="llistat_grups large-block-grid-3">
            <?php
            while ($llistat = tep_db_fetch_array($result)) 
            {
                //1er
                $itemInfo = new objectInfo($llistat);
                
                if (isset($_GET['groupID']))
                {
                    ?>
                    <li class="grup">

                        <a href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'groupID=' . $itemInfo->id . '&group_title=' . $itemInfo->title . '&modul=llistats' ); ?>">
                            <?php
                            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                            if (!is_null($portada)) {
                                ?>
                                <img class="imatge"  src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" />
                                <?php
                            }
                            ?>
                        </a>

                    </li>
                    <?php
                }
                else
                {
                    ?>
                    <li class="grup">

                        <a href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'groupID=' . $itemInfo->id . '&group_title=' . $itemInfo->title . '&modul=llistats' ); ?>">
                            <?php
                            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                            if (!is_null($portada)) {
                                ?>
                                <img class="imatge"  src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" />
                                <?php
                            }
                            ?>
                        </a>

                    </li>
                    <?php
                }
            }
            ?>
        </ul>
        <?php
    }
}  

function pintaBanner($itemInfo)
{
    if (isset($_GET['groupID']))
    {
        pintaBannerProducte($itemInfo);
    }
    else if (isset($_GET['llistat'])) 
    {
        pintaBannerMarca($itemInfo);
    }
}

function pintaBannerMarca ($itemInfo)
{
    ?>
    <div class="bannerMarca">
        <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'groupID=' . $itemInfo->id . '&group_title=' . $itemInfo->title);?>">
            <?php 
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
            if (!is_null($portada)) { ?>
                <img class="imatge imatgeMarca large-12 medium-12 small-12"  src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" />
            <?php } ?>
        </a>
    </div>    
    <?php
}

function pintaBannerProducte ($itemInfo)
{
    ?>
    <div class="bannerInici">
        <div class="capceleraBanner">
            <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'groupID=' . $itemInfo->id . '&group_title=' . $itemInfo->title);?>">
                <?php echo($itemInfo->title); ?>
            </a>
        </div>
        <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'groupID=' . $itemInfo->id . '&group_title=' . $itemInfo->title);?>">
            <?php 
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
            if (!is_null($portada)) { ?>
                <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" class = "imatge large-12 medium-12 small-12" />
            <?php } ?>
        </a>
    </div>
    <?php
}

echo ('</div>');

?>

