<?php

// Metas
//$meta = tep_get_page_metas($pageID);
//$title = $meta['title'] . ' | ' . _('META Title');
//$keywords = $meta['keywords'] . (tep_not_null(_('META Keywords')) ? ',' . _('META Keywords') : '');
//$description = $meta['description'] . ' ' . (tep_not_null(_('META Description')) ? ' ' . _('META Description') : '');


//evitem injeccions via get
$pageID = addslashes($_GET['pageID']);
$itemID = addslashes($_GET['itemID']);
$parentID = addslashes($_GET['parentID']);
$llistatID = addslashes($_GET['llistat']);
$groupID = addslashes($_GET['groupID']);

// Breadcrumb
if ($parentID != '0') 
{
    $breadcrumb->add(tep_get_page_titol($parentID), tep_friendly_url('', tep_get_page_title($parentID)));
}

// Si tenim ID pàgina busquem info pagina
if (isset($pageID) && tep_not_null($pageID)) 
{
    //aqui podem estirar tot l' arbre per posar al bread crumbs
    $breadcrumb->add(tep_get_page_titol($pageID), tep_friendly_url('', tep_get_page_title($pageID), ''));
    
    $id = $pageID;
    $pages_query = tep_db_query(" SELECT p.id, pd.titol, pd.content, pd.title, pd.keywords, pd.description  "
                              . " FROM " . TABLE_PAGES . " p "
                              . " LEFT JOIN  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . (int) $language_id . "' "
                              . " WHERE p.id = '" . (int) $id . "' "
                              . " AND p.active=1 limit 1");
                      
    if ($pages = tep_db_fetch_array($pages_query)) 
    {
        $pageInfo = new objectInfo($pages);
//        
//        //METAS
//        $title = isset($meta['title']) ? $meta['title'] : $pageInfo->titol;
//        $keywords = isset($meta['keywords']) ? $meta['keywords'] : $pageInfo->keywords;
//        $description = isset($meta['description']) ? $meta['description'] : $pageInfo->description;
    }
//    
    if (isset($_GET['itemID']) && tep_not_null($_GET['itemID'])) 
    {
        //METAS ITEM FITXA
        $query =  tep_get_llistat_by_id($_GET['itemID']);
        $item = tep_db_fetch_array($query);
        
        // Titol
        $title = $item['titol']; //. (($meta['title'] && $meta['title'] != ''  ) ? (' - ' . $meta['title']) : '');
        $description = tep_text_curt($item['descripcio'], 160);
        $keywords = ''; //tep_text_curt($item['descripcio'], 160);
        
        // Breadcrumb
        $breadcrumb->add($item['titol'], '');
        
        $galeriaQuery = tep_get_fotos_galeria($item['fotos_groups_id']);
        if($galeria = tep_db_fetch_array($galeriaQuery))
        {
            $ogImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
            $twitterImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
        }
    } 
    else 
    {   
        //METAS LLISTAT / GRUP
        if ($groupID){$query = tep_get_llistat_group_by_id($groupID);}
        else if (isset($llistatID)){$query = tep_get_llistat_group_by_id($llistatID);}
        
        //titol
        $title = $query['title']; //. (($meta['title'] && $meta['title'] != ''  ) ? (' - ' . $meta['title']) : '');
        $description = tep_text_curt($query['description'], 160);
        $keywords = '';//  tep_text_curt($query['description'], 160);
        
        // Breadcrumb
        $breadcrumb->add($query['titol'], '');
        
        $galeriaQuery = tep_get_fotos_galeria($query['fotos_groups_id']);
        if($galeria = tep_db_fetch_array($galeriaQuery))
        {
            $ogImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
            $twitterImage = HTTP_SERVER . DIR_WS_PORTAL_IMAGE_FOTOS . $galeria['image'];
        }
    }
    
    //OG
    $ogTitle = $title;
    $ogDescription  = $description;

    //TWITTER
    $twitterDescription = $description;
    $twitterTitle = $title;
} 
else 
{
    // Redirecció
    tep_redirect(tep_friendly_url('', tep_get_page_title(DEFAULT_PAGE), ''));
}

?>
