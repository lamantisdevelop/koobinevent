<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>

<div class="llistat">
    
    <?php
//    echo '<br />PRODUCTES<br />';
    
    if (isset($_GET['groupID']))
    {
        $llistats = tep_get_llistats_by_group($_GET['groupID']);
        
    }
    else if(isset($_GET['llistat']))
    {
        $llistats = tep_get_llistats_by_group($_GET['llistat']);
         
    }
    
    ?>
    <div class="resum">
        <?php
        if (isset($_GET['pageID']) && tep_not_null($_GET['pageID'])) 
        {
            echo $pageInfo->content;
        } 
        else 
        {
            echo TEXT_INTRO;
        }
        ?>
    </div>
    <?php

    if (tep_db_num_rows($llistats)) 
    {
        
        
        //Recorrem tots els elements del llistat
        ?>
        <ul class="large-block-grid-1 medium-block-grid-1 small-block-grid-1">
            <?php
            $i=0;
            while ($llistat = tep_db_fetch_array($llistats)) 
            {
                $itemInfo = new objectInfo($llistat);
                $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));

                if ($itemInfo->id != $itemID) 
                {//no mostrem item actual en cas de dins de fitxa
                    ?>
                    <li class="item">
                        <div class="item_llistat llistat_<?php echo $llistatID; ?>">
                            <div class="camps<?php echo( ($i%2)==0 ? '0' : '' ); ?>">
                                <h4 class="titol">
                                    <a href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol . '&modul=llistats') ; ?>" title="<?php echo $itemInfo->titol; ?>">
                                        <?php echo $itemInfo->titol; ?>
                                    </a>
                                </h4>
                                <div class="sumari">
                                    <?php echo $itemInfo->descripcio; ?>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php
                    $i++;
                }
            }
            ?>
        </ul>
        <?php 
    }
    ?>
</div>