<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>

<h1><?php echo(tep_get_page_titol($_GET['pageID'])); ?></h1>
<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * FITXA-----------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
if (tep_not_null($_GET['itemID'])) {
    ?>
    <div class="fitxa_llistat llistat_<?php echo $_GET['llistat']; ?>">
        <?php
        tep_llistats_augmentar_visualitzacions((int) $_GET['itemID']);

        // Fitxa

        $listing = tep_get_llistat_by_id($_GET['itemID'], false); //obtenim info del llistat

        $query_numrows = tep_db_num_rows($listing);
        $te_mapa_simple = true; //indiquem a js que ha de generar google maps
        if ($query_numrows > 0) {
            $item = tep_db_fetch_array($listing);
            $itemInfo = new objectInfo($item);
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
            ?>
            <div class="continent_fitxa">
                <?php if (!is_null($portada)) { ?>
                    <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" class = "imatge" />
                <?php } ?>
                <div class = "camps">
                    <div class="titulars">
                        <?php
                        if ($itemInfo->autor) {
                            ?>
                            <h1 class = "titol"><?php echo $itemInfo->titol; ?></h1>
                            <?php
                        }
                        if ($itemInfo->titol2) {
                            ?>
                            <h2 class = "titol2"><?php echo $itemInfo->titol2; ?></h2>
                            <?php
                        }
                        if ($itemInfo->titol3) {
                            ?>
                            <h3 class ="titol3"><?php echo $itemInfo->titol3; ?></h3>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="info_extra">
                        <?php
                        if ($itemInfo->autor) {
                            ?>
                            <div class="autor"><?php echo $itemInfo->autor; ?></div>
                            <?php
                        }

                        if ($itemInfo->published != '0000-00-00 00:00:00') {
                            ?>
                            <div class="data"><?php echo date('d/m/Y', strtotime($itemInfo->published)); ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="descripcions">
                        <div class="desc"><?php echo $itemInfo->descripcio; ?></div>
                        <div class="desc2"><?php echo $itemInfo->descripcio2; ?></div>
                        <div class="desc3"><?php echo $itemInfo->descripcio3; ?></div>
                    </div>
                    <div class="generics">
                        <?php
                        if ($itemInfo->generic1) {
                            ?>
                            <div class="generic1"><?php echo $itemInfo->generic1; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic2) {
                            ?>
                            <div class="generic2"><?php echo $itemInfo->generic2; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic3) {
                            ?>
                            <div class="generic3"><?php echo $itemInfo->generic3; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic4) {
                            ?>
                            <div class="generic4"><?php echo $itemInfo->generic4; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic5) {
                            ?>
                            <div class="generic5"><?php echo $itemInfo->generic5; ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if ($itemInfo->link) {
                        ?>
                        <a class="boto link" target="_blank" href="<?php echo $itemInfo->link; ?>"><?php echo $itemInfo->link; ?></a>
                        <?php
                    }
                    ?>
                </div>
                <?php
            } else {
                echo '<p class="no_item">' . _('Element no trobat') . '</p>';
            }
            ?>
        </div>

    <?php }
    ?>
</div>

<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * LLISTAT---------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
?>

<ul class="llistat large-block-grid-3">
    <?php
    $grups = tep_get_llistat_groups_by_parent_id($_GET['llistat']);
    while ($grup = tep_db_fetch_array($grups)) {
        ?>
        <li >
            <h3><?php echo $grup['name']; ?></h3>
            <?php
            //obtenim els llistats del grup
            $llistats = tep_get_llistats_by_group($grup['id']);
            //Recorrem tots els elements del llistat
            while ($llistat = tep_db_fetch_array($llistats)) {
                $itemInfo = new objectInfo($llistat);
                $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                if ($itemInfo->id != $_GET['itemID']) {//no mostrem item actual en cas de dins de fitxa
                    ?>
                    <div class="item_llistat llistat_<?php echo $_GET['llistat']; ?>">
                        <?php if (is_array($portada)) { ?>
                            <a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->titol; ?>">
                                <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" class = "imatge" />
                            </a>
                        <?php } ?>
                        <div class="camps">
                            <h6 class="titol">

                                <?php echo $itemInfo->titol; ?>

                            </h6>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </li>
        <?php
    }
    ?>
</ul>
<?php
/* } else {
  // No Items
  echo NO_RESULTS;
  } */
?>
