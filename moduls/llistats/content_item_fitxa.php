<?php
if (tep_not_null($_GET['itemID'])) 
{
    ?>
    <div class="fitxa_llistat llistat_<?php echo $_GET['llistat']; ?>">
        <?php
        tep_llistats_augmentar_visualitzacions((int) $_GET['itemID']);

        $listing = tep_get_llistat_by_id($_GET['itemID'], false); //obtenim info del llistat

        $query_numrows = tep_db_num_rows($listing);
        $te_mapa_simple = true; //indiquem a js que ha de generar google maps
        
        if ($query_numrows > 0) 
        {
            $item = tep_db_fetch_array($listing);
            $itemInfo = new objectInfo($item);
            //$portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
            ?>
            <div class="continent_fitxa">
             
                    
                <div class = "camps">
                    <div class="titulars">
                        <?php
                        if ($itemInfo->titol) 
                        {
                            ?>
                            <h1 class = "titol"><?php echo $itemInfo->titol; ?></h1>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="descripcions">
                        <div class="desc2"><?php echo $itemInfo->descripcio2; ?></div>
                    </div>
                    
                    <div>
                        <a class="button" href="<?php echo (tep_friendly_url('',  tep_get_page_title(3)));//3=contact page ?>" target="_blank"><?php echo _('Envia\'ns el CV'); ?></a>
                    </div>

                    <div class="social">
                        <iframe id="facebook" src="http://www.facebook.com/plugins/like.php?href=<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=recommend&amp;colorscheme=light&amp;font&amp;height=35" scrolling="no" frameborder="0"></iframe>
                        <a id="twitter" href="https://twitter.com/share" class="twitter-share-button">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
                    </div> 
                </div>
            </div>
            <?php
        } 
        else 
        {
            echo '<p class="no_item">' . _('Element no trobat') . '</p>';
        }
        ?>
    </div>
    <?php 
}
?>