<?php 

//imprimim les categories de primer nivell de clients, amb els seus productes dins
//esta pensat per a nivells i subnivells fixos.


if (isset($_GET['groupID']))
{
    $result = tep_get_llistat_groups_by_parent_id($_GET['groupID']);
}
else if(isset($_GET['llistat']))
{
     $result = tep_get_llistat_groups_by_parent_id($_GET['llistat']);
}


?>
<div class="resum">
    <?php
    if (isset($_GET['pageID']) && tep_not_null($_GET['pageID'])) 
    {
        echo $pageInfo->content;
    } 
    else 
    {
        echo TEXT_INTRO;
    }
    ?>
</div>
<?php    

if (tep_db_num_rows($result)) 
{
    ?>
    
    <ul class="llistat_grups large-block-grid-1 medium-block-grid-1 small-block-grid-1">
        <?php
        tep_db_data_seek($result, 0);
        
        while ($llistat = tep_db_fetch_array($result)) //CATEGORIES
        {
           
            $itemInfo = new objectInfo($llistat);
             if (($_GET['pageID'] == 134 && $itemInfo->id == 2) || 
                 ($_GET['pageID'] == 135 && $itemInfo->id == 3) ||
                 ($_GET['pageID'] == 135 && $itemInfo->id == 7) || 
                 ($_GET['pageID'] == 136 && $itemInfo->id == 9) || 
                 ($_GET['pageID'] == 137 && $itemInfo->id == 10) ||
                 ($_GET['pageID'] == 138 && $itemInfo->id == 11)
                )
             {
            ?>
            <li class="grup grup_pare grup_<?php echo $itemInfo->id ?>">
                
                <?php
               
                ?>
                
                <?php 
                $llistats = tep_get_llistats_by_group($itemInfo->id);
                if (tep_db_num_rows($llistats)) 
                {
                    if($_GET['pageID'] != 134)
                    {    
                        ?>
                        <h2> <?php echo $itemInfo->title; ?></h2>
                        <?php 
                    }
                    
                    //Recorrem tots els elements del llistat
                    ?>
                    <ul class="large-block-grid-4 medium-block-grid-4 small-block-grid-2">
                        <?php
                        while ($llistat = tep_db_fetch_array($llistats)) //PRODUCTES
                        {
                            $itemInfoIntern = new objectInfo($llistat);
                            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfoIntern->fotos_groups_id));

                            ?>
                            <li>
                                <div class="item_llistat llistat_<?php echo $llistatID; ?>">
                                    <?php 
                                    if (is_array($portada)) 
                                    { 
                                        ?>
                                        <a href="<?php echo $itemInfoIntern->link; ?>" title="<?php echo $itemInfoIntern->titol; ?>" target="_blank">
                                            <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image']; ?>" alt = "<?php echo $itemInfoIntern->titol; ?>" class = "imatge" />
                                        </a>
                                        <?php 
                                    } 
                                    else
                                    {
                                        echo $itemInfoIntern->titol; 
                                    }
                                    ?>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php 
                }
                ?>
                
            </li>
            <?php
             }
        }
        ?>
    </ul>
    <?php
}
?>
