<?php
/*
  JavascriptScript específic pel mòdul
 */
?>

<script type="text/javascript" language="javascript">
    function load() {
        var latlng = new google.maps.LatLng(<?php echo ($te_mapa_simple) ? $itemInfo->lat : '41.74211494855704'; ?>,<?php echo ($te_mapa_simple) ? $itemInfo->lon : '2.1062507294118404'; ?>);
        var myOptions = {
            zoom: <?php echo ($te_mapa_simple) ? '14' : '8'; //si estem dins fitxa fem zoom mes gran   ?>,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        var map = new google.maps.Map(document.getElementById("google_map"),
        myOptions);
<?php
if ($te_mapa_simple) {//cas mostrar un unic punt al mapa
    ?>
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo ($itemInfo->lat) ? $itemInfo->lat : '41.92935747337991'; ?>,<?php echo ($itemInfo->lon) ? $itemInfo->lon : '2.253879555501044'; ?>),
                    map: map
                });

    <?php
}
?>
<?php
if ($te_mapa_multiple) {// cas mostrar multiples punts al mapa
    $listing = tep_db_query($listing_sql); //agafa el select que s'hagi creat dins del content del mòdul
    ?>
                var markers = new Array();
                var infoWindows= new Array();
    <?php
    $i = 0;
    while ($item = tep_db_fetch_array($listing)) {
        $itemInfo = new objectInfo($item);
        ?>
                        //creem el marker
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(<?php echo ($itemInfo->lat) ? $itemInfo->lat : '41.92935747337991'; ?>,<?php echo ($itemInfo->lon) ? $itemInfo->lon : '2.253879555501044'; ?>),
                            title:'<?php echo $itemInfo->nom; ?>',
                            map: map
                        });
                        //posem marker en array per a poder acumular multiples
                        markers.push(marker);
                                                                                                                                                
                        //creem contingut info window
                        var contentString = '<div class="infoWindow">'                            
                            +'<a href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->nom); ?>">'
                            +'<b class="titol"><?php echo $itemInfo->nom; ?></b>'
                            +'</a>'
                            +'</div>';
                        //creem info window
                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });
                        //afegim infoWindow en array per a poder acumular multiples 
                        infoWindows.push(infowindow);
                                                                                                                
                        //creem event on click
                        google.maps.event.addListener(markers[<?php echo $i; ?>], 'click', function() {
                            //infowindow.close();
                            infoWindows[<?php echo $i; ?>].open(map,markers[<?php echo $i; ?>]);
                        });
                                                                                                                                                                                       
        <?php
        $i++;
    }
}
?>

    }
    $(document).ready(function() {
        load(); // carreguem Google Maps
        
        // Slider REVOLUTION   
        if ($.fn.cssOriginal!=undefined)
            $.fn.css = $.fn.cssOriginal;

        $('.banner.fitxa').revolution(
        {
            delay:5000,
            startheight:453,
            startwidth:680,

            hideThumbs:0,

            thumbWidth:135,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
            thumbHeight:90,
            thumbAmount:5,

            navigationType:"<?php echo ($show_thumbs) ? 'thumb' : 'none'; ?>",					//bullet, thumb, none, both		(No Thumbs In FullWidth Version !)
            navigationArrows:"nexttobullets",		//nexttobullets, verticalcentered, none
            navigationStyle:"round",				//round,square,navbar

            touchenabled:"off",						// Enable Swipe Function : on/off
            onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

            navOffsetHorizontal:0,
            navOffsetVertical:-30,

            stopAtSlide:0,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

            shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
            fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
        });
    });
    
    $('#reset').click(function(){
        $("#generic1").find('option:selected').removeAttr('selected');   
        $('#generic1').selectedIndex='0';
        $("#generic2").find('option:selected').removeAttr('selected');   
        $('#generic2').selectedIndex='0';
        $("#generic3").find('option:selected').removeAttr('selected');   
        $('#generic3').selectedIndex='0';
        $("#sector_id").find('option:selected').removeAttr('selected');   
        $('#sector_id').selectedIndex='0';
        $('#nom').val('');
        $('#'+$(this).parents("form").attr("id")).submit();
    });
    
    
    /*Mostrar i amagar membres de TECNICS*/
    $('.membres').click(function(){
            
        if ( $(this).next('.llistat_membres').is(':visible') ){
            $(this).next('.llistat_membres').hide('fast');  
            $(this).html('Mostrar membres');
            $(this).removeClass('obert');
        }else{
            $(this).next('.llistat_membres').show('fast','easeInOutQuad');
            $(this).html('Ocultar membres');
            $(this).addClass('obert');
        }
      
    });
    
</script>