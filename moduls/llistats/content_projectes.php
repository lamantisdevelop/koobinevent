<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>


<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * FITXA-----------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
if (tep_not_null($_GET['itemID'])) {
    ?>
    <div class="fitxa_llistat projectes llistat_<?php echo $_GET['llistat']; ?>">
        <?php
        tep_llistats_augmentar_visualitzacions((int) $_GET['itemID']);

        // Fitxa

        $listing = tep_get_llistat_by_id($_GET['itemID'], false); //obtenim info del llistat

        $query_numrows = tep_db_num_rows($listing);
        $te_mapa_simple = true; //indiquem a js que ha de generar google maps
        if ($query_numrows > 0) {
            $item = tep_db_fetch_array($listing);
            $itemInfo = new objectInfo($item);
            $galeria = tep_get_fotos_galeria($itemInfo->fotos_groups_id);
            $i = 1;
            $related1 = tep_get_llistats_relacionats_by_group($itemInfo->id, 4); //clients
            $related2 = tep_get_llistats_relacionats_by_group($itemInfo->id, 5); //eines
            ?>
            <div class="continent_fitxa row">

                <div class = "camps large-4 large-push-8 columns">
                    <div class="titulars">
                        <?php
                        if ($itemInfo->titol) {
                            ?>
                            <h1 class = "titol"><?php echo $itemInfo->titol; ?></h1>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if ($itemInfo->link) {
                        ?>
                        <div class="camp">
                            <span class="enunciat"><?php echo _('URL'); ?>:</span>
                            <a class="link" target="_blank" href="<?php echo $itemInfo->link; ?>"><?php echo str_replace('http://', '', $itemInfo->link); ?></a>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    $element = current(array_keys($related1));
                    if ($element) {
                        ?>

                        <div class="related1 camp">
                            <span class="enunciat"><?php echo _('Client'); ?>:</span>
                            <?php echo tep_get_llistat_titol($element); ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    $element2 = current(array_keys($related2));
                    if ($element2) {
                        ?>
                        <div class="related2 camp">
                            <span class="enunciat"><?php echo _('Aplicacio'); ?>:</span>
                            <?php $eina = tep_db_fetch_array(tep_get_llistat_by_id($element2)); ?>
                            <a class="link" href="<?php echo tep_friendly_url('', tep_get_page_title(100), 'itemID=' . $eina['id'] . '&item_title=' . $eina['titol']); ?>" title="<?php echo $eina['titol']; ?>"><?php echo $eina['titol']; ?></a>
                        </div>
                        <?php
                    }
                    ?>
                    <hr>
                    <div class="descripcions">
                        <div class="desc"><?php echo $itemInfo->descripcio; ?></div>

                    </div>
                    <div class="social">
                        <iframe id="facebook" src="http://www.facebook.com/plugins/like.php?href=<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=recommend&amp;colorscheme=light&amp;font&amp;height=35" scrolling="no" frameborder="0"></iframe>
                        <a id="twitter" href="https://twitter.com/share" class="twitter-share-button">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
                    </div> 

                </div>
                <div class="galeria large-8 large-pull-4 columns">
                    <?php
                    while ($foto = tep_db_fetch_array($galeria)) {
                        if ($i != 1) {//no mostrem la primera imatge, que es el thumb
                            ?>
                            <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $foto['image']; ?>" alt = "<?php echo $foto->titol; ?>" class = "imatge" />
                            <?php
                        }
                        $i++;
                    }
                    ?>
                </div>
                <?php
            } else {
                echo '<p class="no_item">' . _('Element no trobat') . '</p>';
            }
            ?>
        </div>
    </div>
<?php } else {
    ?>


    <?php
    /* ----------------------------------------------------------------------------------------------------------------------------------------------
     * LLISTAT---------------------------------------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------------------------------------------
     */
    ?>

    <div class="llistat projectes">
        <?php
        $llistats = tep_get_llistats_by_group($_GET['llistat']);
        $llistat_numrows = tep_db_num_rows($llistats);

//mostrem un titular o altre depenent de si estem a fitxa o llistat
        if ($listing_numrows > 0) {
            $i = 0;
            if ($_GET['itemID']) {//cas estem a fitxa i hi ha relacionats
                ?>
                <hr>
                <h3><?php echo(_('Altres elements del llistat')); ?></h3>
                <?php
            } else {
                ?>
                <h1><?php echo(tep_get_page_titol($_GET['pageID'])); ?></h1>
                <?php echo($pageInfo->content); ?>
                <?php
            }
        }
        ?>
        <ul class="grid small-block-grid-2 medium-block-grid-4 large-block-grid-4">
            <?php
            //Recorrem tots els elements del llistat
            while ($llistat = tep_db_fetch_array($llistats)) {
                $itemInfo = new objectInfo($llistat);
                $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                if ($itemInfo->id != $_GET['itemID']) {//no mostrem item actual en cas de dins de fitxa
                    ?>
                    <li class="item_llistat llistat_<?php echo $_GET['llistat']; ?>">
                        <div class="contenidor_item" onclick="window.location='<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>';">
                            <?php if (is_array($portada)) { ?>
                                <a class="" href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->titol; ?>">
                                    <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" />
                                </a>
                                <div class="mascara">
                                    <h5 class="subheader titol_projecte">
                                        <a class="" href="<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->titol; ?>">
                                            <?php echo $itemInfo->titol; ?>
                                        </a>
                                    </h5>
                                </div>                       
                            <?php } ?>
                        </div>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
        <div class="clear"></div>
    </div>
    <?php
    /* } else {
      // No Items
      echo NO_RESULTS;
      } */
}
?>
