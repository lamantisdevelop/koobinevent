<div class="off-canvas-wrap docs-wrap">
    <div class="inner-wrap">
<!--        <nav class="tab-bar show-for-small">
            <section class="left-small">
                <a class="no-fade left-off-canvas-toggle menu-icon"><span></span></a>
            </section>
            <section class="right tab-bar-section">
                <h1 class="title">Menú lateral</h1>
            </section>
        </nav>-->

<!--        <aside class="left-off-canvas-menu">
            <ul class="off-canvas-list">
                <li><label>Menú principal</label></li>
                <?php //tep_print_menu_foundation_sidenav(4, 0, false) ?>
                <li><label>Menú lateral</label></li>
                <?php
                //Cerquem ID del menu general que te associat aquesta pagina...aixi podem saber si aquest menu te fills o no i mostrar-los on ens interesa
//                $id_menu = tep_id_menu_actual_page();
//                $parent_menu_id = tep_get_menu_parent_id($id_menu); //obtenim el pare de l'item del menú
//                $id_menu = ($parent_menu_id ? $parent_menu_id : $id_menu); //si el pare es diferent de 0 vol dir que som dins un submenú
//                $submenu = tep_get_menus_pages_list(4, 'submenu_header', $id_menu, true);
//                if ($submenu) 
//                {
//                    tep_print_menu_foundation_sidenav(4, $id_menu, true);
//                }
                ?>
            </ul>           
        </aside>-->
        
        <a class="no-fade exit-off-canvas"></a>
        <section class="main-section">
           

            <div class="row">
                <?php
                //les pàgines només tindran una columna (dreta o esquerra)
                $span_central_home = tep_get_span_central(SITE_LATERAL_ESQUERRE, SITE_LATERAL_DRET);
                ?>

                <div class="<?php echo $span_central_home; ?> columns modul_llistats llistat_<?php echo $_GET['llistat'] . (tep_not_null($_GET['itemID']) ? ' fons_opac fitxa' : ''); ?>">
                    <div class="page_content">

                        <?php
                        //podem crear una fitxa, per cada llista
                        //si utilitzem algun arxiu del metode antic, el posem com a case de la fitxa i case del llisat
                        //podem moure el div principal (class modul_llistats) a dins del CASE, tb hauriem de more els menús. així podem tenir menús diferents segons el llistat
                        if ($_GET['itemID'])
                        {
                            switch ($_GET['llistat']) 
                            { 
                                //case 1:
                                //    include('content_equip.php');
                                //    break;
                                default:
                                    include('content_item_fitxa.php');
                                    break;
                            }
                        }
                        else if (isset($_GET['llistat'])) 
                        {
                            switch ($_GET['llistat']) 
                            {
                                case 1:
                                //case 9:
                                //case 10:
                                    
                                    include('content_llistat_clients.php');
                                    break;
                                case 6:
                                    
                                    include('content_llistat_treball.php');
                                    break;
                                default:
                                    
                                    include('content_llistat_categories.php');
                                    include('content_llistat_productes.php');
                                    break;
                            }
                        }
                        ?>
                    </div>
                </div>

                <?php
                //les pàgines només tindran una columna (dreta o esquerra)
                if (SITE_LATERAL_ESQUERRE) {
                    ?>
                    <div class="large-3 large-pull-9 medium-3 medium-pull-9 columns">
                        <?php
                        include(DIR_WS_BOXES . 'lateral.php');
                        ?>
                    </div>
                    <?php
                }

                if (SITE_LATERAL_DRET) {
                    ?>
                    <div class="large-3 columns">
                        <?php include(DIR_WS_BOXES . 'lateral.php'); ?>
                    </div>
                    <?php
                }
                ?>

            </div>
        </section>
    </div>
</div>