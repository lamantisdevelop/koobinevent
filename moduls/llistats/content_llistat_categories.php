<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>

<?php 

//echo '<br />CATEGORIES<br />';

if (isset($_GET['groupID']))
{
    $result = tep_get_llistat_groups_by_parent_id($_GET['groupID']);
}
else if(isset($_GET['llistat']))
{
     $result = tep_get_llistat_groups_by_parent_id($_GET['llistat']);
}

if (tep_db_num_rows($result)) 
{
    ?>
    <ul class="llistat_grups large-block-grid-3 medium-block-grid-3 small-block-grid-2">
        <?php
        mysql_data_seek($result, 0);
        while ($llistat = tep_db_fetch_array($result)) 
        {
            $itemInfo = new objectInfo($llistat);
            ?>
            <li class="grup grup_pare grup_<?php echo $itemInfo->id ?>">

                <a href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'groupID=' . $itemInfo->id . '&group_title=' . $itemInfo->title); ?>">
                    <?php
                    $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                    if (!is_null($portada) && $portada != '') 
                    {
                        ?>
                        <img class="imatge"  src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" />
                        <?php
                    }
                    else
                    {
                        ?>
<!--                        <p><a name='<?php //echo str_replace(" ","_",$itemInfo->title); ?>'></a></p>-->
                        <h3 class="titolGrup <?php echo 'titol_subratllat' ?>" data-magellan-destination='<?php echo str_replace(" ","_",$itemInfo->title); ?>'> <?php  $itemInfo->title; ?>
                            <?php
                            echo $itemInfo->title;
                            ?>
                        </h3>
                        <?php 
                    }
                    ?>
                </a>
            </li>
            <?php
        }
        ?>
    </ul>
    <?php
}
?>