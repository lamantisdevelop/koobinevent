<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>

<?php
/* Aplicacions Inici */
//require('includes/application_top_min.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>


        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
        <meta name="google-site-verification" content="u8y5q8g0dY76QBcTICdzITSJ-O2OTlLco8fnF5Yqbaw" />






    </head>
    <body>
        <div id="content_map">
            <div id="google_map" style="height: 500px;width: 700px;"></div>
        </div>

        <script type="text/javascript" language="javascript">
            //<![CDATA[

            /*function load() {
  if (GBrowserIsCompatible()) {
    var map = new GMap2(document.getElementById("map"));
    map.addControl(new GSmallZoomControl());
    map.addControl(new GMapTypeControl());
    map.setCenter(new GLatLng(41.9668335653795,2.8365111351013184), 14);

        var cIcon = new GIcon(G_DEFAULT_ICON);
        cIcon.image = 'http://www.mantis.cat/image/boto_mapa.png';
        cIcon.iconSize = new GSize(91, 116);
        cIcon.iconAnchor = new GPoint(20, 100);

    var marker = new GMarker(new GLatLng(41.9668335653795,2.8365111351013184),cIcon);
    map.addOverlay(marker);
  }
}

load(); // carreguem Google Maps
             */
            function load() {
                var latlng = new google.maps.LatLng(<?php echo $_GET['lat'];?>,<?php echo $_GET['lon'];?>);
                var myOptions = {
                    zoom: 15,
                    center: latlng,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                };
                var map = new google.maps.Map(document.getElementById("google_map"),
                myOptions);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo $_GET['lat'];?>,<?php echo $_GET['lon'];?>),
                    map: map
                });


            }

            load(); // carreguem Google Maps

            //]]>
        </script>
    </body>
</html>



<?php
/* Aplicacions Final *///
//require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
