<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>

<h2><?php echo(tep_get_page_titol($pageID)); ?></h2>
<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * FITXA-----------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
if (tep_not_null($itemID)) {
    ?>
    <div class="fitxa_llistat llistat_<?php echo $llistatID; ?>">
        <?php
        tep_llistats_augmentar_visualitzacions((int) $itemID);

        // Fitxa

        $listing = tep_get_llistat_by_id($itemID, false); //obtenim info del llistat

        $query_numrows = tep_db_num_rows($listing);
        $te_mapa_simple = true; //indiquem a js que ha de generar google maps
        if ($query_numrows > 0) {
            $item = tep_db_fetch_array($listing);
            $itemInfo = new objectInfo($item);
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
            ?>
            <div class="continent_fitxa">
                <?php if (!is_null($portada)) { ?>
                    <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" class = "imatge" />
                <?php } ?>
                <div class = "camps">
                    <div class="titulars">
                        <?php
                        if ($itemInfo->titol) {
                            ?>
                            <h1 class = "titol"><?php echo $itemInfo->titol; ?></h1>
                            <?php
                        }
                        if ($itemInfo->titol2) {
                            ?>
                            <h2 class = "titol2"><?php echo $itemInfo->titol2; ?></h2>
                            <?php
                        }
                        if ($itemInfo->titol3) {
                            ?>
                            <h3 class ="titol3"><?php echo $itemInfo->titol3; ?></h3>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="info_extra">
                        <?php
                        if ($itemInfo->autor) {
                            ?>
                            <div class="autor camp"><?php echo $itemInfo->autor; ?></div>
                            <?php
                        }

                        if ($itemInfo->published != '0000-00-00 00:00:00') {
                            ?>
                            <div class="data camp"><?php echo date('d/m/Y', strtotime($itemInfo->published)); ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="descripcions">
                        <div class="desc"><?php echo $itemInfo->descripcio; ?></div>
                        <div class="desc2"><?php echo $itemInfo->descripcio2; ?></div>
                        <div class="desc3"><?php echo $itemInfo->descripcio3; ?></div>
                    </div>
                    <div class="generics">
                        <?php
                        if ($itemInfo->generic1) {
                            ?>
                            <div class="generic1 camp"><?php echo $itemInfo->generic1; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic2) {
                            ?>
                            <div class="generic2 camp"><?php echo $itemInfo->generic2; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic3) {
                            ?>
                            <div class="generic3 camp"><?php echo $itemInfo->generic3; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic4) {
                            ?>
                            <div class="generic4 camp"><?php echo $itemInfo->generic4; ?></div>
                            <?php
                        }
                        if ($itemInfo->generic5) {
                            ?>
                            <div class="generic5 camp"><?php echo $itemInfo->generic5; ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="social">
                        <iframe id="facebook" src="http://www.facebook.com/plugins/like.php?href=<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=recommend&amp;colorscheme=light&amp;font&amp;height=35" scrolling="no" frameborder="0"></iframe>
                        <a id="twitter" href="https://twitter.com/share" class="twitter-share-button">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
                    </div> 
                    <?php
                    if ($itemInfo->link) {
                        ?>
                        <a class="boto link camp" target="_blank" href="<?php echo $itemInfo->link; ?>"><?php echo $itemInfo->link; ?></a>
                        <?php
                    }
                    ?>
                </div>
                <?php
            } else {
                echo '<p class="no_item">' . _('Element no trobat') . '</p>';
            }
            ?>
        </div>
    </div>
<?php
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * LLISTAT PRODUCTES---------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
?>

<div class="llistat">
    <?php
    $llistats = tep_get_llistats_by_group($llistatID);
    $llistat_numrows = tep_db_num_rows($llistats);
    //mostrem un titular o altre depenent de si estem a fitxa o llistat
    if ($listing_numrows > 0) {
        $i = 0;
        if ($itemID) {//cas estem a fitxa i hi ha relacionats
            ?>
            <hr>
            <h3><?php echo(_('Altres elements del llistat')); ?></h3>
            <?php
        } else {
            ?>
            <h1><?php echo(tep_get_page_titol($pageID)); ?></h1>
            <?php echo($pageInfo->content); ?>
            <?php
        }
    }

    //Recorrem tots els elements del llistat
    while ($llistat = tep_db_fetch_array($llistats)) {
        $itemInfo = new objectInfo($llistat);
        $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
        if ($itemInfo->id != $itemID) {//no mostrem item actual en cas de dins de fitxa
            ?>
            <div class="item_llistat llistat_<?php echo $llistatID; ?>">
        <?php if (is_array($portada)) { ?>
                    <a href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->titol; ?>">
                        <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image']; ?>" alt = "<?php echo $itemInfo->titol; ?>" class = "imatge" />
                    </a>
        <?php } ?>
                <div class="camps">
                    <h4 class="titol">
                        <a href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol); ?>" title="<?php echo $itemInfo->titol; ?>">
        <?php echo $itemInfo->titol; ?>
                        </a>
                    </h4>
                    <div class="sumari">
                        <?php
                        if ($itemInfo->titol2) {
                            ?>
                            <div class="titol2"><?php echo $itemInfo->titol2; ?></div>
                            <?php
                        }

                        if ($itemInfo->titol3) {
                            ?>
                            <div class="titol3"><?php echo $itemInfo->titol3; ?></div>
                            <?php
                        }
                        if ($itemInfo->autor) {
                            ?>
                            <div class="autor"><?php echo $itemInfo->autor; ?></div>
                            <?php
                        }
                        if ($itemInfo->published != '0000-00-00 00:00:00') {
                            ?>
                            <div class="data"><?php echo date('d/m/Y', strtotime($itemInfo->published)); ?></div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>

<?php
/* ----------------------------------------------------------------------------------------------------------------------------------------------
 * LLISTAT GRUPS---------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
 */
?>

<?php
if (!$itemID) 
{
        
    if ($groupID)
    {
        $result = tep_get_llistat_groups_by_parent_id($groupID);
        //echo tep_get_llistat_group_name($groupID);
    }
    else if (isset($llistatID))
    {
        $result = tep_get_llistat_groups_by_parent_id($llistatID);
        //echo tep_get_llistat_name($llistatID);
    }
    
    //if ($groupID)
    {
        //pintem la imatge principal del grup, la tenim entrada al contingut a treves del admin
        //$auxResult = tep_get_llistat_group_by_id($groupID);
        
        //CONTINGUT
        //echo $auxResult['description'];

     
    }
    
 
    if (tep_db_num_rows($result)) 
    {
 
        
        
        ?><ul class="llistat_grups large-block-grid-1 medium-block-grid-1 medium-block-grid-1"><?php
        mysql_data_seek($result, 0);
        while ($llistat = tep_db_fetch_array($result)) 
        {
            
            $itemInfo = new objectInfo($llistat);
            ?>
            <li class="grup grup_pare grup_<?php echo $itemInfo->id ?>">
                
                    <a href="<?php echo tep_friendly_url('', tep_get_page_title($pageID), 'groupID=' . $itemInfo->id . '&group_title=' . $itemInfo->title); ?>">
                        <?php
                        $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
                        if (!is_null($portada) && $portada != '') 
                        {
                            ?>
                            <h3 class="titolGrup ">
                                <img class="imatge"  src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" />
                            </h3>
                            <?php
                        }
                        else
                        {
                            //si nomes imprimim el titol, ho fem despres amb el fill, si no hi ha fill no hi ha titol
                            
                            ?>
                            <p><a name='<?php echo str_replace(" ","_",$itemInfo->title); ?>'></a></p>
                            <h3 class="titolGrup <?php echo 'titol_subratllat' ?>" data-magellan-destination='<?php echo str_replace(" ","_",$itemInfo->title); ?>'> <?php  $itemInfo->title; ?>
                                <?php
                                echo $itemInfo->title;
                                ?>
                            </h3>
                            <?php 
                        }
                        ?>
                    </a>
                

                <?php 
                //tep_print_productes($itemInfo->id);//imprimim productes fills si en te
                
                
                
                
                
                ?>    
            </li>
            <?php
        }
        ?></ul><?php
    }
    else //mostrem productes si no hi ha categories per mostrar la "fitxa de categoria"
    {
        if ($groupID) 
        {
            ?>
<!--            <h3 class="">-->
                <?php
                //echo tep_get_llistat_group_name($groupID);
                ?>
<!--            </h3>-->
            <?php
            
            
            
            tep_print_productes($groupID);
        }
    }
}

function tep_print_productes($itemID)
{
   
    $result = tep_get_llistats_by_group($itemID);     
    if (tep_db_num_rows($result)) 
    {
//        
                $i = 0;
                while ($llistat = tep_db_fetch_array($result)) 
                {
                    
                    
                            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($llistat['fotos_groups_id']));
                            if (!is_null($portada) && $portada != '') 
                            {
                                ?>
                            
<!--                                <a href = "<?php //echo DIR_WS_PORTAL_IMAGE_FOTOS . $portada['image']; ?>"
                                    data-target="flare"
                                    data-flare-scale="fit"
                                    data-flare-gallery="gallery1"
                                    data-pe-target-id="<?php echo $i ?>">
                                    <img src = "<?php //echo DIR_WS_PORTAL_IMAGE_FOTOS . $portada['image']; ?>" alt="<?php //echo $itemInfo->name; ?>" />
                                </a>-->
                            
                                <img class="imatge"  src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image']; ?>" alt = "<?php echo $itemInfo->name; ?>" />
                                <?php
                            }
                           
                    $i++;
                }
                ?>
            <tbody>
        </table>
        <?php
    }
}
?>
