<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<nav class="top-bar  contain-to-grid" data-topbar>
    <ul class="title-area">
        <li class="name"></li>
        <li class="toggle-topbar menu-icon"><a class="no-fade" href=""><span>Menu</span></a></li>
    </ul>
    <section class="top-bar-section">

        <?php
        // Menú Pagines
        echo tep_print_menu_foundation_by_id(4, 'menu-top', 0, true, true); // Menú Catàleg
        ?>   
        
        <ul class="right idiomes show-for-small-only ">
            <li class="divider divisio-idiomes"></li>
            <?php
            $active_languages = tep_get_active_languages();
            for ($i = 0, $n = sizeof($active_languages); $i < $n; $i++) {
                ?>
                <li class="idioma <?php echo (($active_languages[$i]['id'] == $language_id) ? 'active' : '') ?>">
                    <a href="<?php echo tep_friendly_url($active_languages[$i]['code'], tep_get_page_title($_GET['pageID'], $active_languages[$i]['id']), tep_get_friendly_params(), 1) ?>" class="<?php (($active_languages[$i]['id'] == $language_id) ? 'active' : '') ?>">
                        <?php echo $active_languages[$i]['code']; ?>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
        
        <ul class="right xarxes-socials show-for-small-only">
            <li class="social">
                <a href="<?php echo SOCIAL_FACEBOOK; ?>" title="Facebook" target="_blank" class="facebook"><i class="fi-social-facebook"></i></a>
            </li>
            <li class="social">
                <a href="<?php echo SOCIAL_LINKEDIN; ?>" title="LinkedIN" target="_blank" class="youtube"><i class="fi-social-linkedin"></i></a>
            </li>
            <li class="social">
                <a href="<?php echo SOCIAL_GOOGLE_PLUS; ?>" title="Google+" target="_blank" class="youtube"><i class="fi-social-google-plus"></i></a>
            </li>
            
        </ul>
    </section>
</nav>