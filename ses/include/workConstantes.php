<?php

/**
 * Description of workConstantes
 *
 * @author Christian
 */
class workConstantes
{
	protected $con;
	protected $constantes = false;
	protected $datosCon = false;
	
	function __construct($datos = false, $con = false)
	{
		$this->con = $con;
		$this->constantes = false;
		$this->datosCon = $datos;
	}
	
	function cargarConstantes()
	{
		if (!$this->constantes)
		{
			if (!$this->con
				&& $this->datosCon)
			{
				$this->con = db_connect($this->datosCon['host'], $this->datosCon['username'], $this->datosCon['passwd'], $this->datosCon['database']);

				if ($this->con)
				{
					$this->constantes = Array();

					$this->constantes['MYSQL_HOST'] = $this->datosCon['host'];
					if(isset($this->datosCon['host_replica']))
					{
						$this->constantes['MYSQL_HOST_REPLICA'] = $this->datosCon['host_replica'];
					}
					$this->constantes['MYSQL_USERNAME'] = $this->datosCon['username'];
					$this->constantes['MYSQL_PASSWD'] = $this->datosCon['passwd'];
					$this->constantes['MYSQL_DATABASE'] = $this->datosCon['database'];

					$query = "SELECT Conf_constants_id, Conf_constants_valor FROM Conf_constants";
					$result = query($query, __FILE__, __LINE__, $this->con);

					while ($row = fetch($result))
					{
						$this->constantes[$row['Conf_constants_id']] = $row['Conf_constants_valor'];
					}

					if (!isset($this->constantes['CARPETA_CLIENTE']))
					{
						$this->constantes['CARPETA_CLIENTE'] = str_replace(".", "", $this->constantes['PLANTILLA_CLIENTE']);
					}

					if (!isset($this->constantes['TMP_PATH']))
					{
						if(!is_dir(sys_get_temp_dir() . "/koobin/"))
						{
							mkdir(sys_get_temp_dir() . "/koobin/");
						}

						$this->constantes['TMP_PATH'] = sys_get_temp_dir() . "/koobin/" . $this->constantes['MYSQL_DATABASE'];

						if (!is_dir($this->constantes['TMP_PATH'] . "/"))
						{
							if (desarrollo())
							{
								mkdir($this->constantes['TMP_PATH'], 0777, true);
							}
							else
							{
								mkdir($this->constantes['TMP_PATH']);
							}
						}
					}

					if (isset($this->constantes['CARPETA_IMAGENES']) && $this->constantes['CARPETA_IMAGENES'])
					{
						$this->constantes['CARPETA_MEDIA'] = "media/" . $this->constantes['CARPETA_IMAGENES'] . "/";
						$this->constantes['CARPETA_PRINT'] = "print/" . $this->constantes['CARPETA_IMAGENES'] . "/";
					}
					else
					{
						$this->constantes['CARPETA_MEDIA'] = "media/" . $this->constantes['CARPETA_CLIENTE'] . "/";
						$this->constantes['CARPETA_PRINT'] = "print/" . $this->constantes['CARPETA_CLIENTE'] . "/";
					}

					$this->constantes['PATH'] = str_replace("\\", "/", realpath(getcwd()));


					$this->constantes['GESTION_ROOT_URL_NO_SCRIPT'] =  $this->constantes['HTTP_URL_EXTERN'];
					$this->constantes['GESTION_ROOT_URL'] = $this->constantes['GESTION_ROOT_URL_NO_SCRIPT'] . "/gestion.php";
					$this->constantes['PUBLIC_ROOT_URL_NO_SCRIPT'] = $this->constantes['HTTP_URL_EXTERN'];

					if (isset($this->constantes['USA_AMAZON'])
						&& $this->constantes['USA_AMAZON'] == "true")
					{
						$this->constantes['CARPETA_STATIC'] = 'static/';
						$this->constantes['CARPETA_CACHE'] = 'cache/' . $this->constantes['CARPETA_CLIENTE'] . '/';
					}
				}
				else
				{
					return false;
				}
			}
		}
	}
	
	function get($constante)
	{
		if ($this->datosCon)
		{
			$this->cargarConstantes();
			
			if (isset($this->constantes[$constante]))
			{
				return $this->constantes[$constante];
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (defined($constante))
			{
				return constant($constante);
			}
			else
			{
				return false;
			}
		}
	}
	
	function __get($name)
	{
		return $this->get($name);
	}
	
}
