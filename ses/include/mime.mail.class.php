<?php
include_once("./include/mail/email_message.php");
include_once("./include/mail/smtp_message.php");
include_once("./include/mail/smtp.php");
include_once("./include/mail/sasl.php");

class html_mime_mail
{
	var $mail;
	var $idioma;
	var $html_part;
	var $html_images = array();
	var $text_part;
	var $priority = false;
	var $borrar_temporales = array();

	function __construct()
	{
		$this->mail = new smtp_message_class;
	}
	
	function set_priority($priority)
	{
		$this->priority = $priority;
	}

	function add_attachments($attachments)
	{
		reset($attachments);
		foreach($attachments as $fichero)
		{
			$this->mail->AddFilePart($fichero);
		}
	}

	function add_html($html, $text)
	{
		$tipos_fotos = array(
			'gif'	=> 'image/gif',
			'jpg'	=> 'image/jpeg',
			'jpeg'	=> 'image/jpeg',
			'jpe'	=> 'image/jpeg',
			'bmp'	=> 'image/bmp',
			'png'	=> 'image/png',
			'tif'	=> 'image/tiff',
			'tiff'	=> 'image/tiff',
			'swf'	=> 'application/x-shockwave-flash'
			);
		while (list($key,$nada) = each($tipos_fotos))
		{
			$extensions[] = $key;
		}

		preg_match_all('/(?:"|\')([^"\']+\.('.implode('|', $extensions).'))(?:"|\')/Ui', $html, $images);
		
		$imagenes_agregadas = Array();
		for ($i=0; $i<count($images[1]); $i++)
		{
			$image_ori = $images[1][$i];
			$images[1][$i] = str_replace(HTTP_URL . "/", "", $images[1][$i]);
			$extension = strtolower(substr($images[1][$i], strrpos($images[1][$i], ".") + 1));
			
			$this->borrar_temporales = Array();
			if (substr($images[1][$i], 0, 4) == "http")
			{
				$contenido_imagen = @file_get_contents($images[1][$i]);
				if ($contenido_imagen)
				{
					$fichero_temporal = TMP_PATH . "/cache_" . md5($images[1][$i]) . "." . $extension;
					$this->borrar_temporales[] = $fichero_temporal;
					$tmpfp = fopen($fichero_temporal, "w");
					fwrite($tmpfp, $contenido_imagen);
					fclose($tmpfp);
				}
				else
				{
					$fichero_temporal = false;
				}
			}
			else
			{
				$fichero_temporal = $images[1][$i];
			}
			
			if (!isset($imagenes_agregadas[$fichero_temporal]) &&
					$fichero_temporal 
					&& file_exists($fichero_temporal))
			{
				$image=array(
					"FileName" => $fichero_temporal,
					"Content-Type" => $tipos_fotos[$extension],
					"Disposition" => "inline",
					"cache" => 1);

				//$html_images[] = $image;
				//$this->html = str_replace($images[1][$i], basename($images[1][$i]), $this->html);
				$this->mail->CreateFilePart($image, $image_part);
				$image_content_id = $this->mail->GetPartContentID($image_part);
				$html = str_replace($image_ori, "cid:" . $image_content_id, $html);
				$this->html_images[] = $image_part;
				
				$imagenes_agregadas[$fichero_temporal] = true;
			}
		}

		if (count($imagenes_agregadas) > 0)
		{
			$this->mail->CreateQuotedPrintableHTMLPart($html,"utf-8",$this->html_part);

			if ($text != "")
			{
				$this->mail->CreateQuotedPrintableTextPart($this->mail->WrapText($text),"utf-8",$this->text_part);
			}

			if ($this->text_part)
			{
				$alternative_parts[] = $this->text_part;
			}

			if ($this->html_part)
			{
				$alternative_parts[] = $this->html_part;
			}

			$this->mail->CreateAlternativeMultipart($alternative_parts, $alternative_part);

			$related_parts=array(
				$alternative_part
				//$image_part
				//		$background_image_part
			);

			reset($this->html_images);
			foreach($this->html_images as $imagen)
			{
				$related_parts[] = $imagen;
			}

			$this->mail->AddRelatedMultipart($related_parts);
		}
		else
		{
			$this->mail->AddQuotedPrintableHTMLPart($html, "utf-8");
			if ($text != "")
			{
				$this->mail->AddEncodedQuotedPrintableTextPart($this->mail->WrapText($text),"utf-8");
			}
		}
	}

	function send($to_name, $to_address, $from_name, $from_address, $subject, $extra = "", $metodo_envio = "mail")
	{
		$from_address = strtolower($from_address);
		if ($from_address == "monitor@koobin.com"
			|| (is_array($to_address) && in_array("monitor@koobin.com", $to_address))
			|| (!is_array($to_address) && $to_address == "monitor@koobin.com")
		)
		{
			// El monitor lo enviamos desde gmail. Si nos banean por spammers, como mínimo que solo afecte al monitor y no al resto de mails.
			$this->mail->smtp_host = "smtp.googlemail.com";
			$this->mail->smtp_port = "587";
			$this->mail->smtp_user = "koobinevent@gmail.com";
			$this->mail->smtp_password = "2rjtk70hpo6ji5v0xqxw5uvikzorwtsz";
			$this->mail->smtp_start_tls = 1;
			$mail_from = "koobinevent@gmail.com";
		}
		else
		{
			$this->mail->localhost = "localhost";
			if (defined("MAILHOST") && MAILHOST != "")
			{
				$this->mail->smtp_host = MAILHOST;
			}
			else
			{
				$this->mail->smtp_host = 'localhost';
			}

			if (class_exists("session")
				&& session::get('MAILHOST_SUCURSAL'))
			{
				$this->mail->smtp_host = session::get('MAILHOST_SUCURSAL');
			}

			// Mail Port
			if (defined("MAILPORT") && MAILPORT != "")
			{
				$this->mail->smtp_port = MAILPORT;
			}
			else
			{
				$this->mail->smtp_port = 25;
			}

			if (class_exists("session")
				&& session::get('MAILPORT_SUCURSAL'))
			{
				$this->mail->smtp_port = session::get('MAILPORT_SUCURSAL');
			}

			$mail_from = MAILFROM;

			if (defined("MAILUSER") && MAILUSER != "")
			{
				$this->mail->smtp_user = MAILUSER;
				$this->mail->smtp_password = MAILPASSWD;
			}
			else
			{
				$this->mail->smtp_user = '';
				$this->mail->smtp_password = '';
			}

			if (class_exists("session")
				&& session::get('MAILUSER_SUCURSAL'))
			{
				$this->mail->smtp_user = session::get('MAILUSER_SUCURSAL');
				$this->mail->smtp_password = session::get('MAILPASSWD_SUCURSAL');
			}

			if (defined("MAILTLS") && MAILTLS == "Y")
			{
				$this->mail->smtp_start_tls = 1;
			}
		}

		$mail_from = strtolower($mail_from);
//		echo "To: " . $to_address . " => " . $to_name . "<br>";
//		echo "From: " . $from_address . " => " . $from_name . "<br>";

		if (is_array($to_address))
		{
			reset($to_address);
			$to_bcc = array();
			$hay_to = false;
			$emails_agregados = Array();
			foreach ($to_address as $cont => $email_to)
			{
				$email_to = strtolower($email_to);
				if ($email_to == "tickets@koobin.com"
					&& defined("MAILSUBACCOUNT")
					&& MAILSUBACCOUNT != "")
				{
					$this->mail->SetHeader("X-MC-BccAddress", $email_to);
				}
				else
				{
					if (!isset($emails_agregados[$email_to]))
					{
						$emails_agregados[$email_to] = true;
						if (!$hay_to)
						{
							$this->mail->SetEncodedEmailHeader("To", $email_to, $to_name, "utf-8");
							$hay_to = true;
						}
						else
						{
							//$to_bcc .= $email_to . ", ";
							$to_bcc[$email_to] = $to_name;
						}
					}
				}
			}

			if (!$hay_to)
			{
				$this->mail->SetEncodedEmailHeader("To", "tickets@koobin.com", $to_name, "utf-8");
			}

			if (count($to_bcc) > 0)
			{
				//$to_bcc = substr($to_bcc, 0, -2);
				$this->mail->SetMultipleEncodedEmailHeader("Bcc", $to_bcc, "utf-8");
			}
		}
		else
		{
			$to_address = strtolower($to_address);
			$emails_agregados[$to_address] = true;
			$this->mail->SetEncodedEmailHeader("To", $to_address, $to_name, "utf-8");
		}

		$this->mail->SetEncodedEmailHeader("From", $mail_from, $from_name, "utf-8");

		if (is_array($extra) && isset($extra['reply']) && isset($extra['reply']['email']) && isset($extra['reply']['nombre']))
		{
			$this->mail->SetEncodedEmailHeader("Reply-To", strtolower($extra['reply']['email']), $extra['reply']['nombre'], "utf-8");
		}
		else
		{
			$this->mail->SetEncodedEmailHeader("Reply-To", $from_address, $from_name, "utf-8");
		}

		if (defined("MAILSUBACCOUNT")
			&& MAILSUBACCOUNT != "")
		{
			$this->mail->SetHeader("X-MC-Subaccount", MAILSUBACCOUNT);
		}

		if ($this->priority)
		{
			$this->mail->SetHeader("X-Priority", $this->priority);
		}

		// Aquí el mail que realmente enviará el correo.
		$this->mail->SetHeader("Sender", $mail_from);
		$this->mail->SetHeader("Return-Path", $mail_from);

		$this->mail->SetEncodedHeader("Subject", $subject, "utf-8");

		$error = $this->mail->Send();

		if ($this->mail->ses_message_id)
		{
			// Si recibimos este ID significa que estamos usando amazon.
			//echo "Hemos recibido el id " . $this->mail->ses_message_id . "<br>";
			log::mailLog($this->mail->ses_message_id, $mail_from, array_keys($emails_agregados), $subject);
		}
				
		if (is_array($this->borrar_temporales))
		{
			reset($this->borrar_temporales);
			foreach ($this->borrar_temporales as $tmp_filename)
			{
				clearstatcache(true, $tmp_filename);
				if (is_file($tmp_filename))
				{
					unlink($tmp_filename);
				}
			}
		}
		
		if (ini_get("log_errors") == true)
		{			
			if (is_array($to_address))
			{
				$tmp_address = implode(",", $to_address);
			}
			else
			{
				$tmp_address = $to_address;
			}
			
			if(strcmp($error,""))
			{
				$mensaje = " (" . date("d/m/Y H:i:s") . ") Errores al enviar " . $subject . " a " . $tmp_address . "\n" . $error;
			}
			else
			{
				$mensaje = " (" . date("d/m/Y H:i:s") . ") Mail " . $subject . " a " . $tmp_address . " enviado OK!";
			}
			
			if (class_exists("log"))
			{
				log::saveLog("MAIL", $mensaje);
			}
		}
	}
}