<?php

/**
 * Description of log
 *
 * @author Carlos Díaz
 */
class log {

	static $con = false;

	/**
	 * Devuelve la conexion a la BD de logs. Si no está abierta, intentará conectar.
	 * @return bool|resource conexion
	 */
	static function conDB()
	{
		if (!$GLOBALS['workConstantes']->MYSQL_LOG_HOST)
		{
			if (isset($_ENV['desarrollo'])
				&& (strcasecmp($_ENV['desarrollo'], "true") == 0))
			{
				include("include/DEV/logs.cfg.php");
			}
			else
			{
				include("cfg/logs/init.cfg.php");
			}
		}
		self::$con = db_connect(
			$GLOBALS['workConstantes']->MYSQL_LOG_HOST,
			$GLOBALS['workConstantes']->MYSQL_LOG_USERNAME,
			$GLOBALS['workConstantes']->MYSQL_LOG_PASSWD,
			$GLOBALS['workConstantes']->MYSQL_LOG_DATABASE
		);

		return self::$con;
	}
	
	static function saveLog($tipo, $vars, $sid_tmp = false, $tiempo = false, $accion = false)
	{
		if ($tipo == "IMAGEN")
		{
			return false;
		}

		if ($sid_tmp) $sid = $sid_tmp;
		else
		{
			if (class_exists("session") && session::get('sid')) $sid = session::get("sid");
			else $sid = "";
		}

		$fecha = time();
		$fecha_formateada = date("Y-m-d H:i:s", $fecha);

		if (isset($GLOBALS['id_registro_acceso']))
		{
			$id_registro_acceso = $GLOBALS['id_registro_acceso'];
		}
		else
		{
			$id_registro_acceso = "";
		}

		// Cortamos el mensaje, que las consultas largas tampoco sirven para mucho, al final
		if (strlen($vars) > 10000)
		{
			$vars = substr($vars, 0, 10000) . "...";
		}

		$query = "INSERT INTO log SET " .
				"bd = '" . db_real_escape_string($GLOBALS['workConstantes']->MYSQL_DATABASE) . "', " .
				"id_acceso = '" . db_real_escape_string($id_registro_acceso) . "', " .
				"sid = '" . db_real_escape_string($sid) . "', " .
				"fecha = '" . db_real_escape_string($fecha). "', " .
				"fecha_formateada = '" . db_real_escape_string($fecha_formateada) . "', " .
				"tipo = '" . db_real_escape_string($tipo) . "', " .
				"accion = '" . db_real_escape_string($accion) . "', " .
				"vars = '" . db_real_escape_string($vars) . "', " .
				"tiempo = '" . db_real_escape_string($tiempo) . "'";

		$myFile = sys_get_temp_dir() . "/koobin/log.log";
		$fh = fopen($myFile, 'a');
		fwrite($fh, $query . "\n");
		fclose($fh);

		return true;
	}

	/**
	 * @deprecated
	 * Devuelve los id_acceso de la tabla de registro_acceso, tantos como hayamos indicado separados por coma
	 * @param int $num_acciones el numero de registros que nos va a devolver
	 * @return string id's_registros separados por comas
	 */
	static function getUltimosAccesos($num_acciones)
	{
		if (!self::conDB()) return false;

		$query = "SELECT id_acceso FROM registro_acceso " .
			"WHERE bd = '" . db_real_escape_string($GLOBALS['workConstantes']->MYSQL_DATABASE) . "' " .
			"AND accion != 'PING' " .
			"AND id_usuario = '" . db_real_escape_string(session::get('id_admin')) . "#" . db_real_escape_string(session::get('sesion_sucursal')) . "' " .
			"ORDER BY hora DESC LIMIT " . db_real_escape_string($num_acciones);
		$do_query = query($query, __FILE__, __LINE__, self::conDB());
		$ids = "";

		while ($row = fetch($do_query))
		{
			$ids .= $row['id_acceso'] .", ";
		}

		return substr($ids, 0, -2);
	}

	static function getAcceso($id_registro)
	{
		if (!self::conDB()) return false;
		$query_test = "SELECT * FROM registro_acceso " .
			"WHERE bd = '" . db_real_escape_string($GLOBALS['workConstantes']->MYSQL_DATABASE) . "' " .
				"AND id_acceso = '" . db_real_escape_string($id_registro) . "' " .
			"UNION " .
				"SELECT * FROM registro_acceso " .
					"WHERE bd = '" . db_real_escape_string($GLOBALS['workConstantes']->MYSQL_DATABASE) . "' " .
						"AND id = '" . db_real_escape_string($id_registro) . "'";
		$result = query($query_test, __FILE__, __LINE__, self::conDB());
		if ($row = fetch($result))
		{
			$tmp_vars = @unserialize($row['vars']);

			if ($tmp_vars == false)
			{
				// No se ha desearlizado!
				$fix_vars = fix_serialize($row['vars']);
				$tmp_vars = @unserialize($fix_vars);

				if ($tmp_vars == false)
				{
					debug($fix_vars);
					echo "No se ha podido ejecutar el unserialize.";
					die;
				}
			}
		}
		else
		{
			$tmp_vars = false;
		}
		db_close(self::$con);

		return $tmp_vars;
	}

	static function guardaAcceso($filename = "", $custom_vars = null)
	{
		global $action;
		global $HTTP_RAW_POST_DATA;

		if ($action == "INF_estado")
		{
			$GLOBALS['id_registro_acceso'] = 0;
			return 0;
		}

		$vars = array();
		$vars['ID'] = uniqid();

		$vars['vars'] = Array();

		if(!is_null($custom_vars))
		{
			$vars['vars'] = $custom_vars;
		}

		if ($action != "comprova_auth")
		{
			if(isset($_POST))
				$vars['vars']['POST'] = $_POST;

			if(isset($_GET))
				$vars['vars']['GET'] = $_GET;

			if (isset($_SESSION))
			{
				$vars['vars']['SESSION'] = $_SESSION;
			}

			if (isset($_FILES))
			{
				$vars['vars']['FILES'] = $_FILES;
			}

			if ($HTTP_RAW_POST_DATA != "")
			{
				if ($filename == "tkm.php")
				{
					$vars['vars']['HTTP_RAW_POST_DATA'] = utf8_encode($HTTP_RAW_POST_DATA);
				}
				else
				{
					$vars['vars']['HTTP_RAW_POST_DATA'] = $HTTP_RAW_POST_DATA;
				}
			}
		}

		if (isset($vars['vars']['POST']['password']))
		{
			$vars['vars']['POST']['password'] = convert($vars['vars']['POST']['password']);
		}

		if (isset($vars['vars']['GET']['password']))
		{
			$vars['vars']['GET']['password'] = convert($vars['vars']['GET']['password']);
		}

		if (isset($vars['vars']['SESSION']['password']))
		{
			$vars['vars']['SESSION']['password'] = convert($vars['vars']['SESSION']['password']);
		}

		if (isset($_SERVER["HTTP_AUTHORIZATION"]))
		{
			$vars['vars']['HTTP_AUTHORIZATION'] = $_SERVER["HTTP_AUTHORIZATION"];
		}

		$ip = self::getIP();

		$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';

		$ordenador = self::getBrowser();

		$query = "INSERT INTO registro_acceso SET " .
			"bd = '" . db_real_escape_string($GLOBALS['workConstantes']->MYSQL_DATABASE) . "', " .
			"id = '" . db_real_escape_string($vars['ID']) . "', " .
			"sid = '" . db_real_escape_string(session::get("sid")) . "', " .
			"ordenador = '" . db_real_escape_string($ordenador) . " (" . db_real_escape_string($ip) . ")', " .
			"id_usuario = '" . db_real_escape_string(session::get("id_admin") . "#" . session::get("sesion_sucursal")) . "', " .
			"hora = '" . time() . "', " .
			"accion = '" . db_real_escape_string($action) . "' , " .
			"vars = '" . db_real_escape_string(serialize($vars['vars'])) . "', " .
			"filename = '" . db_real_escape_string($filename) . "', " .
			"maquina = '" . db_real_escape_string($maquina) . "'";

		$GLOBALS['log_origen'] = $filename;

		$myFile = sys_get_temp_dir() . "/koobin/log.log";
		$fh = fopen($myFile, 'a');
		fwrite($fh, $query . "\n");
		fclose($fh);

		$GLOBALS['id_registro_acceso'] = $vars['ID'];
		self::iniciaTiempo();

		return true;
	}

	static function guardaTiempoAcceso($accion_ejecutada = false)
	{
		self::finalizaTiempo();

		if (!isset($GLOBALS['total_consultas']))
		{
			$GLOBALS['total_consultas'] = 0;
		}

		if (isset($GLOBALS['id_registro_acceso'])
			&& $GLOBALS['id_registro_acceso'] != 0)
		{
			$query = "UPDATE registro_acceso SET " .
				"tiempo = '" . db_real_escape_string(self::tiempoPasado()) . "', " .
				"num_consultas = '" . db_real_escape_string($GLOBALS['total_consultas']) . "'";

			if ($accion_ejecutada)
			{
				$query .= ", accion = '" . db_real_escape_string($accion_ejecutada) . "'";
			}

			if (isset($GLOBALS['__registro_acceso'])
				&& is_array($GLOBALS['__registro_acceso']))
			{
				foreach ($GLOBALS['__registro_acceso'] as $campo => $valor)
				{
					if (!$accion_ejecutada
						|| $campo != 'accion'
					)
					{
						$query .= ", " . $campo . " = '" . db_real_escape_string($valor) . "'";
						$vars[$campo] = $valor;
					}
				}
			}

			$query .= " WHERE id = '" . db_real_escape_string($GLOBALS['id_registro_acceso']) . "' " .
				"AND bd = '" . db_real_escape_string($GLOBALS['workConstantes']->MYSQL_DATABASE) . "'";

			$myFile = sys_get_temp_dir() . "/koobin/log.log";
			$fh = fopen($myFile, 'a');
			fwrite($fh, $query . "\n");
			fclose($fh);
		}

		$GLOBALS['total_consultas_last'] = $GLOBALS['total_consultas'];
		$GLOBALS['total_consultas'] = 0;
	}

	static function iniciaTiempo()
	{
		$GLOBALS['inicio'] = microtime_float();
	}

	static function finalizaTiempo()
	{
		$GLOBALS['final'] = microtime_float();
	}

	static function tiempoPasado()
	{
		if (isset($GLOBALS['inicio'])
			&& $GLOBALS['inicio'] != 0
			&& isset($GLOBALS['final'])
			&& $GLOBALS['final'] != 0)
		{
			return $GLOBALS['final'] - $GLOBALS['inicio'];
		}
		else
		{
			return false;
		}
	}

	static function guardaDatosAcceso($datos)
	{
		if (!isset($GLOBALS['__registro_acceso'])
			|| !is_array($GLOBALS['__registro_acceso']))
		{
			$GLOBALS['__registro_acceso'] = Array();
		}

		foreach ($datos as $campo => $valor)
		{
			$GLOBALS['__registro_acceso'][$campo] = $valor;
		}
	}

	static function getIP()
	{
		if (isset($_SERVER["HTTP_CF_CONNECTING_IP"]))
		{
			$ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
		}
		elseif (isset($_SERVER["HTTP_CLIENT_IP"]))
		{
			$ip = $_SERVER["HTTP_CLIENT_IP"];
		}
		elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
		{
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		}
		else
		{
			if (isset($_SERVER["REMOTE_ADDR"]))
			{
				$ip = $_SERVER["REMOTE_ADDR"];
			} else
			{
				$ip = "";
			}
		}

		return $ip;
	}

	static function getBrowser()
	{
		if (isset($_SERVER['HTTP_USER_AGENT']))
		{
			if (mb_check_encoding($_SERVER['HTTP_USER_AGENT'], "UTF-8"))
			{
				$browser = $_SERVER['HTTP_USER_AGENT'];
			}
			else
			{
				$browser = utf8_encode($_SERVER['HTTP_USER_AGENT']);
			}
		}
		else
		{
			$browser = "";
		}

		return $browser;
	}

	static function mailLog($messageId, $from, $to, $subject, $hora = false)
	{
		if (!$hora) $hora = time();

		if ($GLOBALS['workConstantes']->MYSQL_DATABASE == "")
		{
			$query = "INSERT IGNORE INTO maillog SET ";
		}
		else
		{
			$query = "REPLACE INTO maillog SET ";
		}
		$query .= "messageId = '" . db_real_escape_string($messageId) . "', " .
			"bd = '" . db_real_escape_string($GLOBALS['workConstantes']->MYSQL_DATABASE) . "', " .
			"de = '" . db_real_escape_string($from) . "', " .
			"asunto = '" . db_real_escape_string($subject) . "', " .
			"fecha_envio = '" . db_real_escape_string($hora) . "'";
		$myFile = sys_get_temp_dir() . "/koobin/log.log";
		$fh = fopen($myFile, 'a');
		fwrite($fh, $query . "\n");
		fclose($fh);

		foreach ($to as $email)
		{
			$query = "INSERT IGNORE INTO maillog_detalle SET " .
				"messageId = '" . db_real_escape_string($messageId) . "', " .
				"para = '" . db_real_escape_string($email) . "', " .
				"fecha_actualizacion = '" . db_real_escape_string($hora) . "'";
			$fh = fopen($myFile, 'a');
			fwrite($fh, $query . "\n");
			fclose($fh);
		}
	}

	static function updateMailLog($messageId, $to, $status, $hora = false, $detalle = Array())
	{
		if (!$hora) $hora = time();
		$query = "UPDATE maillog_detalle SET " .
				"fecha_actualizacion = '" . db_real_escape_string($hora) . "', " .
				"estado = '". db_real_escape_string($status) . "'";
		if (isset($detalle['tipo']))
		{
			$query .= ", tipo = '" . db_real_escape_string($detalle['tipo']) . "'";
		}

		if (isset($detalle['subtipo']))
		{
			$query .= ", subtipo = '" . db_real_escape_string($detalle['subtipo']) . "'";
		}

		if (isset($detalle['action']))
		{
			$query .= ", action = '" . db_real_escape_string($detalle['action']) . "'";
		}

		if (isset($detalle['status']))
		{
			$query .= ", status = '" . db_real_escape_string($detalle['status']) . "'";
		}

		if (isset($detalle['diagnosticCode']))
		{
			$query .= ", diagnosticCode = '" . db_real_escape_string($detalle['diagnosticCode']) . "'";
		}

		if (isset($detalle['smtpResponse']))
		{
			$query .= ", smtpResponse = '" . db_real_escape_string($detalle['smtpResponse']) . "'";
		}

		if (isset($detalle['reportingMTA']))
		{
			$query .= ", reportingMTA = '" . db_real_escape_string($detalle['reportingMTA']) . "'";
		}

		$query .= " WHERE messageId = '" . db_real_escape_string($messageId) . "' " .
			"AND para = '" . db_real_escape_string($to) . "'";

		if ($status != 'SPAM')
		{
			$query .= "AND estado != 'ENTREGADO'";
		}

		$query = preg_replace("/\n/m", '\n', $query);

		$myFile = sys_get_temp_dir() . "/koobin/log.log";
		$fh = fopen($myFile, 'a');
		fwrite($fh, $query . "\n");
		fclose($fh);
	}
}

