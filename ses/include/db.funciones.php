<?php
$n = 0;

/**
 * Conecta con una BD y establece la codificación a UTF-7
 *
 * @param $host
 * @param $username
 * @param $passwd
 * @param $database (optional)
 * @param $replica (optional)
 *
 * @return resource conexion
 */
function db_connect($host, $username, $passwd, $database = false, $replica = false)
{
	$dbtype = DBTYPE;
	$con = $dbtype::connect($host, $username, $passwd, $database);

	if ($con)
	{
		$thread_id = db_thread_id($con);
		db_set_charset("utf8", $con);

		if(defined("ZONA_HORARIA") && in_array(ZONA_HORARIA, DateTimeZone::listIdentifiers()))
		{
			$GLOBALS['conex'][$thread_id]['zona_horaria'] = ZONA_HORARIA;
			$dbtype::query("SET time_zone = '" . db_real_escape_string(ZONA_HORARIA, $con) . "'", $con);
		}
		else
		{
			$GLOBALS['conex'][$thread_id]['zona_horaria'] = false;
		}

		$GLOBALS['conex'][$thread_id]['username'] = $username;
		$GLOBALS['conex'][$thread_id]['passwd'] = $passwd;
		$GLOBALS['conex'][$thread_id]['database'] = $database;
		$GLOBALS['conex'][$thread_id]['host'] = $host;
		$GLOBALS['conex'][$thread_id]['con'] =& $con;
		$GLOBALS['conex'][$thread_id]['replica'] = $replica;

		return $con;
	}
	else
	{
		return false;
	}
}

/**
 * @param      $database
 * @param      $tcon (optional) Conexión de la BD
 *
 * @return bool
 */
function db_select_db($database, $tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	
	$GLOBALS['conex'][db_thread_id($con)]['database'] = $database;
	
	$dbtype = DBTYPE;
	return $dbtype::select_db($database, $con);
}

/**
 * Ejecuta una consulta y devuelve el resultado.
 *
 * @param string     $query
 * @param string     $file
 * @param string     $line
 * @param bool|mysqli|resource $con (optional) Conexión de la BD.
 * @param bool $debug (optional) Si hay un duplicado no da error.
 * @param bool $stop_on_error (optional) Se interrumple la ejecución si hay un error en la consulta.
 * @param bool $envia_mail (optional) Envía un email si hay un error.
 *
 * @return result|resource|bool El resultado de la consulta
 */
function query($query, $file, $line, $con = false, $debug = true, $stop_on_error = false, $envia_mail = true)
{
	$dbtype = DBTYPE;
	
	global $n;

	if (!$con)
	{
		global $con;		//variable de conexió MySql

		if (!$con)
		{
			$con = db_connect(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWD, MYSQL_DATABASE);

			if(!$con) {
				display_db_error(db_connect_errno(), db_connect_error(), MYSQL_HOST, MYSQL_USERNAME, MYSQL_DATABASE, $file, $line);
				die;
			}
		}
	}

	$n++;
	$query = trim($query);

//	debug($file . " - " . $line . ": " . $query);

//	if (MYSQL_DATABASE == "ticketing_ibercamera"
//		|| MYSQL_DATABASE == "ticketing_ibercamera_dev")
//	if (true)
	if (false)
	{
		if (substr($query, 0, strlen("SELECT")) != "SELECT")
		{
			$logfile = fopen(TMP_PATH . "/" . $GLOBALS['conex'][db_thread_id($con)]['database'] . "_query.log", "a");
			fwrite($logfile, "[" . date("d-M-Y H:i:s") . "] #" . db_thread_id($con) . "    " . $query . ";   File: " . $file . " Line: " . $line . "\n");
			fclose($logfile);
		}
	}

	if (db_errno($con) != 2006)
	{
		if (function_exists("desarrollo") && desarrollo()
			&& isset($_SERVER) && isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != "192.168.30.251")
		{
			if (!isset($GLOBALS['consultas_realizadas'][md5($query)]))
			{
				$GLOBALS['consultas_realizadas'][md5($query)]['consulta'] = $query;
				$GLOBALS['consultas_realizadas'][md5($query)]['veces'] = 1;
			}
			else
			{
				$GLOBALS['consultas_realizadas'][md5($query)]['veces']++;
			}
		}
		$result = $dbtype::query($query, $con);
	}
	else
	{
		if (isset($GLOBALS['consulta_anterior'])
			&& $GLOBALS['consulta_anterior'] != "")
		{
			if (!isset($GLOBALS['errores_mysql']))
			{
				$GLOBALS['errores_mysql'] = "";
			}

			$GLOBALS['errores_mysql'] .= "Consulta MySQL antes del timeout:\n";
			$GLOBALS['errores_mysql'] .= $GLOBALS['consulta_anterior'] . "<br>\n";
		}
		die;
	}

	if (!isset($GLOBALS['total_consultas']))
	{
		$GLOBALS['total_consultas'] = 0;
	}
	$GLOBALS['total_consultas']++;
	
	$errno = db_errno($con);
	$error = db_error($con);

	if ($errno != 0)
	{
		if ($errno == 2006)
		{
//			debug("Reconecta!");
			// El MySQL está parado!
			// Vamos a intentar reconectar
			$thread_old = db_thread_id($con);
			$con = db_connect(
				$GLOBALS['conex'][$thread_old]['host'],
				$GLOBALS['conex'][$thread_old]['username'],
				$GLOBALS['conex'][$thread_old]['passwd'],
				$GLOBALS['conex'][$thread_old]['database'],
				$GLOBALS['conex'][$thread_old]['replica']);

			unset($GLOBALS['conex'][$thread_old]);

			$result = $dbtype::query($query, $con);

			$errno = db_errno($con);
			$error = db_error($con);
		}
	}

	if ($errno != 0)
	{
		if ($errno == 2006)
		{
			// El MySQL continua parado! Paramos la ejecución para que no entre en bucle!
			$stop_on_error = true;

			if (isset($GLOBALS['consulta_anterior'])
				&& $GLOBALS['consulta_anterior'] != "")
			{
				if (!isset($GLOBALS['errores_mysql']))
				{
					$GLOBALS['errores_mysql'] = "";
				}

				$GLOBALS['errores_mysql'] .= "Consulta MySQL antes del timeout:\n";
				$GLOBALS['errores_mysql'] .= $GLOBALS['consulta_anterior'] . "<br>\n";
			}
		}
		else
		{
			$GLOBALS['consulta_anterior'] = $query;
		}
		
		if ($debug) // Control d'errors - mostrem tot
		{
			$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $GLOBALS['conex'][db_thread_id($con)]['database'];

			if (function_exists("desarrollo")
				&& !desarrollo()
				&& $envia_mail
				&& class_exists("html_mime_mail"))
			{
				if (!isset($GLOBALS['errores_mysql']))
				{
					$GLOBALS['errores_mysql'] = "";
				}

				$GLOBALS['errores_mysql'] .= "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>\n";
				$GLOBALS['errores_mysql'] .= $query . "<br>\n";
			}

			if ((function_exists("desarrollo") && desarrollo()) || (isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == "127.0.0.1" || $_SERVER['SERVER_ADDR'] == "::1")))
			{
				echo ($query . "<br>" . "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>");
			}
			else
			{
				if (ini_get("log_errors") == true)
				{
					$logfile = fopen(sys_get_temp_dir() . "/koobin/php-error.log", "a");
					if (isset($GLOBALS['id_registro_acceso']))
					{
						$txt_reg = $GLOBALS['id_registro_acceso'];
					}
					else
					{
						$txt_reg = "";
					}
					fwrite($logfile, "[" . date("d-M-Y H:i:s") . "] MySQL error " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " - " . $txt_reg . " (" . $maquina . ") Error en Sentencia Mysql (" . $errno . ")" . "\n" . $error . ", File: " . $file . " Line: " . $line . "\n");
					fclose($logfile);
				}
			}
			
			if ($stop_on_error)
			{
				die;
			}
		}
		else // filtrem errors
		{
			if ($errno != db_duplicate_code()) // si no es el codi de duplicate entry, mostrem
			{
				$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $GLOBALS['conex'][db_thread_id($con)]['database'];

				if (function_exists("desarrollo")
					&& !desarrollo()
					&& $envia_mail
					&& class_exists("html_mime_mail"))
				{
					if (!isset($GLOBALS['errores_mysql']))
					{
						$GLOBALS['errores_mysql'] = "";
					}

					$GLOBALS['errores_mysql'] .= "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>\n";
					$GLOBALS['errores_mysql'] .= $query . "<br>\n";
				}

				if (isset($GLOBALS['id_registro_acceso']))
				{
					$txt_reg = $GLOBALS['id_registro_acceso'];
				}
				else
				{
					$txt_reg = "";
				}

				if ((function_exists("desarrollo") && desarrollo()) || (isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == "127.0.0.1" || $_SERVER['SERVER_ADDR'] == "::1")))
				{
					echo ($query . "<br>" . "Error en Sentencia Mysql (" . $errno . ")" . "<br><b>" . $error . ", File: " . $file . " Line: " . $line . "</b><br>");
				}
				else
				{
					if (ini_get("log_errors") == true)
					{
						$logfile = fopen(sys_get_temp_dir() . "/koobin/php-error.log", "a");
						fwrite($logfile, "[" . date("d-M-Y H:i:s") . "] MySQL error " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " - " . $txt_reg . " (" . $maquina . ") Error en Sentencia Mysql (" . $errno . ")" . "\n" . $error . ", File: " . $file . " Line: " . $line . "\n");
						fclose($logfile);
					}
				}
				
				if ($stop_on_error)
				{
					die;
				}
			}
		}
	}
	else
	{
		$GLOBALS['consulta_anterior'] = $query;
	}

	return $result;
}

//Funcions adicionals mysql

//Fetch
//
//	Rep les variables	$result:	Resultat MySql
//						$file		Nom del fitxer des d'on és cridat
//						$line		Linia del fitxer des de on és cridat
//	Retorna un row de Mysql
/**
 * Recorre el result y devuelve el row correspondiente.
 *
 * @param        $result
 * @param string $parametros (optional)
 *
 * @return array|bool Row de recorrer el result
 */
function fetch($result, $parametros = "ASSOC")
{
	$dbtype = DBTYPE;
	return $dbtype::fetch($result, $parametros);
}

/**
 * Devuelve el número de lineas de un result.
 *
 * @param $result
 *
 * @return int
 */
function db_num_rows($result)
{
	$dbtype = DBTYPE;
	return (int) $dbtype::num_rows($result);
}

/**
 * Devuelve el ID insertado en la última consulta
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return int|bool ID
 */
function db_insert_id($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::insert_id($con);
}

/**
 * Devuelve el número de lineas afectadas en la última consulta
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return int número de lineas
 */
function db_affected_rows($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::affected_rows($con);
}

/**
 * Devuelve el código de error de la última consulta
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return int Código de error
 */
function db_errno($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::errno($con);
}

/**
 * Devuelve el código de error al intentar conectar con la BD.
 *
 * @return int Código de error
 */
function db_connect_errno()
{
	$dbtype = DBTYPE;
	
	return $dbtype::connect_errno();
}

/**
 * Devuelve la descripción del error de la última consulta.
 *
 * @param bool $tcon (optional) Identificador de conexión.
 *
 * @return string Descripción del error
 */
function db_error($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::error($con);
}

/**
 * Devuelve la descripción del error del último intento de conexión con la BD.
 *
 * @return string Descripción del error
 */
function db_connect_error()
{
	$dbtype = DBTYPE;
	
	return $dbtype::connect_error();
}

/**
 * Cierra la conexión con la BD.
 *
 * @param resource $tcon Identificador de la conexión
 *
 * @return bool
 */
function db_close($tcon)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;

	if (isset($GLOBALS['conex'][db_thread_id($con)]))
	{
		unset($GLOBALS['conex'][db_thread_id($con)]);
	}

	return $dbtype::close($con);
}

/**
 * Establece la codificación para la conexión con la BD.
 * @param string    $charset
 * @param $tcon (optional) Identificador de conexión con la BD.
 *
 * @return bool
 */
function db_set_charset($charset, $tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::set_charset($charset, $con);
}

/**
 * Hace un Ping a la BD para ver si la conexión está activa.
 * @param $tcon (optional) Identificador de la conexión
 *
 * @return bool
 */
function db_ping($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::ping($con);
}

/**
 * Mueve la posición del resultado a la linea que se le diga.
 * @param $result
 * @param $row_number
 *
 * @return bool
 */
function db_data_seek($result, $row_number)
{
	$dbtype = DBTYPE;
	return $dbtype::data_seek($result, $row_number);
}

/**
 * Devuelve el número de campos que tiene el resultado de una consulta.
 * @param $result
 *
 * @return bool
 */
function db_num_fields($result)
{
	$dbtype = DBTYPE;
	return $dbtype::num_fields($result);
}

/**
 * Devuelve la siguiente linea del resultado de una consulta.
 * @param $result
 * @return mixed
 */
function db_fetch_row($result)
{
	$dbtype = DBTYPE;
	return $dbtype::fetch_row($result);
}

/**
 * Devuelve el nombre de un campo del resultado de una consulta
 * @param $result
 * @param $field_offset
 * @return mixed
 */
function db_field_name($result, $field_offset)
{
	$dbtype = DBTYPE;
	return $dbtype::field_name($result, $field_offset);
}

/**
 * Devuelve el código de error de registro duplicado
 * @return int
 */
function db_duplicate_code()
{
	$dbtype = DBTYPE;
	return $dbtype::duplicateCode;
}

/**
 * Libera el resultado de una consulta
 *
 * @param $result
 * @return mixed
 */
function db_free_result($result)
{
	$dbtype = DBTYPE;
	return $dbtype::free_result($result);
}

/**
 * Devuelve el identificador de la conexión
 * @param bool $tcon
 * @return mixed
 */
function db_thread_id($tcon = false)
{
	if (!$tcon)
	{
		global $con;
	}
	else
	{
		$con = $tcon;
	}
	$dbtype = DBTYPE;
	return $dbtype::thread_id($con);
}

/**
 * Escapa la cadena según lo que especifica la conexión a la BD
 * @param $escapestr
 * @param bool $tcon
 * @return string
 */
function db_real_escape_string($escapestr, $tcon = false)
{
	if (!is_bool($escapestr))
	{
		if (!$tcon)
		{
			global $con;

			if (!$con)
			{
				$con = db_connect(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWD, MYSQL_DATABASE);

				if(!$con) {
					display_db_error(db_connect_errno(), db_connect_error(), MYSQL_HOST, MYSQL_USERNAME, MYSQL_DATABASE);
					die;
				}
			}
		}
		else
		{
			$con = $tcon;
		}
		$dbtype = DBTYPE;
		return $dbtype::real_escape_string($escapestr, $con);
	}
	else
	{
		return "";
	}
}

/**
 * Muestra la información de un error y los datos de donde ha ocurrido
 * @param int $errno
 * @param string $error
 * @param string $host
 * @param string $username
 * @param string $db
 * @param string|bool $file (opcional)
 * @param int|bool $line (opcional)
 */
function display_db_error($errno, $error, $host, $username, $db, $file = false, $line = false)
{
	if ((defined("SHOW_ERROR_MYSQL")
		&& SHOW_ERROR_MYSQL)
		|| is_debug())
	{
		echo "errno:" . $errno . "<br>\n";
		echo "error:" . $error . "<br>\n";
		echo "host:" . $host . "<br>\n";
		echo "username:" . $username . "<br>\n";
		echo "db:" . $db . "<br>\n";
		if ($file)
		{
			echo "file: " . $file . "<br>\n";
		}
		if ($line)
		{
			echo "line: " . $line . "<br>\n";
		}
		echo "from:" . $_SERVER['SERVER_ADDR'] . "<br>\n";
	}
}

/**
 * Devuelve los segundos de restraso de la conexión de réplica respecto al master
 * @param $con
 * @return bool|null|int
 */
function slave_status($con)
{
	if ($GLOBALS['conex'][db_thread_id($con)]['replica'])
	{
		$result = query("SHOW SLAVE STATUS", __FILE__, __LINE__, $con);
		if ($row = fetch($result))
		{
			return $row['Seconds_Behind_Master'];
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return false;
	}
}

/**
 * Devuelve el código de error de constraint
 * @return int
 */
function db_constraint_code()
{
	$dbtype = DBTYPE;
	return $dbtype::constraintCode;
}

function db_slave_delay($tcon = false, $retry = 10)
{
	if (!$tcon)
	{
		global $con;

		if (!$con)
		{
			$con = db_connect(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWD, MYSQL_DATABASE);

			if(!$con) {
				display_db_error(db_connect_errno(), db_connect_error(), MYSQL_HOST, MYSQL_USERNAME, MYSQL_DATABASE);
				die;
			}
		}
	}
	else
	{
		$con = $tcon;
	}

	if ($GLOBALS['conex'][db_thread_id($con)]['replica'])
	{
		// Es una réplica
		$delay = slave_status($con);

		if ($delay === NULL)
		{
			$maquina = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $GLOBALS['conex'][db_thread_id($con)]['database'];

			$mail = new html_mime_mail;

			$asunto = "Ocurrido error en " . $GLOBALS['conex'][db_thread_id($con)]['database'] . " (" . $maquina . ")";

			$body = "La réplica está rota.</b><br>\n";
			$mail->add_html($body, "");
			$mails[] = "monitor@koobin.com";
			$mail->send("monitor@koobin.com", $mails, "monitor@koobin.com", "monitor@koobin.com", $asunto);
		}

		if ($delay > 0
			&& $retry > 0)
		{
			$cont = 0;
			while ($delay > 0
				|| $cont < $retry)
			{
				sleep(1);
				$delay = slave_status($con);
			}
		}
		return $delay;
	}
}