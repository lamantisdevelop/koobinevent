<?php

class mail
{
	/**
	 * @param int $fecha_ini
	 * @param int|false $fecha_fin
	 * @return array
	 */
	static function estado($fecha_ini, $fecha_fin)
	{
		$con_log = db_connect(MYSQL_LOG_HOST, MYSQL_LOG_USERNAME, MYSQL_LOG_PASSWD, MYSQL_LOG_DATABASE);

		$output = Array();
		if ($con_log)
		{
			$query = "SELECT M.messageId, M.bd, D.para, M.de, M.fecha_envio, M.asunto, M.tipo, M.identificador, D.estado, D.fecha_actualizacion, D.status, D.diagnosticCode, D.smtpResponse, D.tipo as tipo_rebote, D.subtipo FROM maillog M, maillog_detalle D " .
				"WHERE M.messageId = D.messageId " .
				"AND M.fecha_envio >= '" . db_real_escape_string($fecha_ini, $con_log) . "' ";
			if ($fecha_fin)
			{
				$query .= "AND M.fecha_envio <= '" . db_real_escape_string($fecha_fin, $con_log) . "' ";
			}

			$query .= "ORDER BY M.fecha_envio";

			$result = query($query, __FILE__, __LINE__, $con_log);

			while ($row = fetch($result))
			{

				$nombre_cliente = $row['bd'];

				switch ($row['estado'])
				{
					case "ENTREGADO":
						$estado = "<span style=\"color: green;\" title=\"" . quotreplace($row['smtpResponse']) . "\">" . $row['estado'] . "</span>";
						break;

					case "REBOTADO":
						$estado = "<span style=\"color: red;\" title=\"" . quotreplace($row['status'] . " " . $row['diagnosticCode']) . "\">" . $row['estado'] . "</span>";
						break;

					case "SPAM":
						$estado = "<span style=\"color: red;\" title=\"" . quotreplace($row['tipo_rebote'] . " " . $row['subtipo']) . "\"><strong>" . $row['estado'] . "</strong></span>";
						break;

					case "ENVIADO":
						$estado = "<span style=\"color: orange;\"><strong>" . $row['estado'] . "</strong></span>";
						break;

					default:
						$estado = $row['estado'];
				}

				if (mb_strlen($row['asunto']) > 50)
				{
					$asunto = "<span title=\"" . quotreplace($row['asunto']) . "\">" . mb_substr($row['asunto'], 0,
							50) . "...</span>";
				}
				else
				{
					$asunto = $row['asunto'];
				}

				if (mb_strlen($nombre_cliente) > 50)
				{
					$nombre_cliente = "<span title=\"" . quotreplace($nombre_cliente) . "\">" . mb_substr($nombre_cliente,
							0, 50) . "...</span>";
				}

				$output['aaData'][] = Array(
					$row['messageId'],
					$nombre_cliente,
					$row['de'],
					$row['para'],
					$row['fecha_envio'],
					date("d/m/Y H:i:s", $row['fecha_envio']),
					$asunto,
					$row['tipo'],
					$row['identificador'],
					$estado,
					$row['fecha_actualizacion'],
					date("d/m/Y H:i:s", $row['fecha_actualizacion'])
				);
			}
		}

		return $output;
	}
}
