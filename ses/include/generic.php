<?php

/** Se nos notifica con un email a la direccion monitor@koobin.com, con el contenido que se le especifique y el identificador de registro.
 * @param string $asunto Asunto del email
 * @param string $error Cuerpo del mensaje
 */
function notifica_error($asunto, $error)
{
	$mail = new html_mime_mail;

	if (isset($GLOBALS['id_registro_acceso']) && $GLOBALS['id_registro_acceso'])
	{
		$error .= "<br />\nID Registro: " . $GLOBALS['id_registro_acceso'];
	}

	$mail->add_html($error, "");

	$asunto = "[SES] " . $asunto;
	$mails[] = "monitor@koobin.com";

	if(isset($mails))
		$mail->send("Monitor Koobin", $mails, "Koobinevent SL", "monitor@koobin.com", $asunto);

}

function quotreplace($cadena)
{
	return str_replace("\"", "&quot;", $cadena);
}