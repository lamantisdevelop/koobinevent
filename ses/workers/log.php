<?php
/**
 * Inspecciona el fichero de logs y ejecuta las consultas
 */

mb_internal_encoding("UTF-8");
date_default_timezone_set('Europe/Madrid');

chdir(dirname(__FILE__));
chdir("..");

include("include/generic.php");
include("include/log.php");
include("include/mime.mail.class.php");
define("DBTYPE", "dbmysqli");

include("include/db.funciones.php");

include("include/DB/" . DBTYPE . ".php");

if (isset($_ENV['desarrollo'])
	&& (strcasecmp($_ENV['desarrollo'], "true") == 0))
{
	include("include/DEV/logs.cfg.php");
	define("__DESARROLLO", true);
}
else
{
	include("cfg/logs/init.cfg.php");
}

$con = false;

define("PATH", str_replace("\\", "/", realpath(getcwd())));
define("TMP_PATH", sys_get_temp_dir() . "/koobin");

error_reporting(E_ALL);
ini_set("display_errors", "Off");
ini_set("log_errors", true);
if (!is_dir(sys_get_temp_dir() . "/koobin/"))
{
	mkdir(sys_get_temp_dir() . "/koobin/");
}
ini_set("error_log", sys_get_temp_dir() . "/koobin/php-error.log");

// Guardamos el timestamp de última modificación del worker, por si hay que matarlo para que se reinicie
clearstatcache(true, __FILE__);
$ultima_modificacion = @filemtime(__FILE__);

if (PHP_OS == "WINNT")
{
	$log_path = "C:/WINDOWS/TEMP/koobin";
}
else
{
	$log_path = sys_get_temp_dir() . "/koobin";
}

$log_file = $log_path . "/log.log";
$offset_file = $log_path . "/offsetlog.log";

if (is_file($offset_file))
{
	$ultimo_tamanyo = file_get_contents($offset_file);

	if ($ultimo_tamanyo == "") $ultimo_tamanyo = 0;
}
else
{
	$ultimo_tamanyo = 0;
}

//echo "Inspeccionando fichero " . $log_file . "\n";
$handle = false;
while (true)
{
	if (!$handle)
	{
		clearstatcache(true, $log_file);
		$nuevo_tamanyo = @filesize($log_file);

		if ($nuevo_tamanyo < $ultimo_tamanyo)
		{
//			echo "Ha encogido el fichero\n";
			$legacy = true;
			// El fichero ha encogido. Es posible que haya rotado
			touch($offset_file);

			if (is_file($log_file . "-" . date("Ymd")))
			{
				//echo "Existe el rotado\n";
				$handle = @fopen($log_file . "-" . date("Ymd"), 'r');
				if (!isset($fin_fichero))
				{
					//fseek($handle, 0, SEEK_END);
					//$fin_fichero = ftell($handle);
					touch($offset_file);
					$fin_fichero = file_get_contents($offset_file);
					if ($fin_fichero == "")
					{
						$fin_fichero = 0;
					}
				}
			}
			elseif (is_file($log_file . ".1"))
			{
				//echo "Existe el rotado\n";
				$handle = @fopen($log_file . ".1", 'r');
				if (!isset($fin_fichero))
				{
					//fseek($handle, 0, SEEK_END);
					//$fin_fichero = ftell($handle);
					touch($offset_file);
					$fin_fichero = file_get_contents($offset_file);
					if ($fin_fichero == "")
					{
						$fin_fichero = 0;
					}
				}
			}
			else
			{
				$legacy = false;
				$handle = false;
			}
		}
		else
		{
//			echo "Último tamaño / Nuevo tamaño\n";
//			echo $ultimo_tamanyo . "\n";
//			echo $nuevo_tamanyo . "\n";

			$legacy = false;
			$handle = @fopen($log_file, 'r');
			if (!isset($fin_fichero))
			{
				//fseek($handle, 0, SEEK_END);
				//$fin_fichero = ftell($handle);
				touch($offset_file);
				$fin_fichero = file_get_contents($offset_file);
				if ($fin_fichero == ""
					|| $fin_fichero > $ultimo_tamanyo)
				{
					$fin_fichero = 0;
				}
			}
		}

		if ($handle)
		{
			fseek($handle, $fin_fichero);
		}
	}

	if ($handle)
	{
		$data = fgets($handle);
		$nuevo_fin_fichero = ftell($handle);

		if ($data && $data != "")
		{
			if (!$con)
				$con = db_connect(MYSQL_LOG_HOST, MYSQL_LOG_USERNAME, MYSQL_LOG_PASSWD, MYSQL_LOG_DATABASE);
			if ($con)
			{
				query($data, __FILE__, __LINE__, $con, false, false, false);
				if (db_errno() != 0
					&& db_errno() != db_duplicate_code()
				)
				{
					// Ha ocurrido un error en esta consulta.
					$error = db_error() . "\n<br>";
					$error .= $data;
					notifica_error("ERROR REGISTRO ACCESO", $error);
				}

				$ultimo_tamanyo = $nuevo_tamanyo;
				$fin_fichero = $nuevo_fin_fichero;

				if (!$legacy)
				{
					file_put_contents($offset_file, $fin_fichero);
				}
			}
		}
		else
		{
			if ($legacy)
			{
//				echo "Salimos del legacy.\n";
				$ultimo_tamanyo = 0;
				$fin_fichero = 0;
				$legacy = false;
				file_put_contents($offset_file, $fin_fichero);
			}

			fclose($handle);
			$handle = false;
		}
	}
	else
	{
		$data = "";
	}

	if (strlen($data) == 0)
	{
		if ($con)
		{
			db_close($con);
			$con = false;
		}
		sleep(10);
	}
	else
	{
		usleep(10000);
	}

	clearstatcache(true, __FILE__);
	$ts_ultima_modificacion = @filemtime(__FILE__);
	if ($ts_ultima_modificacion != $ultima_modificacion)
	{
		die;
	}
}

