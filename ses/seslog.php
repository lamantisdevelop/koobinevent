<?php

error_reporting(E_ALL);
ini_set("display_errors", "Off");
ini_set("log_errors", true);

if (!is_dir(sys_get_temp_dir() . "/koobin/"))
{
	mkdir(sys_get_temp_dir() . "/koobin/");
}
ini_set("error_log", sys_get_temp_dir() . "/koobin/php-error.log");

mb_internal_encoding("UTF-8");
date_default_timezone_set('Europe/Madrid');

include("include/generic.php");
include("include/log.php");
include("include/seslog.php");
include("include/workConstantes.php");

$workConstantes = new workConstantes();

$HTTP_RAW_POST_DATA = file_get_contents("php://input");

$myFile = sys_get_temp_dir() . "/koobin/ses.log";
$fh = fopen($myFile, 'a');
fwrite($fh, $HTTP_RAW_POST_DATA . "\n");
fclose($fh);

//$HTTP_RAW_POST_DATA = file_get_contents("test.txt");

//file_put_contents("/tmp/log.txt", json_encode($vars) . "\n");
if (!is_dir(sys_get_temp_dir() . "/koobin"))
{
	@mkdir(sys_get_temp_dir() . "/koobin/");
}
$respuesta = json_decode($HTTP_RAW_POST_DATA, true);

if (isset($respuesta['Message']))
{
	$notificacion = json_decode($respuesta['Message'], true);

	if (isset($notificacion['mail']))
	{
		$hora = strtotime($notificacion['mail']['timestamp']);
		log::mailLog($notificacion['mail']['messageId'], $notificacion['mail']['source'],
			$notificacion['mail']['destination'], "", $hora);
	}

	switch (strtolower($notificacion['notificationType']))
	{
		case "bounce":
			$hora = strtotime($notificacion['bounce']['timestamp']);
			$estado = Array();
			$estado['tipo'] = $notificacion['bounce']['bounceType'];
			$estado['subtipo'] = $notificacion['bounce']['bounceSubType'];

			if (isset($notificacion['bounce']['smtpResponse']))
			{
				$estado['smtpResponse'] = $notificacion['bounce']['smtpResponse'];
			}

			if (isset($notificacion['bounce']['reportingMTA']))
			{
				$estado['reportingMTA'] = $notificacion['bounce']['reportingMTA'];
			}
			else
			{
				$estado['reportingMTA'] = "";
			}

			foreach ($notificacion['bounce']['bouncedRecipients'] as $detalle_bounce)
			{
				if (isset($detalle_bounce['action']))
				{
					$estado['action'] = $detalle_bounce['action'];
				}
				elseif(isset($estado['action']))
				{
					unset($estado['action']);
				}

				if (isset($detalle_bounce['status']))
				{
					$estado['status'] = $detalle_bounce['status'];
				}
				elseif(isset($estado['status']))
				{
					unset($estado['status']);
				}

				if (isset($detalle_bounce['diagnosticCode']))
				{
					$estado['diagnosticCode'] = $detalle_bounce['diagnosticCode'];
				}
				elseif(isset($estado['diagnosticCode']))
				{
					unset($estado['diagnosticCode']);
				}

				log::updateMailLog($notificacion['mail']['messageId'], $detalle_bounce['emailAddress'], 'REBOTADO',
					$hora, $estado);
			}
			break;

		case "delivery":
			$hora = strtotime($notificacion['delivery']['timestamp']);

			$estado = Array();
			if (isset($notificacion['delivery']['smtpResponse']))
			{
				$estado['smtpResponse'] = $notificacion['delivery']['smtpResponse'];
			}
			else
			{
				$estado['smtpResponse'] = "";
			}

			if (isset($notificacion['delivery']['reportingMTA']))
			{
				$estado['reportingMTA'] = $notificacion['delivery']['reportingMTA'];
			}
			else
			{
				$estado['reportingMTA'] = "";
			}
			foreach ($notificacion['delivery']['recipients'] as $email)
			{
				log::updateMailLog($notificacion['mail']['messageId'], $email, 'ENTREGADO',
					$hora, $estado);
			}
			break;

		case "complaint":
			$hora = strtotime($notificacion['complaint']['timestamp']);
			$estado = Array();
			$estado['tipo'] = $notificacion['complaint']['complaintFeedbackType'];
			$estado['subtipo'] = $notificacion['complaint']['userAgent'];
			foreach ($notificacion['complaint']['complainedRecipients'] as $email)
			{
				log::updateMailLog($notificacion['mail']['messageId'], $email['emailAddress'], 'SPAM',
					$hora, $estado);
			}
			break;

		default:
			$myFile = sys_get_temp_dir() . "/koobin/ses.log";
			$fh = fopen($myFile, 'a');
			fwrite($fh, $notificacion['notificationType'] . ": \n" . $HTTP_RAW_POST_DATA . "\n");
			fclose($fh);
	}
}
else
{
	log::saveLog("SES", $HTTP_RAW_POST_DATA);
}
