<?php

error_reporting(E_ALL);
ini_set("display_errors", "On");
ini_set("log_errors", true);
if (!is_dir(sys_get_temp_dir() . "/koobin/"))
{
	mkdir(sys_get_temp_dir() . "/koobin");
}
ini_set("error_log", sys_get_temp_dir() . "/koobin/php-error.log");

ini_set("memory_limit", "800M");

$ips_debug = Array();
$ips_debug[] = "84.89.61.129";
$ips_debug[] = "91.126.115.223";
if ((isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], $ips_debug)) ||
	(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && in_array($_SERVER['HTTP_X_FORWARDED_FOR'], $ips_debug))
	||	(isset($_SERVER['HTTP_CF_CONNECTING_IP']) && in_array($_SERVER['HTTP_CF_CONNECTING_IP'], $ips_debug)))
{
	define("DBTYPE", "dbmysqli");
	include("include/generic.php");
	include("cfg/logs/init.cfg.php");
	include("include/db.funciones.php");
	include("include/DB/" . DBTYPE . ".php");
	include("include/mail.php");

	if (!isset($_POST['action']))
	{
		$_POST['action'] = "";
	}

	switch ($_POST['action'])
	{
		case "PC_estado_mails":
			header("Content-Type: application/json");
			$fecha_fin = false;
			$fecha = new DateTime();
			$fecha->sub(new DateInterval('P15D'));
			$fecha->setTime(0, 0);
			$fecha_ini = $fecha->getTimestamp();

			$output = mail::estado($fecha_ini, $fecha_fin);

			echo json_encode($output);
			break;

		default:
			echo file_get_contents("templates/status.html");
			break;
	}
}
else
{
	header("Location: https://www.koobinevent.com");
	exit(0);
}
