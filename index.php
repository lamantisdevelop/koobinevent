<?php
/*
  Web modular v.1.0
  La Mantis- mantis.cat
 */

//REDIRECT SITE EN MANTENIMENT
//if (!isset($_COOKIE['manteniment_koobin_glwmvkso5t0wo9e0o00_mantis'])) {
//    header('location:http://www.mantis.cat');
//}

if (!(getenv('HTTPS') == 'on'))
{
	header('location:https://www.koobinevent.com');
	die;
}
/*
  Tasques que es repeteixen a cada recàrrega de l'index.php - INICI
 */
require('./includes/application_top.php');

/*
  Obtenim info de la pàgina a través del titol (page_title) sinó default
 */
if (tep_not_null($_GET['page_title'])) {
    $page_query = tep_db_query("select * from " . TABLE_PAGES . " p left join " . TABLE_PAGES_DESCRIPTION . " pd on pd.pages_id = p.id and pd.language_id = '" . (int) tep_get_languages_id($_GET['language']) . "' where page_title = '" . $_GET['page_title'] . "' AND p.active=1 ");
} else {
    $page_query = tep_db_query("select * from " . TABLE_PAGES . " p left join " . TABLE_PAGES_DESCRIPTION . " pd on pd.pages_id = p.id and pd.language_id = '" . (int) $language_id . "' where p.id = '" . DEFAULT_PAGE . "'");
}
$page_info = tep_db_fetch_array($page_query);

//CAS titol pagina inexistent llancem un 404 i fem que carregui la pagina per defecte (inici)
if (!is_array($page_info)) {
    header("HTTP/1.1 404 Not Found");
    header("Status: 404 Not Found");
    $page_query = tep_db_query("select * from " . TABLE_PAGES . " p left join " . TABLE_PAGES_DESCRIPTION . " pd on pd.pages_id = p.id and pd.language_id = '" . (int) $language_id . "' where p.id = '" . DEFAULT_PAGE . "'");
    $page_info = tep_db_fetch_array($page_query);
}

//CAS mostrar tipus pàgina
if ($page_info) {
    // Recuperem info de la pàgina
    $_GET['pageID'] = $page_info['pages_id'];
    $_GET['parentID'] = $page_info['parent_id'];
    (tep_not_null($page_info['modul']) && $page_info['action'] == 'modul') ? $_GET['modul'] = $page_info['modul'] : $_GET['modul'] = 'pages';

    // Parse Vars
    $vars = @explode('&', $page_info['vars']);
    $container = array();

    foreach ($vars as $index => $valuepair) {
        $val = @explode('=', $valuepair);
        $_GET[$val[0]] = $val[1];
    }
}

/*
  Carreguem textos mòdul
 */
if (tep_not_null($_GET['modul'])) {
    if (file_exists(DIR_WS_PORTAL_TEXTS . $language . '/' . $_GET['modul'] . '.php')) {
        require(DIR_WS_PORTAL_TEXTS . $language . '/' . $_GET['modul'] . '.php');
    }
}

/*
  Definim rutes als components del mòdul
 */
$path_modul = DIR_WS_MODULS . $_GET ['modul'];
//Definim layout segons dispositiu
$path_layout =  DIR_WS_LAYOUTS . LAYOUT_DEFECTE;

/*
  Carreguem arxius corresponents al módul
 */
if (file_exists($path_layout)) {
    include($path_layout);
} else {
    if (!is_dir($path_modul)) {
        die('Error al cargar el módulo <b>' . $modulo . '</b>. No existe el modulo <b>' . $path_modul . '</b>.');
    }
}

/*
  Tasques que es repeteixen a cada recàrrega de l'index.php - FI
 */
require('./includes/application_bottom.php');
?>
