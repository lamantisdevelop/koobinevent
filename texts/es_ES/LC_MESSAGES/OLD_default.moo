��          �      L      �     �     �     �     �     �               2  
   @     K     T     ]     k     w          �     �     �  �  �     �     �     �  ?   �  /   )     Y  O   `  	   �  	   �  	   �     �     �     �  	   �  v         w  �   �  m  *           	                                               
                                          Email Empresa Enviar Error en enviar el formulari Error formulari captcha Esborrar Formulari enviat correctament META Keywords META Title Mes info Missatge Nom i cognoms Nova imatge Telefon Text avis cookies Text de contacte peu de pagina avis formulari contacte avis formulari contacte protect Project-Id-Version: CMS BASE
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-02 12:33+0100
PO-Revision-Date: 2014-06-30 09:41+0100
Last-Translator: xavi <xavi@mantis.cat>
Language-Team: La Mantis <xavi@mantis.cat>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Generator: Poedit 1.6.5
Language: es_ES
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ../../..
 Correo electrónico Empresa Enviar Error en enviar el formulario, por favor, inténtelo más tarde El código de validación entrado es incorrecto Borrar Formulario enviado correctamente, en breve nos pondremos en contacto con usted. La Mantis La Mantis Más info Mensaje Nombre y apellidos Nueva imagen Teléfono Este web utiliza "cookies" propias y de terceros para ofrecerle un mejor servicio, al navegar el usuario acepta su uso Dirección pié de página <p>Los datos que no proporciona no se incorporaran a ningún fichero y únicamente se utilizarán para responder a sus solicitudes de información.</p> <p>De acuerdo con lo establecido por la Ley Orgánica 15/1999, le informamos que los datos            obtenidos de este formulario serán incorporados a un fichero automatizado bajo la            responsabilidad de XXXXXXXXXXXXX con la finalidad de atender sus consultas y            remitirle información relacionada que pueda ser de su interés.</p>             <p>Puede ejercer sus derechos de acceso, rectificación, cancelación y oposición mediante un escrito a nuestra dirección c/ XXXXXXXXXXXXX, Nº  CODI POSTAL POBLACIÓ.</p>            <p>Mientras no nos comunique lo contrario, entenderemos que sus datos no han sido modificados, que usted se compromete a notificarnos cualquier variación y que tenemos su consentimiento para utilizarlos para las finalidades mencionadas.</p>            <p>El envío de estos datos implica la aceptación de esta cláusula.</p> 