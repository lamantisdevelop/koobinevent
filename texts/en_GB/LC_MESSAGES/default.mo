��    +      t  ;   �      �     �     �     �  	   �  	                       9  5   V     �     �     �     �     �     �     �     �            	   ,     6  	   T     ^     d     t     �     �     �  
   �     �     �     �     �     �     �     �     	          :     >     V    v          �     �     �     �     �     �  
   �     �  -   �     "	     1	     7	     ?	     P	     ]	  �   b	  !   
     (
     /
     M
  j   R
     �
     �
  �   �
     e     s  �   |  x   �  ,   x  
   �     �     �  	   �  	   �     �     �  \   �  �   >     �  �   �  �  o                                 %                             $      
         '             (                                +       #   &      	                    )   !           "              *           + INFO Altres Notícies de Koobin Altres elements del llistat Aplicacio Calendari Cercador Client Com Arribar a Koobin en cotxe Darreres Notícies de Koobin El formulari s'està enviant, un moment si us plau... Element no trobat Email Empresa Envia un correu Envia'ns el CV Enviar Error en enviar el formulari Error formulari captcha Esborrar Escriu el codi de la imatge Etiquetes Formulari enviat correctament Historial Inici Intro formulari Koobin Tweets Localització META Description META Keywords META Title Mes info Missatge No hi ha elements Noticies Nova imatge Telefon Text a cercar Text avis cookies Text de contacte peu de pagina URL avis formulari contacte avis formulari contacte protect Project-Id-Version: CMS BASE
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-11-14 10:35+0100
PO-Revision-Date: 2017-11-14 10:36+0100
Last-Translator: xavi <xavi@mantis.cat>
Language-Team: La Mantis <xavi@mantis.cat>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Generator: Poedit 1.5.4
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: ../../..
 + INFO Other Koobin News Other elements of the list Application Calendar Search Customer Directions Koobin Last News The form is being sent, one moment please ... Item not found Email Company Send us an email Send your CV Send <p><b>El seu missatge no ha pogut ser enviat.</b><br/>Hi ha hagut un error en el procés d'enviament.<br/><a href="javascript:back(-1);">Torni a provar-ho.</a></p> Please enter a correct image code Delete Enter the code from the image Tags <p><b>El seu missatge s'ha enviat amb èxit.</b><br/>Ben aviat rebrà una resposta a la seva consulta.</p> History Home <p>If you want to contact us or have a question, comment or suggestion to us, please send and email. We will contact you shortly. Thank you very much!</p> Koobin Tweets Location Technological innovation for your business. Software systems for sports, cultural activities and management of all kinds of events Sport, Football, Basketball, Theatre, Opera, Theme Park, Meeting Congress, Aquatic, Festival, Concert, Culture, Cultural Koobin: Sports & Cultural Events Management. Learn more Message No items Notícies New image Tel. Search Cookies help us deliver our services. By using our services, you agree to our use of cookies <p><b>KOOBINEVENT SL</b></p><p>PARC CIENTÍFIC i TECNOLÒGIC DE LA UDG · CENTRE D'EMPRESES</p><p>pic de peguera, 11 · es17003 · girona · spain</p> URL <p>Les dades que ens proporciona no s'incorporaran a cap fitxer i únicament s'utilitzaran per a respondre les seves sol·licituds d'informació.</p>  <p>Us informem que les dades personals obtingudes a partir d’aquest formulari, així com la vostra adreça de correu electrònic, seran incloses en un fitxer del que n’és responsable XXXXXXXXXXXXX, amb la finalitat d’atendre les vostres consultes i remetre-us informació relacionada i que pogués ser del vostre interès.</p>            <p>XXXXXXXXXXXXX es compromet a l’ús exclusiu de les dades  recollides mitjançant aquest formulari amb la finalitat mencionada anteriorment.</p>            <p>L’interessat declara tenir coneixement del destí i ús de les dades personals recollides mitjançant la lectura de la present clàusula.</p>            <p>L’enviament d’aquest e-mail implica l’acceptació de les clàusules exposades.</p>            <p>Si desitgeu exercir els drets d’accés, rectificació, cancel.lació o oposició, en els termes que estableix la Llei Orgànica 15/1999, podeu fer-ho a la següent adreça: XXXXXXXXXXXXX c/ XXXXXXXXXXXXX, Nº  CODI POSTAL POBLACIÓ.</p> 