��          �      ,      �     �     �     �     �     �     �     �  
                        )     1     C     b     z  �  �     x  �        #  j   ,  �   �  �   R  �   �  ,   m  	   �     �  	   �  	   �  x   �  �   :  �   �  �  e	                     	       
                                                  Enviar Error en enviar el formulari Esborrar Formulari enviat correctament Intro formulari META Description META Keywords META Title Mes info Missatge Noticies Telefon Text avis cookies Text de contacte peu de pagina avis formulari contacte avis formulari contacte protect Project-Id-Version: CMS BASE
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-30 09:37+0100
PO-Revision-Date: 2014-06-30 10:20+0100
Last-Translator: xavi <xavi@mantis.cat>
Language-Team: La Mantis <xavi@mantis.cat>
Language: ca_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Generator: Poedit 1.6.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ../../..
 Enviar <p><b>El seu missatge no ha pogut ser enviat.</b><br/>Hi ha hagut un error en el procés d'enviament.<br/><a href="javascript:back(-1);">Torni a provar-ho.</a></p> Esborrar <p><b>El seu missatge s'ha enviat amb èxit.</b><br/>Ben aviat rebrà una resposta a la seva consulta.</p> Si vols contactar amb nosaltres o tens alguna pregunta, comentari o suggeriment a fer-nos, omple aquest formulari. Ens posarem en contacte amb tu al més aviat possible. Moltes gràcies! Innovació tecnològica per al seu negoci. Sistemes de programari per a activitats esportives, culturals i de tot tipus de gestió d'esdeveniments Esport, Futbol, Bàsquet, Basket,Teatre, Òpera, Parc, Parc temàtic, Reunió, Congrés, Aquàtic, Festival, Concert, Cultura, Cultural Koobin: Sports & Cultural Events Management. Més info Missatge Notícies Telèfons Aquest lloc web utilitza "cookies" pròpies i de tercers per oferir-te un millor servei, en navegar-hi n'acceptes l'ús. <p><b>KOOBINEVENT SL</b></p><p>PARC CIENTIFIC I TECNOLOGIC DE LA UDG · CENTRE D'EMPRESES</p><p>pic de peguera, 11 · es17003 · girona · spain</p> <p>Les dades que ens proporciona no s'incorporaran a cap fitxer i únicament s'utilitzaran per a respondre les seves sol·licituds d'informació.</p>  <p>Us informem que les dades personals obtingudes a partir d’aquest formulari, així com la vostra adreça de correu electrònic, seran incloses en un fitxer del que n’és responsable XXXXXXXXXXXXX, amb la finalitat d’atendre les vostres consultes i remetre-us informació relacionada i que pogués ser del vostre interès.</p>            <p>XXXXXXXXXXXXX es compromet a l’ús exclusiu de les dades  recollides mitjançant aquest formulari amb la finalitat mencionada anteriorment.</p>            <p>L’interessat declara tenir coneixement del destí i ús de les dades personals recollides mitjançant la lectura de la present clàusula.</p>            <p>L’enviament d’aquest e-mail implica l’acceptació de les clàusules exposades.</p>            <p>Si desitgeu exercir els drets d’accés, rectificació, cancel.lació o oposició, en els termes que estableix la Llei Orgànica 15/1999, podeu fer-ho a la següent adreça: XXXXXXXXXXXXX c/ XXXXXXXXXXXXX, Nº  CODI POSTAL POBLACIÓ.</p> 