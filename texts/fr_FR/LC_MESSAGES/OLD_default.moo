��          �      ,      �     �     �     �     �     �     �     �  
   �     �                    )     1     C     b  �  z     X  
   ^     i  d   q     �  |   �  	   [  	   e     o     ~     �     �     �  ~   �       �   7                      	                       
                                 Email Empresa Enviar Error en enviar el formulari Esborrar Formulari enviat correctament META Keywords META Title Mes info Missatge Nom i Cognoms Nom i cognoms Telefon Text avis cookies Text de contacte peu de pagina avis formulari contacte Project-Id-Version: CMS BASE
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-02 12:33+0100
PO-Revision-Date: 2014-06-30 09:41+0100
Last-Translator: xavi <xavi@mantis.cat>
Language-Team: La Mantis <xavi@mantis.cat>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Generator: Poedit 1.6.5
Language: fr_FR
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ../../..
 Email Entreprise Envoyer <p><b>Votre message n'a pas pu être envoyé.</b><br/>Il y a eu une erreur lors de l'envoi.<br/></p> Effacer <p><b>Votre message a été envoyé avec succès.</b><br/>Nous répondrons à votre demande dans les plus brefs délais.</p> La Mantis La Mantis En savoir plus Commentaires Nom Nom Téléphone Les cookies assurent le bon fonctionnement de nos services. En utilisant ces derniers, vous acceptez l'utilisation des cookies Direcció peu de pàgina <p>Les données que vous nous fournissez ne seront versées dans aucun fichier et ne seront utilisées que pour répondre à vos demandes de renseignements.</p> 