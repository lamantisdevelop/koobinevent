<?php ?>
<script type="text/javascript">

    $(document).ready(function() {
        //<![CDATA[
        /*validacio formularis html5 ie9-
         $('#form_data').html5form({
         allBrowsers : true,
         responseDiv : '#response',
         messages: 'en',
         messages: 'es',
         method : 'GET',
         colorOn :'#6b6764'
         });*/


        $('#avis_cookies').slideDown('medium', 'swing');
        $('#avis_cookies .tancar_cookie').click(function() {
            $('#avis_cookies').slideUp('slow', 'swing');
            var today = new Date();
            var expire = new Date();
            expire.setTime(today.getTime() + 3600000 * 24 * 300);//cookie per un any
            document.cookie = 'cookies_acceptades=true;expires=' + expire.toGMTString();
        });


        $('body').fadeIn('slow');

        // Slider REVOLUTION   
        if ($.fn.cssOriginal != undefined)
            $.fn.css = $.fn.cssOriginal;

        $('.banner.main').revolution(
                {
                    delay: 5000,
                    startheight: 500,
                    startwidth: 1300,
                    hideThumbs: 200,
                    thumbWidth: 100, // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                    thumbHeight: 50,
                    thumbAmount: 5,
                    navigationType: "none", //bullet, thumb, none, both		(No Thumbs In FullWidth Version !)
                    navigationArrows: "verticalcentered", //nexttobullets, verticalcentered, none
                    navigationStyle: "round", //round,square,navbar

                    touchenabled: "off", // Enable Swipe Function : on/off
                    onHoverStop: "off", // Stop Banner Timet at Hover on Slide on/off

                    navOffsetHorizontal: 0,
                    navOffsetVertical: -30,
                    stopAtSlide: 0, // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
                    stopAfterLoops: -1, // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

                    shadow: 0, //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                    fullWidth: "on",
                    forceFullWidth: "on"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
                });
                

        $('.banner.interior').revolution(
                {
                    delay: 5000,
                    startwidth: 970,
                    startheight: 260,
                    navigationType: "none", //bullet, thumb, none, both		(No Thumbs In FullWidth Version !)
                    navigationArrows: "nexttobullets", //nexttobullets, verticalcentered, none
                    navigationStyle: "round", //round,square,navbar

                    touchenabled: "off", // Enable Swipe Function : on/off
                    onHoverStop: "off", // Stop Banner Timet at Hover on Slide on/off

                    navOffsetHorizontal: 0,
                    navOffsetVertical: -30,
                    stopAtSlide: 0, // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
                    stopAfterLoops: -1, // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

                    shadow: 0, //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                    fullWidth: "off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
                });

        /*CALENDARI*/
        <?php
        $mes = $_GET['mes'] ? $_GET['mes'] : date('n');
        $any = $_GET['year'] ? $_GET['year'] : date('Y');
        ?>
        if ($('#calendar').length) {
            $('#calendar').load('calendar.php', {'mes': <?php echo $mes; ?>,'any': <?php echo $any; ?>}, function() {
                $('a.prova').qtip({
                    position: {
                        corner: {
                            target: 'topMiddle',
                            tooltip: 'bottomMiddle'
                        },
                        adjust: {
                            y:1
                        }
                    },

                    style: {
                        name: 'dark',
                        background: '#303030',
                        padding: '10px 10px',
                        width: 210

                    }
                });
            });
        }

        $("a.calendar_button").live("click", function(evento){
            $('#calendar').load($(this).attr('rev'), function() {
                // Code here runs once the content has loaded
                // Put all your event handlers etc. here.
                $('a.prova').qtip({
                    position: {
                        corner: {
                            target: 'topMiddle',
                            tooltip: 'bottomMiddle'
                        },
                        adjust: {
                            y:1
                        }
                    },

                    style: {
                        name: 'dark',
                        background: '#303030',
                        padding: '10px 10px',
                        width: 210

                    }
                });
            });
            evento.preventDefault();
        });

        $ ('.tooltip_txt').each (function (){
            $(this).qtip ({
                position: {
                    corner: {
                        target: 'topMiddle',
                        tooltip: 'bottomMiddle'
                    },
                    adjust: {
                        y:1
                    }
                },

                style: {
                    name: 'dark',
                    background: '#303030',
                    padding: '10px 10px',
                    width: 210
                }
            });
        });


        $('#language').change(function() {
            location.href = $('#language option:selected').attr('title');
        });

        $('.send_form').click(function() {
            $('#' + $(this).parents("form").attr("id")).submit();
        });


        $('.check_form').click(function(evento) {
            if (check_form()) {
                $('#' + $(this).parents("form").attr("id")).submit();
            } else {
                evento.preventDefault();
            }
        });

        $('.delete_form').click(function(evento) {
            delete_form();
            evento.preventDefault();
        });


        //  TO TOP
        $(window).scroll(function() {
            if ($(this).scrollTop() != 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });

        $('#toTop').click(function() {
            $('body,html').animate({scrollTop: 0}, 800);
        });


        /*transicions al passar pagines*/
        /*cal posar la classe fade per fer la transicio*/
        //$('a').not('.foto').not('.no-fade').not('.tabs a').click(function(event) {
        $('.fade').click(function() {
            //event.preventDefault();
            //newLocation = this.href;
            //$('body').fadeOut('slow',newpage);
            $('body').fadeOut('slow');
        });

        /*afegim codi efectes hover imatges*/
        //$('a img').after('<span class="zoom_icon"></span>')

        /*function newpage() {
         window.location = newLocation;
         }*/
    
        $(window).bind("load", function() {
            var footer = $("#footer");
            var pos = footer.position();
            var height = $(window).height();
            height = height - pos.top;
            height = height - footer.height(); //el -48 es el padding top+ padding botom del footer
            if (height > 0) {
                footer.css({
                    'margin-top': height + 'px'
                });
            }
        });

        jQuery(function($) {
            $('a[data-target="flare"]').peFlareLightbox();
        });

        $(document).foundation(
        {
            orbit:
            {
                bullets: false,
                timer_speed: 3000,
                timer:true,
                animation: 'fade',
                navigation_arrows: true,
                pause_on_hover: false,
                resume_on_mouseout: true,
                slide_number: false
                //next_class: 'orbit-next-sub', // Class name given to the next button
                //prev_class: 'orbit-prev-sub' // Class name given to the previous button
            }
        });

        $.stellar({ 
            horizontalScrolling: false ,
            verticalOffset: 150
        });
        
    });



    /**
     * Enviar formulari
     */

    function send_form(name) {
        document.getElementById(name).submit();
    }

</script>