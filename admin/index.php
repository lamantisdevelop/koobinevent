<?php
/*
Web modular v.1.0
La Mantis - www.mantis.cat
*/

//REDIRECT SITE EN MANTENIMENT
if(!isset($_COOKIE['manteniment_mantis_2013&2654656654_nou'])){
//header('location:http://www.mantis.cat');
}

/*
Forcem que no guardi en caché - Problema Explorer
*/
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT"); // Date in the past

if (!(getenv('HTTPS') == 'on'))
{
        header('location:https://www.koobinevent.com/admin/');
        die;
}



/*
Tasques que es repeteixen a cada recàrrega de l'index.php - INICI
*/
require('includes/application_top.php');
 
/*
Comprovem login usuari
*/
if (!tep_session_is_registered('customer_id')) {
  tep_redirect(tep_href_link('login.php'));
}

/*
Verifiquem valors mòdul, sinó els defnits per defecte
*/
if (isset($_GET['modul']) && !empty($_GET['modul'])) {
  $modul = $_GET ['modul'];
} else {
  $modul = MODUL_DEFECTE;
  $_GET ['modul'] = MODUL_DEFECTE;
}

/* Per valors submenu*/
$aux_header = explode('_',$_GET['modul']);

// Si hi ha més d'un valor per mòdul
// if (empty($conf[$modul])) $modul = MODUL_DEFECTE;
//if (empty($conf[$modul]['layout'])) $conf[$modul]['layout'] = LAYOUT_DEFECTE ;

// Definim rutes als components del mòdul
$path_modul  =  DIR_WS_MODULS . $modul;
//echo $path_modul;exit;

$path_layout = ($_GET['iframe']=='si') ? DIR_WS_LAYOUTS . 'layout_iframe.php' : DIR_WS_LAYOUTS . LAYOUT_DEFECTE;

/*
Carreguem arxius corresponents al módul
*/
if (file_exists($path_layout)) {
  include($path_layout);
} else {
  if (!is_dir($path_modul)) {
    die('Error al cargar el módulo <b>' . $modulo . '</b>. No existe el modulo <b>' . $path_modul . '</b>.');
  }
}  

/*
Tasques que es repeteixen a cada recàrrega de l'index.php - FI
*/
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
