﻿/**
 * Enviar formulari
 */

function send_form(name) {
    document.getElementById(name).submit();
}

/**
 * Eliminar espais inici/final d'una cadena
 */

function trim(string) {
    string = strip_tags(string ,''); // Eliminem etiquetes html
    return string.replace(/\t/g, '').replace(/\r/g, '').replace(/\n/g, ''); // Eliminem salts de linea
    return string.replace(/^[\s]+|[\s]+$/g, ''); // Eliminem espais a principi i final
}

/**
 * Eliminar etiquetes 
 */
 
function strip_tags(str, allowed_tags) {
    var key = '', tag = '', allowed = false;
    var matches = allowed_array = [];
 
    var replacer = function(search, replace, str) {
        return str.split(search).join(replace);
    };
    // Build allowes tags associative array
    if (allowed_tags) {
        allowed_array = allowed_tags.match(/([a-zA-Z]+)/gi);
    }
    str += '';
    matches = str.match(/(<\/?[^>]+>)/gi); // Match tags
 
    for (key in matches) {
        if (isNaN(key)) { // IE7 Hack
            continue;
        }
        html = matches[key].toString();// Save HTML tag
        allowed = false;// Is tag not in allowed list? Remove from str!
 
        for (k in allowed_array) {
            allowed_tag = allowed_array[k];
            i = -1;
 
            if (i != 0) {
                i = html.toLowerCase().indexOf('<'+allowed_tag+'>');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('<'+allowed_tag+' ');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('</'+allowed_tag)   ;
            }
 
            if (i == 0) {
                allowed = true;
                break;
            }
        }
        if (!allowed) {
            str = replacer(html, "", str); // Custom replace. No regexing
        }
    }
    return str;
}

/**
 * AJAX Basics v1.0 - Crear connexió i carregar fragment en un element
 */
 
var xmlhttp = getXmlHttpObject();

function getXmlHttpObject(){
    var xmlhttp;

    /*@cc_on
    @if (@_jscript_version >= 5)
    try{
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e){
    try{
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    catch (e){
    xmlhttp = false;
    }
    }
    @else
    xmlhttp = false;
    @end @*/

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined'){
        try{
            xmlhttp = new XMLHttpRequest();
        }
        catch (e){
            xmlhttp = false;
        }
    }
    return xmlhttp;
}

function loadFragmentInToElement(fragment_url, element_id) { 
    var element = document.getElementById(element_id); 
    xmlhttp.open("GET", fragment_url); 
    xmlhttp.onreadystatechange = function() { 
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { 
            element.innerHTML = xmlhttp.responseText; 
        } 
    } 
    xmlhttp.send(null); 
}

//Iframes
function resizeHeight(id_frame){
    var iframe = document.getElementById(id_frame);
    var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
    iframe.style.height = innerDoc.getElementById('pagina').scrollHeight +'px';
    
}
            
function carregarEnIframe(url,id){
    $('#'+id).attr("src",url);
    setTimeout("$('#loading_galeria').hide();",1000);
    setTimeout("$('#loading_galeria_premsa').hide();",1000);
}

function refreshLlistatGaleries(gID,IDllistat){
    $.ajax({
        url: '/admin/ajax.php?function=refreshLlistatGaleries&gID='+gID+'&idLlistat='+IDllistat,
        success: function(data) {
            //reescribim el select
            if(IDllistat!='0'){
                $('#continent_select_galeries_'+IDllistat).html(data);
                //tornar a posar el javascript onchange
                $('#fotos_groups_id_'+IDllistat).change(function() {
                    carregarEnIframe('?modul=fotos_fotos&group_id='+this.value+'&iframe=si&idLlistat='+IDllistat);
                });
            }else{
                $('#continent_select_galeries').html(data);
                //tornar a posar el javascript onchange
                $('#fotos_groups_id').change(function() {
                    carregarEnIframe('?modul=fotos_fotos&group_id='+this.value+'&iframe=si&idLlistat=0');
                });
            }
        }
    });
}


//buscador admin a   
function filtrarFilesTaula(){
    var tarjetas = $(".llista tbody tr");
    var texto    = $("#tfBuscar").val();
    texto        = texto.toLowerCase();
    tarjetas.show();
    for(var i=0; i< tarjetas.size(); i++){
        var contenido = tarjetas.eq(i).text();
        contenido     = contenido.toLowerCase();
        var index     = contenido.indexOf(texto);
        if(index == -1){
            tarjetas.eq(i).hide();
        }		
    }
}

