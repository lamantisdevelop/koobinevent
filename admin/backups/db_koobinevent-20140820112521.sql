# Database Backup For Koobin: Sports & Cultural Events Management.
# Copyright (c) 2014 La Mantis Disseny i pàgines web a Girona
#
# Database: koobinevent
# Database Server: 127.0.0.1
#
# Backup Date: 20/08/2014 11:25:21

drop table if exists activitats;
create table activitats (
  id int(11) not null auto_increment,
  data_inici date not null ,
  data_final date ,
  hora varchar(128) not null ,
  lloc varchar(128) not null ,
  active int(2) default '0' not null ,
  entered datetime default '0000-00-00 00:00:00' not null ,
  modified datetime default '0000-00-00 00:00:00' not null ,
  portada int(1) not null ,
  preu varchar(100) ,
  fotos_groups_id varchar(11) ,
  PRIMARY KEY (id)
);

insert into activitats (id, data_inici, data_final, hora, lloc, active, entered, modified, portada, preu, fotos_groups_id) values ('298', '2014-06-01', '2014-06-30', '15:00', 'Girona', '1', '2014-04-07 08:39:49', '2014-05-27 11:11:57', '0', '', '54');
insert into activitats (id, data_inici, data_final, hora, lloc, active, entered, modified, portada, preu, fotos_groups_id) values ('299', '2014-04-07', '2014-04-12', '16:00', 'Girona', '1', '2014-04-07 13:46:57', '2014-05-15 11:20:29', '0', '', '55');
drop table if exists activitats_description;
create table activitats_description (
  activitats_id int(8) default '0' not null ,
  language_id int(2) default '0' not null ,
  nom varchar(255) not null ,
  descripcio text not null ,
  viewed int(11) not null ,
  link varchar(255) ,
  PRIMARY KEY (activitats_id, language_id)
);

insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('298', '1', 'Activitat 1 Nom', '<p>
	Activitat 1 DESC</p>', '0', '');
insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('298', '2', 'Activitat ES 1', '<p>
	Activitat ES 1</p>', '0', '');
insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('298', '3', 'Activitat EN 1', '<p>
	Activitat EN 1</p>', '0', '');
insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('298', '4', 'Activitat FR 1', '<p>
	Activitat FR 1</p>', '0', '');
insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('299', '1', 'Activitat 2', '<p>
	Activitat 2</p>', '0', '');
insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('299', '2', 'Activitat ES 2', '<p>
	Activitat ES 2</p>', '0', '');
insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('299', '3', 'Activitat EN 2', '<p>
	Activitat EN 2</p>', '0', '');
insert into activitats_description (activitats_id, language_id, nom, descripcio, viewed, link) values ('299', '4', 'Activitat FR 2', '<p>
	Activitat FR 2</p>', '0', '');
drop table if exists admin_users;
create table admin_users (
  id int(11) not null auto_increment,
  name varchar(128) not null ,
  mail varchar(128) not null ,
  password varchar(128) not null ,
  id_rol int(5) not null ,
  entered datetime not null ,
  modified datetime not null ,
  logons int(11) not null ,
  last_logon datetime not null ,
  PRIMARY KEY (id)
);

insert into admin_users (id, name, mail, password, id_rol, entered, modified, logons, last_logon) values ('2', 'La Mantis - Disseny i Internet Admin', 'koobinevent4@mantis.cat', '32b8e6d698f9eca7d98c0f882399d4c7:b6', '1', '2009-07-28 17:42:30', '2014-07-09 18:25:36', '700', '2014-08-20 09:24:59');
insert into admin_users (id, name, mail, password, id_rol, entered, modified, logons, last_logon) values ('9', 'Koobin Editor', 'koobin3@event.com', '1f3291e389da26b85d42c0d774461bd9:4f', '2', '2013-05-08 13:04:13', '2014-07-09 18:25:07', '85', '2014-08-20 06:19:44');
drop table if exists admin_users_rols;
create table admin_users_rols (
  id_rol int(5) not null ,
  nom varchar(100) not null ,
  PRIMARY KEY (id_rol)
);

insert into admin_users_rols (id_rol, nom) values ('0', 'NO ASSIGNAT');
insert into admin_users_rols (id_rol, nom) values ('1', 'Administrador');
insert into admin_users_rols (id_rol, nom) values ('2', 'Editor');
drop table if exists admin_users_rols_permisos;
create table admin_users_rols_permisos (
  id_rol int(5) not null ,
  nom_modul varchar(100) not null ,
  id_tipus_permis int(5) not null ,
  flag int(1) not null ,
  UNIQUE id_rol (id_rol, nom_modul, id_tipus_permis)
);

insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'agenda_agenda', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'agenda_agenda', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'agenda_agenda', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'banners_banners', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'banners_banners', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'banners_banners', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'cataleg_productes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'cataleg_productes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'cataleg_productes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'comandes_comandes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'comandes_comandes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'comandes_comandes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'comandes_edit', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'comandes_edit', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'comandes_edit', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_countries', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_countries', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_countries', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_currencies', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_currencies', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_currencies', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_disponibility', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_disponibility', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_disponibility', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_groups', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_groups', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_groups', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_languages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_languages', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_languages', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_status', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_status', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_status', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_taxes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_taxes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_taxes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_values', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_values', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_values', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_zones', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_zones', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'config_zones', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'distribuidors_distribuidors', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'distribuidors_distribuidors', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'distribuidors_distribuidors', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'estadistiques', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'estadistiques', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'estadistiques', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'fotos_fotos', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'fotos_fotos', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'fotos_fotos', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'ftpfiles_ftpfiles', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'ftpfiles_ftpfiles', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'ftpfiles_ftpfiles', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'img', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'img', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'img', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_clients', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_clients', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_clients', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_productes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_productes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_productes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_search', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_search', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_search', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_vendes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_vendes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'informes_vendes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'llistats_llistats', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'llistats_llistats', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'llistats_llistats', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'menus_menus', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'menus_menus', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'menus_menus', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_mailings', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_mailings', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_mailings', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_messages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_messages', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_messages', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_message_view', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_message_view', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_message_view', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_pare_menu_lateral', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_pare_menu_lateral', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_rebounds', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_rebounds', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_rebounds', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_sendings', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_sendings', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_sendings', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_templates', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_templates', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_templates', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_attributes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_attributes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_attributes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_attributes_values', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_attributes_values', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_attributes_values', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_concil', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_concil', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_concil', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_export', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_export', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_export', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_import', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_import', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_import', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_lists', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_lists', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_lists', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_mail', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_mail', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_mail', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_pare_menu_lateral', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_pare_menu_lateral', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_users', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_users', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'newsletter_users_users', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'noticies_noticies', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'noticies_noticies', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'noticies_noticies', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'pages_pages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'pages_pages', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'pages_pages', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'projectes_projectes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'projectes_projectes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'projectes_projectes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'server_info', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'server_info', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'server_info', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tarifes_tarifes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tarifes_tarifes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tarifes_tarifes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tarifes_zones', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tarifes_zones', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tarifes_zones', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_alt_search', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_alt_search', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_alt_search', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_backup_db', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_backup_db', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_backup_db', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_online', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_online', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_online', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_tpv', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_tpv', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_tpv', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_web_texts', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_web_texts', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'tools_web_texts', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_edit', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_edit', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_edit', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_rols', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_rols', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_rols', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_tipus_permisos', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_tipus_permisos', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_tipus_permisos', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_users', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_users', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'users_users', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'usuaris_contacte', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'usuaris_contacte', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'usuaris_contacte', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'usuaris_usuaris', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'usuaris_usuaris', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('0', 'usuaris_usuaris', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '.', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '.', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '.', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '..', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '..', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '..', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '.svn', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '.svn', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', '.svn', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'agenda_agenda', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'agenda_agenda', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'agenda_agenda', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'banners_banners', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'banners_banners', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'banners_banners', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_estoc', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_estoc', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_estoc', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_fabricants', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_fabricants', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_fabricants', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_inici', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_inici', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_inici', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_ofertes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_ofertes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_ofertes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_opcions', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_opcions', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_opcions', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_opcions_values', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_opcions_values', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_opcions_values', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_preus', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_preus', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_preus', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_productes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_productes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'cataleg_productes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'comandes_comandes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'comandes_comandes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'comandes_comandes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'comandes_edit', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'comandes_edit', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'comandes_edit', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_countries', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_countries', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_countries', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_currencies', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_currencies', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_currencies', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_disponibility', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_disponibility', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_disponibility', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_edicions', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_edicions', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_edicions', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_groups', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_groups', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_groups', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_languages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_languages', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_languages', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_pare_menu_lateral', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_pare_menu_lateral', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_status', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_status', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_status', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_taxes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_taxes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_taxes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_values', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_values', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_values', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_zones', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_zones', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'config_zones', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'distribuidors_distribuidors', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'distribuidors_distribuidors', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'distribuidors_distribuidors', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'estadistiques', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'estadistiques', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'estadistiques', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'fotos_fotos', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'fotos_fotos', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'fotos_fotos', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'fotos_ratios', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'fotos_ratios', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'fotos_ratios', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'ftpfiles_ftpfiles', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'ftpfiles_ftpfiles', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'ftpfiles_ftpfiles', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'ftp_files', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'ftp_files', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'ftp_files', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'img', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'img', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'img', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_clients', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_clients', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_clients', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_estoc', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_estoc', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_estoc', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_productes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_productes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_productes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_search', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_search', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_search', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_vendes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_vendes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'informes_vendes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'llistats_llistats', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'llistats_llistats', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'llistats_llistats', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'menus_menus', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'menus_menus', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'menus_menus', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_mailings', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_mailings', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_mailings', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_messages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_messages', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_messages', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_message_view', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_message_view', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_message_view', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_pare', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_pare', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_pare', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_rebounds', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_rebounds', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_rebounds', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_sendings', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_sendings', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_sendings', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_templates', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_templates', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_templates', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_attributes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_attributes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_attributes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_attributes_values', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_attributes_values', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_attributes_values', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_concil', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_concil', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_concil', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_export', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_export', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_export', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_import', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_import', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_import', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_lists', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_lists', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_lists', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_mail', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_mail', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_mail', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_users', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_users', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'newsletter_users_users', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'noticies_noticies', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'noticies_noticies', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'noticies_noticies', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'pages_pages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'pages_pages', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'pages_pages', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'projectes_projectes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'projectes_projectes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'projectes_projectes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'prop-base', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'prop-base', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'prop-base', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'props', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'props', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'props', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'server_info', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'server_info', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'server_info', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tarifes_tarifes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tarifes_tarifes', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tarifes_tarifes', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tarifes_zones', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tarifes_zones', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tarifes_zones', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'text-base', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'text-base', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'text-base', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tmp', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tmp', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tmp', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_alt_search', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_alt_search', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_alt_search', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_backup_db', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_backup_db', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_backup_db', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_online', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_online', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_online', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_tpv', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_tpv', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_tpv', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_web_texts', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_web_texts', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'tools_web_texts', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_edit', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_edit', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_edit', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_pare_menu_lateral', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_rols', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_rols', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_rols', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_tipus_permisos', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_tipus_permisos', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_tipus_permisos', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_users', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_users', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'users_users', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuarios_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuarios_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuarios_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuaris_contacte', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuaris_contacte', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuaris_contacte', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuaris_usuaris', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuaris_usuaris', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('1', 'usuaris_usuaris', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', '.', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', '.', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', '.', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', '..', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', '..', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', '..', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'agenda_agenda', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'agenda_agenda', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'agenda_agenda', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'banners_banners', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'banners_banners', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'banners_banners', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_estoc', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_estoc', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_estoc', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_fabricants', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_fabricants', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_fabricants', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_inici', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_inici', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_inici', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_ofertes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_ofertes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_ofertes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_opcions', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_opcions', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_opcions', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_opcions_values', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_opcions_values', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_opcions_values', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_preus', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_preus', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_preus', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_productes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_productes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'cataleg_productes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_disponibility', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_disponibility', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_disponibility', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_edicions', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_edicions', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_edicions', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_groups', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_groups', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_groups', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_languages', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_languages', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_languages', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_status', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_status', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_status', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_values', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_values', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_values', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_zones', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_zones', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'config_zones', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'distribuidors_distribuidors', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'distribuidors_distribuidors', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'distribuidors_distribuidors', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'estadistiques', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'estadistiques', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'estadistiques', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'fotos_fotos', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'fotos_fotos', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'fotos_fotos', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'fotos_ratios', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'fotos_ratios', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'fotos_ratios', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_productes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_productes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_productes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_search', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_search', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'informes_search', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'llistats_llistats', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'llistats_llistats', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'llistats_llistats', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'menus_menus', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'menus_menus', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'menus_menus', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_mailings', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_mailings', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_mailings', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_messages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_messages', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_messages', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_message_view', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_message_view', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_message_view', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_rebounds', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_rebounds', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_rebounds', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_sendings', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_sendings', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_sendings', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_templates', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_templates', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_templates', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_attributes', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_attributes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_attributes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_attributes_values', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_attributes_values', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_attributes_values', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_concil', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_concil', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_concil', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_export', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_export', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_export', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_import', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_import', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_import', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_lists', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_lists', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_lists', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_mail', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_mail', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_mail', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_users', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_users', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'newsletter_users_users', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'noticies_noticies', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'noticies_noticies', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'noticies_noticies', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'pages_pages', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'pages_pages', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'pages_pages', '3', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'projectes_projectes', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'projectes_projectes', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'projectes_projectes', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'server_info', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'server_info', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'server_info', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_alt_search', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_alt_search', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_alt_search', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_backup_db', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_backup_db', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_backup_db', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_online', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_online', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_online', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_web_texts', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_web_texts', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'tools_web_texts', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_edit', '1', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_edit', '2', '1');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_edit', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_rols', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_rols', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_rols', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_tipus_permisos', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_tipus_permisos', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_tipus_permisos', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_users', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_users', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'users_users', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuarios_pare_menu_lateral', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuarios_pare_menu_lateral', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuarios_pare_menu_lateral', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuaris_contacte', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuaris_contacte', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuaris_contacte', '3', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuaris_usuaris', '1', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuaris_usuaris', '2', '0');
insert into admin_users_rols_permisos (id_rol, nom_modul, id_tipus_permis, flag) values ('2', 'usuaris_usuaris', '3', '0');
drop table if exists admin_users_tipus_permisos;
create table admin_users_tipus_permisos (
  id_tipus_permis int(5) not null auto_increment,
  nom_permis varchar(100) not null ,
  PRIMARY KEY (id_tipus_permis)
);

insert into admin_users_tipus_permisos (id_tipus_permis, nom_permis) values ('1', 'Lectura');
insert into admin_users_tipus_permisos (id_tipus_permis, nom_permis) values ('2', 'Edició');
insert into admin_users_tipus_permisos (id_tipus_permis, nom_permis) values ('3', 'Eliminar');
drop table if exists banners;
create table banners (
  id int(11) not null auto_increment,
  group_id int(11) not null ,
  name varchar(256) not null ,
  active int(1) not null ,
  data_inici datetime not null ,
  data_final datetime not null ,
  listorder int(11) not null ,
  entered datetime not null ,
  modified datetime not null ,
  UNIQUE id (id)
);

insert into banners (id, group_id, name, active, data_inici, data_final, listorder, entered, modified) values ('35', '2', 'Software de gestión', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '2014-06-19 10:50:11', '2014-08-19 10:56:12');
insert into banners (id, group_id, name, active, data_inici, data_final, listorder, entered, modified) values ('36', '2', 'Gestió integral de ticketing', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2', '2014-06-19 10:51:03', '2014-08-19 10:56:52');
insert into banners (id, group_id, name, active, data_inici, data_final, listorder, entered, modified) values ('37', '4', '10 RAONS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '2014-06-20 10:58:58', '2014-07-01 11:33:26');
drop table if exists banners_description;
create table banners_description (
  banners_id int(11) not null ,
  language_id int(11) not null ,
  titol varchar(100) not null ,
  descripcio text not null ,
  image varchar(128) not null ,
  link varchar(128) not null ,
  target varchar(32) not null ,
  title varchar(255) not null ,
  class varchar(64) not null ,
  rel varchar(128) not null ,
  rev varchar(128) not null ,
  UNIQUE banners_id (banners_id, language_id)
);

insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('35', '1', 'Software de gestió i venda online d\'esdeveniments', '', 'koobin_soft1.png', '/ca/software.html', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('35', '2', 'Software de gestión y venta online de eventos', '', 'koobin_soft11.png', '/es/software.html', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('35', '3', 'Management software sales online event', '', 'koobin_soft12.png', '/en/software.html', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('36', '1', 'Gestió integral de ticketing', '', 'koobin_getio_ticketing.jpg', '/ca/gestio-tiketing.html', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('36', '2', 'Gestión integral de ticketing', '', 'koobin_getio_ticketing1.jpg', '/es/gestio-tiketing.html', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('36', '3', 'Integral managament service ticketing', '', 'koobin_getio_ticketing2.jpg', '/en/gestio-tiketing.html', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('37', '1', '10 Raons per escollir Koobin', '', 'koobin_10_ca.png', '/ca/per-que-koobin.html', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('37', '2', '', '', '', 'http://', '_self', '', '', '', '');
insert into banners_description (banners_id, language_id, titol, descripcio, image, link, target, title, class, rel, rev) values ('37', '3', '', '', '', 'http://', '_self', '', '', '', '');
drop table if exists banners_groups;
create table banners_groups (
  id int(11) not null auto_increment,
  name varchar(256) not null ,
  listorder int(11) default '0' not null ,
  entered datetime default '0000-00-00 00:00:00' not null ,
  modified datetime default '0000-00-00 00:00:00' not null ,
  PRIMARY KEY (id)
);

insert into banners_groups (id, name, listorder, entered, modified) values ('2', 'HOME', '1', '2013-05-13 18:17:52', '2013-11-26 14:52:24');
insert into banners_groups (id, name, listorder, entered, modified) values ('4', 'Peu de pagina', '2', '2013-06-10 21:34:32', '2013-12-03 16:53:26');
drop table if exists categories;
create table categories (
  id int(11) not null auto_increment,
  video varchar(128) not null ,
  image varchar(64) ,
  parent_id int(11) default '0' not null ,
  listorder int(11) ,
  active int(1) not null ,
  usa_active int(1) not null ,
  entered datetime ,
  modified datetime ,
  fotos_groups_id int(4) not null ,
  fotos_groups_id_premsa int(4) not null ,
  PRIMARY KEY (id),
  KEY idx_categories_parent_id (parent_id)
);

drop table if exists categories_description;
create table categories_description (
  categories_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  name varchar(64) not null ,
  subtitol varchar(250) not null ,
  description text not null ,
  desc_short text not null ,
  PRIMARY KEY (categories_id, language_id),
  KEY idx_categories_name (name)
);

drop table if exists config_groups;
create table config_groups (
  id int(11) not null auto_increment,
  name varchar(64) not null ,
  description varchar(255) not null ,
  listorder int(5) ,
  visible int(1) default '1' ,
  PRIMARY KEY (id)
);

insert into config_groups (id, name, description, listorder, visible) values ('1', 'General', 'Valors per defecte per al portal.', '1', '1');
insert into config_groups (id, name, description, listorder, visible) values ('2', 'SMTP / E-mail', 'Valors configuració enviament de correus electrònics.', '2', '1');
insert into config_groups (id, name, description, listorder, visible) values ('5', 'Pàgina INICI', 'Configuració de pàgina d\'inici', '3', '1');
insert into config_groups (id, name, description, listorder, visible) values ('6', 'Fotos / Galeries', 'Configuració del mòdul Galeries / Fotos', '4', '1');
insert into config_groups (id, name, description, listorder, visible) values ('7', 'Banners', 'Configuració del mòdul banners (SLIDES, banners laterals...)', '5', '1');
insert into config_groups (id, name, description, listorder, visible) values ('8', 'BARRES LATERALS', 'Configuració del blocs de les columnes laterals', '6', '1');
insert into config_groups (id, name, description, listorder, visible) values ('9', 'SOCIAL', 'Gestió de valors de xarxes socials', '7', '1');
insert into config_groups (id, name, description, listorder, visible) values ('10', 'GOOGLE', 'Valors d\'aplicacions i widgets de Google. (analytics, maps...)', '8', '1');
drop table if exists config_values;
create table config_values (
  item varchar(35) not null ,
  value longtext ,
  description text not null ,
  editable tinyint(4) default '1' ,
  group_id int(11) not null ,
  type varchar(25) ,
  PRIMARY KEY (item)
);

insert into config_values (item, value, description, editable, group_id, type) values ('AVIS_COOKIES_ACTIU', '1', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('BANNERS_SLIDES_ID', '1', 'ID del grup de banners dels SLIDES HOME', '1', '7', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('BANNERS_TO_PAGES', 'true', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('BREADCRUMB_SEP', '<span class=\"sep\">>></span> ', 'Separador enlla?os breadcrumb.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('DEFAULT_AGENDA_PAGE', '101', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('DEFAULT_CATALOG_PAGE', '28', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('DEFAULT_IMAGE', '0', 'Imatge per defecte a la galeria de fotos', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('DEFAULT_LANGUAGE', 'ca', 'Idioma per defecte.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('DEFAULT_NOTICIES_PAGE', '2', 'ID pàgina noticies per defecte', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('DEFAULT_PAGE', '1', 'P?gina per defecte.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('DEFAULT_PAGINA_CERCADOR', '98', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('EDICIO_ACTUAL', '1', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('EFECTE_TRANSITION_GALERIA_FITXA', 'fade', 'Efecte de transicio per a slide revolution als elements de les fitxes que tenen galeries
valors possibles:
boxslide
boxfade
slotzoom-horizontal
slotslide-horizontal
slotfade-horizontal
slotzoom-vertical
slotslide-vertical
slotfade-vertical
curtain-1
curtain-2
curtain-3
slideleft
slideright
slideup
slidedown
fade
random
slidehorizontal
slidevertical
papercut
flyin
turnoff
cube
3dcurtain-vertical
3dcurtain-horizontal', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FACEBOOK_APP_ID', 'xxxxxxxxxxxxxxxxxx', 'Facebook application ID', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FACEBOOK_PAGE', 'xxxxxxxxxxxxxxxxxx', 'Facebook ID pàgina', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FACEBOOK_SECRET', 'xxxxxxxxxxxxxxxxxx', 'Facebook secret ID', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_FILTRAR_PER_EDICIO', '0', 'SISTEMA DE FILTRE DE GALERIES EN ELS DESPLEGABLES DE L\'ADMINISTRADOR (per tal de reduir nombre de galeries a l\'hora de triar en noticies, espectacles, musics...)
1 - Només mostra les galeries creades amb dates dins l\'edició actual
0 - Mostra totes les galeries independentment de l\'edició', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_AGENDA', '53', '', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_CATEGORIES', '47', '', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_CLIENTS', '58', 'Fotos del llistat de clients', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_EQUIP', '19', '', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_NOTICIES', '2', '', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_PAGES', '45', 'ID del grup de galeries de PAGES. (s\'utilitza per a crear noves galeries des del menú pàgines, per tal de saber on s\'han de guardar. PAGES mostra llavors totes les galeries de tots els grups per a escollir)', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_PREMSA', '63', 'Galeria del llistat de premsa', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_PRODUCTES', '50', '', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_SLIDESHOW', '32', '', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_TOTES', '-1', 'Serveix per a mostrar totes les galeries independentment del grup', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_GROUP_TREBALL', '64', 'Galeria del llistat de treball', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('FOTOS_SLIDES_TEXTES_FORMAT', '<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"200\" data-start=\"500\" data-x=\"200\" data-y=\"100\">
	<span class=\"title_slide\">%title_slide</span>
</div>
<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"300\" data-start=\"700\" data-x=\"200\" data-y=\"140\">
	<span class=\"subtitle_slide\">%subtitle_slide</span>
</div>
<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"300\" data-start=\"800\" data-x=\"200\" data-y=\"170\">
	<span class=\"subtitle2_slide\">%subtitle2_slide</span>
</div>', 'En cas que l\'usuari EDITOR crei els textes d\'slides, es mostraran en aquest format', '1', '6', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('GOOGLE_ANALYTICS_CODE', 'UA-52694856-1', 'Codi Google Analytics', '1', '10', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('GOOGLE_ANALYTICS_HOST', 'www.koobinevent.com', '', '1', '10', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('GOOGLE_LAT', '41.97659296270089000000', 'Latitud - Google Maps', '1', '10', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('GOOGLE_LON', '2.82333183393348000000', 'Longitud - Google Maps', '1', '10', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('GOOGLE_ZOOM', '14', 'Zoom per defecte dels mapes - Google Maps', '1', '10', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HEADERS_TO_PAGES', 'true', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_LATERAL_DRET', '0', '', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_LATERAL_ESQUERRE', '0', '', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_MAX_ITEMS', '1000', 'Màxim nombre d\'espectacles mostrats a la home amb vista 2=destacats tipus llista (Torroella)', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_MAX_ITEMS_PER_CATEGORIA', '6', 'Màxim número d\'espectacles mostrat dins de cada categoria (en vista 3=llistat destacats per categories Tipus TA)', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_MAX_NEWS_DESTACADES', '4', 'Màxim número de notícies destacades a la home', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_TIPUS', '0', 'PERMET ESCOLLIR COM ES MOSTRARAN ELS PRODUCTES A LA HOME
1 = llistat categories amb link a aquestes (Excat)
2 = llistat espectacles destacats (Torroella)
3 = llistat espectacles destacats agrupats per categories (Temporada Alta)', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_TIPUS_DESTACATS_LLISTA', '0', 'Indica si es mostren els espectacles de la HOME en format LLISTA o GRID

0= GRID (caixes verticals)
1= LLISTA (caixes horitzontals)', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('HOME_TIPUS_PRIMER_ITEM_DESTACAT', '1', 'Serveix per a destacar el primer element en el llistat home tipus GRID DESTACATS (caixes verticals)
Si =1 mostra el primer element del llistat com a destacat més gran
Si =0 mostra tots els elements iguals', '1', '5', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('LATERAL_BLOC_NOTICIES', '0', 'Indica si es mostra o no el bloc de notícies destacades a la barra lateral dreta
0 = no es mostra
1= si es mostra', '1', '8', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAIL_FROM', 'jaume@mantis.cat', 'E-mail del que envia el correu.', '1', '2', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAIL_FROM_NAME', 'Koobin', 'Nom de qui envia el correu.', '1', '2', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAIL_SERVER', 'mail.mantis.cat', 'Servidor de correus electrònics utilitzat.', '1', '2', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAX_BOX_ITEMS', '5', 'Màxim items x caixa.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAX_BOX_TAGS', '15', 'M?xim etiquetes tags-cloud.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAX_DESTACATS', '4', 'Màxim destacats pagina inici', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAX_DISPLAY_NEWS', '3', 'Número màxim llistat notícies.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAX_DISPLAY_RESULTS', '25', 'N?mero m?xim de resultats per p?gina.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('MAX_DISPLAY_RSS', '20', 'Màxim entrades per RSS.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('PAGINA_AGENDA', '101', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('PAGINA_COOKIES', '92', '', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('PIWIK_AUTH_CODE', '0d404ede1d313b3f7052c034c7820399', 'Codi d\'autentificació de cada site creat a Piwik per tal de poder accedir a les estadístiques sense haver d\'estar loguejat al panell de Piwik.
El trobem a dinastats.com dins l\'apartat API', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('PIWIK_TRACK_CODE', '1633', 'Codi del gestor d\'analitiques PIWIK (cada site ha de tenir un codi unic generat des del servidor de dinahosting)', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SEARCH_DEFAULT_OPERATOR', 'and', 'Operador per defecte buscador.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SITE_LATERAL_DRET', '0', 'Indica que totes les pàgines excepte home porten barra lateral dreta', '1', '8', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SITE_LATERAL_ESQUERRE', '0', 'Indica que totes les pàgines excepte home porten barra lateral esquerra', '1', '8', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SMTP_PASSWORD', 'HqOMIVnMeotIWiAD', '', '1', '2', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SMTP_SERVER', 'smtp.googlemail.com', '', '1', '2', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SMTP_USERNAME', 'koobinevent@gmail.com', '', '1', '2', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SOCIAL_FACEBOOK', 'https://www.facebook.com/KoobinEvent', 'Link a Facebook', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SOCIAL_GOOGLE_PLUS', 'https://plus.google.com/111369365745682134300', 'Link a Google plus', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SOCIAL_INSTAGRAM', 'http://www.instagram.com', 'Link a Instagram', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SOCIAL_LINKEDIN', 'https://www.linkedin.com/company/koobinevent-sl', '', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SOCIAL_TWITTER', 'http://www.twitter.com', 'Link a Twitter', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('SOCIAL_YOUTUBE', 'http://www.youtube.com', 'Link a YouTube', '1', '9', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('WEB_NAME', 'Koobin: Sports & Cultural Events Management.', 'Nom lloc web.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('WEB_OWNER', 'La Mantis Disseny i pàgines web a Girona', 'Propietari lloc web.', '1', '1', NULL);
insert into config_values (item, value, description, editable, group_id, type) values ('WEB_STATS', '', 'Enllaç estadístiques lloc web', '1', '1', NULL);
drop table if exists counter;
create table counter (
  startdate datetime ,
  counter int(12) 
);

insert into counter (startdate, counter) values ('2013-12-16 00:00:00', '63986');
drop table if exists edicions;
create table edicions (
  id int(11) not null auto_increment,
  name varchar(64) not null ,
  active int(1) not null ,
  data_inici datetime not null ,
  data_final datetime not null ,
  data_inici_edicio datetime not null ,
  listorder int(11) not null ,
  entered datetime not null ,
  UNIQUE id (id)
);

drop table if exists fotos;
create table fotos (
  id int(11) not null auto_increment,
  group_id int(11) not null ,
  name varchar(64) not null ,
  active int(1) not null ,
  listorder int(11) not null ,
  entered datetime not null ,
  modified datetime not null ,
  image varchar(128) not null ,
  efecte varchar(50) not null ,
  UNIQUE id (id)
);

insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('167', '71', 'Koobin Events', '1', '1', '2014-06-19 11:52:34', '2014-06-25 17:29:39', 'koobin_news.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('168', '72', 'Auditori de Barcelona', '1', '1', '2014-06-19 12:28:17', '2014-06-19 12:28:17', 'auditori_bcn.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('169', '73', 'Palau de les Arts Reina Sofía', '1', '1', '2014-06-19 12:32:55', '2014-06-19 12:54:24', 'palau_arts_reina_sofia.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('170', '74', 'L’Atlàntida Centre d’Arts Escèniques d’Osona', '1', '1', '2014-06-19 12:37:10', '2014-06-19 12:53:16', 'atlantida_caeo.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('171', '75', 'Festival GREC 2013', '1', '1', '2014-06-19 12:51:18', '2014-06-19 12:51:18', 'festival_grec_2013.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('172', '76', 'FC Barcelona', '1', '1', '2014-06-19 13:00:14', '2014-06-19 13:00:14', 'fcb.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('173', '77', 'ACB (Asociación de Clubes de Baloncesto)', '1', '1', '2014-06-19 13:02:08', '2014-06-19 13:02:08', 'acb.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('174', '78', 'Clubes Liga Endesa', '1', '1', '2014-06-19 13:04:18', '2014-06-19 13:04:18', 'club_liga_endesa.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('175', '79', 'Euroleague', '1', '1', '2014-06-19 13:06:58', '2014-06-19 13:06:58', 'euroleague.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('177', '81', 'Gran Teatre del Liceu', '1', '1', '2014-06-25 19:30:15', '2014-06-25 19:30:15', 'teatre_liceu.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('178', '82', 'Teatre Victòria', '1', '1', '2014-06-25 19:30:47', '2014-06-25 19:30:47', 'teatre_victoria.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('179', '83', 'Festival Castell de Peralada', '1', '1', '2014-06-25 19:31:21', '2014-06-25 19:31:21', 'festival_castell_perelada.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('180', '84', 'Festival Temporada Alta', '1', '1', '2014-06-25 19:31:58', '2014-06-25 19:31:58', 'temporada_alta.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('181', '33', 'Koobin Event', '1', '2', '2014-06-26 19:06:16', '2014-08-19 10:32:02', 'koobin_slide_02.jpg', 'fade');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('182', '33', 'Koobin Event', '1', '1', '2014-06-27 12:13:05', '2014-08-19 10:29:20', 'paneltaronja11.jpg', 'fade');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('183', '85', 'StarLite Marbella', '1', '1', '2014-06-27 15:29:22', '2014-06-27 15:29:22', 'starlite_marbella_koobin.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('184', '86', 'Llac dels cignes', '1', '1', '2014-06-27 15:32:44', '2014-06-27 15:32:44', 'sabadell_cultura_koobin.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('185', '87', 'Processó de Verges', '1', '1', '2014-06-27 15:36:02', '2014-06-27 15:36:02', 'processo_verges_koobin.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('187', '89', 'Treball', '1', '1', '2014-06-27 15:58:11', '2014-06-30 10:26:04', 'koobin_treball.jpg', 'fade');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('189', '92', 'Gran Teatre del Liceu', '1', '1', '2014-06-30 19:08:44', '2014-06-30 19:08:44', 'koobin_liceu.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('190', '93', 'CANTADA D\'HAVANERES DE CALELLA DE PALAFRUGELL', '1', '1', '2014-06-30 19:12:11', '2014-06-30 19:12:11', 'koobin_calella.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('191', '94', 'Koobin Event', '1', '1', '2014-07-01 11:19:01', '2014-07-01 11:19:01', 'koobin_jornades.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('193', '96', 'Club Baloncesto Gran Canaria', '1', '1', '2014-07-01 12:29:46', '2014-07-01 12:29:46', 'gran_canaria_baloncesto1.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('194', '97', 'Joventut Badalona', '1', '1', '2014-07-01 12:31:03', '2014-07-01 12:31:03', 'joventut_badalona.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('195', '98', 'Museo Picasso Malaga', '1', '1', '2014-07-01 12:34:35', '2014-07-01 12:34:35', 'museo_picasso_malaga.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('196', '99', 'Naviliera Les Goges', '1', '1', '2014-07-01 12:40:01', '2014-07-01 12:40:01', 'naviliera_les_goges.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('197', '100', 'Lady Shelley', '1', '1', '2014-07-01 12:42:29', '2014-07-01 12:42:29', 'lady_shelley.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('198', '101', 'Festival Internacional de Cinema Fantàstic de Catalunya', '0', '1', '2014-07-01 12:45:08', '2014-07-01 12:45:08', 'festival_sitges.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('199', '102', '', '1', '1', '2014-08-11 10:41:45', '2014-08-11 10:41:45', 'image[16].jpeg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('201', '103', '', '1', '1', '2014-08-12 07:26:31', '2014-08-12 07:26:31', 'captura-de-pantalla-2014-08-12-a-les-9.21.30.png', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('202', '104', '', '1', '1', '2014-08-12 07:28:48', '2014-08-12 07:28:48', 'martin-carpena-cancha-llena.jpg', '');
insert into fotos (id, group_id, name, active, listorder, entered, modified, image, efecte) values ('203', '105', '', '1', '1', '2014-08-12 07:37:50', '2014-08-12 07:37:50', 'passbook.jpg', '');
drop table if exists fotos_description;
create table fotos_description (
  fotos_id int(11) not null ,
  language_id int(11) not null ,
  link varchar(128) not null ,
  target varchar(32) not null ,
  title varchar(255) not null ,
  autor varchar(255) not null ,
  description text not null ,
  class varchar(64) not null ,
  rel varchar(128) not null ,
  rev varchar(128) not null ,
  title_slide varchar(300) not null ,
  subtitle varchar(300) not null ,
  subtitle2 varchar(300) not null ,
  UNIQUE fotos_id (fotos_id, language_id)
);

insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('167', '1', 'http://', '_blank', 'Koobin Events', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('167', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('167', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('168', '1', 'http://', '_blank', 'Auditori de Barcelona', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('168', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('168', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('169', '1', 'http://', '_blank', 'Palau de les Arts Reina Sofía', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('169', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('169', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('170', '1', 'http://', '_blank', 'L’Atlàntida Centre d’Arts Escèniques d’Osona', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('170', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('170', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('171', '1', 'http://', '_blank', 'Festival GREC 2013', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('171', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('171', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('172', '1', 'http://', '_blank', 'FC Barcelona', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('172', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('172', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('173', '1', 'http://', '_blank', 'ACB (Asociación de Clubes de Baloncesto)', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('173', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('173', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('174', '1', 'http://', '_blank', 'Clubes Liga Endesa', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('174', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('174', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('175', '1', 'http://', '_blank', 'Euroleague', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('175', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('175', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('177', '1', 'http://', '_blank', 'Gran Teatre del Liceu', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('177', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('177', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('178', '1', 'http://', '_blank', 'Teatre Victòria', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('178', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('178', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('179', '1', 'http://', '_blank', 'Festival Castell de Peralada', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('179', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('179', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('180', '1', 'http://', '_blank', 'Festival Temporada Alta', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('180', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('180', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('181', '1', 'http://', '_blank', 'Koobin Event', '', '<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"200\" data-start=\"500\" data-x=\"0\" data-y=\"100\"><span class=\"title_slide\">La millor soluci&oacute;</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"200\" data-start=\"500\" data-x=\"0\" data-y=\"180\"><span class=\"title_slide\">per gestionar</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"300\" data-start=\"700\" data-x=\"0\" data-y=\"260\"><span class=\"subtitle_slide\">tot tipus d&rsquo;esdeveniments</span></div>', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('181', '2', 'http://', '_blank', 'Koobin Event', '', '<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"200\" data-start=\"500\" data-x=\"0\" data-y=\"100\"><span class=\"title_slide\">La mejor soluci&oacute;n</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"200\" data-start=\"500\" data-x=\"0\" data-y=\"180\"><span class=\"title_slide\">para gestionar</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"300\" data-start=\"700\" data-x=\"0\" data-y=\"260\"><span class=\"subtitle_slide\">cualquier tipo de eventos</span></div>', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('181', '3', 'http://', '_blank', 'Koobin Event', '', '<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"200\" data-start=\"500\" data-x=\"0\" data-y=\"100\"><span class=\"title_slide\">The best solution</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"200\" data-start=\"500\" data-x=\"0\" data-y=\"180\"><span class=\"title_slide\">to manage</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"300\" data-start=\"700\" data-x=\"0\" data-y=\"260\"><span class=\"subtitle_slide\">any type of events</span></div>', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('182', '1', 'http://', '_blank', 'Koobin Event', '', '<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"500\" data-x=\"0\" data-y=\"100\"><span class=\"title_slide\">Software,</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"500\" data-x=\"0\" data-y=\"160\"><span class=\"title_slide\">suport i serveis</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"220\"><span class=\"subtitle_slide\">per a grans</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"280\"><span class=\"subtitle_slide\">esdeveniments</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"360\"><span class=\"subtitle2_slide\">Serveis de software &ndash; Processos de seating</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"390\"><span class=\"subtitle2_slide\">Serveis d&rsquo;impressi&oacute; &ndash; Control d&rsquo;accessos</span></div>

<div class=\"caption lfr\" data-easing=\"easeOutExpo\" data-speed=\"500\" data-start=\"900\" data-x=\"850\" data-y=\"70\"><img alt=\"Un equip innovador\" class=\"imgSlider1\" src=\"/admin/js/kcfinder/upload/images/iphone_koobin.png\" /></div>', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('182', '2', 'http://', '_blank', 'Koobin Event', '', '<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"500\" data-x=\"0\" data-y=\"100\"><span class=\"title_slide\">Software,</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"500\" data-x=\"0\" data-y=\"160\"><span class=\"title_slide\">soporte y servicios </span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"220\"><span class=\"subtitle_slide\">para grandes </span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"280\"><span class=\"subtitle_slide\">eventos</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"360\"><span class=\"subtitle2_slide\">Servicios de software - Procesos de seating</span></div>

<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"390\"><span class=\"subtitle2_slide\">Servicios de impresi&oacute;n - Control de accesos</span></div>

<div class=\"caption lfr\" data-easing=\"easeOutExpo\" data-speed=\"500\" data-start=\"900\" data-x=\"850\" data-y=\"70\"><img alt=\"Un equip innovador\" class=\"imgSlider1\" src=\"/admin/js/kcfinder/upload/images/iphone_koobin.png\" /></div>', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('182', '3', 'http://', '_blank', 'Koobin Event', '', '<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"500\" data-x=\"0\" data-y=\"100\">
  <span class=\"title_slide\">Software,</span>
</div>
<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"500\" data-x=\"0\" data-y=\"160\">
  <span class=\"title_slide\">Support and Services </span>
</div>
<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"220\">
  <span class=\"subtitle_slide\">for large</span>
</div>
<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"280\">
  <span class=\"subtitle_slide\">events</span>
</div>
<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"360\">
  <span class=\"subtitle2_slide\">Software Services - Processes seating </span>
</div>
<div class=\"caption lfl\" data-easing=\"easeOutExpo\" data-speed=\"700\" data-start=\"700\" data-x=\"0\" data-y=\"390\">
  <span class=\"subtitle2_slide\">Print Services - Access Control</span>
</div>
<div class=\"caption lfr\" data-easing=\"easeOutExpo\" data-speed=\"500\" data-start=\"900\" data-x=\"850\" data-y=\"70\">
  <img alt=\"Un equip innovador\" class=\"imgSlider1\" src=\"/admin/js/kcfinder/upload/images/iphone_koobin.png\" />
</div>', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('183', '1', 'http://', '_blank', 'StarLite Marbella', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('184', '1', 'http://', '_blank', 'Llac dels cignes', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('185', '1', 'http://', '_blank', 'Processó de Verges', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('187', '1', 'http://', '_blank', 'Treball', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('189', '1', 'http://', '_blank', 'Gran Teatre del Liceu', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('190', '1', 'http://', '_blank', 'CANTADA D\'HAVANERES DE CALELLA DE PALAFRUGELL', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('191', '1', 'http://', '_blank', 'Koobin Event', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('193', '1', 'http://', '_blank', 'Club Baloncesto Gran Canaria', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('194', '1', 'http://', '_blank', 'Joventut Badalona', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('195', '1', 'http://', '_blank', 'Museo Picasso Malaga', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('196', '1', 'http://', '_blank', 'Naviliera Les Goges', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('197', '1', 'http://', '_blank', 'Lady Shelley', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('198', '1', 'http://', '_blank', 'Festival Internacional de Cinema Fantàstic de Catalunya', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('199', '1', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('199', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('199', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('201', '1', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('201', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('201', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('202', '1', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('202', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('202', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('203', '1', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('203', '2', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
insert into fotos_description (fotos_id, language_id, link, target, title, autor, description, class, rel, rev, title_slide, subtitle, subtitle2) values ('203', '3', 'http://', '_blank', '', '', '', '', '', '', '', '', '');
drop table if exists fotos_groups;
create table fotos_groups (
  id int(11) not null auto_increment,
  parent_id int(11) not null ,
  name varchar(128) not null ,
  entered datetime not null ,
  modified datetime not null ,
  listorder int(11) not null ,
  active int(1) not null ,
  foto_portada int(11) not null ,
  id_ratios int(10) not null ,
  UNIQUE id (id)
);

insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('2', '0', 'NOTÍCIES', '2013-06-03 17:32:17', '2014-06-25 17:28:21', '1', '1', '0', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('32', '0', 'SLIDESHOWS', '2013-07-11 15:59:47', '2014-06-30 09:20:40', '1', '1', '0', '4');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('33', '32', 'Slideshow HOME', '2013-07-11 15:59:58', '2014-06-20 13:19:23', '1', '1', '182', '3');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('58', '0', 'CLIENTS', '2014-06-18 14:47:35', '2014-06-19 12:17:25', '27', '1', '0', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('71', '2', 'Koobin_BASE', '2014-06-19 11:51:52', '2014-06-19 11:51:52', '27', '1', '167', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('72', '58', 'Auditori Barcelona', '2014-06-19 12:28:03', '2014-06-19 12:28:03', '27', '1', '168', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('73', '58', 'Palau de les Arts Reina Sofía', '2014-06-19 12:32:47', '2014-06-19 12:32:47', '27', '1', '169', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('74', '58', 'L’Atlàntida Centre d’Arts Escèniques d’Osona', '2014-06-19 12:37:03', '2014-06-19 12:37:03', '27', '1', '170', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('75', '58', 'Festival GREC 2013', '2014-06-19 12:51:11', '2014-06-19 12:51:11', '27', '1', '171', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('76', '58', 'FC Barcelona', '2014-06-19 13:00:07', '2014-06-19 13:00:07', '27', '1', '172', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('77', '58', 'ACB (Asociación de Clubes de Baloncesto)', '2014-06-19 13:02:03', '2014-06-19 13:02:03', '27', '1', '173', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('78', '58', 'Clubes Liga Endesa', '2014-06-19 13:04:11', '2014-06-19 13:04:11', '27', '1', '174', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('79', '58', 'Euroleague', '2014-06-19 13:06:52', '2014-06-19 13:06:52', '27', '1', '175', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('81', '58', 'Gran Teatre del Liceu', '2014-06-25 19:30:00', '2014-06-25 19:30:00', '27', '1', '177', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('82', '58', 'Teatre Victòria', '2014-06-25 19:30:41', '2014-06-25 19:30:41', '27', '1', '178', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('83', '58', 'Festival Castell de Peralada', '2014-06-25 19:31:15', '2014-06-25 19:31:15', '27', '1', '179', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('84', '58', 'Festival Temporada Alta', '2014-06-25 19:31:51', '2014-06-25 19:31:51', '27', '1', '180', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('85', '2', 'StarLite Marbella', '2014-06-27 15:29:07', '2014-06-27 15:29:07', '27', '1', '183', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('86', '2', 'Llac dels cignes', '2014-06-27 15:32:37', '2014-06-27 15:32:37', '27', '1', '184', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('87', '2', 'Processó de Verges', '2014-06-27 15:35:53', '2014-06-27 15:35:53', '27', '1', '185', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('89', '32', 'Ofertes treball', '2014-06-27 15:58:00', '2014-06-30 09:20:49', '27', '1', '187', '4');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('92', '2', '2014 Gran Teatre del Liceu', '2014-06-30 19:08:12', '2014-06-30 19:08:12', '27', '1', '189', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('93', '2', 'Cantada Calella 2014', '2014-06-30 19:11:53', '2014-06-30 19:11:53', '27', '1', '190', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('94', '2', 'Jornades 2014', '2014-07-01 11:18:45', '2014-07-01 11:18:45', '27', '1', '191', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('96', '58', 'Club Baloncesto Gran Canaria', '2014-07-01 12:29:40', '2014-07-01 12:29:40', '27', '1', '193', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('97', '58', 'Joventut Badalona', '2014-07-01 12:30:58', '2014-07-01 12:30:58', '27', '1', '194', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('98', '58', 'Museo Picasso Malaga', '2014-07-01 12:34:30', '2014-07-01 12:34:30', '27', '1', '195', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('99', '58', 'Naviliera Les Goges', '2014-07-01 12:39:55', '2014-07-01 12:39:55', '27', '1', '196', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('100', '58', 'Lady Shelley', '2014-07-01 12:42:16', '2014-07-01 12:42:16', '27', '1', '197', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('101', '58', 'Festival Internacional de Cinema Fantàstic de Catalunya', '2014-07-01 12:45:03', '2014-07-01 12:45:03', '27', '1', '198', '6');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('102', '2', 'Aquest estiu torna Starlite', '2014-08-11 10:40:36', '2014-08-11 10:40:36', '27', '1', '199', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('103', '2', 'TV GIRONA', '2014-08-12 07:16:12', '2014-08-12 07:16:12', '27', '1', '201', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('104', '2', 'COPA REY', '2014-08-12 07:28:39', '2014-08-12 07:28:39', '27', '1', '202', '1');
insert into fotos_groups (id, parent_id, name, entered, modified, listorder, active, foto_portada, id_ratios) values ('105', '2', 'Passbook', '2014-08-12 07:37:41', '2014-08-12 07:37:41', '27', '1', '203', '1');
drop table if exists fotos_groups_description;
create table fotos_groups_description (
  groups_id int(11) not null ,
  language_id int(11) not null ,
  target varchar(32) not null ,
  title varchar(255) not null ,
  description text not null ,
  class varchar(64) not null ,
  rel varchar(128) not null ,
  rev varchar(128) not null ,
  UNIQUE groups_id (groups_id, language_id)
);

insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('2', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('2', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('2', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('32', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('32', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('32', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('33', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('33', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('33', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('58', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('58', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('58', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('71', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('71', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('71', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('72', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('72', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('72', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('73', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('73', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('73', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('74', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('74', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('74', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('75', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('75', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('75', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('76', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('76', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('76', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('77', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('77', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('77', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('78', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('78', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('78', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('79', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('79', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('79', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('81', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('81', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('81', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('82', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('82', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('82', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('83', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('83', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('83', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('84', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('84', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('84', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('85', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('86', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('87', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('89', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('92', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('93', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('94', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('96', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('97', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('98', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('99', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('100', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('101', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('102', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('102', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('102', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('103', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('103', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('103', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('104', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('104', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('104', '3', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('105', '1', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('105', '2', '', '', '', '', '', '');
insert into fotos_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('105', '3', '', '', '', '', '', '');
drop table if exists fotos_ratios;
create table fotos_ratios (
  id int(11) not null auto_increment,
  nom varchar(100) not null ,
  ratio decimal(10,2) not null ,
  thumb1_h int(11) not null ,
  thumb1_w int(11) not null ,
  thumb2_h int(11) not null ,
  thumb2_w int(11) not null ,
  thumb3_h int(11) not null ,
  thumb3_w int(11) not null ,
  PRIMARY KEY (id)
);

insert into fotos_ratios (id, nom, ratio, thumb1_h, thumb1_w, thumb2_h, thumb2_w, thumb3_h, thumb3_w) values ('1', 'General', '1.50', '147', '220', '227', '340', '469', '704');
insert into fotos_ratios (id, nom, ratio, thumb1_h, thumb1_w, thumb2_h, thumb2_w, thumb3_h, thumb3_w) values ('3', 'Slides grans', '2.60', '500', '1300', '0', '0', '0', '0');
insert into fotos_ratios (id, nom, ratio, thumb1_h, thumb1_w, thumb2_h, thumb2_w, thumb3_h, thumb3_w) values ('4', 'Slides petits', '3.73', '260', '970', '0', '0', '0', '0');
insert into fotos_ratios (id, nom, ratio, thumb1_h, thumb1_w, thumb2_h, thumb2_w, thumb3_h, thumb3_w) values ('5', 'Vertical', '0.76', '225', '170', '660', '500', '0', '0');
insert into fotos_ratios (id, nom, ratio, thumb1_h, thumb1_w, thumb2_h, thumb2_w, thumb3_h, thumb3_w) values ('6', 'Clients', '1.00', '300', '300', '0', '0', '0', '0');
drop table if exists languages;
create table languages (
  id int(11) not null auto_increment,
  name varchar(32) not null ,
  code char(2) not null ,
  i18n_code varchar(5) not null ,
  image varchar(64) ,
  directory varchar(32) ,
  listorder int(3) ,
  active int(1) default '1' not null ,
  PRIMARY KEY (id),
  KEY IDX_LANGUAGES_NAME (name)
);

insert into languages (id, name, code, i18n_code, image, directory, listorder, active) values ('1', 'Català', 'ca', 'ca_ES', 'band_cat.gif', 'catala', '1', '1');
insert into languages (id, name, code, i18n_code, image, directory, listorder, active) values ('2', 'Español', 'es', 'es_ES', 'band_esp.gif', 'espanol', '2', '1');
insert into languages (id, name, code, i18n_code, image, directory, listorder, active) values ('3', 'English', 'en', 'en_GB', 'band_eng.gif', 'english', '3', '1');
insert into languages (id, name, code, i18n_code, image, directory, listorder, active) values ('4', 'Français', 'fr', 'fr_FR', 'band_fra.gif', 'francais', '4', '0');
drop table if exists llistats;
create table llistats (
  id int(11) not null auto_increment,
  group_id int(11) not null ,
  autor varchar(255) not null ,
  fotos_groups_id int(11) not null ,
  published datetime not null ,
  active int(1) not null ,
  entered datetime not null ,
  modified datetime not null ,
  listorder int(11) not null ,
  lat decimal(24,20) not null ,
  lon decimal(24,20) not null ,
  link varchar(255) not null ,
  generic1 varchar(250) not null ,
  generic2 varchar(250) not null ,
  generic3 varchar(250) not null ,
  generic4 varchar(256) not null ,
  generic5 varchar(256) not null ,
  inscripcions int(1) not null ,
  portada int(1) not null ,
  UNIQUE id (id)
);

insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('156', '2', '', '72', '0000-00-00 00:00:00', '1', '2014-06-18 16:02:19', '2014-08-19 10:22:42', '1', '0.00000000000000000000', '0.00000000000000000000', 'http://www.auditori.cat', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('160', '3', '', '76', '0000-00-00 00:00:00', '0', '2014-06-18 16:13:18', '2014-06-19 13:04:45', '1', '0.00000000000000000000', '0.00000000000000000000', 'http://www.fcbarcelona.cat', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('162', '5', '', '0', '0000-00-00 00:00:00', '1', '2014-06-18 16:27:31', '2014-08-19 10:26:16', '1', '0.00000000000000000000', '0.00000000000000000000', 'http://www.elpuntavui.cat/noticia/article/5-cultura/19-cultura/718206-el-festival-de-circ-de-figueres-supera-les-20000-entrades.html?cca=1', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('163', '6', '', '0', '0000-00-00 00:00:00', '1', '2014-06-18 16:52:38', '2014-08-19 14:06:36', '1', '0.00000000000000000000', '0.00000000000000000000', '', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('164', '2', '', '73', '0000-00-00 00:00:00', '1', '2014-06-19 12:32:56', '2014-06-19 13:05:00', '2', '0.00000000000000000000', '0.00000000000000000000', 'http://www.lesarts.com/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('165', '2', '', '74', '0000-00-00 00:00:00', '1', '2014-06-19 12:37:11', '2014-06-19 13:05:06', '3', '0.00000000000000000000', '0.00000000000000000000', 'http://latlantidavic.cat/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('166', '2', '', '75', '0000-00-00 00:00:00', '1', '2014-06-19 12:51:19', '2014-06-19 13:05:12', '4', '0.00000000000000000000', '0.00000000000000000000', 'http://grec.bcn.cat/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('167', '3', '', '77', '0000-00-00 00:00:00', '0', '2014-06-19 13:02:10', '2014-06-19 13:04:40', '2', '0.00000000000000000000', '0.00000000000000000000', 'http://www.acb.com/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('168', '3', '', '78', '0000-00-00 00:00:00', '1', '2014-06-19 13:04:33', '2014-06-19 13:04:33', '3', '0.00000000000000000000', '0.00000000000000000000', 'http://www.acb.com/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('169', '3', '', '79', '0000-00-00 00:00:00', '1', '2014-06-19 13:06:59', '2014-08-19 13:02:15', '4', '0.00000000000000000000', '0.00000000000000000000', 'http://www.euroleague.net', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('170', '5', '', '0', '0000-00-00 00:00:00', '1', '2014-06-19 13:34:05', '2014-08-19 10:27:48', '2', '0.00000000000000000000', '0.00000000000000000000', '/admin/js/kcfinder/upload/files/WEB24SE_BCNinnovadores_INNOVADORES.pdf', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('171', '6', '', '0', '0000-00-00 00:00:00', '0', '2014-06-19 13:40:18', '2014-06-25 18:35:35', '2', '0.00000000000000000000', '0.00000000000000000000', '', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('172', '2', '', '81', '0000-00-00 00:00:00', '1', '2014-06-25 19:30:17', '2014-08-19 13:00:44', '5', '0.00000000000000000000', '0.00000000000000000000', '', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('173', '2', '', '82', '0000-00-00 00:00:00', '1', '2014-06-25 19:30:48', '2014-08-19 13:01:00', '6', '0.00000000000000000000', '0.00000000000000000000', '', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('174', '2', '', '83', '0000-00-00 00:00:00', '1', '2014-06-25 19:31:22', '2014-08-19 13:01:21', '7', '0.00000000000000000000', '0.00000000000000000000', '', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('175', '2', '', '84', '0000-00-00 00:00:00', '1', '2014-06-25 19:32:03', '2014-08-19 13:01:41', '8', '0.00000000000000000000', '0.00000000000000000000', '', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('176', '5', '', '0', '0000-00-00 00:00:00', '1', '2014-06-27 15:39:05', '2014-08-19 10:29:50', '3', '0.00000000000000000000', '0.00000000000000000000', '/admin/js/kcfinder/upload/files/ArticleVanguardiaKoobin.pdf', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('177', '5', '', '0', '0000-00-00 00:00:00', '1', '2014-06-27 15:40:25', '2014-08-19 10:30:55', '4', '0.00000000000000000000', '0.00000000000000000000', '/admin/js/kcfinder/upload/files/ArticleTotssomGirona.pdf', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('178', '5', '', '0', '0000-00-00 00:00:00', '1', '2014-06-27 15:41:03', '2014-08-19 10:31:57', '5', '0.00000000000000000000', '0.00000000000000000000', '/admin/js/kcfinder/upload/files/Nousistemadevenda.pdf', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('179', '5', '', '0', '0000-00-00 00:00:00', '1', '2014-06-27 15:41:52', '2014-08-19 15:33:59', '6', '0.00000000000000000000', '0.00000000000000000000', '/admin/js/kcfinder/upload/files/TemporadaAlta2012.pdf', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('180', '5', '', '0', '0000-00-00 00:00:00', '1', '2014-06-27 15:43:19', '2014-08-19 15:35:59', '7', '0.00000000000000000000', '0.00000000000000000000', '/admin/js/kcfinder/upload/files/Lestrenadelnouespectacle.pdf', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('181', '3', '', '96', '0000-00-00 00:00:00', '1', '2014-07-01 12:29:54', '2014-07-01 12:29:54', '5', '0.00000000000000000000', '0.00000000000000000000', 'http://www.cbgrancanaria.net/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('182', '3', '', '97', '0000-00-00 00:00:00', '1', '2014-07-01 12:31:05', '2014-07-01 12:31:05', '6', '0.00000000000000000000', '0.00000000000000000000', 'http://www.penya.com/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('183', '7', '', '98', '0000-00-00 00:00:00', '1', '2014-07-01 12:34:41', '2014-07-01 12:34:41', '1', '0.00000000000000000000', '0.00000000000000000000', 'http://museopicassomalaga.org/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('184', '7', '', '99', '0000-00-00 00:00:00', '1', '2014-07-01 12:40:03', '2014-07-01 12:40:03', '2', '0.00000000000000000000', '0.00000000000000000000', 'http://www.navilieralesgoges.cat', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('185', '7', '', '100', '0000-00-00 00:00:00', '1', '2014-07-01 12:42:45', '2014-07-01 12:42:45', '3', '0.00000000000000000000', '0.00000000000000000000', 'http://www.ladyshelley.com/', '', '', '', '', '', '0', '0');
insert into llistats (id, group_id, autor, fotos_groups_id, published, active, entered, modified, listorder, lat, lon, link, generic1, generic2, generic3, generic4, generic5, inscripcions, portada) values ('186', '7', '', '101', '0000-00-00 00:00:00', '1', '2014-07-01 12:45:24', '2014-08-19 10:23:26', '4', '0.00000000000000000000', '0.00000000000000000000', 'http://sitgesfilmfestival.com', '', '', '', '', '', '0', '0');
drop table if exists llistats_description;
create table llistats_description (
  llistats_id int(11) not null ,
  language_id int(11) not null ,
  titol varchar(255) not null ,
  titol2 varchar(255) not null ,
  titol3 text not null ,
  descripcio text not null ,
  descripcio2 text not null ,
  descripcio3 text not null ,
  download varchar(128) not null ,
  viewed int(11) not null ,
  UNIQUE llibres_id (llistats_id, language_id)
);

insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('156', '1', 'Auditori de Barcelona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('156', '2', 'Auditori de Barcelona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('156', '3', 'Auditori de Barcelona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('160', '1', 'FC Barcelona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('160', '2', 'FC Barcelona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('160', '3', 'FC Barcelona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('162', '1', 'El Punt Avui (Girona), 19/02/2014, “El Festival de Circ de Figueres supera les 20.000 entrades”', '', '', '', '', '', '', '9');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('162', '2', 'El Punt Avui (Girona), 19/02/2014, “El Festival de Circo de Figueres supera las 20.000 entradas”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('162', '3', 'El Punt Avui (Girona), 19/02/2014, “The Circus Festival in Figueres exceeds 20,000 tickets\"', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('163', '1', 'Programador', '', '', '<p>Es necessita un programador j&uacute;nior a temps complet. Despr&eacute;s d&#39;un per&iacute;ode de formaci&oacute; podr&agrave; passar a assumir la direcci&oacute; d&#39;un equip d&#39;entre 2 i 4 persones, pel que es requereixen dots comunicatives, molta exig&egrave;ncia i responsabilitat.</p>', '<p>Es necessita un programador j&uacute;nior a temps complet. Despr&eacute;s d&#39;un per&iacute;ode de formaci&oacute; podr&agrave; passar a assumir la direcci&oacute; d&#39;un equip d&#39;entre 2 i 4 persones, pel que es requereixen dots comunicatives, molta exig&egrave;ncia i responsabilitat.</p>

<p><strong>Coneixements extensos en</strong>:</p>

<ul class=\"square\">
	<li>PHP</li>
	<li>MySQL</li>
	<li>HTML</li>
	<li>Javascript</li>
	<li>jQuery</li>
	<li>CSS</li>
</ul>

<p><strong>Es valorar&agrave;</strong>:</p>

<ul class=\"square\">
	<li>Plantilles Smarty</li>
	<li>Linux</li>
	<li>Desenvolupament de web per m&ograve;bil</li>
	<li>.Net (VB y C#)</li>
</ul>

<p>&Eacute;s necessari que domini tant parlat com escrit el catal&agrave; i el castell&agrave;.<br />
Ha de tenir un nivell alt en angl&egrave;s.<br />
<br />
<strong>Estudis m&iacute;nims</strong><br />
Formaci&oacute; Professional Grau Superior - Inform&agrave;tica<br />
<br />
<strong>Experi&egrave;ncia m&iacute;nima</strong><br />
1 any<br />
<br />
<strong>Tipus de contracte</strong><br />
Indefinit<br />
<br />
<strong>Jornada laboral</strong><br />
Complerta<br />
<br />
<strong>Horari</strong><br />
8:30 - 13:30 / 15 - 18:30<br />
Div 8 - 14<br />
<br />
<strong>Salari</strong><br />
900 &euro; - 1.500 &euro; Brut/mes</p>', '', '', '86');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('163', '2', 'Programador', '', '', '<p>Se necesita un programador junior a tiempo completo. Tras un periodo de formaci&oacute;n podr&aacute; pasar a asumir la direcci&oacute;n de un equipo de entre 2 y 4 personas, por lo que se requieren dotes comunicativas, mucha exigencia y responsabilidad.</p>', '<p>Se necesita un programador junior a tiempo completo. Tras un periodo de formaci&oacute;n podr&aacute; pasar a asumir la direcci&oacute;n de un equipo de entre 2 y 4 personas, por lo que se requieren dotes comunicativas, mucha exigencia y responsabilidad.</p>

<p><strong>Conocimientos extensos en:</strong></p>

<ul>
	<li>PHP</li>
	<li>MySQL</li>
	<li>HTML</li>
	<li>Javascript</li>
	<li>jQuery</li>
	<li>CSS</li>
</ul>

<p><strong>Se valorar&aacute;:</strong></p>

<ul>
	<li>plantillas Smarty</li>
	<li>Linux</li>
	<li>Desarrollo de web para m&oacute;vil</li>
	<li>.Net (VB y C #)</li>
</ul>

<p>Es necesario que domine tanto hablado como escrito el catal&aacute;n y el castellano.</p>

<p>Debe tener un nivel alto en ingl&eacute;s.</p>

<p><strong>Estudios m&iacute;nimos</strong></p>

<p>Formaci&oacute;n Profesional Grado Superior - Inform&aacute;tica</p>

<p><strong>Experiencia m&iacute;nima</strong></p>

<p>1 a&ntilde;o</p>

<p><strong>Tipo de contrato</strong></p>

<p>Indefinido</p>

<p><strong>Jornada laboral</strong></p>

<p>Completa</p>

<p><strong>Horario</strong></p>

<p>08:30 - 13:30 h&nbsp;/ 15 - 18:30 h</p>

<p>Vie 8-14</p>

<p><strong>Salario</strong></p>

<p>&euro; 900 - &euro; 1500 bruto / mes</p>

<p>&nbsp;</p>', '', '', '11');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('163', '3', 'Programmer', '', '', '<p>It requires a full-time junior programmer. After a training period may happen to assume the leadership of a team of 2 to 4 people, so communication skills are required, many demands and responsibility.</p>', '<p>It requires a full-time junior programmer. After a training period may happen to assume the leadership of a team of 2 to 4 people, so communication skills are required, many demands and responsibility.</p>

<p><strong>Extensive knowledge in:</strong></p>

<ul>
	<li>PHP</li>
	<li>MySQL</li>
	<li>HTML</li>
	<li>JavaScript</li>
	<li>jQuery</li>
	<li>CSS</li>
</ul>

<p><strong>Be assessed:</strong></p>

<ul>
	<li>Smarty Templates</li>
	<li>Linux</li>
	<li>Mobile Web Development</li>
	<li>NET (VB and C #)</li>
</ul>

<p>Domain is necessary for both spoken and written Spanish and Catalan.</p>

<p>Must have a high level in English.</p>

<p><strong>Studies minimal</strong></p>

<p>Training Level - Computers</p>

<p><strong>Minimum Experience</strong></p>

<p>1 year</p>

<p><strong>Type of contract</strong></p>

<p>Undefined</p>

<p><strong>Workday</strong></p>

<p>Complete</p>

<p><strong>Hours</strong></p>

<p>8:30 to 13:30 h / 15 - 18:30 h</p>

<p>Fri 8-14</p>

<p><strong>Salary</strong></p>

<p>&euro; 900 - &euro; 1,500 gross / month</p>', '', '', '5');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('164', '1', 'Palau de les Arts Reina Sofía', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('164', '2', 'Palau de les Arts Reina Sofía', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('164', '3', 'Palau de les Arts Reina Sofía', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('165', '1', 'L’Atlàntida Centre d’Arts Escèniques d’Osona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('165', '2', 'L’Atlàntida Centre d’Arts Escèniques d’Osona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('165', '3', 'L’Atlàntida Centre d’Arts Escèniques d’Osona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('166', '1', 'Festival GREC 2013', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('166', '2', 'Festival GREC 2013', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('166', '3', 'Festival GREC 2013', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('167', '1', 'ACB (Asociación de Clubes de Baloncesto)', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('167', '2', 'ACB (Asociación de Clubes de Baloncesto)', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('167', '3', 'ACB (Asociación de Clubes de Baloncesto)', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('168', '1', 'Clubes Liga Endesa', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('168', '2', 'Clubes Liga Endesa', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('168', '3', 'Clubes Liga Endesa', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('169', '1', 'Euroleague', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('169', '2', 'Euroleague', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('169', '3', 'Euroleague', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('170', '1', 'Suplemento Innovadores, 24/09/2013, “El salt a Europa”', '', '', '', '', '', '', '1');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('170', '2', 'Suplemento Innovadores, 24/09/2013, “El salto a Europa”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('170', '3', 'Suplemento Innovadores, 24/09/2013, “Jump to Europe”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('171', '1', 'Assistent Departament Suport', '', '', '<p>Assistent del departament de suport. En depend&egrave;ncia del cap de suport realitzar&agrave; tasques relacionades amb l&#39;atenci&oacute; a l&#39;usuari (resoluci&oacute; d&#39;incid&egrave;ncies, resposta a dubtes d&#39;&uacute;s, formacions online i presencials...) i de gesti&oacute; interna del departament (actualitzaci&oacute; de la web, gesti&oacute; de xarxes socials, redacci&oacute; de manuals...).</p>', '<p>Assistent del departament de suport. En depend&egrave;ncia del cap de suport realitzar&agrave; tasques relacionades amb l&#39;atenci&oacute; a l&#39;usuari (resoluci&oacute; d&#39;incid&egrave;ncies, resposta a dubtes d&#39;&uacute;s, formacions online i presencials...) i de gesti&oacute; interna del departament (actualitzaci&oacute; de la web, gesti&oacute; de xarxes socials, redacci&oacute; de manuals...).</p>

<p><strong>Coneixements necessaris</strong>:</p>

<ul class=\"square\">
	<li>Adobe InDesign</li>
	<li>Adobe Photoshop</li>
	<li>Comunicaci&oacute; externa</li>
	<li>Comunicaci&oacute; interna</li>
	<li>Internet</li>
	<li>Microsoft Office</li>
	<li>Mac OS<span id=\"cke_bm_655E\" style=\"display: none;\">&nbsp;</span></li>
</ul>

<p><strong>Requisits m&iacute;nims</strong>:</p>

<p>Es requereix una persona responsable, organitzada i proactiva. &Eacute;s imprescindible que domini, tant escrit, parlat com llegit, el catal&agrave;, el castell&agrave; i l&#39;angl&egrave;s. Es valoraran altres lleng&uuml;es (alemany, franc&egrave;s o itali&agrave;).&nbsp; El candidat o candidata ha de saber desenvolupar-se amb programes com Photoshop o Indesign, xarxes socials (Twitter i Facebook) i tenir una base de coneixements t&egrave;cnics i inform&agrave;tics, a nivell d&#39;usuari mig-avan&ccedil;at.&nbsp; Despr&eacute;s d&#39;un per&iacute;ode de formaci&oacute; podr&agrave; realitzar formacions presencialment, pel que es requereixen habilitats comunicatives suficients per a parlar en p&uacute;blic i fer-se entendre amb facilitat. L&#39;equip de treball &eacute;s jove, proper i amb bon ambient.</p>

<p><br />
<strong>Estudis m&iacute;nims</strong><br />
Ciclo Formatiu Superior<br />
<br />
<strong>Experi&egrave;ncia m&iacute;nima</strong><br />
1 any<br />
<br />
<strong>Tipus de contracte</strong><br />
Indefinit<br />
<br />
<strong>Jornada laboral</strong><br />
Complerta<br />
<br />
<strong>Horari</strong><br />
8:30 - 13:30 / 15 - 18:30<br />
Div 8 - 14<br />
<br />
<strong>Salari</strong><br />
1.200 &euro; - 1.500 &euro; Brut/mes</p>', '', '', '9');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('171', '2', 'Assistent Departament Suport', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis diam ut nibh pretium, ut blandit turpis consequat. Proin porttitor dolor eget quam luctus, a varius erat suscipit.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis diam ut nibh pretium, ut blandit turpis consequat. Proin porttitor dolor eget quam luctus, a varius erat suscipit. Mauris et arcu at dui egestas fermentum imperdiet vel leo. Maecenas suscipit dolor et tincidunt dapibus. Vestibulum metus orci, laoreet in urna sed, mattis iaculis metus. Phasellus ac ultrices dui. Mauris orci nunc, vestibulum sit amet neque eget, tristique varius sem. Nunc nec volutpat diam. Nullam mattis lacus sit amet justo faucibus placerat. Nullam luctus porttitor sapien, quis suscipit tortor lacinia ut. Aenean odio urna, malesuada vitae nulla eu, semper tincidunt quam. Cras in hendrerit ligula. Integer feugiat justo sapien, vitae tincidunt purus sollicitudin sit amet.</p>

<p>Nullam aliquam tellus nisi, vel laoreet libero convallis ac. Aenean vitae suscipit arcu. In non luctus nunc. Nunc pharetra pharetra est, nec aliquam sapien porttitor sed. Sed aliquet sem aliquet scelerisque dapibus. Vestibulum ac mauris at turpis ultrices feugiat. Duis nisi sapien, lacinia nec condimentum ac, scelerisque sed felis. Donec pellentesque fermentum mollis. Mauris augue dolor, mollis facilisis commodo tempor, facilisis vitae turpis. Mauris vulputate venenatis libero, non egestas leo commodo vel.</p>', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('171', '3', 'Assistent Departament Suport', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis diam ut nibh pretium, ut blandit turpis consequat. Proin porttitor dolor eget quam luctus, a varius erat suscipit.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis diam ut nibh pretium, ut blandit turpis consequat. Proin porttitor dolor eget quam luctus, a varius erat suscipit. Mauris et arcu at dui egestas fermentum imperdiet vel leo. Maecenas suscipit dolor et tincidunt dapibus. Vestibulum metus orci, laoreet in urna sed, mattis iaculis metus. Phasellus ac ultrices dui. Mauris orci nunc, vestibulum sit amet neque eget, tristique varius sem. Nunc nec volutpat diam. Nullam mattis lacus sit amet justo faucibus placerat. Nullam luctus porttitor sapien, quis suscipit tortor lacinia ut. Aenean odio urna, malesuada vitae nulla eu, semper tincidunt quam. Cras in hendrerit ligula. Integer feugiat justo sapien, vitae tincidunt purus sollicitudin sit amet.</p>

<p>Nullam aliquam tellus nisi, vel laoreet libero convallis ac. Aenean vitae suscipit arcu. In non luctus nunc. Nunc pharetra pharetra est, nec aliquam sapien porttitor sed. Sed aliquet sem aliquet scelerisque dapibus. Vestibulum ac mauris at turpis ultrices feugiat. Duis nisi sapien, lacinia nec condimentum ac, scelerisque sed felis. Donec pellentesque fermentum mollis. Mauris augue dolor, mollis facilisis commodo tempor, facilisis vitae turpis. Mauris vulputate venenatis libero, non egestas leo commodo vel.</p>', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('172', '1', 'Gran Teatre del Liceu', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('172', '2', 'Gran Teatre del Liceu', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('172', '3', 'Gran Teatre del Liceu', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('173', '1', 'Teatre Victòria', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('173', '2', 'Teatre Victòria', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('173', '3', 'Teatre Victòria', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('174', '1', 'Festival Castell de Peralada', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('174', '2', 'Festival Castell de Peralada', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('174', '3', 'Festival Castell de Peralada', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('175', '1', 'Festival Temporada Alta', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('175', '2', 'Festival Temporada Alta', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('175', '3', 'Festival Temporada Alta', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('176', '1', 'La Vanguardia, 13/01/2012, “L\'entrada intel·ligent”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('176', '2', 'La Vanguardia, 13/01/2012, “La entrada inteligente”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('176', '3', 'La Vanguardia, 13/01/2012, “The smart ticket”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('177', '1', 'Tots Som Girona, núm.8, “Les entrades del segle XXI”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('177', '2', 'Tots Som Girona, num.8, “Las entradas del siglo XXI”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('177', '3', 'Tots Som Girona, num.8, “The tickets of the XXI century”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('178', '1', 'Teatralnet, 06/07/2012, “Temporada Alta estrena un nou sistema de venda on-line”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('178', '2', 'Teatralnet, 06/07/2012, “Temporada Alta estrena un nuevo sistema de venta on-line”', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('178', '3', 'Teatralnet, 06/07/2012, “Temporada Alta debuts a new system of online sales\"', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('179', '1', 'AraGirona.cat, 06/07/2012, “Les T de Teatre i el musical Forever Young al Temporada Alta 2012', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('179', '2', 'AraGirona.cat, 06/07/2012, “Las T de Teatre y el musical Forever Young en Temporada Alta 2012', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('179', '3', 'AraGirona.cat, 06/07/2012, “The T de Teatre and the musical Forever Young in Temporada Alta 2012\"', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('180', '1', 'Gironainfo.cat, 06/07/2012, “L’estrena del nou espectacle de T de Teatre i Mayumana, a Temporada Alta 2012', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('180', '2', 'Gironainfo.cat, 06/07/2012, “La estrena del nuevo espectáculo de T de Teatre y Mayumana, en Temporada Alta 2012', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('180', '3', 'Gironainfo.cat, 06/07/2012, \"The premiere of the new show of T de Teatre and Mayumana in Temporada Alta 2012 \"', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('181', '1', 'Club Baloncesto Gran Canaria', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('182', '1', 'Joventut Badalona', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('183', '1', 'http://museopicassomalaga.org/', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('184', '1', 'Naviliera Les Goges', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('185', '1', 'Lady Shelley', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('186', '1', 'Festival Internacional de Cinema Fantàstic de Catalunya', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('186', '2', 'Festival Internacional de Cinema Fantàstic de Catalunya', '', '', '', '', '', '', '0');
insert into llistats_description (llistats_id, language_id, titol, titol2, titol3, descripcio, descripcio2, descripcio3, download, viewed) values ('186', '3', 'Festival Internacional de Cinema Fantàstic de Catalunya', '', '', '', '', '', '', '0');
drop table if exists llistats_groups;
create table llistats_groups (
  id int(11) not null auto_increment,
  parent_id int(10) default '0' not null ,
  name varchar(128) not null ,
  entered datetime not null ,
  modified datetime not null ,
  listorder int(11) not null ,
  active int(1) not null ,
  fotos_groups_id int(11) not null ,
  UNIQUE id (id)
);

insert into llistats_groups (id, parent_id, name, entered, modified, listorder, active, fotos_groups_id) values ('1', '0', 'Clients', '2014-06-18 14:00:39', '2014-06-18 14:03:32', '1', '1', '0');
insert into llistats_groups (id, parent_id, name, entered, modified, listorder, active, fotos_groups_id) values ('2', '1', 'Cultura', '2014-06-18 14:01:30', '2014-08-18 12:49:46', '1', '1', '0');
insert into llistats_groups (id, parent_id, name, entered, modified, listorder, active, fotos_groups_id) values ('3', '1', 'Esport', '2014-06-18 14:01:54', '2014-08-18 12:52:14', '2', '1', '0');
insert into llistats_groups (id, parent_id, name, entered, modified, listorder, active, fotos_groups_id) values ('5', '0', 'Premsa', '2014-06-18 14:03:10', '2014-06-18 14:03:10', '2', '1', '0');
insert into llistats_groups (id, parent_id, name, entered, modified, listorder, active, fotos_groups_id) values ('6', '0', 'Treball', '2014-06-18 14:04:32', '2014-06-18 14:04:32', '3', '1', '0');
insert into llistats_groups (id, parent_id, name, entered, modified, listorder, active, fotos_groups_id) values ('7', '1', 'Oci', '2014-06-25 19:33:39', '2014-08-18 12:52:51', '3', '1', '0');
insert into llistats_groups (id, parent_id, name, entered, modified, listorder, active, fotos_groups_id) values ('8', '3', 'Club Baloncesto Gran Canaria', '2014-07-01 12:28:44', '2014-07-01 12:28:44', '1', '1', '95');
drop table if exists llistats_groups_description;
create table llistats_groups_description (
  groups_id int(11) not null ,
  language_id int(11) not null ,
  target varchar(32) not null ,
  title varchar(255) not null ,
  description text not null ,
  class varchar(64) not null ,
  rel varchar(128) not null ,
  rev varchar(128) not null ,
  UNIQUE groups_id (groups_id, language_id)
);

insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('1', '1', '_blank', 'Clients', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('1', '2', '_blank', '', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('1', '3', '_blank', '', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('2', '1', '_blank', 'Cultura', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('2', '2', '_blank', 'Cultura', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('2', '3', '_blank', 'Culture', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('3', '1', '_blank', 'Esport', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('3', '2', '_blank', 'Deporte', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('3', '3', '_blank', 'Sport', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('5', '1', '_blank', 'Premsa', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('5', '2', '_blank', '', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('5', '3', '_blank', '', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('6', '1', '_blank', 'Treball', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('6', '2', '_blank', '', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('6', '3', '_blank', '', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('7', '1', '_blank', 'Oci', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('7', '2', '_blank', 'Ocio', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('7', '3', '_blank', 'Leisure', '', '', '', '');
insert into llistats_groups_description (groups_id, language_id, target, title, description, class, rel, rev) values ('8', '1', '_blank', 'Club Baloncesto Gran Canaria', '<p>http://www.cbgrancanaria.net/</p>', '', '', '');
drop table if exists llistats_related;
create table llistats_related (
  llistats_id int(11) not null ,
  related_id int(11) not null ,
  id_llistats_groups int(10) not null ,
  entered datetime not null ,
  PRIMARY KEY (llistats_id, related_id)
);

drop table if exists mail_contact;
create table mail_contact (
  id int(11) not null auto_increment,
  name varchar(128) not null ,
  mail varchar(128) not null ,
  subject varchar(255) not null ,
  message text not null ,
  result varchar(32) not null ,
  send datetime not null ,
  UNIQUE id (id)
);

drop table if exists mail_premsa;
create table mail_premsa (
  id int(11) not null auto_increment,
  name varchar(128) not null ,
  mail varchar(128) not null ,
  tel int(11) not null ,
  message text not null ,
  carrec varchar(250) not null ,
  seccio varchar(100) not null ,
  localitat varchar(100) not null ,
  carrer varchar(100) not null ,
  cp varchar(100) not null ,
  provincia varchar(100) not null ,
  result varchar(32) not null ,
  send datetime not null ,
  mobil int(12) not null ,
  nif varchar(10) not null ,
  data_naix varchar(15) not null ,
  professio varchar(250) not null ,
  quota varchar(100) not null ,
  num_compte int(30) not null ,
  anual_semestral varchar(10) not null ,
  autoritzacio tinyint(1) not null ,
  acceptacio tinyint(1) not null ,
  UNIQUE id (id)
);

insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('5', 'Xavi Pujolràs', 'eventis2@mantis.cat', '666', 'asdfasdf', 'La Mantis', '', '', '', '', '', 'success', '2014-02-21 09:12:07', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('6', 'Xavi Pujolràs', 'xpixow@gmail.com', '666', 'sdaf sdaf asdfa fasd fsda', 'La Mantis', '', '', '', '', '', 'success', '2014-02-21 09:25:59', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('7', 'jaume mantis', 'jaume.sebastia@gmail.com', '650607198', 'Hola', 'LaMantis', '', '', '', '', '', 'success', '2014-06-10 10:20:16', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('8', 'jaume mantis', 'jaume.sebastia@gmail.com', '666666888', 'hola 2', 'LAMANTIS', '', '', '', '', '', 'success', '2014-06-10 10:21:02', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('9', 'jaume', 'jaume.sebastia@gmail.com', '888999555', 'adsasd sqdasd asd ad a', '111', '', '', '', '', '', 'success', '2014-06-10 10:25:16', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('10', 'jaum', 'jaume.sebastia@gmail.com', '2147483647', 'sdf', 'xsdf', '', '', '', '', '', 'success', '2014-06-10 10:27:00', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('11', '1', 'jaume.sebastia@gmail.com', '123456', '1<br />
2<br />
3<br />
4<br />
5<br />
', 'lamantis', '', '', '', '', '', 'success', '2014-06-10 11:49:47', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('12', '111111111111111111', 'jaume.sebastia@gmail.com', '1', '1', '1', '', '', '', '', '', 'success', '2014-06-10 12:05:17', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('13', '290', 'jaume.sebastia@gmail.com', '123', '3', '3', '', '', '', '', '', 'success', '2014-06-10 12:07:18', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('14', '7', 'jaume.sebastia@gmail.com', '777777777', '7', '7', '', '', '', '', '', 'success', '2014-06-10 12:08:37', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('15', '290', '2', '999888777', '2', '2', '', '', '', '', '', 'success', '2014-06-10 12:21:07', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('16', '456', 'jaume.sebastia@gmail.com', '888888888', '35', '345', '', '', '', '', '', 'success', '2014-06-10 12:32:59', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('17', '234', 'jaume.sebastia@gmail.com', '888777444', '234', '245', '', '', '', '', '', 'success', '2014-06-10 12:40:56', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('18', 'fyu', 'jaume.sebastia@gmail.com', '2147483647', 'sddddd', 'vfjk,l', '', '', '', '', '', 'success', '2014-06-10 12:49:56', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('19', '54646', 'jaume.sebastia@gmail.com', '2147483647', '456546', '456546', '', '', '', '', '', 'success', '2014-06-10 13:01:02', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('20', 'ghjdgf', 'jaume.sebastia@gmail.com', '2147483647', 'dfghdgfhd', 'dfghdgfh', '', '', '', '', '', 'success', '2014-06-10 13:47:47', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('21', 'sdfg', 'jaume.sebastia@gmail.com', '2147483647', 'sdgf', 'sdgf', '', '', '', '', '', 'success', '2014-06-10 13:50:54', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('22', 'JAUME', 'jaume.sebastia@gmail.com', '2147483647', 'SADASDASDASD', 'ASD', '', '', '', '', '', 'success', '2014-06-16 09:32:38', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('23', 'Jaume Sebastia', 'jaumemantis@gmail.com', '698854712', 'Praesent lacinia volutpat dolor, sit amet eleifend velit aliquam sed. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras tempor est id enim cursus, malesuada tempus sem sodales. Aliquam semper lobortis eros ac tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus pulvinar accumsan dolor, in tincidunt lacus ultrices nec. In adipiscing massa nec arcu mattis, eget aliquet est mollis. Sed vel sodales diam. Etiam tincidunt eu sem iaculis sollicitudin. Curabitur sit amet fermentum ipsum, in tristique quam. Nunc facilisis varius elit nec vulputate. Phasellus cursus diam nec lacus sodales tincidunt. ', 'LaMantis', '', '', '', '', '', 'success', '2014-06-16 10:57:58', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('24', 'JAume', 'jaumemantis@gmail.om', '698854712', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tempus velit sit amet ipsum tempus, ac facilisis nunc viverra. Curabitur hendrerit lectus ac odio sodales elementum. Mauris consectetur, mauris eu lacinia tempor, augue odio viverra odio, adipiscing faucibus sapien lacus ut elit. Proin congue dolor magna, vel vestibulum arcu interdum quis. Maecenas vehicula sit amet diam vel accumsan. Duis eu tristique dui. Nunc nulla est, luctus quis lorem in, lacinia lacinia sem. Phasellus faucibus, neque a luctus eleifend, ligula mauris semper erat, ac posuere enim lacus quis quam. Ut viverra metus est, non varius nisl porta vel. Nullam quis augue non eros semper consectetur vitae ac mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent blandit tellus sem, eget blandit sapien sagittis at. ', 'LaMAntis', '', '', '', '', '', 'success', '2014-06-16 11:03:28', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('25', 'jaume mantis', 'jaumemantis@gmail.om', '666888777', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tempus velit sit amet ipsum tempus, ac facilisis nunc viverra. Curabitur hendrerit lectus ac odio sodales elementum. Mauris consectetur, mauris eu lacinia tempor, augue odio viverra odio, adipiscing faucibus sapien lacus ut elit. Proin congue dolor magna, vel vestibulum arcu interdum quis. Maecenas vehicula sit amet diam vel accumsan. Duis eu tristique dui. Nunc nulla est, luctus quis lorem in, lacinia lacinia sem. Phasellus faucibus, neque a luctus eleifend, ligula mauris semper erat, ac posuere enim lacus quis quam. Ut viverra metus est, non varius nisl porta vel. Nullam quis augue non eros semper consectetur vitae ac mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent blandit tellus sem, eget blandit sapien sagittis at. ', 'LaMantis', '', '', '', '', '', 'success', '2014-06-16 11:19:24', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('26', '1', '4', '666999888', '3', '2', '', '', '', '', '', 'success', '2014-06-16 11:57:15', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('27', '1', '4', '5', '3', '2', '', '', '', '', '', 'success', '2014-06-16 12:00:26', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('28', 'Jaume Sebastià', 'jaume.sebastia@gmail.com', '685541236', 'Aenean id arcu a est sagittis tempus. Nunc malesuada tincidunt eros vel mollis. Integer nec magna posuere, adipiscing dolor nec, eleifend purus. Phasellus a nisi pretium, mattis ante nec, posuere ipsum. Aliquam erat volutpat. Vestibulum consequat tincidunt rutrum. Vivamus gravida dolor nec lobortis pharetra. Quisque luctus venenatis est sit amet sollicitudin. Vestibulum vel tempor est. Integer vitae turpis a eros facilisis molestie eu non turpis. Sed posuere neque eget feugiat ullamcorper. Proin a bibendum dolor, a venenatis sem. Mauris feugiat magna id tellus posuere porttitor. ', 'LaMantis', '', '', '', '', '', 'success', '2014-06-27 14:48:40', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('29', 'Jaume Sebastià', 'jaume.sebastia@gmail.com', '666888999', 'Test final d\' un missatge enviat des de koobin', 'LaMantis', '', '', '', '', '', 'success', '2014-07-01 10:45:22', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('30', 'Jaume Sebastià', 'jaume.sebastia@gmail.com', '666888555', 'Test final d\'un missatge do koobin', 'LaMantis', '', '', '', '', '', 'success', '2014-07-01 10:46:50', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('31', 'Jaume Sebastià', 'jaume.sebastia@gmail.com', '623658647', 'Donec ut semper elit, ut hendrerit nibh. Proin feugiat dignissim dapibus. Aliquam porta magna id leo sodales, quis venenatis augue mollis. Pellentesque posuere in ipsum vitae feugiat. Duis tristique, sapien nec mattis luctus, lectus dui gravida sem, a malesuada sapien sem eget justo. Praesent aliquet, tortor eget imperdiet volutpat, dui lectus consequat mauris, quis commodo tortor nibh ac mi. Proin facilisis, nulla vitae accumsan dapibus, erat ipsum tincidunt turpis, sed lacinia ante libero vel metus. Etiam porta sapien eu nulla viverra, ac sagittis nisi vulputate. Nam lobortis libero ac lorem fringilla, ornare faucibus nibh bibendum. Vivamus metus nibh, tristique sit amet eros id, congue rhoncus massa. Sed sed odio ac magna consectetur congue sed at tortor. Aliquam et urna risus. Nam vulputate fermentum leo vitae pulvinar. Nunc at diam pretium mauris malesuada gravida id eu ligula. Proin et pulvinar nulla. Donec nisl purus, pellentesque eu volutpat nec, auctor sed sapien. ', 'LaMantis', '', '', '', '', '', 'success', '2014-07-01 13:36:40', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('32', 'test mantis', 'jaume.sebastia@gmail.com', '666888999', 'test test test', 'laMantis', '', '', '', '', '', 'error', '2014-07-28 11:13:24', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('33', 'test mantis', 'jaume.sebastia@gmail.com', '111111111', 'test test test', 'sss', '', '', '', '', '', 'error', '2014-07-28 11:14:18', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('34', 'test mantis', 'jaume.sebastia@gmail.com', '111222333', 'test test test', 'LaMantis', '', '', '', '', '', 'error', '2014-07-28 11:40:35', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('35', 'test mantis', 'jaume.sebastia@gmail.com', '444555666', 'test test test', '123', '', '', '', '', '', 'error', '2014-07-28 11:51:21', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('36', 'test mantis', 'jaume.sebastia@gmail.com', '444555666', 'test test test', 'LaMantis', '', '', '', '', '', 'error', '2014-07-28 11:55:26', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('37', 'jaume', 'jaume@mantis.cat', '555888999', 'Test', 'LaMantis', '', '', '', '', '', 'error', '2014-07-29 07:19:44', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('38', 'jaume', 'jaume@mantis.cat', '444555888', 'Test', 'LaMantis', '', '', '', '', '', 'error', '2014-07-29 07:21:09', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('39', 'jaume', 'jaume@mantis.cat', '123456789', 'Test', 'LaMantis', '', '', '', '', '', 'error', '2014-07-29 07:23:04', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('40', 'jaume', 'jaume@mantis.cat', '888999555', 'Test', 'LaMantis', '', '', '', '', '', '', '2014-07-29 07:24:42', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('41', 'jaume', 'jaume@mantis.cat', '555888999', 'Test', 'LaMantis', '', '', '', '', '', '', '2014-07-29 07:25:30', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('42', 'jaume', 'jaume@mantis.cat', '555888666', 'Test', 'zxds', '', '', '', '', '', '', '2014-07-29 07:26:11', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('43', 'jaume', 'jaume@mantis.cat', '666888777', 'Test', 'LaMantis', '', '', '', '', '', '', '2014-07-29 07:28:35', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('44', 'jaume', 'jaume@mantis.cat', '999888777', 'Test', 'LaMantis', '', '', '', '', '', '', '2014-07-29 07:34:41', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('45', 'jaume', 'jaume@mantis.cat', '666888777', 'Test', 'LaMantis', '', '', '', '', '', '', '2014-07-29 07:35:35', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('46', 'Jaume Sebastià', 'jaume@mantis.cat', '123456789', 'test', 'LaManris', '', '', '', '', '', '', '2014-07-29 08:25:17', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('47', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:04:38', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('48', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:09:39', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('49', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:11:01', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('50', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:12:02', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('51', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:15:28', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('52', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:17:27', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('53', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:17:49', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('54', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:19:33', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('55', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:21:27', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('56', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:22:58', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('57', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:23:36', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('58', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:24:27', '0', '', '', '', '', '0', '', '0', '0');
insert into mail_premsa (id, name, mail, tel, message, carrec, seccio, localitat, carrer, cp, provincia, result, send, mobil, nif, data_naix, professio, quota, num_compte, anual_semestral, autoritzacio, acceptacio) values ('59', 'nom i cognom', 'umbert.pensato@koobin.com', '987654321', 'this is a test', 'test', '', '', '', '', '', '', '2014-07-29 11:26:56', '0', '', '', '', '', '0', '', '0', '0');
drop table if exists menus;
create table menus (
  id int(11) not null auto_increment,
  name varchar(128) not null ,
  listorder int(11) not null ,
  entered datetime not null ,
  modified datetime not null ,
  UNIQUE id (id)
);

insert into menus (id, name, listorder, entered, modified) values ('4', 'Menú Principal', '1', '2011-11-03 15:03:42', '2011-11-03 15:03:42');
insert into menus (id, name, listorder, entered, modified) values ('6', 'Menú Peu pàgina', '2', '2013-12-11 12:49:58', '2013-12-11 12:49:58');
drop table if exists menus_pages;
create table menus_pages (
  id int(11) not null auto_increment,
  menus_id int(11) not null ,
  parent_id int(11) not null ,
  action varchar(32) not null ,
  listorder int(11) not null ,
  active int(1) not null ,
  entered datetime not null ,
  modified datetime not null ,
  UNIQUE id (id)
);

insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('110', '4', '0', 'page', '0', '1', '2013-12-16 15:13:14', '2014-08-18 13:04:34');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('113', '6', '0', 'page', '3', '1', '2013-12-16 15:23:09', '2014-08-19 13:58:24');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('114', '6', '0', 'page', '1', '1', '2013-12-16 15:23:16', '2014-08-19 13:58:04');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('115', '6', '0', 'page', '4', '1', '2013-12-16 15:23:24', '2014-08-19 14:00:05');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('190', '4', '0', 'page', '1', '1', '2014-06-18 13:50:54', '2014-08-18 10:32:52');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('191', '4', '0', 'page', '2', '1', '2014-06-18 13:51:39', '2014-08-18 10:34:34');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('192', '4', '0', 'page', '3', '1', '2014-06-18 13:52:17', '2014-08-18 13:02:20');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('193', '4', '0', 'page', '4', '1', '2014-06-18 13:53:23', '2014-08-19 10:19:45');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('194', '4', '0', 'page', '6', '1', '2014-06-18 13:53:55', '2014-08-19 10:20:15');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('195', '4', '0', 'page', '7', '1', '2014-06-18 13:54:19', '2014-08-19 10:20:26');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('196', '4', '191', 'page', '0', '1', '2014-06-18 13:55:27', '2014-08-19 13:51:01');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('197', '4', '191', 'page', '1', '1', '2014-06-18 13:55:43', '2014-08-18 10:35:40');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('198', '4', '191', 'page', '2', '1', '2014-06-18 13:56:16', '2014-08-18 10:36:14');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('199', '4', '191', 'page', '3', '0', '2014-06-18 13:56:44', '2014-06-18 13:56:44');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('200', '4', '0', 'page', '5', '1', '2014-06-18 16:15:49', '2014-08-18 10:39:21');
insert into menus_pages (id, menus_id, parent_id, action, listorder, active, entered, modified) values ('201', '6', '0', 'page', '0', '1', '2014-06-20 13:00:51', '2014-08-19 13:57:49');
drop table if exists menus_pages_description;
create table menus_pages_description (
  menus_pages_id int(11) not null ,
  language_id int(11) not null ,
  page_id int(11) not null ,
  link varchar(128) not null ,
  target varchar(32) not null ,
  class varchar(128) not null ,
  rel varchar(255) not null ,
  rev varchar(255) not null ,
  title varchar(128) not null ,
  alt_title varchar(256) not null ,
  PRIMARY KEY (menus_pages_id, language_id)
);

insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('110', '1', '1', '', '_self', '', '', '', 'Inici', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('110', '2', '1', '', '_self', '', '', '', 'Inicio', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('110', '3', '1', '', '_self', '', '', '', 'Home', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('110', '4', '0', '', '_self', '', '', '', '', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('113', '1', '4', '', '_self', '', '', '', 'Avís legal', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('113', '2', '4', '', '_self', '', '', '', 'Aviso legal', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('113', '3', '4', '', '_self', '', '', '', 'Legal notice', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('113', '4', '0', '', '_self', '', '', '', '', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('114', '1', '5', '', '_self', '', '', '', 'Mapa del web', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('114', '2', '5', '', '_self', '', '', '', 'Mapa del web', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('114', '3', '5', '', '_self', '', '', '', 'Sitemap', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('114', '4', '0', '', '_self', '', '', '', '', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('115', '1', '6', '', '_self', '', '', '', 'Ús de cookies', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('115', '2', '6', '', '_self', '', '', '', 'Uso de cookies', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('115', '3', '6', '', '_self', '', '', '', 'Use of cookies', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('115', '4', '0', '', '_self', '', '', '', '', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('190', '1', '124', '', '_self', '', '', '', 'Empresa', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('190', '2', '124', '', '_self', '', '', '', 'Empresa', 'empresa');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('190', '3', '124', '', '_self', '', '', '', 'Company', 'company');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('191', '1', '0', '', '_self', '', '', '', 'Serveis', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('191', '2', '0', '', '_self', '', '', '', 'Servicios', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('191', '3', '0', '', '_self', '', '', '', 'Services', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('192', '1', '129', '', '_self', '', '', '', 'Clients', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('192', '2', '129', '', '_self', '', '', '', 'Clientes', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('192', '3', '129', '', '_self', '', '', '', 'Customers', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('193', '1', '2', '', '_self', '', '', '', 'Actualitat', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('193', '2', '2', '', '_self', '', '', '', 'Actualidad', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('193', '3', '2', '', '_self', '', '', '', 'News', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('194', '1', '132', '', '_self', '', '', '', 'Treball', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('194', '2', '132', '', '_self', '', '', '', 'Trabajo', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('194', '3', '132', '', '_self', '', '', '', 'Work', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('195', '1', '3', '', '_self', '', '', '', 'Contactar', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('195', '2', '3', '', '_self', '', '', '', 'Contactar', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('195', '3', '3', '', '_self', '', '', '', 'Contact', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('196', '1', '126', '', '_self', '', '', '', 'Software', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('196', '2', '126', '', '_self', '', '', '', 'Software', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('196', '3', '126', '', '_self', '', '', '', 'Software', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('197', '1', '127', '', '_self', '', '', '', 'Gestió Ticketing', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('197', '2', '127', '', '_self', '', '', '', 'Gestión Ticketing', 'Gestión Ticketing');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('197', '3', '127', '', '_self', '', '', '', 'Ticketing management', 'Ticketing management');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('198', '1', '128', '', '_self', '', '', '', 'Per què Koobin', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('198', '2', '128', '', '_self', '', '', '', 'Por qué Koobin', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('198', '3', '128', '', '_self', '', '', '', 'Why Koobin', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('199', '1', '133', '', '_self', '', '', '', 'Accés a DEMO', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('199', '2', '0', '', '_self', '', '', '', '', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('199', '3', '0', '', '_self', '', '', '', '', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('200', '1', '131', '', '_self', '', '', '', 'Premsa', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('200', '2', '131', '', '_self', '', '', '', 'Prensa', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('200', '3', '131', '', '_self', '', '', '', 'Press', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('201', '1', '3', '', '_self', '', '', '', 'Contactar', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('201', '2', '3', '', '_self', '', '', '', 'Contactar', '');
insert into menus_pages_description (menus_pages_id, language_id, page_id, link, target, class, rel, rev, title, alt_title) values ('201', '3', '3', '', '_self', '', '', '', 'Contact', '');
drop table if exists noticies;
create table noticies (
  id int(8) not null auto_increment,
  active int(1) default '0' not null ,
  portada int(1) not null ,
  published datetime not null ,
  finished datetime not null ,
  entered datetime default '0000-00-00 00:00:00' not null ,
  modified datetime default '0000-00-00 00:00:00' not null ,
  fotos_groups_id int(11) not null ,
  facebook_send tinyint(1) not null ,
  PRIMARY KEY (id)
);

insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('3', '1', '0', '2014-03-17 00:00:00', '0000-00-00 00:00:00', '2013-12-16 15:28:06', '2014-08-19 11:57:00', '87', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('4', '1', '0', '2014-01-09 00:00:00', '0000-00-00 00:00:00', '2013-12-16 15:28:37', '2014-08-19 11:57:47', '86', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('5', '1', '0', '2013-12-30 00:00:00', '0000-00-00 00:00:00', '2014-06-27 15:25:06', '2014-08-19 11:59:11', '85', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('6', '1', '1', '2014-06-30 00:00:00', '0000-00-00 00:00:00', '2014-06-30 19:08:48', '2014-08-19 11:54:32', '92', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('7', '1', '1', '2014-06-30 00:00:00', '0000-00-00 00:00:00', '2014-06-30 19:12:12', '2014-08-19 11:52:34', '93', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('8', '1', '1', '2014-07-01 00:00:00', '0000-00-00 00:00:00', '2014-07-01 11:13:47', '2014-08-19 11:50:56', '94', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('9', '1', '0', '2013-09-18 00:00:00', '0000-00-00 00:00:00', '2014-08-11 08:44:48', '2014-08-12 07:38:42', '105', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('10', '1', '0', '2014-08-11 00:00:00', '0000-00-00 00:00:00', '2014-08-11 08:49:07', '2014-08-19 14:31:29', '71', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('11', '1', '0', '2014-08-11 00:00:00', '0000-00-00 00:00:00', '2014-08-11 10:40:13', '2014-08-11 10:40:54', '102', '0');
insert into noticies (id, active, portada, published, finished, entered, modified, fotos_groups_id, facebook_send) values ('12', '1', '0', '2014-08-12 00:00:00', '0000-00-00 00:00:00', '2014-08-12 07:09:05', '2014-08-18 13:14:55', '103', '0');
drop table if exists noticies_description;
create table noticies_description (
  noticies_id int(8) default '0' not null ,
  language_id int(2) default '0' not null ,
  titular varchar(255) not null ,
  destacat text not null ,
  text text not null ,
  tags varchar(255) not null ,
  viewed int(11) not null ,
  PRIMARY KEY (noticies_id, language_id)
);

insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('3', '1', 'Entrades per la Processó de Verges a la venda!', '', '<p>Ja pots comprar les teves entrades per a veure la Process&oacute; de Verges accedint a <a href=\"http://www.koobin.com/laprocessodeverges\" style=\"outline: medium none;\" target=\"_blank\" title=\"Entrades per la Processó de Verges\">www.koobin.com/laprocessodeverges</a></p>

<p>No et perdis aquesta impressionant representaci&oacute; teatral!</p>

<p>Tens tota la informaci&oacute; a&nbsp;<a href=\"http://www.laprocesso.cat\" target=\"_blank\">www.laprocesso.cat</a></p>', 'lorem,ipsum', '105');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('3', '2', '¡Entradas para la Procesión de Verges a la venta!', '', '<p>Ya puedes comprar tus entradas para ver la Procesi&oacute;n de Verges accediendo a www.koobin.com/laprocessodeverges</p>

<p>&nbsp;</p>

<p>No te pierdas esta impresionante representaci&oacute;n teatral!</p>

<p>&nbsp;</p>

<p>Tienes toda la informaci&oacute;n en www.laprocesso.cat</p>', '', '48');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('3', '3', 'Tickets for the Procession of the Verges sale!', '', '<p>You can buy your tickets to see the procession Verges accessing www.koobin.com/laprocessodeverges</p>

<p>&nbsp;</p>

<p>Do not miss this amazing theater performance!</p>

<p>&nbsp;</p>

<p>Have all the information www.laprocesso.cat</p>', '', '1');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('3', '4', '', '', '', '', '0');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('4', '1', 'Ja a la venda les entrades pel “Llac dels Cignes” a Sabadell!', '', '<p>Per tal de comprar la teva entrada pel Llac dels Cignes fes click <a href=\"https://www.koobin.com/sabadell/index.php\" target=\"_blank\">aqu&iacute;</a></p>', 'lorem', '182');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('4', '2', '¡Ya a la venta las entradas para el \"Lago de los Cisnes\" en Sabadell!', '', '<p>Para comprar tu entrada por el Lago de los Cisnes haz click aqu&iacute;</p>', '', '4');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('4', '3', 'Now on sale tickets for \"Swan Lake\" in Sabadell!', '', '<p>To buy your tickets for Swan Lake click here</p>', '', '1');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('4', '4', '', '', '', '', '0');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('5', '1', 'Aquestes Festes regala Starlite!', '', '<p>El Glamur&oacute;s Starlite Festival de Marbella inclou les actuacions d&rsquo;artistes guardonats: Chucho Vald&eacute;s, Marta S&aacute;nchez i Gloria Gaynor, Sergio Dalma, Dani Martin, Rosario o Julio Iglesias, entre altres.</p>

<p>Fins el 5 de gener pots comprar la <strong>Targeta Starlite</strong>&nbsp;per nom&eacute;s 200 euros i amb 250 euros de saldo + 2 samarretes + 2 CD&rsquo;s. El regal perfecte per la persona que m&eacute;s estimis!</p>

<p>Pots adquirir la teva <strong>Targeta Starlite</strong> <a href=\"https://www.koobin.com/starlitefestival/index.php?action=PU_extras&amp;_ga=1.166709310.1848294078.1381747962\" target=\"_blank\">aqu&iacute;</a></p>', '', '31');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('5', '2', '¡Estas Fiestas regala Starlite!', '', '<p>El Glamoroso Starlite Festival de Marbella incluye las actuaciones de artistas galardonados: Chucho Vald&eacute;s, Marta S&aacute;nchez y Gloria Gaynor, Sergio Dalma, Dani Martin, Rosario o Julio Iglesias, entre otros.</p>

<p>&nbsp;</p>

<p>Hasta el 5 de enero puedes comprar la Tarjeta Starlite por s&oacute;lo 200 euros y con 250 euros de saldo + 2 camisetas + 2 CDs. El regalo perfecto para la persona que m&aacute;s quieras!</p>

<p>&nbsp;</p>

<p>Puedes adquirir tu Tarjeta Starlite aqu&iacute;</p>', '', '1');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('5', '3', 'Starlite gift this holiday!', '', '<p>&nbsp;</p>

<p>The glamorous Starlite Marbella Festival includes performances by award-winning artists: Chucho Valdes, Marta Sanchez and Gloria Gaynor, Sergio Dalma, Dani Martin, Rosario and Julio Iglesias, among others.</p>

<p>&nbsp;</p>

<p>Until January 5th you can buy Card Starlite for only 200 euros and 250 euros Balance + 2 CDs + 2 shirts. The perfect gift for the person you love the most!</p>

<p>&nbsp;</p>

<p>You can purchase your card here Starlite</p>', '', '1');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('6', '1', 'Gran Teatre del Liceu', '', '<p>A trav&eacute;s de la <a href=\"http://www.liceubarcelona.cat/\" target=\"_blank\">p&agrave;gina web del Liceu</a> podeu comprar les vostres entrades i abonaments amb el sistema de gesti&oacute; i venda d&rsquo;entrades KoobinEvent. El model de visualitzaci&oacute; de l&rsquo;aforament &eacute;s de manera sencera, pel que podeu seleccionar les localitats de qualsevol zona des d&rsquo;una &uacute;nica p&agrave;gina.</p>

<p>Fent clic a sobre de cada localitat us apareixer&agrave; una fotografia de la visi&oacute; de l&rsquo;escenari des de la mateixa, funci&oacute; molt &uacute;til per a que pugueu con&egrave;ixer com veureu l&rsquo;espectacle abans de comprar-ne les entrades. Tanmateix, cada localitat t&eacute; assignat un percentatge de visibilitat i el sistema us avisar&agrave; quan el seient que seleccioneu no en tingui.</p>

<p>Amb <strong>KoobinEvent</strong> la selecci&oacute; del dia de funci&oacute; &eacute;s molt senzilla, ja que per a espectacles amb m&eacute;s de 20 funcions s&rsquo;activa autom&agrave;ticament un calendari de f&agrave;cil &uacute;s. Un cop comprades les vostres entrades podreu imprimir-vos-les des de casa en PDF o b&eacute; enviar-vos-les al m&ograve;bil en format Passbook o imatge.</p>', '', '145');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('6', '2', 'Gran Teatro del Liceo', '', '<p>A trav&eacute;s de la p&aacute;gina web del Liceo puede comprar sus entradas y abonos con el sistema de gesti&oacute;n y venta de entradas KoobinEvent. El modelo de visualizaci&oacute;n del aforo es de forma entera, por lo que puede seleccionar las localidades de cualquier zona desde una &uacute;nica p&aacute;gina.</p>

<p>&nbsp;</p>

<p>Haciendo clic sobre cada localidad aparecer&aacute; una fotograf&iacute;a de la visi&oacute;n del escenario desde la misma, funci&oacute;n muy &uacute;til para que pueda conocer como ver&aacute; el espect&aacute;culo antes de comprar las entradas. Sin embargo, cada localidad tiene asignado un porcentaje de visibilidad y el sistema le avisar&aacute; cuando el asiento que seleccione no tenga.&nbsp;</p>

<p>Con KoobinEvent la selecci&oacute;n del d&iacute;a de funci&oacute;n es muy sencilla, ya que para espect&aacute;culos con m&aacute;s de 20 funciones se activa autom&aacute;ticamente un calendario de f&aacute;cil uso. Una vez compradas sus entradas podr&aacute; imprimir a nuestras las desde casa en PDF o bien enviarle la foto en el m&oacute;vil en formato Passbook o imagen.</p>', '', '2');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('6', '3', 'Liceu Theatre', '', '<p>Through the website you can buy your tickets Lyceum and subscriptions management system and the ticketing KoobinEvent. The model display space is so full, so you can select any area locations from a single page.</p>

<p>&nbsp;</p>

<p>Clicking on each location you will see a picture of the view of the stage from the very, very useful function for you to learn how to see the show before buying their tickets. However, each location is assigned a percentage of visibility and the system will alert you when the seat you choose not to have.</p>

<p>&nbsp;</p>

<p>With the choice of the day KoobinEvent function is very simple, as for shows with more than 20 functions automatically activates a user-friendly calendar. Once you have purchased your tickets you can print them at home or send PDF back into your phone or Passbook format image.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>', '', '3');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('7', '1', 'Cantada d’Havaneres a Calella', '', '<p>El proper dia 5 de juliol t&eacute; lloc la 48a Cantada d&rsquo;Havaneres de Calella de Palafrugell. &nbsp;Encara sou a temps d&rsquo;adquirir les vostres entrades amb el sistema KoobinEvent, accedint a la p&agrave;gina oficial de la cantada: www.havanerescalella.cat</p>

<p>Aprofiteu aquesta ocasi&oacute; per a gaudir de l&rsquo;espl&egrave;ndida platja de Calella des de la Pla&ccedil;a del Port Bo, amb un ambient estiuenc i mariner. El preu de l&rsquo;entrada anticipada &eacute;s de 27,50&euro; i, el dia 5 de juliol a taquilla tindran un preu de 30 euros.</p>', '', '110');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('7', '2', 'Cantada de Habaneras en Calella', '', '<p>El pr&oacute;ximo d&iacute;a 5 de julio tiene lugar la 48 &ordf; Cantada de Habaneras de Calella de Palafrugell. A&uacute;n est&aacute;s a tiempo de adquirir sus entradas con el sistema KoobinEvent, accediendo a la p&aacute;gina oficial de la cantada: www.havanerescalella.cat</p>

<p>&nbsp;</p>

<p>Aprovecha esta ocasi&oacute;n para disfrutar de la espl&eacute;ndida playa de Calella desde la Plaza del Port Bo, con un ambiente veraniego y marinero. El precio de la entrada anticipada es 27,50 &euro; y, el d&iacute;a 5 de julio en taquilla tendr&aacute;n un precio de 30 euros.</p>', '', '2');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('7', '3', 'Habaneras in Calella', '', '<p>&nbsp;</p>

<p>&nbsp;</p>

<p>The 5th of July there is the 48th Habanera of Calella de Palafrugell. You still have time to purchase their tickets KoobinEvent system by accessing the official website of the sung: www.havanerescalella.cat</p>

<p>&nbsp;</p>

<p>Take this opportunity to enjoy the splendid beach of Calella from the Plaza del Port Bo, with a summery and nautical atmosphere. The advance ticket price is &euro; 27.50 and on July 5 at the door will cost 30 euros.</p>', '', '2');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('8', '1', 'Koobin organitza unes jornades sobre tecnologia al servei de la indústria cultural, esportiva i de l’oci', '', '<p><strong>Es presentaran les darreres novetats de l&rsquo;empresa, especialitzada en gesti&oacute; de ticketing on line.</strong></p>

<p><strong>Koobin</strong> ha preparat unes jornades sobre tecnologia al servei de la ind&uacute;stria cultural, esportiva i de l&#39;oci, que se celebraran el 2 de juliol a l&#39;auditori de l&#39;edifici Jaume Casademont del <a href=\"http://www.parcudg.com/\" target=\"_blank\">Parc&nbsp; Cient&iacute;fic i Tecnol&ograve;gic de la Universitat de Girona</a>.<br />
<br />
A les jornades es presentaran les darreres novetats i els casos d&#39;&egrave;xit de l&rsquo;empresa especialitzada en ticketing on line, i comptaran amb la participaci&oacute; de <strong>Marta Montalb&aacute;n</strong>, gerent de Bit&ograve; Produccions; <strong>Agust&iacute; Filomeno</strong>, director de m&agrave;rqueting del Gran Teatre del Liceu; i <strong>Pep Tugues</strong>, el director del Teatre-Auditori de Sant Cugat, que explicaran les seves experi&egrave;ncies amb l&rsquo;empresa gironina.</p>

<p>A les jornades tamb&eacute; es far&agrave; la presentaci&oacute; de les empreses col&middot;laboradores de Koobin, com <a href=\"http://movintracks.io/\" target=\"_blank\">Movintracks</a>, especialitzada en gesti&oacute; de campanyes en dispositius m&ograve;bils amb NFC i IBeacons; Mobile Media Contents, que desenvolupen aforaments en 3D navegables i interactius; <a href=\"http://www.mantis.cat\" target=\"_blank\">LaMantis</a> i <a href=\"http://www.glam.cat\" target=\"_blank\">Glam Comunicaci&oacute;</a>, que presentaran <a href=\"http://www.eventis.pro\" target=\"_blank\">Eventis</a>, una soluci&oacute; global per a la promoci&oacute; d&rsquo;esdeveniments i espectacles, i Proscenium, la revista d&rsquo;arts esc&egrave;niques per a tauletes. La introducci&oacute; de la jornada, que comen&ccedil;ar&agrave; a dos quarts de deu del mat&iacute;, anir&agrave; a c&agrave;rrec del CEO de Koobin, Carles Arnal.</p>

<p>Cal una inscripci&oacute; pr&egrave;via a <a href=\"mailto:info@koobin.com?subject=Jornades\">info@koobin.com</a> o trucant al <strong>972 183 393</strong>.</p>', '', '272');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('8', '2', 'Koobin organiza unas jornadas sobre tecnología al servicio de la industria cultural, deportiva y del ocio', '', '<p>Se presentar&aacute;n las &uacute;ltimas novedades de la empresa, especializada en gesti&oacute;n de ticketing online.</p>

<p>&nbsp;</p>

<p>Koobi ha preparado unas jornadas sobre tecnolog&iacute;a al servicio de la industria cultural, deportiva y del ocio, que se celebrar&aacute;n el 2 de julio en el auditorio del edificio Jaume Casademont del Parque Cient&iacute;fico y Tecnol&oacute;gico de la Universidad de Girona.</p>

<p>&nbsp;</p>

<p>En las jornadas se presentar&aacute;n las &uacute;ltimas novedades y los casos de &eacute;xito de la empresa especializada en ticketing online, y contar&aacute;n con la participaci&oacute;n de Marta Montalb&aacute;n, gerente de Bit&ograve; Producciones; Agust&iacute;n Filomeno, director de marketing del Gran Teatro del Liceo; y Pep Tugues, el director del Teatro-Auditorio de Sant Cugat, que explicar&aacute;n sus experiencias con la empresa gerundense.</p>

<p>&nbsp;</p>

<p>En las jornadas tambi&eacute;n se har&aacute; la presentaci&oacute;n de las empresas colaboradoras de Koobi, como Movintracks, especializada en gesti&oacute;n de campa&ntilde;as en dispositivos m&oacute;viles con NFC y IBeacons; Mobile Media Contents, que desarrollan aforos en 3D navegables e interactivos; Amante y Glam Comunicaci&oacute;n, que presentar&aacute;n eventos, una soluci&oacute;n global para la promoci&oacute;n de eventos y espect&aacute;culos, y Proscenium, la revista de artes esc&eacute;nicas para tabletas. La introducci&oacute;n de la jornada, que comenzar&aacute; a las nueve y media de la ma&ntilde;ana, correr&aacute; a cargo del CEO de Koobi, Carles Arnal.</p>

<p>&nbsp;</p>

<p>Hay una inscripci&oacute;n previa a info@koobin.com o llamando al 972183393.</p>

<p>&nbsp;</p>', '', '2');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('8', '3', 'Koobin organizes a conference on technology for the cultural industry, sport and leisure', '', '<p>The latest news from the company, which specializes in online ticketing management will be presented.</p>

<p>&nbsp;</p>

<p>Koobin has prepared a conference on technology for cultural, sports and leisure industry, to be held on July 2 at the auditorium of the building Jaume Casademont the Scientific and Technological Park of the University of Girona.</p>

<p>&nbsp;</p>

<p>In the conference the latest developments and success stories of online ticketing company specializing in will be presented and will include the participation of Marta Montalban, Bito manager Productions; Filomeno Augustine, director of marketing of Liceo Theatre; Pep Tugues and the director of the Teatro-Auditorio de Sant Cugat, who will explain their experiences with Girona company.</p>

<p>&nbsp;</p>

<p>There will also be presenting partner companies Koobin as Movintracks specializing in campaign management on mobile devices with NFC and IBeacons at the conference; Mobile Media Contents, developing gauging navigable and interactive 3D; Glam Lover and Communication, events that will present a comprehensive solution for the promotion of events and shows, and Proscenium, performing arts magazine for tablets. The introduction of the day, starting at half past nine, borne by the CEO of Koobin, Carles Arnal.</p>

<p>&nbsp;</p>

<p>There is a pre-registration or by calling 972 183 393 <a href=\"mailto:info@koobin.com\">info@koobin.com</a>.</p>', '', '3');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('9', '1', 'Entrades en format Passbook directament des del teu Print@Home!', '', '<p>El dia 18 de setembre Apple pos&agrave;&nbsp;a disposici&oacute; dels seus usuaris la nova versi&oacute; del sistema operatiu &nbsp;<strong>iOS 7</strong>, la qual inclou una gran novetat per tots aquells de vosaltres que utilitzeu l&#39;aplicaci&oacute; Passbook. A partir d&#39;ara podreu descarregar les vostres entrades en format Passbook&nbsp;<strong><em><u>directament</u></em></strong>&nbsp;des del paper: nom&eacute;s amb escanejar el codi QR impr&egrave;s al teu &nbsp;Print@Home rebr&agrave;s autom&agrave;ticament la teva entrada en format Passbook.</p>

<p>El funcionament &eacute;s molt senzill: obre la teva aplicaci&oacute; de Passbook i accedeix a l&#39;opci&oacute; &quot;escanejar codi&quot;. Ara nom&eacute;s cal que llegeixis el codi QR impr&egrave;s a la teva entrada en Print@Home i...&nbsp;<strong>voil&agrave;!</strong>&nbsp;la teva entrada llesta per a veure la teva obra preferida.</p>

<p><em>Apple, iPhone i iPod touch s&oacute;n marques registrades d&#39;Apple, Inc.</em></p>', '', '10');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('9', '2', '¡Entradas en formato Passbook directamente desde tu Print@Home!', '', '<p>El d&iacute;a 18 de septiembre Apple puso a disposici&oacute;n de sus usuarios la nueva versi&oacute;n del sistema operativo&nbsp;<strong>iOS 7</strong>, la cual incluye una gran novedad para aquellos de vosotros que utiliz&aacute;is la aplicaci&oacute;n Passbook. A partir de ahora ya podr&eacute;is descargar vuestras entradas en formato Passbook&nbsp;<strong><em><u>directamente</u></em></strong>&nbsp;desde el papel: s&oacute;lo con escanear el c&oacute;digo QR impreso en tu Print@Home recibir&aacute;s autom&aacute;ticamente tu entrada en formato Passbook</p>

<p>El funcionamiento es muy sencillo: abre tu aplicaci&oacute;n de Passbook y accede a la opci&oacute;n &quot;escanear c&oacute;digo&quot;. Ahora s&oacute;lo lee el c&oacute;digo QR impreso en tu entrada en Print@Home y...&nbsp;<strong>&iexcl;voil&agrave;!</strong>&nbsp;tu entrada lista para entrar a ver tu obra favorita.</p>

<p><em>Apple, iPhone y iPod touch son marcas registradas de Apple, Inc.</em></p>', '', '4');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('9', '3', 'Entries in Passbook format directly from your Print @ Home!', '', '<p>The September 18, Apple made available to its users the new version of iOS 7 operating system, which includes a big news for those of you that you use the Passbook application. From now you can download your tickets in Passbook format directly from the paper: only scan the QR code printed on your Print @ Home automatically receive your input format Passbook&nbsp;</p>

<p>The operation is very simple: open your Passbook application and access the &quot;scan code&quot; option. Now only reads the QR code printed on your ticket at Print @ Home and ... voil&agrave;! Check your list to get in to see your favorite work.&nbsp;</p>

<p>Apple, iPhone and iPod touch are trademarks of Apple, Inc.</p>', '', '6');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('10', '1', 'Ja a la venda els abonaments per el públic general de la Copa del Rey 2014!', '', '<p><span style=\"line-height: 1.6;\">Una vegada finalitzat el per&iacute;ode exclusiu per a socis i abonats dels Clubs ACB, ha comen&ccedil;at ja la venda d&#39;abonaments per el p&uacute;blic general. Sense termini l&iacute;mit i fins a finalitzar les exist&egrave;ncies!</span></p>

<p>Tots els abonaments permeten gaudir de manera &iacute;ntegra de l&#39;espectacle de la &ldquo;Copa del Rey&rdquo; de M&agrave;laga, incloent les set trobades (quarts de final, semifinals i final).&nbsp;Com en edicions precedents, tots els abonaments adquirits fins el 31 de desembre tindran descompte per compra anticipada.</p>

<p>Compra ja els teus abonaments&nbsp;<a href=\"http://tickets.acb.com/\">aqu&iacute;</a>!</p>', '', '17');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('10', '2', '¡Ya a la venta los abonos para el público general de la Copa del Rey 2014!', '', '<p>Una vez finalizado el periodo exclusivo para socios y abonados de los clubes ACB, ha comenzado ya la venta de abonos para el p&uacute;blico en general. &iexcl;Sin plazo l&iacute;mite y hasta final de existencias!.</p>

<p>Todos los abonos permiten disfrutar de forma &iacute;ntegra del espect&aacute;culo de la Copa del Rey de M&aacute;laga, incluyendo los siete encuentros (cuartos de final, semifinales y final).&nbsp;Como en ediciones precedentes, todos los abonos adquiridos hasta el 31 de diciembre tendr&aacute;n descuento por compra anticipada.</p>

<p>&iexcl;Compra ya tus abonos&nbsp;<a href=\"http://tickets.acb.com/\">aqu&iacute;</a>!</p>', '', '6');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('10', '3', 'Now on sale the season tickets for the general public of Copa del Rey 2014!', '', '<p>After the exclusive period for members and subscribers ACB clubs, now the sale has started for the general public. No time limit and while stocks last!.</p>

<p>All season tickets allow you to enjoy the show in full of the &quot;Copa del Rey&quot; in Malaga, including seven games (quarterfinals, semifinals and final). As in previous editions, all season tickets purchased until December 31 will have an advance purchase discount.</p>

<p>Buy your tickets&nbsp;<a href=\"http://tickets.acb.com/\">here</a>!</p>', '', '7');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('11', '1', 'Aquest estiu torna Starlite', '', '<p>Les nits de Marbella aquest estiu tornaran a ser m&agrave;giques amb l&#39;arribada, una vegada m&eacute;s, del Festival Starlite.</p>

<p>Amb una excel&middot;lent programaci&oacute; di&agrave;ria, Starlite &eacute;s el festival de les estrelles.</p>

<p>Concerts &iacute;ntims i pr&ograve;xims&nbsp;d&#39;artistes, els concerts dels quals solen ser massius, en un auditori privilegiat.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>', '', '17');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('11', '2', 'Este verano vuelve Starlite', '', '<p>Las noches de Marbella este verano volver&aacute;n a ser m&aacute;gicas&nbsp;con la llegada, una vez m&aacute;s, del Festival Starlite.</p>

<p>Con una excelente programaci&oacute;n diaria Starlite es el festival de las estrellas.</p>

<p><span style=\"color: rgb(85, 85, 85); font-family: verdana, geneva; font-size: 13px; line-height: 24px;\">Conciertos &iacute;ntimos y cercanos, de artistas cuyos conciertos suelen ser masivos, en un auditorio privilegiado.</span></p>', '', '5');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('11', '3', 'Starlite returns this summer', '', '<p>Marbella nights this summer was magical again with the arrival once again, Starlite Festival.</p>

<p>With an excellent daily schedule, Starlite is the festival of stars.</p>

<p>Intimate and close concert artists whose concerts are usually massive, in a privileged audience.</p>

<p>&nbsp;</p>', '', '5');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('12', '1', 'KOOBIN A TV GIRONA', '', '<p>El passat dia 16 de Juliol TV Girona va fer-nos una visita per con&egrave;ixer de primera m&agrave; qui s&oacute;m i qu&egrave; fem.</p>

<p>Per veure el v&iacute;deo fes click <a href=\"http://tvgirona.xiptv.cat/te-de-tot/capitol/te-de-tot-en-ruta-del-16-07-2014-_-part-1\" target=\"_blank\">aqu&iacute;.</a></p>', '', '20');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('12', '2', 'KOOBIN EN TV GIRONA', '', '<p>El pasado d&iacute;a 16 de Julio TV Girona nos visit&oacute; para conocer de primera mano qui&eacute;n somos y qu&eacute; hacemos.</p>

<p>Para ver el video haz clic <a href=\"http://tvgirona.xiptv.cat/te-de-tot/capitol/te-de-tot-en-ruta-del-16-07-2014-_-part-1\" target=\"_blank\">aqu&iacute;.</a></p>', '', '8');
insert into noticies_description (noticies_id, language_id, titular, destacat, text, tags, viewed) values ('12', '3', 'KOOBIN ON TV GIRONA', '', '<p>On July 16 Girona TV visited us to see first hand who we are and what we do.</p>

<p>To watch the video click <a href=\"http://tvgirona.xiptv.cat/te-de-tot/capitol/te-de-tot-en-ruta-del-16-07-2014-_-part-1\" target=\"_blank\">here.</a></p>', '', '7');
drop table if exists pages;
create table pages (
  id int(11) not null auto_increment,
  parent_id int(11) default '0' not null ,
  action varchar(32) not null ,
  active int(2) not null ,
  listorder int(11) not null ,
  entered datetime not null ,
  modified datetime not null ,
  fotos_groups_id int(4) not null ,
  banners_id int(4) not null ,
  PRIMARY KEY (id)
);

insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('1', '0', 'modul', '1', '1', '2013-12-16 15:12:28', '2014-06-19 09:18:15', '0', '33');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('2', '0', 'modul', '1', '5', '2013-12-16 15:12:49', '2014-08-19 11:51:04', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('3', '0', 'modul', '1', '100', '2013-12-16 15:13:03', '2014-08-19 14:38:36', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('4', '0', 'content', '1', '101', '2013-12-16 15:15:41', '2014-08-07 10:16:30', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('5', '0', 'modul', '1', '103', '2013-12-16 15:17:19', '2014-08-19 14:54:17', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('6', '0', 'content', '1', '104', '2013-12-16 15:18:22', '2014-08-19 14:40:50', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('124', '0', 'content', '1', '2', '2014-06-18 13:29:10', '2014-08-19 11:56:55', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('125', '0', 'content', '1', '3', '2014-06-18 13:35:01', '2014-08-11 07:49:31', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('126', '125', 'content', '1', '0', '2014-06-18 13:36:42', '2014-08-19 14:11:41', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('127', '125', 'content', '1', '1', '2014-06-18 13:37:18', '2014-08-19 14:12:38', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('128', '125', 'content', '1', '2', '2014-06-18 13:38:08', '2014-08-19 11:16:19', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('129', '0', 'modul', '1', '4', '2014-06-18 13:39:12', '2014-08-19 11:43:01', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('131', '0', 'modul', '1', '6', '2014-06-18 13:42:19', '2014-08-19 11:44:11', '0', '0');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('132', '0', 'modul', '1', '7', '2014-06-18 13:43:14', '2014-08-19 11:48:14', '0', '89');
insert into pages (id, parent_id, action, active, listorder, entered, modified, fotos_groups_id, banners_id) values ('133', '125', 'content', '0', '3', '2014-06-18 13:45:10', '2014-08-11 08:14:11', '0', '0');
drop table if exists pages_description;
create table pages_description (
  pages_id int(11) not null ,
  language_id int(11) not null ,
  titol varchar(128) not null ,
  link varchar(128) not null ,
  target varchar(32) not null ,
  modul varchar(64) not null ,
  vars varchar(128) not null ,
  content text not null ,
  title varchar(255) not null ,
  page_title varchar(128) not null ,
  keywords text not null ,
  description text not null ,
  PRIMARY KEY (pages_id, language_id)
);

insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('1', '1', 'Inici', '', '_blank', 'inici', '', '', 'Inici', 'inici', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('1', '2', 'inicio', '', '_blank', 'inici', '', '', 'inicio', 'inicio', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('1', '3', 'inici en', '', '_blank', 'inici', '', '', 'inici en', 'inici-en', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('1', '4', 'inici fr', '', '_blank', 'inici', '', '', 'inici en', 'inici-fr', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('2', '1', 'Actualitat', '', '_blank', 'noticies', '', '<p>Tota la informaci&oacute; d&rsquo;actualitat sobre Koobin i els seus serveis</p>', 'Notícies', 'noticies-ca', '', 'Tota la informació d’actualitat sobre Koobin i els seus serveis');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('2', '2', 'Actualidad', '', '_blank', 'noticies', '', '<p>Toda la informaci&oacute;n de actualidad sobre Koobin y sus servicios</p>', 'Noticias', 'noticias-es', '', 'Toda la información de actualidad sobre Koobin y sus servicios');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('2', '3', 'News', '', '_blank', 'noticies', '', '<p>All current information on Koobin and services</p>', 'News', 'news', '', 'All the latest information about their services and Koobin');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('2', '4', 'Notícies fr', '', '_blank', 'noticies', '', '', 'Notícies fr', 'noticies-fr', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('3', '1', 'Contactar', '', '_blank', 'contacte', '', '<p>Parc Cient&iacute;fic i Tecnol&ograve;gic Universitat de Girona.</p>

<p>Edificio Centre d&rsquo;Empreses. C/ Pic de Peguera, 11</p>

<p>ES17003 &middot; Girona</p>

<p>Tel +34 972 183 393 &middot; Fax +34 972 183 395 <a href=\"mailto:info@koobin.com\" target=\"_blank\">info@koobin.com</a></p>

<p>Soporte: +34 972 183 207 &middot; <a href=\"mailto:support@koobin.com\" target=\"_blank\">support@koobin.com</a></p>', 'Contactar', 'contactar', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('3', '2', 'Contactar', '', '_blank', 'contacte', '', '<p>Parque&nbsp;Cient&iacute;fico y&nbsp;Tecnol&oacute;gico Universidad de Girona.</p>

<p>Edificio Centre d&rsquo;Empreses. C/ Pic de Peguera, 11</p>

<p>ES17003 &middot; Girona</p>

<p>Tel +34 972 183 393 &middot; Fax +34 972 183 395&nbsp;<a href=\"mailto:info@koobin.com\" target=\"_blank\">info@koobin.com</a></p>

<p>Soporte: +34 972 183 207 &middot;&nbsp;<a href=\"mailto:support@koobin.com\" target=\"_blank\">support@koobin.com</a></p>', 'Contactar ES', 'contactar', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('3', '3', 'Contact', '', '_blank', 'contacte', '', '<p>Science and Technology Park University of Girona.&nbsp;</p>

<p>Building Centre d&#39;Empreses. C / Pic de Peguera 11&nbsp;</p>

<p>ES17003 &middot; Girona&nbsp;</p>

<p>Tel +34 972 183 393 &middot; Fax +34 972 183 395 <a href=\"mailto:info@koobin.com\">info@koobin.com&nbsp;</a></p>

<p>Support: +34 972 183 207 &middot; <a href=\"mailto:support@koobin.com\">support@koobin.com</a></p>', 'Contactar', 'contact', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('3', '4', 'Contactar FR', '', '_blank', 'contacte', '', '', 'Contactar FR', 'contactar-fr', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('4', '1', 'Avís legal', '', '_blank', '', '', '<p>Per donar compliment al disposat a la normativa aplicable en mat&egrave;ria de serveis de societat de la informaci&oacute; i de comer&ccedil; electr&ograve;nic, a continuaci&oacute; s&rsquo;indiquen les dades d&rsquo;informaci&oacute; general d&rsquo;aquest lloc web:<br />
<br />
Titular: KoobinEvent S.L.<br />
Domicili Social: Pic de Peguera 11, Parc Cientific i Tecnol&ograve;gic UdG. Ed. Centre d&rsquo;Empreses. 17003 Girona.<br />
Tel&egrave;fon: (+34) 972 183 393. Fax: (+34) 972 183 395<br />
Correu electr&ograve;nic: info@koobin.com.<br />
Dades Registrals: Registre Mercantil de Girona, Volum 2782, Foli 172, Fulla GI 50281, Inscripci&oacute; 1<br />
CIF: ES B55075022</p>

<ol>
	<li><strong>Condicions d&rsquo;&uacute;s</strong><br />
	Les presents condicions regulen l&rsquo;&uacute;s del lloc web on es facilita al p&uacute;blic informaci&oacute; relativa als serveis de KoobinEvent S.L.. Li demanem que llegeixi detingudament la informaci&oacute; que li facilitem. El fet d&rsquo;accedir a aquest lloc web i utilitzar els materials continguts en ell implica que vost&egrave; ha llegit i accepta, sense cap reserva, aquestes condicions. KoobinEvent S.L. es reserva el dret de denegar, suspendre, interrompre o cancel&middot;lar l&rsquo;acc&eacute;s o la utilitzaci&oacute;, total o parcialment, d&rsquo;aquest lloc web a aquells usuaris o visitants que incompleixin qualsevol de les condicions previstes en aquest Av&iacute;s Legal.<br />
	Aquest lloc web cont&eacute; materials, com articles, estudis, descripci&oacute; de projectes, preparats per KoobinEvent S.L. amb finalitats &uacute;nicament informatives. KoobinEvent S.L. l&rsquo;informa que aquests materials poden ser modificats, desenvolupats o actualitzats en qualsevol moment sense notificaci&oacute; pr&egrave;via.<br />
	&nbsp;</li>
	<li><strong>Drets de Propietat Intel&middot;lectual i Industrial</strong><br />
	&copy; KoobinEvent S.L. Tots els drets reservats.<br />
	<br />
	La totalitat del contingut d&rsquo;aquesta web, entenent per contingut a t&iacute;tol merament enunciatiu, textos, imatges, fitxers, fotografies, logotips, gr&agrave;fics, marques, icones, combinacions de colors, o qualsevol altre element, la seva estructura i disseny, la selecci&oacute; i forma de presentaci&oacute; dels materials inclosos a la mateixa, i el software, links i d&rsquo;altres continguts audiovisuals o sonors, aix&iacute; com el seu disseny gr&agrave;fic i codis font necessaris pel seu funcionament, acc&eacute;s i utilitzaci&oacute;, estan protegits per drets de propietat industrial i intel&middot;lectual, titularitat de KoobinEvent S.L. o de tercers, sense que puguin entendre&rsquo;s cedits els drets d&rsquo;explotaci&oacute; sobre els mateixos m&eacute;s enll&agrave; de l&rsquo;estrictament necessari pel correcte &uacute;s del lloc web.<br />
	En particular, queden prohibides la reproducci&oacute;, la transformaci&oacute;, distribuci&oacute;, comunicaci&oacute; p&uacute;blica, posada a disposici&oacute; del p&uacute;blic i en general qualsevol altra forma d&rsquo;explotaci&oacute;, per qualsevol procediment, de tot o part dels continguts d&rsquo;aquest lloc web, aix&iacute; com del seu disseny i la selecci&oacute; i forma de presentaci&oacute; dels materials inclosos a la mateixa. Aquests actes d&rsquo;explotaci&oacute; nom&eacute;s podran ser realitzats amb l&rsquo;autoritzaci&oacute; expressa de KoobinEvent S.L. i sempre que es faci refer&egrave;ncia a la titularitat de KoobinEvent S.L. dels indicats drets de propietat intel&middot;lectual i industrial.<br />
	Queda aix&iacute; mateix prohibit descompilar, desassemblar, realitzar enginyeria inversa, subllicenciar o transmetre de qualsevol manera, traduir o realitzar obres derivades dels programes d&rsquo;ordinador necessaris pel funcionament, acc&eacute;s i utilitzaci&oacute; d&rsquo;aquest lloc web i dels serveis en ell continguts, aix&iacute; com realizar, respecte a tot o part de tals programes, qualsevol dels actes d&rsquo;explotaci&oacute; descrits al parr&agrave;graf anterior. L&rsquo;usuari del lloc web haur&agrave; d&rsquo;absternir-se en tot cas de suprimir, alterar, eludir o manipular qualsevol dispositiu o sistema de seguretat que puguin estar instal&middot;lats pel mateix.<br />
	Les marques, noms comercials o signes distintius s&oacute;n titularitat de KoobinEvent S.L. o de tercers, sense que pugui entendre&rsquo;s que l&rsquo;acc&eacute;s al lloc web atribueix cap dret sobre les citades marques, noms cmoercials i/o signes distintius.<br />
	&nbsp;</li>
	<li>&nbsp;<strong>Hipervincles</strong><br />
	Els hipervincles o links que cont&eacute; aquest lloc web poden conduir l&rsquo;usuari a altres llocs i p&agrave;gines web gestionats per tercers, sobre les que KoobinEvent S.L. no exerceix cap tipus de control. KoobinEvent S.L. no respon ni dels continguts ni de l&rsquo;estat de tals llocs i p&agrave;gines web, i l&rsquo;acc&eacute;s a les mateixes a trav&eacute;s d&rsquo;aquest lloc web tampoc implica que KoobinEvent S.L. recomani o aprovi els seus continguts.<br />
	&nbsp;</li>
	<li><strong>Modificacions</strong><br />
	Amb l&rsquo;objectiu de millorar el lloc web, KoobinEvent S.L. es reserva el dret, en qualsevol moment i sense pr&egrave;via notificaci&oacute;, a modificar, amplicar o suspendre temporalment la presentaci&oacute;, configuraci&oacute;, especificacions t&egrave;cniques i serveis del lloc web, de forma unilateral.<br />
	Asimismo se reserva el derecho de modificar en cualquier momento las presentes condiciones de uso as&iacute; como cualesquiera otras condiciones particulares.<br />
	&nbsp;</li>
	<li><strong>Exclusi&oacute; de responsabilitat</strong><br />
	Qui utilitzi aquest lloc web ho fa pel seu propi compte i risc. KoobinEvent S.L. , els seus socis, col&middot;laboradors, empleats i representants no es responsabilitzen dels errors o omissions derivats dels continguts d&rsquo;aquest lloc web o d&rsquo;altres continguts als que es pugui accedir a trav&eacute;s de la mateixa. KoobinEvent S.L. , els seus socis, col&middot;laboradors, empleats i representants tampoc podran ser considerats responsables per qualsevol dany derivat de la utilitzaci&oacute; d&rsquo;aquest web, ni per qualsevol actuaci&oacute; realitzada sobre la base de la informaci&oacute; que en ella es facilita.<br />
	La informaci&oacute; d&rsquo;aquest lloc web es proporciona sense garantia de cap classe, ni expl&iacute;cita ni impl&iacute;cita, i podr&agrave; canviar-se o actualitzar-se sense previ av&iacute;s.<br />
	KoobinEvent S.L. no garanteix l&rsquo;abs&egrave;ncia de virus, cucs o d&rsquo;altres elements inform&agrave;tics lesius que puguin causar danys o alteracions en el sistema inform&agrave;tic, als documents electr&ograve;nics o als fitxers de l&rsquo;usuari d&rsquo;aquest web. En conseq&uuml;&egrave;ncia, KoobinEvent S.L. no respon pels danys i perjudicis que tals elements puguin ocasionar a l&rsquo;usuari o a tercers. Aix&iacute; mateix no es responsabilitza ni garanteix la disponibilitat ni continu&iuml;tat en l&rsquo;acc&eacute;s a aquest lloc web o que el mateix estigui lliure d&rsquo;errors, corresponent a l&rsquo;usuari del lloc web, en qualsevol cas, l&rsquo;obligaci&oacute; de disposar d&rsquo;eines adequades per a la detecci&oacute; i desinfecci&oacute; de programes inform&agrave;tics nocius o lesius.<br />
	<br />
	L&rsquo;usuari respondr&agrave; dels danys i perjudicis de tota naturalesa que KoobinEvent S.L. pugui patir com a conseq&uuml;&egrave;ncia de l&rsquo;incompliment de qualsevold e les obligacions a les que queda sotm&egrave;s per les presents condicions. L&rsquo;usuari &eacute;s conscient i accepta volunt&agrave;riament que l&rsquo;&uacute;s de qualsevol contingut d&rsquo;aquest lloc web es d&oacute;na, en qualsevol cas, sota la seva &uacute;nica i exclusiva responsabilitat.<br />
	&nbsp;</li>
	<li><strong>Pol&iacute;tica de Privacitat</strong><br />
	KoobinEvent S.L. est&agrave; compromesa amb la protecci&oacute; i seguretat de les dades de car&agrave;cter personal que els usuaris o visitants d&rsquo;aquest lloc web ens puguin facilitar. Aquesta Pol&iacute;tica de Privacitat t&eacute; per objectiu informar als usuaris i visitants del nostre lloc web de tot el relatiu a la recollida i tractament de les seves dades personals per KoobinEvent S.L.<br />
	<br />
	KoobinEvent S.L. es reserva el dret a modificar i/o actualitzar aquesta pol&iacute;tica de forma peri&ograve;dica, amb la finalitat d&rsquo;adaptar-la a la legislaci&oacute; aplicable a cada moment, aix&iacute; com a les pr&agrave;ctiques leg&iacute;times de KoobinEvent S.L. En tot cas, el contingut de la nova pol&iacute;tica ser&agrave; publicada al lloc web i estar&agrave; disponible permanentment pels usuaris i visitants del mateix. Es recomana als usuaris i visitants que llegeixin i consultin aquesta Pol&iacute;tica de Privacitat cada vegada que accedeixin a aquest lloc web.<br />
	<br />
	<strong>Com recull KoobinEvent S.L. les dades personals?</strong><br />
	KoobinEvent S.L. recull la informaci&oacute; de l&rsquo;usuari o visitant d&rsquo;aquest lloc web en diferents moments:<br />
	En el moment de la seva inscripci&oacute; com a usuari registrat, mitjan&ccedil;ant l&rsquo;emplenament per la seva part d&rsquo;un o m&eacute;s formularis electr&ograve;nics habilitats a l&rsquo;efecte en aquest lloc web. Aquesta identificaci&oacute; permetr&agrave; posteriorment a l&rsquo;usuari registrat accedir o utilitzar determinats continguts o serveis oferts en aquest lloc web.<br />
	Durant la navegaci&oacute; pel lloc web, mitjan&ccedil;ant el registre de la seva direcci&oacute; IP.<br />
	Quan l&rsquo;usuari o visitant es comunica amb KoobinEvent S.L. mitjan&ccedil;ant l&rsquo;enviament d&rsquo;un e-mail.<br />
	<br />
	<strong>Per a qu&egrave; s&rsquo;utilitzen les dades personals?</strong><br />
	Formularis de recollida de dades<br />
	A cadascun dels formularis en els que es recullen dades de car&agrave;cter personal, l&rsquo;usuari rebr&agrave; informaci&oacute; detallada sobre el tractament de les seves dades, la seva finalitat, els possibles destinataris de la informaci&oacute;, el car&agrave;cter obligatori o facultatiu de les seves respostes a les preguntes que li siguin plantejades i, en general, de totes les mencions requerides per la legislaci&oacute; aplicable en mat&egrave;ria de protecci&oacute; de dades de car&agrave;cter personal. Els camps marcats amb un asterisc (*) al formulari de registre a emplenar per l&rsquo;usuari s&oacute;n necessaris i obligatoris per atendre la seva petici&oacute;, essent voluntaria la inclusi&oacute; de dades en els camps restants. La negativa de l&rsquo;usuari a facilitar la informaci&oacute; requerida en els camps obligatoris facultar&agrave; KoobinEvent S.L. a no atendre la petici&oacute;.<br />
	Direccions IP. Des del moment en que l&rsquo;usuari accedeix al nostre lloc web, la seva direcci&oacute; d&rsquo;INternet podria quedar registrada a les nostres m&agrave;quines en forma de fitxers LOG (hist&ograve;rics). En conseq&uuml;&egrave;ncia, l&rsquo;usuari deixa rastre de la direcci&oacute; IP que a cada sessi&oacute; li hagi assignat el seu prove&iuml;dor d&rsquo;acc&eacute;s, de tal forma que cada petici&oacute;, consulta, visita o trucada a un element d&rsquo;un lloc web podria quedar registrada. No obstant l&rsquo;anterior, els fitxers LOG s&oacute;n an&ograve;nims (de tal forma que no identifiquen el nom i cognoms de l&rsquo;usuari) i s&rsquo;utilitzaran &uacute;nicament per finalitats internes com la realitzaci&oacute; d&rsquo;estad&iacute;stiques, per dur un control dels accessos al lloc web.<br />
	Enviament d&rsquo;un correu electr&ograve;nic<br />
	Les dades personals (incloent sense car&agrave;cter limitatiu la seva direcci&oacute; de correu electr&ograve;nic) que l&rsquo;usuari o visitant d&rsquo;aquest lloc web ens faciliti mitjan&ccedil;ant l&rsquo;enviament d&rsquo;un correu electr&ograve;nic, seran incorporades a un fitxer del qual n&rsquo;&eacute;s titular i responsable KoobinEvent S.L. , amb l&rsquo;objectiu d&rsquo;atendre la seva petici&oacute;, aix&iacute; com per enviar-li informaci&oacute; de contingut comercial o promocional sobre productes, serveis, not&iacute;cies o esdeveniments relacionats amb KoobinEvent S.L. , per qualsevol mitj&agrave; incl&ograve;s el correu electr&ograve;nic. No obstant l&rsquo;anterior, aquest consentiment t&eacute; el car&agrave;cter de revocable, pel qual si l&rsquo;usuari no desitja que el tractament de les seves dades es realitzi amb la finalitat indicada, o desitja exercitar els seus drets d&rsquo;acc&eacute;s, rectificaci&oacute;, cancel&middot;laci&oacute; o oposici&oacute;, haur&agrave; de dirigir-se per escrit a KoobinEvent S.L. , Departament d&rsquo;Administraci&oacute;, adjuntat fotoc&ograve;pia del seu DNI o document d&rsquo;identificaci&oacute; equivalent, a KoobinEvent S.L. , Emili Grahit 91, Parc Cientific i Tecnol&ograve;gic UdG. Ed. Narc&iacute;s Monturiol. 17003 Girona.<br />
	<br />
	<strong>Com he d&rsquo;actualitzar les meves dades personals?</strong><br />
	L&rsquo;usuari garanteix que les dades personals facilitades a KoobinEvent S.L. a trav&eacute;s d&rsquo;aquest lloc web, b&eacute; mitjan&ccedil;ant l&rsquo;emplenament del corresponent formulari o b&eacute; mitjan&ccedil;ant l&rsquo;enviament d&rsquo;un correu electr&ograve;nic, s&oacute;n vertaderes, correctes, actuals i completes, i es fa responsable de comunicar a KoobinEvent S.L. qualsevol modificaci&oacute; o actualitzaci&oacute; de les mateixes. Per a fer-ho, hauran de comunicar per escrit a KoobinEvent S.L. qualsevol modificaci&oacute; o actualitzaci&oacute; de les seves dades, enviant una carta, adjuntant fotoc&ograve;pia del seu DNI o document d&rsquo;identificaci&oacute; equivalent, a KoobinEvent S.L. , Emili Grahit 91, Parc Cientific i Tecnol&ograve;gic UdG. Ed. Narc&iacute;s Monturiol. 17003 Girona.<br />
	<br />
	<strong>Qu&egrave; succeeix si facilito a KoobinEvent S.L. dades de terceres persones?</strong><br />
	En el sup&ograve;sit que l&rsquo;usuari inclogui, b&eacute; al formulari electr&ograve;nic corresponent o b&eacute; mitjan&ccedil;ant l&rsquo;enviament d&rsquo;un correu electr&ograve;nic, dades de car&agrave;cter personal referents a persones f&iacute;siques diferents de l&rsquo;usuari, aquest haur&agrave;, amb car&agrave;cter previ a la inclusi&oacute; o comunicaci&oacute; de les seves dades a KoobinEvent S.L., informar a tals persones sobre el contingut d&rsquo;aquesta Pol&iacute;tica de Privacitat. L&rsquo;usuari garanteix que ha obtingut el consentiment previ d&rsquo;aquestes terceres persones per la comunicaci&oacute; de les seves dades personals a KoobinEvent S.L..<br />
	<br />
	<strong>Quan es considera que he autoritzat a KoobinEvent S.L. per al tractament de les meves dades?</strong><br />
	Des del moment en que ens envies les teves dades personals, mitjan&ccedil;ant l&rsquo;emplenament dels formularis electr&ograve;nics o mitjan&ccedil;ant l&rsquo;enviament d&rsquo;un e-mail, ens autoritzes expressament a tractar les teves dades personals, amb les finalitats descrites a peu de cada formulari i/o en aquesta Pol&iacute;tica de Privacitat.<br />
	Igualment, l&rsquo;acc&eacute;s o la utilitzaci&oacute; d&rsquo;aquest lloc web implicar&agrave; l&rsquo;acceptaci&oacute; &iacute;ntegra d&rsquo;aquesta Pol&iacute;tica de Privacidad per l&rsquo;usuari o visitant, aix&iacute; com el seu consentiment i autoritzaci&oacute; per a la recollida, tractament i comunicaci&oacute; de les seves dades personals amb les finalitats descrites a aquesta Pol&iacute;tica de Privacitat.<br />
	&nbsp;</li>
	<li><strong>Comunicaci&oacute; d&rsquo;activitats de car&agrave;cter il&middot;l&iacute;cit o inadequat</strong><br />
	En el cas de que qualsevol usuari del lloc web tingui coneixement de que els hipervincles remeten a p&agrave;gines amb continguts o serveis il&middot;l&iacute;cits, nocius, denigrants, violents o contraris a la moral podr&agrave; posar-se en contacte amb KoobinEvent S.L. indicant els seg&uuml;ents extrems:<br />
	Dades personals del comunicant: nom, direcci&oacute;, n&uacute;mero de tel&egrave;fon i direcci&oacute; de correu electr&ograve;nic;<br />
	Descripci&oacute; dels fets que revelen el car&agrave;cter il&middot;l&iacute;cit o inadequat de l&rsquo;hipervincle;<br />
	En el sup&ograve;sit de violaci&oacute; de drets, tals com els de propietat intel&middot;lectual i industrial, les dades personals del titular del dret infringit quan sigui una persona diferent que el comunicant. Aix&iacute; mateix haur&agrave; d&rsquo;aportar el t&iacute;tol que acrediti la legitimaci&oacute; del titular dels drets i, si escau, el de representaci&oacute; per actuar per compte del titular quan sigui una persona diferent del comunicant. La recepci&oacute; per part de KoobinEvent S.L. de la comunicaci&oacute; prevista en aquesta cl&agrave;usula no suposar&agrave;, segons el disposat a la normativa relativa a serveis de la societat de la informaci&oacute; i de comer&ccedil; electr&ograve;nic, el coneixement efectiu de les activitats i/o continguts indicats pel comunicant.<br />
	&nbsp;</li>
	<li><strong>Legislaci&oacute;</strong><br />
	Les presents condicions d&rsquo;&uacute;s regeixen en tots i cadascun dels seus extrems per la legislaci&oacute; espanyola.</li>
</ol>', 'Avís legal', 'avis-legal', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('4', '2', 'Aviso legal', '', '_blank', '', '', '<p class=\"p1\">Para dar cumplimiento a lo dispuesto en la normativa aplicable en materia de servicios de sociedad de la informaci&oacute;n y de comercio electr&oacute;nico, a continuaci&oacute;n se indican los datos de informaci&oacute;n general de este sitio web:</p>

<p class=\"p1\">Titular:&nbsp;<b>KoobinEvent</b>&nbsp;S.L.</p>

<p class=\"p1\">Domicilio Social: Pic de Peguera 11, Parc Cientific i Tecnol&ograve;gic UdG. Ed. Centre d&#39;Empreses. 17003 Girona.</p>

<p class=\"p1\">Tel&eacute;fono: (+34) 972 183 393. Fax: (+34) 972 183 395</p>

<p class=\"p1\">Correo electr&oacute;nico: <a href=\"mailto:info@koobin.com\"><span class=\"s1\">info@koobin.com</span></a>.</p>

<p class=\"p1\">Datos Registrales: Registro Mercantil de Girona, Tomo 2782, Folio 172, Hoja GI 50281, Inscripci&oacute;n 1</p>

<p class=\"p1\">CIF: ES B55075022</p>

<p class=\"p1\"><b>1.- Condiciones de uso</b></p>

<p class=\"p1\">Las presentes condiciones regulan el uso del sitio web donde se facilita al p&uacute;blico informaci&oacute;n relativa a los servicios de&nbsp;<b>KoobinEvent</b>&nbsp;S.L.. Le rogamos lea detenidamente la informaci&oacute;n que le facilitamos. El hecho de acceder a este sitio web y utilizar los materiales contenidos en ella implica que usted ha le&iacute;do y acepta, sin reserva alguna, estas condiciones.&nbsp;<b>KoobinEvent</b>&nbsp;S.L. se reserva el derecho de denegar, suspender, interrumpir o cancelar el acceso o la utilizaci&oacute;n, total o parcialmente, de este sitio web a aquellos usuarios o visitantes del mismo que incumplan cualquiera de las condiciones previstas en este Aviso Legal.</p>

<p class=\"p1\">Este sitio web contiene materiales, como art&iacute;culos, estudios, descripci&oacute;n de proyectos, preparados por&nbsp;<b>KoobinEvent</b>&nbsp;S.L. con fines &uacute;nicamente informativos.&nbsp;<b>KoobinEvent</b>&nbsp;S.L. le informa que estos materiales puedes ser modificados, desarrollados o actualizados en cualquier momento sin notificaci&oacute;n previa.</p>

<p class=\"p1\"><b>2.- Derechos de Propiedad Intelectual e Industrial</b></p>

<p class=\"p1\">&copy; 2012&nbsp;<b>KoobinEvent</b>&nbsp;S.L. Todos los derechos reservados.</p>

<p class=\"p1\">La totalidad del contenido de este sitio web, entendiendo por contenido a titulo meramente enunciativo, textos, im&aacute;genes, ficheros, fotograf&iacute;as, logotipos, gr&aacute;ficos, marcas, iconos, combinaciones de colores, o cualquier otro elemento, su estructura y dise&ntilde;o, la selecci&oacute;n y forma de presentaci&oacute;n de los materiales incluidos en la misma, y el software, links y dem&aacute;s contenidos audiovisuales o sonoros, as&iacute; como su dise&ntilde;o gr&aacute;fico y c&oacute;digos fuentes necesarios para su funcionamiento, acceso y utilizaci&oacute;n, est&aacute;n protegidos por derechos de propiedad industrial e intelectual, titularidad de&nbsp;<b>KoobinEvent</b>&nbsp;S.L. o de terceros, sin que puedan entenderse cedidos los derechos de explotaci&oacute;n sobre los mismos m&aacute;s all&aacute; de lo estrictamente necesario para el correcto uso del sitio Web.</p>

<p class=\"p1\">En particular, quedan prohibidas la reproducci&oacute;n, la transformaci&oacute;n, distribuci&oacute;n, comunicaci&oacute;n p&uacute;blica, puesta a disposici&oacute;n del p&uacute;blico y en general cualquier otra forma de explotaci&oacute;n, por cualquier procedimiento, de todo o parte de los contenidos de este sitio web, as&iacute; como de su dise&ntilde;o y la selecci&oacute;n y forma de presentaci&oacute;n de los materiales incluidos en la misma. Estos actos de explotaci&oacute;n s&oacute;lo podr&aacute;n ser realizados si media la autorizaci&oacute;n expresa de&nbsp;<b>KoobinEvent</b>&nbsp;S.L. y siempre que se haga referencia a la titularidad de&nbsp;<b>KoobinEvent</b>&nbsp;S.L. de los indicados derechos de propiedad intelectual e industrial.</p>

<p class=\"p1\">Queda asimismo prohibido descompilar, desensamblar, realizar ingenier&iacute;a inversa, sublicenciar o transmitir de cualquier modo, traducir o realizar obras derivadas de los programas de ordenador necesarios para el funcionamiento, acceso y utilizaci&oacute;n de este sitio web y de los servicios en &eacute;l contenidos, as&iacute; como realizar, respecto a todo o parte de tales programas, cualesquiera de los actos de explotaci&oacute;n descritos en el p&aacute;rrafo anterior. El usuario del sitio web deber&aacute; abstenerse en todo caso de suprimir, alterar, eludir o manipular cualquiera dispositivos de protecci&oacute;n o sistemas de seguridad que puedan estar instalados en el mismo.</p>

<p class=\"p1\">Las marcas, nombres comerciales o signos distintivos son titularidad de&nbsp;<b>KoobinEvent</b>&nbsp;S.L. o de terceros, sin que pueda entenderse que el acceso al sitio web atribuya ning&uacute;n derecho sobre las citadas marcas, nombres comerciales y/o signos distintivos.</p>

<p class=\"p1\"><b>3.- Hiperenlaces</b></p>

<p class=\"p1\">Los hiperenlaces o links que contiene este sitio web pueden conducir al usuario a otros sitios y p&aacute;ginas web gestionados por terceros, sobre las que&nbsp;<b>KoobinEvent</b>&nbsp;S.L. no ejerce ning&uacute;n tipo de control,&nbsp;<b>KoobinEvent</b>&nbsp;S.L. no responde ni de los contenidos ni del estado de dichos sitios y p&aacute;ginas web, y el acceso a las mismas a trav&eacute;s de este sitio web tampoco implica que&nbsp;<b>KoobinEvent</b>&nbsp;S.L. recomiende o apruebe sus contenidos.</p>

<p class=\"p1\"><b>4.- Modificaciones</b></p>

<p class=\"p1\">Con el fin de mejorar el sitio web,&nbsp;<b>KoobinEvent</b>&nbsp;S.L. se reserva el derecho, en cualquier momento y sin previa notificaci&oacute;n, a modificar, ampliar o suspender temporalmente la presentaci&oacute;n, configuraci&oacute;n, especificaciones t&eacute;cnicas y servicios del sitio web, de forma unilateral.</p>

<p class=\"p1\">Asimismo se reserva el derecho de modificar en cualquier momento las presentes condiciones de uso as&iacute; como cualesquiera otras condiciones particulares.</p>

<p class=\"p1\"><b>5.- Exclusi&oacute;n de responsabilidad</b></p>

<p class=\"p1\">Quien utiliza este sitio web, lo hace por su propia cuenta y riesgo.&nbsp;<b>KoobinEvent</b>&nbsp;S.L. , sus socios, colaboradores, empleados y representantes no se responsabilizan de los errores u omisiones de los que pudieran adolecer los contenidos este sitio web u otros contenidos a los que se pueda acceder a trav&eacute;s de la misma.&nbsp;<b>KoobinEvent</b>&nbsp;S.L. , sus socios, colaboradores, empleados y representantes tampoco podr&aacute;n ser considerados responsables por cualesquiera da&ntilde;os derivados de la utilizaci&oacute;n de este sitio web, ni por cualquier actuaci&oacute;n realizada sobre la base de la informaci&oacute;n que en ella se facilita.</p>

<p class=\"p1\">La informaci&oacute;n de este sitio web se proporciona sin garant&iacute;a de ninguna clase, ni expl&iacute;cita ni impl&iacute;cita, y podr&aacute; cambiarse o actualizarse sin previo aviso.</p>

<p class=\"p1\"><b>KoobinEvent</b>&nbsp;S.L. no garantiza la ausencia de virus, gusanos u otros elementos inform&aacute;ticos lesivos que pudieran causar da&ntilde;os o alteraciones en el sistema inform&aacute;tico, en los documentos electr&oacute;nicos o en los ficheros del usuario de este sitio web. En consecuencia,&nbsp;<b>KoobinEvent</b>&nbsp;S.L. no responde por los da&ntilde;os y perjuicios que tales elementos pudieran ocasionar al usuario o a terceros. As&iacute; mismo no se responsabiliza ni garantiza la disponibilidad y continuidad en el acceso a este sitio web o que el mismo est&eacute; libre de errores, correspondiendo al usuario del sitio Web, en cualquier caso, la obligaci&oacute;n de disponer herramientas adecuadas para la detecci&oacute;n y desinfecci&oacute;n de programas inform&aacute;ticos da&ntilde;inos o lesivos.</p>

<p class=\"p1\">El usuario responder&aacute; de los da&ntilde;os y perjuicios de toda naturaleza que&nbsp;<b>KoobinEvent</b>&nbsp;S.L. pueda sufrir como consecuencia del incumplimiento de cualquiera de las obligaciones a las que queda sometido por las presentes condiciones. El usuario es consciente y acepta voluntariamente que el uso de cualesquiera contenidos de este sitio web tiene lugar, en todo caso, bajo su &uacute;nica y exclusiva responsabilidad.</p>

<p class=\"p1\"><b>6.- Pol&iacute;tica de Privacidad</b></p>

<p class=\"p1\"><b>KoobinEvent</b>&nbsp;S.L. est&aacute; comprometida con la protecci&oacute;n y la seguridad de los datos de car&aacute;cter personal que los usuarios o visitantes de este sitio web nos pudieran facilitar. Esta Pol&iacute;tica de Privacidad tiene por objeto informar a los usuarios y visitantes de nuestro sitio web de todo lo relativo a la recogida y tratamiento de sus datos personales por&nbsp;<b>KoobinEvent</b>&nbsp;S.L. .</p>

<p class=\"p1\"><b>KoobinEvent</b>&nbsp;S.L. se reserva el derecho de modificar y/o actualizar esta pol&iacute;tica de forma peri&oacute;dica, con el fin de adaptarla a la legislaci&oacute;n aplicable en cada momento, as&iacute; como a las pr&aacute;cticas leg&iacute;timas de&nbsp;<b>KoobinEvent</b>&nbsp;S.L. . En todo caso, el contenido de la nueva pol&iacute;tica ser&aacute; publicada en el sitio web y estar&aacute; permanentemente disponible para los usuarios y visitantes del mismo. Se recomienda a los usuarios y visitantes que lean y consulten esta Pol&iacute;tica de Privacidad cada vez que accedan a este sitio web.</p>

<p class=\"p1\">&iquest;C&oacute;mo recoge&nbsp;<b>KoobinEvent</b>&nbsp;S.L. los datos personales?</p>

<p class=\"p1\"><b>KoobinEvent</b>&nbsp;S.L. recaba la informaci&oacute;n del usuario o visitante de este sitio web en diferentes momentos:</p>

<p class=\"p1\">En el momento de su inscripci&oacute;n como usuario registrado, mediante la cumplimentaci&oacute;n por su parte de uno o m&aacute;s formularios electr&oacute;nicos habilitados al efecto en este sitio web. Esta identificaci&oacute;n permitir&aacute; posteriormente al usuario registrado acceder o utilizar determinados contenidos o servicios ofrecidos en este sitio web.</p>

<p class=\"p1\">Durante la navegaci&oacute;n por el sitio web, mediante el registro de su direcci&oacute;n IP.</p>

<p class=\"p1\">Cuando el usuario o visitante se comunica con&nbsp;<b>KoobinEvent</b>&nbsp;S.L. mediante el env&iacute;o de un e-mail.</p>

<p class=\"p1\">&iquest;Para qu&eacute; se utilizan los datos personales?</p>

<p class=\"p1\">Formularios de recogida de datos<br />
En cada uno de los formularios en los que se recaben datos de car&aacute;cter personal, el usuario recibir&aacute; informaci&oacute;n detallada sobre el tratamiento de sus datos, su finalidad, los posibles destinatarios de la informaci&oacute;n, el car&aacute;cter obligatorio o facultativo de sus respuestas a las preguntas que le sean planteadas y, en general, de todas las menciones requeridas por la legislaci&oacute;n aplicable en materia de protecci&oacute;n de datos de car&aacute;cter personal.<br />
Los campos marcados con un asterisco (*) en el formulario de registro a cumplimentar por el usuario son necesarios y obligatorios para atender su petici&oacute;n, siendo voluntaria la inclusi&oacute;n de datos en los campos restantes. La negativa del usuario a facilitar la informaci&oacute;n requerida en los campos obligatorios facultar&aacute; a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. a no atender la petici&oacute;n.</p>

<p class=\"p1\">Direcciones IP, desde el momento en que el usuario accede a nuestro sitio web, su direcci&oacute;n de Internet podr&iacute;a quedar registrada en nuestras m&aacute;quinas en forma de ficheros LOG (hist&oacute;ricos). En consecuencia, el usuario deja rastro de la direcci&oacute;n IP que en cada sesi&oacute;n le haya asignado su proveedor de acceso, de tal forma que cada petici&oacute;n, consulta, visita o llamada a un elemento de un sitio web podr&iacute;a quedar registrada. No obstante lo anterior, los ficheros LOG son an&oacute;nimos (de tal forma que no identifican el nombre y apellidos del usuario) y se utilizar&aacute;n &uacute;nicamente para fines internos como la realizaci&oacute;n de estad&iacute;sticas, para llevar un control de los accesos al sitio web.</p>

<p class=\"p1\">Env&iacute;o de un correo electr&oacute;nico</p>

<p class=\"p1\">Los datos personales (incluyendo sin car&aacute;cter limitativo su direcci&oacute;n de correo electr&oacute;nico) que el usuario o visitante de este sitio web nos facilite mediante el env&iacute;o de un correo electr&oacute;nico, ser&aacute;n incorporados a un fichero cuyo titular y responsable es&nbsp;<b>KoobinEvent</b>&nbsp;S.L. , con el fin de atender su petici&oacute;n, as&iacute; como para enviarle informaci&oacute;n de contenido comercial o promocional sobre productos, servicios, noticias o eventos relacionados con&nbsp;<b>KoobinEvent</b>&nbsp;S.L. , por cualquier medio incluido el correo electr&oacute;nico. No obstante lo anterior, este consentimiento tiene el car&aacute;cter de revocable, por lo que si el usuario no desea que el tratamiento de sus datos se realice con la finalidad indicada, o desea ejercitar los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n, deber&aacute; dirigirse por escrito a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. , Departamento de Administraci&oacute;n, adjuntando fotocopia de su DNI o documento de identificaci&oacute;n equivalente, a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. , Emili Grahit 91, Parc Cientific i Tecnol&ograve;gic UdG. Ed. Narc&iacute;s Monturiol. 17003 Girona.</p>

<p class=\"p1\">&iquest;C&oacute;mo debo actualizar mis datos personales?</p>

<p class=\"p1\">El usuario garantiza que los datos personales facilitados a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. a trav&eacute;s de este sitio web, bien mediante la cumplimentaci&oacute;n del correspondiente formulario o bien mediante el env&iacute;o de un correo lectr&oacute;nico, son veraces, correctos, actuales y completos, y se hace responsable de comunicar a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. cualquier modificaci&oacute;n o actualizaci&oacute;n de los mismos. Para ello, deber&aacute;n comunicar por escrito a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. cualquier modificaci&oacute;n o actualizaci&oacute;n de sus datos, enviando una carta, adjuntando fotocopia de su DNI o documento de identificaci&oacute;n equivalente, a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. , Emili Grahit 91, Parc Cientific i Tecnol&ograve;gic UdG. Ed. Narc&iacute;s Monturiol. 17003 Girona.</p>

<p class=\"p1\">&iquest;Qu&eacute; ocurre si facilito a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. datos de terceras personas?</p>

<p class=\"p1\">En el supuesto de que el usuario incluya, bien en el formulario electr&oacute;nico correspondiente o bien mediante el env&iacute;o de un correo electr&oacute;nico, datos de car&aacute;cter personal referentes a personas f&iacute;sicas distintas del usuario, &eacute;ste deber&aacute;, con car&aacute;cter previo a la inclusi&oacute;n o comunicaci&oacute;n de sus datos&nbsp;<b>KoobinEvent</b>&nbsp;S.L., informar a dichas personas sobre el contenido de esta Pol&iacute;tica de Privacidad. El usuario garantiza que ha obtenido el consentimiento previo de estas terceras personas para la comunicaci&oacute;n de sus datos personales a&nbsp;<b>KoobinEvent</b>&nbsp;S.L..</p>

<p class=\"p1\">&iquest;Cu&aacute;ndo se considera que he autorizado a&nbsp;<b>KoobinEvent</b>&nbsp;S.L. para el tratamiento de mis datos?</p>

<p class=\"p1\">Desde el momento en que nos env&iacute;as tus datos personales, mediante la cumplimentaci&oacute;n de los formularios electr&oacute;nicos o mediante el env&iacute;o de un e-mail, nos autorizas expresamente a tratar tus datos personales, con las finalidades descritas al pie de cada formulario y/o en esta Pol&iacute;tica de Privacidad.<br />
Igualmente, el acceso o la utilizaci&oacute;n de este sitio web implicar&aacute; la aceptaci&oacute;n &iacute;ntegra de esta Pol&iacute;tica de Privacidad por el usuario o visitante, as&iacute; como su consentimiento y autorizaci&oacute;n para la recogida, tratamiento y comunicaci&oacute;n de sus datos de car&aacute;cter personal con las finalidades descritas en dicha Pol&iacute;tica.</p>

<p class=\"p1\"><b>7.- Comunicaci&oacute;n de actividades de car&aacute;cter il&iacute;cito e inadecuado</b></p>

<p class=\"p1\">En el caso de que cualquier usuario del sitio web tuviera conocimiento de que los hiperenlaces remiten a p&aacute;ginas cuyos contenidos o servicios son il&iacute;citos, nocivos, denigrantes, violentos o contrarios a la moral podr&aacute; ponerse en contacto con&nbsp;<b>KoobinEvent</b>&nbsp;S.L. indicando los siguientes extremos:</p>

<p class=\"p1\">Datos personales del comunicante: nombre, direcci&oacute;n, n&uacute;mero de tel&eacute;fono y direcci&oacute;n de correo electr&oacute;nico;</p>

<p class=\"p1\">Descripci&oacute;n de los hechos que revelan el car&aacute;cter il&iacute;cito o inadecuado del hiperenlace;</p>

<p class=\"p1\">En el supuesto de violaci&oacute;n de derechos, tales como propiedad intelectual e industrial, los datos personales del titular del derecho infringido cuando sea persona distinta del comunicante. Asimismo deber&aacute; aportar el t&iacute;tulo que acredite la legitimaci&oacute;n del titular de los derechos y, en su caso, el de representaci&oacute;n para actuar por cuenta del titular cuando sea persona distinta del comunicante; La recepci&oacute;n por parte de&nbsp;<b>KoobinEvent</b>&nbsp;S.L. de la comunicaci&oacute;n prevista en esta cl&aacute;usula no supondr&aacute;, seg&uacute;n lo dispuesto en la normativa relativa a servicios de la sociedad de la informaci&oacute;n y de comercio electr&oacute;nico, el conocimiento efectivo de las actividades y/o contenidos indicados por el comunicante.</p>

<p class=\"p1\"><b>8.- Legislaci&oacute;n</b></p>

<p class=\"p1\">Las presentes condiciones de uso se rigen en todos y cada uno de sus extremos por la legislaci&oacute;n espa&ntilde;ola.</p>', 'Aviso legal', 'aviso-legal', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('4', '3', 'Legal Notice', '', '_blank', '', '', '<p class=\"p1\"><b>Legal Notice</b></p>

<p class=\"p2\">Welcome to the website of&nbsp;<b>KoobinEvent</b>&nbsp;S.L.</p>

<p class=\"p2\">To comply with the provisions of law applicable to the services of the Information Society and Electronic Commerce, below shows the general information of this website:</p>

<p class=\"p2\">Owner:&nbsp;<b>KoobinEvent</b>&nbsp;S.L.</p>

<p class=\"p2\">Address: Pic de Peguera 11, Science and Technology Park UdG. Enterprise Center Building 17003 Girona.</p>

<p class=\"p2\">Phone: (+34) 972183 393.&nbsp;Fax: (+34) 972 183 395</p>

<p class=\"p3\"><span class=\"s1\">Email: <a href=\"mailto:info@koobin.com\"><span class=\"s2\">info@koobin.com</span></a>.</span></p>

<p class=\"p2\">Registration information: Register of Girona, Volume 2782, Folio 172, Sheet GI 50281, Inscription 1</p>

<p class=\"p2\">CIF: ES B55075022</p>

<p class=\"p2\"><b>1. - Terms of Use</b></p>

<p class=\"p2\">These conditions govern the use of the website which provides the public with information on services KoobinEvent SL.&nbsp;Please read the information we provide.&nbsp;By accessing this website and use the materials contained in it implies that you have read and accept, without reservation, these conditions.&nbsp;KoobinEvent S.L.&nbsp;reserves the right to deny, suspend, discontinue or terminate your access or use all or part of this website to those users or visitors to the same breach any of the conditions in this Legal Notice.</p>

<p class=\"p2\">This website contains materials such as articles, studies and project descriptions prepared by KoobinEvent SL&nbsp;information purposes only.&nbsp;KoobinEvent S.L.&nbsp;advised that these materials may be modified, developed or updated at any time without notice.</p>

<p class=\"p2\"><b>2. - Intellectual and Industrial Property Rights</b></p>

<p class=\"p1\">&copy; 2012 KoobinEvent S.L.&nbsp;All rights reserved.</p>

<p class=\"p2\">The entire contents of this web site, meaning content merely illustrative purposes, text, images, files, photographs, logos, graphics, trademarks, icons, color schemes, or any other element, its structure and design, selection and&nbsp;presentation of the materials included in it, and the software, links and other audiovisual or audio, and graphic design and source code required for its operation, access and use, are protected by copyright and intellectualKoobinEvent ownership of SL&nbsp;or others, without being interpreted as granted rights to exploit them beyond what is necessary for the proper use of the website.</p>

<p class=\"p2\">In particular, the reproduction, processing, distribution, public communication, making available to the general public and any other form of exploitation, by any means, all or part of the contents of this website, and their&nbsp;design and the selection and presentation of the materials included in it.&nbsp;Such use may only be carried out with the permission of KoobinEvent SL&nbsp;and whenever reference is made to the ownership of KoobinEvent SL&nbsp;of those rights of intellectual property.</p>

<p class=\"p2\">Is also prohibited to decompile, disassemble, reverse engineer, sublicense or otherwise transmit, translate or make derivative works of the computer programs necessary for the operation, access and use of this website and the services it contains, and&nbsp;made concerning all or part of such programs, any of the acts of exploitation described in the preceding paragraph.&nbsp;The user of the website must refrain at all times from deleting, altering, evading or manipulating any protection devices or security systems that may be installed in it.</p>

<p class=\"p2\">The trademarks, trade names or logos are owned by SL KoobinEvent&nbsp;or third parties may not be construed that access to the web site gives no rights over any trademarks, trade names and / or logos.</p>

<p class=\"p2\"><b>3. - Hyperlinks</b></p>

<p class=\"p2\">The hyperlinks contained in this web site may lead users to other websites and pages operated by third parties over which KoobinEvent SL&nbsp;exercises no control KoobinEvent SL&nbsp;not responsible for the contents or status of these sites and web pages, and access to them through this website does not mean that SL KoobinEvent&nbsp;recommends or approves their contents.</p>

<p class=\"p2\"><b>4. - Modifications</b></p>

<p class=\"p2\">In order to improve the website, KoobinEvent SL&nbsp;reserves the right at any time, without notice, to modify, extend or suspend the presentation, configuration, technical specifications and website services, unilaterally.</p>

<p class=\"p2\">It also reserves the right to modify at any time the terms of use and any other conditions.</p>

<p class=\"p2\"><b>5. - Disclaimer</b></p>

<p class=\"p2\">Anyone using this web site is at your own risk.&nbsp;KoobinEvent S.L.&nbsp;, Its partners, collaborators, employees and representatives are not liable for any errors or omissions that could suffer the contents of this website or other content that may be accessed through it.KoobinEvent S.L.&nbsp;, Its partners, collaborators, employees and representatives can not be held liable for any damages arising from the use of this website, nor for any action taken on the basis of the information provided therein.</p>

<p class=\"p2\">The information in this web site is provided without warranty of any kind, either express or implied, and may be changed or updated without notice.</p>

<p class=\"p2\">KoobinEvent S.L.&nbsp;not guarantee the absence of viruses, worms or other damaging computer programs that may damage or alter the computer system, electronic documents or user files on this website.Accordingly, S.L. KoobinEvent&nbsp;not liable for damages that such elements may cause to users or third parties.&nbsp;Likewise, no representation or guarantee the availability and continued access to this site or that it is error free, the user of the website, in any case, the requirement to have adequate tools to detect and disinfectmalicious software or harmful.</p>

<p class=\"p2\">The user liable for damages of any kind that KoobinEvent SL&nbsp;may suffer as a result of breach of any of the obligations to which they are bound by these conditions.&nbsp;The user understands and accepts that the use of any content of this website takes place in any event, your sole and exclusive responsibility.</p>

<p class=\"p2\"><b>6. - Privacy Policy</b></p>

<p class=\"p2\">KoobinEvent S.L.&nbsp;is committed to safety and security of personal data that users or visitors of this website we could provide.&nbsp;This Privacy Policy is intended to inform users and visitors to our web site for all matters relating to the collection and processing of personal data by KoobinEvent SL&nbsp;.</p>

<p class=\"p2\">KoobinEvent S.L.&nbsp;reserves the right to modify and / or update this policy periodically in order to adapt to current applicable laws and legitimate practices KoobinEvent SL&nbsp;.&nbsp;In any case, the content of the new policy will be posted on the website and will be permanently available to users and visitors of the same.&nbsp;It is recommended to users and visitors to read and consult this Privacy Policy each time you access this website.</p>

<p class=\"p2\">How do you collect KoobinEvent S.L.&nbsp;personal data?</p>

<p class=\"p2\">KoobinEvent S.L.&nbsp;collects information from the user or viewer of this website at different times:</p>

<p class=\"p2\">At the time of registration as registered user by filling in for their share of one or more electronic forms for this purpose on this website.&nbsp;This identification will allow the registered user subsequently access or use certain content or services offered on this website.</p>

<p class=\"p2\">While navigating the website, by registering your IP address.</p>

<p class=\"p2\">When the user or visitor communicates with KoobinEvent SL&nbsp;by sending an e-mail.</p>

<p class=\"p2\">What is personal information used?</p>

<p class=\"p2\">Data collection forms<br />
In each of the forms in which they are collected personal data, the user will receive detailed information about the processing of data, its purpose, potential recipients of information, the mandatory or optional answers to questions&nbsp;asked and, in general, to all the information required by applicable law on the protection of personal data.<br />
Fields marked with an asterisk (*) in the registration form to be completed by the user are necessary and required to fulfill your request is voluntary inclusion of data in the remaining fields.&nbsp;The refusal of the user to provide the required information in the required fields KoobinEvent empower SL&nbsp;not meet the request.</p>

<p class=\"p2\">IP addresses, from the moment the user accesses our website, your Internet address may be recorded on our machines as files LOG (historical).&nbsp;Consequently, the user leaves trail of the IP address for each session you by your ISP, so that each request, consultation, visit or call to an element of a website could be recorded.Notwithstanding the foregoing, LOG files are anonymous (so you do not identify the name of the user) and used only for internal purposes such as performance statistics to keep track of the accesses to the site.</p>

<p class=\"p2\">Send an email</p>

<p class=\"p2\">Personal data (including but not limited to your email address) the user or viewer of this website we provide by sending an email, will be added to a database owned and managed is KoobinEvent SL&nbsp;In order to fulfill your request, and to send you marketing information or promotional information about products, services, news and events related to KoobinEvent SL&nbsp;By any means including email.Nevertheless, this consent must be revoked, so if the user does not want the processing of data is done with the stated purpose, or wish to exercise their rights of access, rectification, cancellation or opposition, should be addressed&nbsp;writing KoobinEvent SLDepartment of Administration, enclosing a photocopy of your ID or equivalent identification, to KoobinEvent SL&nbsp;, Emili Grahit 91, Science and Technology Park UdG.&nbsp;Ed Narc&iacute;s Monturiol.&nbsp;17003 Girona.</p>

<p class=\"p2\">How do I update my personal information?</p>

<p class=\"p2\">The user guarantees that the personal data provided to KoobinEvent SL&nbsp;through this website or by completing the appropriate form or by sending an e lectr&oacute;nico are true, accurate, current and complete, and is responsible for communicating to KoobinEvent SL&nbsp;any changes or updates to them.&nbsp;To do so, shall give written notice to KoobinEvent SL&nbsp;any changes or updates to their data by sending a letter, enclosing a photocopy of your ID or equivalent identification, to KoobinEvent SL&nbsp;, Emili Grahit 91, Science and Technology Park UdG.&nbsp;Ed Narc&iacute;s Monturiol.&nbsp;17003 Girona.</p>

<p class=\"p2\">What happens if I provide KoobinEvent S.L.&nbsp;data from third parties?</p>

<p class=\"p2\">In the event that the user include either in the corresponding electronic form or by sending an email, personal data relating to individuals other than the user, it must, prior to the inclusion or disclosure of their&nbsp;SL KoobinEvent data to inform such persons of the contents of this Privacy Policy.&nbsp;You warrant that you have obtained the prior consent of such third persons for the communication of personal data to KoobinEvent SL.</p>

<p class=\"p2\">When is that I have authorized KoobinEvent SL&nbsp;for the treatment of my data?</p>

<p class=\"p2\">From the moment you send us your details by filling out electronic forms or by sending an e-mail, expressly authorize us to process your personal data for the purposes described at the bottom of each form and / or&nbsp;this Privacy Policy.<br />
Similarly, access or use of this website implies full acceptance of this Privacy Policy by the user or visitor and consent and authorization for the collection, processing and communication of your personal data for the purposes described in&nbsp;this Policy.</p>

<p class=\"p2\"><b>7. - Communication from illicit and inadequate activities&nbsp;</b></p>

<p class=\"p2\">In the event that any website user becomes aware that the hyperlinks refer to pages whose content or services are illegal, harmful, degrading, violent or contrary to public morality may contact KoobinEvent SL&nbsp;indicating the following:</p>

<p class=\"p2\">Personal data: name, address, phone number and email address;</p>

<p class=\"p2\">Description of the facts showing the unlawful or improper hyperlink;</p>

<p class=\"p2\">In the case of violation of rights, including intellectual property, personal data of the holder of the rights infringed when someone other than the petitioner.&nbsp;It should also provide the title that certifies the holder&#39;s legitimate rights and, where appropriate, the representative to act on behalf of the holder where the informant is a person, the receipt by KoobinEvent SL&nbsp;of the notice provided in this clause will not, in accordance with the rules on services of the information society and electronic commerce, the actual knowledge of the activities and / or contents indicated by the caller.</p>

<p class=\"p2\"><b>8. - Legislation</b></p>

<p class=\"p2\">These conditions of use are governed in each and every one of its ends by Spanish law.</p>', 'Legal Notice', 'legal-notice', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('4', '4', '', '', '_blank', '', '', '', '', '', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('5', '1', 'Mapa del web', '', '_blank', 'sitemap', '', '', 'Mapa del web', 'mapa-del-web', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('5', '2', 'Mapa web', '', '_blank', 'sitemap', '', '', 'Mapa web', 'mapa-web', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('5', '3', 'Sitemap', '', '_blank', 'sitemap', '', '', 'Sitemap', 'sitemap', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('5', '4', '', '', '_blank', '', '', '', '', '', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('6', '1', 'Ús de Cookies', '', '_blank', '', '', '<h3>&Uacute;s de cookies</h3>

<p>Les cookies o galetes s&oacute;n uns petits arxius de text que s&rsquo;instal&middot;len als equips des dels quals s&rsquo;accedeix al nostre lloc web. Poden guardar la identificaci&oacute; de l&#39;usuari que visita el web i els llocs pels quals navega. Quan l&rsquo;usuari (vost&egrave; en aquest cas) ens torna a visitar es llegeixen les cookies per identificar-lo i restablir les seves prefer&egrave;ncies i configuraci&oacute; de navegaci&oacute;. Si un usuari no autoritza l&#39;&uacute;s de les cookies alguns serveis o funcionalitats del lloc web podrien no estar disponibles.</p>

<p>Volem que aquest lloc ofereixi un bon servei i que sigui f&agrave;cil d&#39;utilitzar. El nostre objectiu &eacute;s oferir-li productes i serveis personalitzats. En aquest sentit les cookies ens permeten:</p>

<ul class=\"ulCookies\">
	<li>Identificar els usuaris registrats quan tornen al lloc perqu&egrave; puguin recuperar resultats de la seva activitat anterior i visualitzar les seves factures anteriors.</li>
	<li>Estalviar temps evitant la necessitat de tornar a introduir la mateixa informaci&oacute;.</li>
	<li>Protegir la seva seguretat mitjan&ccedil;ant la comprovaci&oacute; de les dades d&#39;inici de sessi&oacute;.</li>
</ul>

<p>Aquest lloc web utilitza les galetes seg&uuml;ents:</p>

<p><b>Google Analytics</b></p>

<p>Google Analytics &eacute;s un servei gratu&iuml;t que ofereix Google Inc. i que recull informaci&oacute; sobre quines p&agrave;gines del lloc web s&#39;han consultat, en quin moment, amb quin navegador, etc. Posteriorment, aquesta informaci&oacute; s&#39;envia als servidors de Google Inc. als Estats Units.</p>

<p>M&eacute;s informaci&oacute; a <a href=\"www.google.com/analytics/\">www.google.com/analytics/</a>.</p>

<p>Per controlar la recopilaci&oacute; de dades amb finalitats anal&iacute;tiques per part de Google Analytics, pot anar a <a href=\"https://tools.google.com/dlpage/gaoptout?hl=en\">https://tools.google.com/dlpage/gaoptout?hl=en</a></p>

<p><b>Adobe Typekit</b></p>

<p>Typekit &eacute;s un servei que li proporciona acc&eacute;s a una biblioteca de fonts tipogr&agrave;fiques.&nbsp;</p>

<p>Per poder proporcionar el servei Typekit, Adobe pot recopilar informaci&oacute; sobre les fonts que utilitza un lloc web. La informaci&oacute; s&#39;utilitza per a finalitats de facturaci&oacute; i compliment d&rsquo;obligacions legals.</p>

<p><b>Piwik</b></p>

<p>Piwik &eacute;s una eina per a m&egrave;triques en temps real. Permet fer un seguiment de la navegaci&oacute; de les visites: quines p&agrave;gines visiten, en quin navegador, etc. Aquesta informaci&oacute; s&rsquo;envia als servidors de Koobin Event SL.&nbsp;</p>

<p>Vost&egrave; pot revocar el consentiment a la utilitzaci&oacute; de cookies eliminant-les per mitj&agrave; de les opcions que li ofereix el seu navegador.</p>

<p>Si desitja rebre informaci&oacute; addicional sobre l&rsquo;activitat de les empreses de publicitat a Internet i sobre com eliminar les seves dades dels registres d&#39;aquestes empreses, li recomanem la visita de <a href=\"http://www.networkadvertising.org\">www.networkadvertising.org</a>.</p>

<p>Vost&egrave; pot configurar el seu navegador per tal que l&rsquo;informi pr&egrave;viament de la possible instal&middot;laci&oacute; de cookies. Tamb&eacute; podr&agrave; optar per tal que es suprimeixin autom&agrave;ticament un cop es tanqui el navegador, equip o dispositiu. Pot trobar informaci&oacute; sobre com fer-ho a:</p>

<ul class=\"ulCookies\">
	<li>Per a Firefox a <a href=\"http://support.mozilla.org/ca/kb/Galetes\">http://support.mozilla.org/ca/kb/Galetes</a></li>
	<li>Per a Chrome a <a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647\">http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647</a></li>
	<li>Per a Explorer <a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>
	<li>Per a Safari <a href=\"http://support.apple.com/kb/ph5042\">http://support.apple.com/kb/ph5042</a></li>
	<li>Per a Safari per a IOS <a href=\"http://support.apple.com/kb/HT1677?viewlocale=es_ES&amp;locale=es_ES\">http://support.apple.com/kb/HT1677?viewlocale=es_ES&amp;locale=es_ES</a></li>
	<li>Per a Opera a <a href=\"http://help.opera.com/Windows/11.50/es-ES/cookies.html\">http://help.opera.com/Windows/11.50/es-ES/cookies.html</a></li>
</ul>', 'Ús de Cookies', 'us-de-cookies', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('6', '2', 'Uso de cookies', '', '_blank', '', '', '<h3>Pol&iacute;tica de cookies</h3>

<p>Este Web utiliza cookies propias y de terceros para ofrecerle una mejor experiencia y servicio. Al navegar o utilizar nuestros servicios el usuario acepta el uso que hacemos de las cookies. Sin embargo, el usuario tiene la opci&oacute;n de impedir la generaci&oacute;n de cookies y la eliminaci&oacute;n de las mismas mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su Navegador. En caso de bloquear el uso de cookies en su navegador es posible que algunos servicios o funcionalidades de la p&aacute;gina Web no est&eacute;n disponibles.</p>

<h4>&iquest;Qu&eacute; es una cookie?</h4>

<p>Una cookie es un peque&ntilde;o fragmento de texto que los sitios Web env&iacute;an al navegador y que se almacenan en el terminal del usuario, el cual puede ser un ordenador personal, un tel&eacute;fono m&oacute;vil, un tablet, etc.. &nbsp;Estos archivos permiten que el sitio Web recuerde informaci&oacute;n sobre su visita, como el idioma y las opciones preferidas, lo que puede facilitar su pr&oacute;xima visita y hacer que el sitio resulte m&aacute;s &uacute;til al personalizar su contenido. Las cookies desempe&ntilde;an un papel muy importante, al mejorar la experiencia de uso de la Web.</p>

<h4>&iquest;C&oacute;mo se utilizan las cookies?</h4>

<p>Al navegar por este portal Web el usuario est&aacute; aceptando que se puedan instalar cookies en su terminal y que nos permiten conocer la siguiente informaci&oacute;n:</p>

<ul class=\"ulCookies\">
	<li>Informaci&oacute;n estad&iacute;stica del uso de la web.</li>
	<li>El formato de la web preferente en el acceso desde dispositivos m&oacute;viles.</li>
	<li>Las &uacute;ltimas b&uacute;squedas realizadas en los servicios de la web y datos de personalizaci&oacute;n de estos servicios.</li>
</ul>

<h4>Tipos de cookies utilizadas</h4>

<p>Este Web utiliza tanto cookies temporales de sesi&oacute;n como cookies permanentes. Las cookies de sesi&oacute;n almacenan datos &uacute;nicamente mientras el usuario accede al Web y las cookies permanentes almacenan los datos en el terminal para que sean accedidos y utilizados en m&aacute;s de una sesi&oacute;n.</p>

<p>Seg&uacute;n la finalidad para la que se traten los datos obtenidos a trav&eacute;s de las cookies, el Web puede utilizar:</p>

<p><b>Cookies t&eacute;cnicas</b></p>

<p>Son aqu&eacute;llas que permiten al usuario la navegaci&oacute;n a trav&eacute;s de la p&aacute;gina Web o aplicaci&oacute;n y la utilizaci&oacute;n de las diferentes opciones o servicios que en ella existen. Por ejemplo, controlar el tr&aacute;fico y la comunicaci&oacute;n de datos, identificar la sesi&oacute;n, acceder a las partes Web de acceso restringido, recordar los elementos que integran un pedido, realizar la solicitud de inscripci&oacute;n o participaci&oacute;n en un evento, utilizar elementos de seguridad durante la navegaci&oacute;n y almacenar contenidos para la difusi&oacute;n de videos o sonido.</p>

<p><b>Cookies de personalizaci&oacute;n</b></p>

<p>Son aqu&eacute;llas que permiten al usuario acceder al servicio con algunas caracter&iacute;sticas de car&aacute;cter general predefinidas en su terminal o que el propio usuario defina. Por ejemplo, el idioma, el tipo de navegador a trav&eacute;s del cual accede al servicio, el dise&ntilde;o de contenidos seleccionado, geolocalizaci&oacute;n del terminal y la configuraci&oacute;n regional desde donde se accede al servicio.</p>

<p><b>Cookies de an&aacute;lisis estad&iacute;stico</b></p>

<p>Son aqu&eacute;llas que permiten realizar el seguimiento y an&aacute;lisis del comportamiento de los usuarios en los sitios Web. La informaci&oacute;n recogida mediante este tipo de cookies se utiliza en la medici&oacute;n de la actividad de los sitios Web, aplicaci&oacute;n o plataforma y para la elaboraci&oacute;n de perfiles de navegaci&oacute;n de los usuarios de dichos sitios, con el fin de introducir mejoras en el servicio en funci&oacute;n de los datos de uso que hacen los usuarios.</p>

<p><b>Cookies de terceros</b></p>

<p>En algunas p&aacute;ginas Web se pueden instalar cookies de terceros que permiten gestionar y mejorar los servicios ofrecidos. Como por ejemplo, servicios estad&iacute;sticos de Google Analytics.</p>

<p>&nbsp;</p>

<h4>&iquest;C&oacute;mo administrar cookies en el navegador?</h4>

<p>El usuario tiene la opci&oacute;n de permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuraci&oacute;n de las opciones del navegador instalado en su terminal:</p>

<p>Para ajustar los permisos relacionados con las cookies en el <b>Navegador Google Chrome</b>:</p>

<ul class=\"ulCookies\">
	<li>Hacer clic en el men&uacute; situado en la barra de herramientas.</li>
	<li>Seleccionar Configuraci&oacute;n.</li>
	<li>Hacer clic en Mostrar opciones avanzadas.</li>
	<li>En la secci&oacute;n &ldquo;Privacidad&rdquo; hacer clic en el bot&oacute;n Configuraci&oacute;n de contenido.</li>
	<li>En la secci&oacute;n de &ldquo;Cookies&rdquo; se pueden configurar las opciones.</li>
</ul>

<p>Para ajustar los permisos relacionados con las cookies en el Navegador <b>Mozilla Firefox</b>:</p>

<ul class=\"ulCookies\">
	<li>En la parte superior de la ventana de Firefox hacer clic en el men&uacute; Herramientas.</li>
	<li>Seleccionar Opciones.</li>
	<li>Seleccionar el panel Privacidad.</li>
	<li>En la opci&oacute;n Firefox podr&aacute; elegir Usar una configuraci&oacute;n personalizada para el historial para configurar las opciones.</li>
</ul>

<p><a href=\"https://support.mozilla.org/es/kb/cookies-informacion-que-los-sitios-web-guardan-en-\">M&aacute;s informaci&oacute;n sobre Mozilla Firefox.</a></p>

<p>Para ajustar los permisos relacionados con las cookies en el Navegador <b>Internet Explorer 9</b>:</p>

<ul class=\"ulCookies\">
	<li>En la parte superior de la ventana de Internet Explorer hacer clic en el men&uacute; Herramientas.</li>
	<li>Seleccionar la pesta&ntilde;a de Seguridad y utilizar la opci&oacute;n Eliminar el historial de exploraci&oacute;n para eliminar las cookies. Activar la casilla Cookies y, a continuaci&oacute;n, hacer clic en Eliminar.</li>
	<li>Seleccionar la pesta&ntilde;a de Seguridad y utilizar la Configuraci&oacute;n mover el control deslizante totalmente hacia arriba para bloquear todas las cookies o totalmente hacia abajo para permitir todas las cookies. Tras ello, hacer clic enAceptar.</li>
</ul>

<p><a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\">M&aacute;s informaci&oacute;n sobre Internet Explorer 9.</a></p>

<p>En caso de bloquear el uso de cookies en su navegador es posible que algunos servicios o funcionalidades de la p&aacute;gina Web no est&eacute;n disponibles.</p>

<p>En algunos navegadores se pueden configurar reglas espec&iacute;ficas para administrar cookies por sitio Web, lo que ofrece un control m&aacute;s preciso sobre la privacidad. Esto significa que se puede inhabilitar cookies de todos los sitios salvo de aquellos en los que se conf&iacute;e.</p>', 'Uso de cookies', 'uso-de-cookies', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('6', '3', 'use of cookies', '', '_blank', '', '', '<div class=\"euText\">
<h3><strong>What is a cookie?</strong></h3>

<p>A cookie is a text file no bigger than 4k that a website asks your browser to store on your computer or mobile device. This allows the website to &quot;remember&quot; your actions or preferences over a period of time.</p>

<p>Most browsers support cookies however users can set their browsers to decline them and can also delete them whenever they like.</p>

<h3><strong>What are they used for?</strong></h3>

<p>Websites mainly use cookies:</p>

<ul>
	<li>to identify users</li>
	<li>to remember the user&#39;s custom preferences</li>
	<li>to help complete a task without having to re-enter information when browsing from one page to another or when visiting the site again sometime later</li>
</ul>

<p>Cookies can also be used for online behavioral target advertising and show adverts relevant to something that the user searched for in the past.</p>

<h3><strong>How are they used?</strong></h3>

<p>The web server supplying the webpage can store a cookie on the user&#39;s computer or mobile device. An external web server that manages files included or referenced in the webpage is also able to store cookies. All these cookies are called http header cookies. Another way of storing cookies is through JavaScript code contained or referenced in that page.</p>

<p>Each time the user requests a new page, the web server can receive the values of the cookies it previously set and return the page with content relating to these values. Similarly, JavaScript code is able to read a cookie belonging to its domain and perform an action accordingly.</p>

<h3><strong>What are the different types of cookies?</strong></h3>

<p>A cookie can be classified by its lifespan and the domain to which it belongs. By lifespan, a cookie is either a:</p>

<ul>
	<li><strong>session cookie </strong>which is erased when the user closes the browser <i>or</i></li>
	<li><strong>persistent cookie </strong>which remains on the user&#39;s computer/device for a pre-defined period of time.</li>
</ul>

<p>As for the domain to which it belongs, there are either:</p>

<ul>
	<li><strong>first-party cookies </strong>which are set by the web server of the visited page and share the same domain</li>
	<li><strong>third-party cookies</strong> stored by a different domain to the visited page&#39;s domain. This can happen when the webpage references a file, such as JavaScript, located outside its domain.</li>
</ul>
<span class=\"clear\">&nbsp;</span></div>

<table>
	<tbody>
		<tr>
			<td align=\"left\" valign=\"top\">
			<p><b>Browser</b></p>

			<p>&nbsp;</p>
			</td>
			<td align=\"left\" valign=\"top\">
			<p><b>Where to find information about controlling cookies</b></p>
			</td>
		</tr>
		<tr>
			<td align=\"left\" valign=\"top\">
			<p>Internet Explorer</p>
			</td>
			<td align=\"left\" valign=\"top\">
			<p><a href=\"http://support.microsoft.com/kb/278835\">http://support.microsoft.com/kb/278835</a></p>
			</td>
		</tr>
		<tr>
			<td align=\"left\" valign=\"top\">
			<p>Chrome</p>
			</td>
			<td align=\"left\" valign=\"top\"><a href=\"https://support.google.com/chrome/answer/95647?hl=en-GB\">https://support.google.com/chrome/answer/95647?hl=en-GB</a></td>
		</tr>
		<tr>
			<td align=\"left\" valign=\"top\">
			<p>Firefox</p>
			</td>
			<td align=\"left\" valign=\"top\"><a href=\"http://support.mozilla.org/en-US/kb/Clear%20Recent%20History\">http://support.mozilla.org/en-US/kb/Clear%20Recent%20History</a></td>
		</tr>
		<tr>
			<td align=\"left\" valign=\"top\">
			<p>Safari</p>
			</td>
			<td align=\"left\" valign=\"top\"><a href=\"http://support.apple.com/kb/PH5042\">http://support.apple.com/kb/PH5042</a></td>
		</tr>
		<tr>
			<td align=\"left\" valign=\"top\">
			<p>Opera</p>
			</td>
			<td align=\"left\" valign=\"top\"><a href=\"http://www.opera.com/browser/tutorials/security/privacy/\">http://www.opera.com/browser/tutorials/security/privacy/</a></td>
		</tr>
	</tbody>
</table>', 'use of cookies', 'use-of-cookies', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('6', '4', '', '', '_blank', '', '', '', '', '', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('124', '1', 'Empresa', '', '_blank', '', '', '<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaIntroduccioEmpresa\">
<h1>KOOBIN: T&#39;HO POSEM F&Agrave;CIL</h1>

<p>A Koobin t&rsquo;ho volem posar f&agrave;cil. Per aix&ograve;, hem desenvolupat una tecnologia innovadora i molt intu&iuml;tiva que permet els organitzadors d&rsquo;esdeveniments vendre i gestionar el ticketing sense intermediaris, d&rsquo;una manera eficient i totalment personalitzable.</p>

<p align=\"center\"><iframe allowfullscreen=\"\" frameborder=\"0\" height=\"360\" src=\"//www.youtube.com/embed/6dtCzm1Xg60\" width=\"640\"></iframe></p>

<p>M&eacute;s de deu anys d&rsquo;experi&egrave;ncia i una &agrave;mplia cartera de clients de tot tipus ens avalen. Perqu&egrave; som molt m&eacute;s que un partner tecnol&ograve;gic: a m&eacute;s de subministrar el software per a l&rsquo;autogesti&oacute; del ticketing, a Koobin oferim una &agrave;mplia oferta de serveis complementaris que s&rsquo;adapten a les teves necessitats.</p>
</div>
<img alt=\"KoobinEvents\" src=\"/admin/js/kcfinder/upload/images/koobin_panoramica.jpg\" /></div>
</div>

<div class=\"bg1 \" data-stellar-background-ratio=\"0.5\">&nbsp;</div>

<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaEquip\">
<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<h2>Un equip innovador i ple d&rsquo;experi&egrave;ncia</h2>

<p>A Koobin treballa un equip jove que incorpora l&rsquo;experi&egrave;ncia de professionals pioners en e-commerce. L&rsquo;any 2000 van crear una de les primeres reserves hoteleres per internet del mercat i, el 2002, un dels primers software de gesti&oacute; d&rsquo;esdeveniments i venda d&rsquo;entrades i abonaments totalment on line.</p>

<p>Amb una &agrave;mplia traject&ograve;ria en companyies IT i de m&agrave;rqueting d&rsquo;esdeveniments, el nostre equip dedica els seus esfor&ccedil;os a desenvolupar millores cont&iacute;nues del seu sistema de gesti&oacute; del ticketing per adaptar-lo a les necessitats canviants de cada client. L&rsquo;objectiu: oferir un suport tecnol&ograve;gic efica&ccedil; i un servei proper i professional.</p>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Carles Arnal\" src=\"/admin/js/kcfinder/upload/images/carles_arnal_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Carles Arnal</div>

<div class=\"descEquip\">CEO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Fernando Martín\" src=\"/admin/js/kcfinder/upload/images/fernando_martin_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Fernando Mart&iacute;n</div>

<div class=\"descEquip\">Marketing Manager</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Christian Dávila\" src=\"/admin/js/kcfinder/upload/images/christian_davila_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Christian D&aacute;vila</div>

<div class=\"descEquip\">CTO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Carlos Díaz\" src=\"/admin/js/kcfinder/upload/images/carlos_diaz_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Carlos D&iacute;az</div>

<div class=\"descEquip\">Development</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Iván Oliva\" src=\"/admin/js/kcfinder/upload/images/ivan_oliva_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Iv&aacute;n Oliva</div>

<div class=\"descEquip\">Development</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Umberto Pensato\" src=\"/admin/js/kcfinder/upload/images/umbert_pensato_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Umbert Pensato</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Anna Arostegui\" src=\"/admin/js/kcfinder/upload/images/anna_arostegui_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Anna Arostegui</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Bernat Recasens\" src=\"/admin/js/kcfinder/upload/images/bernat_recasens_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Bernat Recasens</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Eduald Rossell\" src=\"/admin/js/kcfinder/upload/images/eduald_rossell_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Eudald Rossell</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Cristina Galera\" src=\"/admin/js/kcfinder/upload/images/cristina_galera_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Cristina Galera</div>

<div class=\"descEquip\">Support</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Llorenç Noell\" src=\"/admin/js/kcfinder/upload/images/llorenc_noell_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Lloren&ccedil; Noell</div>

<div class=\"descEquip\">CFO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Maia Kabramova\" src=\"/admin/js/kcfinder/upload/images/maia_kabramova_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Maia Kabramova</div>

<div class=\"descEquip\">Administration</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Laura Llovera\" src=\"/admin/js/kcfinder/upload/images/laura_llovera_koobin%281%29.jpg\" /></div>

<div class=\"nomEquip\">Laura Llovera&nbsp;</div>

<div class=\"descEquip\">Customer Support</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"David Asensio\" src=\"/admin/js/kcfinder/upload/images/david_asensio_koobin%281%29.jpg\" /></div>

<div class=\"nomEquip\">David Asensio</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">&nbsp;</div>

<div class=\"columns large-3 medium-3 small-6\">&nbsp;</div>
</div>
</div>
</div>
</div>

<div class=\"bg2 \" data-stellar-background-ratio=\"0.4\">&nbsp;</div>

<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaExp\">
<h2>Vocaci&oacute; internacional</h2>

<p>A Koobin tenim una llarga experi&egrave;ncia en la gesti&oacute; d&rsquo;esdeveniments d&rsquo;&agrave;mbit internacional, com ara la Final Four de l&rsquo;Eurolliga de 2012 a Istanbul.</p>

<p>La nostra plataforma es pot gestionar en diversos idiomes (catal&agrave;, castell&agrave;, angl&egrave;s i itali&agrave;), tot i que el sistema pot traduir-se a la idioma que millor s&rsquo;adapti a les necessitats del client.</p>

<p>A m&eacute;s, el web p&uacute;blic de Koobin es troba a l&rsquo;abast d&rsquo;usuaris de diferents proced&egrave;ncies, ja que est&agrave; disponible en catal&agrave;, castell&agrave;, angl&egrave;s, itali&agrave;, franc&egrave;s, alemany i turc.</p>
</div>
</div>
</div>

<div class=\"bg3\" data-stellar-background-ratio=\"0.7\">&nbsp;</div>', 'Empresa', 'empresa', '', 'A Koobin treballa un equip jove que incorpora l’experiència de professionals pioners en e-commerce. L’any 2000 van crear una de les primeres reserves hoteleres per internet del mercat i, el 2002, un dels primers software de gestió d’esdeveniments i venda d’entrades i abonaments totalment on line.');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('124', '2', 'Empresa', '', '_blank', '', '', '<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaIntroduccioEmpresa\">
<h1>KOOBIN: TE LO PONEMOS FACIL</h1>

<p>En Koobin te lo queremos poner f&aacute;cil. Por eso, hemos desarrollado una tecnolog&iacute;a innovadora y muy intuitiva que permite a los organizadores de eventos vender y gestionar el ticketing sin intermediarios, de manera eficiente y totalmente personalizable.</p>

<p align=\"center\"><iframe allowfullscreen=\"\" frameborder=\"0\" height=\"360\" src=\"//www.youtube.com/embed/6dtCzm1Xg60\" width=\"640\"></iframe></p>

<p>Mas de diez a&ntilde;os de experiencia y una amplia cartera de clientes de todo tipo no avalan. Porque somos mucho mas que un partner tecnol&oacute;gico: adem&aacute;s de suministrar el software para la autogesti&oacute;n del ticketing, en Koobin ofrecemos una amplia oferta de servicios complementarios que se adaptan a tus necesidades.</p>
</div>
<img alt=\"KoobinEvents\" src=\"/admin/js/kcfinder/upload/images/koobin_panoramica.jpg\" /></div>
</div>

<div class=\"bg1 \" data-stellar-background-ratio=\"0.5\">&nbsp;</div>

<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaEquip\">
<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<h2>UN EQUIPO INNOVADOR Y LLENO DE EXPERIENCIA</h2>

<p>En Koobin trabaja un equipo joven que incorpora la experiencia de profesionales pioneros en e-commerce. El a&ntilde;o 2000 crearon una de las primeras reservas hoteleras por internet del mercado y, el 2002 uno de los primeros software de gesti&oacute;n de eventos y venta de entradas i abonos totalmente online.</p>

<p>Con una amplia trayectoria en compa&ntilde;&iacute;as IT y de marketing de eventos, nuestro equipo dedica sus esfuerzos a desarrollar mejoras continuas de su sistema de gesti&oacute;n del ticketing para adaptarlo a las necesidades cambiantes de cada cliente. El objetivo: ofrecer un soporte tecnol&oacute;gico eficaz y un servicio pr&oacute;ximo y profesional.</p>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Carles Arnal\" src=\"/admin/js/kcfinder/upload/images/carles_arnal_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Carles Arnal</div>

<div class=\"descEquip\">CEO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Fernando Martín\" src=\"/admin/js/kcfinder/upload/images/fernando_martin_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Fernando Mart&iacute;n</div>

<div class=\"descEquip\">Marketing Manager</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Christian Dávila\" src=\"/admin/js/kcfinder/upload/images/christian_davila_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Christian D&aacute;vila</div>

<div class=\"descEquip\">CTO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Carlos Díaz\" src=\"/admin/js/kcfinder/upload/images/carlos_diaz_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Carlos D&iacute;az</div>

<div class=\"descEquip\">Development</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Iván Oliva\" src=\"/admin/js/kcfinder/upload/images/ivan_oliva_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Iv&aacute;n Oliva</div>

<div class=\"descEquip\">Development</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Umberto Pensato\" src=\"/admin/js/kcfinder/upload/images/umbert_pensato_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Umbert Pensato</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Anna Arostegui\" src=\"/admin/js/kcfinder/upload/images/anna_arostegui_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Anna Arostegui</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Bernat Recasens\" src=\"/admin/js/kcfinder/upload/images/bernat_recasens_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Bernat Recasens</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Eduald Rossell\" src=\"/admin/js/kcfinder/upload/images/eduald_rossell_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Eudald Rossell</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Cristina Galera\" src=\"/admin/js/kcfinder/upload/images/cristina_galera_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Cristina Galera</div>

<div class=\"descEquip\">Support</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Llorenç Noell\" src=\"/admin/js/kcfinder/upload/images/llorenc_noell_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Lloren&ccedil; Noell</div>

<div class=\"descEquip\">CFO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Maia Kabramova\" src=\"/admin/js/kcfinder/upload/images/maia_kabramova_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Maia Kabramova</div>

<div class=\"descEquip\">Administration</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Laura Llovera\" src=\"/admin/js/kcfinder/upload/images/laura_llovera_koobin%281%29.jpg\" /></div>

<div class=\"nomEquip\">Laura Llovera&nbsp;</div>

<div class=\"descEquip\">Customer Support</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"David Asensio\" src=\"/admin/js/kcfinder/upload/images/david_asensio_koobin%281%29.jpg\" /></div>

<div class=\"nomEquip\">David Asensio</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">&nbsp;</div>

<div class=\"columns large-3 medium-3 small-6\">&nbsp;</div>
</div>
</div>
</div>
</div>

<div class=\"bg2 \" data-stellar-background-ratio=\"0.4\">&nbsp;</div>

<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaExp\">
<h2>Vocaci&oacute; internacional</h2>

<p>En Koobin tenemos una larga experiencia en la gesti&oacute;n de eventos de &aacute;mbito internacional, como la Final Four de la Euroliga de 2012 en Estambul.</p>

<p>Nuestra plataforma se puede gestionar en varios idiomas (catal&aacute;n, castellano, ingl&eacute;s e italiano), aunque el sistema puede traducirse en la idioma que mejor se adapte a las necesidades del cliente.</p>

<p>Adem&aacute;s, la web p&uacute;blica de Koobin se encuentra al alcance de usuarios de diferentes procedencias, ya que est&aacute; disponible en catal&aacute;n, castellano, ingl&eacute;s, italiano, franc&eacute;s, alem&aacute;n y turco.</p>
</div>
</div>
</div>

<div class=\"bg3\" data-stellar-background-ratio=\"0.7\">&nbsp;</div>', 'Empresa', 'empresa', '', 'En Koobin trabaja un equipo joven que incorpora la experiencia de profesionales pioneros en e-commerce. El año 2000 crearon una de las primeras reservas hoteleras por internet del mercado y, el 2002 uno de los primeros software de gestión  de eventos y venta de entradas i abonos totalmente online.');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('124', '3', 'Company', '', '_blank', '', '', '<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaIntroduccioEmpresa\">
<h1>WE PUT IT EASY</h1>

<p>In Koobin you want to put it easy. Therefore, we have developed an innovative and intuitive technology that enables event organizers manage ticketing and sell without intermediaries, efficiently and fully customizable.</p>

<p align=\"center\"><iframe allowfullscreen=\"\" frameborder=\"0\" height=\"360\" src=\"//www.youtube.com/embed/6dtCzm1Xg60\" width=\"640\"></iframe></p>

<p>More than ten years of experience and a broad portfolio of clients of all types do not support. Because we&#39;re much more than a technology partner: in addition to providing the software for self ticketing in Koobin offer a wide range of additional services to suit your needs.</p>
</div>
<img alt=\"KoobinEvents\" src=\"/admin/js/kcfinder/upload/images/koobin_panoramica.jpg\" /></div>
</div>

<div class=\"bg1 \" data-stellar-background-ratio=\"0.5\">&nbsp;</div>

<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaEquip\">
<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<h2>AN INNOVATIVE AND FULL TEAM OF EXPERIENCE</h2>

<p>In Koobin works a young team that incorporates the experience of pioneering professionals in e-commerce. 2000 created one of the first online hotel reservations market, and 2002 one of the first software event management and ticketing i subscriptions entirely online.</p>

<p>With extensive experience in IT companies and event marketing, our team devotes its efforts to develop continuous improvement of its ticketing management system to suit the changing needs of each client. The goal: to provide an effective technological support and the near and professional service.</p>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Carles Arnal\" src=\"/admin/js/kcfinder/upload/images/carles_arnal_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Carles Arnal</div>

<div class=\"descEquip\">CEO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Fernando Martín\" src=\"/admin/js/kcfinder/upload/images/fernando_martin_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Fernando Mart&iacute;n</div>

<div class=\"descEquip\">Marketing Manager</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Christian Dávila\" src=\"/admin/js/kcfinder/upload/images/christian_davila_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Christian D&aacute;vila</div>

<div class=\"descEquip\">CTO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Carlos Díaz\" src=\"/admin/js/kcfinder/upload/images/carlos_diaz_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Carlos D&iacute;az</div>

<div class=\"descEquip\">Development</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Iván Oliva\" src=\"/admin/js/kcfinder/upload/images/ivan_oliva_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Iv&aacute;n Oliva</div>

<div class=\"descEquip\">Development</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Umberto Pensato\" src=\"/admin/js/kcfinder/upload/images/umbert_pensato_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Umbert Pensato</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Anna Arostegui\" src=\"/admin/js/kcfinder/upload/images/anna_arostegui_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Anna Arostegui</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Bernat Recasens\" src=\"/admin/js/kcfinder/upload/images/bernat_recasens_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Bernat Recasens</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Eduald Rossell\" src=\"/admin/js/kcfinder/upload/images/eduald_rossell_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Eudald Rossell</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Cristina Galera\" src=\"/admin/js/kcfinder/upload/images/cristina_galera_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Cristina Galera</div>

<div class=\"descEquip\">Support</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Llorenç Noell\" src=\"/admin/js/kcfinder/upload/images/llorenc_noell_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Lloren&ccedil; Noell</div>

<div class=\"descEquip\">CFO</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Maia Kabramova\" src=\"/admin/js/kcfinder/upload/images/maia_kabramova_koobin_01.jpg\" /></div>

<div class=\"nomEquip\">Maia Kabramova</div>

<div class=\"descEquip\">Administration</div>
</div>
</div>
</div>

<div class=\"row\">
<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"Laura Llovera\" src=\"/admin/js/kcfinder/upload/images/laura_llovera_koobin%281%29.jpg\" /></div>

<div class=\"nomEquip\">Laura Llovera&nbsp;</div>

<div class=\"descEquip\">Customer Support</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">
<div class=\"contentEquip\">
<div class=\"imatgeEquip\"><img alt=\"David Asensio\" src=\"/admin/js/kcfinder/upload/images/david_asensio_koobin%281%29.jpg\" /></div>

<div class=\"nomEquip\">David Asensio</div>

<div class=\"descEquip\">Programming</div>
</div>
</div>

<div class=\"columns large-3 medium-3 small-6\">&nbsp;</div>

<div class=\"columns large-3 medium-3 small-6\">&nbsp;</div>
</div>
</div>
</div>
</div>

<div class=\"bg2 \" data-stellar-background-ratio=\"0.4\">&nbsp;</div>

<div class=\"row\">
<div class=\"columns large-12 medium-12 small-12\">
<div class=\"aguantaExp\">
<h2>INTERNATIONAL CALLING</h2>

<p>In Koobin have extensive experience in managing international events such as the Final Four Euroleague 2012 in Istanbul.</p>

<p>Our platform can be managed in different languages (Catalan, Spanish, English and Italian), although the system can be translated to the language that best suits the client&#39;s needs.</p>

<p>In addition, the public website of Kooba is available to users from different backgrounds, as is available in Catalan, Spanish, English, Italian, French, German and Turkish.</p>
</div>
</div>
</div>

<div class=\"bg3\" data-stellar-background-ratio=\"0.7\">&nbsp;</div>', 'Company', 'company', '', 'In Koobin works a young team that incorporates the experience of pioneering professionals in e-commerce. 2000 created one of the first online hotel reservations market, and 2002 one of the first software event management and ticketing i subscriptions entirely online.');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('124', '4', '', '', '_blank', '', '', '', '', '', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('125', '1', 'Serveis', '', '_blank', '', '', '', 'Serveis', 'serveis', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('125', '2', 'Servicios', '', '_blank', '', '', '', 'Servicios', 'servicios', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('125', '3', 'Services', '', '_blank', '', '', '', 'Services', 'services', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('126', '1', 'Software', '', '_blank', '', '', '<h1>Software de gesti&oacute; i venda online d&rsquo;esdeveniments</h1>

<h2>Una plataforma &uacute;nica sempre amb les darreres innovacions<br />
&nbsp;</h2>

<p><img alt=\"\" src=\"/admin/js/kcfinder/upload/images/koobin_software_02%282%29.png\" /></p>

<p><br />
A Koobin hem desenvolupat l&rsquo;&uacute;nic software de gesti&oacute; i venda on line d&rsquo;esdeveniments d&rsquo;Espanya basat en la plataforma SaaS (Software as a Service). D&rsquo;aquesta manera, els nostres clients disminueixen les seves despeses i aconsegueixen en tot moment la flexibilitat i el suport professional que necessiten.</p>

<p>A m&eacute;s, els nostres professionals desenvolupen millores tecnol&ograve;giques cont&iacute;nues del sistema per adaptar-se a les novetats del mercat, per&ograve; tamb&eacute; a les necessitats reals dels clients.</p>

<div class=\"row\">
<div class=\"columns large-6 medium-6 small-12\">
<h3>Servidors autoescalables: Sense l&iacute;mit de dades</h3>

<p>Per garantir la m&agrave;xima fiabilitat, a Koobin utilitzem l&rsquo;allotjament web d&rsquo;AWS (Amazon Web Services), considerat un dels millors data centers a l&rsquo;&agrave;mbit europeu. Aquest servidor autoescalable permet oferir m&eacute;s prestacions a m&eacute;s demanda. Sense l&iacute;mit de dades. Aix&iacute;, els usuaris poden comprar sempre les entrades al moment, encara que hi hagi molta demanda. Sense cues ni temps d&rsquo;espera!</p>
</div>

<div class=\"columns large-6 medium-6 small-12\">
<h3>Nom&eacute;s pagues pel que necessites</h3>

<p>Som l&rsquo;&uacute;nic prove&iuml;dor que factura nom&eacute;s pel servei ofert i no pel nombre d&rsquo;entrades venudes. El resultat: t&rsquo;oferim el servei que necessites amb una excel&middot;lent relaci&oacute; qualitat / preu.</p>
</div>
</div>

<h2><br />
Tot el que Koobin et pot oferir:</h2>

<h3>Servei de venda:</h3>

<ul class=\"square\">
	<li>Venda d&rsquo;entrades i abonaments.</li>
	<li>Venda de paquets i serveis extres (marxandatge, sopars, c&agrave;tering, copes de cava, hotels, vols...).</li>
	<li>Integraci&oacute; amb diversos canals de distribuci&oacute;: internet, taquilles, punts de venda, xarxes socials.</li>
</ul>

<h3>Tecnologia adaptada a les necessitats reals:</h3>

<ul class=\"square\">
	<li>Integraci&oacute; total amb el web del client, tant pel que fa al disseny com a l&rsquo;estructura.</li>
	<li>Disseny responsiu: permet que els continguts del web s&rsquo;adaptin de manera &ograve;ptima a diferents dispositius m&ograve;bils.</li>
	<li>Empresa pionera a Espanya del sistema <em>Print at Home</em>.</li>
	<li>Integraci&oacute; amb aplicacions com <em>Passbook</em> d&rsquo;Apple per a l&rsquo;emmagatzematge de l&rsquo;entrada.</li>
	<li>Integraci&oacute; en xarxes socials, que permet la compra en grup i un efecte viral</li>
	<li><em>Partner </em>tecnol&ograve;gic: a Koobin mantenim una estreta relaci&oacute; amb els nostres clients i adaptem el nostre producte a les seves necessitats.</li>
</ul>

<h3>Con&egrave;ixer la demanda per augmentar les vendes</h3>

<ul class=\"square\">
	<li>La Gesti&oacute; de les Relacions amb el Client (CRM) permet con&egrave;ixer les necessitats dels teus clients, fet que facilita la segmentaci&oacute; de l&rsquo;oferta i la fidelitzaci&oacute; de l&rsquo;audi&egrave;ncia.</li>
	<li>Formularis de clients personalitzables: transforma les dades en informaci&oacute; valuosa, com hist&ograve;ric de compres i assist&egrave;ncia als esdeveniments.</li>
	<li>Sistema integrat amb serveis d&rsquo;enviament i gesti&oacute; de newsletters, com Mailchimp o Axonmail.</li>
	<li>Validaci&oacute; on line per a col&middot;lectius: Carnet Jove, Club Super3, Subscriptors de La Vanguardia, Promoentrada VIP...</li>
	<li>Gesti&oacute; de col&middot;lectius i socis amb venda preferent.</li>
	<li>Integraci&oacute; a altres plataformes de venda d&rsquo;entrades (TeleEntrada, Ticketmaster&hellip;).</li>
</ul>', 'Software', 'software', '', 'Koobin ha desarrollado el único software de gestión y venta online de eventos de España basado en la plataforma SaaS (Software as a Service). De esta manera, nuestros clientes disminuyen sus gastos y consiguen en todo momento la flexibilidad y el apoyo profesional que necesitan.');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('126', '2', 'Software', '', '_blank', '', '', '<h1>Software de gesti&oacute;n y venta online de eventos</h1>

<h2>Una plataforma &uacute;nica siempre con las &uacute;ltimas innovaciones<br />
&nbsp;</h2>

<p><img alt=\"\" src=\"/admin/js/kcfinder/upload/images/koobin_software_02%282%29.png\" /></p>

<p><br />
En Koobin hemos desarrollado el &uacute;nico software de gesti&oacute;n y venta on line de eventos de Espa&ntilde;a basado en la plataforma SaaS (Software as a Service). De esta manera, nuestros clientes disminuyen sus gastos y consiguen en todo momento la flexibilidad y el apoyo profesional que necesitan.</p>

<p>Adem&aacute;s, nuestros profesionales desarrollan mejoras tecnol&oacute;gicas continuas del sistema para adaptarse a las novedades del mercado, pero tambi&eacute;n a las necesidades reales de los clientes.</p>

<div class=\"row\">
<div class=\"columns large-6 medium-6 small-12\">
<h3>Servidores autoescalables: Sin l&iacute;mite de datos</h3>

<p>Para garantizar la m&aacute;xima fiabilidad, en Koobin utilizamos el alojamiento web de AWS (Amazon Web Services), considerado uno de los mejores data centers en el &aacute;mbito europeo. Este servidor autoescalable permite ofrecer m&aacute;s prestaciones a m&aacute;s demanda. Sin l&iacute;mite de datos. As&iacute;, los usuarios pueden comprar siempre las entradas al momento, aunque haya mucha demanda. Sin colas ni tiempo de espera!</p>
</div>

<div class=\"columns large-6 medium-6 small-12\">
<h3>S&oacute;lo pagas por lo que necesitas</h3>

<p>Somos el &uacute;nico proveedor que factura s&oacute;lo por el servicio ofrecido y no por el n&uacute;mero de entradas vendidas. El resultado: te ofrecemos el servicio que necesitas con una excelente relaci&oacute;n calidad / precio.</p>
</div>
</div>

<h2><br />
Todo lo que Koobin te puede ofrecer:</h2>

<h3>Servicio de venta:</h3>

<ul class=\"square\">
	<li>Venta de entradas y abonos.</li>
	<li>Venta de paquetes y servicios extras (merchandising, cenas, catering, copas de cava, hoteles, vuelos ...).</li>
	<li>Integraci&oacute;n con varios canales de distribuci&oacute;n: internet, taquillas, puntos de venta, redes sociales.</li>
</ul>

<h3>Tecnolog&iacute;a adaptada a las necesidades reales:</h3>

<ul class=\"square\">
	<li>Integraci&oacute;n total con la web del cliente, tanto en cuanto al dise&ntilde;o como a la estructura.</li>
	<li>Dise&ntilde;o responsivo: permite que los contenidos de la web se adapten de manera &oacute;ptima a diferentes dispositivos m&oacute;viles.</li>
	<li>Empresa pionera en Espa&ntilde;a del sistema <em>Print at Home</em>.</li>
	<li>Integraci&oacute;n con aplicaciones como <em>Passbook</em> de Apple para el almacenamiento de la entrada</li>
	<li>Integraci&oacute;n en redes sociales, que permite la compra en grupo y un efecto viral</li>
	<li><em>Partner </em>tecnol&oacute;gico: a Koobi mantenemos una estrecha relaci&oacute;n con nuestros clientes y adaptamos nuestro producto a sus necesidades.</li>
</ul>

<h3>Conocer la demanda para aumentar las ventas</h3>

<ul class=\"square\">
	<li>La Gesti&oacute;n de las Relaciones con el Cliente (CRM) permite conocer las necesidades de tus clientes, lo que facilita la segmentaci&oacute;n de la oferta y la fidelizaci&oacute;n de la audiencia.</li>
	<li>Formularios de clientes personalizables: transforma los datos en informaci&oacute;n valiosa, como hist&oacute;rico de compras y asistencia a los eventos.</li>
	<li>Sistema integrado con servicios de env&iacute;o y gesti&oacute;n de newsletters, como MailChimp o Axonmail.</li>
	<li>Validaci&oacute;n on line para colectivos: Carnet Joven, Club Super 3, Suscriptores de La Vanguardia, Promoentrada VIP ...</li>
	<li>Gesti&oacute;n de colectivos y socios con venta preferente.</li>
	<li>Integraci&oacute;n en otras plataformas de venta de entradas (TeleEntrada, Ticketmaster ...).</li>
</ul>', 'Software', 'software', '', 'En Koobin hemos desarrollado el único software de gestión y venta on line de eventos de España basado en la plataforma SaaS (Software as a Service). De esta manera, nuestros clientes disminuyen sus gastos y consiguen en todo momento la flexibilidad y el apoyo profesional que necesitan.');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('126', '3', 'Software', '', '_blank', '', '', '<h1>Sofware management and online sales events</h1>

<h2>A single platform provided with the latest innovations<br />
&nbsp;</h2>

<p><img alt=\"\" src=\"/admin/js/kcfinder/upload/images/koobin_software_02%282%29.png\" /></p>

<p><br />
In Koobin we have developed unique software management and online sales events in Spain based SaaS platform (Software as a Service). Thus, our customers reduce their costs and achieve at all times the flexibility and the professional support they need.</p>

<p>Furthermore, our professionals develop continuous technological improvements of the system to adapt to market developments, but also to the real needs of customers.</p>

<div class=\"row\">
<div class=\"columns large-6 medium-6 small-12\">
<h3>Autoescalables Servers: Unlimited data</h3>

<p>To ensure maximum reliability, to use the web hosting Koobi AWS (Amazon Web Services), considered one of the best data centers in Europe. This self-scalable server can offer more benefits to more demand. Unlimited data. Thus, users can always buy tickets at the time, although there is much demand. No queues or waiting times!</p>
</div>

<div class=\"columns large-6 medium-6 small-12\">
<h3>You only pay for what you need</h3>

<p>We are the only provider to bill only for the service provided and not by the number of tickets sold. The result: we offer you the service you need with an excellent quality / price ratio.</p>
</div>
</div>

<h2><br />
All I can offer Koobin:</h2>

<h3>Service Sales:</h3>

<ul class=\"square\">
	<li>Ticket and subscriptions</li>
	<li>Sales packages and extras (merchandising, dinner, catering, champagne glasses, hotels, flights ...)</li>
	<li>Integration with various distribution channels: internet, lockers, retail, social networking</li>
</ul>

<h3>Technology adapted to the real needs:</h3>

<ul class=\"square\">
	<li>Full integration with the client&#39;s website in both the design and the structure</li>
	<li>Responsive Design: allows website content optimally adapt to different mobile devices</li>
	<li>The pioneer in Spain Print at Home System</li>
	<li>Integration with applications like Apple Passbook for storing input</li>
	<li>Integration with social networks, allowing the group purchase and a viral effect</li>
	<li>Technological partner: a Koobin maintain a close relationship with our customers and adapt our product to your needs.</li>
</ul>

<h3>Meet the demand to increase sales:</h3>

<ul class=\"square\">
	<li>Management of Customer Relationship (CRM) can meet the needs of your customers, which makes the segmentation of supply and audience loyalty</li>
	<li>Customizable forms customers: transforms the data as valuable historical purchasing information and event attendance</li>
	<li>Integrated shipping services and managing newsletters, as MailChimp or Axonmail System</li>
	<li>Validation on line for groups: Youth Card, Club Super 3 Subscribers La Vanguardia, Promoentrada VIP ...</li>
	<li>Managing groups and partners prior sale</li>
	<li>Integration into other ticketing platforms (TeleEntrada, Ticketmaster ...)</li>
</ul>', 'Software', 'software', '', 'In Koobin it has developed the only management software and online sales events de Espanya platform based on SaaS (Software as a Service). In this way, our customers reduce their costs and achieve at all times the flexibility and professional support they need.');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('127', '1', 'Gestió Ticketing', '', '_blank', '', '', '<h1>Servei de gesti&oacute; integral de ticketing</h1>

<p><img alt=\"Koobin\" src=\"/admin/js/kcfinder/upload/images/koobin_integral.jpg\" /></p>

<h2>Ens encarreguem de tot</h2>

<p>A Koobin ens encarreguem de tot perqu&egrave; no t&rsquo;hagis de preocupar de res. No importa com sigui de gran el teu esdeveniment, sempre trobar&agrave;s la soluci&oacute; que millor s&rsquo;adapta a les teves necessitats.</p>

<ul class=\"square\">
	<li>Plataforma SaaS (software as a Service) que permet la m&agrave;xima flexibilitat</li>
	<li>Aplicaci&oacute; de gesti&oacute; d&rsquo;esdeveniments 100% on line</li>
	<li>Gesti&oacute; d&rsquo;aforaments amb representaci&oacute; gr&agrave;fica real del recinte</li>
	<li>Gesti&oacute; de temporades i esdeveniments puntuals</li>
	<li>Gesti&oacute; de clients, CRM i comunicaci&oacute;</li>
	<li>Mapeig d&rsquo;aforaments</li>
	<li>Venda d&rsquo;abonaments, paquets, entrades i extres</li>
	<li>Venda multicanal: Internet, taquilla, punts de venda, xarxes socials, distribu&iuml;dors&hellip;</li>
	<li>Impressi&oacute;: Print-at-Home, Mobile Ticket, Passbook</li>
	<li>Cobrament on line amb el teu propi TPV</li>
</ul>

<h2>Serveis de suport a la gesti&oacute; d&rsquo;esdeveniments</h2>

<p>Una bona gesti&oacute; &eacute;s b&agrave;sica per a l&rsquo;&egrave;xit d&rsquo;un esdeveniment. Per aix&ograve;, a Koobin oferim tamb&eacute; la possibilitat de contractar serveis puntuals que garanteixen els millors resultats:</p>

<ul class=\"square\">
	<li>Consultoria de gesti&oacute; d&rsquo;esdeveniments</li>
	<li>Suport en la gesti&oacute; de temporades i abonaments</li>
	<li>Yield Management, per a una tarifaci&oacute; en temps real, que adapta el preu a la demanda existent</li>
	<li>M&agrave;rqueting on line</li>
	<li>Integraci&oacute; a mida del ticketing on line en el web del client amb total seguretat</li>
	<li>Impressi&oacute; i enviament</li>
	<li>Control d&rsquo;accessos</li>
	<li>Control de venedors, caixer, porters i arqueig</li>
	<li>Control d&rsquo;assist&egrave;ncia en temps real (numerats i/o no numerats)</li>
</ul>

<div class=\"panel\">
<p>A Koobin ja apliquem amb &egrave;xit les nostres solucions tecnol&ograve;giques i de servei en casos molt diversos: el festival GREC De Barcelona, Final Four Barcelona 2011, Copa del Rey Barcelona 2012, Final Futsal Cup Lleida 2012, Final Four Istanbul 2012, Copa del Rey Vitoria 2013 i Copa del Rey M&agrave;laga 2014, entre d&rsquo;altres.</p>
</div>', 'Gestió Tiketing', 'gestio-tiketing', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('127', '2', 'Gestión Ticketing', '', '_blank', '', '', '<h1>Servicio de gesti&oacute;n integral de ticketing</h1>

<p><img alt=\"Koobin\" src=\"/admin/js/kcfinder/upload/images/koobin_integral.jpg\" /></p>

<h2>Nos encargamos de todo</h2>

<p>En Koobin nos encargamos de todo para que no te hayas de preocupar por nada. No importa como de grande sea tu evento, siempre encontrar&aacute;s la soluci&oacute;n que mejor se adapte a tus necesidades.</p>

<ul class=\"square\">
	<li>Plataforma SaaS (Software as a Service) que permite la m&aacute;xima flexibilidad.</li>
	<li>Aplicaci&oacute;n de gesti&oacute;n de eventos 100% online.</li>
	<li>Gesti&oacute;n de aforos con representaci&oacute;n gr&aacute;fica real del recinto.</li>
	<li>Gesti&oacute;n de temporadas y eventos puntuales.</li>
	<li>Gesti&oacute;n de clientes, CRM i comunicaci&oacute;n.</li>
	<li>Mapeo de aforos.</li>
	<li>Venta de abonos, paquetes, entradas y extras.</li>
	<li>Venta multicanal: Internet, taquilla, puntos de venta, redes sociales, distribuidores&hellip;</li>
	<li>Impresi&oacute;n: Print-at-Home, Mobile Ticket, Passbook.</li>
	<li>Cobro online con tu propio TPV.</li>
</ul>

<h2>Servicios de soporte a la gesti&oacute;n de eventos</h2>

<p>Una buena gesti&oacute;n es fundamental para el &eacute;xito de un evento. Por eso, en Koobin ofrecemos tambi&eacute;n la posibilidad de contratar servicios puntuales que garanticen los mejores resultados:</p>

<ul class=\"square\">
	<li>Consultor&iacute;a de gesti&oacute;n de eventos.</li>
	<li>Soporte en la gesti&oacute;n de temporadas y abonos.</li>
	<li>Yield Management, para una tarifaci&oacute;n en tiempo real, que adapta el precio a la demanda existente.</li>
	<li>Marketing online.</li>
	<li>Integraci&oacute;n ajustada del ticketing online en el web del cliente con total seguridad.</li>
	<li>Impresi&oacute;n y env&iacute;o.</li>
	<li>Control de accesos.</li>
	<li>Control de vendedores, cajeros, porteros y arqueo.</li>
	<li>Control de asistencia en tiempo real (numerados y/o no numerados).</li>
</ul>

<div class=\"panel\">
<p>En Koobin ya aplicamos con &eacute;xito nuestras soluciones tecnol&oacute;gicas y de servicio en casos muy diversos: el festival GREC De Barcelona, Final Four Barcelona 2011,&nbsp; Copa del Rey Barcelona 2012, Final Futsal Cup Lleida 2012, Final Four Istanbul 2012, Copa del Rey Vitoria 2013 y Copa del Rey M&aacute;laga 2014, entre otros.</p>
</div>', 'Gestión Ticketing', 'gestion-ticketing', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('127', '3', 'Ticketing management', '', '_blank', '', '', '<h1>Comprehensive management service ticketing</h1>

<p><img alt=\"Koobin\" src=\"/admin/js/kcfinder/upload/images/koobin_integral.jpg\" /></p>

<h2>We handle everything</h2>

<p>In Koobin take care of everything so you do not have to worry about anything. No matter how great your event, you will always find the solution that best suits your needs.&nbsp;</p>

<ul class=\"square\">
	<li>Platform SaaS (Software as a Service) that allows for maximum flexibility.&nbsp;</li>
	<li>Event management application 100% online.&nbsp;</li>
	<li>Management gauging real graphical representation of the enclosure.&nbsp;</li>
	<li>Management seasons and specific events.&nbsp;</li>
	<li>Customer management, CRM i communication.&nbsp;</li>
	<li>Mapping capacities.&nbsp;</li>
	<li>Season tickets, packages, tickets and extras.&nbsp;</li>
	<li>Multichannel Sales: Internet, locker, retail, social networks, distributors ...&nbsp;</li>
	<li>Print: Print-at-Home, Mobile Ticket, Passbook.&nbsp;</li>
	<li>Collection online with your own POS.</li>
</ul>

<p>&nbsp;</p>

<h2>Support services to event management</h2>

<p>Good management is critical to the success of an event. So in Koobin also offer the possibility of contracting specific services to ensure the best results:&nbsp;</p>

<ul class=\"square\">
	<li>Event management consultancy.&nbsp;</li>
	<li>Support management and fertilizers seasons.&nbsp;</li>
	<li>Yield Management, for pricing in real time, adapting to the demand for money.&nbsp;</li>
	<li>Marketing online.&nbsp;</li>
	<li>Tight integration of online ticketing in the web client safely.&nbsp;</li>
	<li>Printing and postage.&nbsp;</li>
	<li>Access control.&nbsp;</li>
	<li>Control of salespeople, cashiers, janitors and tonnage.&nbsp;</li>
	<li>Control systems in real time (numbered and / or numbered).&nbsp;</li>
</ul>

<div class=\"panel\">
<p>In Koobin already successfully applied our technology and service solutions in a variety of cases: the GREC De Barcelona, Barcelona 2011 Final Four, 2012 Copa del Rey Barcelona, Lleida Futsal Cup Final 2012, Final Four Istanbul 2012 Cup festival Vitoria 2013 and Malaga Copa del Rey 2014, among others.</p>
</div>', 'Ticketing management', 'ticketing-management', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('128', '1', 'Per què Koobin', '', '_blank', '', '', '<h1>
  10 raons per escollir Koobin
</h1>
<p>
  <img alt=\"logo koobin\" src=\"/admin/js/kcfinder/upload/images/koobin_10.jpg\" />
</p>
  <br />
<ul class=\"ulNumeric\">
  <li>Partner tecnol&ograve;gic que ofereix una innovaci&oacute; cont&iacute;nua en una aplicaci&oacute; 100 % on line</li>
  <li>Soluci&oacute; totalment independent, sense intermediaris en el negoci</li>
  <li>Plataforma SaaS (Software as a Service): el client contracta un software i un servei, no paga per entrada venuda</li>
  <li>Gesti&oacute; completa de tot els de canals de venda</li>
  <li>Un dels sistemes m&eacute;s complets de gesti&oacute; de venda, renovaci&oacute; i millora d&rsquo;abonaments on line</li>
  <li>Venda d&rsquo;entrades i paquets &ldquo;Print-at-Home&rdquo; i &ldquo;Mobile tickets&rdquo; per a dispositius m&ograve;bils</li>
  <li>Integraci&oacute; total amb apps tipus &ldquo;Passbook&rdquo;</li>
  <li>Un ampli ventall d&rsquo;eines per fer promocions i incrementar els ingressos</li>
  <li>Allotjament web molt fiable (AWS): sense cues d&rsquo;espera</li>
  <li>Sistema totalment adaptable al web del client i disseny adaptat a dispositius m&ograve;bils</li>
</ul>', 'Per què Koobin', 'per-que-koobin', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('128', '2', '¿Por qué Koobin?', '', '_blank', '', '', '<h1>
  10 razones para escojer Koobin
</h1>
<p>
  <img alt=\"logo koobin\" src=\"/admin/js/kcfinder/upload/images/koobin_10.jpg\" />
</p>
  <br />
<ul class=\"ulNumeric\">
  <li>Partner tecnol&oacute;gico que ofrece una innovaci&oacute;n continua en una aplicaci&oacute;n 100 % on line.</li>
  <li>Soluci&oacute;n totalmente independiente, sin intermediarios en el negocio.</li>
  <li>Plataforma SaaS (Software as a Service): el cliente contracta un software y un servicio, no paga por entrada vendida.</li>
  <li>Gesti&oacute;n completa de todos los canales de venta.</li>
  <li>Uno de los sistemas mas completos de gesti&oacute;n de venta, renovaci&oacute;n y mejora de abonos on line.</li>
  <li>Venta de entradas y paquetes &ldquo;Print-at-Home&rdquo; y &ldquo;Mobile Tickets&rdquo; para dispositivos m&oacute;viles.</li>
  <li>Integraci&oacute;n total con apps tipo &ldquo;Passbook&rdquo;.</li>
  <li>Una amplia gamma de herramientas para hacer promociones e incrementar los ingresos.</li>
  <li>Alojamiento web muy fiable (AWS): sin colas de espera</li>
  <li>Sistema totalmente adaptable al web del cliente y dise&ntilde;o adaptado a dispositivos m&oacute;viles.</li>
</ul>', 'Por qué Koobin', 'por-que-koobin', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('128', '3', 'Why Koobin', '', '_blank', '', '', '<h1>
  10 reasons to choose Koobin
</h1>
<p>
  <img alt=\"logo koobin\" src=\"/admin/js/kcfinder/upload/images/koobin_10.jpg\" />
</p>
  <br />
<ul class=\"ulNumeric\">
  <li>Technology partner that provides continuous innovation in a 100 % online.&nbsp;</li>
  <li>Totally independent solution without intermediaries in the business.&nbsp;</li>
  <li>Platform SaaS (Software as a Service): contracta client software and service, do not pay ticket sold.&nbsp;</li>
  <li>Complete management of all sales channels.&nbsp;</li>
  <li>One of the most comprehensive retail management systems, renovation and improvement of online subscriptions.&nbsp;</li>
  <li>Ticket packages and &quot;Print-at-Home&quot; and &quot;Mobile Ticket&quot; for mobile devices.&nbsp;</li>
  <li>Full integration with apps like &quot;Passbook&quot;.&nbsp;</li>
  <li>A wide gamma of tools to make promotions and increase revenue.&nbsp;</li>
  <li>Very reliable web hosting (AWS): no queues&nbsp;</li>
  <li>System fully customizable to client&#39;s website and design adapted to mobile devices.</li>
</ul>', 'Why Koobin', 'why-koobin', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('129', '1', 'Clients', '', '_blank', 'llistats', 'llistat=1', '<h1>
  CLIENTS
</h1>
<h2>
  PORTAFOLI
</h2>
<p>
  Koobin ofereix els seus serveis a clients de tot tipus. Aqu&iacute; podeu trobar-ne una mostra.
</p>', 'Clients', 'clients', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('129', '2', 'Clientes', '', '_blank', 'llistats', 'llistat=1', '<h1>CLIENTES</h2>
<h2>PORTFOLIO</h2>
<p>
  Koobin ofrece sus servicios a todo tipo de clientes. Aqu&iacute; pod&eacute;is encontrar una muestra.
</p>', 'Clientes', 'clientes', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('129', '3', 'Customers', '', '_blank', 'llistats', 'llistat=1', '<h1>CUSTOMERS&nbsp;</h1>
<h2>PORTFOLIO&nbsp;</h2>
<p>
  Koobin offers its services to all types of customers. Here you can find a sample.
</p>', 'Customers', 'customers', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('131', '1', 'Premsa', '', '_blank', 'llistats', 'llistat=5', '<h1>KOOBIN</h1>

<h2>A LA PREMSA</h2>

<p>Aqu&iacute; trobareu tota la informaci&oacute; relativa a Koobin que ha aparegut a la premsa.</p>', 'Premsa', 'premsa', '', 'Aquí trobareu tota la informació relativa a Koobin que ha aparegut a la premsa');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('131', '2', 'Prensa', '', '_blank', 'llistats', 'llistat=5', '<h1>KOOBIN</h1>
<h2>EN LA PRENSA</h2>
<p>
  Aqu&iacute; encontrar&eacute;is toda la informaci&oacute;n relativa a Koobin que ha aparecido en la prensa
</p>', 'Prensa', 'prensa', '', 'Aquí encontraréis toda la información relativa a Koobin que ha aparecido en la prensa');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('131', '3', 'Press', '', '_blank', 'llistats', 'llistat=5', '<h1>KOOBIN</h1>

<h2>IN THE PRESS&nbsp;</h2>

<p>Here you will find all the information on Koobin that has appeared in the press</p>', 'Press', 'press', '', 'Here you will find all the information on Koobin that has appeared in the press');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('132', '1', 'Treball', '', '_blank', 'llistats', 'llistat=6', '<h2>
  VOLS FORMAR PART DE <b>KOOBIN</b>?
</h2>
<p>
  A Koobin som una empresa jove en plena expansi&oacute;. Si vols treballar amb nosaltres, consulta peri&ograve;dicament aquest apartat, en el qual trobar&agrave;s les nostres ofertes laborals.
</p>', 'Treball', 'treball', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('132', '2', 'Trabajo', '', '_blank', 'llistats', 'llistat=6', '<h2>
  ¿QUIERES FORMAR PARTE DE <strong>KOOBIN</strong>?
</h2>
<p>
  En Koobin somos una empresa joven en plena expansi&oacute;n. Si quieres trabajar con nosotros consulta peri&oacute;dicamente este apartado, en el que encontrar&aacute;s nuestras ofertas laborales.
</p>', 'Trabajo', 'trabajo', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('132', '3', 'Work', '', '_blank', 'llistats', 'llistat=6', '<h2>
  WANT TO BE PART&nbsp;<strong>KOOBIN</strong>?
</h2>
<p>
  In Koobin we are a young growing company. If you want to work with us regularly consults this section, where you&#39;ll find our job offers.
</p>', 'Job', 'work', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('133', '1', 'Accés a DEMO', '', '_blank', '', '', '', 'Accés a DEMO', 'acces-a-demo', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('133', '2', 'Acceso a DEMO', '', '_blank', '', '', '', 'Acceso a DEMO', 'acceso-a-demo', '', '');
insert into pages_description (pages_id, language_id, titol, link, target, modul, vars, content, title, page_title, keywords, description) values ('133', '3', 'Access DEMO', '', '_blank', '', '', '', 'Access DEMO', 'access-demo', '', '');
drop table if exists pages_history;
create table pages_history (
  id int(11) not null auto_increment,
  pages_id int(11) not null ,
  summary varchar(255) ,
  ip varchar(255) ,
  date datetime ,
  detail text ,
  systeminfo text ,
  PRIMARY KEY (id),
  KEY userididx (pages_id),
  KEY dateidx (date)
);

insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('466', '1', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2013-12-16 15:23:50', NULL, 'Mozilla/5.0 (X11; Linux i686; rv:25.0) Gecko/20100101 Firefox/25.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('467', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2013-12-16 16:06:01', NULL, 'Mozilla/5.0 (X11; Linux i686; rv:25.0) Gecko/20100101 Firefox/25.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('468', '97', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-02-06 11:47:25', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('469', '98', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-02-10 09:20:01', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('470', '99', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-02-12 17:27:05', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('471', '99', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-02-12 17:27:15', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('472', '100', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-03-14 12:59:43', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:27.0) Gecko/20100101 Firefox/27.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('473', '100', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-03-14 13:00:06', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:27.0) Gecko/20100101 Firefox/27.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('474', '100', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-03-14 13:02:37', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:27.0) Gecko/20100101 Firefox/27.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('475', '6', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-02 12:36:33', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('476', '6', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-02 12:38:55', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('477', '6', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-02 12:39:53', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('478', '101', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-07 13:14:31', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('479', '101', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-07 13:14:45', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('480', '101', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-07 13:16:16', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('481', '1', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-08 12:11:08', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('482', '1', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-08 12:11:37', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('483', '1', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-08 12:12:17', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('484', '99', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-08 12:21:28', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('485', '2', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-08 14:19:39', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('486', '100', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-08 14:36:21', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('487', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-04-08 14:40:52', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('488', '102', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-20 14:16:28', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('489', '102', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-20 14:17:00', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('490', '103', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 16:57:45', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('491', '104', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 16:58:34', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('492', '105', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 16:58:58', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('493', '106', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:02:16', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('494', '107', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:08:17', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('495', '1', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:08:41', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('496', '108', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:10:13', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('497', '109', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:15:26', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('498', '110', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:15:46', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('499', '111', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:16:23', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('500', '112', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:29:32', NULL, 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('501', '113', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:37:42', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('502', '114', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:37:58', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('503', '115', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:43:47', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('504', '116', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 17:46:05', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('505', '117', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:01:41', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('506', '118', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:15:51', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('507', '118', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:19:07', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('508', '118', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:19:15', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('509', '118', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:19:22', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('510', '119', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:22:11', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('511', '120', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:25:25', NULL, 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('512', '121', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:25:40', NULL, 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('513', '122', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-05-21 18:28:15', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('514', '123', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-13 14:07:13', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('515', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-13 14:08:19', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('516', '6', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-13 14:09:02', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('517', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-13 14:09:17', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('518', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-13 14:14:01', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('519', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-13 14:20:21', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('520', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-13 14:38:38', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('521', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-16 08:39:55', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('522', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-16 08:47:59', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('523', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-16 08:50:44', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('524', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-16 08:58:24', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('525', '123', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-16 09:14:02', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('526', '100', 'Desactive by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:11:18', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('527', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:12:00', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('528', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:12:18', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('529', '5', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:12:36', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('530', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:14:09', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('531', '4', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:14:25', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('532', '5', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:14:45', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('533', '6', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:15:04', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('534', '99', 'Desactive by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:15:18', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('535', '97', 'Desactive by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:15:27', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('536', '2', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:16:06', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('537', '2', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:27:51', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('538', '124', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:29:10', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('539', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:29:33', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('540', '125', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:35:01', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('541', '126', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:36:42', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('542', '127', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:37:18', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('543', '128', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:38:08', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('544', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:38:26', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('545', '129', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:39:12', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('546', '2', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:39:53', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('547', '130', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:40:18', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('548', '131', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:42:19', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('549', '132', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:43:14', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('550', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:43:32', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('551', '133', 'Insert by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:45:10', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('552', '133', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 13:45:42', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('553', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 14:17:26', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('554', '131', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 16:31:14', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('555', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-18 16:34:28', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('556', '1', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 09:18:15', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('557', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 11:34:28', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('558', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 12:04:35', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('559', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 12:07:19', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('560', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 12:10:31', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('561', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 12:12:47', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('562', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 13:21:43', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('563', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 13:22:09', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('564', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 13:43:30', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('565', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 13:44:52', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('566', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 13:45:35', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('567', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-19 13:46:04', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('568', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-20 10:42:53', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('569', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-20 10:49:40', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('570', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-20 17:58:16', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('571', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-20 18:01:15', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('572', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 10:26:15', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('573', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 10:58:14', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('574', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 14:13:15', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('575', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 14:18:14', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('576', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 14:19:23', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('577', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 14:44:34', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('578', '131', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 14:48:26', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('579', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 14:51:18', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('580', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 17:36:42', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('581', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 19:37:05', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('582', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 19:37:58', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('583', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 19:39:19', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('584', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 19:40:35', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('585', '4', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 19:43:26', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('586', '4', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-25 19:50:28', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('587', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 09:15:36', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('588', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 09:24:22', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('589', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 09:44:13', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('590', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 09:45:34', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('591', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 10:35:14', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('592', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 10:40:19', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('593', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 10:41:14', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('594', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 10:42:47', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('595', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 16:51:16', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('596', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 17:22:56', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('597', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 17:24:28', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('598', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 17:25:51', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('599', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 17:31:01', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('600', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 17:31:30', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('601', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 17:36:36', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('602', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 18:03:09', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('603', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 18:03:40', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('604', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 18:04:08', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('605', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 18:04:57', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('606', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 18:28:25', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('607', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 18:32:36', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('608', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-26 18:34:35', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('609', '133', 'Desactive by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 10:32:26', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('610', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 11:58:31', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('611', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:06:14', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('612', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:09:14', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('613', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:10:22', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('614', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:11:35', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('615', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:12:32', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('616', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:16:21', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('617', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:17:13', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('618', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 12:18:15', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('619', '131', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 14:07:39', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('620', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 14:07:54', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('621', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 15:55:08', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('622', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 15:58:43', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('623', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-27 16:08:16', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('624', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-30 10:29:42', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('625', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-06-30 13:00:30', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('626', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 10:31:22', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('627', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:45:25', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('628', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:46:33', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('629', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:47:05', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('630', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:51:03', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('631', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:51:24', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('632', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:51:39', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('633', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:53:32', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('634', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:53:51', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('635', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 11:56:11', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('636', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 12:11:07', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('637', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 12:14:41', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('638', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 12:53:03', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('639', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 13:35:11', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('640', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 13:38:08', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('641', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 13:41:46', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('642', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 16:15:16', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('643', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-01 17:55:03', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('644', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-29 12:03:37', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('645', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-30 16:25:22', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('646', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-30 16:27:34', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('647', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-30 16:29:23', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('648', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-30 16:34:53', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('649', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-30 16:35:54', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('650', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-07-30 16:36:23', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('651', '2', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-01 08:59:21', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('652', '131', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-01 09:00:23', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('653', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-01 09:01:31', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('654', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 09:43:48', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('655', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 09:47:01', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('656', '126', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:00:16', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('657', '127', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:09:14', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('658', '128', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:39:13', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('659', '133', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:41:03', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('660', '129', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:43:19', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('661', '2', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:44:34', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('662', '131', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:46:24', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('663', '131', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:50:42', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('664', '132', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:53:10', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('665', '3', 'Update by Koobin Editor', '84.89.61.129', '2014-08-06 10:54:36', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('666', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 09:58:30', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('667', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 09:59:50', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('668', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:01:17', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('669', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:01:42', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('670', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:01:54', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('671', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:02:08', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('672', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:02:45', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('673', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:03:20', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('674', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:04:40', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('675', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:05:25', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('676', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:06:00', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('677', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:07:04', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('678', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:08:27', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('679', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:09:03', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('680', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:10:21', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('681', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:10:44', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('682', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:11:10', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('683', '4', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:12:48', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('684', '4', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:15:52', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('685', '4', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 10:16:30', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('686', '123', 'Update by Koobin Editor', '84.89.61.129', '2014-08-07 12:39:02', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('687', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 07:47:26', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('688', '124', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 07:48:17', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('689', '125', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 07:49:31', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('690', '126', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 07:56:12', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('691', '126', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 07:57:31', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('692', '126', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 07:58:20', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('693', '127', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:10:51', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('694', '128', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:13:33', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('695', '133', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:14:11', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('696', '129', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:16:16', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('697', '2', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:18:05', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('698', '131', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:19:40', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('699', '132', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:21:27', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('700', '3', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:22:26', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('701', '5', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:23:32', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('702', '3', 'Update by Koobin Editor', '84.89.61.129', '2014-08-11 08:26:26', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('703', '131', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-18 12:59:30', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('704', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-18 13:01:25', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('705', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-18 13:02:00', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('706', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-18 13:17:08', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('707', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 10:21:31', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('708', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 10:38:21', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('709', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 10:42:17', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('710', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 10:47:07', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('711', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 10:50:21', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('712', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:03:23', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('713', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:06:22', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('714', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:11:45', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('715', '127', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:13:02', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('716', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:14:19', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('717', '128', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:16:19', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('718', '129', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:43:01', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('719', '131', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:44:11', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('720', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:44:54', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('721', '132', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:48:14', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('722', '2', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:51:04', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('723', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:54:35', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('724', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:56:02', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('725', '124', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 11:56:55', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('726', '126', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 12:05:34', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('727', '126', 'Update by Koobin Editor', '84.89.61.71', '2014-08-19 13:50:17', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('728', '127', 'Update by Koobin Editor', '84.89.61.71', '2014-08-19 14:09:33', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('729', '126', 'Update by Koobin Editor', '84.89.61.71', '2014-08-19 14:11:41', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('730', '127', 'Update by Koobin Editor', '84.89.61.71', '2014-08-19 14:12:05', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('731', '127', 'Update by Koobin Editor', '84.89.61.71', '2014-08-19 14:12:38', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('732', '3', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 14:38:36', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('733', '6', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 14:40:50', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36');
insert into pages_history (id, pages_id, summary, ip, date, detail, systeminfo) values ('734', '5', 'Update by La Mantis - Disseny i Internet Admin', '84.89.61.71', '2014-08-19 14:54:17', NULL, 'Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0');
drop table if exists search_queries;
create table search_queries (
  id int(11) not null auto_increment,
  search varchar(255) not null ,
  count int(11) default '0' not null ,
  result int(11) default '0' not null ,
  entered datetime not null ,
  modified datetime not null ,
  PRIMARY KEY (id)
);

drop table if exists search_swap;
create table search_swap (
  id int(11) not null auto_increment,
  search varchar(100) not null ,
  replacement varchar(100) not null ,
  entered datetime not null ,
  modified datetime not null ,
  PRIMARY KEY (id)
);

drop table if exists sessions;
create table sessions (
  sesskey varchar(32) not null ,
  expiry int(11) unsigned default '0' not null ,
  value text not null ,
  PRIMARY KEY (sesskey)
);

drop table if exists whos_online;
create table whos_online (
  id int(8) ,
  name varchar(64) not null ,
  session_id varchar(128) not null ,
  ip_address varchar(15) not null ,
  user_agent varchar(128) not null ,
  time_entry varchar(14) not null ,
  time_last_click varchar(14) not null ,
  last_page_url varchar(128) not null 
);

insert into whos_online (id, name, session_id, ip_address, user_agent, time_entry, time_last_click, last_page_url) values ('0', 'Guest', '01l5kqd5qe3es2v2g37g61r7e7', '84.89.61.71', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36', '1408526171', '1408526682', '/admin/favicon.png');
insert into whos_online (id, name, session_id, ip_address, user_agent, time_entry, time_last_click, last_page_url) values ('0', 'Guest', 'b32oidd8mms0kfh9pndbdnnic6', '84.89.61.129', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/7.0.6 Safari/537.78.2', '1408526247', '1408526247', '/');
insert into whos_online (id, name, session_id, ip_address, user_agent, time_entry, time_last_click, last_page_url) values ('0', 'Guest', 'ue02msg697gl70t797qrtu0r94', '66.249.78.14', 'Googlebot-Image/1.0', '1408526381', '1408526381', '/es/noticias/image/fotos/thumbs3/koobin_calella.jpg');
insert into whos_online (id, name, session_id, ip_address, user_agent, time_entry, time_last_click, last_page_url) values ('0', 'Guest', '1mgs1q9kttek2sjs055ig3kfm6', '88.31.140.70', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D257', '1408526664', '1408526664', '/zendesk/engranatge.png');
insert into whos_online (id, name, session_id, ip_address, user_agent, time_entry, time_last_click, last_page_url) values ('0', 'Guest', '2idb7uk134t4r36bcfbmt25nb7', '88.31.140.70', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D257', '1408526664', '1408526664', '/zendesk/logo.png');
