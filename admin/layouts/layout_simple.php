<?php

/*CRIDEM AL FUNCTION_TOP DE CADA MODUL -> Funcions auxiliars per mòdul */
$path_top = $path_modul . '/function_top.php';
if (is_file($path_top)){ include($path_top); }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='<?php echo LANGUAGE_CONTENT; ?>' xml:lang='<?php echo LANGUAGE_CONTENT; ?>' xmlns="http://www.w3.org/1999/xhtml">

    <head>
        
        <title><?php echo (tep_not_null($titular)) ? $titular : WEB_NAME; ?></title>
        
        <meta name="author" content="La Mantis - www.mantis.cat"/>
        <meta name="keywords" content="<?php echo (tep_not_null($keywords)) ? $keywords : WEB_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo (tep_not_null($description)) ? $description : WEB_DESCRIPTION; ?>" />
        
        <meta http-equiv="content-language" content="<?php echo LANGUAGE_CONTENT; ?>" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="robots" content="index,follow" />

      <link href="../image/favicon.png" type="image/png" rel="shortcut icon" />
        <link href="../image/favicon.ico" type="image/ico" rel="icon" />

        <link href='//fonts.googleapis.com/css?family=Titillium+Web:400,700,200|Arimo:400,700,400italic' rel='stylesheet' type='text/css'/>

        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

        <link rel="stylesheet" type="text/css" href="css/structure.css" media="screen" title="default" />
        <link rel="stylesheet" type="text/css" href="css/colorbox.css" media="screen" title="default" />
<!--        <link rel="stylesheet" type="text/css" href="css/dateInput.css" media="screen" title="default" />-->

        <link rel="stylesheet" href="css/multiselect/common.css" type="text/css" />
        <!--<link type="text/css" rel="stylesheet" href="css/jquery-ui/jquery-ui-1.8.21.custom.css" />-->
        <link type="text/css" rel="stylesheet" href="css/jquery-ui/jquery-ui-1.10.4.custom.min.css" />
        <link type="text/css" href="css/multiselect/ui.multiselect.css" rel="stylesheet" />
        <link type="text/css" href="css/jcrop/jquery.Jcrop.min.css" rel="stylesheet" />
<!--        <link type="text/css" href="css/jquery.timePicker.css" rel="stylesheet" />-->


<!--        <link rel="stylesheet" type="text/css" href="js/codemirror/theme/monokai.css" /> 
        <script src="js/codemirror/lib/codemirror.js"></script>
        <link rel="stylesheet" href="js/codemirror/lib/codemirror.css"/>
        <script src="js/codemirror/mode/php/php.js"></script>
        <script src="js/codemirror/mode/clike/clike.js"></script>
        <script src="js/codemirror/mode/xml/xml.js"></script>
        <script src="js/codemirror/mode/css/css.js"></script>
        <script src="js/codemirror/mode/javascript/javascript.js"></script>-->


        <?php
        /* CSS auxiliars per mòdul */
        $path_css = $path_modul . '/styles.css';
        if (is_file($path_css))
            echo '<link rel="stylesheet" type="text/css" href="' . $path_css . '" media="screen" title="default" />' . "\n";
        ?>

<!--[if IE]> <link href="<?php echo $URL_BASE . 'css/hack_ie.css'; ?>" rel="stylesheet" type="text/css"> <![endif]-->

        <!-- Javascript: Al final!!! Els scripts es carreguen un cop ho ha fet la pàgina!!! -->

        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
        <script type="text/javascript" src="js/jquery.colorbox.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>
<!--        <script type="text/javascript" src="js/jquery.dateInput.js"></script>-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js?v=1"></script>
        <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
        <script type="text/javascript" src="js/ui.multiselect.js"></script>
        <script type="text/javascript" src="js/jcrop/jquery.Jcrop.min.js"></script>
<!--        <script type="text/javascript" src="js/jquery.timePicker.js"></script>-->


        <script type="text/javascript" language="javascript">
            //<![CDATA[

            $(document).ready(function() {

                function check_form() {
                      
                    //mirem si s'ha de validar camps (funcio declarada particularment en el js de cada modul)
                    if(typeof (window.validarCamps) == 'function'){
                        return validarCamps();
                    }
                    //per defecte no validem
                    return true;
                }

                $('a.top').click(function(){
                    $.scrollTo('body',800);
                    return false;
                });

                $('#VerColMenu li > a').click(function() {
                    if ($(this).filter('.collapsed').length) {
                        $(this).addClass('expanded').removeClass('collapsed').find('+ ul').slideToggle('medium');
                    } else if ($(this).filter('.expanded').length) {
                        $(this).addClass('collapsed').removeClass('expanded').find('+ ul').slideToggle('medium');
                    }
                });

                $('.message_header').fadeIn('medium').fadeTo('medium',0.2).fadeTo('medium',0.8).fadeTo('medium',0.2).fadeTo('medium',1);

                $('.send_form').click(function() {
                    $('#pagina').block({
                        message: '<h1><img src="../../admin/image/loading.gif"/>&nbsp;&nbsp;Processant...</h1>'
                    });
                    $('#'+$(this).parents("form").attr("id")).submit();
                });

                $('.check_form').click(function(evento) {
                                      
                    if(check_form()) {
                        $('#pagina').block({
                            message: '<h1><img src="../../admin/image/loading.gif"/>&nbsp;&nbsp;Processant...</h1>'
                        });
                        $('#'+$(this).parents("form").attr("id")).submit();
                    } else {
                        evento.preventDefault();
                    }
                });

                $('.check_form_no_message').click(function(evento) {
                    if(check_form()) {
                        $('#'+$(this).parents("form").attr("id")).submit();
                    } else {
                        evento.preventDefault();
                    }
                });

                $('.active').click(function(evento) {
                    if(confirm('<?php echo addslashes(TEXT_ACTIVE); ?>')) {
                        $('#pagina').block({
                            message: '<h1><img src="../../admin/image/loading.gif"/>&nbsp;&nbsp;Processant...</h1>'
                        });
                    } else {
                        evento.preventDefault();
                        evento.stopPropagation();
                    }
                });

                $('.desactive').click(function(evento) {
                    if(confirm('<?php echo addslashes(TEXT_DESACTIVE); ?>')) {
                        $('#pagina').block({
                            message: '<h1><img src="../../admin/image/loading.gif"/>&nbsp;&nbsp;Processant...</h1>'
                        });
                    } else {
                        evento.preventDefault();
                    }
                });

                $('.delete').click(function(evento) {
                    if(confirm('<?php echo addslashes(TEXT_DELETE); ?>')) {
                        $('#pagina').block({
                            message: '<h1><img src="../../admin/image/loading.gif"/>&nbsp;&nbsp;Processant...</h1>'
                        });
                    } else {
                        evento.preventDefault();
                    }
                });

                $('a.tab').click(function () {
                    $('.current').removeClass('current');
                    $(this).addClass('current');

                    $('.lang_content').hide();

                    var mostra = $(this).attr('title');
                    $('#'+mostra).show();
                });
                $('a.tab2').click(function () {
                    $('.current2').removeClass('current2');
                    $(this).addClass('current2');

                    $('.lang_content2').hide();

                    var mostra = $(this).attr('title');
                    $('#'+mostra).show();
                });

                $('a.tab_editor').click(function () {
                    $('.current_editor').removeClass('current_editor');
                    $(this).addClass('current_editor');

                    $('.editor_content').hide();

                    var mostra = $(this).attr('title');
                    $('.'+mostra).show();
                    $('.link_'+mostra).addClass('current_editor');
                });
                
                $('a.opcio').click(function () {
                    var mostra = $(this).attr('id');
                    $('#valors_'+mostra).toggle();
                });
                $('a.grup').click(function () {
                    var mostra = $(this).attr('id');
                    $('#valors_'+mostra).toggle();
                });

                $('a.mostrar').click(function () {
                    var mostra = $(this).attr('rel');
                    $('#'+mostra).toggle();
                });

//                $(function() {
//                    $(".date_input").date_input();
//                    $('.time_input').timePicker();
//                });

                $.datepicker.regional['es'] = {
                    closeText: 'Cerrar',
                    prevText: '<Ant',
                    nextText: 'Sig>',
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                    weekHeader: 'Sm',
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                
                $.datepicker.setDefaults($.datepicker.regional['es']);

                
                $( ".date_input" ).datepicker();
                

                $("a[rel='colorbox']").colorbox({});
                $("a[rel='colorbox-gal']").colorbox({});
                $("a[rel='colorbox-flv']").colorbox({fixedWidth:"632",fixedHeight:"550", iframe:true});

                $('.link_del').click(function() {
                    $(this).parent().find('input').attr('value','');
                    //mirem si s'ha de borrar miniatura
                    if($('#miniatura').length>0){
                        $('#miniatura').hide();
                    }
                });

                //Codemirror
                if(document.getElementById("code")){
                    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                        lineNumbers: true,
                        matchBrackets: true,
                        mode: "application/x-httpd-php",
                        indentUnit: 4,
                        indentWithTabs: true,
                        enterMode: "keep",
                        tabMode: "shift",
                        lineWrapping: true,
                        onCursorActivity: function() {
                            editor.setLineClass(hlLine, null);
                            hlLine = editor.setLineClass(editor.getCursor().line, "activeline");
                        }
                    });
                    var hlLine = editor.setLineClass(0, "activeline");
                
                }
                   
                //GALERIA NORMAL
                $('#childframe').load(function() {
                    resizeHeight('childframe');
                });
                
                $('#fotos_groups_id').change(function() {
                    var src ='about:blank';//si s'ha seleccionat -- en el select posem iframe en blanc
                    if(this.value!=''){//si s'ha seleccionat una galeria en el select
                        src='?modul=fotos_fotos&group_id='+this.value+'&iframe=si&idLlistat=0';
                    }
                    carregarEnIframe(src,'childframe');
                });
                
                
                //GALERIA DE PREMSA      
                $('#childframe_premsa').load(function() {
                    resizeHeight('childframe_premsa');
                });
                
                $('#fotos_groups_id_premsa').change(function() {
                    var src ='about:blank';//si s'ha seleccionat -- en el select posem iframe en blanc
                    if(this.value!=''){//si s'ha seleccionat una galeria en el select
                        src='?modul=fotos_fotos&group_id='+this.value+'&iframe=si&idLlistat=premsa';
                    }
                    carregarEnIframe(src,'childframe_premsa');
                });
                
                
                //  TO TOP
                $(window).scroll(function() {
                    if($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();	
                    } else {
                        $('#toTop').fadeOut();
                    }
                });
 
                $('#toTop').click(function() {
                    $('body,html').animate({scrollTop:0},800);
                })
               
                //Images previes
                $(".img_input").change(function(){
                    readURL(this);
                });
                
                //efecte acordeon fieldsets camps fitxa
                $('h4.desplegable').click(function() {
                    if ($(this).filter('.collapsed').length) {
                        //important el display none ja que sinó no funciona efecte slide (detecta display block)
                        $(this).addClass('expanded').removeClass('collapsed').find('+ fieldset').css('visibility','visible').css('display','none').slideToggle('medium');
                    } else if ($(this).filter('.expanded').length) {
                        $(this).addClass('collapsed').removeClass('expanded').find('+ fieldset').hide('fast');
                    }
                });
                
                $("#tfBuscar").keyup(filtrarFilesTaula);

            });
            
            
            CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    
    config.filebrowserBrowseUrl = '/admin/js/kcfinder/browse.php?opener=ckeditor&type=files&2324098=781215';
    config.filebrowserImageBrowseUrl = '/admin/js/kcfinder/browse.php?opener=ckeditor&type=images&2324098=781215';
    config.filebrowserFlashBrowseUrl = '/admin/js/kcfinder/browse.php?opener=ckeditor&type=flash&2324098=781215';
    
    config.filebrowserUploadUrl = '/admin/js/kcfinder/upload.php?opener=ckeditor&type=files&7492556=548831';
    config.filebrowserImageUploadUrl = '/admin/js/kcfinder/upload.php?opener=ckeditor&type=images&7492556=548831';
    config.filebrowserFlashUploadUrl = '/admin/js/kcfinder/upload.php?opener=ckeditor&type=flash&7492556=548831';
    
   //config.allowedcallowedContent= 'false';
    config.allowedContent = true;
    
    config.format_tags = 'h1;h2;h3;p';//indiquem quins elements surten en el desplegable de format
    config.extraPlugins = 'codemirror';
    config.toolbar_Editor =
    [
        { name: 'document', items : ['Templates','Source' ] },
        { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
        //{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] },
        { name: 'insert', items : [ 'Image','Table','SpecialChar','Iframe' ] },
        //{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
        //{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
        //'HiddenField' ] },
        //'/',
        { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike'] },
        { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
        '-','JustifyLeft','JustifyCenter','JustifyRight' ] },
        { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
        //'/',
        { name: 'styles', items : [ 'Format','FontSize' ] }
        //{ name: 'colors', items : [ 'TextColor','BGColor' ] }

    ];
    
    config.toolbar_Links =
    [
        { name: 'document', items : [ 'Source'] },

        { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike'] },
        { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
        '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },

        //'/',
        { name: 'styles', items : [ 'FontSize' ] },
        //{ name: 'colors', items : [ 'TextColor','BGColor' ] }

    ];
    
    config.codemirror = {

        // Set this to the theme you wish to use (codemirror themes)
        theme: 'default',

        // Whether or not you want to show line numbers
        lineNumbers: true,

        // Whether or not you want to use line wrapping
        lineWrapping: true,

        // Whether or not you want to highlight matching braces
        matchBrackets: true,

        // Whether or not you want tags to automatically close themselves
        autoCloseTags: true,

        // Whether or not you want Brackets to automatically close themselves
        autoCloseBrackets: true,

        // Whether or not to enable search tools, CTRL+F (Find), CTRL+SHIFT+F (Replace), CTRL+SHIFT+R (Replace All), CTRL+G (Find Next), CTRL+SHIFT+G (Find Previous)
        enableSearchTools: true,

        // Whether or not you wish to enable code folding (requires 'lineNumbers' to be set to 'true')
        enableCodeFolding: true,

        // Whether or not to enable code formatting
        enableCodeFormatting: true,

        // Whether or not to automatically format code should be done when the editor is loaded
        autoFormatOnStart: true,

        // Whether or not to automatically format code should be done every time the source view is opened
        autoFormatOnModeChange: true,

        // Whether or not to automatically format code which has just been uncommented
        autoFormatOnUncomment: true,

        // Whether or not to highlight the currently active line
        highlightActiveLine: true,

        // Define the language specific mode 'htmlmixed' for html including (css, xml, javascript), 'application/x-httpd-php' for php mode including html, or 'text/javascript' for using java script only
        mode: 'htmlmixed',

        // Whether or not to show the search Code button on the toolbar
        showSearchButton: true,

        // Whether or not to show Trailing Spaces
        showTrailingSpace: true,

        // Whether or not to highlight all matches of current word/selection
        highlightMatches: true,

        // Whether or not to show the format button on the toolbar
        showFormatButton: true,

        // Whether or not to show the comment button on the toolbar
        showCommentButton: true,

        // Whether or not to show the uncomment button on the toolbar
        showUncommentButton: true,

        // Whether or not to show the showAutoCompleteButton button on the toolbar
        showAutoCompleteButton: true

    };
};

            /*Formularis fitxes*/

            function confirmar(){
                return confirm("<?php echo JS_TEXT_CONFIRM_PERDRE_CANVIS ?>");
            }
            
            
            function novaGaleria(group_id) {
                $('#loading_galeria').show('normal',function(){
                    carregarEnIframe('?modul=fotos_fotos&action=new_group&iframe=si&idLlistat=0&group_id='+group_id,'childframe');
                });
                return false;   
            }
            
            function novaGaleriaPremsa(group_id) {
                $('#loading_galeria_premsa').show('normal',function(){
                    carregarEnIframe('?modul=fotos_fotos&action=new_group&iframe=si&idLlistat=premsa&group_id='+group_id,'childframe_premsa');
                });
                
                return false;   
            }
            
            
            /*preview imatges*/
            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img_preview').fadeOut();
                        $('#img_preview').attr('src', e.target.result);
                        $('#img_preview').fadeIn();
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            
            //]]>
        </script>



        <?php
        /* Javascript auxiliars per mòdul */
        $path_js = $path_modul . '/js_script.php';
        if (is_file($path_js))
            include($path_js);
        ?>


    </head>

    <body>
        
        <?php //precarreguem el gif però no el mostrem, així aconseguim que en firefox com a minim es vegi la icona ?>
        <img style="display: none" src="../../admin/image/loading.gif"/>
        
        <div id="pagina">
           
            <?php
            /* Capçalera */
            include(DIR_WS_MODULS . 'header.php');
            ?>
            <div id="estructura">
                <div id="body_admin">
                    <div id="lateral_container">
                        <div id="lateral">
                            <?php
                            include(DIR_WS_MODULS . 'lateral.php');
                            ?>
                        </div>
                    </div>
                    <div id="contingut_container">
                        <div id="contingut">
                            <?php
                            /* MISSATGES ERROR-SUCCESS-INFO */
                            if ($messageStack->size > 0) 
                            {
                                echo $messageStack->output();
                            }

                            /* Contingut segons mòdul */
                            $path_content = $path_modul . '/content.php';
                            if (is_file($path_content)) {
                                include($path_content);
                            } else {
                                echo '<h2 class="not_found">Error: mòdul ' . $modul . ' no instal.lat.</h2>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include(DIR_WS_MODULS . 'footer.php'); ?>
        
    </body>
</html>
