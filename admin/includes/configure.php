<?php

/*
  Definim variables per al servidor
  FS = Filesystem (physical)
  WS = Webserver (virtual)
 */

define('HTTP_SERVER', '');   // Buides si estan a l'arrel
//define('HTTPS_SERVER', '');
define('DIR_WS_DOCUMENT_ROOT', './');  // ROOT !!!
define('DIR_FS_DOCUMENT_ROOT', str_replace("\\", "/", realpath(dirname(__FILE__) . "/..") . '/'));  // ROOT !!!
define('ENABLE_SSL', false); // secure webserver
define('HTTP_COOKIE_DOMAIN', '');
define('HTTPS_COOKIE_DOMAIN', '');
define('HTTP_COOKIE_PATH', '');
define('HTTPS_COOKIE_PATH', '');
define('BOTIGA', TRUE); // Define que ens diu si la plana que estem muntant es una botiga o no.
// Directori admin: paths relatius
define('DIR_WS_ADMIN', './');
define('DIR_FS_ADMIN', DIR_FS_DOCUMENT_ROOT . DIR_WS_ADMIN);

define('DIR_WS_TEXTS', DIR_WS_ADMIN . 'texts/'); // Textos
define('DIR_WS_ICONS', DIR_WS_ADMIN . 'image/icons/'); // Icones
define('DIR_WS_BACKUPS', DIR_WS_ADMIN . 'backups/'); // Còpies de seguretat
define('DIR_WS_PORTAL_UPLOADS', DIR_WS_ADMIN . 'uploads/'); // temporal uploads
define('DIR_WS_INCLUDES', DIR_WS_ADMIN . 'includes/');  // php's inclosos
define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');

// Directori portal: paths relatius i absoluts
define('DIR_WS_PORTAL', '../');
define('DIR_FS_PORTAL', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL);

define('DIR_WS_PORTAL_TEXTS', DIR_WS_PORTAL . 'texts/');
define('DIR_FS_PORTAL_TEXTS', DIR_FS_PORTAL . 'texts/');

define('DIR_WS_PORTAL_DOWNLOADS', DIR_WS_PORTAL . 'downloads/');
define('DIR_FS_PORTAL_DOWNLOADS', DIR_FS_PORTAL . 'downloads/');

define('DIR_WS_PORTAL_BANNERS', DIR_WS_PORTAL . 'image/banners/');
define('DIR_FS_PORTAL_BANNERS', DIR_FS_PORTAL . 'image/banners/');

define('DIR_WS_PORTAL_IMAGE_NEWS', DIR_WS_PORTAL . 'image/noticies/');
define('DIR_FS_PORTAL_IMAGE_NEWS', DIR_FS_PORTAL . 'image/noticies/');

define('DIR_WS_PORTAL_GALERIES', DIR_WS_PORTAL . 'image/galeries/');
define('DIR_FS_PORTAL_GALERIES', DIR_FS_PORTAL . 'image/galeries/');

define('DIR_WS_PORTAL_DOCUMENTS', DIR_WS_PORTAL . 'image/documents/');
define('DIR_FS_PORTAL_DOCUMENTS', DIR_FS_PORTAL . 'image/documents/');

define('DIR_WS_PORTAL_IMAGE_CATALEG', DIR_WS_PORTAL . 'image/cataleg/');
define('DIR_FS_PORTAL_IMAGE_CATALEG', DIR_FS_DOCUMENT_ROOT . DIR_WS_PORTAL_IMAGE_CATALEG);

define('DIR_WS_PORTAL_IMAGE_AGENDA', DIR_WS_PORTAL . 'image/agenda/');
define('DIR_FS_PORTAL_IMAGE_AGENDA', DIR_FS_PORTAL . 'image/agenda/');

define('DIR_WS_PORTAL_IMAGE_LLISTATS', DIR_WS_PORTAL . 'image/llistats/');
define('DIR_FS_PORTAL_IMAGE_LLISTATS', DIR_FS_PORTAL . 'image/llistats/');

define('DIR_WS_PORTAL_IMAGE_FOTOS', DIR_WS_PORTAL . 'image/fotos/');
define('DIR_FS_PORTAL_IMAGE_FOTOS', DIR_FS_PORTAL . 'image/fotos/');

define('DIR_WS_PORTAL_IMAGE_PROJECTES', DIR_WS_PORTAL . 'image/projectes/');
define('DIR_FS_PORTAL_IMAGE_PROJECTES', DIR_FS_PORTAL . 'image/projectes/');



/*
  Definim IDs de permisos per a modul usuaris
 */
define('PERMIS_LECTURA', 1);
define('PERMIS_ESCRIPTURA', 2);
define('PERMIS_ESBORRAR', 3);


// Dades de facebook
define('DIR_WS_FACEBOOK',DIR_WS_INCLUDES.'socialnetworks/facebook/facebook-php-sdk-v3.1.1-25/');
define('URL_CONNECTION', 'http://' . $_SERVER['SERVER_NAME'] . '/');

/*
  Definim mòduls
 */
define('DIR_WS_MODULS', DIR_WS_ADMIN . 'moduls/');
define('DIR_WS_LAYOUTS', DIR_WS_ADMIN . 'layouts/');
define('MODUL_DEFECTE', 'estadistiques');
define('MODUL_FORBIDDEN', 'users_users');
define('LAYOUT_DEFECTE', 'layout_simple.php');

/*
  Definim connexio amb la base de dades
 */
define('DB_SERVER', '127.0.0.1');
define('DB_SERVER_USERNAME','koobinevent_adm');
define('DB_SERVER_PASSWORD','FnKrHetNTfPioQjJXtd8');
define('DB_DATABASE','koobinevent');
define('USE_PCONNECT', 'false');
define('STORE_SESSIONS', 'mysql');
define('SESSION_WRITE_DIRECTORY', null);
?>
