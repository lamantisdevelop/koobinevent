<?php

// Start the clock for the page parse time log
define('PAGE_PARSE_START_TIME', microtime());
define('MAX_ADMIN_RESULTS', '50'); // Per paginacio admin
// Set the level of error reporting
error_reporting(E_ALL & ~E_NOTICE);

//forcem charset a utf
header('Content-type: text/html; charset=utf-8');

// Incloem paràmetres de configuració
require('includes/configure.php');

// set php_self in the local scope
if (!isset($PHP_SELF))
    $PHP_SELF = $_SERVER['PHP_SELF'];

// Used in the "Backup Manager" to compress backups
define('LOCAL_EXE_GZIP', '/usr/bin/gzip');
define('LOCAL_EXE_GUNZIP', '/usr/bin/gunzip');

// Definim taules de la BD utilitzades
require(DIR_WS_INCLUDES . 'database_tables.php');

// include the database functions
require(DIR_WS_FUNCTIONS . 'database.php');

// make a connection to the database... now
tep_db_connect() or die('Unable to connect to database server!');
tep_db_query("SET NAMES 'utf8'");

// Idioma per defecte admin
define('DEFAULT_LANG_ADMIN', 'ca');
// set application wide parameters
$configuration_query = tep_db_query('select item, value from ' . TABLE_CONFIG_VALUES . '');
while ($configuration = tep_db_fetch_array($configuration_query)) {
    define($configuration['item'], $configuration['value']);
}

// define our general functions used application-wide
require(DIR_WS_FUNCTIONS . 'general.php');
//require(DIR_WS_FUNCTIONS . 'catalogue.php');
//require(DIR_WS_FUNCTIONS . 'catalogue_admin.php');
require(DIR_WS_FUNCTIONS . 'fotos.php');
require(DIR_WS_FUNCTIONS . 'admin.php');
require(DIR_WS_FUNCTIONS . 'llistats.php');
require(DIR_WS_FUNCTIONS . 'edicions.php');
require(DIR_WS_FUNCTIONS . 'noticies.php');
require(DIR_WS_FUNCTIONS . 'agenda.php');
require(DIR_WS_FUNCTIONS . 'html_output.php');

// initialize the logger class
require(DIR_WS_CLASSES . 'logger.php');

// define how the session functions will be used
require(DIR_WS_FUNCTIONS . 'sessions.php');

// set the session name and save path
tep_session_name('userID');
tep_session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
if (function_exists('session_set_cookie_params')) {
    session_set_cookie_params(0, $cookie_path, $cookie_domain);
} elseif (function_exists('ini_set')) {
    ini_set('session.cookie_lifetime', '0');
    ini_set('session.cookie_path', $cookie_path);
    ini_set('session.cookie_domain', $cookie_domain);
}

// set the session ID if it exists
if (isset($_POST[tep_session_name()])) {
    tep_session_id($_POST[tep_session_name()]);
} elseif (($request_type == 'SSL') && isset($_GET[tep_session_name()])) {
    tep_session_id($_GET[tep_session_name()]);
}

// start the session
$session_started = false;
if (SESSION_FORCE_COOKIE_USE == 'True') {
    tep_setcookie('cookie_test', 'please_accept_for_session', time() + 60 * 60 * 24 * 30, $cookie_path, $cookie_domain);

    if (isset($HTTP_COOKIE_VARS['cookie_test'])) {
        tep_session_start();
        $session_started = true;
    }
} elseif (SESSION_BLOCK_SPIDERS == 'True') {
    $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
    $spider_flag = false;

    if (tep_not_null($user_agent)) {
        $spiders = file(DIR_WS_INCLUDES . 'spiders.txt');

        for ($i = 0, $n = sizeof($spiders); $i < $n; $i++) {
            if (tep_not_null($spiders[$i])) {
                if (is_integer(strpos($user_agent, trim($spiders[$i])))) {
                    $spider_flag = true;
                    break;
                }
            }
        }
    }

    if ($spider_flag == false) {
        tep_session_start();
        $session_started = true;
    }
} else {
    tep_session_start();
    $session_started = true;
}

// set SID once, even if empty
$SID = (defined('SID') ? SID : '');

// language
require(DIR_WS_FUNCTIONS . 'languages.php');

// Definim idioma: només 1 per administrador
if (!tep_session_is_registered('language')) {
    tep_session_register('language');
    tep_session_register('language_id');

    include(DIR_WS_CLASSES . 'language.php');
    $lng = new language();

    $lng->set_language(DEFAULT_LANG_ADMIN);

    $language = $lng->language['directory'];
    $language_id = $lng->language['id'];
}

// initialize the message stack for output messages
require(DIR_WS_CLASSES . 'message_stack.php');
$messageStack = new messageStack;

/* VALIDEM PERMISOS D'USUARI ACTUAL */
if (!empty($_GET['modul']) && tep_usuaris_mirar_permis_modul($_GET['modul']) === false) {
    $messageStack->add('Error, no tens permisos per a veure aquest modul', 'error');
    $modul = MODUL_FORBIDDEN;
    $_GET ['modul'] = MODUL_FORBIDDEN;
    $_GET ['action'] = 'forbidden';
}

//Inicialitzem CKEDITOR
// Make sure you are using a correct path here.
//include_once 'js/ckeditor/ckeditor.php';
//
//$ckeditor = new CKEditor();
//$ckeditor->basePath = 'js/ckeditor/';
//$ckeditor->config['extraPlugins'] = 'docprops';
//$ckeditor->config['toolbar'] = 'Editor';


/* EDICIO ACTUAL */
$edicio_actual = tep_get_edicio_actual();

// include the language translations: general + current page
if (file_exists(DIR_WS_TEXTS . 'general.php'))
    require(DIR_WS_TEXTS . 'general.php');
if (!empty($_GET['modul'])) {
    if (file_exists(DIR_WS_TEXTS . $_GET['modul'] . '.php')) {
        require(DIR_WS_TEXTS . $_GET['modul'] . '.php');
    }
} else {
    $current_page = basename($PHP_SELF);
    if (file_exists(DIR_WS_TEXTS . $current_page)) {
        require(DIR_WS_TEXTS . $current_page);
    } else if (file_exists(DIR_WS_TEXTS . MODUL_DEFECTE . '.php')) {
        require(DIR_WS_TEXTS . MODUL_DEFECTE . '.php');
    }
}

// split-page-results
require(DIR_WS_CLASSES . 'split_page_results.php');


// entry/item info classes
require(DIR_WS_CLASSES . 'object_info.php');
?>
