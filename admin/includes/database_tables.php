<?php

// Definim les taules utilitzades en la base de dades

//********** General **********//

define('TABLE_ADMIN_USERS', 'admin_users');
define('TABLE_ADMIN_USERS_ROLS', 'admin_users_rols');
define('TABLE_FTPFILES', 'ftpfiles');
define('TABLE_FTPFILESPERMISSIONS', 'ftpfiles_permissions');
define('TABLE_ADMIN_USERS_ROLS_PERMISOS', 'admin_users_rols_permisos');
define('TABLE_ADMIN_USERS_TIPUS_PERMISOS', 'admin_users_tipus_permisos');

define('TABLE_PAGES','pages');
define('TABLE_PAGES_DESCRIPTION','pages_description');
define('TABLE_PAGES_HISTORY','pages_history');

define('TABLE_BANNERS','banners');
define('TABLE_BANNERS_DESCRIPTION','banners_description');
define('TABLE_BANNERS_GROUPS','banners_groups');

define('TABLE_COUNTER', 'counter');
define('TABLE_LANGUAGES','languages');
define('TABLE_SESSIONS', 'sessions');
define('TABLE_WHOS_ONLINE','whos_online');

define('TABLE_CONFIG_GROUPS','config_groups');
define('TABLE_CONFIG_VALUES','config_values');

define('TABLE_EDICIONS','edicions');

define('TABLE_MAIL_CONTACT', 'mail_contact');
define('TABLE_MAIL_PREMSA', 'mail_premsa');
define('TABLE_MAIL_AMIC', 'mail_amic');


//********** Mòduls **********//

//noticies
define('TABLE_NOTICIES','noticies');
define('TABLE_NOTICIES_DESCRIPTION','noticies_description');
define('TABLE_NOTICIES_IMAGES','noticies_images');
define('TABLE_NOTICIES_IMAGES_DESCRIPTION','noticies_images_description');

//galeria fotos
define('TABLE_IMAGES','images');
define('TABLE_IMAGES_DESCRIPTION','images_description');
define('TABLE_IMAGES_GROUPS','images_groups');


//cataleg productes
define('TABLE_CATALOG_CATEGORIES','catalog_categories');
define('TABLE_CATALOG_CATEGORIES_DESCRIPTION','catalog_categories_description');
define('TABLE_CATALOG_CATEGORIES_IMAGES','catalog_categories_images');

//Catalogo
define('TABLE_CATALOG_PRODUCTS','catalog_products');
define('TABLE_CATALOG_PRODUCTS_DESCRIPTION','catalog_products_description');
define('TABLE_CATALOG_PRODUCTS_IMAGES','catalog_products_images');
define('TABLE_CATALOG_PRODUCTS_IMAGES_DESCRIPTION','catalog_products_images_description');
define('TABLE_CATALOG_PRODUCTS_TO_CATEGORIES','catalog_products_to_categories');
define('TABLE_CATALOG_PRODUCTS_DOWNLOADS','catalog_products_downloads');
define('TABLE_CATALOG_PRODUCTS_VIDEOS','catalog_products_videos');
define('TABLE_CATALOG_PRODUCTS_VIDEOS_DESCRIPTION','catalog_products_videos_description');

//llistats
define('TABLE_LLISTATS','llistats');
define('TABLE_LLISTATS_DESCRIPTION','llistats_description');
define('TABLE_LLISTATS_GROUPS','llistats_groups');
define('TABLE_LLISTATS_GROUPS_DESCRIPTION','llistats_groups_description');
define('TABLE_LLISTATS_RELATED','llistats_related');

//Menus
define('TABLE_MENUS', 'menus');
define('TABLE_MENUS_PAGES', 'menus_pages');
define('TABLE_MENUS_PAGES_DESCRIPTION', 'menus_pages_description');

//Galeria de fotos
define('TABLE_FOTOS','fotos');
define('TABLE_FOTOS_DESCRIPTION','fotos_description');
define('TABLE_FOTOS_GROUPS','fotos_groups');
define('TABLE_FOTOS_GROUPS_DESCRIPTION','fotos_groups_description');
define('TABLE_FOTOS_RATIOS','fotos_ratios');

//Projectes
define('TABLE_PROJECTES','projectes');
define('TABLE_PROJECTES_DESCRIPTION','projectes_description');
define('TABLE_PROJECTES_IMAGES','projectes_images');
define('TABLE_PROJECTES_IMAGES_DESCRIPTION','projectes_images_description');



//************ Botiga *************//
define('TABLE_CUSTOMERS','customers');
define('TABLE_ADDRESS_BOOK','address_book');     

define('TABLE_CUSTOMERS_BASKET','customers_basket');
define('TABLE_CUSTOMERS_BASKET_ATTRIBUTES','customers_basket_attributes');

define('TABLE_SPECIALS','specials');

define('TABLE_MANUFACTURERS','manufacturers');

define('TABLE_DISPONIBILITY','disponibility');

define('TABLE_GEO_ZONES', 'geo_zones');

define('TABLE_TARIFES', 'tarifes');
define('TABLE_TARIFES_TIPUS', 'tarifes_tipus');

define('TABLE_TPV_PROCESS', 'tpv_process');

define('TABLE_ORDERS','orders');
define('TABLE_ORDERS_STATUS','orders_status');
define('TABLE_ORDERS_STATUS_HISTORY','orders_status_history');
define('TABLE_ORDERS_PRODUCTS','orders_products');
define('TABLE_ORDERS_PRODUCTS_ATTRIBUTES','orders_products_attributes');
define('TABLE_ORDERS_TOTAL','orders_total');

define('TABLE_SEARCH_SWAP','search_swap');
define('TABLE_SEARCH_QUERIES','search_queries');

define('TABLE_CATEGORIES','categories');
define('TABLE_CATEGORIES_DESCRIPTION','categories_description');

define('TABLE_DISTRIBUIDORS','distribuidors');
define('TABLE_DISTRIBUIDORS_DESCRIPTION','distribuidors_description');

define('TABLE_COUNTRIES', 'countries');
define('TABLE_ZONES', 'zones');
define('TABLE_CURRENCIES', 'currencies');
define('TABLE_TAX_CLASS', 'tax_class');


//Productos
define('TABLE_PRODUCTS','products');
define('TABLE_PRODUCTS_DESCRIPTION','products_description');
define('TABLE_PRODUCTS_TO_CATEGORIES','products_to_categories');
define('TABLE_PRODUCTS_DESTACATS','products_destacats');
define('TABLE_PRODUCTS_INICI','products_inici');
define('TABLE_PRODUCTS_IMAGES','products_images');

define('TABLE_PRODUCTS_DOWNLOADS','products_downloads');
define('TABLE_PRODUCTS_DOWNLOADS_DESCRIPTION','products_downloads_description');

define('TABLE_PRODUCTS_RELATED','products_related');
define('TABLE_PRODUCTS_GROUPED','products_grouped');

define('TABLE_PRODUCTS_ATTRIBUTES','products_attributes');

define('TABLE_PRODUCTS_OPTIONS','products_options');
define('TABLE_PRODUCTS_OPTIONS_GROUPS','products_options_groups');
define('TABLE_PRODUCTS_OPTIONS_VALUES','products_options_values');

//Funcions
define('TABLE_FUNCIONS','funcions');

//Agenda activitats
define('TABLE_ACTIVITATS','activitats');
define('TABLE_ACTIVITATS_DESCRIPTION','activitats_description');
?>
