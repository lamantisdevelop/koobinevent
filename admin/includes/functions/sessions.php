<?php
function tep_session_start() {
  $success = session_start();

  if ($success && count($_SESSION)) {
    $session_keys = array_keys($_SESSION);
    foreach($session_keys as $variable) {
      link_session_variable($variable, true);
    }
  }
  return $success;
}

function tep_session_register($variable) {
  link_session_variable($variable, true);
  return true;
}

function tep_session_is_registered($variable) {
  return isset($_SESSION[$variable]);
}

function tep_session_unregister($variable) {
  link_session_variable($variable, false);
  unset($_SESSION[$variable]);
  return true;
}

function link_session_variable($var_name, $map) {
  if ($map) {
    if (isset($GLOBALS[$var_name])) {
      $_SESSION[$var_name] = $GLOBALS[$var_name];
    }

    $GLOBALS[$var_name] =& $_SESSION[$var_name];
  } else {
    $nothing = 0;
    $GLOBALS[$var_name] =& $nothing;
    unset($GLOBALS[$var_name]);
    $GLOBALS[$var_name] = $_SESSION[$var_name];
  }
}

function tep_session_id($sessid = '') {
  if (!empty($sessid)) {
    return session_id($sessid);
  } else {
    return session_id();
  }
}

function tep_session_name($name = '') {
  if (!empty($name)) {
    return session_name($name);
  } else {
    return session_name();
  }
}

function tep_session_close() {
  if (count($_SESSION)) {
    $session_keys = array_keys($_SESSION);
    foreach($session_keys as $variable) {
      link_session_variable($variable, false);
    }
  }

  if (function_exists('session_close')) {
    session_close();
  }
}

function tep_session_destroy() {
  if (count($_SESSION)) {
    $session_keys = array_keys($_SESSION);
    foreach($session_keys as $variable) {
      link_session_variable($variable, false);
      unset($_SESSION[$variable]);
    }
  }
  return session_destroy();
}

function tep_session_save_path($path = '') {
  if (!empty($path)) {
    return session_save_path($path);
  } else {
    return session_save_path();
  }
}
?>
