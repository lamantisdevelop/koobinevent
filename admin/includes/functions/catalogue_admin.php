<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<?php

/**
 * Inserta un producte des del formulari ADMIN fitxa_producte
 */
function tep_insert_product_admin($current_category_id) {

    // Creem Array Idiomes
    $languages = tep_get_languages();
    // Traiem limit scripts
    tep_set_time_limit(0);

    // Insertem dades generals
    $sql_data_array = array('quantity' => tep_db_prepare_input($_POST['quantity']),
        'min_quantity' => tep_db_prepare_input($_POST['min_quantity']),
        'model' => tep_db_prepare_input($_POST['model']),
        'video' => tep_db_prepare_input($_POST['video']),
        'video2' => tep_db_prepare_input($_POST['video2']),
        'price' => tep_db_prepare_input($_POST['product_price']),
        'disponibility_id' => tep_db_prepare_input($_POST['disponibility_id']),
        'available' => tep_db_prepare_input($_POST['available']),
        'tax_class_id' => tep_db_prepare_input($_POST['tax_class_id']),
        'espais_id' => tep_db_prepare_input($_POST['espais_id']),
        'web_companyia' => tep_db_prepare_input($_POST['web_companyia']),
        'companyia' => tep_db_prepare_input($_POST['companyia']),
        'autor' => tep_db_prepare_input($_POST['autor']),
        'weight' => tep_db_prepare_input($_POST['product_weight']),
        'status' => $_GET['action'] == 'preview' ? '2' : '0', //en cas de preview fem actiu per tal que pugui veure's
        'entered' => 'now()',
        'modified' => 'now()',
        'grouped' => isset($_POST['grouped_product']) ? 1 : 0,
        'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
        'fotos_groups_id_premsa' => tep_db_prepare_input($_POST['fotos_groups_id_premsa']),
        'id_edicions' => EDICIO_ACTUAL,
        'ordered' => '0');

    tep_db_perform(TABLE_PRODUCTS, $sql_data_array);
    $aux_id = tep_db_insert_id();

    // Entrades segons idioma
    $name_array = $_POST['name'];
    $subtitol_array = $_POST['subtitol'];
    $desc_short_array = $_POST['desc_short'];
    $description_array = $_POST['description'];
    $tecnical_array = $_POST['tecnical'];
    $durada_array = $_POST['durada'];
    $llengua_array = $_POST['llengua'];
    $box_content_array = $_POST['box_content'];
    $activitats_relacionades_array = $_POST['activitats_relacionades'];
    $observacions_array = $_POST['observacions'];
    $link_comprar_array = $_POST['link_comprar'];
    $download = $_FILES['download_new'];
    $download_title_array = $_POST['download_title'];
    $download2 = $_FILES['download2_new'];
    $download2_title_array = $_POST['download2_title'];
    $download_premsa = $_FILES['download_premsa_new'];
    $download_premsa_title_array = $_POST['download_premsa_title'];
    $download_premsa2 = $_FILES['download_premsa2_new'];
    $download_premsa2_title_array = $_POST['download_premsa2_title'];
    $page_title_array = $_POST['page_title'];
    $keywords_array = $_POST['keywords'];
    $page_description_array = $_POST['page_description'];
    $destacat_array = $_POST['destacat'];
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        $lang_aux = $languages[$i]['id'];

        // Upload descàrrega
        $download_aux = $download['name'][$lang_aux];
        if (!empty($download_aux)) {
            if (is_uploaded_file($download['tmp_name'][$lang_aux])) {
                // Nom no repetit
                $download_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_aux);
                // Copiar
                move_uploaded_file($download['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_aux);
            }
        }
        //descarrega 2
        $download2_aux = $download2['name'][$lang_aux];
        if (!empty($download2_aux)) {
            if (is_uploaded_file($download2['tmp_name'][$lang_aux])) {
                // Nom no repetit
                $download2_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download2_aux);
                // Copiar
                move_uploaded_file($download2['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download2_aux);
            }
        }
        //descarrega premsa
        $download_premsa_aux = $download_premsa['name'][$lang_aux];
        if (!empty($download_premsa_aux)) {
            if (is_uploaded_file($download_premsa['tmp_name'][$lang_aux])) {
                // Nom no repetit
                $download_premsa_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_premsa_aux);
                // Copiar
                move_uploaded_file($download_premsa['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_premsa_aux);
            }
        }
        //descarrega premsa2
        $download_premsa2_aux = $download_premsa2['name'][$lang_aux];
        if (!empty($download_premsa2_aux)) {
            if (is_uploaded_file($download_premsa2['tmp_name'][$lang_aux])) {
                // Nom no repetit
                $download_premsa2_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_premsa2_aux);
                // Copiar
                move_uploaded_file($download_premsa2['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_premsa2_aux);
            }
        }

        // Insertem dades
        $sql_data_array = array('products_id' => (int) $aux_id,
            'language_id' => (int) $lang_aux,
            'name' => tep_db_prepare_input($name_array[$lang_aux]),
            'subtitol' => tep_db_prepare_input($subtitol_array[$lang_aux]),
            'desc_short' => tep_db_prepare_input($desc_short_array[$lang_aux]),
            'description' => tep_db_prepare_input($description_array[$lang_aux]),
            'tecnical' => tep_db_prepare_input($tecnical_array[$lang_aux]),
            'durada' => tep_db_prepare_input($durada_array[$lang_aux]),
            'llengua' => tep_db_prepare_input($llengua_array[$lang_aux]),
            'box_content' => tep_db_prepare_input($box_content_array[$lang_aux]),
            'activitats_relacionades' => tep_db_prepare_input($activitats_relacionades_array[$lang_aux]),
            'observacions' => tep_db_prepare_input($observacions_array[$lang_aux]),
            'link_comprar' => tep_db_prepare_input($link_comprar_array[$lang_aux]),
            'page_title' => tep_db_prepare_input($page_title_array[$lang_aux]),
            'keywords' => tep_db_prepare_input($keywords_array[$lang_aux]),
            'page_description' => tep_db_prepare_input($page_description_array[$lang_aux]),
            'destacat' => $destacat_array[$lang_aux],
            'download' => tep_db_prepare_input($download_aux),
            'download_title' => tep_db_prepare_input($download2_title_array[$lang_aux]),
            'download2' => tep_db_prepare_input($download2_aux),
            'download2_title' => tep_db_prepare_input($download2_title_array[$lang_aux]),
            'download_premsa' => tep_db_prepare_input($download_premsa_aux),
            'download_premsa_title' => tep_db_prepare_input($download_premsa_title_array[$lang_aux]),
            'download_premsa2' => tep_db_prepare_input($download_premsa2_aux),
            'download_premsa2_title' => tep_db_prepare_input($download_premsa2_title_array[$lang_aux]));
        // Crida base dades
        tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array);
    }

    // Ordre
    $order_now = tep_search_new_order_group(TABLE_PRODUCTS_TO_CATEGORIES, 'categories_id', $current_category_id);

    // Afegir a productes x categoria
    $sql_data_array = array('products_id' => (int) $aux_id,
        'categories_id' => (int) $current_category_id,
        'listorder' => $order_now);

    tep_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $sql_data_array);
    $p2c_id = tep_db_insert_id();

    // Canviem items ordre si cal
    if ($_POST['listorder'] > 0 && $_POST['listorder'] < $order_now) {
        tep_update_order_group($p2c_id, $order_now, $_POST['listorder'], TABLE_PRODUCTS_TO_CATEGORIES, 'categories_id', $current_category_id);
    }

    // Atributs: opcions producte
    $values_array = $_POST['values'];
    $options_id_array = $_POST['options_id'];
    $values_id_array = $_POST['values_id'];
    $price_array = $_POST['price'];
    $price_prefix_array = $_POST['price_prefix'];
    $weight_array = $_POST['weight'];
    $weight_prefix_array = $_POST['weight_prefix'];

    if (!is_array($values_array))
        $values_array = array();

    foreach ($values_array as $key => $value) {
        // Vector entrades
        $sql_data_array = array('products_id' => (int) $aux_id,
            'options_id' => tep_db_prepare_input($options_id_array[$key]),
            'values_id' => tep_db_prepare_input($values_id_array[$key]),
            'price' => tep_db_prepare_input($price_array[$key]),
            'price_prefix' => tep_db_prepare_input($price_prefix_array[$key]),
            'weight' => tep_db_prepare_input($weight_array[$key]),
            'weight_prefix' => tep_db_prepare_input($weight_prefix_array[$key]));
        // Guardem a BD
        tep_db_perform(TABLE_PRODUCTS_ATTRIBUTES, $sql_data_array);
    }


    //COPIEM FUNCIONS D'ORIGEN SI PREVIEW
    if ($_POST['preview']) {
        $id_funcio_array = $_POST['id_funcio'];
        //recorrem les funcions en BBDD i actualitzem els valors de les actuals
        foreach ($id_funcio_array as $id_funcio) {
            $funcio = tep_get_funcio_by_id($id_funcio); //obtenim funcio a clonar
            $funcio['id'] = null; //fem que sigui autoincrement
            $funcio['id_espectacle'] = (int) $aux_id; //posem ID espectacle temporal (copiat)
            tep_db_perform(TABLE_FUNCIONS, $funcio);
        }
    }

    //Funcions de l'espectacle
    $data_funcio_array = $_POST['data_funcio'];
    $hora_funcio_array = $_POST['hora_funcio'];
    $nova_array = $_POST['nova_hidden'];

    foreach ($data_funcio_array as $key => $value) {
        if ($key > 0) {
            // Vector entrades
            $sql_data_array = array('id_espectacle' => (int) $aux_id,
                'data' => $value . ' ' . $hora_funcio_array[$key],
                'nova' => $nova_array[$key]);
            // Guardem a BD
            tep_db_perform(TABLE_FUNCIONS, $sql_data_array);
        }
    }


    // Descàrregues adicionals
    $file_download = $_FILES['file_download'];
    $download_order = $_POST['download_order'];
    $download_titol = $_POST['download_titol'];
    for ($key = 0; $key < sizeof($file_download['name']); $key++) { // Més petit, la última és la que es copia
        // Pugem arxiu
        $download_aux = $file_download['name'][$key];
        if (!empty($download_aux)) {
            if (is_uploaded_file($file_download['tmp_name'][$key])) {
                // Nom no repetit
                $download_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_aux);
                // Copiar
                move_uploaded_file($file_download['tmp_name'][$key], DIR_FS_PORTAL_DOWNLOADS . $download_aux);
            }
            // Insertem dades
            $sql_data_array = array('products_id' => (int) $aux_id,
                'download' => tep_db_prepare_input($download_aux),
                'listorder' => tep_db_prepare_input($download_order[$key]),
                'entered' => 'now()');
            // Crida base dades
            tep_db_perform(TABLE_PRODUCTS_DOWNLOADS, $sql_data_array);
            $download_id = tep_db_insert_id();

            // Dades segons idioma
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                $lang_aux = $languages[$i]['id'];
                // Insertem dades
                $sql_data_array = array('downloads_id' => tep_db_prepare_input($download_id),
                    'language_id' => (int) $lang_aux,
                    'titol' => tep_db_prepare_input($download_titol[$lang_aux][$key]));
                // Crida base dades
                tep_db_perform(TABLE_PRODUCTS_DOWNLOADS_DESCRIPTION, $sql_data_array);
            }
        }
    }


    //***************Productes relacionats***************
    //Copiem productes relacionats d'origen si PREVIEW
    if ($_POST['preview']) {
        $products_rel_aux_id = $_POST['products_rel_aux_id'];
        if (!empty($products_rel_aux_id)) {
            foreach ($products_rel_aux_id as $id_producte) {
                $item = tep_get_espectacle_by_id($id_producte); //obtenim funcio a clonar
                $sql_data_array = array('products_id' => (int) $aux_id, //producte NOU per fer preview 
                    'related_id' => tep_db_prepare_input((int) $id_producte), //posem ID relacionat
                    'entered' => 'now()');
                //INSERT a BBDD de nou producte relacionat (en el nou producte temporal per preview)
                tep_db_perform(TABLE_PRODUCTS_RELATED, $sql_data_array);
            }
        }
    }
    $products_rel = array();
    $products_rel_id_array = $_POST['products_rel_id'];
    foreach ($products_rel_id_array as $key => $value) {
        if ($key > 0 && !in_array($value, $products_rel)) { // No agafem el primer, només ens serveix per clonar i tampoc si ja existeix
            // Actualitzem vector personal
            $products_rel[] = $value;
            // Vector entrades
            $sql_data_array = array('products_id' => (int) $aux_id,
                'related_id' => tep_db_prepare_input($value),
                'entered' => 'now()');
            // Guardem a BD
            tep_db_perform(TABLE_PRODUCTS_RELATED, $sql_data_array);
        }
    }


    return $aux_id;
}

/**
 * Actualitza un producte des del formulari ADMIN fitxa_producte
 */
function tep_update_product_admin($current_category_id) {
    // Creem Array Idiomes
    $languages = tep_get_languages();
    // Identificador item
    $aux_id = tep_db_prepare_input($_GET['pID']);

    // Traiem limit scripts
    tep_set_time_limit(0);

    // Insertem dades generals
    $sql_data_array = array('quantity' => tep_db_prepare_input($_POST['quantity']),
        'min_quantity' => tep_db_prepare_input($_POST['min_quantity']),
        'model' => tep_db_prepare_input($_POST['model']),
        'video' => tep_db_prepare_input($_POST['video']),
        'video2' => tep_db_prepare_input($_POST['video2']),
        'price' => tep_db_prepare_input($_POST['product_price']),
        'disponibility_id' => tep_db_prepare_input($_POST['disponibility_id']),
        'available' => tep_db_prepare_input($_POST['available']),
        'tax_class_id' => tep_db_prepare_input($_POST['tax_class_id']),
        'espais_id' => tep_db_prepare_input($_POST['espais_id']),
        'web_companyia' => tep_db_prepare_input($_POST['web_companyia']),
        'companyia' => tep_db_prepare_input($_POST['companyia']),
        'autor' => tep_db_prepare_input($_POST['autor']),
        'weight' => tep_db_prepare_input($_POST['product_weight']),
        'grouped' => isset($_POST['grouped_product']) ? 1 : 0,
        'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
        'fotos_groups_id_premsa' => tep_db_prepare_input($_POST['fotos_groups_id_premsa']),
        'modified' => 'now()');

    tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "id = '" . (int) $aux_id . "'");

    // Entrades segons idioma
    $name_array = $_POST['name'];
    $subtitol_array = $_POST['subtitol'];
    $desc_short_array = $_POST['desc_short'];
    $description_array = $_POST['description'];
    $tecnical_array = $_POST['tecnical'];
    $durada_array = $_POST['durada'];
    $llengua_array = $_POST['llengua'];
    $box_content_array = $_POST['box_content'];
    $download_act_array = $_POST['download_act'];
    $download_title_array = $_POST['download_title'];
    $download = $_FILES['download_new'];
    $download_prev_array = $_POST['download_prev'];
    $download2_act_array = $_POST['download2_act'];
    $download2_title_array = $_POST['download2_title'];
    $download2 = $_FILES['download2_new'];
    $download2_prev_array = $_POST['download2_prev'];
    $download_premsa = $_FILES['download_premsa_new'];
    $download_premsa_title_array = $_POST['download_premsa_title'];
    $download_premsa_act_array = $_POST['download_premsa_act'];
    $download_premsa_prev_array = $_POST['download_premsa_prev'];
    $download_premsa2 = $_FILES['download_premsa2_new'];
    $download_premsa2_title_array = $_POST['download_premsa2_title'];
    $download_premsa2_act_array = $_POST['download_premsa2_act'];
    $download_premsa2_prev_array = $_POST['download_premsa2_prev'];
    $activitats_relacionades_array = $_POST['activitats_relacionades'];
    $observacions_array = $_POST['observacions'];
    $link_comprar_array = $_POST['link_comprar'];
    $page_title_array = $_POST['page_title'];
    $keywords_array = $_POST['keywords'];
    $page_description_array = $_POST['page_description'];
    $destacat_array = $_POST['destacat'];
    // Recorrem idiomes
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        $lang_aux = $languages[$i]['id'];

        // Upload descàrrega
        $download_aux = $download['name'][$lang_aux];
        if (!empty($download_aux)) {
            if (is_uploaded_file($download['tmp_name'][$lang_aux])) {
                // Borrem imatge vella
                $download_old = DIR_FS_PORTAL_DOWNLOADS . $download_prev_array[$lang_aux];
                @unlink($download_old);
                // Nom imatge no repetit
                $download_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_aux);
                // Copiar imatge
                move_uploaded_file($download['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_aux);
                //assignar permisos per a poder borrar despres
                $old = umask(0);
                chmod(DIR_FS_PORTAL_DOWNLOADS . $download_aux, 0777);
                umask($old);
            }
        } else {
            $download_aux = $download_prev_array[$lang_aux];
            // Si és buit borrem imatge, cas que n'hi hagi
            if (!tep_not_null($download_act_array[$lang_aux]) && tep_not_null($download_prev_array[$lang_aux])) {
                // Borrem imatge vella
                $download_old = DIR_FS_PORTAL_DOWNLOADS . $download_prev_array[$lang_aux];
                if (file_exists($download_old))
                    @unlink($download_old);
            }
        }
        // Upload descàrrega2
        $download2_aux = $download2['name'][$lang_aux];
        if (!empty($download2_aux)) {
            if (is_uploaded_file($download2['tmp_name'][$lang_aux])) {
                // Borrem imatge vella
                $download2_old = DIR_FS_PORTAL_DOWNLOADS . $download2_prev_array[$lang_aux];
                if (file_exists($download2_old))
                    @unlink($download2_old);
                // Nom imatge no repetit
                $download2_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download2_aux);
                // Copiar imatge
                move_uploaded_file($download2['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download2_aux);
                //assignar permisos per a poder borrar despres
                $old = umask(0);
                chmod(DIR_FS_PORTAL_DOWNLOADS . $download2_aux, 0777);
                umask($old);
            }
        } else {
            $download2_aux = $download2_act_array[$lang_aux];
            // Si és buit borrem imatge, cas que n'hi hagi
            if (!tep_not_null($download2_act_array[$lang_aux]) && tep_not_null($download2_prev_array[$lang_aux])) {
                // Borrem imatge vella
                $download2_old = DIR_FS_PORTAL_DOWNLOADS . $download2_prev_array[$lang_aux];
                if (file_exists($download2_old))
                    @unlink($download2_old);
            }
        }

        // Upload descàrrega premsa
        $download_premsa_aux = $download_premsa['name'][$lang_aux];
        if (!empty($download_premsa_aux)) {
            if (is_uploaded_file($download_premsa['tmp_name'][$lang_aux])) {
                // Borrem imatge vella
                $download_premsa_old = DIR_FS_PORTAL_DOWNLOADS . $download_premsa_prev_array[$lang_aux];
                if (file_exists($download_premsa_old))
                    @unlink($download_premsa_old);
                // Nom imatge no repetit
                $download_premsa_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_premsa_aux);
                // Copiar imatge
                move_uploaded_file($download_premsa['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_premsa_aux);
                //assignar permisos per a poder borrar despres
                $old = umask(0);
                chmod(DIR_FS_PORTAL_DOWNLOADS . $download_premsa_aux, 0777);
                umask($old);
            }
        } else {
            $download_premsa_aux = $download_premsa_act_array[$lang_aux];
            // Si és buit borrem imatge, cas que n'hi hagi
            if (!tep_not_null($download_premsa_act_array[$lang_aux]) && tep_not_null($download_premsa_prev_array[$lang_aux])) {
                // Borrem imatge vella
                $download_premsa_old = DIR_FS_PORTAL_DOWNLOADS . $download_premsa_prev_array[$lang_aux];
                if (file_exists($download_premsa_old))
                    @unlink($download_premsa_old);
            }
        }
        // Upload descàrrega premsa2
        $download_premsa2_aux = $download_premsa2['name'][$lang_aux];
        if (!empty($download_premsa2_aux)) {
            if (is_uploaded_file($download_premsa2['tmp_name'][$lang_aux])) {
                // Borrem imatge vella
                $download_premsa2_old = DIR_FS_PORTAL_DOWNLOADS . $download_premsa2_prev_array[$lang_aux];
                if (file_exists($download_premsa2_old))
                    @unlink($download_premsa2_old);
                // Nom imatge no repetit
                $download_premsa2_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_premsa2_aux);
                // Copiar imatge
                move_uploaded_file($download_premsa2['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_premsa2_aux);
                //assignar permisos per a poder borrar despres
                $old = umask(0);
                chmod(DIR_FS_PORTAL_DOWNLOADS . $download_premsa2_aux, 0777);
                umask($old);
            }
        } else {
            $download_premsa2_aux = $download_premsa2_act_array[$lang_aux];
            // Si és buit borrem imatge, cas que n'hi hagi
            if (!tep_not_null($download_premsa2_act_array[$lang_aux]) && tep_not_null($download_premsa2_prev_array[$lang_aux])) {
                // Borrem imatge vella
                $download_premsa2_old = DIR_FS_PORTAL_DOWNLOADS . $download_premsa2_prev_array[$lang_aux];
                if (file_exists($download_premsa2_old))
                    @unlink($download_premsa2_old);
            }
        }

        // Insertem dades
        $sql_data_array = array('name' => tep_db_prepare_input($name_array[$lang_aux]),
            'subtitol' => tep_db_prepare_input($subtitol_array[$lang_aux]),
            'desc_short' => tep_db_prepare_input($desc_short_array[$lang_aux]),
            'description' => tep_db_prepare_input($description_array[$lang_aux]),
            'tecnical' => tep_db_prepare_input($tecnical_array[$lang_aux]),
            'durada' => tep_db_prepare_input($durada_array[$lang_aux]),
            'llengua' => tep_db_prepare_input($llengua_array[$lang_aux]),
            'tecnical' => tep_db_prepare_input($tecnical_array[$lang_aux]),
            'box_content' => tep_db_prepare_input($box_content_array[$lang_aux]),
            'activitats_relacionades' => tep_db_prepare_input($activitats_relacionades_array[$lang_aux]),
            'observacions' => tep_db_prepare_input($observacions_array[$lang_aux]),
            'link_comprar' => tep_db_prepare_input($link_comprar_array[$lang_aux]),
            'page_title' => tep_db_prepare_input($page_title_array[$lang_aux]),
            'keywords' => tep_db_prepare_input($keywords_array[$lang_aux]),
            'page_description' => tep_db_prepare_input($page_description_array[$lang_aux]),
            //'destacat' => $destacat_array[$lang_aux],
            'download' => tep_db_prepare_input($download_aux),
            'download_title' => tep_db_prepare_input($download_title_array[$lang_aux]),
            'download2' => tep_db_prepare_input($download2_aux),
            'download2_title' => tep_db_prepare_input($download2_title_array[$lang_aux]),
            'download_premsa' => tep_db_prepare_input($download_premsa_aux),
            'download_premsa_title' => tep_db_prepare_input($download_premsa_title_array[$lang_aux]),
            'download_premsa2' => tep_db_prepare_input($download_premsa2_aux),
            'download_premsa2_title' => tep_db_prepare_input($download_premsa2_title_array[$lang_aux]));

        // Crida base dades
        tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array, 'update', 'products_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');


        //Categories on el producte es DESTACAT
        $categories = tep_get_categories_ids_by_product($aux_id); //totes les categories del producte
        $destacats = tep_get_categories_destacades_by_product($aux_id, $lang_aux); //obtenim les categories destacades per idioma
        while ($categoria = tep_db_fetch_array($categories)) {//recorrem totes les categories a qui pertany el producte
            if ($destacat_array[$lang_aux][$categoria['categories_id']]) {//si el producte està DESTACAT en aquesta categoria
                //si ja era destacat no cal fer res, el deixem igual
                if (!$destacats[$categoria['categories_id']]) {//si NO DESTACAT fem INSERT
                    // Vector entrades
                    $sql_data_array = array(
                        'products_id' => $aux_id,
                        'categories_id' => $categoria['categories_id'],
                        'language_id' => $lang_aux);
                    // Guardem a BD
                    tep_db_perform(TABLE_PRODUCTS_DESTACATS, $sql_data_array);
                }
            } else {//producte NO DESTACAT
                //Eliminem el destacat (si exsitia ja com a destacat el borra sino no fa res)
                tep_db_query("delete from " . TABLE_PRODUCTS_DESTACATS . " where products_id = " . $aux_id . " AND categories_id=" . $categoria['categories_id'] . " AND language_id=" . $lang_aux);
            }
        }
    }//FI IDIOMES
    // Trobar posicio actual a p2c: id i listorder per ordenar a taula p2c
    $order_query = tep_db_query("select id, listorder from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $aux_id . "' and categories_id = '" . (int) $current_category_id . "'");
    $order = tep_db_fetch_array($order_query);
    $id_now = $order['id'];
    // Canviem items ordre si cal
    if ($_POST['listorder'] > 0 && $_POST['listorder'] != $_POST['listorder_aux']) {
        tep_update_order_group($id_now, $_POST['listorder_aux'], $_POST['listorder'], TABLE_PRODUCTS_TO_CATEGORIES, 'categories_id', $current_category_id);
    }

    // Atributs: opcions producte, eliminem tots i tornem a introduir
    tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int) $aux_id . "'");

    $values_array = $_POST['values'];
    $options_id_array = $_POST['options_id'];
    $values_id_array = $_POST['values_id'];
    $price_array = $_POST['price'];
    $price_prefix_array = $_POST['price_prefix'];
    $weight_array = $_POST['weight'];
    $weight_prefix_array = $_POST['weight_prefix'];

    if (!is_array($values_array))
        $values_array = array();

    foreach ($values_array as $key => $value) {
        // Vector entrades
        $sql_data_array = array('products_id' => (int) $aux_id,
            'options_id' => tep_db_prepare_input($options_id_array[$key]),
            'values_id' => tep_db_prepare_input($values_id_array[$key]),
            'price' => tep_db_prepare_input($price_array[$key]),
            'price_prefix' => tep_db_prepare_input($price_prefix_array[$key]),
            'weight' => tep_db_prepare_input($weight_array[$key]),
            'weight_prefix' => tep_db_prepare_input($weight_prefix_array[$key]));
        // Guardem a BD
        tep_db_perform(TABLE_PRODUCTS_ATTRIBUTES, $sql_data_array);
    }


    //---------------FUNCIONS de l'espectacle-----------
    //actualitzem FUNCIONS 
    $id_funcio_array = $_POST['id_funcio'];
    $data_funcio_aux_array = $_POST['data_aux_funcio'];
    $hora_funcio_aux_array = $_POST['hora_aux_funcio'];
    $nova_aux_array = $_POST['nova_aux_hidden'];
    $funcions = tep_get_funcions_by_espectacle((int) $aux_id);
    $key = 0;
    //recorrem les funcions en BBDD i actualitzem els valors de les actuals
    while ($item = tep_db_fetch_array($funcions)) {

        if (!in_array((int) $item['id'], $id_funcio_array)) {  // Si no hi és eliminem
            tep_db_query("delete from " . TABLE_FUNCIONS . " where id = '" . (int) $item['id'] . "'");
        } else { //actualitzem
            // Vector entrades
            $sql_data_array = array(
                'data' => $data_funcio_aux_array[$key] . ' ' . $hora_funcio_aux_array[$key],
                'nova' => (int) $nova_aux_array[$key]);
            // Guardem a BD
            tep_db_perform(TABLE_FUNCIONS, $sql_data_array, 'update', 'id = \'' . (int) $item['id'] . '\'');
            $key++;
        }
    }

    //Insertem FUNCIONS noves
    $data_funcio_array = $_POST['data_funcio'];
    $hora_funcio_array = $_POST['hora_funcio'];
    $nova_array = $_POST['nova_hidden'];
    foreach ($data_funcio_array as $key => $value) {
        if ($key > 0) {
            // Vector entrades
            $sql_data_array = array('id_espectacle' => (int) $aux_id,
                'data' => $value . ' ' . $hora_funcio_array[$key],
                'nova' => (int) $nova_array[$key]); //la key dels checkboxes sempre keda un mennys
            // Guardem a BD
            tep_db_perform(TABLE_FUNCIONS, $sql_data_array);
        }
    }


    // -----------------------Descàrregues Adicionals: actualitzem -----------------
    $file_aux_download = $_POST['file_aux_download'];
    $download_aux_order = $_POST['download_aux_order'];
    $download_aux_titol = $_POST['download_aux_titol'];

    if (!is_array($file_aux_download))
        $file_aux_download = array();
    $download_files = tep_download_files_array(TABLE_PRODUCTS_DOWNLOADS, 'products_id', $aux_id);

    foreach ($download_files as $key => $val) {
        if (!in_array($val, $file_aux_download)) {  // Si no hi és eliminem
            // Valors auxiliars
            $file_aux = tep_get_download_files_name(TABLE_PRODUCTS_DOWNLOADS, $val);
            // Eliminar imatge
            $download_del = DIR_WS_PORTAL_DOWNLOADS . $file_aux;
            if (file_exists($download_del))
                @unlink($download_del);
            tep_db_query("delete from " . TABLE_PRODUCTS_DOWNLOADS . " where products_id = '" . (int) $aux_id . "' and id = '" . (int) $val . "'");
            tep_db_query("delete from " . TABLE_PRODUCTS_DOWNLOADS_DESCRIPTION . " where downloads_id = '" . (int) $val . "'");
        } else { // Sinó actualitzem: listorder !!!
            // Posició en el vector formulari
            $aux_key = array_search($val, $file_aux_download);
            // Insertem dades
            $sql_data_array = array('listorder' => tep_db_prepare_input($download_aux_order[$aux_key]));
            // Crida base dades
            tep_db_perform(TABLE_PRODUCTS_DOWNLOADS, $sql_data_array, 'update', 'products_id = \'' . (int) $aux_id . '\' and id = \'' . (int) $file_aux_download[$aux_key] . '\'');

            // Segons Idioma
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                $lang_aux = $languages[$i]['id'];
                // Insertem dades
                $sql_data_array = array('titol' => tep_db_prepare_input($download_aux_titol[$lang_aux][$aux_key]));
                // Crida base dades
                tep_db_perform(TABLE_PRODUCTS_DOWNLOADS_DESCRIPTION, $sql_data_array, 'update', 'downloads_id = \'' . (int) $val . '\' and language_id = \'' . (int) $lang_aux . '\'');
            }
        }
    }
    // Descàrregues adicionals: insertem nous!!!
    $file_download = $_FILES['file_download'];
    $download_order = $_POST['download_order'];
    $download_titol = $_POST['download_titol'];
    for ($key = 0; $key < sizeof($file_download['name']); $key++) { // Més petit, la última és la que es copia
        // Pugem arxiu
        $download_aux = $file_download['name'][$key];
        if (!empty($download_aux)) {
            if (is_uploaded_file($file_download['tmp_name'][$key])) {
                // Nom no repetit
                $download_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_aux);
                // Copiar
                move_uploaded_file($file_download['tmp_name'][$key], DIR_FS_PORTAL_DOWNLOADS . $download_aux);
            }
            // Insertem dades
            $sql_data_array = array('products_id' => (int) $aux_id,
                'download' => tep_db_prepare_input($download_aux),
                'listorder' => tep_db_prepare_input($download_order[$key]),
                'entered' => 'now()');
            // Crida base dades
            tep_db_perform(TABLE_PRODUCTS_DOWNLOADS, $sql_data_array);
            $download_id = tep_db_insert_id();

            // Dades segons idioma
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                $lang_aux = $languages[$i]['id'];
                // Insertem dades
                $sql_data_array = array('downloads_id' => tep_db_prepare_input($download_id),
                    'language_id' => (int) $lang_aux,
                    'titol' => tep_db_prepare_input($download_titol[$lang_aux][$key]));
                // Crida base dades
                tep_db_perform(TABLE_PRODUCTS_DOWNLOADS_DESCRIPTION, $sql_data_array);
            }
        }
    }

    // Productes relacionats: actualitzem
    $products_rel = array($aux_id);
    $products_rel_aux_id = $_POST['products_rel_aux_id'];
    if (!is_array($products_rel_aux_id))
        $products_rel_aux_id = array(); // Forcem que sigui un array amb algun valor

    $related_query = tep_db_query("select pr.related_id from " . TABLE_PRODUCTS_RELATED . " pr where pr.products_id = '" . (int) $aux_id . "'");
    while ($related_info = tep_db_fetch_array($related_query)) {
        $itemInfo = new objectInfo($related_info);
        // Si existeix...
        if (in_array($itemInfo->related_id, $products_rel_aux_id)) { // ...afegim id a vector relacionats
            $products_rel[] = $itemInfo->related_id;
        } else { // ...sinó borrem
            tep_db_query("delete from " . TABLE_PRODUCTS_RELATED . " where related_id = '" . (int) $itemInfo->related_id . "' and products_id = '" . (int) $aux_id . "'");
        }
    }
    // Productes relacionats: insertem nous!!!
    $products_rel_id = $_POST['products_rel_id'];
    foreach ($products_rel_id as $key => $value) {
        if ($key > 0 && $value > 0 && !in_array($value, $products_rel)) { // No agafem el primer, només ens serveix per clonar i tampoc si ja existeix
            // Actualitzem vector personal
            $products_rel[] = $value;
            // Vector entrades
            $sql_data_array = array('products_id' => (int) $aux_id,
                'related_id' => tep_db_prepare_input($products_rel_id[$key]),
                'entered' => 'now()');
            // Guardem a BD
            tep_db_perform(TABLE_PRODUCTS_RELATED, $sql_data_array);
        }
    }

    return $aux_id;
}

?>