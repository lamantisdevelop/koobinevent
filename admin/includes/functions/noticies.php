<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */

//------------------------------------ Noticies ------------------------------------//
// Obtenir titular
function tep_get_noticies_titular($id) {
    global $language_id;

    $query = tep_db_query("select distinct nd.noticies_id, nd.titular from " . TABLE_NOTICIES_DESCRIPTION . " nd where nd.noticies_id = '" . (int) $id . "' and nd.language_id = '" . $language_id . "'");
    $values = tep_db_fetch_array($query);
    return $values['titular'];
}

/**
 * Retorna la informacio d'una noticia segons id i idioma
 * @global int $language_id
 * @param int $id_noticia
 * @param int $language_id
 * @return mysqli_result
 */
function tep_get_noticia_by_id($id_noticia, $language_id = '') {
    //si no especifiquem idioma agafem l'actual
    if ($language_id == '') {
        global $language_id;
    }
    return tep_db_query("select notDesc.noticies_id, notDesc.titular, notDesc.text, notDesc.tags, noticies.fotos_groups_id 
            FROM " . TABLE_NOTICIES . " noticies 					
            JOIN " . TABLE_NOTICIES_DESCRIPTION . " notDesc ON notDesc.noticies_id = noticies.id 
            WHERE notDesc.noticies_id = '" . $id_noticia . "' AND notDesc.language_id = '" . $language_id . "'");
}






function tep_print_noticia_fitxa($fitxaInfo)
{
    ?>
    <div class="noticia fitxa">
        
        <div class="info_aux">
            <h1 class="titol capital"><?php echo $fitxaInfo->titular; ?></h1>
            <p class="data color0"><?php echo tep_date_short($fitxaInfo->published); ?></p>
            <div class="img_gal">
                <?php 
                if ($fitxaInfo->fotos_groups_id) 
                {
                    //CAS té una galeria assignada.
                    //podem utilitzar qualsevol impresio de galeria.
                   //tep_print_gallery_clearing_lightbox_foundation($fitxaInfo->fotos_groups_id,'thumbs3');
                   
                    $galeriaQuery = tep_get_fotos_galeria($fitxaInfo->fotos_groups_id);
                    $galeria = tep_db_fetch_array($galeriaQuery);
                    $fotoInfo = new objectInfo($galeria);
                            
                           
                    ?>
                    <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . '/' . $fotoInfo->image; ?>" 
                         alt="<?php echo $fotoInfo->title; ?>" 
                         data-caption="<?php echo $fotoInfo->title . ($fotoInfo->autor ? ' | &copy; ' . $fotoInfo->autor : '' ); ?>"/>
                    <?php
                }
                ?>
            </div>
            <div class="resum">
                <?php echo $fitxaInfo->text; ?>
            </div>
            <?php echo ((tep_not_null($fitxaInfo->download) && is_file(DIR_WS_PORTAL_DOWNLOADS . $fitxaInfo->download)) ? '<p id="download">' . TEXT_DOWNLOAD . ' <a href="' . DIR_WS_PORTAL_DOWNLOADS . $fitxaInfo->download . '" target="_blank">' . $fitxaInfo->download . ' (' . formatBytes(filesize(DIR_WS_PORTAL_DOWNLOADS . $fitxaInfo->download)) . ')</a></p>' : '');?>
            <div class="clear"></div>
            <div class="social">
                <iframe id="facebook" src="http://www.facebook.com/plugins/like.php?href=<?php echo tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'itemID=' . $fitxaInfo->id . '&item_title=' . $fitxaInfo->titular); ?>&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=recommend&amp;colorscheme=light&amp;font&amp;height=35" scrolling="no" frameborder="0"></iframe>
                <a id="twitter" href="https://twitter.com/share" class="twitter-share-button">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
            </div>
        </div>
    </div>
    <?php
}






/**
 * imprimim les noticies cercades per tags o string en format rows
 * els valors de la cerca venen del get
 * @param type $llargadaDesc llargada de la descripcio en string
 */
function tep_print_noticies_llistat_cerca_row($llargadaDesc)
{
    tep_print_noticies_llistat_cerca(true,$llargadaDesc);
}

/**
 * imprimim les noticies cercades per tags o string en format block ( nomes li ),
 * els valors de la cerca venen del get
 * @param type $llargadaDesc llargada de la descripcio en string
 */
function tep_print_noticies_llistat_cerca_block($llargadaDesc)
{
    tep_print_noticies_llistat_cerca(false,$llargadaDesc);
}

/** 
 * imprimim les ultimes noticies fins la data en format row
 * @param type $llargadaDesc llargada de la descripcio en string
 * @param type $date data maxima
 */
function tep_print_noticies_llistat_ultimes_by_date_row($date,$llargadaDesc,$maxDestacats)
{
    tep_print_noticies_llistat_ultimes_by_date($date,$llargadaDesc,true,false,$maxDestacats);
}

function tep_print_noticies_llistat_ultimes_by_date_row_inici($date,$llargadaDesc,$isInici,$maxDestacats)
{
    tep_print_noticies_llistat_ultimes_by_date($date,$llargadaDesc,true,$isInici,$maxDestacats);
}

/** 
 * imprimim les ultimes noticies fins la data en format block ( nomes li )
 * @param type $llargadaDesc llargada de la descripcio en string
 * @param type $date data maxima
 */
function tep_print_noticies_llistat_ultimes_by_date_block($date,$llargadaDesc,$maxDestacats)
{
    tep_print_noticies_llistat_ultimes_by_date($date,$llargadaDesc,false,$maxDestacats);
}

/**
 * imprimim les ultimes noticies segons data
 * @global int $language_id, id del idioma
 * @global type $_GET -> $_GET['itemID']
 * @param type $date data maxima
 * @param type $llargadaDesc llargada de la descripcio en string
 * @param type $isRow si es row = true si es blok = false
 */
function tep_print_noticies_llistat_ultimes_by_date($date,$llargadaDesc,$isRow = true, $isInici=false,$maxDestacats=true)
{
    global $language_id;
    global $_GET;

    $query_raw = "SELECT n.id, nd.titular, nd.text,n.fotos_groups_id, n.active, n.published, n.finished "
                . "FROM " . TABLE_NOTICIES . " n "
                . "LEFT JOIN " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.language_id = '" . (int) $language_id . "' "
                . "WHERE (nd.titular!='' and nd.text !='') "
                . "AND n.active = '1' "
                . "AND (n.published IS NULL or TO_DAYS(n.published) <= TO_DAYS('" . $date . "')) "
                . "AND (n.finished IS NULL or TO_DAYS(n.finished) > TO_DAYS('" . $date . "')) "
                . (($isInici) ? " AND portada=1 " : "")
                . "ORDER BY n.published desc, n.entered desc "
                . (($maxDestacats) ? ("LIMIT " . MAX_DESTACATS) : "") ;
    
    //echo $query_raw;
    
    $listing = tep_db_query($query_raw);
    $aux_numrows = tep_db_num_rows($listing);
    // Llistat segons busqueda
    $i = 0;
    while ($item = tep_db_fetch_array($listing))
    {
        
        if ($item['id'] != $_GET['itemID'])
        {
            $itemInfo = new objectInfo($item);

            //busquem foto de portada de galeria
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));

            // Data
            $aux_data = explode('-', $itemInfo->published);

            //traiem hora
            $aux_data2 = explode(' ', $aux_data[2]);

            $text_data = $day_text[date("w", strtotime($itemInfo->published))] . ' ' . $aux_data2[0] . ' ' . $month_text[$aux_data[1] - 1] . ' ' . date("Y", strtotime($itemInfo->published));

            $fldtext = tep_text_curt($itemInfo->text, $llargadaDesc);

            if($isRow && $isInici)
            {
                tep_print_noticia_row_inici($itemInfo, $fldtext,  null, false, ($i == ($aux_numrows-1)));
            }

            else if ($isRow)
            {
                tep_print_noticia_row($itemInfo, $fldtext,  null, false, ($i == ($aux_numrows-1)));
            }
            else
            {
                tep_print_noticia_bloc($itemInfo, $fldtext,  null, false);
            }
        }
        $i++;
    }
}

/**
 * imprimim un llistat de noticies, cercades, per tag, o totes ( a la fitxa de notis )
 * @global int $language_id id del idioma
 * @global type $_GET -> mes,any,tag,news-search,itemID
 * @param type $isRow
 * @param type $llargadaDesc
 */
function tep_print_noticies_llistat_cerca($isRow = true,$llargadaDesc)
{
    global $language_id;
    global $_GET;

    $listing_sql = " SELECT n.id,n.fotos_groups_id, nd.titular, nd.destacat, nd.text, n.active, n.published, n.entered "
                 . " FROM " . TABLE_NOTICIES . " n "
                 . " LEFT JOIN " . TABLE_NOTICIES_DESCRIPTION . " nd ON n.id = nd.noticies_id AND nd.titular<>'' AND nd.language_id = '" . (int) $language_id . "' ";
    
    if (tep_not_null($_GET['mes']) && tep_not_null($_GET['any']))
    {
        $where_str = " WHERE (TO_DAYS(NOW()) - TO_DAYS(n.published) >= 0) "
                   . " AND (n.finished IS NULL or TO_DAYS(n.finished) - TO_DAYS(NOW()) >= 0) "
                   . " AND n.active='1'  "
                   . " AND nd.text !='' "
                   . " AND MONTH(n.published) = '" . (int) $_GET['mes'] . "' "
                   . " AND YEAR(n.published) = '" . (int) $_GET['any'] . "' "
                   . " ORDER BY n.published desc, n.id DESC";
        
        $mostrar_primer_destacat = false;
    }
    else if (tep_not_null($_GET['tag']))
    {
        // Canviem guions per espais
        
        $_GET['tag'] = tep_db_input(str_replace("-", " ", $_GET['tag']));
        $tagLower = strtolower($_GET['tag']);
        $tagLower = trim($tagLower);
        
//        $where_str = " where ( LOWER(nd.tags) REGEXP '[\,]{1}[ \n]*" . $_GET['tag'] . "$' or  "
//                            . "LOWER(nd.tags) REGEXP '^" . $_GET['tag'] . "[ \n]*[\,]{1}' or  "
//                            . "LOWER(nd.tags) REGEXP '[\,]{1}[ \n]*" . $_GET['tag'] . "[ \n]*[\,]{1}' ) "
//                    . "and n.active='1'  and nd.text !='' order by n.published desc, n.id desc";
        
        $where_str = " WHERE  LOWER(nd.tags) LIKE '%".$tagLower."%'"
                    ." AND n.active='1'  "
                    ." AND nd.text !='' "
                    ." ORDER BY n.published DESC, n.id DESC";
        
        //echo $where_str;
                
        $mostrar_primer_destacat = false;
    }
    else if (tep_not_null($_GET['news-search']))
    {
        $mostrar_primer_destacat = false;
        // Condicions
        $keyword_str = "";
        if (tep_parse_search_string(stripslashes($_GET['news-search']), $search_keyword))
        {
            $keyword_str .= " and (";
            $size = sizeof($search_keyword);
            $aux_search = 0;
            for ($i = 0; $i < $size; $i++)
            { //Per cada paraula creem la cadena
                $keyword = $search_keyword[$i];
                switch ($keyword)
                {
                    case '(':
                    case ')':
                    case 'and':
                    case 'or':
                        $keyword_str .= " " . $keyword . " ";
                        $aux_search = 0;
                        break;
                    default:
                        $aux_search++;
                        if ($aux_search > 1){$keyword_str .= " AND ";}
                        $keyword_str .= " (nd.titular LIKE '%" . addslashes($keyword) . "%' "
                                      . " OR nd.titular REGEXP '[^&]" . addslashes($keyword) . "' "
                                      . " OR nd.text LIKE '%" . addslashes($keyword) . "%' "
                                       . "OR nd.text REGEXP '[^&]" . addslashes($keyword) . "'  )";
                        break;
                }
            }
            $keyword_str .= ") ";
            $where_str = " WHERE nd.text !='' " . $keyword_str . " "
                       . " AND (TO_DAYS(NOW()) - TO_DAYS(n.published) >= 0) "
                       . " AND (n.finished IS NULL or TO_DAYS(n.finished) - TO_DAYS(NOW()) >= 0) "
                       . " AND n.active='1'  and nd.text !='' "
                       . " ORDER BY n.published desc, n.id DESC";
        }
    } else
    {
        $where_str = " WHERE (TO_DAYS(NOW()) - TO_DAYS(n.published) >= 0) "
                   . " AND (n.finished IS NULL or TO_DAYS(n.finished) - TO_DAYS(NOW()) >= 0) "
                   . " AND n.active='1'  "
                   . " AND nd.text !='' "
                   . " ORDER BY n.published desc, n.id DESC";
    }
    
    $listing_sql = $listing_sql . $where_str;
    $listing = tep_db_query($listing_sql);
    $listing_numrows = tep_db_num_rows($listing);
    $page_numrows = tep_db_num_rows($listing);

    if ($listing_numrows > 0)
    {
        // Llistat segons búsqueda
        $i = 0;
        while ($item = tep_db_fetch_array($listing))
        {
            if ($item['id'] != $_GET['itemID'])
            {
                $i++;
                $aux = ($aux % 2) + 1;
                $itemInfo = new objectInfo($item);

                // Ressaltar resultats búsuqeda
                // Text busqueda titol
                $fldname = $itemInfo->titular;

                // Text busqueda text
                $fldtext = tep_text_curt($itemInfo->text, $llargadaDesc); //$itemInfo->descripcio;

                if (tep_not_null($newsSearch))
                {

                    if ($i == 1)
                    {//mostrem text buscat només una vegada
                        $newsSearch = urldecode($newsSearch);
                        echo('<h2>' . $newsSearch . '</h2>');
                    }

                    $fldtext = html_entity_decode($fldtext);

                    $string_to_explode = $_GET['news-search']; // is result LOWERCASE ?
                    $tok = preg_split('~ ~', $string_to_explode);
                    for ($k = 0; $k < sizeof($tok); $k++)
                    {
                        // minuscules
                        $fldtext = preg_replace("~" . strtolower($tok[$k]) . "(?!([^<]+)?>)~", '<span class="search_str">' . strtolower($tok[$k]) . '</span>', $fldtext);
                        // majúscules
                        $fldtext = preg_replace("~" . strtoupper($tok[$k]) . "(?!([^<]+)?>)~", '<span class="search_str">' . strtoupper($tok[$k]) . '</span>', $fldtext);
                        // 1º majuscula
                        $fldtext = preg_replace("~" . ucwords($tok[$k]) . "(?!([^<]+)?>)~", '<span class="search_str">' . ucwords($tok[$k]) . '</span>', $fldtext);
                    }
                }

                if ($isRow)
                {
                    tep_print_noticia_row($itemInfo, $fldtext,  isset($_GET['news-search']) ? $_GET['news-search'] : null, $mostrar_primer_destacat);
                }
                else
                {
                    tep_print_noticia_bloc($itemInfo, $fldtext,  isset($_GET['news-search']) ? $_GET['news-search'] : null, $mostrar_primer_destacat);
                }
            }
        }
    }
    else
    {
        // No Items
        echo '<div class="no_items fons_opac">' . _('No hi ha elements') . '</div>';
    }
}

/**
 * imprimim una noticia en format row
 * @param type $itemInfo objecte d' una fila de noticia
 * @param type $pageID id de la pagina
 * @param type $fldtext text de la noticia
 * @param type $newsSearch cerca (si s' escau)
 * @param type $mostrar_primer_destacat (si s' escau)
 */
function tep_print_noticia_row($itemInfo, $fldtext, $newsSearch=null, $mostrar_primer_destacat = false,$esUltim)
{
    echo $pageID;
    $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
    ?>
    <div class="row">
        <div class="columns large-12 medium-12">
            
            <div class="noticia <?php echo (($mostrar_primer_destacat) ? ' primer ' : ''); ?> <?php echo (($esUltim) ? ' ultim ' : ''); ?>">
                <div class="row">
                    <div class="columns large-3 medium-3 show-for-medium-up">
                        <a class="fade" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular . '&modul=noticies'); ?>">
                            <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs3'. '/' . $portada['image']; ?>" alt="<?php echo $itemInfo->titular; ?>"/>
                        </a>
                    </div>
                    <div class="columns sumari large-9 medium-9 ">
                        <h3>
                            <a class="fade" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular . '&modul=noticies'); ?>">
                                <?php echo $itemInfo->titular; ?>
                            </a>
                        </h3>
                        <a class="fade show-for-small" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular . '&modul=noticies'); ?>">
                            <img class="imatgeMobil" src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs3'. '/' . $portada['image']; ?>" alt="<?php echo $itemInfo->titular; ?>"/>
                        </a>
                        <span class="data"><?php echo tep_date_short($itemInfo->published); ?></span>
                        <p class="desc "><?php echo $fldtext; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function tep_print_noticia_row_inici($itemInfo, $fldtext, $newsSearch=null, $mostrar_primer_destacat = false,$esUltim = false)
{
    echo $pageID;
    $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
    ?>
    <div class="row">
        <div class="columns large-12 medium-12">
            <div class="noticia <?php echo (($mostrar_primer_destacat) ? ' primer ' : ''); ?> <?php echo (($esUltim) ? ' ultim ' : ''); ?>">
                <div class="row">
                    <div class="columns large-4 medium-4">
                        <a class="fade" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular . '&modul=noticies'); ?>">
                            <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs'. '/' . $portada['image']; ?>" alt="<?php echo $itemInfo->titular; ?>"/>
                        </a>
                    </div>
                    <div class="columns sumari large-8 medium-8 ">
                        <h3>
                            <a class="fade" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular . '&modul=noticies'); ?>">
                                <?php echo $itemInfo->titular; ?>
                            </a>
                        </h3>
                        <span class="data"><?php echo tep_date_short($itemInfo->published); ?></span>
                        <p class="desc "><?php echo $fldtext; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}

/**
 * imprimim una noticia en format blok ( li )
 * @param type $itemInfo objecte d' una fila de noticia
 * @param type $pageID id de la pagina
 * @param type $fldtext text de la noticia
 * @param type $newsSearch cerca (si s' escau)
 * @param type $mostrar_primer_destacat (si s' escau)
 */
function tep_print_noticia_bloc($itemInfo,$fldtext, $newsSearch=null, $mostrar_primer_destacat = false)
{
    $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
    ?>
    <li>
        <div class="noticia <?php echo (($mostrar_primer_destacat) ? ' primer ' : ''); ?>">
            <div class="row">
                <div class="columns large-12 medium-12">
                    <h3>
                        <a class="fade" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular . '&modul=noticies'); ?>">
                            <?php echo $itemInfo->titular; ?>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="columns large-12 medium-12">
                    <a class="fade" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular . '&modul=noticies'); ?>">
                        <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs3' . '/' . $portada['image']; ?>" alt="<?php echo $itemInfo->titular; ?>"/>
                    </a>
                </div>
           </div>
           <div class="row">     
                <div class="columns sumari large-12 medium-12">
                    
                    <span class="data"><?php echo tep_date_short($itemInfo->published); ?></span>
                    <p class="desc "><?php echo $fldtext; ?></p>
                </div>
            </div>
        </div>
    </li>
    <?php
}
