<?php

// Funcions admin: actuem sobre la base de dades
//------------------------------------ Config  ------------------------------------//
// Obtenir array llista
function tep_get_config_groups_array() {

    $values_array = array();
    $query = tep_db_query("select distinct id, name from " . TABLE_CONFIG_GROUPS . " order by listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

//------------------------------------ Ordenar: Simple  ------------------------------------//
// Obtenir nou ordre en una taula
function tep_search_new_id($db = '') {

    if (tep_not_null($db)) {
        $id_query = tep_db_query("select max(id) as max_id from " . $db);
        $id = tep_db_fetch_array($id_query);
        $id_now = $id['max_id'] + 1;
        return $id_now;
    }
}

function tep_search_new_order($db = '') {

    if (tep_not_null($db)) {
        $order_query = tep_db_query("select max(listorder) as max_order from " . $db);
        $order = tep_db_fetch_array($order_query);
        $order_now = $order['max_order'] + 1;
        return $order_now;
    }
}

// Obtenir ordre d'un element en una taula
function tep_search_item_order($id, $db = '') {

    if (tep_not_null($id) && tep_not_null($db)) {
        $order_query = tep_db_query("select listorder from " . $db . " where id = '" . (int) $id . "'");
        $order = tep_db_fetch_array($order_query);
        return $order['listorder'];
    }
}

// Reorganitzar ordres en una taula quan traiem un element
function tep_reorder_items($order, $db = '') {

    if (tep_not_null($order) && tep_not_null($db)) {
        $reorder_query = tep_db_query("select id, listorder from " . $db . " where listorder > " . (int) $order);
        while ($items = tep_db_fetch_array($reorder_query)) {
            tep_db_query("update " . $db . " set listorder= '" . (int) ($items['listorder'] - 1) . "' where id = '" . (int) $items['id'] . "'");
        }
    }
}

// Reorganitzar ordres dels fills d'un grup en una taula quan traiem un element 
function tep_reorder_items_fills($order, $parent_id, $db = '') {

    if (tep_not_null($order) && tep_not_null($db)) {
        $reorder_query = tep_db_query("select id, listorder from " . $db . " where listorder > " . (int) $order) . " AND parent_id=" . $parent_id;
        while ($items = tep_db_fetch_array($reorder_query)) {
            tep_db_query("update " . $db . " set listorder= '" . (int) ($items['listorder'] - 1) . "' where id = '" . (int) $items['id'] . "'");
        }
    }
}

// Cambiar ordres en una taula entre dos elements
function tep_update_order($id, $pos_ini, $pos_fi, $db = '') {

    // Busquem total items per evitar buits
    $order_info = tep_db_fetch_array(tep_db_query("select count(DISTINCT id) as total from " . $db . ""));

    if (tep_not_null($id) && tep_not_null($pos_ini) && tep_not_null($pos_fi) && tep_not_null($db)) {
        if ($pos_ini < $pos_fi) {
            if ($pos_fi > $order_info['total'])
                $pos_fi = $order_info['total'];
            $listing = tep_db_query("select id, listorder from " . $db . " where listorder>'" . $pos_ini . "' and listorder<='" . $pos_fi . "'");
            while ($item = tep_db_fetch_array($listing)) {
                $id_aux = $item['id'];
                $sql_data_array = array('listorder' => $item['listorder'] - 1);
                tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id_aux . '\'');
            }
            $sql_data_array = array('listorder' => $pos_fi);
            tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id . '\'');
        } else if ($pos_ini > $pos_fi) {
            if ($pos_fi < 1)
                $pos_fi = 1;
            $listing = tep_db_query("select id, listorder from " . $db . " where listorder>='" . $pos_fi . "' and listorder<'" . $pos_ini . "'");
            while ($item = tep_db_fetch_array($listing)) {
                $id_aux = $item['id'];
                $sql_data_array = array('listorder' => $item['listorder'] + 1);
                tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id_aux . '\'');
            }
            $sql_data_array = array('listorder' => $pos_fi);
            tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id . '\'');
        }
    }
}

// Cambiar ordres en una taula entre dos elements
function tep_change_order($id, $pos_ini, $pos_fi, $db = '') {

    if (tep_not_null($id) && tep_not_null($pos_ini) && tep_not_null($pos_fi) && tep_not_null($db)) {
        $id_query = tep_db_fetch_array(tep_db_query("select id from " . $db . " where listorder='" . $pos_fi . "' limit 1"));
        $id_aux = $id_query['id'];
        // Vectors entrades
        $sql1_data_array = array('listorder' => $pos_fi);
        $sql2_data_array = array('listorder' => $pos_ini);
        // Crida base dades
        tep_db_perform($db, $sql1_data_array, 'update', 'id = \'' . $id . '\'');
        tep_db_perform($db, $sql2_data_array, 'update', 'id = \'' . $id_aux . '\'');
    }
}

//------------------------------------ Ordenar: X Tipus  ------------------------------------//
// Buscar nou ordre
function tep_search_new_order_tipus($db = '', $tipus_id = '') {

    if (tep_not_null($db) && tep_not_null($tipus_id)) {
        $order_query = tep_db_query("select max(listorder) as max_order from " . $db . " where tipus_id = '" . (int) $tipus_id . "'");
        $order = tep_db_fetch_array($order_query);
        $order_now = $order['max_order'] + 1;
        return $order_now;
    }
}

// Obtenir ordre d'un element en una taula
function tep_search_item_order_tipus($id, $db = '', $tipus_id = '') {

    if (tep_not_null($id) && tep_not_null($db) && tep_not_null($tipus_id)) {
        $order_query = tep_db_query("select listorder from " . $db . " where id = '" . (int) $id . "' and tipus_id = '" . (int) $tipus_id . "'");
        $order = tep_db_fetch_array($order_query);
        return $order['listorder'];
    }
}

// Cambiar ordres en una taula entre dos elements
function tep_change_order_tipus($id, $pos_ini, $pos_fi, $db = '', $tipus_id = '') {

    if (tep_not_null($id) && tep_not_null($pos_ini) && tep_not_null($pos_fi) && tep_not_null($db) && tep_not_null($tipus_id)) {
        $id_query = tep_db_fetch_array(tep_db_query("select id from " . $db . " where listorder='" . $pos_fi . "' and tipus_id = '" . (int) $tipus_id . "' limit 1"));
        $id_aux = $id_query['id'];
        // Vectors entrades
        $sql1_data_array = array('listorder' => $pos_fi);
        $sql2_data_array = array('listorder' => $pos_ini);
        // Crida base dades
        tep_db_perform($db, $sql1_data_array, 'update', 'id = \'' . $id . '\'');
        tep_db_perform($db, $sql2_data_array, 'update', 'id = \'' . $id_aux . '\'');
    }
}

// Reorganitzar ordres en una taula quan traiem un element
function tep_reorder_items_tipus($order, $db = '', $tipus_id = '') {

    if (tep_not_null($order) && tep_not_null($db) && tep_not_null($tipus_id)) {
        $reorder_query = tep_db_query("select id, listorder from " . $db . " where tipus_id = '" . (int) $tipus_id . "' and listorder > " . (int) $order);
        while ($items = tep_db_fetch_array($reorder_query)) {
            tep_db_query("update " . $db . " set listorder= '" . (int) ($items['listorder'] - 1) . "' where id = '" . (int) $items['id'] . "'");
        }
    }
}

//------------------------------------ Ordenar: X Grup  ------------------------------------//
// Buscar nou ordre
function tep_search_new_order_group($db = '', $camp_group = '', $group_id = '') {

    if (tep_not_null($db) && tep_not_null($camp_group)) {
        $order_query = tep_db_query("select max(listorder) as max_order from " . $db . " where " . $camp_group . " = '" . (int) $group_id . "'");
        $order = tep_db_fetch_array($order_query);
        $order_now = $order['max_order'] + 1;
        return $order_now;
    }
}

// Obtenir ordre d'un element en una taula
function tep_search_item_order_group($id, $db = '', $camp_group = '', $group_id = '') {

    if (tep_not_null($id) && tep_not_null($db) && tep_not_null($camp_group)) {
        $order_query = tep_db_query("select listorder from " . $db . " where id = '" . (int) $id . "' and " . $camp_group . " = '" . (int) $group_id . "'");
        $order = tep_db_fetch_array($order_query);
        return $order['listorder'];
    }
}

// Cambiar ordres en una taula entre dos elements
function tep_change_order_group($id, $pos_ini, $pos_fi, $db = '', $camp_group = '', $group_id = '') {

    if (tep_not_null($id) && tep_not_null($pos_ini) && tep_not_null($pos_fi) && tep_not_null($db) && tep_not_null($camp_group)) {
        $id_query = tep_db_fetch_array(tep_db_query("select id from " . $db . " where listorder='" . $pos_fi . "' and " . $camp_group . " = '" . (int) $group_id . "' limit 1"));
        $id_aux = $id_query['id'];
        // Vectors entrades
        $sql1_data_array = array('listorder' => $pos_fi);
        $sql2_data_array = array('listorder' => $pos_ini);
        // Crida base dades
        tep_db_perform($db, $sql1_data_array, 'update', 'id = \'' . $id . '\'');
        tep_db_perform($db, $sql2_data_array, 'update', 'id = \'' . $id_aux . '\'');
    }
}

// Cambiar ordres en una taula entre dos elements
function tep_update_order_group($id, $pos_ini, $pos_fi, $db = '', $camp_group = '', $group_id = '') {

    // Busquem total items per evitar buits
    $order_info = tep_db_fetch_array(tep_db_query("select count(DISTINCT id) as total from " . $db . " where " . $camp_group . " = '" . (int) $group_id . "'"));

    if (tep_not_null($id) && tep_not_null($pos_ini) && tep_not_null($pos_fi) && tep_not_null($db) && tep_not_null($camp_group)) {
        if ($pos_ini < $pos_fi) {
            if ($pos_fi > $order_info['total'])
                $pos_fi = $order_info['total'];
            $listing = tep_db_query("select id, listorder from " . $db . " where listorder>'" . $pos_ini . "' and listorder<='" . $pos_fi . "' and " . $camp_group . " = '" . (int) $group_id . "'");
            while ($item = tep_db_fetch_array($listing)) {
                $id_aux = $item['id'];
                $sql_data_array = array('listorder' => $item['listorder'] - 1);
                tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id_aux . '\'');
            }
            $sql_data_array = array('listorder' => $pos_fi);
            tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id . '\'');
        } else if ($pos_ini > $pos_fi) {
            if ($pos_fi < 1)
                $pos_fi = 1;
            $listing = tep_db_query("select id, listorder from " . $db . " where listorder>='" . $pos_fi . "' and listorder<'" . $pos_ini . "' and " . $camp_group . " = '" . (int) $group_id . "'");
            while ($item = tep_db_fetch_array($listing)) {
                $id_aux = $item['id'];
                $sql_data_array = array('listorder' => $item['listorder'] + 1);
                tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id_aux . '\'');
            }
            $sql_data_array = array('listorder' => $pos_fi);
            tep_db_perform($db, $sql_data_array, 'update', 'id = \'' . $id . '\'');
        }
    }
}

// Reorganitzar ordres en una taula quan traiem un element
function tep_reorder_items_group($order, $db = '', $camp_group = '', $group_id = '') {

    if (tep_not_null($order) && tep_not_null($db) && tep_not_null($camp_group)) {
        $reorder_query = tep_db_query("select id, listorder from " . $db . " where " . $camp_group . " = '" . (int) $group_id . "' and listorder > " . (int) $order);
        while ($items = tep_db_fetch_array($reorder_query)) {
            tep_db_query("update " . $db . " set listorder= '" . (int) ($items['listorder'] - 1) . "' where id = '" . (int) $items['id'] . "'");
        }
    }
}

//------------------------------------ Mail ------------------------------------//
// Busquem si hi ha algun usuari amb el mateix mail
function tep_get_mail_exists($mail, $id = '0') {
    if (tep_not_null($mail)) {
        $mail_query = tep_db_query("select id from " . TABLE_USER . " where id != '" . (int) $id . "' and email like '" . $mail . "'");
        $query_numrows = tep_db_num_rows($mail_query);
        if ($query_numrows > 0) {
            return true;
        } else {
            return false;
        }
    }
}

// Busquem si hi ha algun usuari amb el mateix mail
function tep_get_email_id($mail) {
    if (tep_not_null($mail)) {
        $mail_query = tep_db_query("select id from " . TABLE_USER . " where email like '%" . tep_db_input($mail) . "%' limit 1");
        $email = tep_db_fetch_array($mail_query);
        return $email['id'];
    }
}

// Obtenir identificador únic
function getUniqid($table = "") {

    if (!$table) {
        $table = TABLE_USER;
    }
    # make sure it is really unique
    $id = md5(uniqid(mt_rand()));
    $req = tep_db_query("select id from " . $table . " where uniqid = '" . $id . "'");
    while (tep_db_num_rows($req)) {
        $id = md5(uniqid(mt_rand()));
        $req = tep_db_query("select id from " . $table . " where uniqid = '" . $id . "'");
    }
    return $id;
}

//------------------------------------ Pàgines ------------------------------------//
// Obtenir arbre pagines
function tep_get_pages_tree($parent_id = '0', $spacing = '', $exclude = '', $pages_tree_array = '', $include_itself = false) {
    global $language_id;

    if (!is_array($pages_tree_array))
        $pages_tree_array = array();
    if ((sizeof($pages_tree_array) < 1) && ($exclude != '0'))
        $pages_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {
        $pages_query = tep_db_query("select pd.titol from " . TABLE_PAGES_DESCRIPTION . " pd where pd.language_id = '" . $language_id . "' and pd.pages_id = '" . $parent_id . "'");
        $pages = tep_db_fetch_array($pages_query);
        $pages_tree_array[] = array('id' => $parent_id, 'text' => htmlentities($pages['titol'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    }

    $pages_query = tep_db_query("select p.id, pd.titol, p.parent_id from " . TABLE_PAGES . " p left join  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . $language_id . "'where p.parent_id = '" . $parent_id . "' order by p.listorder, pd.titol");
    while ($pages = tep_db_fetch_array($pages_query)) {
        if ($exclude != $pages['id'])
            $pages_tree_array[] = array('id' => $pages['id'], 'text' => $spacing . htmlentities($pages['titol'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
        $pages_tree_array = tep_get_pages_tree($pages['id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $pages_tree_array);
    }

    return $pages_tree_array;
}

function tep_get_subpages_array($parent_id = '0', $id_array = '') {

    if (!is_array($id_array))
        $id_array = array();

    $childs_query = tep_db_query("select p.id from " . TABLE_PAGES . " p where p.parent_id = '" . $parent_id . "' order by p.listorder");
    $childs_num = tep_db_num_rows($childs_query);
    if ($childs_num > 0) {
        while ($childs = tep_db_fetch_array($childs_query)) {
            $id_array[] = $childs['id'];
            $id_array = tep_get_subpages_array($childs['id'], $id_array);
        }
    }

    return $id_array;
}

function tep_has_subpages($parent_id = '0') {
    global $language_id;

    $pages_query = tep_db_query("select p.id, pd.titol, p.parent_id from " . TABLE_PAGES . " p left join  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . $language_id . "'where p.parent_id = '" . $parent_id . "' order by p.listorder, pd.titol");
    if ($pages = tep_db_fetch_array($pages_query)) {
        return true;
    } else {
        return false;
    }
}

function tep_get_page_titol($page_id = '0', $lang = '') {
    global $language_id;

    if (!tep_not_null($lang))
        $lang = $language_id;
    $query = tep_db_query("select p.id, pd.titol from " . TABLE_PAGES . " p left join  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . (int) $lang . "'where p.id = '" . (int) $page_id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['titol'];
}

function tep_get_page_title($page_id = '0', $lang = '') {
    global $language_id;

    if (!tep_not_null($lang))
        $lang = $language_id;
    $query = tep_db_query("select p.id, pd.page_title from " . TABLE_PAGES . " p left join  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . (int) $lang . "'where p.id = '" . (int) $page_id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['page_title'];
}

function tep_get_page_metas($page_id = '0') {
    global $language_id;

    $query = tep_db_query("select pd.pages_id, pd.titol, pd.title, pd.keywords, pd.description from " . TABLE_PAGES_DESCRIPTION . " pd where pd.language_id = '" . (int) $language_id . "' and pd.pages_id = '" . (int) $page_id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values;
}

function tep_get_admin_name($id) {
    // Busquem info administrador
    $query_raw = "select u.name from " . TABLE_ADMIN_USERS . " u where u.id = '" . (int) $id . "'";
    $item = tep_db_fetch_array(tep_db_query($query_raw));

    return $item['name'];
}

function tep_pages_history_insert($id, $text) {

    $sql_data_array = array('pages_id' => (int) $id,
        'summary' => tep_db_prepare_input(ucfirst($text)),
        'ip' => tep_db_prepare_input($_SERVER["REMOTE_ADDR"]),
        'date' => 'now()',
        'systeminfo' => tep_db_prepare_input($_SERVER["HTTP_USER_AGENT"]));

    tep_db_perform(TABLE_PAGES_HISTORY, $sql_data_array);
}

//------------------------------------ Menús  ------------------------------------//
// Obtenir arbre pagines
function tep_get_menu_pages_tree($menus_id, $parent_id = '0', $spacing = '', $exclude = '', $menu_pages_tree_array = '') {
    global $language_id;

    if (!is_array($menu_pages_tree_array))
        $menu_pages_tree_array = array();
    if ((sizeof($menu_pages_tree_array) < 1) && ($exclude != '0'))
        $menu_pages_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    $pages_query = tep_db_query("select p.id, pd.title, p.parent_id from " . TABLE_MENUS_PAGES . " p left join  " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . $language_id . "'where p.menus_id = '" . (int) $menus_id . "' and p.parent_id = '" . $parent_id . "' order by p.listorder, pd.title");
    while ($pages = tep_db_fetch_array($pages_query)) {
        if ($exclude != $pages['id'])
            $menu_pages_tree_array[] = array('id' => $pages['id'], 'text' => $spacing . htmlentities($pages['title'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
        $menu_pages_tree_array = tep_get_menu_pages_tree($menus_id, $pages['id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $menu_pages_tree_array);
    }

    return $menu_pages_tree_array;
}

//------------------------------------ Països i zones  ------------------------------------//

function tep_get_sectors($default = '') {
    global $languages_id;

    $sectors_array = array();
    if ($default) {
        $sectors_array[] = array('id' => '',
            'text' => $default);
    }
    $sectors_query = tep_db_query("select sectors_id, sectors_name from " . TABLE_SECTORS . " where language_id = '" . $languages_id . "' order by sort_order");
    while ($sectors = tep_db_fetch_array($sectors_query)) {
        $sectors_array[] = array('id' => $sectors['sectors_id'],
            'text' => $sectors['sectors_name']);
    }
    return $sectors_array;
}

function tep_get_countries($default = '') {

    $countries_array = array();
    if ($default) {
        $countries_array[] = array('id' => '',
            'text' => $default);
    }
    $countries_query = tep_db_query("select id, name from " . TABLE_COUNTRIES . " order by name");
    while ($countries = tep_db_fetch_array($countries_query)) {
        $countries_array[] = array('id' => $countries['id'],
            'text' => $countries['name']);
    }
    return $countries_array;
}

function tep_get_country_zones($countries_id) {

    $zones_array = array();
    $zones_query = tep_db_query("select id, name from " . TABLE_ZONES . " where countries_id = '" . $countries_id . "' order by name");
    while ($zones = tep_db_fetch_array($zones_query)) {
        $zones_array[] = array('id' => $zones['id'],
            'text' => $zones['name']);
    }
    return $zones_array;
}

function tep_prepare_country_zones_pull_down($countries_id = '') {

    $pre = '';
    if ((!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4'))) {
        for ($i = 0; $i < 45; $i++)
            $pre .= '&nbsp;';
    }

    $zones = tep_get_country_zones($countries_id);

    if (sizeof($zones) > 0) {
        $zones_select = array(array('id' => '', 'text' => TEXT_SELECT));
        $zones = tep_array_merge($zones_select, $zones);
    } else {
        $zones = array(array('id' => '', 'text' => TYPE_BELOW));
        // create dummy options for Netscape to preset the height of the drop-down
        if ((!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4'))) {
            for ($i = 0; $i < 9; $i++) {
                $zones[] = array('id' => '', 'text' => $pre);
            }
        }
    }
    return $zones;
}

function tep_get_country_name($country_id) {

    $country_query = tep_db_query("select name from " . TABLE_COUNTRIES . " where id = '" . $country_id . "'");
    if (!tep_db_num_rows($country_query)) {
        return $country_id;
    } else {
        $country = tep_db_fetch_array($country_query);
        return $country['name'];
    }
}

function tep_get_zone_name($zone_id) {

    $zone_query = tep_db_query("select name from " . TABLE_ZONES . " where id = '" . $zone_id . "'");
    if (!tep_db_num_rows($zone_query)) {
        return $zone_id;
    } else {
        $zone = tep_db_fetch_array($zone_query);
        return $zone['name'];
    }
}

function tep_get_zone_name_address($country_id, $zone_id, $default_zone) {

    $zone_query = tep_db_query("select name from " . TABLE_ZONES . " where countries_id = '" . $country_id . "' and id = '" . $zone_id . "'");
    if (tep_db_num_rows($zone_query)) {
        $zone = tep_db_fetch_array($zone_query);
        return $zone['name'];
    } else {
        return $default_zone;
    }
}

//------------------------------------ Adreça  ------------------------------------//

function tep_address_label($customers_id, $address_id = 1, $html = false, $boln = '', $eoln = "\n") {

    $address_query = tep_db_query("select firstname, lastname, nif, company, address, city, postcode, state, zone_id, country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . $customers_id . "' and address_id = '" . $address_id . "'");
    $address = tep_db_fetch_array($address_query);
    return tep_address_format($address, $html, $boln, $eoln);
}

function tep_address_format($address, $html, $boln, $eoln) {

    $address_format = '$company$name$cr$nif$street$cr$city, $postcode$cr$state ($country)';

    $company = addslashes(htmlentities($address['company'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    $name = addslashes(htmlentities($address['name'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    $nif = addslashes(htmlentities($address['nif'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    $firstname = addslashes(htmlentities($address['firstname'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    $lastname = addslashes(htmlentities($address['lastname'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    $street = addslashes(htmlentities($address['address'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    $city = addslashes(htmlentities($address['city'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    $state = addslashes($address['state']);
    $country_id = $address['country_id'];
    $zone_id = $address['zone_id'];
    $postcode = addslashes($address['postcode']);
    $zip = $postcode;
    $country = tep_get_country_name($country_id);
    $state = tep_get_zone_name_address($country_id, $zone_id, $state);

    if ($html) {
        // HTML Mode
        $HR = '<hr/>';
        $hr = '<hr/>';
        if (($boln == '') && ($eoln == "\n")) { // Values not specified, use rational defaults
            $CR = '<br/>';
            $cr = '<br/>';
            $eoln = $cr;
        } else { // Use values supplied
            $CR = $eoln . $boln;
            $cr = $CR;
        }
    } else {
        // Text Mode
        $CR = $eoln;
        $cr = $CR;
        $HR = '----------------------------------------';
        $hr = '----------------------------------------';
    }

    if (tep_not_null($company))
        $company = $company . '<br/>';
    if ($name == '')
        $name = $firstname . ' ' . $lastname;
    if (tep_not_null($nif))
        $nif = $nif . '<br/>';

    if ($country == '')
        $country = addslashes($address['country']);
    if ($state != '')
        $statecomma = $state . ', ';

    $fmt = $address_format;
    eval("\$address = \"$fmt\";");
    $address = stripslashes($address);

    return $boln . $address . $eoln;
}

function tep_address_label_top($customers_id, $address_id = 1, $html = false, $boln = '', $eoln = "\n") {

    $address_query = tep_db_query("select entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . $customers_id . "' and address_id = '" . $address_id . "'");
    $address = tep_db_fetch_array($address_query);
    return tep_address_format('1', $address, $html, $boln, $eoln);
}

function tep_address_summary($customers_id, $address_id) {

    $customers_id = tep_db_prepare_input($customers_id);
    $address_id = tep_db_prepare_input($address_id);

    $address_query = tep_db_query("select ab.entry_street_address, ab.entry_suburb, ab.entry_postcode, ab.entry_city, ab.entry_state, ab.entry_country_id, ab.entry_zone_id, c.countries_name, c.address_format_id from " . TABLE_ADDRESS_BOOK . " ab, " . TABLE_COUNTRIES . " c where ab.address_book_id = '" . tep_db_input($address_id) . "' and ab.customers_id = '" . tep_db_input($customers_id) . "' and ab.entry_country_id = c.countries_id");
    $address = tep_db_fetch_array($address_query);

    $street_address = $address['entry_street_address'];
    $suburb = $address['entry_suburb'];
    $postcode = $address['entry_postcode'];
    $city = $address['entry_city'];
    $state = tep_get_zone_name_address($address['entry_country_id'], $address['entry_zone_id'], $address['entry_state']);
    $country = $address['countries_name'];

    $address_format_query = tep_db_query("select address_summary from " . TABLE_ADDRESS_FORMAT . " where address_format_id = '" . $address['address_format_id'] . "'");
    $address_format = tep_db_fetch_array($address_format_query);

    $address_summary = $address_format['address_summary'];
    eval("\$address = \"$address_summary\";");
    return $address;
}

//------------------------------------ Galeria Imatges ------------------------------------//
// Obtenir array amb imatges per galeria
function tep_image_galery_array($db = '', $camp_id = '', $id = '') {

    $image_galery = array();
    if (tep_not_null($db) && tep_not_null($camp_id) && tep_not_null($id)) {
        $query = tep_db_query("select id from " . $db . " where " . $camp_id . "='" . (int) $id . "'");
        $query_numrows = tep_db_num_rows($query);
        if ($query_numrows > 0) {
            while ($item = tep_db_fetch_array($query)) {
                $image_galery[] = $item['id'];
            }
        }
    }
    return $image_galery;
}

/**
 * @desc retorna el nom d'un album (group fotos) actiu
 * @global type $language_id
 * @param type $gal_id
 * @param type $lang
 * @return string 
 */
function tep_get_title_galeria($gal_id, $lang = '') {
    global $language_id;

    if (!tep_not_null($lang))
        $lang = $language_id;
    $query = tep_db_query("select g.title FROM " . TABLE_FOTOS_GROUPS_DESCRIPTION . " g WHERE g.groups_id='" . (int) $gal_id . "' AND g.language_id = '" . (int) $lang . "'");
    $values = tep_db_fetch_array($query);
    return $values['title'];
}

/**
 * @desc retorna 0/1 si album inactiu/actiu
 * @param type $gal_id
 * @return int 
 */
function tep_get_album_active($gal_id) {

    if (!tep_not_null($lang))
        $lang = $language_id;
    $query = tep_db_query("select g.active FROM " . TABLE_FOTOS_GROUPS . " g WHERE g.id='" . (int) $gal_id . "' AND g.active=1");
    $values = tep_db_fetch_array($query);
    return $values['active'];
}

// Obtenir ordre imatge galeria
function tep_get_image_gal_name($db = '', $id = '') {

    if (tep_not_null($db) && tep_not_null($id)) {
        $name_query = tep_db_query("select image from " . $db . " where id = '" . (int) $id . "'");
        $name = tep_db_fetch_array($name_query);
        return $name['image'];
    }
}

// Obtenir ordre imatge galeria
function tep_get_image_gal_order($db = '', $id = '') {

    if (tep_not_null($db) && tep_not_null($id)) {
        $order_query = tep_db_query("select listorder from " . $db . " where id = '" . (int) $id . "'");
        $order = tep_db_fetch_array($order_query);
        return $order['listorder'];
    }
}

// Obtenir nou valor per ordre imatge galeria
function tep_search_image_gal_new_order($db = '', $camp_id = '', $id = '') {

    if (tep_not_null($db) && tep_not_null($camp_id) && tep_not_null($id)) {
        $order_query = tep_db_query("select max(listorder) as max_order from " . $db . " where " . $camp_id . "='" . (int) $id . "'");
        $order = tep_db_fetch_array($order_query);
        $order_now = $order['max_order'] + 1;
        return $order_now;
    }
}

// Reorganitzar ordres en una taula quan traiem un element
function tep_reorder_image_gal($db = '', $camp_id = '', $id = '', $order = '') {

    if (tep_not_null($order) && tep_not_null($db)) {
        $reorder_query = tep_db_query("select id, listorder from " . $db . " where " . $camp_id . "='" . (int) $id . "' and listorder > " . (int) $order);
        while ($items = tep_db_fetch_array($reorder_query)) {
            tep_db_query("update " . $db . " set listorder= '" . (int) ($items['listorder'] - 1) . "' where id = '" . (int) $items['id'] . "'");
        }
    }
}

//------------------------------------ Banners ------------------------------------//
// Obtenir nom
function tep_get_banners_grups_nom($id) {
    global $language_id;

    $query = tep_db_query("select distinct bt.id, bt.nom from " . TABLE_BANNERS_TIPUS . " bt where bt.id = '" . (int) $id . "'");
    $values = tep_db_fetch_array($query);
    return $values['nom'];
}

// Obtenir array llista
function tep_get_banners_grups_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct bg.id, bg.name from " . TABLE_BANNERS_GROUPS . " bg order by bg.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

//------------------------------------ Clients ------------------------------------//
// Obtenir array llista
function tep_get_clients_array($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct ci.id, ci.name from " . TABLE_CLIENTS_ITEMS . " ci order by ci.name");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

function tep_get_clients_name($id) {

    $query = tep_db_query("select distinct ci.id, ci.name from " . TABLE_CLIENTS_ITEMS . " ci where ci.id = '" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['name'];
}

//------------------------------------ Usuaris ------------------------------------//

function tep_get_users_activitats_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct ua.id, ua.name from " . TABLE_USERS_ACTIVITATS . " ua where ua.language_id = '" . (int) $language_id . "' order by ua.listorder ");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

/**
 * @desc retorna tots els rols d'usuari existents ordenats per nom
 * @return array
 */
function tep_get_admin_users_rols() {

    $values_array = array();
    $query = tep_db_query("select ur.id_rol, ur.nom from " . TABLE_ADMIN_USERS_ROLS . " ur order by ur.nom ");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id_rol'],
            'text' => $values['nom']);
    }
    return $values_array;
}

//------------------------------------ Usuaris Downloads ------------------------------------//

function tep_get_downloads_group_name($id) {
    global $language_id;

    $query = tep_db_query("select distinct dg.id, dgd.name from " . TABLE_DOWNLOADS_GROUPS . " dg left join " . TABLE_DOWNLOADS_GROUPS_DESCRIPTION . " dgd on dg.id = dgd.groups_id and dgd.language_id = '" . (int) $language_id . "' where dg.id = '" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['name'];
}

// Obtenir array amb valors de llistes subscrites
function tep_download_to_activitats_array($id) {

    $subscripcions = array();
    if (tep_not_null($id)) {
        $query = tep_db_query("select activitats_id from " . TABLE_DOWNLOADS_TO_ACTIVITATS . " where downloads_id = '" . (int) $id . "'");
        $query_numrows = tep_db_num_rows($query);
        if ($query_numrows > 0) {
            while ($item = tep_db_fetch_array($query)) {
                $subscripcions[] = $item['activitats_id'];
            }
        }
    }
    return $subscripcions;
}

//------------------------------------ Activitats ------------------------------------//
// Obtenir array llista
function tep_get_activitats_tipus_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct at.id, at.nom from " . TABLE_ACTIVITATS_TIPUS . " at where at.language_id = '" . (int) $language_id . "' order by at.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['nom']);
    }
    return $values_array;
}

function tep_get_activitats_nom($id,$language_id = false) {
      if (!$language_id) {
        global $language_id;
    }
    $query = tep_db_query("SELECT ad.activitats_id, ad.nom from " . TABLE_ACTIVITATS_DESCRIPTION . " ad "
                        . "WHERE ad.activitats_id = '" . (int) $id . "' "
                        . "AND ad.language_id = '" . (int) $language_id . "'limit 1");
    $values = tep_db_fetch_array($query);
    return $values['nom'];
}

//------------------------------------ llistats ------------------------------------//
// Obtenir nom
function tep_get_llibre_nom($id) {
    global $language_id;

    $query = tep_db_query("select ld.llistats_id, ld.nom from " . TABLE_LLISTATS_DESCRIPTION . " ld where ld.llistats_id = '" . (int) $id . "' and ld.language_id = '" . (int) $language_id . "'limit 1");
    $values = tep_db_fetch_array($query);
    return $values['nom'];
}

//------------------------------------ Newsletter ------------------------------------// 
//******** Usuaris **************//
// Obtenir nom
function tep_get_newsletter_user_name($value_id) {

    $query = tep_db_query("select name, surname from " . TABLE_NEWSLETTER_USERS . " where id = '" . (int) $value_id . "' limit 1");
    $value = tep_db_fetch_array($query);
    return (($value['surname']) ? $value['surname'] . ', ' : '') . $value['name'];
}

// Obtenir array amb valors de llistes subscrites to list ????
function tep_get_newsletter_user_list_array($userID) {

    $subscripcions = array();
    if (tep_not_null($userID)) {
        $query = tep_db_query("select listid from " . TABLE_USER_TO_LIST . " where userid = '" . (int) $userID . "'");
        $query_numrows = tep_db_num_rows($query);
        if ($query_numrows > 0) {
            while ($item = tep_db_fetch_array($query)) {
                $subscripcions[] = $item['listid'];
            }
        }
    }
    return $subscripcions;
}

// Historial usuaris newsletter
function tep_newsletter_users_history_insert($id, $text) {

    $sql_data_array = array('user_id' => (int) $id,
        'summary' => tep_db_prepare_input(ucfirst($text)),
        'ip' => tep_db_prepare_input($_SERVER["REMOTE_ADDR"]),
        'date' => 'now()',
        'systeminfo' => tep_db_prepare_input($_SERVER["HTTP_USER_AGENT"]));

    tep_db_perform(TABLE_NEWSLETTER_USERS_HISTORY, $sql_data_array);
}

//****** Llistes **********//

function tep_get_lists_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct l.id, l.name from " . TABLE_NEWSLETTER_LISTS . " l where l.language_id = '" . (int) $language_id . "' order by l.name");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

function tep_get_newsletter_list_name($value_id) {

    $query = tep_db_query("select name from " . TABLE_NEWSLETTER_LISTS . " where id = '" . (int) $value_id . "' limit 1");
    $value = tep_db_fetch_array($query);
    return $value['name'];
}

// Obtenir array amb valors de llistes subscrites
function tep_user_list_array($userID) {

    $subscripcions = array();
    if (tep_not_null($userID)) {
        $query = tep_db_query("select lists_id from " . TABLE_NEWSLETTER_USERS_TO_LISTS . " where users_id = '" . (int) $userID . "'");
        $query_numrows = tep_db_num_rows($query);
        if ($query_numrows > 0) {
            while ($item = tep_db_fetch_array($query)) {
                $subscripcions[] = $item['lists_id'];
            }
        }
    }
    return $subscripcions;
}

//****** Atributs **********//                            
// Obtenir nom taula attributes
function tep_get_table_attribute($id) {

    if (tep_not_null($id)) {
        $table_query = tep_db_query("select tablename from " . TABLE_NEWSLETTER_ATTRIBUTES . " where id = " . (int) $id);
        $table = tep_db_fetch_array($table_query);
        return $table['tablename'];
    }
}

// Obtenir vector valors attribut
function tep_get_attr_values($db = '', $default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    if (tep_not_null($db)) {
        $attr_db = "newsletter_attr_" . $db;
        $query = tep_db_query("select id, name from " . $attr_db . " where language_id = '" . (int) $language_id . "' order by listorder");
        while ($values = tep_db_fetch_array($query)) {
            $values_array[] = array('id' => $values['id'],
                'text' => $values['name']);
        }
    }
    return $values_array;
}

// Obtenir id valor atribut
function tep_get_attribute_id($value, $db = '') {

    if (tep_not_null($db)) {
        $attr_db = "newsletter_attr_" . $db;
        $query = tep_db_query("select id from " . $attr_db . " where name = '" . $value . "'");
        $value = tep_db_fetch_array($query);
        return $value['id'];
    } else {
        return 0;
    }
}

// Obtenir nom valor atribut
function tep_get_attribute_value_name($value_id, $db = '') {

    if (tep_not_null($db)) {
        $attr_db = "newsletter_attr_" . $db;
        $query = tep_db_query("select name from " . $attr_db . " where id = '" . (int) $value_id . "'");
        $value = tep_db_fetch_array($query);
        return $value['name'];
    } else {
        return $value_id;
    }
}

// Obtenir nom valor atribut
function tep_get_user_attribute_value($user_id, $attr_id) {

    if (tep_not_null($user_id)) {
        $query = tep_db_query("select value from " . TABLE_NEWSLETTER_USERS_TO_ATTRIBUTES . " where users_id = '" . (int) $user_id . "' and attributes_id = '" . (int) $attr_id . "'");
        $value = tep_db_fetch_array($query);
        return $value['value'];
    }
}

//********** Plantilles ********//
// Obtenir array llista
function tep_get_template_values($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select id, title from " . TABLE_NEWSLETTER_TEMPLATES . " order by listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['title']);
    }
    return $values_array;
}

// Obtenir nom plantilla
function tep_get_template_name($id) {

    $query = tep_db_query("select id, title from " . TABLE_NEWSLETTER_TEMPLATES . " where id = '" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['title'];
}

// Obtenir text plantilla
function tep_get_template_text($id) {

    $query = tep_db_query("select id, template from " . TABLE_NEWSLETTER_TEMPLATES . " where id = '" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['template'];
}

//********** Missatges ********//
// Obtenir array llista
function tep_get_message_values($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select id, entered, subject from " . TABLE_NEWSLETTER_MESSAGES . " order by entered desc");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => tep_date_Short($values['entered']) . ' - ' . $values['subject']);
    }
    return $values_array;
}

// Obtenir text missatge
function tep_get_message_text($id) {

    $query = tep_db_query("select id, message from " . TABLE_NEWSLETTER_MESSAGES . " where id = '" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['message'];
}

// Obtenir nom
function tep_get_message_name($value_id) {

    $query = tep_db_query("select subject from " . TABLE_NEWSLETTER_MESSAGES . " where id = '" . (int) $value_id . "' limit 1");
    $value = tep_db_fetch_array($query);
    return $value['subject'];
}

//********* Mailings *************//
// Obtenir array llista
function tep_get_mailing_values($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select id, title from " . TABLE_NEWSLETTER_MAILINGS . " order by entered desc");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['title']);
    }
    return $values_array;
}

//------------------------------------ Descàrregues Adicionals ------------------------------------//
// Obtenir array amb imatges per galeria
function tep_download_files_array($db = '', $camp_id = '', $id = '') {

    $download_files = array();
    if (tep_not_null($db) && tep_not_null($camp_id) && tep_not_null($id)) {
        $query = tep_db_query("select id from " . $db . " where " . $camp_id . "='" . (int) $id . "'");
        $query_numrows = tep_db_num_rows($query);
        if ($query_numrows > 0) {
            while ($item = tep_db_fetch_array($query)) {
                $download_files[] = $item['id'];
            }
        }
    }
    return $download_files;
}

// Obtenir ordre imatge galeria
function tep_get_download_files_name($db = '', $id = '') {

    if (tep_not_null($db) && tep_not_null($id)) {
        $name_query = tep_db_query("select download from " . $db . " where id = '" . (int) $id . "'");
        $name = tep_db_fetch_array($name_query);
        return $name['download'];
    }
}

?>
