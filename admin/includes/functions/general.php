<?php

// Recull de funcions que no depenen de la base de dades:
//------------------------------------ General  ------------------------------------//
// Redirecció
function tep_redirect($url) {
    global $logger;

    $url = str_replace('&amp;', '&', $url);  // Canviem &amp per &, ja que no escribim el link
    header('Location: ' . $url);
    if (STORE_PAGE_PARSE_TIME == 'true') {
        if (!is_object($logger))
            $logger = new logger;
        $logger->timer_stop();
    }
    exit;
}

// Obtenir tots els paràmetres passats per GET
function tep_get_all_get_params($exclude_array = '') {
    global $_GET;

    if ($exclude_array == '')
        $exclude_array = array();
    $get_url = '';
    reset($_GET);
    while (list($key, $value) = each($_GET)) {
        if ((tep_not_null($value)) && ($key != tep_session_name()) && ($key != 'error') && (!tep_in_array($key, $exclude_array)))
            $get_url .= $key . '=' . $value . '&amp;';
    }
    return $get_url;
}

// Obtenir informació del sistema
function tep_get_system_information() {
    global $HTTP_SERVER_VARS;

    $db_query = tep_db_query("select now() as datetime");
    $db = tep_db_fetch_array($db_query);

    list($system, $host, $kernel) = preg_split('/[\s,]+/', @exec('uname -a'), 5);

    return array('date' => tep_datetime_short(date('Y-m-d H:i:s')),
        'system' => $system,
        'kernel' => $kernel,
        'host' => $host,
        'ip' => gethostbyname($host),
        'uptime' => @exec('uptime'),
        'http_server' => $HTTP_SERVER_VARS['SERVER_SOFTWARE'],
        'php' => PHP_VERSION,
        'zend' => (function_exists('zend_version') ? zend_version() : ''),
        'db_server' => DB_SERVER,
        'db_ip' => gethostbyname(DB_SERVER),
        'db_version' => 'MySQL ' . tep_db_server_info(),
        'db_date' => tep_datetime_short($db['datetime']));
}

function tep_get_local_path($path) {

    if (substr($path, -1) == '/')
        $path = substr($path, 0, -1);
    return $path;
}

function tep_call_function($function, $parameter, $object = '') {

    if ($object == '') {
        return call_user_func($function, $parameter);
    } elseif (PHP_VERSION < 4) {
        return call_user_method($function, $object, $parameter);
    } else {
        return call_user_func(array($object, $function), $parameter);
    }
}

function tep_class_exists($class_name) {

    if (function_exists('class_exists')) {
        return class_exists($class_name);
    } else {
        return true;
    }
}

function tep_reset_cache_block($cache_block) {
    global $cache_blocks;

    for ($i = 0, $n = sizeof($cache_blocks); $i < $n; $i++) {
        if ($cache_blocks[$i]['code'] == $cache_block) {
            if ($cache_blocks[$i]['multiple']) {
                if ($dir = @opendir(DIR_FS_CACHE)) {
                    while ($cache_file = readdir($dir)) {
                        $cached_file = $cache_blocks[$i]['file'];
                        $languages = tep_get_languages();
                        for ($j = 0, $k = sizeof($languages); $j < $k; $j++) {
                            $cached_file_unlink = ereg_replace('-language', '-' . $languages[$j]['directory'], $cached_file);
                            if (ereg('^' . $cached_file_unlink, $cache_file)) {
                                @unlink(DIR_FS_CACHE . $cache_file);
                            }
                        }
                    }
                    closedir($dir);
                }
            } else {
                $cached_file = $cache_blocks[$i]['file'];
                $languages = tep_get_languages();
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $cached_file = ereg_replace('-language', '-' . $languages[$i]['directory'], $cached_file);
                    @unlink(DIR_FS_CACHE . $cached_file);
                }
            }
            break;
        }
    }
}

function link_get_variable($var_name) {

    // Map global to GET variable
    if (isset($_GET[$var_name])) {
        $GLOBALS[$var_name] = & $_GET[$var_name];
    }
}

function link_post_variable($var_name) {

    // Map global to POST variable
    if (isset($_POST[$var_name])) {
        $GLOBALS[$var_name] = & $_POST[$var_name];
    }
}

// Detectar navegador
function tep_browser_detect($component) {

    return stristr($_SERVER['HTTP_USER_AGENT'], $component);
}

// Obtenir nom navegador o robot
function tep_get_agent_name($user_agent) {
    $navegadores = array(
        // Navegadors
        'Opera' => 'Opera',
        'Mozilla Firefox' => '(Firebird)|(Firefox)',
        'Safari' => 'Safari',
        'Galeon' => 'Galeon',
        'Mozilla' => 'Gecko',
        'MyIE' => 'MyIE',
        'Lynx' => 'Lynx',
        'Netscape' => '(Mozilla\/4\.75)|(Netscape6)|(Mozilla\/4\.08)|(Mozilla\/4\.5)|(Mozilla\/4\.6)|(Mozilla\/4\.79)',
        'Konqueror' => 'Konqueror',
        'Internet Explorer 7' => '(MSIE 7\.[0-9]+)',
        'Internet Explorer 6' => '(MSIE 6\.[0-9]+)',
        'Internet Explorer 5' => '(MSIE 5\.[0-9]+)',
        'Internet Explorer 4' => '(MSIE 4\.[0-9]+)',
        // Robots
        'Google' => 'Google',
        'Yahoo!' => 'Yahoo',
        'MSN Search' => 'msnbot',
        'Altavista' => 'Scooter',
        'Snapbot' => 'Snapbot',
        'YodaoBot' => 'YodaoBot');

    // Si el troba a la llista el retorna
    foreach ($navegadores as $navegador => $pattern) {
        if (preg_match('/^' . $pattern . '/', $user_agent))
            return $navegador;
    }
    // Si no retornem variable enviada
    return $user_agent;
}

//------------------------------------ Configuració  ------------------------------------//

function tep_cfg_select_option($select_array, $key_value, $key = '') {

    for ($i = 0, $n = sizeof($select_array); $i < $n; $i++) {
        $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');
        $string .= '<br><input type="radio" name="' . $name . '" value="' . $select_array[$i] . '"';
        if ($key_value == $select_array[$i])
            $string .= ' CHECKED';
        $string .= '> ' . $select_array[$i];
    }
    return $string;
}

function tep_mod_select_option($select_array, $key_name, $key_value) {

    reset($select_array);
    while (list($key, $value) = each($select_array)) {
        if (is_int($key))
            $key = $value;
        $string .= '<br><input type="radio" name="configuration[' . $key_name . ']" value="' . $key . '"';
        if ($key_value == $key)
            $string .= ' CHECKED';
        $string .= '> ' . $value;
    }
    return $string;
}

//------------------------------------ PERMISOS USUARIS  ------------------------------------//
/**
 * @desc mira si l'usuari actual te permisos per el modul passat (1-lectura, 2-Escriptura, 3-Esborrar)
 * @param string $modul
 * @param string $permis //per defecte 1(lectura)
 * @param string $rol
 * @return bool 
 */
function tep_usuaris_mirar_permis_modul($modul, $permis = PERMIS_LECTURA, $rol = 0) {

    //si no passem rol, agafem l'actual de sessio
    $rol = $rol == 0 ? $_SESSION['customer_id_rol'] : $rol;

    //obtenim permisos a la bd
    if (!is_null($rol)) {
        $item = tep_db_fetch_array(tep_db_query("SELECT * FROM " . TABLE_ADMIN_USERS_ROLS_PERMISOS . "
                WHERE id_rol = " . $rol . " 
                AND nom_modul='" . $modul . "' 
                AND id_tipus_permis =" . $permis . " 
                AND flag=1"));
    }
    return is_array($item);
}

/**
 * @desc mira si l'usuari actual te permisos per al modul actual. En cas negatiu mostra error
 * @global type $messageStack
 * @param type $permis 
 */
function tep_usuaris_filtrar_acces($permis) {
    global $messageStack;
    //Mirar permisos   
    if (!tep_usuaris_mirar_permis_modul($_GET['modul'], $permis)) {
        // Redirecció
        $messageStack->add_session(sprintf(ERROR_PERMISOS, $_POST['id_tipus_permis']), 'error');
        tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
    }
}

/**
 * @desc retorna tots els tipus de permisos de la BD
 * @return array 
 */
function tep_usuaris_get_all_tipus_permisos() {

    return tep_db_query("SELECT * FROM " . TABLE_ADMIN_USERS_TIPUS_PERMISOS);
}

/**
 * Mira si l'usuari actual es administrador
 * @return bool
 */
function tep_usuaris_es_admin() {
    return $_SESSION['customer_id_rol'] == 1;
}

//------------------------------------ Moduls ------------------------------------//
/**
 * @desc retorna un llistat de totes les carpetes que hi ha dins del directori /moduls
 * @return array 
 */
function tep_moduls_get_all() {
    $dir = DIR_FS_ADMIN . '/moduls/';
    //busquem els noms dels moduls basant-nos amb els directoris de la carpeta /moduls
    $iterator = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($dir),
                    RecursiveIteratorIterator::SELF_FIRST);
    foreach ($iterator as $file) {
        if ($file->isDir()) {
            $moduls[$file->getFilename()] = $file->getFilename();
        }
    }
    //afegim moduls que no son directoris, manualment (PER EL MENU LATERAL DE L'ADMIN)
    $moduls['informes_pare_menu_lateral'] = 'informes_pare_menu_lateral'; //per al pare del menu NEWSLETTER
    $moduls['cataleg_pare_menu_lateral'] = 'cataleg_pare_menu_lateral'; //per al pare del menu NEWSLETTER
    $moduls['usuarios_pare_menu_lateral'] = 'usuarios_pare_menu_lateral'; //per al pare del menu NEWSLETTER
    $moduls['newsletter_pare_menu_lateral'] = 'newsletter_pare_menu_lateral'; //per al pare del menu NEWSLETTER
    $moduls['newsletter_users_pare_menu_lateral'] = 'newsletter_users_pare_menu_lateral'; //per al pare del menu USUARIS NEWSLETTER
    $moduls['users_pare_menu_lateral'] = 'users_pare_menu_lateral'; //per al pare del menu GESTIO USUARIS
    $moduls['tools_pare_menu_lateral'] = 'tools_pare_menu_lateral'; //per al pare del menu EINES
    $moduls['server_info'] = 'server_info';
    $moduls['estadistiques'] = 'estadistiques';
    $moduls['config_pare_menu_lateral'] = 'config_pare_menu_lateral'; //per al pare del menu CONFIGURACIO
    //ordenem alfabetic
    asort($moduls, SORT_STRING);
    return $moduls;
}

//------------------------------------ Password  ------------------------------------//
// Crear password de forma aleatoria
function tep_create_random_value($length, $type = 'mixed') {

    if (($type != 'mixed') && ($type != 'chars') && ($type != 'digits'))
        return false;

    $rand_value = '';
    while (strlen($rand_value) < $length) {
        if ($type == 'digits') {
            $char = tep_rand(0, 9);
        } else {
            $char = chr(tep_rand(0, 255));
        }
        if ($type == 'mixed') {
            if (eregi('^[a-z0-9]$', $char))
                $rand_value .= $char;
        } elseif ($type == 'chars') {
            if (eregi('^[a-z]$', $char))
                $rand_value .= $char;
        } elseif ($type == 'digits') {
            if (ereg('^[0-9]$', $char))
                $rand_value .= $char;
        }
    }
    return $rand_value;
}

// Encriptar password
function tep_encrypt_password($plain) {

    $password = '';
    for ($i = 0; $i < 10; $i++) {
        $password .= tep_rand();
    }
    $salt = substr(md5($password), 0, 2);
    $password = md5($salt . $plain) . ':' . $salt;

    return $password;
}

// Validar password
function validate_password($plain_pass, $db_pass) {

    if ($plain_pass == $db_pass)
        return (true);

    if (!($subbits = preg_split("/:/", $db_pass, 2)))
        return (false);

    $dbpassword = $subbits[0];
    $salt = $subbits[1];

    $passtring = $salt . $plain_pass;

    $encrypted = md5($passtring);
    if (strcmp($dbpassword, $encrypted) == 0) {
        return (true);
    } else {
        return (false);
    }
}

// Validar password
function tep_validate_password($plain, $encrypted) {

    if (tep_not_null($plain) && tep_not_null($encrypted)) {
        $stack = explode(':', $encrypted);
        if (sizeof($stack) != 2)
            return false;
        if (md5($stack[1] . $plain) == $stack[0]) {
            return true;
        }
    }
    return false;
}

//------------------------------------ E-mail  ------------------------------------//
// Email vàlid
function is_email($email) {

    $atomchar_re = "[a-z0-9!#$%&'*+\\/=?^_`{|}~-]";
    $escape_re = "(\\\\.)";
    $word_re = "(" . $atomchar_re . "|" . $escape_re . ")+";
    $local_re = $word_re . "(\\." . $word_re . ")*";
    $email_re =
            "/^" .
            "(" .
            // Local part is atoms or escaped chars, separated by dots
            $local_re . "|" .
            // or a quoted string
            "\"[^\"]*\"" .
            ")" .
            // Need an at-sign!
            "@" .
            "(" .
            // Domain is anything ending with a dot and 2-4 letters.
            "([a-z0-9.-]+)(\\.[a-z]{2,4})" .
            ")" .
            "$/i";

    // Try to match the email address
    return preg_match($email_re, $email);
}

// Intentar arreglar auxtomàticament mail
function fixEmail($email) {
    if (preg_match("#(.*)@.*hotmail.*#i", $email, $regs)) {
        $email = $regs[1] . '@hotmail.com';
    }
    if (preg_match("#(.*)@.*aol.*#i", $email, $regs)) {
        $email = $regs[1] . '@aol.com';
    }
    if (preg_match("#(.*)@.*yahoo.*#i", $email, $regs)) {
        $email = $regs[1] . '@yahoo.com';
    }
    $email = str_replace(" ", "", $email);
    $email = preg_replace("#,#", ".", $email);
    $email = str_replace("\.", ".", $email);
    $email = str_replace("..", ".", $email);
    $email = preg_replace("#\.cpm$#i", "\.com", $email);
    $email = preg_replace("#\.couk$#i", "\.co\.uk", $email);
    return $email;
}

//------------------------------------ Fitxers  ------------------------------------//
// Canviar nom arxiu si existeix
function rename_if_exists($dir, $filename) {

    $filename = strtolower($filename);

    // Treure accents
    $filename = strtr($filename, 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    $filename = str_replace(" ", "-", $filename);

    $ext = strrchr($filename, '.');
    $prefix = substr($filename, 0, -strlen($ext));
    $i = 0;
    while (file_exists($dir . $filename)) { // If file exists, add a number to it.
        $filename = $prefix . ++$i . $ext;
    }
    return $filename;
}

// Obtenir informació d'un fitxer pujat
function tep_get_uploaded_file($filename) {

    if (isset($_FILES[$filename])) {
        $uploaded_file = array('name' => $_FILES[$filename]['name'],
            'type' => $_FILES[$filename]['type'],
            'size' => $_FILES[$filename]['size'],
            'tmp_name' => $_FILES[$filename]['tmp_name']);
    } elseif (isset($GLOBALS['HTTP_POST_FILES'][$filename])) {
        global $HTTP_POST_FILES;

        $uploaded_file = array('name' => $HTTP_POST_FILES[$filename]['name'],
            'type' => $HTTP_POST_FILES[$filename]['type'],
            'size' => $HTTP_POST_FILES[$filename]['size'],
            'tmp_name' => $HTTP_POST_FILES[$filename]['tmp_name']);
    } else {
        $uploaded_file = array('name' => $GLOBALS[$filename . '_name'],
            'type' => $GLOBALS[$filename . '_type'],
            'size' => $GLOBALS[$filename . '_size'],
            'tmp_name' => $GLOBALS[$filename]);
    }
    return $uploaded_file;
}

// Copiar fitxer pujat al destí
function tep_copy_uploaded_file($filename, $target) {

    if (substr($target, -1) != '/')
        $target .= '/';
    $target .= $filename['name'];
    move_uploaded_file($filename['tmp_name'], $target);
}

//  Eliminar fitxer
function tep_remove($source) {
    global $messageStack, $tep_remove_error;

    if (isset($tep_remove_error))
        $tep_remove_error = false;

    if (is_dir($source)) {
        $dir = dir($source);
        while ($file = $dir->read()) {
            if (($file != '.') && ($file != '..')) {
                if (is_writeable($source . '/' . $file)) {
                    tep_remove($source . '/' . $file);
                } else {
                    $messageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source . '/' . $file), 'error');
                    $tep_remove_error = true;
                }
            }
        }
        $dir->close();

        if (is_writeable($source)) {
            rmdir($source);
        } else {
            $messageStack->add(sprintf(ERROR_DIRECTORY_NOT_REMOVEABLE, $source), 'error');
            $tep_remove_error = true;
        }
    } else {
        if (is_writeable($source)) {
            unlink($source);
        } else {
            $messageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source), 'error');
            $tep_remove_error = true;
        }
    }
}

// Obtenir permisos d'un fitxer
function tep_get_file_permissions($mode) {
    // determine type
    if (($mode & 0xC000) == 0xC000) { // unix domain socket
        $type = 's';
    } elseif (($mode & 0x4000) == 0x4000) { // directory
        $type = 'd';
    } elseif (($mode & 0xA000) == 0xA000) { // symbolic link
        $type = 'l';
    } elseif (($mode & 0x8000) == 0x8000) { // regular file
        $type = '-';
    } elseif (($mode & 0x6000) == 0x6000) { //bBlock special file
        $type = 'b';
    } elseif (($mode & 0x2000) == 0x2000) { // character special file
        $type = 'c';
    } elseif (($mode & 0x1000) == 0x1000) { // named pipe
        $type = 'p';
    } else { // unknown
        $type = '?';
    }

    // determine permissions
    $owner['read'] = ($mode & 00400) ? 'r' : '-';
    $owner['write'] = ($mode & 00200) ? 'w' : '-';
    $owner['execute'] = ($mode & 00100) ? 'x' : '-';
    $group['read'] = ($mode & 00040) ? 'r' : '-';
    $group['write'] = ($mode & 00020) ? 'w' : '-';
    $group['execute'] = ($mode & 00010) ? 'x' : '-';
    $world['read'] = ($mode & 00004) ? 'r' : '-';
    $world['write'] = ($mode & 00002) ? 'w' : '-';
    $world['execute'] = ($mode & 00001) ? 'x' : '-';

    // adjust for SUID, SGID and sticky bit
    if ($mode & 0x800)
        $owner['execute'] = ($owner['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x400)
        $group['execute'] = ($group['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x200)
        $world['execute'] = ($world['execute'] == 'x') ? 't' : 'T';

    return $type .
            $owner['read'] . $owner['write'] . $owner['execute'] .
            $group['read'] . $group['write'] . $group['execute'] .
            $world['read'] . $world['write'] . $world['execute'];
}

// Forçar descàrrega
function tep_force_download($path, $export_file) {

    if (is_file($path)) {
        $size = filesize($path);
        if (function_exists('mime_content_type')) {
            $type = mime_content_type($path);
        } else if (function_exists('finfo_file')) {
            $info = finfo_open(FILEINFO_MIME);
            $type = finfo_file($info, $path);
            finfo_close($info);
        }
        if ($type == '') {
            $type = "application/force-download";
        }
        // Set Headers
        header("Content-Type: $type");
        header("Content-Disposition: attachment; filename=$export_file");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . $size);
        // Download File
        readfile($path);
    }
}

//------------------------------------ Cadena  ------------------------------------//
// Comprovar si una cadena és buida
function tep_not_null($value) {

    if (is_array($value)) {
        if (sizeof($value) > 0) {
            return true;
        } else {
            return false;
        }
    } else {
        // Etiquetes HTML no compten
        if (($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0)) { // Si és zero tampoc
            return true;
        } else {
            return false;
        }
    }
}

// Convertir cadena a enter
function tep_string_to_int($string) {

    return (int) $string;
}

// Dividir cadena
function tep_break_string($string, $len, $break_char = '-') {

    $l = 0;
    $output = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        if ($char != ' ') {
            $l++;
        } else {
            $l = 0;
        }
        if ($l > $len) {
            $l = 1;
            $output .= $break_char;
        }
        $output .= $char;
    }
    return $output;
}

function tep_parse_search_string($search_str = '', &$objects) {
    $search_str = trim(strtolower($search_str));

    // Break up $search_str on whitespace; quoted string will be reconstructed later
    $pieces = preg_split('/[[:space:]]+/', $search_str);

    $objects = array();
    $tmpstring = '';
    $flag = '';

    for ($k = 0; $k < count($pieces); $k++) {
        while (substr($pieces[$k], 0, 1) == '(') {
            $objects[] = '(';
            if (strlen($pieces[$k]) > 1) {
                $pieces[$k] = substr($pieces[$k], 1);
            } else {
                $pieces[$k] = '';
            }
        }
        $post_objects = array();
        while (substr($pieces[$k], -1) == ')') {
            $post_objects[] = ')';
            if (strlen($pieces[$k]) > 1) {
                $pieces[$k] = substr($pieces[$k], 0, -1);
            } else {
                $pieces[$k] = '';
            }
        }
        // Check individual words
        if ((substr($pieces[$k], -1) != '"') && (substr($pieces[$k], 0, 1) != '"')) {
            $objects[] = trim($pieces[$k]);
            for ($j = 0; $j < count($post_objects); $j++) {
                $objects[] = $post_objects[$j];
            }
        } else {
            $tmpstring = '';
            $tmpstring .= trim(ereg_replace('"', ' ', $pieces[$k]));
            // Check for one possible exception to the rule. That there is a single quoted word.
            if (substr($pieces[$k], -1) == '"') {
                // Turn the flag off for future iterations
                $flag = 'off';
                $objects[] = trim($pieces[$k]);
                for ($j = 0; $j < count($post_objects); $j++) {
                    $objects[] = $post_objects[$j];
                }
                unset($tmpstring);
                // Stop looking for the end of the string and move onto the next word.
                continue;
            }
            // Otherwise, turn on the flag to indicate no quotes have been found attached to this word in the string.
            $flag = 'on';
            // Move on to the next word
            $k++;
            // Keep reading until the end of the string as long as the $flag is on
            while (($flag == 'on') && ($k < count($pieces))) {
                while (substr($pieces[$k], -1) == ')') {
                    $post_objects[] = ')';
                    if (strlen($pieces[$k]) > 1) {
                        $pieces[$k] = substr($pieces[$k], 0, -1);
                    } else {
                        $pieces[$k] = '';
                    }
                }
                // If the word doesn't end in double quotes, append it to the $tmpstring.
                if (substr($pieces[$k], -1) != '"') {
                    // Tack this word onto the current string entity
                    $tmpstring .= ' ' . $pieces[$k];
                    // Move on to the next word
                    $k++;
                    continue;
                } else {
                    $tmpstring .= ' ' . trim(ereg_replace('"', ' ', $pieces[$k]));
                    // Push the $tmpstring onto the array of stuff to search for
                    $objects[] = trim($tmpstring);
                    for ($j = 0; $j < count($post_objects); $j++) {
                        $objects[] = $post_objects[$j];
                    }
                    unset($tmpstring);
                    // Turn off the flag to exit the loop
                    $flag = 'off';
                }
            }
        }
    }
    // add default logical operators if needed
    $temp = array();
    for ($i = 0; $i < (count($objects) - 1); $i++) {
        $temp[sizeof($temp)] = $objects[$i];

        if (($objects[$i] != 'and') &&
                ($objects[$i] != 'or') &&
                ($objects[$i] != '(') &&
                ($objects[$i] != ')') &&
                ($objects[$i + 1] != 'and') &&
                ($objects[$i + 1] != 'or') &&
                ($objects[$i + 1] != '(') &&
                ($objects[$i + 1] != ')')) {
            $temp[sizeof($temp)] = SEARCH_DEFAULT_OPERATOR;
        }
    }
    $temp[sizeof($temp)] = $objects[$i];
    $objects = $temp;

    $keyword_count = 0;
    $operator_count = 0;
    for ($i = 0; $i < count($objects); $i++) {
        if (($objects[$i] == 'and') || ($objects[$i] == 'or')) {
            $operator_count++;
        } elseif (($objects[$i]) && ($objects[$i] != '(') && ($objects[$i] != ')')) {
            $keyword_count++;
        }
    }

    if ($operator_count < $keyword_count) {
        return true;
    } else {
        return false;
    }
}

// Només caràcters i espais, no entitats html
function unhtmlentities($cadena) {

    $cadena = html_entity_decode($cadena);
    $cadena = str_replace('&rsquo;', '\'', $cadena); // Comilla simple final
    $cadena = str_replace('&ldquo;', '"', $cadena); // Comillas dobles inicio
    $cadena = str_replace('&rdquo;', '"', $cadena); // Comillas dobles final
    $cadena = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $cadena);
    $cadena = preg_replace('~&#([0-9]+);~e', 'chr(\\1)', $cadena);
    return $cadena;
}

// Entitats html restaurant etiquetes
function keephtml($string) {

    $res = htmlentities($string);
    $res = str_replace("&lt;", "<", $res);
    $res = str_replace("&gt;", ">", $res);
    //$res = str_replace("&quot;",'"',$res);
    //$res = str_replace("&amp;",'&',$res);
    return $res;
}

// Obtenir text longitud determinada, maxim fins ultim espai
function tep_text_curt($text, $max) {

    if (tep_not_null($text)) {
        $text = strip_tags($text);
        $text_aux = html_entity_decode($text, ENT_NOQUOTES, 'UTF-8');

        $long = strlen($text_aux);

        if ($long > $max) {
            $text_curt_aux = substr($text_aux, 0, $max);
            $pos_fi = strrpos($text_curt_aux, ' ');
            $text_curt = substr($text_aux, 0, $pos_fi) . '...';
        } else {
            $text_curt = $text_aux;
        }
        return ($text_curt);
    }
}

/**
 * Using this function, we can break(substr) a string without losing html tags,
 *
 * @param $text
 *    String which is to be shortened.
 *
 * @param $length
 *    The length of the string .
 *
 * @param $ending
 *    The string that is to be appended after shortening.  Defaults to &hellip;
 *
 * @param boolean $exact
 *   If false, $text will not be cut mid-word
 *
 * @param boolean $considerHtml
 *   If true, HTML tags would be handled correctly
 *
 *
 * @return
 *     string Trimmed string..
 */
function html_substr($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    if ($considerHtml) {
        // If the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash (f.e. <br/>)
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag (f.e. </b>)
                } elseif (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== FALSE) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag (f.e. <b>)
                } elseif (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space..
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if ($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            // OJOO TRET EXPRESSAMENT XK MOSTRAVA UN </strong> DE MÉS AL FINAL !!! $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}

function clean($value) {
    $value = trim($value);
    $value = ereg_replace("\r", "", $value);
    $value = ereg_replace("\n", "", $value);
    $value = ereg_replace('"', "&quot;", $value);
    $value = ereg_replace("'", "&rsquo;", $value);
    $value = ereg_replace("`", "&lsquo;", $value);
    $value = stripslashes($value);
    return $value;
}

//------------------------------------ Vectors  ------------------------------------//
// Unir arrays (màx 3)
function tep_array_merge($array1, $array2, $array3 = '') {

    if ($array3 == '')
        $array3 = array();
    if (function_exists('array_merge')) {
        $array_merged = array_merge($array1, $array2, $array3);
    } else {
        while (list($key, $val) = each($array1))
            $array_merged[$key] = $val;
        while (list($key, $val) = each($array2))
            $array_merged[$key] = $val;
        if (sizeof($array3) > 0)
            while (list($key, $val) = each($array3))
                $array_merged[$key] = $val;
    }
    return (array) $array_merged;
}

// Buscar si un valor està en un vector
function tep_in_array($lookup_value, $lookup_array) {

    if (function_exists('in_array')) {
        if (in_array($lookup_value, $lookup_array))
            return true;
    } else {
        reset($lookup_array);
        while (list($key, $value) = each($lookup_array)) {
            if ($value == $lookup_value)
                return true;
        }
    }
    return false;
}

// Traiem el primer valor del vector
function tep_array_shift(&$array) {

    if (function_exists('array_shift')) {
        return array_shift($array);
    } else {
        $i = 0;
        $shifted_array = array();
        reset($array);
        while (list($key, $value) = each($array)) {
            if ($i > 0) {
                $shifted_array[$key] = $value;
            } else {
                $return = $array[$key];
            }
            $i++;
        }
        $array = $shifted_array;
        return $return;
    }
}

// Invertim l'ordre dels elements del vector
function tep_array_reverse($array) {

    if (function_exists('array_reverse')) {
        return array_reverse($array);
    } else {
        $reversed_array = array();
        for ($i = sizeof($array) - 1; $i >= 0; $i--) {
            $reversed_array[] = $array[$i];
        }
        return $reversed_array;
    }
}

function tep_array_slice($array, $offset, $length = '0') {

    if (function_exists('array_slice')) {
        return array_slice($array, $offset, $length);
    } else {
        $length = abs($length);
        if ($length == 0) {
            $high = sizeof($array);
        } else {
            $high = $offset + $length;
        }

        for ($i = $offset; $i < $high; $i++) {
            $new_array[$i - $offset] = $array[$i];
        }

        return $new_array;
    }
}

function tep_get_uprid($prid, $params) {

    $uprid = $prid;
    if ((is_array($params)) && (!strstr($prid, '{'))) {
        while (list($option, $value) = each($params)) {
            $uprid = $uprid . '{' . $option . '}' . $value;
        }
    }
    return $uprid;
}

function tep_get_prid($uprid) {

    $pieces = explode('{', $uprid);
    return $pieces[0];
}

// Convertir vector a cadena
function tep_array_to_string($array, $exclude = '', $equals = '=', $separator = '&') {

    if (!is_array($exclude))
        $exclude = array();

    $get_string = '';
    if (sizeof($array) > 0) {
        while (list($key, $value) = each($array)) {
            if ((!in_array($key, $exclude)) && ($key != 'x') && ($key != 'y')) {
                $get_string .= $key . $equals . $value . $separator;
            }
        }
        $remove_chars = strlen($separator);
        $get_string = substr($get_string, 0, -$remove_chars);
    }
    return $get_string;
}

/**
 * Passa els resultats d'una query a array
 * @param type array
 */
function tep_query_to_array($query) {
    $items = array();
    if (!is_null($query) && !empty($query)) {
        //recorrem tots els elements del query
        while ($item = tep_db_fetch_array($query)) {
            $items[] = $item;
        }
    }
    return $items;
}

//------------------------------------ Números  ------------------------------------//
// Evaluar constant
function tep_constant($constant) {
    if (function_exists('constant')) {
        $temp = constant($constant);
    } else {
        eval("\$temp=$constant;");
    }
    return $temp;
}

// Arrodonir decimal
function tep_round($value, $precision) {

    if (PHP_VERSION < 4) {
        $exp = pow(10, $precision);
        return round($value * $exp) / $exp;
    } else {
        return round($value, $precision);
    }
}

// Obtenir valor aleatori
function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!$seeded) {
        mt_srand((double) microtime() * 1000000);
        $seeded = true;
    }

    if (isset($min) && isset($max)) {
        if ($min >= $max) {
            return $min;
        } else {
            return mt_rand($min, $max);
        }
    } else {
        return mt_rand();
    }
}

//------------------------------------ Data i temps  ------------------------------------//

function tep_date_raw($date, $reverse = false) {

    if ($reverse) {
        return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
    } else {
        return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
    }
}

function tep_date_db($raw_date) {
    if (($raw_date == '0000-00-00 00:00:00') || ($raw_date == '0000-00-00') || ($raw_date == ''))
        return false;
    return substr($raw_date, 0, 4) . '-' . substr($raw_date, 5, 2) . '-' . substr($raw_date, 8, 2);
}

function tep_date_long($raw_date) {
    if (($raw_date == '0000-00-00 00:00:00') || ($raw_date == '0000-00-00') || ($raw_date == ''))
        return false;

    $year = (int) substr($raw_date, 0, 4);
    $month = (int) substr($raw_date, 5, 2);
    $day = (int) substr($raw_date, 8, 2);
    $hour = (int) substr($raw_date, 11, 2);
    $minute = (int) substr($raw_date, 14, 2);
    $second = (int) substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
        return date(DATE_FORMAT_LONG, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
        return ereg_replace('2037' . '$', $year, date(DATE_FORMAT_LONG, mktime($hour, $minute, $second, $month, $day, 2037)));
    }
}

function tep_date_short($raw_date) {

    if (($raw_date == '0000-00-00 00:00:00') || ($raw_date == '0000-00-00') || ($raw_date == ''))
        return false;

    $year = substr($raw_date, 0, 4);
    $month = (int) substr($raw_date, 5, 2);
    $day = (int) substr($raw_date, 8, 2);
    $hour = (int) substr($raw_date, 11, 2);
    $minute = (int) substr($raw_date, 14, 2);
    $second = (int) substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
        return date(DATE_FORMAT_SHORT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
        return ereg_replace('2037' . '$', $year, date(DATE_FORMAT_SHORT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }
}

function tep_date_file($raw_date) {

    if (($raw_date == '00/00/0000 00:00:00') || ($raw_date == '0000/00/00') || ($raw_date == ''))
        return false;

    $year = substr($raw_date, 6, 4);
    $month = (int) substr($raw_date, 3, 2);
    $day = (int) substr($raw_date, 0, 2);
    $hour = (int) substr($raw_date, 11, 2);
    $minute = (int) substr($raw_date, 14, 2);
    $second = (int) substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
        return date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
        return ereg_replace('2037' . '$', $year, date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }
}

function comparar_dates($data1, $data2) {

    if (preg_match("/([0-9][0-9]){1,2}\/[0-9]{1,2}\/[0-9]{1,2}/", $data1))
        list($año1, $mes1, $dia1) = explode("/", $data1);
    if (preg_match("/([0-9][0-9]){1,2}-[0-9]{1,2}-[0-9]{1,2}/", $data1))
        list($año1, $mes1, $dia1) = explode("-", $data1);
    if (preg_match("/([0-9][0-9]){1,2}\/[0-9]{1,2}\/[0-9]{1,2}/", $data2))
        list($año2, $mes2, $dia2) = explode("/", $data2);
    if (preg_match("/([0-9][0-9]){1,2}-[0-9]{1,2}-[0-9]{1,2}/", $data2))
        list($año2, $mes2, $dia2) = explode("-", $data2);
    $dif = mktime(0, 0, 0, $mes1, $dia1, $año1) - mktime(0, 0, 0, $mes2, $dia2, $año2);
    return ($dif);
}

function tep_set_time_limit($limit) {

    if (!get_cfg_var('safe_mode')) {
        set_time_limit($limit);
    }
}

/**
 * @desc ordenar una taula multidimensional per $camp
 * @param array $array
 * @param string $camp
 */
function ordenar_multiarray(&$array, $camp) {
    if (!empty($array)) {
        foreach ($array as $key => $row) {
            $dates[$key] = $row[$camp];
        }
        array_multisort($dates, SORT_ASC, $array);
    }
}

/**
 * busca un element en una taula multidimensional
 * @param type $needle
 * @param type $haystack
 * @param type $strict
 * @return type 
 */
function in_array_r($needle, $haystack, $strict = true) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

/**
 * Donats els valors del menus laterals, retornem la classe de la llargada del centre, per a foundation
 * @param type boolean $siteLateralEsquerre -> el site te menu esquerra
 * @param type boolean $siteLateralDret -> el site te menu dret
 * @return string -> classe de la llargada del centre
 */
function tep_get_span_central($siteLateralEsquerre = false,$siteLateralDret = false)
{
      $span_central_home = '';
    if ($siteLateralEsquerre)
    {
        $span_central_home = "large-9 large-push-3 medium-9 medium-push-3";
    }
    else if ($siteLateralDret)
    { 
        $span_central_home = "large-9 medium-9";
    }
    else if ($siteLateralDret == false && $siteLateralEsquerre == false)
    {
        $span_central_home = "large-12 medium-12";
    }
    
    return $span_central_home;
}

?>
