<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<?php

/**
 * retorna tota la info de l'edicio actual
 * @return array
 */
function tep_get_edicio_actual() {

    return tep_db_fetch_array(tep_db_query(" SELECT  id,name,data_inici,data_final,data_inici_edicio,active
                            FROM " . TABLE_EDICIONS . " e
                            WHERE id=" . EDICIO_ACTUAL
                    ));
}

/**
 * retorna tota la info d'una edicio
 * @param int $id_edicions
 * @return array
 */
function tep_get_edicio_by_id($id_edicions) {

    return tep_db_fetch_array(tep_db_query(" SELECT  id,name,data_inici,data_final,data_inici_edicio,active
                            FROM " . TABLE_EDICIONS . " e
                            WHERE e.id=" . $id_edicions
                    ));
}

?>