<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * 
 * Funcions per a utilitzar el modul de fotos (Galeries)
 * 
 * **************************** */
// Include thumbnail class
require_once(DIR_WS_CLASSES . 'ThumbLib.inc.php');

/**
 * @desc retorna un array preparat per a imprimir un <select> d'html de les galeries amb parent_id (si parent_id ==-1) retorna totes les galeries (es pot filtrar per $edicio)
 * @param string $default
 * @param array $edicio
 * @return array 
 */
function tep_get_fotos_groups_array($parent_id = FOTOS_GROUP_TOTES, $default = '', $edicio = false) {

    //global $edicio_actual;
    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("SELECT distinct fg.id, fg.name,fg.parent_id
                            FROM " . TABLE_FOTOS_GROUPS . " fg " .
            //Mirem si s'han de mostrar totes les galeries o només les d'una categoria
            "WHERE " . ($parent_id == -1 ? "fg.parent_id <>0" : "fg.parent_id = " . $parent_id) .
            //Mirem si s'han de mostrar totes les galeries o només les de l'edició actual
            //(FOTOS_FILTRAR_PER_EDICIO ? " AND entered >'" . $edicio_actual['data_inici_edicio'] . "' AND entered<'" . $edicio_actual['data_final'] . "'" : "") . " 
            (FOTOS_FILTRAR_PER_EDICIO && $edicio ? " AND entered >'" . $edicio['data_inici_edicio'] . "' AND entered<'" . $edicio['data_final'] . "'" : "") . " 
                            ORDER BY fg.name");

    while ($values = tep_db_fetch_array($query)) {
        $name = htmlentities($values['name'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
        if ($parent_id == FOTOS_GROUP_TOTES) {//cas mostrar totes les galeries, posem el grup al costat entre parèntesis
            $name.=' (' . tep_get_galeria_name(($values['parent_id'])) . ')';
        }

        $values_array[] = array('id' => $values['id'],
            'text' => $name);
    }
    return $values_array;
}

/**
 * @desc retorna un array preparat per a imprimir un <select> d'html
 * @param string $default
 * @return array 
 */
function tep_get_fotos_ratios_array($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct fr.id,fr.nom,fr.ratio from " . TABLE_FOTOS_RATIOS . " fr order by fr.nom");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => htmlentities($values['nom'] . ' (Ratio:' . $values['ratio'] . ')', ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    }
    return $values_array;
}

/**
 * @desc retorna una taula amb totes les dades d'una galeria i la seva descripcio segons idioma. Si aquest es buit, passa idioma actual
 * @global type $language_id
 * @param int $id
 * @param type $language_id
 * @return mysqli result  (a tractar amb tep_db_fetch_array)
 */
function tep_get_info_galeria($id, $language_id = '') {

    //si no especifiquem idioma agafem l'actual
    if ($language_id == '') {
        global $language_id;
    }

    //obtenim dades de la galeria per idioma
    return tep_db_query(" SELECT  fg.id, fg.name, fg.entered,fg.modified,fg.listorder,fg.foto_portada,fgd.target,fgd.language_id,fgd.title,fgd.description,fgd.class,fgd.rel,fgd.rev 
                            FROM " . TABLE_FOTOS_GROUPS . " fg
                            LEFT JOIN " . TABLE_FOTOS_GROUPS_DESCRIPTION . " fgd
                            ON fg.id=fgd.groups_id AND fgd.language_id= " . (int) $language_id . "
                            WHERE fg.id=" . (int) $id . " AND fg.active=1"
    );
}

/**
 * Retorna el nom de la galeria
 * @global type $language_id
 * @param type $id
 * @param type $language_id
 * @return string
 */
function tep_get_galeria_name($id, $language_id = '') {

    //si no especifiquem idioma agafem l'actual
    if ($language_id == '') {
        global $language_id;
    }

    //obtenim dades de la galeria per idioma
    $galeria = tep_db_fetch_array(tep_db_query(" SELECT  fg.name
                            FROM " . TABLE_FOTOS_GROUPS . " fg
                            LEFT JOIN " . TABLE_FOTOS_GROUPS_DESCRIPTION . " fgd
                            ON fg.id=fgd.groups_id AND fgd.language_id= " . (int) $language_id . "
                            WHERE fg.id=" . (int) $id . " AND fg.active=1"
            ));
    return $galeria['name'];
}

/**
 * @desc retorna totes les fotos d'una galeria, Agafa la descripcio segons idioma passat, si aquest es buit passa idioma actual 
 * @global type $language_id
 * @param int $id_galeria
 * @param type $language_id
 * @return mysqli result (a tractar amb tep_db_fetch_array)
 */
function tep_get_fotos_galeria($id_galeria, $language_id = '') {

    //si no especifiquem idioma agafem l'actual
    if ($language_id == '') {
        global $language_id;
    }

    //obtenim dades de les fotos de la galeria per idioma
    return tep_db_query(" SELECT  f.id, f.group_id,f.name,f.efecte, f.entered,f.modified,f.listorder,f.image,fd.language_id,fd.link,fd.target,fd.title,fd.autor,fd.description,fd.class,fd.rel,fd.rev,fd.title_slide,fd.subtitle,fd.subtitle2
                            FROM " . TABLE_FOTOS . " f
                            LEFT JOIN " . TABLE_FOTOS_DESCRIPTION . " fd
                            ON f.id=fd.fotos_id AND fd.language_id= " . (int) $language_id . "
                            WHERE f.group_id=" . (int) $id_galeria . " AND f.active=1
                            ORDER BY listorder");
}

/**
 * Retorna el total de fotos actives d'una galeria
 * @param int $id_galeria
 * @return int
 */
function tep_fotos_count_by_galeria($id_galeria) {

    //obtenim dades de les fotos de la galeria per idioma
    $total = tep_db_fetch_array(tep_db_query(" SELECT  count(id) as total
                            FROM " . TABLE_FOTOS . " f
                            WHERE f.group_id=" . (int) $id_galeria . " AND f.active=1"));
    return $total['total'];
}

/**
 * @desc Retorna una taula amb la portada de la galeria
 * @global type $language_id
 * @param int $id_galeria
 * @return mysqli result (a tractar amb tep_db_fetch_array)
 */
function tep_get_foto_portada_galeria($id_galeria) {

    //si no especifiquem idioma agafem l'actual
    if ($language_id == '') {
        global $language_id;
    }
    /*
     *     return tep_db_query(" SELECT  f.id,f.name, f.entered,f.modified,f.listorder,f.image,fd.language_id,fd.link,fd.target,fd.title,fd.class,fd.rel,fd.rev
      FROM " . TABLE_FOTOS . " f
      LEFT JOIN " . TABLE_FOTOS_DESCRIPTION . " fd
      ON f.id=fd.fotos_id AND fd.language_id= " . (int) $language_id . "
      WHERE f.id= (SELECT fg.foto_portada FROM " . TABLE_FOTOS_GROUPS . " fg WHERE fg.id=" . (int) $id_galeria . ")"
     */
    //obtenim dades de la foto per idioma
    return tep_db_query(" SELECT  f.id,f.name, f.entered,f.modified,f.listorder,f.image,fd.language_id,fd.link,fd.target,fd.autor,fd.description,fd.title,fd.class,fd.rel,fd.rev
                            FROM " . TABLE_FOTOS . " f, " . TABLE_FOTOS_GROUPS . " fg
                            LEFT JOIN " . TABLE_FOTOS_DESCRIPTION . " fd
                            ON fg.foto_portada=fd.fotos_id AND fd.language_id= " . (int) $language_id . "
                            WHERE fg.id=" . (int) $id_galeria . "
                                AND fg.foto_portada=f.id"
    );
}

/**
 * @desc Genera les 3 miniatures i les guarda en el sistema (en cas que hi hagi crop fa les redimensions adecuades)
 * @param object $galeria
 * @param string $image_aux
 * @param array $crop 
 */
function tep_generar_miniatures($galeria, $image_aux, $crop = false) {

    // Ajustem imatge per crear thumbnail1
    if ($galeria->thumb1_w != 0 && $galeria->thumb1_h != 0) {
        $thumb = PhpThumbFactory::create(DIR_FS_PORTAL_IMAGE_FOTOS . $image_aux);
        if ($crop)
            $thumb->crop($crop['x'], $crop['y'], round($crop['w']), round($crop['h']));
        $thumb->adaptiveResize($galeria->thumb1_w, $galeria->thumb1_h);
        $thumb->save(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $image_aux);
    }

    // Ajustem imatge per crear thumbnail2
    if ($galeria->thumb2_w != 0 && $galeria->thumb2_h != 0) {
        $thumb = PhpThumbFactory::create(DIR_FS_PORTAL_IMAGE_FOTOS . $image_aux);
        if ($crop)
            $thumb->crop($crop['x'], $crop['y'], round($crop['w']), round($crop['h']));
        $thumb->adaptiveResize($galeria->thumb2_w, $galeria->thumb2_h);
        $thumb->save(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $image_aux);
    }

    // Ajustem imatge per crear thumbnail3
    if ($galeria->thumb3_w != 0 && $galeria->thumb3_h != 0) {
        $thumb = PhpThumbFactory::create(DIR_FS_PORTAL_IMAGE_FOTOS . $image_aux);
        if ($crop)
            $thumb->crop($crop['x'], $crop['y'], round($crop['w']), round($crop['h']));
        $thumb->adaptiveResize($galeria->thumb3_w, $galeria->thumb3_h);
        $thumb->save(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs3/' . $image_aux);
    }

    // Ajustem imatge per crear cas especial per a fotos grans thumbs4
    $thumb = PhpThumbFactory::create(DIR_FS_PORTAL_IMAGE_FOTOS . $image_aux);
    if ($crop)
        $thumb->crop($crop['x'], $crop['y'], round($crop['w']), round($crop['h']));
    $thumb->adaptiveResize(1200, 800);
    $thumb->save(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs4/' . $image_aux);
}

function tep_galeria_exists($id_galeria) {

    //obtenim dades de la galeria
    $result = tep_db_fetch_array(tep_db_query("SELECT count(id) as total FROM " . TABLE_FOTOS_GROUPS . " WHERE id=" . $id_galeria));

    return($result['total'] != 0);
}

/**
 * retorna les categories filles d'un $group_id
 * @param int $cgroup_id
 * @return mysqli_result a tractar amb fetch_array 
 */
function tep_get_galeries_by_parent_id($group_id) {

    $groups_raw = "SELECT distinct bg.id, bg.name, bg.listorder,bg.active from " . TABLE_FOTOS_GROUPS . " bg WHERE bg.parent_id= " . $group_id . " ORDER BY bg.listorder";

    return tep_db_query($groups_raw);
}

/**
 * Retorna la informació d'una galeria
 * @param type $galeria_id
 * @return mysqli_result a tractar amb fetch_array 
 */
function tep_get_galeria_by_id($galeria_id) {

    $query_raw = "select bg.name,bg.parent_id, bg.listorder,bg.id_ratios from " . TABLE_FOTOS_GROUPS . " bg where bg.id = '" . (int) $galeria_id . "'";

    return tep_db_query($query_raw);
}

/**
 * retorna el ratio d'una galeria
 * @param type $galeria_id
 * @return int
 */
function tep_get_galeria_ratio($galeria_id) {

    $query_raw = "select id_ratios from " . TABLE_FOTOS_GROUPS . " where id = '" . (int) $galeria_id . "'";

    $ratio = tep_db_fetch_array(tep_db_query($query_raw));

    return $ratio['id_ratios'];
}

/**
 * Retorna el ratio arrel d'una galeria
 * @param type $galeria_id
 * @return int
 */
function tep_get_ratio_final_galeria($galeria_id) {

    $query_raw = "select parent_id,id_ratios from " . TABLE_FOTOS_GROUPS . " where id = '" . (int) $galeria_id . "'";

    $parent = tep_db_fetch_array(tep_db_query($query_raw));
    //busquem pares fins trobar pare arrel
    if ($parent['parent_id'] != 0) {
        $parent = tep_get_ratio_final_galeria($parent['parent_id']);
    }
    return $parent['id_ratios'];
}

/**
 * Retorna el parent_id arrel d'una galeria
 * @param type $galeria_id
 * @return int
 */
function tep_get_parent_final_galeria($galeria_id) {

    $query_raw = "select parent_id from " . TABLE_FOTOS_GROUPS . " where id = '" . (int) $galeria_id . "'";

    $parent = tep_db_fetch_array(tep_db_query($query_raw));
    //busquem pares fins trobar pare arrel
    if ($parent['parent_id'] != 0) {
        $parent = tep_get_parent_final_galeria($parent['parent_id']);
    }
    return $parent['parent_id'];
}


/**
 * Mostra una galeria com a slide revolution
 * @global type $language_id
 * @param type $banner_group_id
 * @param type $show_timmer
 */
function tep_print_gallery_slide_revolution($id_galeria, $show_timmer = 1, $tipus = "main", $efecte = 'random') {

    global $language_id;

    $fotos = tep_get_fotos_galeria($id_galeria);
    $fotos_numrows = tep_db_num_rows($fotos);

    if ($fotos_numrows > 1) {//MOSTRAR O NO THUMBS, utilitzat a javascript al cridar a revolution slide
        global $show_thumbs;
        $show_thumbs = true;
    }

    if ($fotos_numrows > 0) {
        ?>
        <div class="bannercontainer <?php echo $tipus; ?>">
            <div class="banner <?php echo $tipus; ?>">
                <ul>
                    <?php
                    while ($image = tep_db_fetch_array($fotos)) {
                        $imageInfo = new objectInfo($image);
                        //mirem quina miniatura hem d'agafar segons el tipus d'slide a mostrar
                        switch ($tipus) {
                            case 'fitxa':
                                $mida_thumb = 'thumbs3/';
                                $mida_gran = 'thumbs4/';
                                $shadowbox = ' data-link="' . DIR_WS_PORTAL_IMAGE_FOTOS . $mida_gran . $imageInfo->image . '" data-rel="shadowbox[\'' . $tipus . '\']" data-linktitle="' . $imageInfo->title . ' | ' . (($imageInfo->autor) ? '&copy; ' . $imageInfo->autor : '') . '" '; //a les fitxes fem que les galeries facin shadowbox per defecte
                                break;
                            default:
                                $mida_thumb = 'thumbs/';
                                $mida_gran = 'thumbs/';
                        }
                        ?> 
                        <li data-thumb="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $mida_thumb . $imageInfo->image; ?>" data-masterspeed="200" data-slotamount="5" data-transition="<?php echo $imageInfo->efecte ? $imageInfo->efecte : $efecte; ?>" <?php echo $shadowbox; ?>>

                            <?php //echo tep_image(DIR_WS_PORTAL_BANNERS . $imageInfo->image, $imageInfo->name);       ?>

                            <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $mida_thumb . $imageInfo->image; ?>" alt="<?php echo $imageInfo->name; ?>"/>
                            <!--<div class="desc color1"><?php $imageInfo->title; ?></div><div class="autor color1"><?php echo($imageInfo->autor ? '&copy; ' . $imageInfo->autor : ''); ?></div>-->


                            <?php if ($imageInfo->link != '' && $imageInfo->link != 'http://') { ?>
                                <a href="<?php echo ($imageInfo->rel == 'shadowbox') ? DIR_WS_PORTAL_IMAGE_FOTOS . $mida_gran . $imageInfo->image : $imageInfo->link; ?>" title="<?php echo $imageInfo->name; ?>" rel="<?php echo $imageInfo->rel; ?>" target="<?php echo $imageInfo->target; ?>" >
                                <?php } ?>


                                <?php
                                if ($imageInfo->title_slide) {
                                    $search = array('%title_slide', '%subtitle_slide', '%subtitle2_slide');
                                    $replace = array($imageInfo->title_slide, $imageInfo->subtitle, $imageInfo->subtitle2);
                                    echo str_replace($search, $replace, FOTOS_SLIDES_TEXTES_FORMAT);
                                } else {
                                    echo $imageInfo->description;
                                }
                                ?>

                                <?php if ($imageInfo->link != '' && $imageInfo->link != 'http://') { ?>
                                </a>
                            <?php } ?>

                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <div class="tp-bannertimer tp-bottom <?php echo ($show_timmer) ? '' : 'hide'; ?>"></div>
            </div>
        </div>
        <?php
    }
}

/**
 * Mostra una galeria com a slide revolution
 * @global type $language_id
 * @param type $banner_group_id
 * @param type $show_timmer
 */
function banners_slide_revolution_by_galery($id_galeria, $show_timmer = 1, $tipus = "main", $efecte = 'random') {

    global $language_id;

    $fotos = tep_get_fotos_galeria($id_galeria);
    $fotos_numrows = tep_db_num_rows($fotos);

    if ($fotos_numrows > 1) {//MOSTRAR O NO THUMBS, utilitzat a javascript al cridar a revolution slide
        global $show_thumbs;
        $show_thumbs = true;
    }

    if ($fotos_numrows > 0) {
        ?>
        <div class="bannercontainer <?php echo $tipus; ?>">
            <div class="banner <?php echo $tipus; ?>">
                <ul>
                    <?php
                    while ($image = tep_db_fetch_array($fotos)) {
                        $imageInfo = new objectInfo($image);
                        //mirem quina miniatura hem d'agafar segons el tipus d'slide a mostrar
                        switch ($tipus) {
                            case 'fitxa':
                                $mida_thumb = 'thumbs3/';
                                $mida_gran = 'thumbs4/';
                                $shadowbox = ' data-link="' . DIR_WS_PORTAL_IMAGE_FOTOS . $mida_gran . $imageInfo->image . '" data-rel="shadowbox[\'' . $tipus . '\']" data-linktitle="' . $imageInfo->title . ' | ' . (($imageInfo->autor) ? '&copy; ' . $imageInfo->autor : '') . '" '; //a les fitxes fem que les galeries facin shadowbox per defecte
                                break;
                            default:
                                $mida_thumb = 'thumbs/';
                                $mida_gran = 'thumbs/';
                        }
                        ?> 
                        <li data-thumb="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $mida_thumb . $imageInfo->image; ?>" data-masterspeed="200" data-slotamount="5" data-transition="<?php echo $imageInfo->efecte ? $imageInfo->efecte : $efecte; ?>" <?php echo $shadowbox; ?>>

                            <?php //echo tep_image(DIR_WS_PORTAL_BANNERS . $imageInfo->image, $imageInfo->name);      ?>

                            <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $mida_thumb . $imageInfo->image; ?>" alt="<?php echo $imageInfo->name; ?>"/>
                            <!--<div class="desc color1"><?php $imageInfo->title; ?></div><div class="autor color1"><?php echo($imageInfo->autor ? '&copy; ' . $imageInfo->autor : ''); ?></div>-->


                            <?php if ($imageInfo->link != '' && $imageInfo->link != 'http://') { ?>
                                <a href="<?php echo ($imageInfo->rel == 'shadowbox') ? DIR_WS_PORTAL_IMAGE_FOTOS . $mida_gran . $imageInfo->image : $imageInfo->link; ?>" title="<?php echo $imageInfo->name; ?>" rel="<?php echo $imageInfo->rel; ?>" target="<?php echo $imageInfo->target; ?>" >
                                <?php } ?>


                                <?php
                                if ($imageInfo->title_slide) {
                                    $search = array('%title_slide', '%subtitle_slide', '%subtitle2_slide');
                                    $replace = array($imageInfo->title_slide, $imageInfo->subtitle, $imageInfo->subtitle2);
                                    echo str_replace($search, $replace, FOTOS_SLIDES_TEXTES_FORMAT);
                                } else {
                                    echo $imageInfo->description;
                                }
                                ?>

                                <?php if ($imageInfo->link != '' && $imageInfo->link != 'http://') { ?>
                                </a>
                            <?php } ?>

                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <div class="tp-bannertimer tp-bottom <?php echo ($show_timmer) ? '' : 'hide'; ?>"></div>
            </div>
        </div>
        <?php
    }
}







/**
 * Imprimim una galeria de fotos com un orbit slide de foundation 
 * @global type $language_id
 * @global boolean $show_thumbs
 * @param type $id_galeria -> id de la galeria
 * @param type $tipus -> classe per assignar una classe per definir el tipus
 * @param type $mostrarPrimer -> mostrar o no la primera imatge de la galeria
 * example result
 * <ul data-orbit> 
 *   <li> 
 *       <img src="../assets/img/examples/satelite-orbit.jpg" alt="slide 1" /> 
 *       <div class="orbit-caption"> Caption One. </div> 
 *   </li> 
 *   <li> 
 *       <img src="../assets/img/examples/andromeda-orbit.jpg" alt="slide 2" /> 
 *       <div class="orbit-caption"> Caption Two. </div> 
 *   </li> 
 *   <li> 
 *       <img src="../assets/img/examples/launch-orbit.jpg" alt="slide 3" /> 
 *       <div class="orbit-caption"> Caption Three. </div> 
 *   </li> 
 * </ul>
 */

function tep_print_gallery_slide_orbit($id_galeria, $tipus = "main", $mostrarPrimer = true)
{
    global $language_id;

    $fotos = tep_get_fotos_galeria($id_galeria);
    $fotos_numrows = tep_db_num_rows($fotos);
    $mostrarPrimer ? $initFotos = 0 : $initFotos = 1;
    
    if ($fotos_numrows > $initFotos) 
    {
        ?><ul data-orbit> <?php
        while ($image = tep_db_fetch_array($fotos))
        {
            $imageInfo = new objectInfo($image);
            switch ($tipus)
            {
                case 'fitxa':
                    $mida_thumb = 'thumbs4/';
                    break;
                default:
                    $mida_thumb = 'thumbs/';
            }
            
            ?>
                <li> 
                    <img src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $mida_thumb . $imageInfo->image; ?>" alt="<?php echo $imageInfo->name; ?>"/>
                    <div class="orbit-caption"> 
                        <div class="desc color1"><?php echo $imageInfo->title; ?></div>
                        <div class="autor color1"><?php echo($imageInfo->autor ? '&copy; ' . $imageInfo->autor : ''); ?></div>
                    </div> 
                </li> 
            <?php
        }
        ?></ul><?php
    }
}

/**
 * imprimim les imatges d' una galeria en lis per mostrar en un ul de blocks, ul li del foundation
 * exemple ul extern: <ul class="galeria clearing-thumbs large-block-grid-3" data-clearing></ul>
 * nomes imprimim lis!
 * @param type $id_galeria -> $pageInfo->fotos_groups_id
 */
function tep_print_gallery_blocks($id_galeria)
{
    $galeriaQuery = tep_get_fotos_galeria($id_galeria);
    $galeria_numrows = tep_db_num_rows($galeriaQuery);
    if ($galeria_numrows > 0) 
    {
        while ($galeria = tep_db_fetch_array($galeriaQuery)) 
        {
            $fotoInfo = new objectInfo($galeria);
            tep_print_image_foundation_li($fotoInfo,"","","");
        }
    }
}

/**
 * imprimim una galeria en forma de foundation clearing lightbox 
 * @param type $id_galeria id de la galera
 * 
 * EXAMPLE RESULT:
 * <ul class="clearing-thumbs" data-clearing> 
 *   <li><a href="path/to/your/img"><img src="path/to/your/img-th"></a></li> 
 *   <li><a href="path/to/your/img"><img src="path/to/your/img-th"></a></li> 
 *   <li><a href="path/to/your/img"><img src="path/to/your/img-th"></a></li> 
 * </ul>
 * 
 */
function tep_print_gallery_clearing_lightbox_foundation($id_galeria, $thumbs='thumbs')
{
    $galeriaQuery = tep_get_fotos_galeria($id_galeria);
    $galeria_numrows = tep_db_num_rows($galeriaQuery);
    if ($galeria_numrows > 0) 
    {
        ?><ul class="clearing-thumbs" data-clearing> <?php
        while ($galeria = tep_db_fetch_array($galeriaQuery)) 
        {
            $fotoInfo = new objectInfo($galeria);
            tep_print_image_foundation_li($fotoInfo,"","","",$thumbs);
        }
        ?></ul><?php
    }
}

/**
 * imprimim una galeria en forma de foundation Featured Image
 * @param type $id_galeria id de la galera
 * 
 * EXAMPLE RESULT:
 * <ul class="clearing-thumbs clearing-feature" data-clearing> 
 *     <li><a href="path/to/your/img"><img src="path/to/your/img-th"></a></li> 
 *     <li class="clearing-featured-img"><a href="path/to/your/img"><img src="path/to/your/img-th"></a></li> 
 *     <li><a href="path/to/your/img"><img src="path/to/your/img-th"></a></li> 
 * </ul>
 * 
 */
function tep_print_gallery_featured_image_foundation($id_galeria,$featuredID)
{
    $galeriaQuery = tep_get_fotos_galeria($id_galeria);
    $galeria_numrows = tep_db_num_rows($galeriaQuery);
    if ($galeria_numrows > 0) 
    {
        ?><ul class="clearing-thumbs clearing-feature" data-clearing><?php
        
        while ($galeria = tep_db_fetch_array($galeriaQuery)) 
        {
            $fotoInfo = new objectInfo($galeria);
            
            if ($fotoInfo->id != $featuredID)
            {
                tep_print_image_foundation_li($fotoInfo);
            }
            else
            {
                tep_print_image_foundation_li($fotoInfo,"clearing-featured-img","","");
            }
        }
        ?></ul><?php
    }
}


/**
 * imprimim un li per a les galeries de foundation
 * @param type $fotoInfo -> objecte d' una fila foto
 * @param type $liClass -> classe que s' assigna al li
 * @param type $aClass -> classe que s' assigna al a
 * @param type $imgClass -> classe que s' assigna al img
 * example result: <li><a href="path/to/your/img"><img src="path/to/your/img-th"></a></li> 
 * si hi ha classes es posen al seu tag, si sen passa mes d' una separades per espai
 */
function tep_print_image_foundation_li($fotoInfo, $liClass="", $aClass="", $imgClass="", $thumb="thumbs" )
{
    $liClass = $liClass != '' ? 'class="'.$liClass.'"' : '';
    $aClass = $aClass != '' ? 'class="'.$aClass.'"' : '';
    $imgClass = $imgClass != '' ? 'class="'.$imgClass.'"' : '';
    
    ?>
    
    <li <?php echo $liClass ?>>
        <a <?php echo $aClass ?> href="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $fotoInfo->image; ?>" >
            <img <?php echo $imgClass ?> 
                 src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $thumb . '/' . $fotoInfo->image; ?>" 
                 alt="<?php echo $fotoInfo->title; ?>" 
                 data-caption="<?php echo $fotoInfo->title . ($fotoInfo->autor ? ' | &copy; ' . $fotoInfo->autor : '' ); ?>"/>
        </a>
    </li>
    <?php
}


/***************************FUNCIONS PER FLARE*********************************/
/**
 * imprimim les imatges d' una galeria en visor flare en lis per mostrar en un ul de blocks, ul li del foundation
 * exemple ul extern: <ul class="galeria clearing-thumbs large-block-grid-3" data-clearing></ul>
 * nomes imprimim lis!
 * @param type $liClass -> classe que s' assigna al li
 * @param type $aClass -> classe que s' assigna al a
 * @param type $imgClass -> classe que s' assigna al img
 * @param type $id_galeria -> id fe la galeria que imprimirem
 * @param type $galeriaFlare -> id de galeria del flare, el visor on es mostren
 */
function tep_print_gallery_blocks_flare($id_galeria, $galeriaFlare, $liClass="", $aClass="", $imgClass="", $thumbExt="thumbs", $thumbIN="thumbs3")
{
    $galeriaQuery = tep_get_fotos_galeria($id_galeria);
    $galeria_numrows = tep_db_num_rows($galeriaQuery);
    if ($galeria_numrows > 0) 
    {
        $i=0;
        while ($galeria = tep_db_fetch_array($galeriaQuery)) 
        {
            $fotoInfo = new objectInfo($galeria);
            tep_print_image_fundation_flare_li($fotoInfo, $galeriaFlare, $i, $liClass, $aClass, $imgClass, $thumbExt,$thumbIN);
            $i++;
        }
    }
}

/**
 * imprimim un li per a blocks de foundation i utilitzat el viso del flare
 * @param type $fotoInfo -> objecte d' una fila foto
 * @param type $liClass -> classe que s' assigna al li
 * @param type $aClass -> classe que s' assigna al a
 * @param type $imgClass -> classe que s' assigna al img
 * @param type $gallery -> string que ens indicia a quin galery de flare pertany
 * @param type $imageNum -> numero d' imate dins la galeria
 * example result: <li> amb contingut per flare </li> 
 * si hi ha classes es posen al seu tag, si sen passa mes d' una separades per espai
 * si thumb = "", s' agafa la imatge original.
 * per l' imatge ampliada s' agafa la imatge original
 */
function tep_print_image_fundation_flare_li($fotoInfo, $gallery, $imageNum, $liClass="", $aClass="", $imgClass="",$thumbExt="thumbs", $thumbIN="thumbs3")
{
    $liClass = ( $liClass != '' ? 'class="'.$liClass.'"' : '' );
    $aClass = ( $aClass != '' ? 'class="'.$aClass.'"' : '' );
    $imgClass = ( $imgClass != '' ? 'class="'.$imgClass.'"' : '' );
    
    //<!--           data-flare-plugin="shutter" --> (per mostrar imatges a pantalla completa quan s' obrin)
    
    ?>
    <li <?php echo $liClass ?>>
        
            <?php// type a ?>
<!--        <a <?php //echo $aClass ?> href="<?php // echo DIR_WS_PORTAL_IMAGE_FOTOS . $fotoInfo->image; ?>"
            data-target="flare"
            data-flare-scale="fit"
            data-flare-gallery="<?php echo $gallery?>"
            data-pe-target-id="<?php echo $imageNum ?>">
            <img <?php //echo $imgClass ?> src="<?php // echo DIR_WS_PORTAL_IMAGE_FOTOS . ($thumb != "" ? ($thumb  . "/") : "" ) . $fotoInfo->image; ?>" alt="<?php // echo  $fotoInfo->title; ?>" />
        </a>-->
        
        <?php// type b ?>
        <a <?php echo $aClass ?> data-flare-thumb="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $thumbExt . "/" . $fotoInfo->image; ?>" <?php //thumb de la miniatura dins el visor ?>
           
           data-flare-gallery="<?php echo $gallery?>"
           data-flare-scale="fit" 
           data-target="flare" 
           title="<?php  echo  $fotoInfo->title;  ?>"
           data-description="<?php  echo  $fotoInfo->autor;  ?>" 
           href="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $thumbIN . "/" . $fotoInfo->image; ?>" <?php //imatge gran dins el visor ?>
           data-pe-target-id="<?php echo $imageNum ?>">
           <img <?php echo $imgClass ?> alt="<?php echo  $fotoInfo->title; ?>" src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $thumbExt . "/" . $fotoInfo->image; ?>"> <?php //thumb fora del visor ?>
        </a>
    </li>
    
    <?php
}

/**
 * Imprimim la PRIMERA imatge d' una galeria amb la llibreria flare. <a><img /></a>
 * @param type $id_galeria -> id de la galeria, nomes agafem la primera
 * @param type $aClass -> classe que li donarem al tag a
 * @param type $imgClass -> classe que li dornarem al tag img
 * @param type $thumbExt -> thumbs exterior de la imatge
 * @param type $thumbIN -> thum interior de la imatge
 */
function tep_print_image_fundation_flare_first_image($id_galeria,$gallery, $aClass="", $imgClass="", $thumbExt="", $thumbIN="")
{
    $galeriaQuery = tep_get_fotos_galeria($id_galeria);
    $galeria_numrows = tep_db_num_rows($galeriaQuery);
    
    if ($galeria_numrows > 0) 
    {
        $galeria = tep_db_fetch_array($galeriaQuery);
        $fotoInfo = new objectInfo($galeria);

        $aClass = ( $aClass != '' ? 'class="'.$aClass.'"' : '' );
        $imgClass = ( $imgClass != '' ? 'class="'.$imgClass.'"' : '' );

        ?>
        <a <?php echo $aClass ?> data-flare-thumb="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $thumbExt . "/" . $fotoInfo->image; ?>" <?php //thumb de la miniatura dins el visor ?>
           
           data-flare-gallery="<?php echo $gallery?>"
           data-flare-scale="fit" 
           data-target="flare" 
           title="<?php  echo  $fotoInfo->title;  ?>"
           data-description="<?php  echo  $fotoInfo->description;  ?>" 
           href="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $thumbIN . "/" . $fotoInfo->image; ?>" <?php //imatge gran dins el visor ?>>
           <img <?php echo $imgClass ?> alt="<?php echo  $fotoInfo->title; ?>" src="<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . $thumbExt . "/" . $fotoInfo->image; ?>"> <?php //thumb fora del visor ?>
        </a>
        <?php
    }
}
/******************************************************************************/


?>
