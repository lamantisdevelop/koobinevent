<?php

/**
 * @param type $byBlocks
 * 
 */
function tep_print_agenda_inici($byBlocks = true)
{
     global $language_id;
    // Busquem les ultimes noticies
    $date_aux = date("Y-m-d", time());
    $query_raw = " SELECT  a.id, a.data_inici, a.data_final, a.hora, a.lloc, ad.nom, ad.descripcio, a.active,a.portada, a.entered "
               . " FROM " . TABLE_ACTIVITATS . " a "
               . " LEFT JOIN " . TABLE_ACTIVITATS_DESCRIPTION . " ad on a.id = ad.activitats_id and ad.language_id = '" . $language_id . "' "
               . " WHERE  a.active = 1 "
               . " AND a.portada = 1"
               . " ORDER BY a.data_inici ASC "
               . " LIMIT " . HOME_MAX_ACTIVITATS;
    
    $listing = tep_db_query($query_raw);
    //$aux_numrows = tep_db_num_rows($listing);
    // Llistat segons busqueda
    while ($item = tep_db_fetch_array($listing)) 
    {
        $itemInfo = new objectInfo($item);
        //busquem foto de portada de galeria
        //$portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
        // Data
        //$aux_data = explode('-', $itemInfo->data_inici);
        //traiem hora
        //$aux_data2 = explode(' ', $aux_data[2]);
        //$text_data = $day_text[date("w", strtotime($itemInfo->data_inici))] . ' ' . $aux_data2[0] . ' ' . $month_text[$aux_data[1] - 1] . ' ' . date("Y", strtotime($itemInfo->data_inici));
        
        ?>
        <li>
            <?php 
            echo tep_print_activitat_agenda_li($itemInfo)
            ?>
        </li>
        <?php
    }
   
}

/**
 * 
 * @param type $categoria
 * @param type $itemInfo
 */
function tep_print_activitat_agenda_li($itemInfo)
{
     // echo tep_image(DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image'], '', '', '') . "\n";
    ?>
    <div class="activitatItemHome">
        <p class="data"><?php echo tep_date_short($itemInfo->data_inici); ?> </p>
        <p class="titol"><a href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_AGENDA_PAGE), 'aID=' . $itemInfo->id . '&item_title=' . $itemInfo->nom . '&modul=agenda'); ?>" class=""><?php echo $itemInfo->nom ?></a></p>
        <p class="dia">Dia: <?php echo mostrar_data_funcio_format_text(strtotime($itemInfo->data_inici), false, true, false) ?></p>
        <p class="hora">Hora: <?php echo $itemInfo->hora ?></p>
        <p class="lloc">Lloc: <?php echo $itemInfo->lloc ?></p>
    </div>
    <?php
    
    /*
    <div class="espectacle span6 <?php echo ($j % 2 == 1) ? 'primer' : ''; ?>">
        <div class="genere genere_<?php echo $categoria['id']; ?>"><?php echo $categoria['text']; ?><span>&nbsp;</span></div>
        <a  href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_CATALOG_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->name); ?>" title="<?php echo $itemInfo->name; ?>"><img src="<?php echo (DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $portada['image']); ?>" alt="<?php echo $itemInfo->name; ?>"/></a>
        <div class="sumari">
            <h2 class="titol">
                <a class=" color1" href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_CATALOG_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->name); ?>" title="<?php echo $itemInfo->name; ?>">
                    <?php echo $itemInfo->name; ?>
                </a>
            </h2>
            <div class="autor"><?php echo $itemInfo->autor; ?></div>
            <div class="cia"><?php echo mostrar_dates_funcions_format_text($itemInfo->id); ?></div>
            <?php $espai = tep_db_fetch_array(tep_get_espai($itemInfo->espais_id)); ?>
            <div class="espai color1 "><?php echo $espai['nom']; ?></div>
        </div>
    </div>
    */
}


