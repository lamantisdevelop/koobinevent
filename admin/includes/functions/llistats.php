<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */

/* * ****************************GENERAL**************************************** */

/**
 * @desc retorna el nom del llistat segons id i idioma
 * @global int $language_id
 * @param type $llistats_id
 * @param int $language_id
 * @return type 
 */
function tep_get_llistat_group_name($groups_id, $language_id = false) {
    if (!$language_id) 
    {
        global $language_id;
    }
    
    $category_query = tep_db_query("SELECT title  
                                    FROM " . TABLE_LLISTATS_GROUPS_DESCRIPTION . " 
                                    WHERE groups_id = '" . $groups_id . "' "
                                  . "AND  language_id = '" . (int) $language_id . "'");
    
    $category = tep_db_fetch_array($category_query);
    return $category['title'];
}

/**
 * Retorna un array amb els elements del grup de llistat (id, text)
 * @param string $default
 * @return type  array;
 */
function tep_get_llistats_array_by_id_group($id_llistat, $default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '0',
            'text' => $default);
    }
    $query = tep_db_query("select e.id, ed.titol from " . TABLE_LLISTATS . " e LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ed
        ON e.id=ed.llistats_id  WHERE e.group_id=" . $id_llistat . " AND e.active!=0 GROUP BY e.id order by e.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => htmlentities($values['titol'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    }
    return $values_array;
}

/**
 * Retorna un element del llistat segons id i idioma
 * @global int $language_id
 * @param type $id_espai
 * @param bool $filtrar_actius
 * @param int $language_id
 * @return mysqli_result a tractar amb fetch_array  
 */
function tep_get_llistat_by_id($id_llistat, $filtrar_actius = false, $language_id = false) {
    
    //if (!$language_id)
    //{
        global $language_id;
    //}
    

    //obtenim dades del llistat per idioma
    $query = (" SELECT e.id,e.active,e.listorder,e.lat,e.lon,e.autor,e.link,e.generic1,e.generic2,e.generic3,e.generic4,generic5,
                e.inscripcions,e.fotos_groups_id,e.published,ed.language_id,ed.titol,ed.titol2,ed.titol3,ed.descripcio,ed.descripcio2,
                ed.descripcio3,ed.download
                FROM " . TABLE_LLISTATS . " e  
                LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ed ON e.id = ed.llistats_id AND ed.language_id = '" . $language_id . "' 
                WHERE e.id = " . $id_llistat);
    
    if ($filtrar_actius) 
    {
        $query.=" AND e.active=1";
    }
    
    return tep_db_query($query);
}



/**
 * Retorna el titol d'un llistat
 * @global int $language_id
 * @param type $id_llistat
 * @param int $language_id
 * @return type string
 */
function tep_get_llistat_titol($id_llistat, $language_id = false) {
    if (!$language_id)
    {
        global $language_id;
    }
    $query = tep_db_query("select ld.llistats_id, ld.titol from " . TABLE_LLISTATS_DESCRIPTION . " ld where ld.llistats_id = '" . $id_llistat . "' and ld.language_id = '" . (int) $language_id . "'limit 1");
    $values = tep_db_fetch_array($query);
    return $values['titol'];
}

/**
 * retorna les categories filles d'un $category_id
 * @param int $category_id
 * @return mysqli_result a tractar amb fetch_array 
 */
function tep_get_llistat_groups_by_parent_id($group_id, $language_id = false) {

    if (!$language_id)
    {
        global $language_id;
    }

    $groups_raw = " SELECT lg.id,lg.name,lgd.title,lgd.description,lg.parent_id,lg.listorder,lg.fotos_groups_id,lg.active
                    FROM " . TABLE_LLISTATS_GROUPS . " lg  
                    LEFT JOIN " . TABLE_LLISTATS_GROUPS_DESCRIPTION . " lgd ON lg.id = lgd.groups_id AND lgd.language_id = '" . $language_id . "' 
                    WHERE lg.parent_id = " . $group_id . " 
                    AND lg.active = '1'
                    ORDER BY lg.listorder";
    
    //echo $groups_raw;
    
    return tep_db_query($groups_raw);
}

/**
 * retorna les categories filles d'un $category_id
 * @param int $category_id
 * @return mysqli_result a tractar amb fetch_array 
 */
function tep_get_llistat_groups_by_parent_id_admin($group_id, $language_id = false) {

    if (!$language_id)
    {
        global $language_id;
    }

    $groups_raw = " SELECT lg.id,lg.name,lgd.title,lgd.description,lg.parent_id,lg.listorder,lg.fotos_groups_id,lg.active
                    FROM " . TABLE_LLISTATS_GROUPS . " lg  
                    LEFT JOIN " . TABLE_LLISTATS_GROUPS_DESCRIPTION . " lgd ON lg.id = lgd.groups_id AND lgd.language_id = '" . $language_id . "' 
                    WHERE lg.parent_id = " . $group_id . " 
                    
                    ORDER BY lg.listorder";

    return tep_db_query($groups_raw);
}

/**
 * retorna tota la informació d'un grup segons id (inclosa descripcio per idioma)
 * @global int $language_id
 * @param int $group_id
 * @param int $language_id
 * @return array
 */
function tep_get_llistat_group_by_id($group_id, $language_id = false) {

    if (!$language_id)
    {
        global $language_id;
    }

    //obtenim dades del grup per idioma
    return tep_db_fetch_array(tep_db_query(" SELECT lg.name,lgd.title,lgd.description,lg.parent_id,lg.listorder,lg.fotos_groups_id
                    FROM " . TABLE_LLISTATS_GROUPS . " lg  
                    LEFT JOIN " . TABLE_LLISTATS_GROUPS_DESCRIPTION . " lgd ON lg.id = lgd.groups_id AND lgd.language_id = '" . $language_id . "' 
                    WHERE lg.id = " . $group_id . "
                    AND lg.active=1"));
}

/**
 * @desc Obtenir els llistats actius relacionats a un altre llistat dins d'un grup
 * @param int $products_id
 * @return array
 */
function tep_get_llistats_relacionats_by_group($llistat_id, $group_id, $array = true) {

    $items = tep_db_query("   SELECT lr.llistats_id,lr.related_id
                            FROM " . TABLE_LLISTATS_RELATED . " lr
                            LEFT JOIN " . TABLE_LLISTATS . " l ON l.id=lr.llistats_id
                            WHERE lr.llistats_id = '" . $llistat_id . "' AND lr.id_llistats_groups=" . $group_id);
    $return = $items;
    if ($array) {//retornar format taula
        $return = array();
        while ($item = tep_db_fetch_array($items)) {

            $return[$item['related_id']] = $item['llistats_id'];
        }
    }
    return $return;
}

/**
 * @desc Obtenir els llistats actius relacionats a un altre llistat dins d'un grup (invers: saber des de quins llistats l'han relacionat)
 * @param int $products_id
 * @return array
 */
function tep_get_llistats_relacionats_by_group_and_related($related_id, $group_id, $array = true) {

    $items = tep_db_query("   SELECT lr.llistats_id,lr.related_id
                            FROM " . TABLE_LLISTATS_RELATED . " lr
                            LEFT JOIN " . TABLE_LLISTATS . " l ON l.id=lr.llistats_id
                            WHERE lr.related_id = '" . $related_id . "' AND lr.id_llistats_groups=" . $group_id);
    $return = $items;
    if ($array) {//retornar format taula
        $return = array();
        while ($item = tep_db_fetch_array($items)) {

            $return[$item['related_id']] = $item['llistats_id'];
        }
    }
    return $return;
}

/**
 * @desc Obtenir els llistats actius relacionats a un altre llistat dins d'un grup i els seus grups fills recursivament
 * @param int $products_id
 * @return array
 */
function tep_get_llistats_relacionats_by_group_recursiu_fills($llistat_id, $group_id) {

    $grupsFills = tep_get_llistat_groups_by_parent_id($group_id); //obtenim els grups fills
    $return = array();
    while ($grupFill = tep_db_fetch_array($grupsFills)) {//recorrem tots els grups fills
        $return = array_merge(tep_get_llistats_relacionats_by_group($llistat_id, $grupFill['id'])); //anem afegint els relacionats trobats
    }
    return $return;
}

/**
 * Retorna els llistats d'un grup
 * @global int $language_id
 * @param type $group_id
 * @param int $language_id
 * @return mysqli_result a tractar amb mysqli_fetch_array
 */
function tep_get_llistats_by_group($group_id, $language_id = false) {

    if (!$language_id)
    {
        global $language_id;
    }

    return tep_db_query("   SELECT l.id,l.active,l.listorder,l.lat,l.lon,l.autor,l.link,l.fotos_groups_id,l.generic1,l.generic2,l.generic3,l.generic4,l.generic5,
                                   l.inscripcions,l.published,ld.language_id,ld.titol,ld.titol2,ld.titol3,ld.descripcio,ld.descripcio2,ld.descripcio3,ld.download
                            FROM " . TABLE_LLISTATS . " l 
                            LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ld ON l.id=ld.llistats_id AND ld.language_id = '" . (int) $language_id . "' 
                            WHERE (l.group_id = " . $group_id . ") 
                            AND l.active='1'  
                            AND ld.titol<>'' 
                            ORDER BY l.group_id,l.listorder,l.entered desc");
}

/**
 * Incrementa en 1 el camp viewed de la BD per id de llistat
 * @global int $language_id
 * @param type $item_id
 * @param int $language_id
 */
function tep_llistats_augmentar_visualitzacions($item_id, $language_id = false) {

    if (!$language_id)
        global $language_id;
    // Augmentar visualitzacio
    tep_db_query("update " . TABLE_LLISTATS_DESCRIPTION . " set viewed = viewed+1 where llistats_id = '" . $item_id . "' and language_id = '" . $language_id . "'");
}

/**
 * Omple l'array passat per parametre ($llistatGroups) amb els ids dels grups de llistats dels que penja $group_id
 * @param array $group_path
 * @param int $group_id
 */
function tep_get_llistats_group_path(&$group_path, $group_id) {
    if ($group_id > 0) {
        array_push($group_path, $group_id);
        $parent_group_id = tep_get_id_parent_by_id_group($group_id);
        tep_get_llistats_group_path($group_path, $parent_group_id);
    }
}

/**
 * Retorna el parent_id d'un grup
 * @param type $group_id
 * @return type
 */
function tep_get_id_parent_by_id_group($group_id) {
    $groups_raw = tep_db_query(" SELECT bg.parent_id"
            . " FROM " . TABLE_LLISTATS_GROUPS . " bg "
            . " WHERE bg.id = '" . $group_id . "'");

    $values = tep_db_fetch_array($groups_raw);
    return $values['parent_id'];
}

/**
 * Retorna un arbre de tots els grups fills
 * @param type $group_id
 * @param type $groups_tree
 * @return multi array
 */
function tep_get_llistats_groups_fills_tree($group_id, $groups_tree = array()) {

    //obtenim tots els fills del primer nivell
    $grups_fills = tep_get_llistat_groups_by_parent_id($group_id);
    if (tep_db_num_rows($grups_fills)) {
        //recorrem tots els fills del primer nivell
        while ($grup_fill = tep_db_fetch_array($grups_fills)) {
            $groups_tree[$grup_fill['id']] = $grup_fill; //afegim el grup actual al return
            $groups_tree[$grup_fill['id']]['fill'] = tep_get_llistats_groups_fills_tree($grup_fill['id']); //recorrem recursiu els fills del proper nivell
        }
    }
    return $groups_tree;
}

/**
 * Imprimeix una llista html de l'arbre de grups fills
 * @param type $group_id
 * @param type $groups_tree
 * @return string
 */
function tep_print_llistats_groups_fills_tree($group_id, $class_sub_ul = '') {

    //obtenim tots els fills del primer nivell
    $grups_fills = tep_get_llistat_groups_by_parent_id($group_id);
    if (tep_db_num_rows($grups_fills)) {
        //recorrem tots els fills del primer nivell
        while ($grup_fill = tep_db_fetch_array($grups_fills)) {
            $return .= '<li><a' . ($_GET['groupID'] == $grup_fill['id'] ? 'class="actual"' : '') . ' href="' . tep_friendly_url('', tep_get_page_title($_GET['pageID']), 'groupID=' . $grup_fill['id'] . '&group_title=' . $grup_fill['title']) . '">' . $grup_fill['title'] . '</a>'; //afegim el grup actual al return
            $fill_tmp = tep_print_llistats_groups_fills_tree($grup_fill['id'],$class_sub_ul); //recorrem recursiu els fills del proper nivell
            if (!empty($fill_tmp)) {
                $return .= "\n" . '<ul class="' . $class_sub_ul . '">' . "\n" . $fill_tmp . '</ul>' . "\n";
            }
            $return .='</li>' . "\n";
        }
    }

    return $return;
}

/* * ****************************PATROCINADORS**************************************** */

/**
 * @desc retorna tots els proveidors del $grup_id indicat en cas de $grup_id=false retorna els de tots els grups.  (organitzadors, col·laboradors...)
 * @global int $language_id
 * @param int $group_id
 * @param int $language_id
 * @return mysqli_result a tractar amb fetch_array 
 */
function tep_get_patrocinadors($group_id = false, $language_id = false) {

    if (!$language_id)
        global $language_id;

    if (!$group_id) {
        $group_id = '4 OR l.group_id = 10 OR l.group_id = 11 OR l.group_id = 12 OR l.group_id = 13 OR l.group_id = 14';
    }

    return tep_db_query("   SELECT l.id, l.group_id, l.autor, l.link, ld.titol,ld.subtitol, ld.descripcio,ed.descripcio2,ed.descripcio3, l.active, l.entered 
                            FROM " . TABLE_LLISTATS . " l 
                            LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ld 
                            ON l.id=ld.llistats_id AND ld.language_id = '" . (int) $language_id . "' 
                            WHERE (l.group_id = " . $group_id . ") AND l.active='1'  AND ld.nom<>'' 
                            ORDER BY l.group_id,l.listorder,l.entered desc");
}

/* * ****************************ESPAIS**************************************** */

/**
 * Retorna un array amb els espais (id, text)
 * @param string $default
 * @return type 
 */
function tep_get_espais_array($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select e.id, ed.nom from " . TABLE_LLISTATS . " e LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ed
        ON e.id=ed.llistats_id  WHERE e.group_id='3' AND e.active!=0 GROUP BY e.id order by ed.nom");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => htmlentities($values['nom'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    }
    return $values_array;
}

/**
 * Retorna un espai segons id i idioma
 * @global int $language_id
 * @param type $id_espai
 * @param int $language_id
 * @return type 

 */
function tep_get_espai($id_espai, $language_id = false) {
    if (!$language_id)
        global $language_id;

    //obtenim dades de l'espai per idioma
    return tep_db_query(" SELECT e.id,e.active,e.listorder,e.lat,e.lon,ed.language_id,ed.nom,ed.subtitol,ed.descripcio,ed.descripcio2,ed.descripcio3,ed.download
                    FROM " . TABLE_LLISTATS . " e  
                    LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ed ON e.id = ed.llistats_id AND ed.language_id = '" . $language_id . "' 
                    WHERE e.id = " . $id_espai);
}


/**
 * 
 * @param type $group_id
 * @param type $any
 * @param type $mes
 * @param int $language_id
 * @param type $isMax
 * @return mysqli_result a tractar amb mysqli_fetch_array
 */
function tep_get_llistats_by_group_filter_any_mes($group_id, $any ='', $mes='', $language_id = false, $isMax = false) {

    if (!$language_id)
    {
        global $language_id;
    }
    
    $cerca = ($any != '' && $mes != '' ? (" AND MONTH(l.published) = '" . (int) $_GET['mes'] . "' " . " AND YEAR(l.published) = '" . (int) $_GET['any'] . "' ") : '');
    $maxValues = ( $isMax ? 'LIMIT '. HOME_MAX_PUBLICACIONS : '' );

    return tep_db_query("   SELECT l.id,l.active,l.listorder,l.lat,l.lon,l.autor,l.link,l.fotos_groups_id,l.generic1,l.generic2,l.generic3,l.generic4,
                                   l.generic5,l.inscripcions,l.published,ld.language_id,ld.titol,ld.titol2,ld.titol3,ld.descripcio,ld.descripcio2,ld.descripcio3,ld.download
                            FROM " . TABLE_LLISTATS . " l 
                            LEFT JOIN " . TABLE_LLISTATS_DESCRIPTION . " ld ON l.id=ld.llistats_id AND ld.language_id = '" . (int) $language_id . "' 
                            WHERE (l.group_id = " . $group_id . ") 
                            AND l.active='1'  
                            AND ld.titol<>'' 
                            " .$cerca. "
                            ORDER BY l.group_id,l.listorder,l.entered desc
                            " . $maxValues );
}

function tep_get_array_llistats_groups_by_id(&$llistatGroups, $idv)
{
    if ($idv > 0)
    {
        array_push($llistatGroups, $idv);
        $idvAux = tep_get_idparent_by_idgroup($idv);
        echo "<br />inici:" . $idvAux . "<br />";
        tep_get_array_llistats_groups_by_id($llistatGroups,$idvAux);
    }
}
    
function tep_get_idparent_by_idgroup($group_id)
{
    $groups_raw = tep_db_query(" SELECT bg.parent_id"
                             . " FROM " . TABLE_LLISTATS_GROUPS . " bg "
                             . " WHERE bg.id = '" . $group_id ."'");

    $values = tep_db_fetch_array($groups_raw);
    return $values['parent_id'];
}

//retornem un array amb totes les ids categories filles
function tep_get_array_llistat_groups_fills_by_id(&$array,$idv)
{
     $groups_raw =tep_db_query( " SELECT DISTINCT bg.id"
                             . " FROM " . TABLE_LLISTATS_GROUPS . " bg "
                             . " WHERE bg.parent_id = '" . $idv ."'");
    
    while ($item = tep_db_fetch_array($groups_raw)) 
    {
        array_push($array,$item['id']);
        tep_get_array_llistat_groups_fills_by_id($array,$item['id']);
    }
}

/**
 * Imprimim el grup de llistat en forma de <li></li>
 * Pensat per posar un ul block exterior
 * @param type $idGroup -> id del llistat que volem imprimir
 */
function tep_print_llistat_bygroup_inici ($idGroup,$language_id = false,$isMax = false)
{
    if (!$language_id)
    {
        global $language_id;
    }
    
    $listing = tep_get_llistats_by_group($idGroup,$language_id,$isMax);
    while ($item = tep_db_fetch_array($listing)) 
    {
        
        $itemInfo = new objectInfo($item);
         //busquem foto de portada de galeria
        $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($itemInfo->fotos_groups_id));
    
        ?>
        <li>
            <div class="publicacionsItemHome">
                <p class="data"><?php echo tep_date_short($itemInfo->published); ?> </p>
                <p class="titol"><a href="<?php echo tep_friendly_url('', tep_get_page_title(DEFAULT_PUBLICACIONS_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titol . '&modul=llistats'); ?>" class=""><?php echo $itemInfo->titol ?></a></p>
                <p class="desc"><?php echo(tep_text_curt($itemInfo->descripcio, '120')); ?></p>
            </div>
<!--            <div class="">
                <?php //if ($portada){echo tep_image(DIR_WS_PORTAL_IMAGE_FOTOS . "thumbs/" . $portada['image'], '', '', '') . "\n"; }?>
                <span class="sumari">
                    <span class="data color1"><?php //echo tep_date_short($itemInfo->data); ?> </span>
                    <span class="titol "><?php //echo $itemInfo->titol; ?> </span>                    
                    <span class="desc"><?php //echo(tep_text_curt($itemInfo->descripcio, '50')); ?></span>
                </span>
        
            </div>   -->
        </li>
        <?php
    } 
}



/**
 * 
 * @global int $language_id
 * @param type $maxItemsTotals = num de columns per files
 * @return string
 */
function clients_aleatori_foundation($maxItemsTotals = 4) 
{
    global $language_id;
    
    $maxItemsTotals = $maxItemsTotals - 1;

    $list_aux = '';
    
    // Banners
    $banners_sql = " select l.fotos_groups_id, l.link, ld.titol "
                 . " from " . TABLE_LLISTATS . " l "
                 . " left join " . TABLE_LLISTATS_DESCRIPTION . " ld on (l.id = ld.llistats_id) "
                 . " left join " . TABLE_LLISTATS_GROUPS . " lg on (l.group_id = lg.id) "
                 . " where l.active = '1' "
                 . " and lg.active='1'"
                 . " and ( l.group_id = '2' OR l.group_id = '3' OR l.group_id = '4' )  "
                 . " and ld.language_id = '" . $language_id . "'"
                 . " order by RAND()"
                 . " limit 12";
    
    
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);
    
    if ($banners_numrows > 0) 
    {
        $i = 0;
        $inici = false;
        while ($banner = tep_db_fetch_array($banners))
        {
            $bannerInfo = new objectInfo($banner);
            $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($bannerInfo->fotos_groups_id));
            
            if($i > $maxItemsTotals){$i = 0;}
            
            if($i == 0)
            {
                ?>
                <li>
                    <div class="row">
                <?php
            }
            
            ?>
            
            <div class="columns large-<?php echo (12/($maxItemsTotals+1)) ?> medium-<?php echo (12/($maxItemsTotals+1)) ?> small-<?php echo (12/($maxItemsTotals+1)) ?>">
            
                 <?php 
                if (is_array($portada)) 
                { 
                    ?>
                    <a href="<?php echo $bannerInfo->link; ?>" title="<?php echo $bannerInfo->titol; ?>" target="_blank">
                        <img src = "<?php echo DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image']; ?>" alt = "<?php echo $bannerInfo->titol; ?>" class = "imatge" />
                    </a>
                    <?php 
                } 
                else
                {
                    echo $bannerInfo->titol; 
                }
                ?>
            </div>
            
            <?php 
            
            if($i == $maxItemsTotals)
            {
                ?>
                    </div>
                </li>
                <?php
            }
            
            $i++;
        }
    }
    return $list_aux;
}
?>
