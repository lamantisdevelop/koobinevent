<?php

// The HTML href link wrapper function

function tep_href_link($page = '', $parameters = '', $connection = 'NONSSL') {
    global $PHP_SELF;

    if ($connection == 'NONSSL') {
        $link = HTTP_SERVER . DIR_WS_DOCUMENT_ROOT;
    } elseif ($connection == 'SSL') {
        if (ENABLE_SSL == 'true') {
            $link = HTTPS_SERVER . DIR_WS_DOCUMENT_ROOT;
        } else {
            $link = HTTP_SERVER . DIR_WS_DOCUMENT_ROOT;
        }
    } else {
        $link = HTTP_SERVER . DIR_WS_DOCUMENT_ROOT;
    }
    if ($parameters == '') {
        $link = $link . $page;
    } else {
        $link = $link . $page . '?' . $parameters;
    }

    while (substr($link, -5) == '&amp;')
        $link = substr($link, 0, -5);
    while (substr($link, -1) == '?')
        $link = substr($link, 0, -1);

    return $link;
}

// Hide form elements
function tep_hide_session_id() {
    if (tep_not_null(SID))
        return tep_draw_hidden_field(tep_session_name(), tep_session_id());
}

// The HTML image wrapper function
function tep_image($src, $alt = '', $width = '', $height = '', $params = '') {
    $image = '<img src="' . $src . '" alt="' . $alt . '"';
    if ($alt) {
        $image .= ' title=" ' . $alt . ' "';
    }
    if ($width) {
        $image .= ' width="' . $width . '"';
    }
    if ($height) {
        $image .= ' height="' . $height . '"';
    }
    if ($params) {
        $image .= ' ' . $params;
    }
    $image .= ' />';

    return $image;
}

// Outputs a image button
function tep_image_submit($image, $alt, $params = '') {
    global $language;

    return '<input type="image" src="' . DIR_WS_LANGUAGES . $language . '/images/buttons/' . $image . '" border="0" alt="' . $alt . '"' . (($params) ? ' ' . $params : '') . ' />';
}

// Output a function button in the selected language
function tep_image_button($image, $alt = '', $params = '') {
    global $language;

    return tep_image(DIR_WS_LANGUAGES . $language . '/image/button/' . $image, $alt, '', '', $params);
}

// Output a form
function tep_draw_form($name, $action, $method = 'post', $parameters = '') {
    $form = '<form name="' . $name . '" id="' . $name . '" action="' . $action . '" method="' . $method . '"';
    if (tep_not_null($parameters))
        $form .= ' ' . $parameters;
    $form .= '>';

    return $form;
}

// Output a form input field
function tep_draw_input_field($name, $value = '', $parameters = '', $required = false, $type = 'text', $reinsert_value = true) {
    $field = '<input type="' . $type . '" name="' . $name . '" id="' . $name . '"';
    if (($GLOBALS[$name]) && ($reinsert_value)) {
        $field .= ' value="' . htmlspecialchars(trim($GLOBALS[$name])) . '"';
    } elseif ($value != '') {
        $field .= ' value="' . htmlspecialchars(trim($value)) . '"';
    }
    if ($parameters != '') {
        $field .= ' ' . $parameters;
    }
    $field .= ' />';

    if ($required)
        $field .= TEXT_FIELD_REQUIRED;

    return $field;
}

// Output a form password field
function tep_draw_password_field($name, $value = '', $required = false) {
    $field = tep_draw_input_field($name, $value, 'maxlength="40"', $required, 'password', false);

    return $field;
}

// Output a form filefield
function tep_draw_file_field($name, $required = false, $parameters = '') {
    $field = tep_draw_input_field($name, '', $parameters, $required, 'file');

    return $field;
}

// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
function tep_draw_selection_field($name, $type, $value = '', $checked = false, $compare = '', $params = '') {
    $selection = '<input type="' . $type . '" class="' . $type . '" name="' . $name . '"';
    if ($type != 'radio') {
        $field .= ' id="' . $name . '"';
    }
    if ($value != '') {
        $selection .= ' value="' . $value . '"';
    }
    if (($checked == true) || ($GLOBALS[$name] == 'on') || ($value && ($GLOBALS[$name] == $value)) || ($value && ($value == $compare))) {
        $selection .= ' checked="checked"';
    }
    if ($params) {
        $selection .= ' ' . $params;
    }
    $selection .= ' />';

    return $selection;
}

// Output a form checkbox field
function tep_draw_checkbox_field($name, $value = '', $checked = false, $compare = '', $params = '') {
    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $compare, $params);
}

// Output a form radio field
function tep_draw_radio_field($name, $value = '', $checked = false, $compare = '', $params = '') {
    return tep_draw_selection_field($name, 'radio', $value, $checked, $compare, $params);
}

// Output a form textarea field
function tep_draw_textarea_field($name, $wrap, $width, $height, $text = '', $params = '', $reinsert_value = true) {
    $field = '<textarea name="' . $name . ' id="' . $name . '" wrap="' . $wrap . '" cols="' . $width . '" rows="' . $height . '"';
    if ($params)
        $field .= ' ' . $params;
    $field .= '>';
    if (($GLOBALS[$name]) && ($reinsert_value)) {
        $field .= $GLOBALS[$name];
    } elseif ($text != '') {
        $field .= $text;
    }
    $field .= '</textarea>';

    return $field;
}

// Output a form hidden field
function tep_draw_hidden_field($name, $value = '', $params = '') {
    $field = '<input type="hidden" class="hidden" name="' . $name . '" value="';
    if ($value != '') {
        $field .= trim($value);
    } else {
        $field .= trim($GLOBALS[$name]);
    }
    if ($params)
        $field .= ' ' . $params;
    $field .= '" />';

    return $field;
}

// Output a form pull down menu
function tep_draw_pull_down_menu($name, $values, $default = '', $params = '', $required = false) {
    $field = '<select name="' . $name . '"';
    if ($params)
        $field .= ' ' . $params;
    $field .= '>';
    for ($i = 0; $i < sizeof($values); $i++) {
        $field .= '<option value="' . $values[$i]['id'] . '"';
        if (((strlen($values[$i]['id']) > 0) && ($GLOBALS[$name] == $values[$i]['id'])) || ($default == $values[$i]['id'])) {
            $field .= ' selected="selected"';
        }
        $field .= '>' . $values[$i]['text'] . '</option>';
    }
    $field .= '</select>';

    if ($required)
        $field .= TEXT_FIELD_REQUIRED;

    return $field;
}

// Javascript to dynamically update the states/provinces list when the country is changed
function tep_js_zone_list($country, $form, $field) {
    $countries_query = tep_db_query("select distinct countries_id from " . TABLE_ZONES . " order by countries_id");
    $num_country = 1;
    $output_string = '';
    while ($countries = tep_db_fetch_array($countries_query)) {
        if ($num_country == 1) {
            $output_string .= '  if (' . $country . ' == "' . $countries['countries_id'] . '") {' . "\n";
        } else {
            $output_string .= '  } else if (' . $country . ' == "' . $countries['countries_id'] . '") {' . "\n";
        }

        $states_query = tep_db_query("select name, id from " . TABLE_ZONES . " where countries_id = '" . $countries['countries_id'] . "' order by name");

        $num_state = 1;
        while ($states = tep_db_fetch_array($states_query)) {
            if ($num_state == '1')
                $output_string .= '    ' . $form . '.' . $field . '.options[0] = new Option("' . TEXT_SELECT . '", "");' . "\n";
            $output_string .= '    ' . $form . '.' . $field . '.options[' . $num_state . '] = new Option("' . $states['name'] . '", "' . $states['id'] . '");' . "\n";
            $num_state++;
        }
        $num_country++;
    }
    $output_string .= '  } else {' . "\n" .
            '    ' . $form . '.' . $field . '.options[0] = new Option("' . TYPE_BELOW . '", "");' . "\n" .
            '  }' . "\n";

    return $output_string;
}

//*********** SEO URL *************//
// Friendy url
// The HTML href link wrapper function
// $from_idiomes indica si venim del header en el menu idiomes per a traduir la pagina actual
function tep_friendly_url($language = '', $title = '', $parameters = '', $from_idiomes = 0) {
    global $PHP_SELF, $_GET, $language_code;

    if (!tep_not_null($language)) {
        $language = $language_code;
    }
    if (!tep_not_null($modul)) {
        $modul = $_GET['modul'];
    }

    $link = HTTP_SERVER;
    $link = $link . friendly_param($language) . '/';
    $link = $link . friendly_param($title) . '/';

    $param = parse_parameters($parameters);

    // Data
    ($param['any'] != '') ? $link = $link . friendly_param($param['any']) . '-' : $link = $link;
    ($param['mes'] != '') ? $link = $link . friendly_param($param['mes']) . '-' : $link = $link;
    ($param['dia'] != '') ? $link = $link . friendly_param($param['dia']) . '/' : $link = $link;

    ($param['data'] != '') ? $link = $link . friendly_param($param['data']) . '/' : $link = $link;
    ($param['history'] != '') ? $link = $link . friendly_param($param['history']) . '/' : $link = $link;
    ($param['action'] != '') ? $link = $link . 'action-' . friendly_param($param['action']) . '/' : $link = $link;
    ($param['view'] != '') ? $link = $link . 'view-' . friendly_param($param['view']) . '/' : $link = $link;
    ($param['tag'] != '') ? $link = $link . 'tag-' . friendly_param($param['tag']) . '/' : $link = $link;
    ($param['page'] != '') ? $link = $link . friendly_param($param['page']) . '/' : $link = $link;

    ($param['item_aux'] != '') ? $link = $link . 'item-' . friendly_param($param['item_aux']) . '-' : $link = $link;

    //LLISTATS
    if ($param['modul'] == 'llistats' && $param['itemID'] != '') {
        if ($from_idiomes) {
            $param['item_title'] = tep_get_llistat_titol(friendly_param($param['itemID']), tep_get_languages_id($language));
        }
        $link = $link . friendly_param($param['itemID']) . '-' . friendly_param($param['item_title']) . '/';
    } else if (($param['modul'] == 'llistats' && $param['groupID'] != '') || (is_array($param['modul']) && $param['modul']['llistats'] == '' && $param['groupID'] != '')) {
        if ($from_idiomes) {
            $param['group_title'] = tep_get_llistat_group_name(friendly_param($param['groupID']), tep_get_languages_id($language));
        }
        $link = $link . friendly_param($param['group_title']) . '-' . friendly_param($param['groupID']) . '/';
    }

    //CATALEG
    if ($param['modul'] == 'botiga' && $param['cPath'] != '' && $param['itemID'] != '') {
        if ($from_idiomes) {
            $param['item_title'] = tep_get_products_name(friendly_param($param['itemID']), tep_get_languages_id($language));
        }
        $link = $link . friendly_param($param['itemID']) . '-' . friendly_param($param['item_title']) . '/';
    } else if ($param['modul'] == 'botiga' && $param['itemID'] != '') {
        if ($from_idiomes) {
            $param['item_title'] = tep_get_category_name(friendly_param($param['itemID']), tep_get_languages_id($language));
        }
        $link = $link . friendly_param($param['itemID']) . '-' . friendly_param($param['item_title']) . '/';
    }

    //NOTICIES
    if ((array_key_exists('noticies', $param) && $param['itemID'] != '') || ($param['modul'] == 'noticies' && $param['itemID'] != '')) {
        if ($from_idiomes) {
            $queryNoticia = tep_get_noticia_by_id($param['itemID'], tep_get_languages_id($language));
            $values = tep_db_fetch_array($queryNoticia);
            $param['item_title'] = $values['titular'];
        }
        $link = $link . friendly_param($param['itemID']) . '-' . friendly_param($param['item_title']) . '/';
    }

    //AGENDA
    if ($param['aID'] != '') {
        if ($from_idiomes) {
            $param['item_title'] = tep_get_activitats_nom(friendly_param($param['aID']), tep_get_languages_id($language));
        }
        $link = $link . friendly_param($param['item_title']) . '_' . friendly_param($param['aID']) . '/';
    }

    //AFEGIM .HTML
    while (substr($link, -1) == '/') {
        $link = substr($link, 0, -1);
    }
    $link = $link . '.html';

    //PARAMETRES NO FRIENDLY DIRECTAMENT AL LINK
    ($param['search'] != '') ? $link = $link . '?search=' . urlencode(strtolower($param['search'])) : $link = $link;
    ($param['news-search'] != '') ? $link = $link . '?news-search=' . urlencode(strtolower($param['news-search'])) : $link = $link;
    ($param['preview'] != '') ? $link = $link . '?preview=' . $param['preview'] : $link = $link;

    return $link;
}

function tep_get_friendly_params($exclude_array = '') {
    global $_GET;

    if ($exclude_array == '') {
        $exclude_array = array();
    }
    $get_url = '';
    reset($_GET);
    while (list($key, $value) = each($_GET)) {
        if ((tep_not_null($value)) && ($key != tep_session_name()) && ($key != 'error') && (!tep_in_array($key, $exclude_array)))
            $get_url .= $key . '=' . $value . '&';
    }
    return $get_url;
}

function parse_parameters($params) {
    $p = @explode('&', $params);
    $container = array();

    foreach ($p as $index => $valuepair) {
        $p2 = @explode('=', $valuepair);
        $container[$p2[0]] = $p2[1];
    }
    return $container;
}

function friendly_param($phrase) {

    // Treure caràcters extranys
    $table = array(
        'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
        'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
        'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
        'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
        'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
        'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r',
    );
    $result = strtr($phrase, $table);

    // Substituim caràcters no permesos
    $result = trim($result);
    $result = mb_strtolower($result, 'UTF-8');
    $result = preg_replace("/[^a-z0-9\s-]/", "", $result);
    $result = trim(preg_replace("/\s+/", " ", $result));
    $result = preg_replace("/\s/", "-", $result);
    $result = preg_replace("/--/", "-", $result);
    $result = substr($result, 0, 45);

    return $result;
}

// Canviar nom arxiu si existeix

function page_exists($id, $name, $language) {

    if (tep_not_null($name)) {
        $mail_query = tep_db_query("select pages_id from " . TABLE_PAGES_DESCRIPTION . " where page_title like '" . tep_db_input($name) . "' and pages_id != '" . (int) $id . "'and language_id =   '" . (int) $language . "'");
        $query_numrows = tep_db_num_rows($mail_query);
        if ($query_numrows > 0) {
            return true;
        } else {
            return false;
        }
    }
}

function rename_page_if_exists($id, $name, $language) {

    $name_aux = $name;
    while (page_exists($id, $name_aux, $language)) { // If file exists, add a number to it.
        $name_aux = $name . ++$i;
    }
    return $name_aux;
}

//***************************** Banners/Galeries *****************************//

function banners_list_group($group_id, $name_list = 'banners') {
    global $language_id;

    $list_aux = '';
    // Banners
    $banners_sql = "select b.id, b.name, bd.image, bd.link, bd.target, bd.title, bd.class, bd.rel, bd.rev, b.active from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on  bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' where b.active = '1' and b.group_id = '" . (int) $group_id . "' order by b.listorder";
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);

    if ($banners_numrows > 0) {
        echo '<ul class="' . $name_list . '">' . "\n";

        $i = 0;
        while ($banner = tep_db_fetch_array($banners)) {
            $i++;
            $bannerInfo = new objectInfo($banner);

            echo '<li ' . (($i == 1) ? 'class="primer"' : '') . '>'
            . ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '<a href="' . $bannerInfo->link . '" ' . (tep_not_null($bannerInfo->target) ? 'target="' . $bannerInfo->target . '"' : '') . ' title="' . $bannerInfo->title . '" class="' . $bannerInfo->class . '" rel="' . $bannerInfo->rel . '" >' : '')
            . '<img src="' . DIR_WS_PORTAL_BANNERS . $bannerInfo->image . '" alt="' . $bannerInfo->name . '"/>' . "\n"
            . ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '</a>' : '')
            . '</li>' . "\n";
        }
        echo '</ul>' . "\n";
    }

    return $list_aux;
}

/**
 * Imprimeix un grup de banners amb format llista sense imatge, nomes text (si passem classe block-grid mostra en columnes)
 * @global type $language_id
 * @param type $group_id
 * @param type $name_list
 * @return string
 */
function banners_list_group_foundation_text($group_id, $name_list = 'banners') {
    global $language_id;

    $list_aux = '';
    // Banners
    $banners_sql = "select b.id, b.name, bd.image, bd.link,bd.descripcio,bd.titol, bd.target, bd.title, bd.class, bd.rel, bd.rev, b.active from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on  bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' where b.active = '1' and b.group_id = '" . (int) $group_id . "' order by b.listorder";
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);

    if ($banners_numrows > 0) {
        echo '<ul class="' . $name_list . '">' . "\n";

        $i = 0;
        while ($banner = tep_db_fetch_array($banners)) {
            $i++;
            $bannerInfo = new objectInfo($banner);

            echo '<li ' . (($i == 1) ? 'class="primer"' : '') . '>'
            . '<div class="panel">'
            . ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '<a href="' . $bannerInfo->link . '" ' . (tep_not_null($bannerInfo->target) ? 'target="' . $bannerInfo->target . '"' : '') . ' title="' . $bannerInfo->title . '" class="' . $bannerInfo->class . ' no-fade" rel="' . $bannerInfo->rel . '" >' : '')
            //. '<img src="' . DIR_WS_PORTAL_BANNERS . $bannerInfo->image . '" alt="' . $bannerInfo->name . '"/>' . "\n"
            . '<h4>' . $bannerInfo->titol . '</h4>'
            . '<div class="desc">' . $bannerInfo->descripcio . '</div>'
            . ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '</a>' : '')
            . '</div>'
            . '</li>' . "\n";
        }
        echo '</ul>' . "\n";
    }

    return $list_aux;
}

/**
 * Imprimeix un grup de banners amb format imatge, sense text (si passem classe block-grid mostra en columnes)
 * @global type $language_id
 * @param type $group_id
 * @param type $name_list
 * @return string
 */
function banners_list_group_foundation($group_id) 
{
    global $language_id;

    $list_aux = '';
    
    // Banners
    $banners_sql = "select b.id, b.name, bd.image, bd.link,bd.descripcio,bd.titol, bd.target, bd.title, bd.class, bd.rel, bd.rev, b.active "
                 . "from " . TABLE_BANNERS . " b "
                 . "left join " . TABLE_BANNERS_DESCRIPTION . " bd on  bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' "
                 . "where b.active = '1' "
                 . "and b.group_id = '" . (int) $group_id . "' "
                 . "order by b.listorder";
    
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);
    if ($banners_numrows > 0) 
    {
        $i = 0;
        while ($banner = tep_db_fetch_array($banners)) 
        {
            $i++;
            $bannerInfo = new objectInfo($banner);
            ?>
                <li>
                    <div class="panelBannerInici">
                        <?php echo ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '<a href="' . $bannerInfo->link . '" ' . (tep_not_null($bannerInfo->target) ? 'target="' . $bannerInfo->target . '"' : '') . ' title="' . $bannerInfo->title . '" class="linkImg ' . $bannerInfo->class . '" rel="' . $bannerInfo->rel . '" >' : ''); ?>
                        <img src="<?php echo (DIR_WS_PORTAL_BANNERS . $bannerInfo->image . '" alt="' . $bannerInfo->name) ?> "/>         
                        <?php echo ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '</a>' : '') ?>
                        <h2 class="h2BannerInici"><?php echo ($bannerInfo->titol); ?></h2>
                        <?php echo ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '<a href="' . $bannerInfo->link . '" ' . (tep_not_null($bannerInfo->target) ? 'target="' . $bannerInfo->target . '"' : '') . ' title="' . $bannerInfo->title . '" class="button ' . $bannerInfo->class . '" rel="' . $bannerInfo->rel . '" >' . _('+ INFO') . '</a>' : ''); ?>
                    </div>
                </li>
            <?php 
        }
    }
    return $list_aux;
}

/**
 * Imprimeix un grup de banners amb format imatge. nomes li
 * @global type $language_id
 * @param type $group_id
 * @param type $name_list
 * @return string
 */
function banners_list_group_peu_foundation($group_id) 
{
    global $language_id;

    $list_aux = '';
    
    // Banners
    $banners_sql = "select b.id, b.name, bd.image, bd.link,bd.descripcio,bd.titol, bd.target, bd.title, bd.class, bd.rel, bd.rev, b.active "
                 . "from " . TABLE_BANNERS . " b "
                 . "left join " . TABLE_BANNERS_DESCRIPTION . " bd on  bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' "
                 . "where b.active = '1' "
                 . "and b.group_id = '" . (int) $group_id . "' "
                 . "order by b.listorder";
    
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);
    if ($banners_numrows > 0) 
    {
        $i = 0;
        while ($banner = tep_db_fetch_array($banners)) 
        {
            $i++;
            $bannerInfo = new objectInfo($banner);
                        
            ?>
                <li>
                    <div class="panelBannerIniciPeu">
                        <?php echo ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '<a href="' . $bannerInfo->link . '" ' . (tep_not_null($bannerInfo->target) ? 'target="' . $bannerInfo->target . '"' : '') . ' title="' . $bannerInfo->title . '" class="' . $bannerInfo->class . '" rel="' . $bannerInfo->rel . '" >' : ''); ?>
                        <img src="<?php echo (DIR_WS_PORTAL_BANNERS . $bannerInfo->image . '" alt="' . $bannerInfo->name) ?> "/>         
                        <?php echo ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '</a>' : '');  ?>
                    </div>
                </li>
            <?php 
        }
    }
    return $list_aux;
}

/**
 * @desc retornales imatges del grup de banners a dins (sense li ni ul) si detecta links en BD crea els <a>
 * @global type $language_id
 * @param type $group_id
 * @param type $name_list
 * @return string 
 */
function banners_list_plain_imgs_group($group_id, $class = 'banners') {
    global $language_id;

    $list_aux = '';
    // Banners
    $banners_sql = "select b.id, b.name, bd.image, bd.link,bd.title,bd.descripcio, bd.target, bd.class, bd.rel, bd.rev, b.active from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on  bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' where b.active = '1' and b.group_id = '" . (int) $group_id . "' order by b.listorder";
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);

    if ($banners_numrows > 0) {


        $i = 0;
        while ($banner = tep_db_fetch_array($banners)) {
            $i++;
            $bannerInfo = new objectInfo($banner);
            if ($bannerInfo->image) {
                echo '<div class="' . $class . (($i == 1) ? ' primer' : '') . '">' . "\n";
                echo ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '<a href="' . $bannerInfo->link . '" ' . (tep_not_null($bannerInfo->target) ? 'target="' . $bannerInfo->target . '"' : '') . ' title="' . $bannerInfo->name . '" class="imatge ' . $bannerInfo->class . '" rel="' . $bannerInfo->rel . '">' : '')
                . '<img class="' . (($i == 1) ? 'primer ' : '') . (tep_not_null($bannerInfo->descripcio) ? ' amb_desc' : '') . ' " src="' . DIR_WS_PORTAL_BANNERS . $bannerInfo->image . '" alt="' . $bannerInfo->name . '"/>' . "\n"
                . (tep_not_null($bannerInfo->descripcio) ? '<span class="banner_desc">' . $bannerInfo->descripcio . '</span>' : '') . "\n"
                . ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '</a>' : '')
                . "\n";
                echo '</div>';
            }
        }
    }

    return $list_aux;
}

function banners_slide_revolution($banner_group_id, $show_timmer = 1) {

    global $language_id;

    $banners_sql = "select b.id, b.name, bd.image, bd.link, bd.target, bd.rel,bd.descripcio from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on  b.id = bd.banners_id and bd.language_id = '" . (int) $language_id . "' where b.active = '1' and b.group_id = " . $banner_group_id . " order by b.listorder";
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);

    if ($banners_numrows > 0) {
        ?>
        <div class="bannercontainer">
            <div class="banner">
                <ul>
                        <?php
                        while ($image = tep_db_fetch_array($banners)) {
                            $imageInfo = new objectInfo($image);
                            ?> 
                        <li data-thumb="<?php echo DIR_WS_PORTAL_BANNERS . $imageInfo->image; ?>" data-masterspeed="200" data-slotamount="5" data-transition="random">

                            <?php //echo tep_image(DIR_WS_PORTAL_BANNERS . $imageInfo->image, $imageInfo->name);    ?>

                            <img src="<?php echo DIR_WS_PORTAL_BANNERS . $imageInfo->image; ?>" alt="<?php echo $imageInfo->name; ?>"/>


                                <?php if ($imageInfo->link != '' && $imageInfo->link != 'http://') { ?>
                                <a href="<?php echo $imageInfo->link; ?>" title="<?php echo $imageInfo->name; ?>" >
                            <?php } ?>

            <?php echo $imageInfo->descripcio; ?>

                        <?php if ($imageInfo->link != '' && $imageInfo->link != 'http://') { ?>
                                </a>
                        <?php } ?>

                        </li>
            <?php
        }
        ?>
                </ul>
                <div class="tp-bannertimer tp-bottom <?php echo ($show_timmer) ? '' : 'hide'; ?>"></div>
            </div>
        </div>
        <?php
    }
}

/**
 * Pinta l'imatge del banner com a link de youtube
 * El video es mostra dins d' un popup de fundation
 * No es fa servir target.
 * No s' ha provat amb cap altre embed
 * 
 * @global type $language_id -> idioma activat al sistema
 * @param type $idBanner -> id del banner que mostrarem
 * @param type $class -> classe que li donarem al contenidor del banner
 * @param type $classCapcelera -> classe que li donem a la capcelera del contenidor ( mirar )
 * @return string
 */
function tep_print_banner_POPUP_By_ID($idBanner, $class = 'banners', $classCapcelera = '') {
    global $language_id;

    $list_aux = '';
    // Banners
    $banners_sql = "select b.id, b.name, bd.image, bd.link,bd.titol,bd.title,bd.descripcio, bd.target, bd.class, bd.rel, bd.rev, b.active "
            . "from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on  bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' "
            . "where b.active = '1' and b.id = '" . (int) $idBanner . "' "
            . "order by b.listorder";
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);

    if ($banners_numrows > 0) {


        $i = 0;
        while ($banner = tep_db_fetch_array($banners)) {
            $i++;
            $bannerInfo = new objectInfo($banner);
            if ($bannerInfo->image) {
                ?>
                <div class="<?php echo $class . (($i == 1) ? ' primer' : '') ?>">
                    <div class="large-12 medium-12 small-12 <?php echo $classCapcelera ?>">Vídeo</div>
                    <a class="no-fade" title="Vídeo" href="#" data-reveal-id="videoModal<?php echo $idBanner ?>">
                        <img class="large-12 medium-12 small-12 <?php echo ($i == 1) ? 'primer ' : '' ?> " alt="Vídeo" src="<?php echo (DIR_WS_PORTAL_BANNERS . $bannerInfo->image) ?>">

                    </a>
                </div>
                <div id="videoModal<?php echo $idBanner ?>" class="reveal-modal" data-reveal>
                    <div class="flex-video">
                        <iframe width="970px" height="546px" src="<?php echo $bannerInfo->link ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <a class="close-reveal-modal no-fade">&#215;</a>
                </div>
                <?php
            }
        }
    }
    return $list_aux;
}

function banners_lateral_list($group_id, $class = 'banners') {
    global $language_id;

    $list_aux = '';
    // Banners
    $banners_sql = "select b.id, b.name,bd.titol,bd.descripcio, bd.image, bd.link, bd.target, bd.class, bd.rel, bd.rev, b.active from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on  bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' where b.active = '1' and b.group_id = '" . (int) $group_id . "'  AND bd.image<>'' order by b.listorder";
    $banners = tep_db_query($banners_sql);
    $banners_numrows = tep_db_num_rows($banners);

    if ($banners_numrows > 0) {


        $i = 0;
        while ($banner = tep_db_fetch_array($banners)) {
            $i++;
            $bannerInfo = new objectInfo($banner);
            if ($bannerInfo->titol != '') {//CAS mostrar titol
                /* echo('<div class="' . $class . (($i == 1) ? ' primer' : '') . '  separat">' . "\n");
                  echo('<h3>' . $bannerInfo->titol . '</h3>' . "\n");
                  echo('</div>' . "\n"); */
            }
            echo '<div class="' . $class . (($i == 1) ? ' primer' : '') . (($bannerInfo->titol != '') ? '' : ' separat') . ' ">' . "\n";
            echo ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '<a href="' . $bannerInfo->link . '" ' . (tep_not_null($bannerInfo->target) ? 'target="' . $bannerInfo->target . '"' : '') . ' title="' . $bannerInfo->name . '" class=" imatge ' . $bannerInfo->class . '" rel="' . $bannerInfo->rel . '">' : '')
            . '<img ' . (($i == 1) ? 'class="primer"' : '') . ' src="' . DIR_WS_PORTAL_BANNERS . $bannerInfo->image . '" alt="' . $bannerInfo->name . '"/>' . "\n"
            . ((tep_not_null($bannerInfo->link) && $bannerInfo->link != 'http://') ? '</a>' : '')
            . "\n";
            echo '</div>';
        }
    }

    return $list_aux;
}

// MENUS
// Obtenir arbre categories
// Obtenir arbre categories
function tep_get_menus_pages_list($menus_id, $name = '', $parent_id = '0', $expanded = 'false', $mostrar_fills = 'true') {
    global $language_id, $_GET;

    $pages_query = tep_db_query("select p.id, p.parent_id, p.action, p.listorder, p.active, pd.page_id, pd.link, pd.target, pd.class, pd.rel, pd.rev, pd.title, pd.alt_title from " . TABLE_MENUS_PAGES . " p left join  " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . $language_id . "'where p.menus_id = '" . (int) $menus_id . "' and p.parent_id = '" . $parent_id . "' and p.active = '1' order by p.listorder, pd.title");
    $pages_num = tep_db_num_rows($pages_query);
    if ($pages_num > 0) {
        $pages_list .= '<ul class="side-nav"' . (tep_not_null($name) ? 'id="' . $name . '"' : '') . ' ' . (($expanded == 'true') ? 'class="block"' : 'class="none" ') . '>'; // ID!!!

        $i = 0;
        while ($pages = tep_db_fetch_array($pages_query)) {
            $i++;
            $itemInfo = new objectInfo($pages);

            // Busquem quins son fills de la categoria actual
            $childs_array = array();
            $childs_query = tep_db_query("select distinct p.id, pd.page_id, p.parent_id, p.listorder from " . TABLE_MENUS_PAGES . " p left join " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . (int) $language_id . "' where p.parent_id = '" . $itemInfo->id . "' order by p.listorder");
            $childs_num = tep_db_num_rows($childs_query);
            if ($childs_num > 0) {
                while ($childs = tep_db_fetch_array($childs_query)) {
                    if (tep_not_null($childs['page_id']))
                        $childs_array[] = $childs['page_id'];
                }
            }

            // Definim classe per enllaç, normal/actual + fills:expanded/collapsed
            //$expanded = 'false';
            if ($childs_num > 0) {
                if (in_array($_GET['pageID'], $childs_array)) {
                    $css_class = (($_GET['pageID'] == $itemInfo->page_id ) ? 'actual' : '') . ' ' . (($i == 1) ? 'primer' : '') . '"';
                    $expanded = 'true';
                } else {
                    $css_class = 'class="collapsed ' . (($i == 1) ? 'primer' : '') . '"';
                }
            } else {
                $css_class = 'class="' . $itemInfo->class . (($_GET['pageID'] == $itemInfo->page_id ) ? ' actual' : '') . (($i == 1) ? ' primer' : '') . '"';
            }

            switch ($itemInfo->action) {
                case 'link':
                    if ($itemInfo->title) {
                        $pages_list .= '<li class="' . (($parent_id == 0) ? ' level_top id_' . $itemInfo->page_id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' selected' : '') . '"><a href="' . $itemInfo->link . '" ' . ((tep_not_null($itemInfo->target)) ? ' target="' . $itemInfo->target . '"' : '') . ((tep_not_null($itemInfo->rel)) ? ' rel="' . $itemInfo->rel . '"' : '') . ((tep_not_null($itemInfo->rev)) ? ' rev="' . $itemInfo->rev . '"' : '') . ' ' . $css_class . '>' . $itemInfo->title . '</a>';
                    }
                    break;

                default:
                    if ($itemInfo->title) {
                        $pages_list .= '<li class="' . (($parent_id == 0) ? ' level_top id_' . $itemInfo->id . '#panel' . $itemInfo->id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' selected' : '') . '"><a href="' . (($itemInfo->page_id) ? tep_friendly_url('', tep_get_page_title($itemInfo->page_id), '') : 'javascript:void(0);') . '" ' . $css_class . ' >' . $itemInfo->title . '</a>';
                    }
                    break;
            }
            if ($mostrar_fills) {
                $expanded = ($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? true : false;
                $pages_list .= tep_get_menus_pages_list($menus_id, '', $pages['id'], $expanded) . '</dd>';
            } else {
                // Només 1 nivell !!!
                $pages_list .= '</li>';
            }
        }
        $pages_list .= '</ul>';
    }
    return $pages_list;
}

function tep_print_menu_foundation_sidenav($menus_id, $parent_id = '0', $mostrar_fills = 'true', $es_pare = true) {
    global $language_id, $_GET;

    //obtenim les pàgines del menú
    $pages_query = tep_get_menu_by_id($menus_id, $parent_id, $language_id);
    $i = 0;
    while ($pages = tep_db_fetch_array($pages_query)) {//mostrem items pare
        $i++;
        $itemInfo = new objectInfo($pages);
        $id_menu = tep_id_menu_actual_page(); //obtenim menu actual per tal de saber on som
        if ($es_pare && $i > 1) 
        {//cas som primer nivell
            ?>
            <li class="divider">|</li>
            <?php 
            
        } ?>
        <li class="<?php echo (($es_pare) ? 'heading  id_' . $itemInfo->id : '') . (($_GET['pageID'] == $itemInfo->page_id ) ? ' active' : ''); ?>">
            <?php
            if ($itemInfo->action == "link") 
            {
                ?>
                <a href="<?php echo $itemInfo->link; ?>" <?php echo ((tep_not_null($itemInfo->target)) ? ' target="' . $itemInfo->target . '"' : '') . ((tep_not_null($itemInfo->rel)) ? ' rel="' . $itemInfo->rel . '"' : '') . ((tep_not_null($itemInfo->rev)) ? ' rev="' . $itemInfo->rev . '"' : ''); ?>>
                    <?php
            } 
            else 
            {
                ?>
                <a class="fade" href="<?php echo (($itemInfo->page_id) ? tep_friendly_url('', tep_get_page_title($itemInfo->page_id), '') : 'javascript:void(0);'); ?>" >
                    <?php
                    echo $itemInfo->title;
            }
            ?>
                </a>
        </li>
        <?php
        if ($mostrar_fills) {
            //$expanded = ($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? true : false;
            echo tep_print_menu_foundation_sidenav($menus_id, $pages['id'], true, false);
            ?>
            <?php
        }
    }
}

/* dona error actualment */

function tep_print_menu_by_id_advanced($menus_id, $name = '', $parent_id = '0', $expanded = 'false', $mostrar_fills = 'true') {
    global $language_id, $_GET;

    //obtenim les pàgines del menú
    $pages_query = tep_get_menu_by_id($menus_id, $parent_id, $language_id);
    $pages_num = tep_db_num_rows($pages_query);

    if ($pages_num > 0) {
        if ($parent_id != 0) {// primer mirem si es el primer nivell, en cas contrari posem div (submenus)
            ?>
            <div class="menu-dropdown">
                <?php } else { ?>
                <ul class="<?php echo $name ?>">
                    <?php
                }


                $i = 0;
                while ($pages = tep_db_fetch_array($pages_query)) {

                    if (($parent_id != 0) && ($i % 3 == 0)) {//si es submenu cada x elements fem columna (menu-sub)
                        ?>
                        <div class="<?php echo $name; ?>">
                            <ul class="menu-sub">
                                <?php
                                $tancat = false;
                            }
                            $itemInfo = new objectInfo($pages);

                            // Busquem submenus
                            $childs_array = array();
                            $childs_query = tep_get_menu_by_id($language_id, $itemInfo->id);
                            $childs_num = tep_db_num_rows($childs_query);
                            if ($childs_num > 0) {
                                while ($childs = tep_db_fetch_array($childs_query)) {
                                    if (tep_not_null($childs['page_id']))
                                        $childs_array[] = $childs['page_id'];
                                }
                            }

                            switch ($itemInfo->action) {
                                case 'link':
                                    ?>
                                    <li class="<?php echo (($parent_id == 0) ? '  id_' . $itemInfo->page_id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' selected' : ''); ?>">
                                        <a class="<?php echo (($parent_id != 0) ? 'menu-subbutton' : 'menu-button'); ?>" href="<?php echo $itemInfo->link; ?>" <?php echo ((tep_not_null($itemInfo->target)) ? ' target="' . $itemInfo->target . '"' : '') . ((tep_not_null($itemInfo->rel)) ? ' rel="' . $itemInfo->rel . '"' : '') . ((tep_not_null($itemInfo->rev)) ? ' rev="' . $itemInfo->rev . '"' : '') . ' ' . $css_class; ?>>
                                        <?php echo $itemInfo->title; ?>
                                        </a>
                                        <?php
                                        break;

                                    default:
                                        ?>
                                    <li class="<?php echo (($parent_id == 0) ? '  id_' . $itemInfo->id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' selected' : ''); ?>">
                                        <a class="<?php echo (($parent_id != 0) ? 'menu-subbutton' : 'menu-button'); ?>" href="<?php echo (($itemInfo->page_id) ? tep_friendly_url('', tep_get_page_title($itemInfo->page_id), '') : 'javascript:void(0);'); ?>" >
                                            <span class="menu-label"><?php echo $itemInfo->title; ?></span>
                                        </a>
                                        <?php
                                        break;
                                }
                                if ($mostrar_fills) {
                                    $expanded = ($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? true : false;
                                    echo tep_print_menu_by_id_advanced($menus_id, 'menu-row', $pages['id'], $expanded);
                                    ?>
                                </li>
                                <?php
                            }
                            $i++;
                            if (($parent_id != 0) && ($i % 3 == 0)) {//si es submenu cada x elements fem columna (menu-sub)
                                ?>
                            </ul>
                        </div>
                        <?php
                        $tancat = true; //ens assegurem que es tanqui (menu-sub) per a submenus amb elements no multiples de 3
                    }
                }
                if (!$tancat) {
                    ?>

                </ul> 
            </div>

            <?php
        }

        if ($parent_id != 0) { //tanquem menu-dropdown i menu-row o menu-top
            ?>
            </div>
        <?php } else { ?>

            </ul> 

            <?php
        }
    }
}

function tep_print_menu_by_id($menus_id, $name = '', $parent_id = '0', $expanded = 'false', $mostrar_fills = 'true') {
    global $language_id, $_GET;

    //obtenim les pàgines del menú
    $pages_query = tep_get_menu_by_id($menus_id, $parent_id, $language_id);
    $pages_num = tep_db_num_rows($pages_query);

    if ($pages_num > 0) {
        if ($parent_id != 0) {// primer mirem si es el primer nivell, en cas contrari posem div (submenus)
            ?>
            <div class="menu-dropdown">
                <?php } ?>
            <ul class="<?php echo $name ?>">
                <?php
                while ($pages = tep_db_fetch_array($pages_query)) {

                    $itemInfo = new objectInfo($pages);

                    // Busquem submenus
                    $childs_array = array();
                    $childs_query = tep_get_menu_pages_info($itemInfo->id);
                    $childs_num = tep_db_num_rows($childs_query);
                    if ($childs_num > 0) {
                        while ($childs = tep_db_fetch_array($childs_query)) {
                            if (tep_not_null($childs['page_id']))
                                $childs_array[] = $childs['page_id'];
                        }
                    }

                    switch ($itemInfo->action) {
                        case 'link':
                            ?>
                            <li class="<?php echo (($parent_id == 0) ? '  id_' . $itemInfo->page_id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' selected' : ''); ?>">
                                <a class="<?php echo (($parent_id != 0) ? 'menu-subbutton' : 'menu-button'); ?>" href="<?php echo $itemInfo->link; ?>" <?php echo ((tep_not_null($itemInfo->target)) ? ' target="' . $itemInfo->target . '"' : '') . ((tep_not_null($itemInfo->rel)) ? ' rel="' . $itemInfo->rel . '"' : '') . ((tep_not_null($itemInfo->rev)) ? ' rev="' . $itemInfo->rev . '"' : '') . ' ' . $css_class; ?>>
                                <?php echo $itemInfo->title; ?>
                                </a>
                                <?php
                                break;

                            default:
                                ?>
                            <li class="<?php echo (($parent_id == 0) ? '  id_' . $itemInfo->id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' selected' : ''); ?>">
                                <a class="<?php echo (($parent_id != 0) ? 'menu-subbutton' : 'menu-button'); ?>" href="<?php echo (($itemInfo->page_id) ? tep_friendly_url('', tep_get_page_title($itemInfo->page_id), '') : 'javascript:void(0);'); ?>" >
                                    <span class="menu-label"><?php echo $itemInfo->title; ?></span>
                                </a>
                                <?php
                                break;
                        }
                        if ($mostrar_fills) {
                            $expanded = ($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? true : false;
                            echo tep_print_menu_by_id($menus_id, 'menu-sub', $pages['id'], $expanded);
                            ?>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
            <?php
            if ($parent_id != 0) {// primer mirem si es el primer nivell, en cas contrari posem div (submenus)
                ?>
            </div>
        <?php } ?>
        <?php
    }
}

/**
 * Imprimeix menú amb format foundation4 TOP-BAR
 * @global type $language_id
 * @global type $_GET
 * @param type $menus_id
 * @param type $name
 * @param type $parent_id
 * @param type $expanded
 * @param type $mostrar_fills
 */
function tep_print_menu_foundation_by_id($menus_id, $class = '', $parent_id = '0', $expanded = 'false', $mostrar_fills = 'true') {
    global $language_id, $_GET;

    //obtenim les pàgines del menú
    $pages_query = tep_get_menu_by_id($menus_id, $parent_id, $language_id);
    $pages_num = tep_db_num_rows($pages_query);

    if ($pages_num > 0) {
        ?>
        <ul class="<?php echo $class . (($parent_id != 0) ? ' dropdown' : ' '); //dropdown si submenu (no es pare==0)                                  ?>">

            <?php
            while ($pages = tep_db_fetch_array($pages_query)) {

                $itemInfo = new objectInfo($pages);
                // Busquem submenus
                $childs_array = array();
                $childs_query = tep_get_menu_pages_info($itemInfo->id);
                $childs_num = tep_db_num_rows($childs_query);
                if ($childs_num > 0) {
                    while ($childs = tep_db_fetch_array($childs_query)) {//obtenim pagines per primer nivell de menu
                        if (tep_not_null($childs['page_id'])) {
                            $childs_array[] = $childs['page_id'];
                        }

                        $childs_query2 = tep_get_menu_pages_info(tep_id_menu_by_page($childs['page_id'])); //obtenim pagines per segon nivell de menu
                        $childs_num2 = tep_db_num_rows($childs_query2);
                        if ($childs_num2 > 0) {
                            while ($childs2 = tep_db_fetch_array($childs_query2)) {
                                if (tep_not_null($childs2['page_id'])) {
                                    $childs_array[] = $childs2['page_id'];
                                }
                            }
                        }
                    }
                }

                switch ($itemInfo->action) {
                    case 'link':
                        ?>
                        <li class="<?php echo (($parent_id == 0) ? '  id_' . $itemInfo->page_id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' active' : ''); ?>">
                            <a class="<?php echo (($parent_id != 0) ? 'menu-subbutton' : 'menu-button'); ?>" href="<?php echo $itemInfo->link; ?>" <?php echo ((tep_not_null($itemInfo->target)) ? ' target="' . $itemInfo->target . '"' : '') . ((tep_not_null($itemInfo->rel)) ? ' rel="' . $itemInfo->rel . '"' : '') . ((tep_not_null($itemInfo->rev)) ? ' rev="' . $itemInfo->rev . '"' : '') . ' ' . $css_class; ?>>
                            <?php echo $itemInfo->title; ?>
                            </a>
                            <?php
                            break;

                        default:
                            ?>
                        <li class="<?php echo (($childs_num > 0 && $mostrar_fills) ? ' has-dropdown not-click' : '') . (($parent_id == 0) ? '  id_' . $itemInfo->id : '') . (($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? ' active' : ''); ?>">
                            <a class=" no-fade <?php echo (($parent_id != 0) ? 'menu-subbutton' : 'menu-button'); ?>" href="<?php echo (($itemInfo->page_id) ? tep_friendly_url('', tep_get_page_title($itemInfo->page_id), '') : 'javascript:void(0);'); ?>" >
                                <span class="menu-label"><?php echo $itemInfo->title; ?></span>
                            </a>
                            <?php
                            break;
                    }
                    if ($mostrar_fills) {
                        $expanded = ($_GET['pageID'] == $itemInfo->page_id || in_array($_GET['pageID'], $childs_array)) ? true : false;
                        echo tep_print_menu_foundation_by_id($menus_id, 'dropdown', $pages['id'], $expanded);
                        ?>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
        <?php
    }
}

/**
 * Imprimeix un menu  en format sub-nav de foundation 
 * @global type $language_id
 * @global type $_GET
 * @param type $menus_id
 * @param type $name
 * @param type $parent_id
 * @param type $expanded
 * @param type $mostrar_fills
 */
function tep_print_menu_foundation_sub_nav($menus_id, $name = '', $parent_id = '0') {
    global $language_id, $_GET;

    //obtenim les pàgines del menú
    $pages_query = tep_get_menu_by_id($menus_id, $parent_id, $language_id);
    $pages_num = tep_db_num_rows($pages_query);

    if ($pages_num > 0) {
        ?>
        <dl class="sub-nav <?php echo $name; ?>">
            <dt></dt>

            <?php
            while ($pages = tep_db_fetch_array($pages_query)) {

                $itemInfo = new objectInfo($pages);


                switch ($itemInfo->action) {
                    case 'link':
                        ?> 
                        <dd class="<?php echo (($parent_id == 0) ? '  id_' . $itemInfo->page_id : '') . (($_GET['pageID'] == $itemInfo->page_id) ? ' active' : ''); ?>">
                            <a href="<?php echo $itemInfo->link; ?>" <?php echo ((tep_not_null($itemInfo->target)) ? ' target="' . $itemInfo->target . '"' : '') . ((tep_not_null($itemInfo->rel)) ? ' rel="' . $itemInfo->rel . '"' : '') . ((tep_not_null($itemInfo->rev)) ? ' rev="' . $itemInfo->rev . '"' : ''); ?>>
                            <?php echo $itemInfo->title; ?>
                            </a>
                            <?php
                            break;

                        default:
                            ?>
                        <dd class=" <?php echo (($_GET['pageID'] == $itemInfo->page_id) ? ' active' : ''); ?>">
                            <a class="fade" href="<?php echo (($itemInfo->page_id) ? tep_friendly_url('', tep_get_page_title($itemInfo->page_id), '') : 'javascript:void(0);'); ?>" >
                                <span class="menu-label"><?php echo $itemInfo->title; ?></span>
                            </a>
                            <?php
                            break;
                    }
                    ?>
                </dd>
                <?php
            }
            ?>
        </dl>
        <?php
    }
}

//Pintem el div del resultat de l' accio d' enviar del formulari de contatce
function pintaMissatgeResultatEnviar()
{
    if (isset($_GET['action']) && $_GET['action'] == 'success')
    {
        ?>
        <div data-alert class="alert-box success radius">  
            <div class='row'>
                <div class='columns large-1'>
                    <i class='fi-check'></i>
                </div>
                <div class='columns large-11'>
                    <?php echo _('Formulari enviat correctament'); ?>
                </div>
            </div>
        </div>
        <?php
    }
    else if ($_GET['action'] == 'error')
    {
        if (isset($_GET['action']) && $_GET['action'] == 'error')
        {
            ?>
            <div data-alert class="alert-box warning radius">  
                <div class='row'>
                    <div class='columns large-1'>
                        <i class='fi-x'></i>
                    </div>
                    <div class='columns large-11'>
                        <?php echo _('Error en enviar el formulari'); ?>
                    </div>
                </div>
            </div>
            <?php
        }
    } 
}

?>
