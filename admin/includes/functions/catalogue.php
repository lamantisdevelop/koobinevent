<?php

// Funcions catàleg: productes, categories, monedes, impostos...
//------------------------------------ Categories  ------------------------------------//
// Obtenir arbre categories
function tep_get_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false) {
    global $language_id;

    if (!is_array($category_tree_array))
        $category_tree_array = array();
    if ((sizeof($category_tree_array) < 1) && ($exclude != '0'))
        $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {
        $category_query = tep_db_query("select cd.name from " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.language_id = '" . $language_id . "' and cd.categories_id = '" . $parent_id . "'");
        $category = tep_db_fetch_array($category_query);
        $category_tree_array[] = array('id' => $parent_id, 'text' => htmlentities($category['name'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    }

    $categories_query = tep_db_query("select c.id, cd.name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.id = cd.categories_id and cd.language_id = '" . $language_id . "' and c.parent_id = '" . $parent_id . "' and c.active=1 order by c.listorder, cd.name");
    while ($categories = tep_db_fetch_array($categories_query)) {
        if ($exclude != $categories['id'])
            $category_tree_array[] = array('id' => $categories['id'], 'text' => $spacing . htmlentities($categories['name'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
        $category_tree_array = tep_get_category_tree($categories['id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array);
    }

    return $category_tree_array;
}

/**
 * @desc Obtenir els ids de les categories d'un producte
 * @param int $products_id
 * @return mysqli_result a tractar amb fetch_array
 */
function tep_get_categories_ids_by_product($products_id) {

    return tep_db_query("   SELECT categories_id
                            FROM " . TABLE_PRODUCTS_TO_CATEGORIES . " 
                            WHERE products_id = '" . $products_id . "' 
                            ORDER BY categories_id DESC");
}

/**
 * @desc Obtenir les categories on un producte es destacat
 * @param int $products_id
 * @return array
 */
function tep_get_categories_destacades_by_product($products_id, $language_id = false) {

    if (!$language_id) {
        global $language_id;
    }

    $categories = tep_db_query("   SELECT categories_id,products_id
                            FROM " . TABLE_PRODUCTS_DESTACATS . "
                            WHERE products_id = '" . $products_id . "' AND language_id=" . $language_id);
    while ($categoria = tep_db_fetch_array($categories)) {

        $return[$categoria['categories_id']] = $categoria;
    }
    return $return;
}

// Obtenir nom categoria
function tep_get_category_name($category_id,$language_id = false) {
      if (!$language_id) {
        global $language_id;
    }

    $category_query = tep_db_query("select name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . $category_id . "' and language_id = '" . (int) $language_id . "'");
    $category = tep_db_fetch_array($category_query);
    return $category['name'];
}

/**
 * Retorna tota la informació d'una categoria segons $id_categoria passat
 * @global type $language_id
 * @param type $id_categoria
 * @param type $language_id
 * @return type
 */
function tep_get_category_by_id($id_categoria, $language_id = false) {

    if (!$language_id) {
        global $language_id;
    }
    return tep_db_query("select distinct c.id,c.video,c.image,c.parent_id,c.listorder,c.fotos_groups_id,c.fotos_groups_id_premsa,cd.name,cd.subtitol,cd.description,cd.desc_short 
    from " . TABLE_CATEGORIES . " c
    left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on c.id = cd.categories_id and cd.language_id = '" . $language_id . "' 
    where c.id = '" . $id_categoria . "' 
    and c.active = '1' 
    and cd.language_id = '" . $language_id . "' limit 1");
}

// Saber si la categoria té subcategories
function tep_has_category_subcategories($category_id) {

    $child_category_query = tep_db_query("select count(*) as count from " . TABLE_CATEGORIES . " where parent_id = '" . $category_id . "'");
    $child_category = tep_db_fetch_array($child_category_query);

    if ($child_category['count'] > 0) {
        return true;
    } else {
        return false;
    }
}

// Obtenir array amb subcategories
function tep_get_subcategories(&$subcategories_array, $parent_id = 0) {

    $subcategories_query = tep_db_query("select id from " . TABLE_CATEGORIES . " where parent_id = '" . $parent_id . "'");
    while ($subcategories = tep_db_fetch_array($subcategories_query)) {
        $subcategories_array[sizeof($subcategories_array)] = $subcategories['id'];
        if ($subcategories['id'] != $parent_id) {
            tep_get_subcategories($subcategories_array, $subcategories['id']);
        }
    }
}

// Obtenir cami categories
function tep_get_path($current_category_id = '') {
    global $cPath_array;

    if ($current_category_id == '') {
        $cPath_new = implode('-', $cPath_array);
    } else {
        if (sizeof($cPath_array) == 0) {
            $cPath_new = $current_category_id;
        } else {
            $cPath_new = '';
            $last_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where id = '" . $cPath_array[(sizeof($cPath_array) - 1)] . "'");
            $last_category = tep_db_fetch_array($last_category_query);
            $current_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where id = '" . $current_category_id . "'");
            $current_category = tep_db_fetch_array($current_category_query);
            if ($last_category['parent_id'] == $current_category['parent_id']) {
                for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
                    $cPath_new .= '-' . $cPath_array[$i];
                }
            } else {
                for ($i = 0, $n = sizeof($cPath_array); $i < $n; $i++) {
                    $cPath_new .= '-' . $cPath_array[$i];
                }
            }
            $cPath_new .= '-' . $current_category_id;
            if (substr($cPath_new, 0, 1) == '-') {
                $cPath_new = substr($cPath_new, 1);
            }
        }
    }

    return 'cPath=' . $cPath_new;
}

// Generar cami cap a una categoria
function tep_generate_category_path($id, $from = 'category', $categories_array = '', $index = 0) {
    global $language_id;

    if (!is_array($categories_array))
        $categories_array = array();

    if ($from == 'product') {
        $categories_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $id . "'");
        while ($categories = tep_db_fetch_array($categories_query)) {
            if ($categories['categories_id'] == '0') {
                $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
            } else {
                $category_query = tep_db_query("select c.id, cd.name, c.parent_id from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on c.id = cd.categories_id where c.id = '" . (int) $categories['categories_id'] . "' and cd.language_id = '" . (int) $language_id . "'");
                $category = tep_db_fetch_array($category_query);
                $categories_array[$index][] = array('id' => $category['id'], 'text' => $category['name']);
                if ((tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0'))
                    $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
                $categories_array[$index] = array_reverse($categories_array[$index]);
            }
            $index++;
        }
    } elseif ($from == 'category') {
        $category_query = tep_db_query("select cd.name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.id = '" . (int) $id . "' and c.id = cd.categories_id and cd.language_id = '" . (int) $language_id . "'");
        $category = tep_db_fetch_array($category_query);
        $categories_array[$index][] = array('id' => $id, 'text' => $category['name']);
        if ((tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0'))
            $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
    }

    return $categories_array;
}

// Obtenir cadena amb id's cami categories
function tep_get_generated_category_path_ids($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = tep_generate_category_path($id, $from);
    for ($i = 0, $n = sizeof($calculated_category_path); $i < $n; $i++) {
        for ($j = 0, $k = sizeof($calculated_category_path[$i]); $j < $k; $j++) {
            $calculated_category_path_string .= $calculated_category_path[$i][$j]['id'] . '-';
        }
        $calculated_category_path_string = substr($calculated_category_path_string, 0, -1) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);

    if (strlen($calculated_category_path_string) < 1)
        $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
}

function tep_parse_category_path($cPath) {

    $cPath_array = array_map('tep_string_to_int', explode('-', $cPath));
    $tmp_array = array();
    $n = sizeof($cPath_array);
    for ($i = 0; $i < $n; $i++) {
        if (!in_array($cPath_array[$i], $tmp_array)) {
            $tmp_array[] = $cPath_array[$i];
        }
    }
    return $tmp_array;
}

// Obtenir categories "pares" de la donada
function tep_get_parent_categories(&$categories, $categories_id) {

    $parent_categories_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where id = '" . $categories_id . "'");
    while ($parent_categories = tep_db_fetch_array($parent_categories_query)) {
        if ($parent_categories['parent_id'] == 0)
            return true;
        $categories[sizeof($categories)] = $parent_categories['parent_id'];
        if ($parent_categories['parent_id'] != $categories_id) {
            tep_get_parent_categories($categories, $parent_categories['parent_id']);
        }
    }
}

/**
 * @desc a partir d'una categoria retorna un array d'objectes amb les categories pare (la categoria_id esta inclosa en aquest array)
 * @global type $language_id
 * @param int $categoria_id
 * @return array $parents 
 */
function tep_get_pares_categoria($categoria_id) {
    global $language_id;
    while ($categoria_id != 0) {
        $cat_raw = "select c.id, cd.name, c.parent_id from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on c.id = cd.categories_id and cd.language_id = '" . (int) $language_id . "' WHERE c.id=" . $categoria_id . " AND active=1";
        $cat_query = tep_db_query($cat_raw);
        $cat = new objectInfo(tep_db_fetch_array($cat_query));
        $categoria_id = (int) $cat->parent_id;
        $parents[sizeof($parents)] = $cat;
    }
    //retornem l'ordre invers de l'array per tal de mostrar les categories pares ordenades logicament
    return array_reverse($parents);
}

// Eliminar Categoria
function tep_remove_category($category_id) {
    // Busquem Imatge
    //$files_query = tep_db_query("select thumb, image from " . TABLE_CATEGORIES . " where id = '" . (int)$category_id . "'");
    //$files = tep_db_fetch_array($files_query);
    //if (file_exists(DIR_FS_PORTAL_IMAGE_SHOP . $files['thumb'])) @unlink(DIR_FS_PORTAL_IMAGE_SHOP . $files['thumb']);
    //if (file_exists(DIR_FS_PORTAL_IMAGE_SHOP . $files['image'])) @unlink(DIR_FS_PORTAL_IMAGE_SHOP . $files['image']);
    // Eliminem entrada general
    tep_db_query("delete from " . TABLE_CATEGORIES . " where id = '" . tep_db_input($category_id) . "'");
    // Borrar x descripcio
    //$languages = tep_get_languages();
    //for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
    //$lang_aux = $languages[$i]['id'];
    //$files_query = tep_db_query("select image from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$lang_aux . "'");
    //$files = tep_db_fetch_array($files_query);
    //if (file_exists(DIR_FS_PORTAL_IMAGE_SHOP . $files['image'])) @unlink(DIR_FS_PORTAL_IMAGE_SHOP . $files['image']);
    //}
    tep_db_query("delete from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int) $category_id . "'");
    // Borrar entrades relacionades
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int) $category_id . "'");
}

//------------------------------------ Productes  ------------------------------------//

function tep_get_product_path($products_id) {
    $cPath = '';

    $cat_count_sql = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . $products_id . "'");
    $cat_count_data = tep_db_fetch_array($cat_count_sql);

    if ($cat_count_data['count'] == 1) {
        $categories = array();

        $cat_id_sql = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . $products_id . "'");
        $cat_id_data = tep_db_fetch_array($cat_id_sql);
        tep_get_parent_categories($categories, $cat_id_data['categories_id']);

        $size = sizeof($categories) - 1;
        for ($i = $size; $i >= 0; $i--) {  // Size -> size-1, si no volem 1º categoria
            if ($cPath != '')
                $cPath .= '-';
            $cPath .= $categories[$i];
        }
        if ($cPath != '')
            $cPath .= '-';
        $cPath .= $cat_id_data['categories_id'];
    }
    return $cPath;
}

// Obtenir nom producte
function tep_get_products_name($products_id,$language_id) {
    
    if (!$language_id) {
        global $language_id;
    }

    $product_query = tep_db_query("select name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int) $products_id . "' and language_id = '" . (int) $language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['name'];
}

// Obtenir metas del  producte
function tep_get_products_meta($products_id) {
    global $language_id;

    $product_query = tep_db_query("select keywords,page_description,page_title from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int) $products_id . "' and language_id = '" . (int) $language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product;
}

// obtenim imatge producte
function tep_get_products_image($id) {

    $product_query = tep_db_query("select image from " . TABLE_PRODUCTS . " where id = '" . (int) $id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['image'];
}

// Obtenir productes d'una categoria
function tep_get_productes($category_id, $edicio = EDICIO_ACTUAL) {
    global $language_id;

    $productes_array = array();
    $productes_query = tep_db_query("SELECT p.id, pd.name 
                                        FROM " . TABLE_PRODUCTS . " p 
                                        LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " pc ON pc.products_id = p.id 
                                        LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON pd.products_id = p.id AND pd.language_id = " . $language_id . " 
                                        WHERE pc.categories_id = '" . $category_id . "' " . ($edicio ? ' AND p.id_edicions=' . $edicio : '') . "
                                        ORDER BY pd.name");

    while ($productes = tep_db_fetch_array($productes_query)) {
        $productes_array[] = array('id' => $productes['id'],
            'text' => $productes['name']);
    }

    return $productes_array;
}

// Obtenir desplegable productes
function tep_prepare_productes_pull_down($category_id = '') {

    $productes = tep_get_productes($category_id);

    if (sizeof($productes) > 0) {
        $productes_select = array(array('id' => '', 'text' => SELECT_PRODUCT));
        $productes = tep_array_merge($productes_select, $productes);
    } else {
        $productes = array(array('id' => '', 'text' => SELECT_CATEGORY));
    }

    return $productes;
}

// Eliminar Producte
function tep_remove_product($product_id) {
    // Busquem Imatge
    $files_query = tep_db_query("select image from " . TABLE_PRODUCTS . " where id = '" . (int) $product_id . "'");
    $files = tep_db_fetch_array($files_query);
    if (file_exists(DIR_FS_PORTAL_IMAGE_SHOP . $files['image']))
        @unlink
                        (DIR_FS_PORTAL_IMAGE_SHOP . $files[
                        'image']);
    // Eliminem entrada general
    tep_db_query("delete from " . TABLE_PRODUCTS . " where id = '" . (int) $product_id . "'");
    // Borrar x descripcio
    $languages = tep_get_languages();
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        $lang_aux = $languages[$i]['id'];
        $files_query = tep_db_query("select download from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int) $product_id . "' and language_id = '" . (int) $lang_aux . "'");
        $files = tep_db_fetch_array($files_query);
        if (file_exists(DIR_FS_PORTAL_DOWNLOADS . $files['download']))
            @unlink(DIR_FS_PORTAL_DOWNLOADS . $files['download']);
    }
    tep_db_query("delete from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int) $product_id . "'");

    // Borrar entrades relacionades
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $product_id . "'");
    // Borrar productes relacionades
    tep_db_query("delete from " . TABLE_PRODUCTS_RELATED . " where products_id = '" . (int) $product_id . "'");
}

/**
 * Elimina el producte i totes les seves relacions amb categories, funcions...
 * @param type $espectacle_id
 */
function tep_remove_product_and_relacions($espectacle_id) {
    $aux_id = $espectacle_id;
    $categories_query = tep_get_categories_ids_by_product($aux_id);
    while ($categoria = tep_db_fetch_array($categories_query)) {
        $delete_categories[] = $categoria['categories_id'];
    }
    // Borrem les seleccionades
    for ($i = 0, $n = sizeof($delete_categories); $i < $n; $i++) {
        // Busquem i modifiquem ordre
        $order_query = tep_db_query("select listorder from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $aux_id . "' and categories_id = '" . (int) $delete_categories[$i] . "'");
        $order = tep_db_fetch_array($order_query);
        $order_now = $order['listorder'];
        // Eliminem entrada
        tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $aux_id . "' and categories_id = '" . (int) $delete_categories[$i] . "'");
        // Reordenem items
        tep_reorder_items_group($order_now, TABLE_PRODUCTS_TO_CATEGORIES, 'categories_id', (int) $delete_categories[$i]);

        // Eliminem producte si no està enllaçat a cap categoria
        $product_categories_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $aux_id . "'");
        $product_categories = tep_db_fetch_array($product_categories_query);
        if ($product_categories['total'] == '0') {
            //Eliminem funcions de l'espectacle
            eliminar_funcions_espectacle((int) $aux_id);
            //eliminem espectacle
            tep_remove_product($aux_id); //esborra productes rel, descripcions, ...
        }
    }
}

/**
 * @desc suma a l'estoc del producte la quantitat indicada (si es passa negatiu resta)
 * @param int $quantitat
 * @param int $product_id


 * @return bool 
 */
function tep_update_estoc_product($quantitat, $product_id) {
    return tep_db_query("UPDATE " . TABLE_PRODUCTS . " SET quantity = quantity + " . (int) $quantitat . " WHERE id = " . (int) $product_id . "'");
}

// Script per actualitzar productes quan canviem categoria
function tep_js_productes_list($category, $form, $field, $edicio = EDICIO_ACTUAL) {
    global $language_id;

    $categories_query = tep_db_query("select distinct categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " order by categories_id");
    $num_category = 1;
    $output_string = '

      ';
    while ($categories = tep_db_fetch_array($categories_query)) {
        if ($num_category == 1) {
            $output_string .= ' if(' . $category . ' == "' . $categories['categories_id'] . '") {' . "\n";
        } else {
            $output_string .= ' } else if (' . $category . ' == "' . $categories['categories_id'] . '") {' . "\n";
        }

        $productes_query = tep_db_query("select pc.products_id, pd.name 
                                            from " . TABLE_PRODUCTS_TO_CATEGORIES . " pc  
                                            left join " . TABLE_PRODUCTS . " p on p.id = pc.products_id 
                                            left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on pd.products_id = pc.products_id and pd.language_id = '" . $language_id . "' 
                                            where pc.categories_id = '" . $categories['categories_id'] . "' " . ($edicio ? ' AND p.id_edicions=' . $edicio : '') . "
                                            order by name");

        $num_producte = 1;
        while ($productes = tep_db_fetch_array($productes_query)) {
            if ($num_producte == '1')
                $output_string .= ' ' . $form . ' . ' . $field . ' . options[0] = new Option("' . SELECT_PRODUCT . '", "");
                ' . "\n";

            $nom_producte = $productes['name'];
            $nom_producte = str_replace('\'', '', $nom_producte); // Comilla simple
            $nom_producte = str_replace('"', '', $nom_producte); // Comillas dobles inicio

            $output_string .= '    ' . $form . '.' . $field . '.options[' . $num_producte . '] = new Option("' . $nom_producte . '", "' . $productes['products_id'] . '");' . "\n";
            $num_producte++;
        }
        $num_category++;
    }
    $output_string .= '  } else {' . "\n" .
            '    ' . $form . '.' . $field . '.options[0] = new Option("' . SELECT_CATEGORY . '", "");' . "\n" .
            '  }' . "\n";

    return $output_string;
}

function tep_js_productes($category, $edicio = EDICIO_ACTUAL) {
    global $language_id;

    $categories_query = tep_db_query("select distinct categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " order by categories_id");
    $num_category = 1;
    $output_string = '';
    while ($categories = tep_db_fetch_array($categories_query)) {
        if ($num_category == 1) {
            $output_string .= '  if (' . $category . ' == "' . $categories['categories_id'] . '") {' . "\n";
        } else {
            $output_string .= '  } else if (' . $category . ' == "' . $categories['categories_id'] . '") {' . "\n";
        }

        $productes_query = tep_db_query("select pc.products_id, pd.name 
                            from " . TABLE_PRODUCTS_TO_CATEGORIES . " pc  
                            left join " . TABLE_PRODUCTS . " p on p.id = pc.products_id 
                            left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on pd.products_id = pc.products_id and pd.language_id = '" . $language_id . "' 
                            where pc.categories_id = '" . $categories['categories_id'] . "' and p.grouped = 0 " . ($edicio ? ' AND p.id_edicions=' . $edicio : '') . "
                            order by name");

        $num_producte = 1;
        $output_string .= '    new_values = "';
        while ($productes = tep_db_fetch_array($productes_query)) {
            if ($num_producte == '1')
                $output_string .= '<option value = \'0\'>' . SELECT_PRODUCT . '<\/option>';

            $nom_producte = $productes['name'];
            $nom_producte = str_replace('\'', '', $nom_producte); // Comilla simple
            $nom_producte = str_replace('"', '', $nom_producte); // Comillas dobles inicio

            $output_string .= '<option value=\'' . (int) $productes['products_id'] . '\'>' . $nom_producte . '<\/option>';
            $num_producte++;
        }
        $output_string .= '";' . "\n";
        $num_category++;
    }
    $output_string .= '  } else {' . "\n" .
            '    new_values = "<option value=\'0\'>' . SELECT_CATEGORY . '<\/option>";' . "\n" .
            '  }' . "\n";

    return $output_string;
}

// Funcions
function tep_js_products_price($form, $field) {
    global $currencies;

    $products_query = tep_db_query("select p.id, p.price, p.status from " . TABLE_PRODUCTS . " p where status = 1 order by id");
    $num_product = 1;
    $output_string = '';
    while ($products = tep_db_fetch_array($products_query)) {
        if ($num_product == 1) {
            $output_string .= '  if (product_id == "' . $products['id'] . '") {' . "\n";
        } else {
            $output_string .= '  } else if (product_id == "' . $products['id'] . '") {' . "\n";
        }
        $output_string .= '    ' . $form . '.' . $field . '.value = "' . $currencies->format($products['price']) . '";' . "\n";
        $num_product++;
    }
    $output_string .= '  }' . "\n";

    return $output_string;
}

// Script per actualitzar preus quan canviem opció producte
function tep_js_price_options_list($products_id) {
    global $currency, $currencies, $price_iva;

    $vars_string = '  var options_prices_array = new Array();' . "\n"
            . '  var decimal_places = ' . $currencies->currencies[$currency]['decimal_places'] . ';' . "\n"
            . '  var symbol_left = \'' . $currencies->currencies[$currency]['symbol_left'] . '\';' . "\n"
            . '  var symbol_right = \'' . $currencies->currencies[$currency]['symbol_right'] . '\';' . "\n"
            . '  var decimal_point = \'' . $currencies->currencies[$currency]['decimal_point'] . '\';' . "\n"
            . '  var thousands_point = \'' . $currencies->currencies[$currency]['thousands_point'] . '\';' . "\n"
            . '  var add_price = 0;' . "\n\n";
    ;
    $options_string = '';
    $actions_string = '  add_price = ';

// Atributs
    $options_listing = tep_db_query("select distinct pa.options_id, po.id, po.listorder, p.tax_class_id from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join  " . TABLE_PRODUCTS_OPTIONS . " po on po.id = pa.options_id  and po.language_id = '" . $language_id . "' left join " . TABLE_PRODUCTS . " p on p.id = pa.products_id where pa.products_id='" . $products_id . "' order by pa.options_listorder");
    if (tep_db_num_rows($options_listing) > 0) {
        $i = 0;
        while ($option = tep_db_fetch_array($options_listing)) {
            $i++;
            $opcioInfo = new objectInfo($option);

            $vars_string .= '  var opcio_value_' . $opcioInfo->options_id . ' = document.add_product.option_id_' . $opcioInfo->options_id . '.value;' . "\n";
            $vars_string .= '  options_prices_array[' . $opcioInfo->options_id . '] = new Array();' . "\n";
            $actions_string .= ( ($i > 1) ? ' + ' : '') . 'options_prices_array[' . $opcioInfo->options_id . '][opcio_value_' . $opcioInfo->options_id . ']';

            $values_listing = tep_db_query("select distinct pov.id as values_id, pa.price, pa.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov  on pa.values_id = pov.id and pa.options_id = pov.options_id where pa.products_id = '" . $products_id . "' and pa.options_id = '" . $opcioInfo->options_id . "' order by pa.values_listorder, pa.price");
            $values_rows = tep_db_num_rows($values_listing);
            while ($products_options = tep_db_fetch_array($values_listing)) {

                // IVA??
                $attr_price_aux = ($price_iva ? $currencies->display_number($products_options['price']) : $currencies->display_number(tep_substract_tax($products_options['price'], tep_get_tax_rate($opcioInfo->tax_class_id))));

                $options_string .= '  options_prices_array[' . $opcioInfo->options_id . '][' . $products_options['values_id'] . ']=' . $products_options['price_prefix'] . $attr_price_aux . ';' . "\n";
            }
        }
    } else {
        $actions_string .= '\'\'';
    }
    $actions_string .= ';' . "\n";
    $output_string = $vars_string . "\n" . $options_string . "\n" . $actions_string;

    return $output_string;
}

//------------------------------------ Productes: Opcions / Valors ---------------------------------//
// Obtenir array llista
function tep_get_option_groups_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct og.id, og.name from " . TABLE_PRODUCTS_OPTIONS_GROUPS . " og order by og.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

function tep_get_opcions_by_espectacle($id_espectacle, $id_opcio, $language_id = false) {
    if (!$language_id)
        global $language_id;

// Atributs
    $options = '';
    $options_name = tep_db_query("select distinct pa.options_id, po.id, po.name from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join " . TABLE_PRODUCTS_OPTIONS . " po on po.id = pa.options_id and po.language_id = '" . $language_id . "' where pa.products_id=" . $id_espectacle . " AND pa.options_id=" . (int) $id_opcio . " order by po.listorder");
//echo("select distinct pa.options_id, po.id, po.name from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join " . TABLE_PRODUCTS_OPTIONS . " po on po.id = pa.options_id and po.language_id = '" . $language_id . "' where pa.products_id='" . $id_espectacle . " AND pa.options_id=" . (int)$id_opcio . " ' order by po.listorder");
//echo($id_espectacle);
    if (tep_db_num_rows($options_name) > 0) {
        while ($option = tep_db_fetch_array($options_name)) {
            $products_options_array = array();
            $options_values_query = tep_db_query("  SELECT distinct pa.values_id, pa.price, pa.price_prefix, pov.name
                                                    FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa
                                                    LEFT JOIN " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov ON pov.id = pa.values_id  AND pov.language_id = '" . $language_id . "' where pa.products_id = '" . $id_espectacle . "' AND pa.options_id = '" . $option['id'] . "'
                                                    ORDER BY pov.listorder, pa.price");
            if (tep_db_num_rows($options_values_query) >= 1) {//cas multiples opcions (desplegable)
                while ($option_values = tep_db_fetch_array($options_values_query)) {
                    $products_options_array[] = array('id' => $option_values['values_id'], 'text' => $option_values['name']);
                }
            }
        }
    }
    return $products_options_array;
}

// Obtenir nom producte
function tep_get_option_name($option_id) {
    global $language_id;

    $option_query = tep_db_query("select nom from " . TABLE_PRODUCTS_OPTIONS . " where id = '" . (int) $option_id . "' and language_id = '" . (int) $language_id . "'");
    $option = tep_db_fetch_array($option_query);

    return $option['nom'];
}

// Obtenir nom producte
function tep_get_value_name($value_id) {
    global $language_id;

    $option_query = tep_db_query("select nom from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where id = '" . (int) $value_id . "' and language_id = '" . (int) $language_id . "'");
    $option = tep_db_fetch_array($option_query);

    return $option['nom'];
}

/**
 * Simple funció que ens diu si un producte es agrupat o no.
 */
function tep_product_grouped($id_product) {
    $query_prod = tep_db_query("select grouped from " . TABLE_PRODUCTS . " WHERE id = " . (int) $id_product);
    $result = tep_db_fetch_array($query_prod);
    if ($result['grouped'])
        return TRUE;
    else
        return FALSE;
}

/**
 * @file $itemInfo
 * Objecte amb la info del producte
 *
 * @info
 * Comprobar si el producte forma part d'un producte agrupat. Si es pare, aprofitem per buscar el preu 'desde' del producte
 *
 * @return
 * TRUE si forma part d'un producte agrupat
 * FALSE si no forma part o es un pare.
 */
function tep_in_grouped_product(&$itemInfo) {
// Si es un pare, tornem TRUE
    if ($itemInfo->grouped) {
        $itemInfo->price = tep_get_grouped_price($itemInfo->id);
        return TRUE;
    }
    $query = tep_db_query("select id_parent from " . TABLE_PRODUCTS_GROUPED . " where id_product = " . (int) $itemInfo->id);
    if (tep_db_fetch_array($query)) {
        return FALSE;
    }
    return TRUE;
}

/**
 * Buscamos los precios agrupados si un producto es agrupado.
 */
function tep_get_grouped_price($itemInfo) {

    $array_prices = array();

    $query = tep_db_query("select id_product from " . TABLE_PRODUCTS_GROUPED . " WHERE id_parent = " . (int) $itemInfo);

    while ($row = tep_db_fetch_array($query)) {

//Buscamos el precio del producto.
        $query_prod = tep_db_query("select price from " . TABLE_PRODUCTS . " WHERE id = " . (int) $row['id_product']);
        $product = tep_db_fetch_array($query_prod);

        if ($product) {
            $array_prices[$product['price']] = $product['price'];
        }
    }

    if ($array_prices) {
        ksort($array_prices);
        return current($array_prices);
    }
    return NULL;
}

/**
 * Buscamos toda la relación de productos agrupados
 */
function teb_get_all_grouped_products($id_product, $language_id = '') {
    $itemInfoGrouped = array();

    $options_name = tep_db_query("SELECT pd.name, p.id, p.price FROM " . TABLE_PRODUCTS_GROUPED . " pg 
                              JOIN " . TABLE_PRODUCTS . " p ON p.id = pg.id_product
                              JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd on pd.products_id 
                              WHERE pg.id_parent = " . $id_product . "
                               GROUP BY p.id ORDER BY p.price");

    while ($option = tep_db_fetch_array($options_name)) {
// Buscamos el producto
        $grouped_product = tep_db_query("select distinct p.id, p.grouped, p.price, p.weight, p2c.categories_id, pd.name, pd.desc_short, pd.description, pd.tecnical, pd.box_content, p.image, p.video, p.status, pd.download, p.espais_id, p.disponibility_id, p.available, p.tax_class_id, p.entered, s.new_price from " . TABLE_PRODUCTS . " p
  left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.id = pd.products_id and pd.language_id = '" . $language_id . "' 
  left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.id = p2c.products_id 
  left join " . TABLE_SPECIALS . " s on p.id = s.products_id and s.status = '1'
    where p.id = '" . (int) $option['id'] . "' 
    and p.status = '1' 
    and pd.language_id = '" . $language_id . "' limit 1");

        $grouped_item = tep_db_fetch_array($grouped_product);
        if ($grouped_item) {
            $itemInfoGrouped[$grouped_item['id']] = new objectInfo($grouped_item);
        }
    }
    return $itemInfoGrouped;
}

//------------------------------------ Productes: Accions  ------------------------------------//
// Obtenir producte aleatori
function tep_random_select($query) {

    $random_product = '';
    $random_query = tep_db_query($query);
    $num_rows = tep_db_num_rows($random_query);
    if ($num_rows > 0) {
        $random_row = tep_rand(0, ($num_rows - 1));
        tep_db_data_seek($random_query, $random_row);
        $random_product = tep_db_fetch_array($random_query);
    }
    return $random_product;
}

// Obtenir preu oferta
function tep_get_products_special_price($product_id) {

    $product_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id = '" . $product_id . "' and status");
    $product = tep_db_fetch_array($product_query);
    return $product['specials_new_products_price'];
}

// Comprovar si un producte té atributs
function tep_has_product_attributes($products_id) {

    $attributes_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . $products_id . "'");
    $attributes = tep_db_fetch_array($attributes_query);

    if ($attributes['count'] > 0) {
        return true;
    } else {
        return false;
    }
}

// Obtenir id producte a partir de l'id amb atributs
function tep_get_products_short_id($product_id) {

    $product_val = explode('{', $product_id);
    return $product_val[0];
}

// Obtenir array llista opcions
function tep_get_products_options_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct po.id, po.name from " . TABLE_PRODUCTS_OPTIONS . " po where po.language_id = '" . (int) $language_id . "' order by po.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

//------------------------------------ Productes: Disponibilitat  ------------------------------------//
// Obtenir array llista
function tep_get_disponibility_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct d.id, d.text from " . TABLE_DISPONIBILITY . " d where d.language_id = '" . (int) $language_id . "'order by d.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['text']);
    }
    return $values_array;
}

// Obtenir nom disponibilitat
function tep_get_disponibility_name($disponibility_id) {
    global $language_id;

    $disponibility_query = tep_db_query("select text from " . TABLE_DISPONIBILITY . " where id = '" . $disponibility_id . "' and language_id = '" . $language_id . "'");
    if (!tep_db_num_rows($disponibility_query)) {
        return $disponibility_id;
    } else {
        $disponibility = tep_db_fetch_array($disponibility_query);
        return $disponibility['text'];
    }
}

//------------------------------------ Productes: Llicenciats  ------------------------------------//
// Obtenir array llista
function tep_get_llicenciats_array($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct l.id, l.name from " . TABLE_LLICENCIATS . " l order by l.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => htmlentities($values['name'], ENT_QUOTES | ENT_IGNORE, "UTF-8"));
    }
    return $values_array;
}

// Obtenir nom disponibilitat
function tep_get_llicenciats_name($llicenciats_id) {

    $llicenciats_query = tep_db_query("select name from " . TABLE_LLICENCIATS . " where id = '" . $llicenciats_id . "'");
    if (!tep_db_num_rows($llicenciats_query)) {
        return $llicenciats_id;
    } else {
        $llicenciats = tep_db_fetch_array($llicenciats_query);
        return htmlentities($llicenciats['name'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
    }
}

//------------------------------------ Funcions d'espectacles  ------------------------------------//

/**
 * Retorna la info d'una funcio segons id
 * @param type $id_funcio
 * @return array
 */
function tep_get_funcio_by_id($id_funcio) {
    return tep_db_fetch_array(tep_db_query("   SELECT *
                            FROM " . TABLE_FUNCIONS . " 
                            WHERE id = '" . $id_funcio . "'"));
}

/**
 * @desc Retorna les funcions d'un espectacle
 * @param int $id_espectacle
 * @return mysqli_result (a tractar amb tep_db_fetch_array) 
 */
function tep_get_funcions_by_espectacle($id_espectacle, $nova = 2) {

    $query = ("  SELECT id,id_espectacle,nova,data 
                FROM " . TABLE_FUNCIONS . " f 
                WHERE f.id_espectacle = '" . (int) $id_espectacle . "'");

//mirem si volem mostrar nomes noves funcions o nomes normals(si $nova==2 mostra totes)
    if ($nova == 0) {
        $query .= " AND f.nova=0 ";
    } else if ($nova == 1) {
        $query .= " AND f.nova=1 ";
    }

    $query .= " order by f.data ";




    return tep_db_query($query);
}

/**
 * @desc Retorna les funcions d'un espectacle a partir d'una data. En cas de data buida, a partir d'avui
 * @param int $id_espectacle
 * @return mysqli_result (a tractar amb tep_db_fetch_array) 
 */
function tep_get_properes_funcions_by_espectacle($id_espectacle, $nova = 2, $data = false) {
    if (!$data)
        $data = date('Y-n-j H:i');

//obtenim dades de les fotos de la galeria per idioma
    $query = ("SELECT id,id_espectacle,nova,data
                            FROM " . TABLE_FUNCIONS . " f 
                            WHERE f.id_espectacle = '" . (int) $id_espectacle . "'  
                            AND f.data>='" . $data . "' ");

//mirem si volem mostrar nomes noves funcions o nomes normals(si $nova==2 mostra totes)
    if ($nova == 0) {
        $query .= " AND f.nova=0 ";
    } else if ($nova == 1) {
        $query .= " AND f.nova=1 ";
    }

    $query .= " ORDER BY f.data  ";

    return tep_db_query($query);
}

/**
 * @desc retorna la data de la propera funcio de l'espectacle
 * @param type $id_espectacle
 * @return datetime 
 */
function tep_get_data_propera_funcio_by_espectacle($id_espectacle) {
    $query_aux = tep_get_properes_funcions_by_espectacle($id_espectacle);
//agafem la primera data
    $funcio_array = tep_db_fetch_array($query_aux);

    if (isset($funcio_array['data'])) {//en el cas de destacats no mostrem els caducats
        $data_propera_funcio = $funcio_array['data'];
    }
    return $data_propera_funcio;
}

/**
 * @desc retorna una taula amb el camp data_hora dividit en dos (data / hora)
 * @param timestamp $data_hora
 * @return array $data_hora (amb un camp data i un camp hora)
 */
function obtenir_data_hora_funcio($data_hora) {

    $result['hora'] = substr($data_hora, -8, -3);
    $result['data'] = substr($data_hora, 0, 10);

    return $result;
}

/**
 * Retorna totes les funcions amb la informacio de l'espectacle i la data separada en dia mes i any
 * @global type $language_id
 * @param type $language_id
 * @return mysqli_result (a tractar amb tep_db_fetch_array) 
 */
function obtenir_all_funcions_format_calendari($language_id = false, $edicio = EDICIO_ACTUAL) {

    if (!$language_id) {
        global $language_id;
    }
    $query = (" SELECT DISTINCT f.id,f.id_espectacle,f.nova,f.data,DAYOFMONTH(f.data) as dia, MONTH(f.data) as mes, YEAR(f.data) as any , TIME(f.data) as hora, pd.name,pd.subtitol,p.fotos_groups_id,pd.destacat
                FROM " . TABLE_FUNCIONS . " f 
                LEFT JOIN " . TABLE_PRODUCTS . " p ON f.id_espectacle = p.id
                LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.id = pd.products_id AND pd.language_id = '" . $language_id . "'     
                WHERE p.status = '1' " . ($edicio ? ' AND id_edicions=' . $edicio : ''));


    $query .= " GROUP BY p.id ORDER BY f.data ";

    return tep_db_query($query);
}

/**
 * @desc elimina un espectacle de la BD
 * @param type $id_espectacle 
 */
function eliminar_funcions_espectacle($id_espectacle) {

    tep_db_query("delete from " . TABLE_FUNCIONS . " where id_espectacle = '" . (int) $id_espectacle . "'");
}

/**
 * @desc obte els espectacles segons si $destacat d'una categoria (ordenat per data de primera funcio) SI $DESTACAT=2 no s'utilitza el parametre!
 * @global int $language_id
 * @param int $id_categoria
 * @param int $destacat
 * @param int $language_id
 * @return array 
 */
function tep_get_espectacles_by_categoria($id_categoria, $destacat, $noves_funcions = 2, $mostrar_caducats = true, $language_id = false, $edicio = EDICIO_ACTUAL) {

    if (!$language_id) {
        global $language_id;
    }

//CAS mostrar tots els espectacles, indiferentment si son destacats o no
    $query_text = ("SELECT distinct p.id, pd.name,p.grouped,p.fotos_groups_id,p.fotos_groups_id_premsa,p2c.categories_id,pd.subtitol,pd.destacat,pd.link_comprar, pd.desc_short, pd.description, pd.tecnical, pd.box_content, p.image, p.video,p.video2, p.price, p.tax_class_id, p.espais_id, p.web_companyia,p.companyia,p.autor,p.disponibility_id, p.available, p.status, p.entered, pd.download,pd.download2,pd.download_premsa,pd.download_premsa2,pd.download_title,pd.download2_title,pd.download_premsa_title,pd.download_premsa2_title
                    FROM " . TABLE_PRODUCTS . " p  
                    LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.id = pd.products_id AND pd.language_id = '" . $language_id . "' 
                    LEFT JOIN  " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c ON p.id = p2c.products_id " );

    $where = " WHERE p.status = '1' AND p.id = p2c.products_id AND pd.products_id = p2c.products_id AND pd.language_id = '" . $language_id . "' AND p2c.categories_id = " . $id_categoria . ($edicio ? ' AND p.id_edicions=' . $edicio : '');

    if ($destacat == 1) {//CAS volem filtrar per destacats 
//$query_text .= ( "  AND pd.destacat=" . $destacat);
        $query_text .= ( "  LEFT JOIN " . TABLE_PRODUCTS_DESTACATS . " pdt ON pdt.products_id=p.id ");
        $where .= " AND pdt.categories_id=" . $id_categoria . " AND pdt.language_id=" . $language_id;
    }
    if ($destacat == 0) {//CAS volem filtrar per NO destacats 
        $query_text .= ( "  LEFT JOIN " . TABLE_PRODUCTS_DESTACATS . " pdt ON pdt.products_id=p.id AND pdt.categories_id=" . $id_categoria . " AND pdt.language_id=" . $language_id);
        $where .= " AND pdt.products_id is NULL AND pdt.categories_id is NULL AND pdt.language_id is NULL";
    }

    $query = tep_db_query($query_text . $where);

//recorrem tots els espectacles
    while ($espectacle = tep_db_fetch_array($query)) {
//busquem funcions de l'espectacle
        $query_aux = tep_get_properes_funcions_by_espectacle($espectacle['id'], $noves_funcions);
//agafem la primera data
        $funcio_array = tep_db_fetch_array($query_aux);


        if (!empty($funcio_array['data']) || $mostrar_caducats) {
            $espectacle['data_propera_funcio'] = $funcio_array['data'];
            $espectacles[$espectacle['id']] = $espectacle;
        }


        if ($destacat == 0 && $espectacle['destacat'] == 1 && isset($funcio_array['data'])) {//Eliminem espectacles destacats i amb dates properes xk ja han sortit a destacats (cas llistat espectacles no destacats)
            //unset($espectacles[$espectacle['id']]);
        }

        if ($noves_funcions == 1 && !isset($funcio_array['data'])) {//Cas mostrar només noves funcions, eliminem tots els espectacles que no tenen noves funcions
            unset($espectacles[$espectacle['id']]);
        }
    }
    ordenar_multiarray($espectacles, 'data_propera_funcio');

    return $espectacles;
}

/**
 * @desc obte els espectacles caducats segons si $destacat d'una categoria (ordenat per data de primera funcio) SI $DESTACAT=2 no s'utilitza el parametre!
 * @global int $language_id
 * @param int $id_categoria
 * @param int $destacat
 * @param int $language_id
 * @return array 
 */
function tep_get_espectacles_caducats_by_categoria($id_categoria, $destacat, $noves_funcions = 2, $language_id = false, $edicio = EDICIO_ACTUAL) {

    if (!$language_id) {
        global $language_id;
    }

//CAS mostrar tots els espectacles, indiferentment si son destacats o no
    $query_text = ("SELECT distinct p.id, pd.name,p.grouped,p.fotos_groups_id,p.fotos_groups_id_premsa,p2c.categories_id,pd.subtitol,pd.destacat, pd.desc_short, pd.description, pd.tecnical, pd.box_content, p.image, p.video,p.video2, p.price, p.tax_class_id, p.espais_id, p.web_companyia,p.companyia,p.autor,p.disponibility_id, p.available, p.status, p.entered, pd.download,pd.download2,pd.download_premsa,pd.download_premsa2,pd.download_title,pd.download2_title,pd.download_premsa_title,pd.download_premsa2_title
                    FROM " . TABLE_PRODUCTS . " p  
                    LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.id = pd.products_id AND pd.language_id = '" . $language_id . "' 
                    LEFT JOIN  " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c ON p.id = p2c.products_id " );

    $where = " WHERE p.status = '1' AND p.id = p2c.products_id AND pd.products_id = p2c.products_id AND pd.language_id = '" . $language_id . "' AND p2c.categories_id = " . $id_categoria . ($edicio ? ' AND p.id_edicions=' . $edicio : '');

    if ($destacat == 1) {//CAS volem filtrar per destacats 
//$query_text .= ( "  AND pd.destacat=" . $destacat);
        $query_text .= ( "  LEFT JOIN " . TABLE_PRODUCTS_DESTACATS . " pdt ON pdt.products_id=p.id ");
        $where .= " AND pdt.categories_id=" . $id_categoria . " AND pdt.language_id=" . $language_id;
    }
    if ($destacat == 0) {//CAS volem filtrar per NO destacats 
        $query_text .= ( "  LEFT JOIN " . TABLE_PRODUCTS_DESTACATS . " pdt ON pdt.products_id=p.id AND pdt.categories_id=" . $id_categoria . " AND pdt.language_id=" . $language_id);
        $where .= " AND pdt.products_id is NULL AND pdt.categories_id is NULL AND pdt.language_id is NULL";
    }

    $query = tep_db_query($query_text . $where);

//recorrem tots els espectacles
    while ($espectacle = tep_db_fetch_array($query)) {
//busquem funcions de l'espectacle
        $query_aux = tep_get_properes_funcions_by_espectacle($espectacle['id'], $noves_funcions);
//agafem la primera data
        $funcio_array = tep_db_fetch_array($query_aux);


        if (empty($funcio_array['data'])) {
            $espectacle['data_propera_funcio'] = $funcio_array['data'];
            $espectacles[$espectacle['id']] = $espectacle;
        }


        if ($destacat == 0 && $espectacle['destacat'] == 1 && isset($funcio_array['data'])) {//Eliminem espectacles destacats i amb dates properes xk ja han sortit a destacats (cas llistat espectacles no destacats)
            //unset($espectacles[$espectacle['id']]);
        }

        if ($noves_funcions == 1 && !isset($funcio_array['data'])) {//Cas mostrar només noves funcions, eliminem tots els espectacles que no tenen noves funcions
            unset($espectacles[$espectacle['id']]);
        }
    }
    ordenar_multiarray($espectacles, 'data_propera_funcio');

    return $espectacles;
}

/**
 * Retorna tots els espectacles actius
 * @global type $language_id
 * @param type $language_id
 * @return type
 */
function tep_get_espectacles($caducats = true, $language_id = false, $edicio = EDICIO_ACTUAL) {

    if (!$language_id) {
        global $language_id;
    }
    $query = tep_db_query("select distinct p.id,p.fotos_groups_id,pd.durada,pd.link_comprar, pd.destacat,p.grouped, p.price, p.weight, p2c.categories_id, pd.name,pd.subtitol, pd.desc_short, pd.description, pd.tecnical, pd.box_content, p.image, p.video,p.video2, p.status, pd.download,pd.download_title,pd.download2,pd.download2_title, p.espais_id, p.web_companyia,pd.llengua,pd.activitats_relacionades,p.companyia,p.autor, p.disponibility_id, p.available, p.tax_class_id, p.entered,pd.observacions from " . TABLE_PRODUCTS . " p
    left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.id = pd.products_id and pd.language_id = '" . $language_id . "' 
    left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.id = p2c.products_id 
    where p.status = '1' " . ($edicio ? ' AND id_edicions=' . $edicio : '') . "
    and pd.language_id = '" . $language_id . "'");

//recorrem tots els espectacles
    while ($espectacle = tep_db_fetch_array($query)) {
//busquem funcions de l'espectacle
        $query_aux = tep_get_properes_funcions_by_espectacle($espectacle['id']);
//agafem la primera data
        $funcio_array = tep_db_fetch_array($query_aux);

        if (!empty($funcio_array['data']) || $caducats) {
            $espectacle['data_propera_funcio'] = $funcio_array['data'];
            $espectacles[$espectacle['id']] = $espectacle;
        }
    }
    ordenar_multiarray($espectacles, 'data_propera_funcio');


    return $espectacles;
}

/**
 * Retorna tots els espectacles actius
 * @global type $language_id
 * @param type $language_id
 * @return type
 */
function tep_get_espectacles_caducats($language_id = false) {

    if (!$language_id) {
        global $language_id;
    }
    $query = tep_db_query("select distinct p.id,p.fotos_groups_id,pd.durada,pd.link_comprar, pd.destacat,p.grouped, p.price, p.weight, p2c.categories_id, pd.name,pd.subtitol, pd.desc_short, pd.description, pd.tecnical, pd.box_content, p.image, p.video,p.video2, p.status, pd.download,pd.download_title,pd.download2,pd.download2_title, p.espais_id, p.web_companyia,pd.llengua,pd.activitats_relacionades,p.companyia,p.autor, p.disponibility_id, p.available, p.tax_class_id, p.entered,pd.observacions from " . TABLE_PRODUCTS . " p
    left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.id = pd.products_id and pd.language_id = '" . $language_id . "' 
    left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.id = p2c.products_id 
    where p.status = '1' 
    and pd.language_id = '" . $language_id . "'");

//recorrem tots els espectacles
    while ($espectacle = tep_db_fetch_array($query)) {
//busquem funcions de l'espectacle
        $query_aux = tep_get_properes_funcions_by_espectacle($espectacle['id']);
//agafem la primera data
        $funcio_array = tep_db_fetch_array($query_aux);

        if (empty($funcio_array['data'])) {
            $espectacle['data_propera_funcio'] = $funcio_array['data'];
            $espectacles[$espectacle['id']] = $espectacle;
        }
    }
    ordenar_multiarray($espectacles, 'data_propera_funcio');


    return $espectacles;
}

/**
 * @desc obte els espectacles segons si $destacat d'una categoria (ordenat per data de primera funcio) SI $DESTACAT=2 no s'utilitza el parametre!
 * @global int $language_id
 * @param int $id_categoria
 * @param int $destacat
 * @param int $language_id
 * @return array 

  function tep_get_espectacles_by_categoria($id_categoria, $destacat, $noves_funcions=2, $language_id=false) {

  if (!$language_id) {
  global $language_id;
  }

  //CAS mostrar tots els espectacles, indiferentment si son destacats o no
  $query_text = ("SELECT distinct p.id, pd.name,p.grouped,p.fotos_groups_id,p.fotos_groups_id_premsa,pd.destacat, pd.desc_short, pd.description, pd.tecnical, pd.box_content, p.image, p.video, p.price, p.tax_class_id, p.espais_id, p.web_companyia,p.companyia,p.autor,p.disponibility_id, p.available, p.status, p.entered, pd.download,pd.download2,pd.download_premsa,pd.download_premsa2,pd.download_title,pd.download2_title,pd.download_premsa_title,pd.download_premsa2_title
  FROM " . TABLE_PRODUCTS . " p
  LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.id = pd.products_id AND pd.language_id = '" . $language_id . "'
  LEFT JOIN  " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c ON p.id = p2c.products_id
  WHERE p.status = '1' AND p.id = p2c.products_id AND pd.products_id = p2c.products_id AND pd.language_id = '" . $language_id . "' AND p2c.categories_id = " . $id_categoria );

  if ($destacat != 2) {//CAS volem filtrar per destacats o no
  $query_text .= ( "  AND pd.destacat=" . $destacat);
  }

  $query = tep_db_query($query_text);
  //recorrem tots els espectacles
  while ($espectacle = tep_db_fetch_array($query)) {
  //busquem funcions de l'espectacle
  $query_aux = tep_get_properes_funcions_by_espectacle($espectacle['id'], $noves_funcions);
  //agafem la primera data
  $funcio_array = tep_db_fetch_array($query_aux);


  $espectacle['data_propera_funcio'] = $funcio_array['data'];
  $espectacles[$espectacle['id']] = $espectacle;


  if ($destacat == 0 && $espectacle['destacat'] == 1 && isset($funcio_array['data'])) {//Eliminem espectacles destacats i amb dates properes xk ja han sortit a destacats (cas llistat espectacles no destacats)
  unset($espectacles[$espectacle['id']]);
  }

  if ($noves_funcions == 1 && !isset($funcio_array['data'])) {//Cas mostrar només noves funcions, eliminem tots els espectacles que no tenen noves funcions
  unset($espectacles[$espectacle['id']]);
  }
  }
  ordenar_multiarray($espectacles, 'data_propera_funcio');

  return $espectacles;
  }
 */

/**
 * @desc obtenir informacio d'un espectacle
 * @global type $language_id
 * @param type $id_espectacle
 * @param type $language_id
 * @return mysqli_result (a tractar amb tep_db_fetch_array)  
 */
function tep_get_espectacle_by_id($id_espectacle, $status = 1, $language_id = false) {

    if (!$language_id) {
        global $language_id;
    }
    return tep_db_query("select distinct p.id,p.fotos_groups_id,pd.durada,pd.link_comprar, pd.destacat,p.grouped, p.price, p.weight,p.facebook_send, p2c.categories_id, pd.name,pd.subtitol, pd.desc_short, pd.description, pd.tecnical, pd.box_content, p.image, p.video,p.video2, p.status, pd.download,pd.download_title,pd.download2,pd.download2_title, p.espais_id, p.web_companyia,pd.llengua,pd.activitats_relacionades,p.companyia,p.autor, p.disponibility_id, p.available, p.tax_class_id, p.entered,pd.observacions from " . TABLE_PRODUCTS . " p
  left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.id = pd.products_id and pd.language_id = '" . $language_id . "' 
  left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.id = p2c.products_id 
    where p.id = '" . $id_espectacle . "' 
    and p.status = '" . $status . "' 
    and pd.language_id = '" . $language_id . "' limit 1");
}

/**
 * @desc mostra la data en format text segons idioma i data passada
 * @global type $language_id
 * @param type $timestamp
 * @param type $language_id
 * @param type $hora
 * @return string 
 */
function mostrar_data_funcio_format_text($timestamp, $language_id = false, $data = true, $hora = true) {
    if (!$language_id) {
        global $language_id;
    }
    $dias[1] = array("Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte");
    $meses[1] = array("de gener", "de febrer", "de març", "d'abril", "de maig", "de juny", "de juliol", "d'agost", "de setembre", "d'octubre", "de novembre", "de desembre");
    $dias[2] = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
    $meses[2] = array("de Enero", "de Febrero", "de Marzo", "de Abril", "de Mayo", "de Junio", "de Julio", "de Agosto", "de Septiembre", "de Octubre", "de Noviembre", "de Diciembre");
    $dias[3] = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    $meses[3] = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $dias[4] = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
    $meses[4] = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

    $textes[1] = array('de' => ' de ', 'les' => ' a les ');
    $textes[2] = array('de' => ' de ', 'les' => ' a las ');
    $textes[3] = array('de' => ' de ', 'les' => ', ');
    $textes[4] = array('de' => ' de ', 'les' => ', ');

    $return = null;
    if (isset($timestamp)) {
        if ($data) {
            $return = $dias[$language_id][date('w', $timestamp)] . " " . date('d', $timestamp) . ' ' . $meses[$language_id][date('n', $timestamp) - 1];
        }
        if ($hora) {//cas mostrar hora
            $return .= /* $textes[$language_id]['les'] . */ date('H:i', $timestamp);
        }
    }
    return $return;
}

/**
 * Retorna dia i mes en format petit tipus icona
 * @global type $language_id
 * @param type $timestamp
 * @param type $language_id
 * @return string
 */
function mostrar_data_funcio_format_icona($timestamp, $language_id = false) {
    if (!$language_id) {
        global $language_id;
    }
    $meses[1] = array("gen", "feb", "mar", "abr", "mai", "jun", "jul", "ago", "set", "oct", "nov", "des");
    $meses[2] = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses[3] = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $meses[4] = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

    $return = null;
    if (isset($timestamp)) {
        $return = '<div class="dia">' . date('d', $timestamp) . '</div><div class="mes"> ' . $meses[$language_id][date('n', $timestamp) - 1] . '</div>';
    }
    return $return;
}

/**
 * Retorna tots els productes relacionats
 * @param type $id_espectacle
 * @global type $language_id
 * @return mysqli result a tractar amb fetch_array
 */
function tep_get_espectacles_relacionats($id_espectacle) {
    global $language_id;
    return tep_db_query("select pr.related_id, pd.name,p.fotos_groups_id 
                            from " . TABLE_PRODUCTS_RELATED . " pr 
                            left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on pr.related_id = pd.products_id 
                            left join " . TABLE_PRODUCTS . " p on pr.related_id = p.id
                            where pr.products_id = '" . $id_espectacle . "' 
                            and pd.language_id = '" . (int) $language_id . "'
                            order by pd.name");
}

/**
 * @desc retorna la primera i ultima data de les funcions de id_espectacle en format curt per a un llistat
 * @global type $language_id
 * @param <type> $id_espectacle
 * @param type $language_id
 * @return string
 */
function mostrar_dates_funcions_format_text($id_espectacle, $language_id = false) {
    if (!$language_id) {
        global $language_id;
    }

    $meses[1] = array("gen.", "febr.", "març", "abr.", "maig", "juny", "jul.", "ag.", "set.", "oct.", "nov.", "des.");

    $meses[2] = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");

    $meses[3] = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec");

    $meses[4] = array("Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Jui", "Aoû", "Sept", "Oct", "Nov", "Déc");


    $funcions = tep_get_funcions_by_espectacle($id_espectacle);
    $num_funcions = tep_db_num_rows($funcions);
    $i = 0;
    while ($funcio = tep_db_fetch_array($funcions)) {//recorrem totes les funcions de l'espectacle
        $timestamp = strtotime($funcio['data']);
        if (isset($timestamp)) {//si hi ha funcions
            if ($i == 0) {//primera data
                $return = date('d', $timestamp) . ' ' . $meses[$language_id][date('n', $timestamp) - 1];
            } elseif ($num_funcions > 1 && $i == $num_funcions - 1) {//SI hi ha més d'una funcio i estem a la ultima
                $return .= ' - ' . date('d', $timestamp) . ' ' . $meses[$language_id][date('n', $timestamp) - 1]; //última data
            }
        }
        $i++;
    }
    return $return;
}

/* -------------------------------------Pagina inici--------------------------------- */

/**
 * retorna tots els espectacles que hi ha a la pagina d'inici d'una categoria
 * @global type $language_id
 * @param type $categories_id
 * @param type $language_id
 * @return array d'espectacles
 */
function tep_get_espectacles_pagina_inici_by_category($categories_id, $language_id = false, $edicio = EDICIO_ACTUAL) {
    if (!$language_id) {
        global $language_id;
    }

    $query_text = " SELECT products_id,categories_id,language_id,listorder 
                    FROM " . TABLE_PRODUCTS_INICI . " pi
                    LEFT JOIN " . TABLE_PRODUCTS . " p ON pi.products_id=p.id 
                    WHERE pi.categories_id=" . $categories_id . " AND pi.language_id=" . $language_id . "
                    AND p.status=1 " . ($edicio ? ' AND p.id_edicions=' . $edicio : '') . "
                    ORDER BY listorder";

    $query = tep_db_query($query_text);

//recorrem tots els espectacles
    while ($espectacle = tep_db_fetch_array($query)) {
//omplim array amb espectacles (amb tota la info de cada un)
        $return[$espectacle['products_id']] = tep_db_fetch_array(tep_get_espectacle_by_id($espectacle['products_id']));
    }
    return $return;
}

/**
 * retorna tots els espectacles que hi ha a la pagina d'inici
 * @global type $language_id
 * @param type $language_id
 * @return array d'espectacles
 */
function tep_get_espectacles_pagina_inici($language_id = false, $edicio = EDICIO_ACTUAL) {
    if (!$language_id) {
        global $language_id;
    }

    $query_text = " SELECT products_id,categories_id,language_id,listorder 
                    FROM " . TABLE_PRODUCTS_INICI . " pi
                    LEFT JOIN " . TABLE_PRODUCTS . " p ON pi.products_id=p.id 
                    WHERE pi.language_id=" . $language_id . "
                    AND p.status=1 " . ($edicio ? ' AND p.id_edicions=' . $edicio : '') . "
                    ORDER BY listorder";

    $query = tep_db_query($query_text);

//recorrem tots els espectacles
    while ($espectacle = tep_db_fetch_array($query)) {
//omplim array amb espectacles (amb tota la info de cada un)
        $return[$espectacle['products_id']] = tep_db_fetch_array(tep_get_espectacle_by_id($espectacle['products_id']));
    }
    return $return;
}

//------------------------------------ Monedes  ------------------------------------//
// Comprovar si la moneda està definida
function tep_currency_exists($code) {

    $currency_code = tep_db_query("select currencies_id from " . TABLE_CURRENCIES . " where code = '" . $code . "'");
    if (tep_db_num_rows($currency_code)) {
        return $code;
    } else {
        return false;
    }
}

// Obtenir array monedes
function tep_get_currencies($default = '') {

    $currencies_array = array();
    if ($default) {
        $currencies_array[] = array('id' => '',
            'text' => $default);
    }
    $currencies_query = tep_db_query("select id, name from " . TABLE_CURRENCIES . " where active = '1' order by listorder");
    while ($currencies = tep_db_fetch_array($currencies_query)) {
        $currencies_array[] = array('id' => $currencies['id'],
            'text' => $currencies['name']);
    }
    return $currencies_array;
}

// Obtenir nom moneda
function tep_get_currencies_name($currencies_id) {

    $currencies_name_query = tep_db_query("select name from " . TABLE_CURRENCIES . " where id = '" . $currencies_id . "'");
    $currencies_name = tep_db_fetch_array($currencies_name_query);

    return $currencies_name['name'];
}

//------------------------------------ Impostos  ------------------------------------//
// Obtenir array llista
function tep_get_tax_class_array($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct tc.id, tc.title, tc.rate from " . TABLE_TAX_CLASS . " tc order by tc.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['title'] . ' (' . $values['rate'] . '%)');
    }
    return $values_array;
}

// Obtenir % impost
function tep_get_tax_rate($class_id) {

    $tax_query = tep_db_query("select rate from " . TABLE_TAX_CLASS . " where id = '" . (int) $class_id . "'");
    if (tep_db_num_rows($tax_query)) {
        $tax_multiplier = 0;
        while ($tax = tep_db_fetch_array($tax_query)) {
            $tax_multiplier += $tax['rate'];
        }
        return $tax_multiplier;
    } else {
        return 0;
    }
}

// Calcular impost sobre el preu
function tep_calculate_tax($price, $tax) {
    global $currencies;

    if (DISPLAY_PRICE_WITH_TAX == 'True') {
        return tep_round($tax * ($price / (100 + $tax)), 4);  // Numero, llocs decimals
    } else {
        return tep_round($tax * ($price / (100)), 4);  // Numero, llocs decimals ;
    }
}

// Sumar impost al preu
function tep_add_tax($price, $tax) {
    global $currencies;

    $tax_value = tep_round($tax * ($price / (100)), 4);  // Numero, llocs decimals ;
    return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + $tax_value;
}

// Restar impost al preu
function tep_substract_tax($price, $tax) {
    global $currencies;

    $tax_value = tep_round($tax * ($price / (100 + $tax)), 4);  // Numero, llocs decimals ;
    return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) - $tax_value;
}

//------------------------------------ Mòduls: Pagament i Enviament  ------------------------------------//
// Obtenir número de mòduls
function tep_count_modules($modules = '') {
    $count = 0;

    if (empty($modules))
        return $count;
    $modules_array = split(';', $modules);
    for ($i = 0, $n = sizeof($modules_array); $i < $n; $i++) {
        $class = substr($modules_array[$i], 0, strrpos($modules_array[$i], '.'));
        if (is_object($GLOBALS[$class])) {
            if ($GLOBALS[$class]->enabled) {
                $count++;
            }
        }
    }
    return $count;
}

// Obtenir número de mòduls d'enviament
function tep_count_shipping_modules() {

    return tep_count_modules(MODULE_SHIPPING_INSTALLED);
}

// Obtenir número de mòduls de pagament
function tep_count_payment_modules() {

    return tep_count_modules(MODULE_PAYMENT_INSTALLED);
}

//------------------------------------ Descomptes  ------------------------------------//
// Obtenir array llista tipus categories
function tep_get_descomptes_categories_array($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct dtc.id, dtc.title from " . TABLE_DESCOMPTES_TIPUS_CATEGORIES . " dtc order by dtc.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['title']);
    }
    return $values_array;
}

//------------------------------------ Comandes  ------------------------------------//
// Obtenir array llista status comandes
function tep_get_orders_status_array($default = '') {
    global $language_id;

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct os.id, os.name from " . TABLE_ORDERS_STATUS . " os where os.language_id = '" . (int) $language_id . "'order by os.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

// Obtenir nom zona geogràfica
function tep_get_orders_status_name($id) {
    global $language_id;

    $query = tep_db_query("select name from " . TABLE_ORDERS_STATUS . " where id='" . (int) $id . "' and language_id = '" . (int) $language_id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['name'];
}

//------------------------------------ Geo Zones  ------------------------------------//
// Obtenir array llista zones geogràfica
function tep_get_geo_zones_array($default = '') {

    $values_array = array();
    if ($default) {
        $values_array[] = array('id' => '',
            'text' => $default);
    }
    $query = tep_db_query("select distinct gz.id, gz.name from " . TABLE_GEO_ZONES . " gz order by gz.listorder");
    while ($values = tep_db_fetch_array($query)) {
        $values_array[] = array('id' => $values['id'],
            'text' => $values['name']);
    }
    return $values_array;
}

// Obtenir nom zona geogràfica
function tep_get_geo_zones_name($id) {

    $query = tep_db_query("select gz.name from " . TABLE_GEO_ZONES . " gz where gz.id='" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['name'];
}

// Obtenir nom zona geogràfica
function tep_get_tarifa_tipus_name($id) {

    $query = tep_db_query("select tt.name from " . TABLE_TARIFES_TIPUS . " tt where tt.id='" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['name'];
}

//------------------------------------ Clients  ------------------------------------//
// Obtenir nom zona geogràfica
function tep_get_customer_name($id) {

    $query = tep_db_query("select c.firstname, c.lastname from " . TABLE_CUSTOMERS . " c where c.id='" . (int) $id . "' limit 1");
    $values = tep_db_fetch_array($query);
    return $values['firstname'] . ' ' . $values['lastname'];
}

?>
