<?php
// Actualitzem visitants
function tep_update_whos_online() {
  global $customer_id;

  if (isset($customer_id)) {
    $wo_customer_id = $customer_id;
    // Buscar nom usuari
    //$customer_query = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . $customer_id . "'");
    //$customer = tep_db_fetch_array($customer_query);

    $wo_full_name = addslashes($customer['customers_firstname'] . ' ' . $customer['customers_lastname']);
  } else {
    $wo_customer_id = '';
    $wo_full_name = 'Guest';
  }

  $wo_session_id = tep_session_id();
  $wo_ip_address = getenv('REMOTE_ADDR');
  $wo_last_page_url = addslashes(getenv('REQUEST_URI'));

  $current_time = time();
  $xx_mins_ago = ($current_time - 600);
  $user_agent = getenv("HTTP_USER_AGENT");

  // Eliminem visitants sense activitat
  tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where time_last_click < '" . $xx_mins_ago . "'");

  $stored_customer_query = tep_db_query("select count(*) as count from " . TABLE_WHOS_ONLINE . " where session_id = '" . $wo_session_id . "'");
  $stored_customer = tep_db_fetch_array($stored_customer_query);

  if ($stored_customer['count'] > 0) {
    tep_db_query("update " . TABLE_WHOS_ONLINE . " set id = '" . $wo_customer_id . "', name = '" . $wo_full_name . "', ip_address = '" . $wo_ip_address . "', time_last_click = '" . $current_time . "', last_page_url = '" . $wo_last_page_url . "' where session_id = '" . $wo_session_id . "'");
  } else {
    tep_db_query("insert into " . TABLE_WHOS_ONLINE . " (id, name, session_id, ip_address, user_agent, time_entry, time_last_click, last_page_url) values ('" . $wo_customer_id . "', '" . $wo_full_name . "', '" . $wo_session_id . "', '" . $wo_ip_address . "', '" . $user_agent . "', '" . $current_time . "', '" . $current_time . "', '" . $wo_last_page_url . "')");
  }
}
?>
