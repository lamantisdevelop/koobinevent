<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */


function tep_get_page_by_id($page_id, $language_id = '') {

    //si no especifiquem idioma agafem l'actual
    if ($language_id == '') {
        global $language_id;
    }

    return tep_db_query("   select p.id,p.fotos_groups_id, pd.titol, pd.content, pd.title, pd.keywords, pd.description,p.parent_id ,p.action,p.banners_id 
                            from " . TABLE_PAGES . " p 
                            left join  " . TABLE_PAGES_DESCRIPTION . " pd 
                            on p.id = pd.pages_id 
                            and pd.language_id = '" . (int)$language_id . "' 
                            where p.id = '" . (int)$page_id . "' 
                            AND p.active=1 limit 1");
}
?>
