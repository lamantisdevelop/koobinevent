<?php
// Obtenir Idiomes
function tep_get_languages() {
  $languages_query = tep_db_query("select id, name, code, image, listorder, directory from " . TABLE_LANGUAGES . " where active=1  order by listorder");
  while ($languages = tep_db_fetch_array($languages_query)) {
    $languages_array[] = array('id' => $languages['id'],
                               'name' => $languages['name'],
                               'code' => $languages['code'],
                               'image' => $languages['image'],
                               'directory' => $languages['directory']);
  }
  return $languages_array;
}

// Obtenir Idiomes
function tep_get_active_languages() {
  $languages_query = tep_db_query("select id, name, code, image, listorder, directory from " . TABLE_LANGUAGES . " where active = '1' order by listorder ASC");
  while ($languages = tep_db_fetch_array($languages_query)) {
    $languages_array[] = array('id' => $languages['id'],
                               'name' => $languages['name'],
                               'code' => $languages['code'],
                               'image' => $languages['image'],
                               'directory' => $languages['directory']);
  }
  return $languages_array;
}

// Obtenir Directori Idioma
function tep_get_languages_directory($code) {
  global $language_id;

  $language_query = tep_db_query("select id, directory from " . TABLE_LANGUAGES . " where code = '" . $code . "'");
  if (tep_db_num_rows($language_query)) {
    $language = tep_db_fetch_array($language_query);
    return $language['directory'];
  } else {
    return false;
  }
}

// Obtenir Codi Idioma
function tep_get_languages_code($id) {
  global $language_id;

  $language_query = tep_db_query("select id, code from " . TABLE_LANGUAGES . " where id = '" . $id . "'");
  if (tep_db_num_rows($language_query)) {
    $language = tep_db_fetch_array($language_query);
    return $language['code'];
  } else {
    return false;
  }
}

// Obtenir Codi Idioma
function tep_get_languages_id($code) {
  global $language_id;

  $language_query = tep_db_query("select id, code from " . TABLE_LANGUAGES . " where code = '" . $code . "'");
  if (tep_db_num_rows($language_query)) {
    $language = tep_db_fetch_array($language_query);
    return $language['id'];
  } else {
    return false;
  }
}


/**
 * retorna el codi i18n d'un idioma
 * @global type $language_id
 * @param type $id
 * @return boolean
 */
function tep_get_languages_i18n_code($id) {
    global $language_id;

    $language_query = tep_db_query("select id, i18n_code from " . TABLE_LANGUAGES . " where id = '" . $id . "'");
    if (tep_db_num_rows($language_query)) {
        $language = tep_db_fetch_array($language_query);
        return $language['i18n_code'];
    } else {
        return false;
    }
}
?>
