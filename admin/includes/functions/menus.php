<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */

/**
 * retorna totes les pagines d'un menu (parent_id serveix per a agafar submenus interns)
 * @param type $menus_id
 * @param type $parent_id
 * @return type
 */
function tep_get_menu_by_id($menus_id, $parent_id = 0, $language_id = false) {
    if (!$language_id)
        global $language_id;
    return tep_db_query("select p.id, p.parent_id, p.action, p.listorder, p.active, pd.page_id, pd.link, pd.target, pd.class, pd.rel, pd.rev, pd.title, pd.alt_title from " . TABLE_MENUS_PAGES . " p left join  " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . $language_id . "'where p.menus_id = '" . (int) $menus_id . "' and p.parent_id = '" . $parent_id . "' and p.active = '1' order by p.listorder, pd.title");
}

/**
 * Retorna parent_id d'un element del menu
 * @param type $menu_id
 * @return int
 */
function tep_get_menu_parent_id($menu_item_id) {
    $sql_query = ("select p.parent_id 
        from " . TABLE_MENUS_PAGES . " p 
        where p.id = '" . (int) $menu_item_id . "' and p.active = '1' ");

    $return = tep_db_fetch_array(tep_db_query($sql_query));
    return $return['parent_id'];
}

/**
 * Retorna la informació de les pàgines d'un menú
 * @global type $language_id
 * @param type $id_menu
 * @return mysqli_result
 */
function tep_get_menu_pages_info($id_menu, $parent_id = 0, $inclou_inactius = 1) {
    global $language_id;
    $query = "select distinct p.id, pd.page_id, p.parent_id, p.listorder from " . TABLE_MENUS_PAGES . " p left join " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . (int) $language_id . "' where p.parent_id = '" . $id_menu . "'";
    $query .= $parent_id ? "and p.parent_id = '" . $parent_id : '';//incloem filtre per pare
    $query .= $inclou_inactius ? " and p.active = 1" : '';//incloem filtre actiu/inactiu
    $order_by = (" order by p.listorder");
    return tep_db_query($query . $order_by);
}

//Obtenir id del menu que apunta aquesta pàgina...aixi podem generar un arbre a partir dels fills nomes savent el id de pagina
//Potser es podria fer millor però la funció tep_get_menus_pages_list no m'anava bé perque els submenus m'els generava interns al <li> pare..i m'era dificil posicionar-los on jo volia...Poc codi i està solucionat...millor que adaptar-me a la funció peti qui peti
function tep_id_menu_actual_page() {
    global $language_id, $_GET;

    $sql = "select p.id,p.parent_id from " . TABLE_MENUS_PAGES . " p left join  " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . $language_id . "' where pd.page_id = '" . $_GET['pageID'] . "' and p.active = '1'";

    $pages_query = tep_db_query($sql);
    $pages_num = tep_db_num_rows($pages_query);
    if ($pages_num > 0) {
        $pages = tep_db_fetch_array($pages_query);
        if ($pages['parent_id'] == 0) {
            $id = $pages['id'];
        } else {
            $id = $pages['parent_id'];
        }
    } else {
        $id = -1;
    }
    return $id;
}

//Obtenir id del menu que apunta a una...aixi podem generar un arbre a partir dels fills nomes savent el id de pagina
//Potser es podria fer millor però la funció tep_get_menus_pages_list no m'anava bé perque els submenus m'els generava interns al <li> pare..i m'era dificil posicionar-los on jo volia...Poc codi i està solucionat...millor que adaptar-me a la funció peti qui peti
function tep_id_menu_by_page($page_id) {
    global $language_id;

    $sql = "select p.id,p.parent_id from " . TABLE_MENUS_PAGES . " p left join  " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . $language_id . "' where pd.page_id = '" . $page_id . "' and p.active = '1'";

    $pages_query = tep_db_query($sql);
    $pages_num = tep_db_num_rows($pages_query);
    if ($pages_num > 0) {
        $pages = tep_db_fetch_array($pages_query);

            $id = $pages['id'];
        
    } else {
        $id = -1;
    }
    return $id;
}
