<?php
function tep_db_connect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABASE, $link = 'db_link') {
  global $$link;

  $$link = mysqli_connect($server, $username, $password);
  
  if ($$link)
  {
     mysqli_select_db($$link, $database);
  }

  return $$link;
}

function tep_db_close($link = 'db_link') {
  global $$link;

  return mysqli_close($$link);
}

function tep_db_error($query, $errno, $error) {
  die('<font color="#000000"><b>' . $errno . ' - ' . $error . '<br><br>' . $query . '<br><br><small><font color="#ff0000">[TEP STOP]</font></small><br><br></b></font>');
}

function tep_db_query($query, $link = 'db_link') {
  global $$link, $logger;

  if (STORE_DB_TRANSACTIONS == 'true') {
    if (!is_object($logger)) $logger = new logger;
    $logger->write($query, 'QUERY');
  }

  $result = mysqli_query($$link, $query) or tep_db_error($query, mysqli_errno($$link), mysqli_error($$link));

  if (STORE_DB_TRANSACTIONS == 'true') {
    if (mysqli_error($$link)) $logger->write(mysqli_error($$link), 'ERROR');
  }

  return $result;
}

function tep_db_perform($table, $data, $action = 'insert', $parameters = '', $link = 'db_link') {
  if ($action == 'insert') {
    reset($data);
    $query = 'insert into ' . $table . ' (';
    while (list($columns, ) = each($data)) {
      $query .= $columns . ', ';
    }
    $query = substr($query, 0, -2) . ') values (';
    reset($data);
    while (list(, $value) = each($data)) {
      switch ((string)$value) {
        case 'now()':
          $query .= 'now(), ';
          break;
        case 'null':
          $query .= 'null, ';
          break;
        default:
          $query .= '\'' . tep_db_input($value) . '\', ';
          break;
      }
    }
    $query = substr($query, 0, -2) . ')';
  } elseif ($action == 'update') {
    reset($data);
    $query = 'update ' . $table . ' set ';
    while (list($columns, $value) = each($data)) {
      switch ((string)$value) {
        case 'now()':
          $query .= $columns . ' = now(), ';
          break;
        case 'null':
          $query .= $columns .= ' = null, ';
          break;
        default:
          $query .= $columns . ' = \'' . tep_db_input($value) . '\', ';
          break;
      }
    }
    $query = substr($query, 0, -2) . ' where ' . $parameters;
  } elseif ($action == 'delete') {
    $query = 'delete from ' . $table . ' where ' . $parameters;
  }
  return tep_db_query($query, $link);
}

function tep_db_fetch_array($db_query) {
  return mysqli_fetch_array($db_query, MYSQL_ASSOC);
}

function tep_db_result($res, $row=0, $col=0) {
    $numrows = mysqli_num_rows($res); 
    if ($numrows && $row <= ($numrows-1) && $row >=0){
        mysqli_data_seek($res,$row);
        $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
        if (isset($resrow[$col])){
            return $resrow[$col];
        }
    }
    return false;
}

function tep_db_num_rows($db_query) {
	return $db_query->num_rows;
}

function tep_db_data_seek($db_query, $row_number) {
  return mysqli_data_seek($db_query, $row_number);
}

function tep_db_insert_id($link = 'db_link') {
  global $$link;
  return mysqli_insert_id($$link);
}

function tep_db_free_result($db_query) {
  return mysqli_free_result($db_query);
}

function tep_db_fetch_fields($db_query) {
  return mysqli_fetch_field($db_query);
}

function tep_db_output($string) {
  return stripslashes($string);
}

function tep_db_input($string) {
  return addslashes($string);
}

function tep_db_prepare_input($string) {
  if (is_string($string)) {
    return trim(stripslashes($string));
  } elseif (is_array($string)) {
    reset($string);
    while (list($key, $value) = each($string)) {
      $string[$key] = tep_db_prepare_input($value);
    }
    return $string;
  } else {
    return $string;
  }
}
?>
