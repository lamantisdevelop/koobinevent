<?

// Remember to copy files from the SDK's src/ directory to a
// directory in your application on the server, such as php-sdk/
include('src/facebook.php');

function check_login_facebook() {
    $config = array(
        'appId' => FACEBOOK_APP_ID,
        'secret' => FACEBOOK_SECRET,
    );

    $facebook = new Facebook($config);
    $user_id = $facebook->getUser();

    if (strlen($user_id) != 0) {
        $login_url = '';
    } else {

        // No user, so print a link for the user to login
        // To post to a user's wall, we need publish_stream permission
        // We'll use the current URL as the redirect_uri, so we don't
        // need to specify it here.
        $login_url = $facebook->getLoginUrl(array('scope' => 'publish_stream, manage_pages'));
        //echo 'Please <a href="' . $login_url . '">login.</a>';
    }

    return $login_url;
}

function ret_logout_facebook() {
    $config = array(
        'appId' => FACEBOOK_APP_ID,
        'secret' => FACEBOOK_SECRET,
    );

    $facebook = new Facebook($config);
    $user_id = $facebook->getUser();
    if ($user_id) {
        $logout = $facebook->getLogoutUrl();
    } else {
        $logout = '';
    }
    return $logout;
}

function add_post_facebook($notInfo, $link, $image) {
    $config = array(
        'appId' => FACEBOOK_APP_ID,
        'secret' => FACEBOOK_SECRET,
    );

    //codi de la pagina  a la qual volem fr el post al mur
    $page_id = FACEBOOK_PAGE;

    $facebook = new Facebook($config);
    $user_id = $facebook->getUser();

    if ($user_id) {

        // We have a user ID, so probably a logged in user.
        // If not, we'll get an exception, which we handle below.
        try {

            //Per entrar amb una compte diferent de la principal, necessitem cercar el acces_token
            //sino entrariem cm l'usuari principal i no pas com l'admin de la pagina del facebook on volem fer el post
            if ($page_id != '') {
                $pag = $facebook->api('/me/accounts');

                foreach ($pag["data"] as $page) {
                    if ($page["id"] == $page_id) {
                        $page_access_token = $page["access_token"];
                        break;
                    }
                }
            }

            //Aquesta funci� es necessaria si no volem fer els posts en nom de l'usuari principal
            //Suposant que l'usuari principal tingui m�s d'una identitat (cas clustermoto.org, usuari te identital personal i identitat administrador pagina facebook, amb mateix usuari i contrasenya d'entrada)
            if ($page_id != '') {
                $facebook->setAccessToken($page_access_token);
            }

            //echo $message_array[$lang_id];
            $ret_obj = $facebook->api('/' . $page_id . '/feed', 'POST', array(
                'link' => $link,
                'message' => strip_tags(trim($notInfo->titular)),
                'picture' => $image
                    ));
            //mirem si venim de noticies o espectacles, per tal d'actualitzar la taula corresponent
            if ($notInfo->noticies_id) {//es noticia
                $id = (int) $notInfo->noticies_id;
                $taula = TABLE_NOTICIES;
            } else {//productes / espectacles
                $id = (int) $notInfo->id;
                $taula = TABLE_PRODUCTS;
            }
            //indiquem que s'ha publicat a facebook
            tep_db_query('UPDATE ' . $taula . ' SET facebook_send = 1 WHERE id = ' . $id);
            echo "<script language='javascript'> alert('Noticia publicada correctamente!'); </script>";



            //Control errors facebook
        } catch (FacebookApiException $e) {
            // If the user is logged out, you can have a 
            // user ID even though the access token is invalid.
            // In this case, we'll get an exception, so we'll
            // just ask the user to login again here.
            $login_url = $facebook->getLoginUrl(array(
                'scope' => 'publish_stream, manage_pages'
                    ));
            echo 'FACEBOOK: Siusplau <a href="' . $login_url . '">No estas loguejat, clic aquí per loguejar-te.</a>';
            echo $e->getMessage();
            error_log($e->getType());
            error_log($e->getMessage());
        }
        // Give the user a logout link 
        //echo '<br /><a href="' . $facebook->getLogoutUrl() . '">Desloguejar</a>';
    } else {

        // No user, so print a link for the user to login
        // To post to a user's wall, we need publish_stream permission
        // We'll use the current URL as the redirect_uri, so we don't
        // need to specify it here.
        $login_url = $facebook->getLoginUrl(array('scope' => 'publish_stream, manage_pages'));
        echo 'Please <a href="' . $login_url . '">loguejar.</a>';
    }
}

?>