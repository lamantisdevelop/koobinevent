<?php
class order {
  var $info, $totals, $products, $customer, $delivery;

  function order($order_id = '') {
    $this->info = array();
    $this->totals = array();
    $this->products = array();
    $this->customer = array();
    $this->delivery = array();

    // Segons s'hagi realitzat o no
    if (tep_not_null($order_id)) {
      $this->query($order_id);
    } else {
      $this->cart();
    }
  }

  function query($order_id) {
    global $language_id;

    $order_id = tep_db_prepare_input($order_id);

    $order_query = tep_db_query("select id, customers_id, customers_name, customers_nif, customers_company, customers_address, customers_city, customers_postcode, customers_state, customers_country, customers_tel, customers_mail, delivery_name, delivery_nif, delivery_company, delivery_address, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_iva, delivery_zona, shipping_method, shipping_tax, billing_name, billing_nif, billing_company, billing_address, billing_city, billing_postcode, billing_state, billing_country, billing_iva, billing_zona, payment_method, language_id, comments, currency, currency_value, entered, orders_status, modified from " . TABLE_ORDERS . " where id = '" . (int)$order_id . "'");
    $order = tep_db_fetch_array($order_query);

    $totals_query = tep_db_query("select id, title, text, class, value from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$order_id . "' order by listorder");
    while ($totals = tep_db_fetch_array($totals_query)) {
      $this->totals[] = array('total_id' => $totals['id'],
                              'class' => $totals['class'],
                              'title' => $totals['title'],
                              'text' => $totals['text'],
                              'value' => $totals['value']);
    }

    $order_total_query = tep_db_query("select text from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $order_id . "' and class = 'ot_total'");
    $order_total = tep_db_fetch_array($order_total_query);

    $shipping_method_query = tep_db_query("select title from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $order_id . "' and class = 'ot_shipping'");
    $shipping_method = tep_db_fetch_array($shipping_method_query);

    $order_status_query = tep_db_query("select name from " . TABLE_ORDERS_STATUS . " where id = '" . $order['orders_status'] . "' and language_id = '" . $language_id . "'");
    $order_status = tep_db_fetch_array($order_status_query);

    $this->info = array('id' => $order['id'],
    			'currency' => $order['currency'],
                        'currency_value' => $order['currency_value'],
                        'payment_method' => $order['payment_method'],
                        'language_id' => $order['language_id'],
                        'comments' => $order['comments'],
                        'entered' => $order['entered'],
                        'modified' => $order['modified'],
                        'orders_status_id' => $order['orders_status'],
                        'orders_status' => $order_status['name'],
                        'total' => strip_tags($order_total['text']),
                        'shipping_method' => $order['shipping_method'],
                        'shipping_tax' => $order['shipping_tax']);

    $this->customer = array('id' => $order['customers_id'],
                            'name' => $order['customers_name'],
                            'nif' => $order['customers_nif'],
                            'company' => $order['customers_company'],
                            'address' => $order['customers_address'],
                            'city' => $order['customers_city'],
                            'postcode' => $order['customers_postcode'],
                            'state' => $order['customers_state'],
                            'country' => $order['customers_country'],
                            'tel' => $order['customers_tel'],
                            'mail' => $order['customers_mail']);

    $this->delivery = array('name' => $order['delivery_name'],
                            'nif' => $order['delivery_nif'],
                            'company' => $order['delivery_company'],
                            'address' => $order['delivery_address'],
                            'city' => $order['delivery_city'],
                            'postcode' => $order['delivery_postcode'],
                            'state' => $order['delivery_state'],
                            'country' => $order['delivery_country'],
                            'geo_zone_id' => $order['delivery_zona'],
                            'zona_iva' => $order['delivery_iva']);

    $this->billing = array('name' => $order['billing_name'],
                           'nif' => $order['billing_nif'],
                           'company' => $order['billing_company'],
                           'address' => $order['billing_address'],
                           'city' => $order['billing_city'],
                           'postcode' => $order['billing_postcode'],
                           'state' => $order['billing_state'],
                           'country' => $order['billing_country'],
                           'geo_zone_id' => $order['billing_zona'],
                           'zona_iva' => $order['billing_iva']);

    // Productes comanda
    $index = 0;
    $orders_products_query = tep_db_query("select id, products_id, name, model, price, tax, quantity, final_price from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . tep_db_input($order_id) . "'");
    while ($orders_products = tep_db_fetch_array($orders_products_query)) {

      $this->products[$index] = array('orders_products_id' => $orders_products['id'],
                                      'products_id' => $orders_products['products_id'],
                                      'qty' => $orders_products['quantity'],
                                      'name' => $orders_products['name'],
                                      'model' => $orders_products['model'],
                                      'tax' => $orders_products['tax'],
                                      'price' => $orders_products['price'],
                                      'final_price' => $orders_products['final_price']);

      $subindex = 0;
      $attributes_query = tep_db_query("select id, options_name, values_name, price, price_prefix from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_id = '" . (int)$order_id . "' and orders_products_id = '" . (int)$orders_products['id'] . "'");
      if (tep_db_num_rows($attributes_query)) {
        while ($attributes = tep_db_fetch_array($attributes_query)) {
          $this->products[$index]['attributes'][$subindex] = array('id' => $attributes['id'],
                                                                   'options_name' => $attributes['options_name'],
                                                                   'values_name' => $attributes['values_name'],
                                                                   'price_prefix' => $attributes['price_prefix'],
                                                                   'price' => $attributes['price']);

          $subindex++;
        }
      }
      $index++;
    }
  }

  function cart() {
    global $customer_id, $sendto, $billto, $cart, $language_id, $currency, $currencies, $shipping, $payment;

    $customer_address_query = tep_db_query("select c.firstname, c.lastname, c.tel, c.mail, ab.company, ab.nif, ab.address, ab.postcode, ab.city, ab.state, ab.zone_id, z.name as zone_name, z.geo_zone_id, z.zone_iva, ab.country_id, co.id, co.name as countries_name, co.iso_code_2, co.iso_code_3 from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " ab on (c.default_address_id = ab.address_id and c.id = ab.customers_id) left join " . TABLE_ZONES . " z on (ab.zone_id = z.id) left join " . TABLE_COUNTRIES . " co on (ab.country_id = co.id) where c.id = '" . $customer_id . "'");
    $customer_address = tep_db_fetch_array($customer_address_query);

    $shipping_address_query = tep_db_query("select ab.firstname, ab.lastname, ab.nif, ab.company, ab.address, ab.postcode, ab.city, ab.state, ab.zone_id, z.name as zone_name, z.geo_zone_id, z.zone_iva, ab.country_id, c.id, c.name as countries_name, c.iso_code_2, c.iso_code_3 from " . TABLE_ADDRESS_BOOK . " ab left join " . TABLE_ZONES . " z on (ab.zone_id = z.id) left join " . TABLE_COUNTRIES . " c on (ab.country_id = c.id) where ab.customers_id = '" . $customer_id . "' and ab.address_id = '" . $sendto . "'");
    $shipping_address = tep_db_fetch_array($shipping_address_query);

    $billing_address_query = tep_db_query("select ab.firstname, ab.lastname, ab.nif, ab.company, ab.address, ab.postcode, ab.city, ab.state, ab.zone_id, z.name as zone_name, z.geo_zone_id, z.zone_iva,  ab.country_id, c.id, c.name as countries_name, c.iso_code_2, c.iso_code_3 from " . TABLE_ADDRESS_BOOK . " ab left join " . TABLE_ZONES . " z on (ab.zone_id = z.id) left join " . TABLE_COUNTRIES . " c on (ab.country_id = c.id) where ab.customers_id = '" . $customer_id . "' and ab.address_id = '" . $billto . "'");
    $billing_address = tep_db_fetch_array($billing_address_query);

    $this->info = array('currency' => $currency,
                        'currency_value' => $currencies->currencies[$currency]['value'],
                        'payment_method' => $payment,
                        'shipping_method' => $shipping['text'],
                        'shipping_cost' => $shipping['cost'],
                        'shipping_tax' => tep_get_tax_rate($shipping['tax_class']),
                        'language_id' => $languages_id,
                        'comments' => $GLOBALS['comments']);

    if (is_object($GLOBALS[$payment])) {
      $this->info['payment_method'] = $GLOBALS[$payment]->title;
    }

    $this->customer = array('firstname' => $customer_address['firstname'],
                            'lastname' => $customer_address['lastname'],
                            'company' => $customer_address['company'],
                            'nif' => $customer_address['nif'],
                            'address' => $customer_address['address'],
                            'city' => $customer_address['city'],
                            'postcode' => $customer_address['postcode'],
                            'state' => ((tep_not_null($customer_address['zone_name'])) ? $customer_address['zone_name'] : $customer_address['state']),
                            'country' => array('id' => $customer_address['country_id'], 'title' => $customer_address['countries_name'], 'iso_code_2' => $customer_address['iso_code_2'], 'iso_code_3' => $customer_address['iso_code_3']),
                            'tel' => $customer_address['tel'],
                            'mail' => $customer_address['mail']);

    $this->delivery = array('firstname' => $shipping_address['firstname'],
                            'lastname' => $shipping_address['lastname'],
                            'company' => $shipping_address['company'],
                            'nif' => $shipping_address['nif'],
                            'address' => $shipping_address['address'],
                            'city' => $shipping_address['city'],
                            'postcode' => $shipping_address['postcode'],
                            'state' => ((tep_not_null($shipping_address['zone_name'])) ? $shipping_address['zone_name'] : $shipping_address['state']),
                            'country' => array('id' => $shipping_address['country_id'], 'title' => $shipping_address['countries_name'], 'iso_code_2' => $shipping_address['iso_code_2'], 'iso_code_3' => $shipping_address['iso_code_3']),
                            'country_id' => $shipping_address['country_id'],
                            'geo_zone_id' => (tep_not_null($shipping_address['zone_name']) ? $shipping_address['geo_zone_id'] : DEFAULT_GEO_ZONE_ID),
                            'zone_iva' => $shipping_address['zone_iva']);

    $this->billing = array('firstname' => $billing_address['firstname'],
                           'lastname' => $billing_address['lastname'],
                           'company' => $billing_address['company'],
                           'nif' => $billing_address['nif'],
                           'address' => $billing_address['address'],
                           'city' => $billing_address['city'],
                           'postcode' => $billing_address['postcode'],
                           'state' => ((tep_not_null($billing_address['zone_name'])) ? $billing_address['zone_name'] : $billing_address['state']),
                           'country' => array('id' => $billing_address['country_id'], 'title' => $billing_address['countries_name'], 'iso_code_2' => $billing_address['iso_code_2'], 'iso_code_3' => $billing_address['iso_code_3']),
                           'country_id' => $billing_address['country_id'],
                           'geo_zone_id' => (tep_not_null($billing_address['zone_name']) ? $billing_address['geo_zone_id'] : DEFAULT_GEO_ZONE_ID),
                           'zone_iva' => $billing_address['zone_iva']);

    // Recorrem productes de la cistella
    $index = 0;
    $products = $cart->get_products();
    $psize = sizeof($products);
    for ($i=0; $i<$psize; $i++) {
      $this->products[$index] = array('qty' => $products[$i]['quantity'],
                                      'name' => $products[$i]['name'],
                                      'image' => $products[$i]['image'],
                                      'model' => $products[$i]['model'],
                                      'tax_class_id' => $products[$i]['tax_class_id'],
                                      'price' => $products[$i]['price'],
                                      'final_price' => $products[$i]['price'] + $cart->attributes_price($products[$i]['id']),
                                      'weight' => $products[$i]['weight'],
                                      'id' => $products[$i]['id']);

      $products_qty = $this->products[$index]['qty'];
      $products_tax  = tep_get_tax_rate($this->products[$index]['tax_class_id']);

      $shown_price = tep_substract_tax($this->products[$index]['final_price'], $products_tax) * $products_qty;
      $shown_tax = tep_calculate_tax($this->products[$index]['final_price'], $products_tax) * $products_qty;
      $this->info['subtotal'] += $shown_price;
      $this->info['tax'] += $shown_tax;

      $attrib_price = 0;
      if ($products[$i]['attributes']) {
        $subindex = 0;
        reset($products[$i]['attributes']);
        while (list($option, $value) = each($products[$i]['attributes'])) {
          $attributes_query = tep_db_query("select popt.name as options_name, poval.name as values_name, pa.price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . $products[$i]['id'] . "' and pa.options_id = '" . $option . "' and pa.options_id = popt.id and pa.values_id = '" . $value . "' and pa.values_id = poval.id and popt.language_id = '" . $language_id . "' and poval.language_id = '" . $language_id . "'");
          $attributes = tep_db_fetch_array($attributes_query);

          $this->products[$index]['attributes'][$subindex] = array('options_name' => $attributes['options_name'],
                                                                   'values_name' => $attributes['values_name'],
                                                                   'option_id' => $option,
                                                                   'value_id' => $value,
                                                                   'price_prefix' => $attributes['price_prefix'],
                                                                   'price' => $attributes['price']);

          $subindex++;


          // Preu per atributs
          if ($attributes['price_prefix'] == '-') {
            $this->info['subtotal'] -=  tep_substract_tax($attributes['price'], $products_tax) * $products_qty;
            $this->info['tax'] -=  tep_calculate_tax($attributes['price'], $products_tax) * $products_qty;
          } else {
            $this->info['subtotal'] +=  tep_substract_tax($attributes['price'], $products_tax) * $products_qty;
            $this->info['tax'] +=  tep_calculate_tax($attributes['price'], $products_tax) * $products_qty;
          }
        }
      }
      $index++;
    }

    // Nom�s sumem impostos si s'aplica iva
    if ($billing_address['zone_iva']==0 ) $this->info['tax']=0;

    $this->info['total'] = $this->info['subtotal'] + $this->info['tax'] + $this->info['shipping_cost'] ;
  }

}
?>
