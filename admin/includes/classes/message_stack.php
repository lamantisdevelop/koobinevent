<?php
class messageStack {
  var $size = 0;

  function messageStack() {
    global $messageToStack;

    $this->errors = array();

    if (isset($_SESSION['messageToStack'])) {
      $messageToStack = $_SESSION['messageToStack'];
      for ($i = 0, $n = sizeof($messageToStack); $i < $n; $i++) {
        $this->add($messageToStack[$i]['text'], $messageToStack[$i]['type']);
      }
      unset($_SESSION['messageToStack']);
    }
  }

  function add($message, $type = 'error') {
    if ($type == 'error') {
      $this->errors[] = array('params' => 'class="message_header message_error"', 'text' => $message);
    } elseif ($type == 'warning') {
      $this->errors[] = array('params' => 'class="message_header message_warning"', 'text' => $message);
    } elseif ($type == 'success') {
      $this->errors[] = array('params' => 'class="message_header message_success"', 'text' => $message);
    } else {
      $this->errors[] = array('params' => 'class="message_header message_error"', 'text' => $message);
    }

    $this->size++;
  }

  function add_session($message, $type = 'error') {
    global $messageToStack;

    if (!isset($_SESSION['messageToStack'])) {
      $_SESSION['messageToStack'] = array();
    }

    $_SESSION['messageToStack'][] = array('text' => $message, 'type' => $type);
  }

  function reset() {
    $this->errors = array();
    $this->size = 0;
  }

  function output() {
    $output = '';
    for ($i = 0, $n = sizeof($this->errors); $i < $n; $i++) {
      $output .= '<div ' . $this->errors[$i]['params'] . '>' . $this->errors[$i]['text'] .'</div>';
    }
    return $output;
  }
}
?>
