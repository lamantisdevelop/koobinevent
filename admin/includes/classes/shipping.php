<?php

class shipping {
  var $modules;

  // class constructor
  function shipping($module = '') {
    global $language, $PHP_SELF;

    if (defined('MODULE_SHIPPING_INSTALLED') && tep_not_null(MODULE_SHIPPING_INSTALLED)) {
      $this->modules = explode(';', MODULE_SHIPPING_INSTALLED);

      $include_modules = array();

      if ( (tep_not_null($module)) && (in_array($module . '.' . substr($PHP_SELF, (strrpos($PHP_SELF, '.')+1)), $this->modules)) ) {
          $include_modules[] = array('class' => $module, 'file' => $module . '.php');
      } else {
        reset($this->modules);
        while (list(, $value) = each($this->modules)) {
          $class = substr($value, 0, strrpos($value, '.'));
          $include_modules[] = array('class' => $class, 'file' => $value);
        }
      }

      for ($i=0, $n=sizeof($include_modules); $i<$n; $i++) {
        include(DIR_WS_PORTAL_TEXTS . $language . '/modules/shipping/' . $include_modules[$i]['file']);
        include(DIR_WS_MODULES . 'shipping/' . $include_modules[$i]['file']);

        $GLOBALS[$include_modules[$i]['class']] = new $include_modules[$i]['class'];
      }
    }
  }

  function quote($module = '') {
    global $total_weight, $shipping_weight;


    $quotes_array = array();
    $shipping_weight = $total_weight;

      if (is_array($this->modules)) {
        reset($this->modules);
        while (list(, $value) = each($this->modules)) {
          $class = substr($value, 0, strrpos($value, '.'));

          if (tep_not_null($module)) {
            if ( ($module == $class) && ($GLOBALS[$class]->enabled) ) {
              $quote = $GLOBALS[$class]->quote();
              if (is_array($quote)) $quotes_array[] = $quote;
            }
          } elseif ($GLOBALS[$class]->enabled) {
            $quote = $GLOBALS[$class]->quote();
            if (is_array($quote)) $quotes_array[] = $quote;
          }
        }
      }                   

      return $quotes_array;
  }

  function javascript_validation() {
    $js = '';
    if (is_array($this->modules)) {
      $js = 'function check_form() {' . "\n" .
              '  var error = 0;' . "\n" .
              '  var error_message = "' . JS_ERROR . '";' . "\n" .
              '  var shipping_value = null;' . "\n" .
              '  if (document.checkout_shipping.shipping.length) {' . "\n" .
              '    for (var i=0; i<document.checkout_shipping.shipping.length; i++) {' . "\n" .
              '      if (document.checkout_shipping.shipping[i].checked) {' . "\n" .
              '        shipping_value = document.checkout_shipping.shipping[i].value;' . "\n" .
              '      }' . "\n" .
              '    }' . "\n" .
              '  } else if (document.checkout_shipping.shipping.checked) {' . "\n" .
              '    shipping_value = document.checkout_shipping.shipping.value;' . "\n" .
              '  } else if (document.checkout_shipping.shipping.value) {' . "\n" .
              '    shipping_value = document.checkout_shipping.shipping.value;' . "\n" .
              '  }' . "\n\n";

      reset($this->modules);
      while (list(, $value) = each($this->modules)) {
        $class = substr($value, 0, strrpos($value, '.'));
        if ($GLOBALS[$class]->enabled) {
          $js .= $GLOBALS[$class]->javascript_validation();
        }
      }

      $js .= "\n" . '  if (shipping_value == null) {' . "\n" .
               '    error_message = error_message + "' . JS_ERROR_NO_SHIPPING_MODULE_SELECTED . '";' . "\n" .
               '    error = 1;' . "\n" .
               '  }' . "\n\n" .
               '  if (error == 1) {' . "\n" .
               '    alert(error_message);' . "\n" .
               '    return false;' . "\n" .
               '  } else {' . "\n" .
               '    return true;' . "\n" .
               '  }' . "\n" .
               '}' . "\n";
    }

    return $js;
  }

}
?>
