<?php
class breadcrumb {
  var $_trail;

  function breadcrumb() {
    $this->reset();
  }

  function reset() {
    $this->_trail = array();
  }

  function add($title, $link = '') {
    $this->_trail[] = array('title' => $title, 'link' => $link);
  }

  function trail($separator = ' ') {
    $trail_string = '';
    $trail_size = sizeof($this->_trail);

    for ($i=0; $i<$trail_size; $i++) {
      if (tep_not_null($this->_trail[$i]['link']) && ($i+1)<$trail_size) {
        $trail_string .= '<li><a class="color2" href="' . $this->_trail[$i]['link'] . '">' . htmlentities($this->_trail[$i]['title'], ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</a></li>';
      } else {
        $trail_string .= '<li class="current">'.htmlentities($this->_trail[$i]['title'], ENT_QUOTES | ENT_IGNORE, "UTF-8").'</li>';
      }
      if (($i+1) < $trail_size) $trail_string .= $separator;
    }
    return $trail_string;
  }
}
?>
