<?php

class order_total {
  var $modules;

  function order_total() {
    global $language;

    if (defined('MODULE_ORDER_TOTAL_INSTALLED') && tep_not_null(MODULE_ORDER_TOTAL_INSTALLED)) {
      $this->modules = explode(';', MODULE_ORDER_TOTAL_INSTALLED);

      reset($this->modules);
      while (list(, $value) = each($this->modules)) {
        include(DIR_WS_PORTAL_TEXTS . $language . '/modules/order_total/' . $value);
        include(DIR_WS_MODULES . 'order_total/' . $value);

        $class = substr($value, 0, strrpos($value, '.'));
        $GLOBALS[$class] = new $class;
      }
    }
  }

  function process() {
    $order_total_array = array();
    if (MODULE_ORDER_TOTAL_INSTALLED) {
      reset($this->modules);
      while (list(, $value) = each($this->modules)) {
        $class = substr($value, 0, strrpos($value, '.'));
        if ($GLOBALS[$class]->enabled) {
          $GLOBALS[$class]->process();

          $size = sizeof($GLOBALS[$class]->output);
          for ($i=0; $i<$size; $i++) {
            if (tep_not_null($GLOBALS[$class]->output[$i]['title']) && tep_not_null($GLOBALS[$class]->output[$i]['text'])) {
              $order_total_array[] = array('code' => $GLOBALS[$class]->code,
                                           'title' => $GLOBALS[$class]->output[$i]['title'],
                                           'text' => $GLOBALS[$class]->output[$i]['text'],
                                           'value' => $GLOBALS[$class]->output[$i]['value'],
                                           'listorder' => $GLOBALS[$class]->listorder);
            }
          }
        }
      }
    }                                           
    return $order_total_array;
  }

  function output() {
    $output_string = '';
    if (MODULE_ORDER_TOTAL_INSTALLED) {
      reset($this->modules);
      while (list(, $value) = each($this->modules)) {
        $class = substr($value, 0, strrpos($value, '.'));
        if ($GLOBALS[$class]->enabled) {
          $size = sizeof($GLOBALS[$class]->output);
          for ($i=0; $i<$size; $i++) {
            $output_string .= '<span class="item"><span class="tit_aux fixed_w">' . $GLOBALS[$class]->output[$i]['title'] . '</span>&nbsp;';
            if ($class=='ot_total'){
              $output_string .=  '<span class="price">' . $GLOBALS[$class]->output[$i]['text'] . '</span></span>';
            }else{
              $output_string .= '<span class="price_aux">' . $GLOBALS[$class]->output[$i]['text'] . '</span></span>';
            }
          }
        }
      }
    }
    return $output_string;
  }

  function output_value() {
    global $currencies, $currency;

    $output_string = '';
    if (MODULE_ORDER_TOTAL_INSTALLED) {
      reset($this->modules);
      while (list(, $value) = each($this->modules)) {
        $class = substr($value, 0, strrpos($value, '.'));
        if ($GLOBALS[$class]->enabled) {
          $size = sizeof($GLOBALS[$class]->output);
          for ($i=0; $i<$size; $i++) {
            $output_string .= '<span class="item"><span class="tit_aux fixed_w">' . $GLOBALS[$class]->output[$i]['title'] . '</span>&nbsp;';
            if ($class=='ot_total'){
              $output_string .=  '<span class="price">' . $currencies->display_price($GLOBALS[$class]->output[$i]['value'],1,$currency) . '</span></span>';
            }else{
              $output_string .= '<span class="price_aux">' . $currencies->display_price($GLOBALS[$class]->output[$i]['value'],1,$currency) . '</span></span>';
            }
          }
        }
      }
    }
    return $output_string;
  }
}
?>
