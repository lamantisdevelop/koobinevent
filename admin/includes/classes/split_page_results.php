<?php
class splitPageResults {

  function splitPageResults(&$current_page_number, $max_rows_per_page, &$sql_query, &$query_num_rows) {
    if (empty($current_page_number)) $current_page_number = 1;

    $query_num_rows = tep_db_num_rows(tep_db_query($sql_query));

    $num_pages = ceil($query_num_rows / $max_rows_per_page);
    if ($current_page_number > $num_pages) {
      $current_page_number = $num_pages;
    }
    $offset = ($max_rows_per_page * ($current_page_number - 1));
    $sql_query .= " limit " . max($offset, 0) . ", " . $max_rows_per_page;   
  }


  // Pagines
  function display_links($query_numrows, $max_rows_per_page, $max_page_links, $current_page_number = '1', $parameters = '', $page_name = 'page') {

    if ( tep_not_null($parameters) && (substr($parameters, -5) != '&amp;') ) $parameters .= '&amp;';

    // calculate number of pages needing links
    $num_pages = intval($query_numrows / $max_rows_per_page);

    // $num_pages now contains int of pages needed unless there is a remainder from division
    if ($query_numrows % $max_rows_per_page) $num_pages++; // has remainder so add one page

    $pages_array = array();
    for ($i=1; $i<=$num_pages; $i++) {
      $pages_array[] = array('id' => $i, 'text' => $i);
    }

    if ($num_pages > 1) {
      $display_links = tep_draw_form('pagines', tep_href_link('', tep_get_all_get_params(array($page_name,'x','y'))), 'get');

      if ($parameters != '') {
        if (substr($parameters, -5) == '&amp;') $parameters = substr($parameters, 0, -5);
        $pairs = explode('&amp;', $parameters);
        while (list(, $pair) = each($pairs)) {
          list($key,$value) = explode('=', $pair);
          if ( ($key != 'page') && ($key != 'action') && ($key != 'x') && ($key != 'y') && ($key != tep_session_name()) ) {
            if (tep_not_null($value)) $display_links .= tep_draw_hidden_field(rawurldecode($key), rawurldecode($value));
          }
        }
      }

      if ($current_page_number > 1) {
        $display_links .= '<a href="' . tep_href_link('',tep_get_all_get_params(array($page_name,'x','y')) . $page_name . '=' . ($current_page_number - 1)) . '" title="' . PREVIOUS . '">' . PREVNEXT_BUTTON_PREV . '</a>';
      } else {
        $display_links .= PREVNEXT_BUTTON_PREV;
      }

      $display_links .= '<label>' . TEXT_PAGE . tep_draw_pull_down_menu($page_name, $pages_array, $current_page_number, 'onchange="this.form.submit();"') . sprintf(TEXT_RESULT_PAGE, $num_pages) . '</label>';

      if (($current_page_number < $num_pages) && ($num_pages != 1)) {
        $display_links .= '<a href="' . tep_href_link('',tep_get_all_get_params(array($page_name,'x','y')) . $page_name . '=' . ($current_page_number + 1)) . '" title="' . NEXT . '">' . PREVNEXT_BUTTON_NEXT . '</a>';
      } else {
        $display_links .=  PREVNEXT_BUTTON_NEXT;
      }

      $display_links .= '</form>';
    } else {
      $display_links = '<p id="pagines">' . sprintf(TEXT_PAGE_NUM, $num_pages, $num_pages) . '</p>';
    }

    return $display_links;
  }

  // Pagines
  function display_digg_links($query_numrows, $max_rows_per_page, $max_page_links, $current_page_number = '1', $parameters = '', $page_name = 'page') {

    if ( tep_not_null($parameters) && (substr($parameters, -5) != '&amp;') ) $parameters .= '&amp;';

    // calculate number of pages needing links
	$prev = $current_page_number - 1;
	$next = $current_page_number + 1;
    $lastpage = ceil($query_numrows / $max_rows_per_page);
    $lpm1 = $lastpage - 1;
    $adjacents = 2;

	$pagination = '';
	if($lastpage > 1) {
		$pagination .= '<div class="pagination">';

		//previous button
		if ($current_page_number > 1) {
			$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $prev) . '" class="prev_next">« ' . TEXT_PREV . '</a>';
		} else {
			$pagination .= '<span class="prev_next disabled">« ' . TEXT_PREV . '</span>';
        }

		//pages
		if ($lastpage <= 5 + ($adjacents * 2))  {
			for ($counter = 1; $counter <= $lastpage; $counter++) {
				if ($counter == $current_page_number) {
					$pagination .= '<span class="current">' . $counter . '</span>';
		        } else {
					$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $counter) . '">' . $counter . '</a>';
                }
			}
		} else {
			//close to beginning; only hide later pages
			if($current_page_number < 4 + $adjacents){
                $limit = (($current_page_number <= 3) ? 5 : $current_page_number + $adjacents );
				for ($counter = 1; $counter <= $limit; $counter++) {
					if ($counter == $current_page_number) {
						$pagination .= '<span class="current">' . $counter . '</span>';
					} else {
						$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $counter) . '">' . $counter . '</a>';
                    }
				}
				$pagination .= '<span class="punts">...</span>';
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $lpm1) . '">' . $lpm1. '</a>';
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $lastpage) . '">' . $lastpage . '</a>';

			} else if ($lastpage - ($adjacents * 2) > $current_page_number && $current_page_number > ($adjacents * 2)) {
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page=1') . '">1</a>';
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page=2') . '">2</a>';
				$pagination .= '<span class="punts">...</span>';
				for ($counter = $current_page_number - $adjacents; $counter <= $current_page_number + $adjacents; $counter++) {
					if ($counter == $current_page_number) {
						$pagination .= '<span class="current">' . $counter . '</span>';
					} else {
						$pagination .= '<a href="' . tep_href_link('',tep_get_all_get_params(array($page_name,'x','y')) . $page_name . '=' . $counter) . '">' . $counter . '</a>';
                    }
				}
				$pagination .= '<span class="punts">...</span>';
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $lpm1) . '">' . $lpm1. '</a>';
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $lastpage) . '">' . $lastpage . '</a>';

			} else {
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page=1') . '">1</a>';
				$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page=2') . '">2</a>';
				$pagination .= '<span class="punts">...</span>';

                $limit = (($current_page_number > $lastpage - 3) ? $lastpage - 4 : $current_page_number - $adjacents);
				for ($counter = $limit; $counter <= $lastpage; $counter++) {
					if ($counter == $current_page_number) {
						$pagination .= '<span class="current">' . $counter . '</span>';
					} else {
						$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $counter) . '">' . $counter . '</a>';
                    }
				}
			}
		}

		//next button
		if ( $current_page_number < $lastpage ) {
			$pagination .= '<a href="' . tep_friendly_url('',tep_get_page_title($_GET['pageID']),tep_get_friendly_params() . 'page='. $next) . '" class="prev_next">' . TEXT_NEXT . ' »</a>';
		} else {
			$pagination .= '<span class="prev_next disabled">' . TEXT_NEXT . ' »</span>';
        }
		$pagination .='</div>' . "\n";
	}

    return $pagination;
  }


  // Veient
  function display_count($query_numrows, $max_rows_per_page, $current_page_number, $text_output) {
    $to_num = ($max_rows_per_page * $current_page_number);
    if ($to_num > $query_numrows) $to_num = $query_numrows;
    $from_num = ($max_rows_per_page * ($current_page_number - 1));
    if ($to_num == 0) {
      $from_num = 0;
    } else {
      $from_num++;
    }

    return sprintf($text_output, $from_num, $to_num, $query_numrows);
  }
}
?>
