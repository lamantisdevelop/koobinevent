<?php

class shoppingCart {
  var $contents, $total, $weight, $cartID, $content_type;

  function shoppingCart() {
  
    $this->reset();
  }

  function restore_contents() {
    global $customer_id;

    if (!tep_session_is_registered('customer_id')) return false;

    // insert current cart contents in database
    if (is_array($this->contents)) {
      reset($this->contents);
      while (list($products_id, ) = each($this->contents)) {
        $qty = $this->contents[$products_id]['qty'];
        $product_query = tep_db_query("select products_id from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
        if (!tep_db_num_rows($product_query)) {
          tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET . " (customers_id, products_id, quantity, entered) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id) . "', '" . $qty . "', '" . date('Ymd') . "')");
          if (isset($this->contents[$products_id]['attributes'])) {
            reset($this->contents[$products_id]['attributes']);
            while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
              tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " (customers_id, products_id, options_id, values_id) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id) . "', '" . (int)$option . "', '" . (int)$value . "')");
            }
          }
        } else {
          tep_db_query("update " . TABLE_CUSTOMERS_BASKET . " set quantity = '" . $qty . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
        }
      }
    }

    // reset per-session cart contents, but not the database contents
    $this->reset(false);

    $products_query = tep_db_query("select products_id, quantity from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");
    while ($products = tep_db_fetch_array($products_query)) {
      $this->contents[$products['products_id']] = array('qty' => $products['quantity']);
      // attributes
      $attributes_query = tep_db_query("select options_id, values_id from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products['products_id']) . "'");
      while ($attributes = tep_db_fetch_array($attributes_query)) {
        $this->contents[$products['products_id']]['attributes'][$attributes['options_id']] = $attributes['values_id'];
      }
    }

    $this->cleanup();
  }

  function reset($reset_database = false) {
    global $customer_id;

    $this->contents = array();
    $this->total = 0;
    $this->weight = 0;
    $this->content_type = false;

    if (tep_session_is_registered('customer_id') && ($reset_database == true)) {
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "'");
    }

    unset($this->cartID);
    if (tep_session_is_registered('cartID')) tep_session_unregister('cartID');
  }

  function add_cart($products_id, $qty = '1', $attributes = '', $notify = true) {
    global $new_products_id_in_cart, $customer_id;

    $products_id_string = tep_get_uprid($products_id, $attributes);
    $products_id = tep_get_prid($products_id_string);

    $attributes_pass_check = true;

    if (is_array($attributes)) {
      reset($attributes);
      while (list($option, $value) = each($attributes)) {
        if (!is_numeric($option) || !is_numeric($value)) {
          $attributes_pass_check = false;
          break;
        }
      }
    }

    if (is_numeric($products_id) && is_numeric($qty) && ($attributes_pass_check == true)) {

      $check_product_query = tep_db_query("select status from " . TABLE_PRODUCTS . " where id = '" . (int)$products_id . "'");
      $check_product = tep_db_fetch_array($check_product_query);

      if (($check_product !== false) && ($check_product['status'] == '1')) {

        if ($notify == true) {
          $new_products_id_in_cart = $products_id;
          tep_session_register('new_products_id_in_cart');
        }

        if ($this->in_cart($products_id_string)) {
          // Si existeix a la cistella, ctualitzem la quantitat (actual+1)
          $this->update_quantity($products_id_string, $qty, $attributes);

        } else {

          $this->contents[$products_id_string] = array('qty' => $qty);
          // insert into database
          if (tep_session_is_registered('customer_id')) tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET . " (customers_id, products_id, quantity, entered) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id_string) . "', '" . (int)$qty . "', '" . date('Ymd') . "')");

          if (is_array($attributes)) {
            reset($attributes);
            while (list($option, $value) = each($attributes)) {
              $this->contents[$products_id_string]['attributes'][$option] = $value;
              // insert into database
              if (tep_session_is_registered('customer_id')) tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " (customers_id, products_id, options_id, values_id) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id_string) . "', '" . (int)$option . "', '" . (int)$value . "')");
            }
          }
        }

        $this->cleanup();

        // assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
        $this->cartID = $this->generate_cart_id();    
      }
    }
  }

  function update_quantity($products_id, $quantity = '', $attributes = '') {
    global $customer_id;

    $products_id_string = tep_get_uprid($products_id, $attributes);
    $products_id = tep_get_prid($products_id_string);

    $attributes_pass_check = true;

    if (is_array($attributes)) {
      reset($attributes);
      while (list($option, $value) = each($attributes)) {
        if (!is_numeric($option) || !is_numeric($value)) {
          $attributes_pass_check = false;
          break;
        }
      }
    }

    if (is_numeric($products_id) && isset($this->contents[$products_id_string]) && is_numeric($quantity) && ($attributes_pass_check == true)) {
      $this->contents[$products_id_string] = array('qty' => $quantity);
      // update database
      if (tep_session_is_registered('customer_id')) tep_db_query("update " . TABLE_CUSTOMERS_BASKET . " set quantity = '" . (int)$quantity . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id_string) . "'");

      if (is_array($attributes)) {
        reset($attributes);
        while (list($option, $value) = each($attributes)) {
          $this->contents[$products_id_string]['attributes'][$option] = $value;
          // update database
          if (tep_session_is_registered('customer_id')) tep_db_query("update " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " set values_id = '" . (int)$value . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id_string) . "' and options_id = '" . (int)$option . "'");
        }
      }
    }
  }

  function cleanup() {
    global $customer_id;

    reset($this->contents);
    while (list($key,) = each($this->contents)) {
      if ($this->contents[$key]['qty'] < 1) {
        unset($this->contents[$key]);
        // remove from database
        if (tep_session_is_registered('customer_id')) {
          tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($key) . "'");
          tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($key) . "'");
        }
      }
    }
  }

  function count_contents() {  // !!!
    $total_items = 0;
    if (is_array($this->contents)) {
      reset($this->contents);
      while (list($products_id, ) = each($this->contents)) {
        $total_items += $this->get_quantity($products_id);
      }
    }
    return $total_items;
  }

  function get_quantity($products_id) {
    if (isset($this->contents[$products_id])) {
      return $this->contents[$products_id]['qty'];
    } else {
      return 0;
    }
  }

  function in_cart($products_id) {
    if (isset($this->contents[$products_id])) {
      return true;
    } else {
      return false;
    }
  }

  function remove($products_id) {
    global $customer_id;

    unset($this->contents[$products_id]);
    // remove from database
    if (tep_session_is_registered('customer_id')) {
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
    }

    // assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
    $this->cartID = $this->generate_cart_id();
  }

  function remove_all() {
    $this->reset();
  }

  function get_product_id_list() {
    $product_id_list = '';
    if (is_array($this->contents)) {
      reset($this->contents);
      while (list($products_id, ) = each($this->contents)) {
        $product_id_list .= ', ' . $products_id;
      }
    }

    return substr($product_id_list, 2);
  }

  function calculate() {
    global $price_iva;

    $this->subtotal = 0;
    $this->tax = 0;
    $this->total = 0;
    $this->weight = 0;

    if (!is_array($this->contents)) return 0;

    reset($this->contents);
    while (list($products_id, ) = each($this->contents)) {
      $qty = $this->contents[$products_id]['qty'];

      $product_query = tep_db_query("select p.id, p.price, p.tax_class_id, p.weight, s.new_price from " . TABLE_PRODUCTS . " p left join " . TABLE_SPECIALS . " s on p.id = s.products_id where p.id = '" . (int)$products_id . "'");
      if ($product = tep_db_fetch_array($product_query)) {
        $prid = $product['id'];
        $products_tax = tep_get_tax_rate($product['tax_class_id']);
        $products_price = $product['price'];
        $products_weight = $product['weight'];

        if ($product['new_price']) $products_price = $product['new_price']; // Preu oferta !!!

        $this->subtotal +=  tep_substract_tax($products_price, $products_tax) * $qty;
        $this->tax +=  tep_calculate_tax($products_price, $products_tax) * $qty;
        $this->weight += ($qty * $products_weight);
      }

      // attributes price & weight
      if (isset($this->contents[$products_id]['attributes'])) {
        reset($this->contents[$products_id]['attributes']);
        while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
          $attribute_values_query = tep_db_query("select price, price_prefix, weight, weight_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$prid . "' and options_id = '" . (int)$option . "' and values_id = '" . (int)$value . "'");
          $attribute_values = tep_db_fetch_array($attribute_values_query);
          // Preu per atributs
          if ($attribute_values['price_prefix'] == '-') {
            $this->subtotal -=  tep_substract_tax($attribute_values['price'], $products_tax) * $qty;
            $this->tax -=  tep_calculate_tax($attribute_values['price'], $products_tax) * $qty;
          } else {
            $this->subtotal +=  tep_substract_tax($attribute_values['price'], $products_tax) * $qty;
            $this->tax +=  tep_calculate_tax($attribute_values['price'], $products_tax) * $qty;
          }

          // Pes per atributs
          if ($attribute_values['weight_prefix'] == '-') {
            $this->weight -= $qty * $attribute_values['weight'];
          } else {
            $this->weight += $qty * $attribute_values['weight'];
          }
        }
      }
    }
    // Si no hi ha iva inclós impostos = 0
    if (!$price_iva) $this->tax = 0;
    $this->total = $this->subtotal + $this->tax;
  }

  function attributes_price($products_id) {
    $attributes_price = 0;

    if (isset($this->contents[$products_id]['attributes'])) {
      reset($this->contents[$products_id]['attributes']);
      while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
        $attribute_price_query = tep_db_query("select price, price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$products_id . "' and options_id = '" . (int)$option . "' and values_id = '" . (int)$value . "'");
        $attribute_price = tep_db_fetch_array($attribute_price_query);
        if ($attribute_price['options_values_price_prefix'] == '-') {
          $attributes_price -= $attribute_price['options_values_price'];
        } else {
          $attributes_price += $attribute_price['options_values_price'];
        }
      }
    }

    return $attributes_price;
  }

  function get_products() {
    global $language_id;

    if (!is_array($this->contents)) return false;

    $products_array = array();
    reset($this->contents);
    while (list($products_id, ) = each($this->contents)) {
      $products_query = tep_db_query("select p.id, pd.name, p.model, p.image, p.price, p.weight, p.tax_class_id, s.new_price from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.id = pd.products_id and pd.language_id = '" . (int)$language_id . "' left join " . TABLE_SPECIALS . " s on p.id = s.products_id where p.id = '" . (int)$products_id . "'");
      if ($products = tep_db_fetch_array($products_query)) {
        $prid = $products['id'];
        $products_price = $products['price'];
        // Canviem preu si està d'oferta
        if ($products['new_price']) $products_price = $products['new_price'];

        $products_array[] = array('id' => $products_id,
                                  'name' => $products['name'],
                                  'model' => $products['model'],
                                  'image' => $products['image'],
                                  'price' => $products_price,
                                  'quantity' => $this->contents[$products_id]['qty'],
                                  'weight' => $products['weight'],
                                  'final_price' => ($products_price + $this->attributes_price($products_id)),
                                  'tax_class_id' => $products['tax_class_id'],
                                  'attributes' => (isset($this->contents[$products_id]['attributes']) ? $this->contents[$products_id]['attributes'] : ''));
      }
    }

    return $products_array;
  }

  function show_subtotal() {
    $this->calculate();

    return $this->subtotal;
  }

  function show_tax() {
    $this->calculate();

    return $this->tax;
  }

  function show_total() {
    $this->calculate();

    return $this->total;
  }

  function show_weight() {
    $this->calculate();

    return $this->weight;
  }

  function generate_cart_id($length = 5) {
    return tep_create_random_value($length, 'digits');
  }

  // function get_content_type() { }

  function unserialize($broken) {
    for(reset($broken);$kv=each($broken);) {
      $key=$kv['key'];
      if (gettype($this->$key)!="user function") $this->$key=$kv['value'];
    }
  }
}
?>
