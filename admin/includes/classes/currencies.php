<?php
class currencies {
  var $currencies;

  function currencies() {
    $this->currencies = array();
    $currencies_query = tep_db_query("select code, name, symbol_left, symbol_right, decimal_point, thousands_point, decimal_places, value from " . TABLE_CURRENCIES . " where active='1'");
    while ($currencies = tep_db_fetch_array($currencies_query)) {
	$this->currencies[$currencies['code']] = array('name' => $currencies['name'],
                                                 'symbol_left' => $currencies['symbol_left'],
                                                 'symbol_right' => $currencies['symbol_right'],
                                                 'decimal_point' => $currencies['decimal_point'],
                                               	 'thousands_point' => $currencies['thousands_point'],
                                                 'decimal_places' => $currencies['decimal_places'],
                                                 'value' => $currencies['value']);
    }
  }

  function format($number, $calculate_currency_value = true, $currency_type = DEFAULT_CURRENCY, $currency_value = '') {          
    if ($calculate_currency_value) {
      $rate = ($currency_value) ? $currency_value : $this->currencies[$currency_type]['value'];
      $format_string = $this->currencies[$currency_type]['symbol_left'] . number_format($number * $rate, $this->currencies[$currency_type]['decimal_places'], $this->currencies[$currency_type]['decimal_point'], $this->currencies[$currency_type]['thousands_point']) . $this->currencies[$currency_type]['symbol_right'];
    } else {
      $format_string = $this->currencies[$currency_type]['symbol_left'] . number_format($number, $this->currencies[$currency_type]['decimal_places'], $this->currencies[$currency_type]['decimal_point'], $this->currencies[$currency_type]['thousands_point']) . $this->currencies[$currency_type]['symbol_right'];
    }

    return $format_string;
  }

  function get_value($code) {
    return $this->currencies[$code]['value'];
  }

  function get_decimal_places($code) {
    return $this->currencies[$code]['decimal_places'];
  }

  function display_price($products_price, $quantity = 1, $currency_type = DEFAULT_CURRENCY, $currency_value = '') {
    return $this->format($products_price * $quantity,1,$currency_type,$currency_value);
  }

  function display_number($number, $currency_type = '') {
    global $currency;

    if ($currency_type == '') $currency_type = $currency;

    $rate = $this->currencies[$currency_type]['value'];

    return $number * $rate;
  }
}
?>
