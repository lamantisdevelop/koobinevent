<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */


$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");

//per defecte NO es dispositiu mobil
$dispositiu_mobil = FALSE;

//mirem si estem navegant en un dispositiu mobil
if ($iphone || $android || $palmpre || $ipod || $berry || $ipad == true) {
    $dispositiu_mobil = TRUE;
    define(DEFAULT_PAGE, 28);//entrem per defecte al cataleg (programacio)

}
?>