<?php
require('includes/application_top.php');

   ob_start();

   phpinfo();
   $php_info .= ob_get_contents();
   ob_end_clean();

   // configure command:

   $php_info = str_replace("' '", " \<br>", $php_info);
   $php_info = str_replace("'", "", $php_info);

   // include_path:

   $php_info = str_replace(".:/", "/", $php_info);

   // paths:

   $php_info = str_replace(":/", "<br>/", $php_info);
   $php_info = str_replace("<br>//", "://", $php_info); // correct urls

   // LS_COLORS and some paths:

   $php_info = str_replace(";", "; ", $php_info);
   $php_info = str_replace(",", ", ", $php_info);

   // apache address :

   $php_info = str_replace("&lt; ", "<", $php_info);
   $php_info = str_replace("&gt;", ">", $php_info);

   echo $php_info;
?>
