<?php
// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
  switch ($_GET['action']) {
    case 'insert':
      tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
      if ($_POST['text']) {
        // Busquem nou ordre
        $new_id = tep_search_new_id(TABLE_DISPONIBILITY);
        $order_now = tep_search_new_order(TABLE_DISPONIBILITY);

        // Entrades segons idioma
	    $text_array = $_POST['text'];
	    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $lang_aux = $languages[$i]['id'];
          // Insertem dades
          $sql_data_array = array('id' => tep_db_prepare_input($new_id),
             				'language_id' => (int)$lang_aux,
                            'listorder' => (int)$order_now,
        					'text' => tep_db_prepare_input($text_array[$lang_aux]));
          // Crida base dades
          tep_db_perform(TABLE_DISPONIBILITY, $sql_data_array);
        }
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y')) . 'dID=' . $new_id));
      break;

    case 'update':
      tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
      if ($_GET['dID'] && $_POST['text']) {
        // Identificador
        $aux_id = tep_db_prepare_input($_GET['dID']);

        // Entrades segons idioma
	    $text_array = $_POST['text'];
	    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $lang_aux = $languages[$i]['id'];
          // Insertem dades
          $sql_data_array = array('text' => tep_db_prepare_input($text_array[$lang_aux]));
          // Crida base dades
          tep_db_perform(TABLE_DISPONIBILITY, $sql_data_array, 'update', 'id = \'' . (int)$aux_id . '\' and language_id=\'' . $lang_aux . '\'' );
        }
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y'))));
      break;

    case 'delete':
      tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
      if ($_GET['dID']) {
        $aux_id = tep_db_prepare_input($_GET['dID']);
        // Busquem i modifiquem ordre
        $order_now = tep_search_item_order($aux_id,TABLE_DISPONIBILITY);
        tep_reorder_items($order_now,TABLE_DISPONIBILITY);
        // Eliminem entrada
        tep_db_query("delete from " . TABLE_DISPONIBILITY . " where id = '" . (int)$aux_id . "'");
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','dID','x','y'))));
      break;

    case 'listorder':
      tep_usuaris_filtrar_acces(PERMIS_LECTURA);
      if ($_GET['dID'] && isset($_GET['value'])) {
        // Identificador
        $aux_id = tep_db_prepare_input($_GET['dID']);
        // Trobar posicio actual
        $order_now = tep_search_item_order($aux_id,TABLE_DISPONIBILITY);
        $pos_aux = (($_GET['value']=='up') ? $order_now + 1 : $order_now -1 );
        // Busquem id pos_aux per intercanviar posicions
        tep_change_order($aux_id,$order_now,$pos_aux,TABLE_DISPONIBILITY);
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','value','x','y'))));
      break;
  }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
