<!-- Inicic Lateral -->

<ul id="VerColMenu">

    <!-- Cataleg de productes -->
    <?php
    if (tep_usuaris_mirar_permis_modul('cataleg_pare_menu_lateral', PERMIS_LECTURA)) {
        ?>
        <li class="full_border <?php echo (($aux_header[0] == 'cataleg' && $aux_header[1] != 'inici') ? 'actual' : '' ) ?>"><a class="<?php echo (($aux_header[0] == 'cataleg') ? 'expanded' : 'collapsed' ) ?>"><span class="cataleg">Catàleg</span></a>
            <ul>
                <?php if (tep_usuaris_mirar_permis_modul('cataleg_productes', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=cataleg_productes&edicio=<?php echo EDICIO_ACTUAL; ?>" class="<?php echo (($modul == 'cataleg_productes' && $_GET['edicio'] == EDICIO_ACTUAL) ? 'current' : '' ) ?>"><span>Espectacles <?php echo $edicio_actual['name']; ?></span></a></li>
                <?php endif; ?>
                <?php if (tep_usuaris_mirar_permis_modul('cataleg_productes', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=cataleg_productes&edicio=0" class="<?php echo (($modul == 'cataleg_productes' && $_GET['edicio'] == 0) ? 'current' : '' ) ?>"><span>TOTS els Espectacles</span></a></li>
                <?php endif; ?>
                <?php if (tep_usuaris_mirar_permis_modul('cataleg_estoc', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=cataleg_estoc" class="<?php echo (($modul == 'cataleg_estoc') ? 'current' : '' ) ?>"><span>Gestión de estoc</span></a></li>
                <?php endif; ?>

                <?php if (tep_usuaris_mirar_permis_modul('cataleg_fabricants', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=cataleg_fabricants" class="<?php echo (($modul == 'cataleg_fabricants') ? 'current' : '' ) ?>"><span>Fabricants</span></a></li>
                <?php endif; ?>

                <?php if (tep_usuaris_mirar_permis_modul('cataleg_opcions', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=cataleg_opcions" class="<?php echo (($modul == 'cataleg_opcions' || $modul == 'cataleg_opcions_values') ? 'current' : '' ) ?>"><span>Opcions</span></a></li>
                <?php endif; ?>
            </ul>

        </li>
        <?php
    }
    ?>
    <!-- End cataleg -->
    <!-- pàgina inici -->
    <?php if (tep_usuaris_mirar_permis_modul('cataleg_inici', PERMIS_LECTURA)): ?>
        <li class="full_border"><a href="./?modul=cataleg_inici" class="<?php echo (($modul == 'cataleg_inici') ? 'current' : '' ) ?>"><span class="home">Pàgina inici</span></a></li>
    <?php endif; ?>
    <!-- End pàgina inici --> 
    <!--Notícies-->
    <?php
    if (tep_usuaris_mirar_permis_modul('noticies_noticies', PERMIS_LECTURA)) {
        ?>
        <li class="full_border"><a href="./?modul=noticies_noticies" class="<?php echo(($modul == 'noticies_noticies') ? 'current' : '' ) ?>"><span class="noticies">Notícies</a></span></li>
        <?php
    }
    ?>
    <!--end noticies-->
    <?php
    if (tep_usuaris_mirar_permis_modul('fotos_fotos', PERMIS_LECTURA)) {
        ?>
        <li class="full_border"><a href="./?modul=fotos_fotos&slideshow=1&group_id=<?php echo FOTOS_GROUP_SLIDESHOW; ?>" class="<?php echo(($_GET['slideshow']) ? 'current' : '' ) ?>"><span class="slideshows">Slideshows</span></a></li>
        <?php
    }
    if (tep_usuaris_mirar_permis_modul('fotos_fotos', PERMIS_LECTURA)) {
        ?>
        <li class="full_border <?php echo(($aux_header[0] == 'fotos' && !$_GET['slideshow']) ? 'actual' : '' ) ?>"><a class="<?php echo(($aux_header[0] == 'fotos' && !$_GET['slideshow']) ? 'expanded' : 'collapsed' ) ?>"><span class="fotos">Fotos</span></a>
            <ul>
                <?php
                if (tep_usuaris_mirar_permis_modul('fotos_fotos', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=fotos_fotos" class="<?php echo(($modul == 'fotos_fotos' && !$_GET['slideshow']) ? 'current' : '' ) ?>"><span>Galeries</span></a></li>
                    <?php
                }
                if (tep_usuaris_mirar_permis_modul('fotos_ratios', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=fotos_ratios" class="<?php echo(($modul == 'fotos_ratios') ? 'current' : '' ) ?>"><span>Ratios</span></a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
    if (tep_usuaris_mirar_permis_modul('banners_banners', PERMIS_LECTURA)) {
        ?>
        <li class="full_border"><a href="./?modul=banners_banners" class="<?php echo(($aux_header[0] == 'banners') ? 'current' : '' ) ?>"><span class="banners">Banners</span></a></li>
        <?php
    }

    //Inici MODULS PÀGINES I MENÚS
    if (tep_usuaris_mirar_permis_modul('pages_pages', PERMIS_LECTURA)) {
        ?>
        <li><a href="./?modul=pages_pages" class="<?php echo(($aux_header[0] == 'pages') ? 'current' : '' ) ?>"><span class="pages">Pàgines</span></a></li>
        <?php
    }
    if (tep_usuaris_mirar_permis_modul('menus_menus', PERMIS_LECTURA)) {
        ?>
        <li><a href="./?modul=menus_menus" class="<?php echo(($aux_header[0] == 'menus') ? 'current' : '' ) ?>"><span class="menus">Menús</span></a></li>
        <?php
    }
    //FI MODULS PAGINES I MENUS
    ?>
    <!-- End PREMSA-->
    <?php
    //Llistat Clients
    if (tep_usuaris_mirar_permis_modul('llistats_llistats', PERMIS_LECTURA)) {
        ?>
        <li class="full_border"><a href="./?modul=llistats_llistats&gID=1&llistat=1" class="<?php echo(($_GET['gID'] == '1' || $_GET['llistat'] == '1') ? 'current' : '' ) ?>"><span class="cartells">Clients</span></a></li>
        <?php
    }

    //Llistat premsa
    if (tep_usuaris_mirar_permis_modul('llistats_llistats', PERMIS_LECTURA)) {
        ?>
        <li class="full_border"><a href="./?modul=llistats_llistats&gID=5&llistat=5" class="<?php echo(($_GET['gID'] == '5' || $_GET['llistat'] == '5') ? 'current' : '' ) ?>"><span class="cartells">Premsa</span></a></li>
        <?php
    }

    //Llistat treball
    if (tep_usuaris_mirar_permis_modul('llistats_llistats', PERMIS_LECTURA)) {
        ?>
        <li class="full_border"><a href="./?modul=llistats_llistats&gID=6&llistat=6" class="<?php echo(($_GET['gID'] == '6' || $_GET['llistat'] == '6') ? 'current' : '' ) ?>"><span class="cartells">Treball</span></a></li>
        <?php
    }
    //END llistat productes
    
    
    //CARTELLS
    /* if (tep_usuaris_mirar_permis_modul('llistats_llistats', PERMIS_LECTURA)) {
      ?>
      <li class="full_border"><a href="./?modul=llistats_llistats&group_id=2" class="<?php echo(($_GET['group_id'] == '2') ? 'current' : '' ) ?>"><span class="icon_llistat_4">Ofertes especials</span></a></li>
      <?php
      }
      //END CARTELLS
      //MUSICS
      if (tep_usuaris_mirar_permis_modul('llistats_llistats', PERMIS_LECTURA)) {
      ?>
      <li class="full_border"><a href="./?modul=llistats_llistats&group_id=3" class="<?php echo(($_GET['group_id'] == '3') ? 'current' : '' ) ?>"><span class="cartells">Tallers</span></a></li>
      <?php
      } */
    //END MUSICS
    ?>


    <!-- Usuarios -->
    <?php
    if (tep_usuaris_mirar_permis_modul('usuarios_pare_menu_lateral', PERMIS_LECTURA)) {
        ?>
        <li class="<?php echo (($aux_header[0] == 'usuaris') ? 'actual' : '' ) ?>"><a class="<?php echo (($aux_header[0] == 'usuaris') ? 'expanded' : 'collapsed' ) ?>"><span class="pages">Usuarios</span></a>
            <ul>
                <?php if (tep_usuaris_mirar_permis_modul('usuaris_usuaris', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=usuaris_usuaris" class="<?php echo (($modul == 'usuaris_usuaris') ? 'current' : '' ) ?>">Usuarios</a></li>
                <?php endif; ?>

                <?php if (tep_usuaris_mirar_permis_modul('usuaris_contacte', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=usuaris_contacte" class="<?php echo (($modul == 'usuaris_contacte') ? 'current' : '' ) ?>">Contactar</a></li>
                <?php endif; ?>
            </ul></li>

        <?php
    }
    ?>
    <!--End usuarios-->

    <!-- Informes -->
    <?php
    if (tep_usuaris_mirar_permis_modul('informes_pare_menu_lateral', PERMIS_LECTURA)) {
        ?>
        <li class="<?php echo (($aux_header[0] == 'informes') ? 'actual' : '') ?> "><a class="<?php echo(($aux_header[0] == 'informes') ? 'expanded' : 'collapsed') ?>"><span class="pages">Informes</span></a>
            <ul>
                <?php if (tep_usuaris_mirar_permis_modul('informes_clients', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=informes_clients" class="<?php echo (($modul == 'informes_clients') ? 'current' : '' ) ?>">Informes Usuarios</a></li>
                <?php endif; ?>

                <?php if (tep_usuaris_mirar_permis_modul('informes_productes', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=informes_productes" class="<?php echo (($modul == 'informes_productes') ? 'current' : '' ) ?>">Informes Productos</a></li>
                <?php endif; ?>

                <?php if (tep_usuaris_mirar_permis_modul('informes_estoc', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=informes_estoc" class="<?php echo (($modul == 'informes_estoc') ? 'current' : '' ) ?>">Informes Estoc</a></li>
                <?php endif; ?>

                <?php if (tep_usuaris_mirar_permis_modul('informes_vendes', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=informes_vendes" class="<?php echo (($modul == 'informes_vendes') ? 'current' : '' ) ?>">Informes Ventas</a></li>
                <?php endif; ?>

                <?php if (tep_usuaris_mirar_permis_modul('informes_search', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=informes_search" class="<?php echo (($modul == 'informes_search') ? 'current' : '' ) ?>">Informes Búsquedas</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <?php
    }
    ?>
    <!-- End informes-->
    <!--End botiga -->

    <!--Usuaris-->
    <?php
    if (tep_usuaris_mirar_permis_modul('users_pare_menu_lateral', PERMIS_LECTURA)) {
        ?>
        <li class="full_border <?php echo(($aux_header[0] == 'users') ? 'actual' : '' ) ?>"><a class="<?php echo(($aux_header[0] == 'users') ? 'expanded' : 'collapsed' ) ?>"><span class="users">Gestió d'Usuaris</span></a>
            <ul>
                <?php
                if (tep_usuaris_mirar_permis_modul('users_users', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=users_users" class="<?php echo(($modul == 'users_users') ? 'current' : '' ) ?>"><span>Usuaris</span></a></li>
                    <?php
                }
                if (tep_usuaris_mirar_permis_modul('users_rols', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=users_rols" class="<?php echo(($modul == 'users_rols') ? 'current' : '' ) ?>"><span>Rols</span></a></li>
                    <?php
                }
                if (tep_usuaris_mirar_permis_modul('users_tipus_permisos', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=users_tipus_permisos" class="<?php echo(($modul == 'users_tipus_permisos') ? 'current' : '' ) ?>"><span>Tipus de permisos</span></a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
    ?>
    <!-- End Usuaris-->

    <?php
    if (tep_usuaris_mirar_permis_modul('tools_pare_menu_lateral', PERMIS_LECTURA)) {
        ?>
        <li class="<?php echo(($aux_header[0] == 'tools') ? 'actual' : '' ) ?>"><a class="<?php echo(($aux_header[0] == 'tools') ? 'expanded' : 'collapsed' ) ?>"><span class="tools">Eines</span></a>
            <ul>
                <?php
                if (tep_usuaris_mirar_permis_modul('tools_online', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=tools_online" class="<?php echo(($modul == 'tools_online') ? 'current' : '' ) ?>"><span>Usuaris Online</span></a></li>
                    <?php
                }
                if (tep_usuaris_mirar_permis_modul('tools_backup_db', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=tools_backup_db" class="<?php echo(($modul == 'tools_backup_db') ? 'current' : '' ) ?>"><span>Còpies de Seguretat</span></a></li>
                    <?php
                }
                /* if (tep_usuaris_mirar_permis_modul('tools_web_texts', PERMIS_LECTURA)) {
                  ?>
                  <li><a href="./?modul=tools_web_texts" class="<?php echo(($modul == 'tools_web_texts') ? 'current' : '' ) ?>">Definir Texts Web</a></li>
                  <?php
                  } */
                if (tep_usuaris_mirar_permis_modul('tools_files', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=tools_files" class="<?php echo(($modul == 'tools_files') ? 'current' : '' ) ?>"><span>Administrador d\'Arxius</span></a></li>            
                    <?php
                }
                if (tep_not_null(WEB_STATS)) {

                    if (tep_usuaris_mirar_permis_modul('estadistiques', PERMIS_LECTURA)) {
                        ?>
                        <li><a href="<?php WEB_STATS ?>" target="_blank"><span>Estadístiques</span></a></li> 
                        <?php
                    }
                }

//Botiga
                ?>
                <?php /* if (tep_usuaris_mirar_permis_modul('tools_tpv', PERMIS_LECTURA)): ?>
                  <li><a href="./?modul=tools_tpv" class="<?php echo (($modul == 'tools_tpv') ? 'current' : '' ) ?>">Acceso TPV</a></li>
                  <?php endif; */ ?>

                <?php if (tep_usuaris_mirar_permis_modul('tools_alt_search', PERMIS_LECTURA)): ?>
                    <li><a href="./?modul=tools_alt_search" class="<?php echo (($modul == 'tools_alt_search') ? 'current' : '' ) ?>'"><span>Búsqueda Alternativa</span></a></li>
                <?php endif; ?>

                <?php
                if (tep_usuaris_mirar_permis_modul('server_info', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="server_info.php" target="_blank"><span>Informació Servidor</span></a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
    if (tep_usuaris_mirar_permis_modul('config_pare_menu_lateral', PERMIS_LECTURA)) {
        ?>
        <li class="full_border <?php echo(($aux_header[0] == 'config') ? 'actual' : '' ) ?>"><a class="<?php echo(($aux_header[0] == 'config') ? 'expanded' : 'collapsed' ) ?>"><span class="config">Configuració</span></a>
            <ul>
                <?php
                if (tep_usuaris_mirar_permis_modul('config_groups', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=config_groups"  class="<?php (($modul == 'config_groups') ? 'current' : '' ) ?>"><span>Valors Configuració</span></a></li>
                    <?php
                }
                if (tep_usuaris_mirar_permis_modul('config_languages', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=config_languages"  class="<?php (($modul == 'config_languages') ? 'current' : '' ) ?>"><span>Idiomes</span></a></li>
                    <?php
                }
                if (tep_usuaris_mirar_permis_modul('config_edicions', PERMIS_LECTURA)) {
                    ?>
                    <li><a href="./?modul=config_edicions"  class="<?php (($modul == 'config_edicions') ? 'current' : '' ) ?>"><span>Edicions</span></a></li>
                    <?php
                }
                ?>

                <!-- Botiga -->
                <?php /* if (tep_usuaris_mirar_permis_modul('config_currencies', PERMIS_LECTURA)): ?>
                  <li><a href="./?modul=config_currencies"  class="<?php echo (($modul == 'config_currencies') ? 'current' : '' ) ?>">Monedas</a></li>
                  <?php endif; ?>

                  <?php if (tep_usuaris_mirar_permis_modul('config_countries', PERMIS_LECTURA)): ?>
                  <li><a href="./?modul=config_countries"  class="<?php echo (($modul == 'config_countries') ? 'current' : '' ) ?>">Países</a></li>
                  <?php endif; ?>

                  <?php if (tep_usuaris_mirar_permis_modul('config_zones', PERMIS_LECTURA)): ?>
                  <li><a href="./?modul=config_zones"  class="<?php echo (($modul == 'config_zones') ? 'current' : '' ) ?>">Zonas</a></li>
                  <?php endif; ?>

                  <?php if (tep_usuaris_mirar_permis_modul('config_disponibility', PERMIS_LECTURA)): ?>
                  <li><a href="./?modul=config_disponibility"  class="<?php echo (($modul == 'config_disponibility') ? 'current' : '' ) ?>">Disponibilidad</a></li>
                  <?php endif; */ ?>

                <!--End botiga-->

            </ul>
        </li>
        <?php
    }
    ?>
</ul>
<!-- Fi Lateral -->
