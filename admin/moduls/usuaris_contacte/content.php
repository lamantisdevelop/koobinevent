<h2 class="pages"><?php echo TITLE_PAGE; ?></h2>
<?php echo TEXT_FORM; ?>

<?php
// Auxiliar
if (isset($_GET['uID'])) {
    $mail_query = tep_db_query("select c.mail, c.firstname, c.lastname from " . TABLE_CUSTOMERS . " c where c.id = '" . (int) $_GET['uID'] . "' limit 1");
    $mail = tep_db_fetch_array($mail_query);
    $aux_mail = $mail['mail'];
}

$customers = array();
$customers[] = array('id' => '', 'text' => TEXT_SELECT);
$customers[] = array('id' => '***', 'text' => TEXT_ALL_CUSTOMERS);
$mail_query = tep_db_query("select c.mail, c.firstname, c.lastname from " . TABLE_CUSTOMERS . " c order by lastname");
while ($customers_values = tep_db_fetch_array($mail_query)) {
    $customers[] = array('id' => $customers_values['mail'],
        'text' => ucwords(mb_strtolower($customers_values['lastname'], 'UTF-8')) . ', ' . ucwords(mb_strtolower($customers_values['firstname'], 'UTF-8')) . ' (' . mb_strtolower($customers_values['mail'], 'UTF-8') . ')');
}
?>

<div id="form">
    <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=send'), 'post', 'enctype="multipart/form-data"'); ?>
    <fieldset>
        <label>
            <span>Usuario</span>
            <?php echo tep_draw_pull_down_menu('mail', $customers, (isset($aux_mail) ? $aux_mail : ''), ''); ?>
        </label>
        <label>
            <span>Remite</span>
            <input type="text" name="from" value="<?php echo EMAIL_FROM; ?>" />
        </label>
        <label>
            <span>Asunto</span>
            <input type="text" name="subject" value="" />
        </label>
        <label class="width_50">
            <span>Mensaje</span>
        </label> 
        <textarea name="text" id="text">

        </textarea>
        

        <script>
            <?php 
            if (!tep_usuaris_es_admin())
            { 
                echo("CKEDITOR.config.toolbar = 'Editor';");                     
            }
            ?>
            CKEDITOR.replace( 'text');
        </script>
        
    </fieldset>
    <p class="botonera">
        <?php
        echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="javascript:history.back(-1)"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>
</form>
</div>
