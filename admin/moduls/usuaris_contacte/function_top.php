<?php
// Enviament Mails
require_once(DIR_WS_CLASSES . 'class.phpmailer.php');
require_once(DIR_WS_CLASSES . 'class.smtp.php');

// Accions
if ($_GET['action']) {
  switch ($_GET['action']) {
    case 'send':
      tep_usuaris_filtrar_acces(PERMIS_LECTURA);
      // Enviem mails
      switch ($_POST['mail']) {
        case '***':
      	  $mail_query = tep_db_query("select firstname, lastname, mail from " . TABLE_CUSTOMERS);
      	  $mail_sent_to = TEXT_ALL_CUSTOMERS;
      	break;

    	default:
          $mail_query = tep_db_query("select firstname, lastname, mail from " . TABLE_CUSTOMERS . " where mail = '" . tep_db_input($_POST['mail']) . "'");
      	  $mail_sent_to = $_POST['mail'];
        break;
      }
      $mail_sent_to = '';

      // Construim missatge
      $from = tep_db_prepare_input($_POST['from']);
      $subject = tep_db_prepare_input($_POST['subject']);
      $message = tep_db_prepare_input($_POST['message']);

      // Enviem els mails a les diferents direccions seleccionades
      $error = false;
      while ($mail_array = tep_db_fetch_array($mail_query)) {

        $mail = new PHPMailer();
	$mail->CharSet = "UTF-8";
	$mail->IsHTML(true);

	$mail->Host = MAIL_SERVER; // SMTP server
	$mail->From = EMAIL_FROM; // Mail
	$mail->FromName = EMAIL_FROM_NAME; // Nom
	$mail->AddAddress($mail_array['mail'], $mail_array['firstname'] . ' ' . $mail_array['lastname']); // A qui l'enviem

	$mail->Subject = $subject; // Titol
	$mail->Body = $message;
	$mail->WordWrap = 100;

	if (!$mail->Send()) {
          $error = true;
	  $messageStack->add_session(sprintf(ERROR_EMAIL_SENT_TO, $mail_array['mail']), 'error');
	} else {
          $mail_sent_to = (tep_not_null($mail_sent_to)?', ':'') . $mail_array['mail'];
        }
      }
      if ($error==false) {
        $messageStack->add_session(sprintf(NOTICE_EMAIL_SENT_TO, $mail_sent_to), 'success');
      }

      // Reenviem mails a la botiga
      $mail = new PHPMailer();
      $mail->CharSet = "UTF-8";
      $mail->IsHTML(true);

      $mail->Host = MAIL_SERVER; // SMTP server
      $mail->From = EMAIL_FROM; // Mail
      $mail->FromName = EMAIL_FROM_NAME; // Nom
      $mail->AddAddress(EMAIL_FROM, EMAIL_FROM_NAME); // A qui l'enviem

      $mail->Subject = $subject . ' (' . $mail_sent_to . ')'; // Titol
      $mail->Body = $message;
      $mail->WordWrap = 100;

      $mail->Send();

      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y'))));
      break;
  }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
