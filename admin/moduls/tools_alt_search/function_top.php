<?php
// Accions
if ($_GET['action']) {
  switch ($_GET['action']) {
    case 'insert':
      if ($_POST['replacement']) {
        // Vector entrades
        $sql_data_array = array('search' => tep_db_prepare_input($_POST['search']),
                                'replacement' => tep_db_prepare_input($_POST['replacement']),
                                'entered' => 'now()',
                                'modified' => 'now()');
        // Crida base dades
        tep_db_perform(TABLE_SEARCH_SWAP, $sql_data_array);
        $aux_id = tep_db_insert_id();
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y')) . 'sID=' . $aux_id));
      break;

    case 'update':
      if ($_GET['sID'] && $_POST['replacement']) {
        // Identificador Usuari
        $aux_id = tep_db_prepare_input($_GET['sID']);
        // Vector entrades
        $sql_data_array = array('replacement' => tep_db_prepare_input($_POST['replacement']),
                                'modified' => 'now()');
        // Crida base dades
        tep_db_perform(TABLE_SEARCH_SWAP, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y'))));
      break;

    case 'delete':
      if ($_GET['sID']) {
        $aux_id = tep_db_prepare_input($_GET['sID']);
        // Eliminem entrada
        tep_db_query("delete from " . TABLE_SEARCH_SWAP . " where id = '" . tep_db_input($aux_id) . "'");
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','sID','x','y'))));
      break;

  }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
