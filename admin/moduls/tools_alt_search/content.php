<h2 class="config"><?php echo TITLE_PAGE; ?></h2>
<?php echo (($_GET['action']== 'new')? TEXT_FORM : TEXT_LIST); ?>  

<?php
if ($_GET['action'] == 'new') {
  // Fitxa
  if (isset($_GET['sID']) && tep_not_null($_GET['sID'])) {
    $id = $_GET['sID'];
    $query_raw = "select id, search, replacement from " . TABLE_SEARCH_SWAP . " where id = '" . (int)$id . "'";
    $item = tep_db_fetch_array(tep_db_query($query_raw));
  } else {
    $item = array('id' => '',
    	          'search' => '',
	          'replacement' => '');
  }
  $itemInfo = new objectInfo($item);

  $form_action = ($_GET['sID']) ? 'update' : 'insert';
?>

<div id="form">
  <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action','pos','x','y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
  <fieldset>
    <label>
      <span>Búsqueda</span>
      <?php
      if ($_GET['sID']) {
        echo '<input type="text" name="search" value="' . $itemInfo->search . '" class="camp_read" readonly="readonly" />' . "\n";
      } else {
        echo '<input type="text" name="search" value="' . $itemInfo->search . '" />' . "\n";
      }
      ?>
    </label>
    <label>
      <span>Alternativa</span>
      <input type="text" name="replacement" value="<?php echo $itemInfo->replacement; ?>" />
    </label>
  </fieldset>
  <p class="botonera">
    <?php
    echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
    echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','sID','x','y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
    ?>
  </p>
  </form>
</div>

<?php
} else {
  // Llistat
  if ($_GET['keyword']) {
    $query_raw = "select id, search, replacement from " . TABLE_SEARCH_SWAP . " where ( search like '%" . $_GET['keyword'] . "%' or replacement like '%" . $_GET['keyword'] . "%' ) order by search";
  } else {
    $query_raw = "select id, search, replacement from " . TABLE_SEARCH_SWAP . " order by search";
  }
  $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
  $listing = tep_db_query($query_raw);
?>

<table summary="<?php echo TAULA; ?>"  class="llista fixed">
  <thead>
    <tr>
      <th style="width:30%;"><?php echo COL_1; ?></th>
      <th style="width:60%;"><?php echo COL_2; ?></th>
      <th style="width:10%; text-align:center;"><?php echo COL_3; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="3">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page','action','x','y'))); ?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if ($query_numrows>0) {
  // Llistat segons búsqueda
  $i=0;
  while ($item = tep_db_fetch_array($listing)) {
    $i++;
    $aux=($aux%2)+1;
    $itemInfo = new objectInfo($item);

    echo '<tr class="' . (($itemInfo->id == $_GET['sID'])? 'current_list' : 'taula' . $aux) . '">' . "\n"
     . '  <td>' . htmlentities($itemInfo->search, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</td>' . "\n"
     . '  <td><span class="vermell">-->&nbsp;&nbsp;' . htmlentities($itemInfo->replacement, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</span></td>' . "\n"
     . '  <td style="text-align:center;">'
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','sID','x','y')) . 'sID=' . $itemInfo->id . '&amp;action=new') . '">' . ICON_EDIT . '</a> | '
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','sID','x','y')) . 'sID=' . $itemInfo->id . '&amp;action=delete') . '" class="delete">' . ICON_DELETE . '</a>'
     . '</td>' . "\n"
     . '</tr>' . "\n";
  }

} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="3" class="no_item">' . sprintf(NO_ITEMS,mb_strtolower(NEW_ITEM,'UTF-8')) . '</td>' . "\n" ;
  echo '</tr>' . "\n";

}
?>

  </tbody>
</table>

<p class="botonera">
  <?php echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','sID','x','y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
</p>

<div id="buscador">
  <?php
  echo tep_draw_form('form_search', tep_href_link('', tep_get_all_get_params(array('action','x','y'))), 'get');
  // Recuperem totes les variables $_GET
  reset($_GET);
  while (list($key, $value) = each ($_GET)) {
    if ( ($key != 'sID') && ($key != 'action') && ($key != 'page') && ($key != 'x') && ($key != 'y') && ($key != tep_session_name()) ) {
      if (tep_not_null($value)) echo tep_draw_hidden_field(rawurldecode($key), rawurldecode($value));
    }
  }
  // Variables buscador
  echo '<label><span>' . SEARCH . '</span>'
    . '<input type="text" name="keyword" value="' . $_GET['keyword'] . '" />'
    . '</label>';
  ?>
  </form>
</div>

<?php
}
?>
