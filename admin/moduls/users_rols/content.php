<h2 class="users"><?php echo TITLE_PAGE; ?></h2> 

<?php
if ($_GET['action'] == 'new') {
    // FITXA
    //Obtenim dades del rol si fem update / cas insert dades buides
    if (isset($_GET['uID']) && tep_not_null($_GET['uID'])) {
        $id = $_GET['uID'];
        $query_raw = "select * from " . TABLE_ADMIN_USERS_ROLS . " WHERE id_rol=" . $id;
        $item = tep_db_fetch_array(tep_db_query($query_raw));
    } else {
        $item = array('id_rol' => '',
            'nom' => ''
        );
    }
    $itemInfo = new objectInfo($item);

    //Llistat de moduls
    $moduls = tep_moduls_get_all();

    //mirem si estem fent update o insert
    $form_action = (isset($_GET['uID'])) ? 'update' : 'insert';

    echo(TEXT_FORM);
    ?>

    <div id="form">
        <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
        <fieldset>
            <h3><?php echo $itemInfo->nom; ?></h3>
            <label class="width_50_1">
                <span>Id*</span>
                <input type="text" name="id_rol" value="<?php echo $itemInfo->id_rol; ?>" />
            </label>
            <label class="width_50_2">
                <span>Nom*</span>
                <input type="text" name="name" value="<?php echo $itemInfo->nom; ?>" />
            </label>
            <p class="form_sep"></p>
            <?php
            echo(TEXT_PERMISOS);
            //mirem els tipus de permisos a la BD
            $tipus_permisos = tep_usuaris_get_all_tipus_permisos();
            ?>
            <table summary="<?php echo TAULA; ?>" class="llista fixed">
                <thead>
                    <tr>
                        <th style="width:90%"><?php echo COL_2; ?></th>
                        <?php
                        //per a cada tipus de permis mostrem una columna
                        while ($tipus_permis = tep_db_fetch_array($tipus_permisos)) {
                            ?>
                            <th style="width:5%; text-align:center;"><?php echo ucfirst($tipus_permis['nom_permis']); ?></th>
                            <?php
                        }
                        //resetegem query
                        tep_db_data_seek($tipus_permisos, 0);
                        ?>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th colspan="7">
                        </th>
                    </tr>
                </tfoot>
                <tbody>

                    <?php
                    if (count($moduls) > 0 && isset($_GET['uID'])) {
                        // Llistat segons búsqueda
                        $i = 0;
                        //llistem tots els moduls
                        foreach ($moduls as $modul) {
                            $i++;
                            $aux = ($aux % 2) + 1;
                            //mirem permisos del rol per aquest modul en BD
                            $query_raw = "SELECT * FROM " . TABLE_ADMIN_USERS_ROLS_PERMISOS . " WHERE id_rol=" . $itemInfo->id_rol . " AND nom_modul='" . $modul . "'";
                            $item = tep_db_fetch_array(tep_db_query($query_raw));
                            ?>
                            <tr class="taula">
                                <td><?php echo $modul; ?> </td> 
                                <?php
                                //per a cada tipus de permis i modul mostrem un check
                                while ($tipus_permis = tep_db_fetch_array($tipus_permisos)) {
                                    $permis = tep_usuaris_mirar_permis_modul($modul, $tipus_permis['id_tipus_permis'], $itemInfo->id_rol);
                                    ?>
                                    <td style="width:5%; text-align:center;"><input class="checkbox" name="permisos[<?php echo $modul; ?>][<?php echo $tipus_permis['id_tipus_permis']; ?>]" <?php echo $permis ? "checked='yes'" : ''; ?> type="checkbox"/></td>
                                    <?php
                                }
                                //resetegem query
                                tep_db_data_seek($tipus_permisos, 0);
                                ?>
                            </tr>
                            <?php
                        }
                    } else {
                        // No Items
                        echo '<tr>' . "\n";
                        echo '  <td colspan="7" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
                        echo '</tr>' . "\n";
                    }
                    ?>

                </tbody>
            </table>

        </fieldset>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
    </form>
    </div>

    <?php
} else {
    // Llistat
    $query_raw = "SELECT ur.id_rol, ur.nom,(SELECT count(u.id) FROM " . TABLE_ADMIN_USERS . " u WHERE u.id_rol=ur.id_rol) as users FROM " . TABLE_ADMIN_USERS_ROLS . " ur ORDER BY ur.id_rol";
    $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
    $listing = tep_db_query($query_raw);

    echo(TEXT_LIST);
    ?>
    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>
    <table summary="<?php echo TAULA; ?>" class="llista fixed">
        <thead>
            <tr>
                <th style="width:5%;"><?php echo COL_1; ?></th>
                <th style="width:75%"><?php echo COL_2; ?></th>
                <th style="width:5%"><?php echo COL_3; ?></th>
                <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="7">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'action', 'x', 'y'))); ?></div>
    </th>
    </tr>
    </tfoot>
    <tbody>

        <?php
        if ($query_numrows > 0) {
            // Llistat segons búsqueda
            $i = 0;
            while ($item = tep_db_fetch_array($listing)) {
                $i++;
                $aux = ($aux % 2) + 1;
                $itemInfo = new objectInfo($item);

                echo '<tr class="' . (($itemInfo->id_rol == $_GET['uID']) ? 'current_list' : 'taula' . $aux) . '" onmouseover="this.className=\'sobre\';" onmouseout="this.className=\'' . (($itemInfo->id_rol == $_GET['uID']) ? 'current_list' : 'taula' . $aux) . '\';">' . "\n"
                . '  <td>' . $itemInfo->id_rol . '</td>' . "\n"
                . '  <td>' . $itemInfo->nom . '</td>' . "\n"
                . '  <td>' . $itemInfo->users . '</td>' . "\n"
                . '  <td style="text-align:center;" class="accions">'
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'buscar', 'x', 'y')) . 'action=new&amp;uID=' . $itemInfo->id_rol) . '">' . ICON_EDIT . '</a> | '
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'buscar', 'x', 'y')) . 'action=delete&amp;uID=' . $itemInfo->id_rol) . '" class="delete">' . ICON_DELETE . '</a>'
                . '</td>' . "\n"
                . '</tr>' . "\n";
            }
        } else {
            // No Items
            echo '<tr>' . "\n";
            echo '  <td colspan="7" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
            echo '</tr>' . "\n";
        }
        ?>

    </tbody>
    </table>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>

    <?php
}
?>
