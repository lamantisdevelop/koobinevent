<?php

// Enviament Mails
require_once(DIR_WS_CLASSES . 'class.phpmailer.php');
require_once(DIR_WS_CLASSES . 'class.smtp.php');

// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
        case 'insert':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_POST['name'])) {
                // Comprovem que no existeixi un rol amb el mateix id
                $user_query = tep_db_query("select count(*) as total from " . TABLE_ADMIN_USERS_ROLS . " where id_rol = '" . tep_db_prepare_input($_POST['id_rol']) . "'");
                $user_mail = tep_db_fetch_array($user_query);
                $aux_duplicate = (int) $user_mail['total'];
                if ($aux_duplicate == 0) {
                    // Vector entrades
                    $sql_data_array = array('id_rol' => tep_db_prepare_input($_POST['id_rol']),
                        'nom' => tep_db_prepare_input($_POST['name'])
                    );

                    // Crida base dades
                    tep_db_perform(TABLE_ADMIN_USERS_ROLS, $sql_data_array);
                    $aux_id = tep_db_insert_id();
                } else {
                    $messageStack->add_session(sprintf(TEXT_EXISTENT_ROL, $_POST['id_rol']), 'error');
                    tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y'))));
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'uID=' . $aux_id));
            break;


        case 'update':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['uID']) && isset($_POST['name'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['uID']);

                // Comprovem que no existeixi un rol amb el mateix id
                $user_query = tep_db_query("select count(*) as total from " . TABLE_ADMIN_USERS_ROLS . " where id_rol = '" . tep_db_prepare_input($_POST['id_rol']) . "'");
                $user_mail = tep_db_fetch_array($user_query);
                $aux_duplicate = (int) $user_mail['total'];
                //mirem si s'esta utilitzant un id nou k ja existeixi (id duplicat)
                if ($aux_duplicate == 0 || tep_db_prepare_input($_POST['id_rol']) == $aux_id) {
                    // Vector entrades
                    $sql_data_array = array('id_rol' => (int) $aux_id,
                        'nom' => tep_db_prepare_input($_POST['name'])
                    );

                    // Crida base dades Rol
                    tep_db_perform(TABLE_ADMIN_USERS_ROLS, $sql_data_array, 'update', "id_rol = '" . (int) $aux_id . "'");

                    // Permisos especifics per moduls
                    $permisos = $_POST['permisos'];
                    $moduls = tep_moduls_get_all();
                    foreach ($moduls as $modul) {
                        $sql_data_array = array('id_rol' => (int) $aux_id,
                            'nom_modul' => $modul
                        );
                        //mirem els tipus de permisos a la BD
                        $tipus_permisos = tep_usuaris_get_all_tipus_permisos();
                        //per a cada tipus de permis mirem valor post
                        while ($tipus_permis = tep_db_fetch_array($tipus_permisos)) {
                            $sql_data_array['id_tipus_permis'] = $tipus_permis['id_tipus_permis'];
                            $sql_data_array['flag'] = $permisos[$modul][$tipus_permis['id_tipus_permis']] == "on" ? 1 : 0;

                            //actualitzem permisos del modul a la BD
                            //mirem si ja existeix a la bd
                            $pages_query = tep_db_query("SELECT id_rol FROM " . TABLE_ADMIN_USERS_ROLS_PERMISOS . " WHERE id_rol = " . (int) $aux_id . " AND nom_modul='" . $modul . "' AND id_tipus_permis=" . $tipus_permis['id_tipus_permis']);
                            $pages_numrows = tep_db_num_rows($pages_query);
                            if ($pages_numrows == 0) { //Cas que no existeix el permis anteriorment fem INSERT
                                tep_db_perform(TABLE_ADMIN_USERS_ROLS_PERMISOS, $sql_data_array);
                            } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                                tep_db_perform(TABLE_ADMIN_USERS_ROLS_PERMISOS, $sql_data_array, 'update', 'id_rol = \'' . (int) $aux_id . '\' AND nom_modul=\'' . $modul . '\' AND id_tipus_permis=\'' . $tipus_permis['id_tipus_permis'] . '\'');
                            }
                        }
                    }

                    $messageStack->add_session(sprintf(TEXT_USER_UPDATED, $_POST['name']), 'success');
                } else {
                    $messageStack->add_session(sprintf(TEXT_EXISTENT_ROL, $_POST['id_rol']), 'error');
                    tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y'))));
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;


        case 'delete':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['uID']) {
                $aux_id = tep_db_prepare_input($_GET['uID']);
                tep_db_query("delete from " . TABLE_ADMIN_USERS_ROLS . " where id_rol = '" . (int) $aux_id . "'");
                tep_db_query("delete from " . TABLE_ADMIN_USERS_ROLS_PERMISOS . " where id_rol = '" . (int) $aux_id . "'");
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'uID=' . $aux_id));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
