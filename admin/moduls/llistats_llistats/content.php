<div id="buscador">
    <input type="text" id="tfBuscar" placeholder="Entrar text per filtrar...">
    <input type="submit" class="boto_buscador" value=""/>
</div>
<h2 class="icon_llistat_3<?php// echo $_GET['gID']; ?>"><?php echo (tep_not_null($groupInfo->name) ? ' ' . $groupInfo->name : ''); ?></h2>

<?php
//Indiquem llistat pare principal (per tal de saber en quin tipus de llistat estem en els casos de subcategories)
$llistat = isset($_GET['llistat']) ? $_GET['llistat'] : $_GET['gID'];

if (substr($_GET['action'], 0, 3) == 'new') 
{
    //insert o update
    if ($_GET['action'] == 'new_item') 
    {
        // Fitxa items
        switch ($llistat) 
        {
            case'1':
                include('content_item_clients.php');
                break;
            case'5':
                include('content_item_premsa.php');
                break;
            case'6':
                include('content_item_treball.php');
                break;
            default:
                include('content_item.php'); //fitxa per defecte posem organigrama per si hi ha subcategories no troba ni 4 ni 10
                break;
        }
    } 
    else if ($_GET['action'] == 'new_group') 
    {
        // Fitxa Categories
        include('content_group.php');
    }
} 
else 
{

    //mirem si estem dins d'una carpeta/categoria
    $group_id = ($_GET['gID'] != '') ? $_GET['gID'] : 0;
    
    // Llistat CATEGORIES
    
    //obtenim les categories filles
    $groups_query = tep_get_llistat_groups_by_parent_id_admin($group_id);
    $groups_rows = tep_db_num_rows($groups_query);

    $group = tep_get_llistat_group_by_id($group_id);

    // LListat PRODUCTES
    if (isset($_GET['keyword']) && tep_not_null($_GET['keyword'])) {
        $query_raw = "select p.id,p.autor,pd.download,pd.titol, p.active, p.published, p.entered,p.listorder from " . TABLE_LLISTATS . " p left join " . TABLE_LLISTATS_DESCRIPTION . " pd on p.id = pd.llistats_id and pd.language_id = '" . (int) $language_id . "' where p.group_id = '" . (int) $_GET['gID'] . "' and pd.titol like '%" . tep_db_input($_GET['keyword']) . "%' order by p.listorder,p.published desc";
    } else {
        $query_raw = "select p.id,p.autor,pd.download,pd.titol, p.active, p.published, p.entered,p.listorder from " . TABLE_LLISTATS . " p left join " . TABLE_LLISTATS_DESCRIPTION . " pd on p.id = pd.llistats_id and pd.language_id = '" . (int) $language_id . "' where p.group_id = '" . (int) $_GET['gID'] . "' order by p.listorder,p.published desc";
    }
    $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
    $listing = tep_db_query($query_raw);
    $num_productes = tep_db_num_rows($listing);

    if (!$num_productes) {
        ?>
        <table summary="<?php echo TAULA; ?>"  class="llista fixed">
            <thead>
                <tr>
                    <th style="width:45%;"><?php echo COL_1; ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_2a; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_3; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="5">
            <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_GROUPS, $groups_rows); ?></div>
        </th>
        </tr>
        </tfoot>
        <tbody>
            <?php
            //Tornar categoria superior
            if ($group['parent_id'] != 0) {//Mostrem icona carpeta enrere en cas de tecnics
                ?>
                <tr class="taula2" onclick="window.location = '<?php echo tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'gID', 'pID', 'x', 'y')) . 'gID=' . $group['parent_id']); ?>'">
                    <td colspan="5"><a title="Pujar un nivell en les carpetes" href="<?php echo tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'gID', 'pID', 'x', 'y')) . 'gID=' . $group['parent_id']); ?>"><?php echo ICON_FOLDER; ?><em>&uarr;</em></a></td>
                </tr>
                <?php
            }
            if ($groups_rows > 0) {

                // Llistat CATEGORIES
                while ($categories = tep_db_fetch_array($groups_query)) {
                    $i++;
                    $aux = ($aux % 2) + 1;
                    $cInfo = new objectInfo($categories);

                    $aux_order = '';
                    if ($cInfo->listorder > 1) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=down&amp;gID=' . $cInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }
                    $aux_order .= '&nbsp;&nbsp;' . (($cInfo->listorder < 10) ? '0' : '') . $cInfo->listorder . '&nbsp;&nbsp;';
                    if ($cInfo->listorder < $groups_rows) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=up&amp;gID=' . $cInfo->id) . '" onclick="window.location.href=\'evento.preventDefault();\'">' . ICON_ORDER_UP . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }
                    if ($cInfo->active) {
                        $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'pID', 'buscar', 'x', 'y')) . 'action=desactive_category&amp;cID=' . $cInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                    } else {
                        $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'pID', 'buscar', 'x', 'y')) . 'action=active_category&amp;cID=' . $cInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                    }

                    //Indiquem llistat pare principal (per tal de saber en quin tipus de llistat estem en els casos de subcategories)
                    $llistat = isset($_GET['llistat']) ? $_GET['llistat'] : $cInfo->id;


                    echo '<tr onclick="window.location.href=\'' . tep_href_link('', 'modul=llistats_llistats&gID=' . $cInfo->id . '&llistat=' . $llistat) . '\'" class="' . (($cInfo->id == $_GET['gID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                    . '  <td><a href="' . tep_href_link('', 'modul=llistats_llistats&gID=' . $cInfo->id . '&llistat=' . $llistat) . '">' . ICON_FOLDER . ' ' . ($cInfo->name) . '</a></td>' . "\n"
                    . '  <td style="text-align:center;">' . $cInfo->id . '</td>' . "\n"
                    . '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
                    . '  <td class="order">' . $aux_order . '</td>' . "\n"
                    . '  <td style="text-align:center;" class="accions">'
                    . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'gID=' . $cInfo->id . '&amp;action=new_group') . '">' . ICON_EDIT . '</a> '
                    . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'gID=' . $cInfo->id . '&amp;action=delete_group') . '"  class="delete">' . ICON_DELETE . '</a>'
                    . '</td>' . "\n"
                    . '</tr>' . "\n";
                }
            } else {
                // No Items
                echo '<tr>' . "\n";
                echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_CATEGORY, 'UTF-8')) . '</td>' . "\n";
                echo '</tr>' . "\n";
            }
        } else {
            // LListat PRODUCTES
            ?>

        <p class="botonera">
            <?php
            if (tep_usuaris_es_admin()) {
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'pID', 'keyword', 'x', 'y')) . 'parent_id=' . $group_id . '&action=new_group') . '"><span>' . NEW_CATEGORY . '</span></a>';
            }
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'pID', 'keyword', 'x', 'y')) . 'gID=' . $group_id . '&action=new_item') . '"><span>' . NEW_ITEM . '</span></a>';
            ?>
        </p>
        <table summary="<?php echo TAULA; ?>" class="llista fixed">
            <thead>
                <tr>
                    <th style="width:66%;"><?php echo COL_1; ?></th>
<!--                    <th style="width:10%;"><?php echo COL_2; ?></th>-->
                    <th style="width:7%; text-align:center;"><?php echo COL_3; ?></th>
                    <th style="width:7%; text-align:center;"><?php echo ($_GET['group_id'] == '12' ? 'Portada' : ''); ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="6">
            <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
            <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'action', 'x', 'y'))); ?></div>
            </th>
            </tr>
            </tfoot>
            <tbody>
                <?php
                // Tornar a categories
                /* echo '<tr class="taula2">' . "\n"
                  . '  <td colspan="5"><a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'group_id', 'gID', 'pID', 'x', 'y')) . 'gID=' . $_GET['group_id']) . '">' . ICON_FOLDER . '..</a></td>' . "\n"
                  . '</tr>' . "\n"; */
                //Indiquem llistat pare principal (per tal de saber en quin tipus de llistat estem en els casos de subcategories)
                $llistat = isset($_GET['llistat']) ? $_GET['llistat'] : $_GET['gID'];
                //Tornar categoria superior
                $llistat_actual = (tep_get_llistat_group_by_id($_GET['gID'])); //obtenim info del grup actual
                if ($llistat_actual['parent_id'] != '0') {//Mostrem icona carpeta enrere en subcarpetes
                    echo '<tr class="taula2" onclick="window.location = \''. tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'gID', 'pID', 'x', 'y')) . 'gID=' . $group['parent_id']).'\'">' . "\n"
                    . '  <td colspan="6"><a  title="Pujar un nivell en les carpetes" href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'gID', 'pID', 'x', 'y')) . 'gID=' . $group['parent_id']) . '">' . ICON_FOLDER . '<em>&uarr; </em></a></td>' . "\n"
                    . '</tr>' . "\n";
                }
                if ($query_numrows > 0) {

                    // Llistat segons b�squeda
                    $i = 0;
                    while ($item = tep_db_fetch_array($listing)) {
                        $i++;
                        $aux = ($aux % 2) + 1;
                        $itemInfo = new objectInfo($item);

                        $aux_order = '';
                        if ($itemInfo->listorder > 1) {
                            $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'value', 'keyword', 'x', 'y')) . 'action=listorder_item&amp;value=down&amp;pID=' . $itemInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
                        } else {
                            $aux_order .= ICON_ORDER_NO;
                        }
                        $aux_order .= '&nbsp;&nbsp;' . (($itemInfo->listorder < 10) ? '0' : '') . $itemInfo->listorder . '&nbsp;&nbsp;';
                        if ($itemInfo->listorder < $query_numrows) {
                            $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'value', 'keyword', 'x', 'y')) . 'action=listorder_item&amp;value=up&amp;pID=' . $itemInfo->id) . '">' . ICON_ORDER_UP . '</a>';
                        } else {
                            $aux_order .= ICON_ORDER_NO;
                        }
                        if ($itemInfo->active) {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=desactive&amp;pID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                        } else {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=active&amp;pID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                        }
                        //en cas de management mostrem nom sense idioma (autor) en comptes de (nom)
                        $nom = ($group_id == 26 || $group_id == 3 || $itemInfo->titol == '') ? $itemInfo->autor : $itemInfo->titol;

                        echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=new_item&amp;pID=' . $itemInfo->id) . '\'" class="' . (($itemInfo->id == $_GET['pID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                        . '  <td class="nom">' . $itemInfo->titol . '</td>' . "\n"
                        //. '  <td>' . (tep_not_null(tep_date_short($itemInfo->published)) ? tep_date_short($itemInfo->published) : '---------------') . '</td>' . "\n"
                        . '  <td style="text-align:center;" class="fix_width">' . $aux_active . '</td>' . "\n"
                        . '  <td style="text-align:center;" class="fix_width">' . (($_GET['group_id'] == '12') ? $aux_portada : '') . '</td>' . "\n"//només per a formacio
                        . '  <td class="order">' . $aux_order . '</td>' . "\n"
                        . '  <td style="text-align:center;" class="fix_width accions">'
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=new_item&amp;pID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a> '
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=delete_item&amp;pID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>'
                        . '</td>' . "\n"
                        . '</tr>' . "\n";
                    }
                } else {
                    // No Items
                    echo '<tr>' . "\n";
                    echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS, strtolower(NEW_ITEM)) . '</td>' . "\n";
                    echo '</tr>' . "\n";
                }
            }
            ?>

        </tbody>
    </table>

    <p class="botonera">
        <?php
        if (tep_usuaris_es_admin()) {
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'pID', 'keyword', 'x', 'y')) . 'parent_id=' . $group_id . '&action=new_group') . '"><span>' . NEW_CATEGORY . '</span></a>';
        }
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'pID', 'keyword', 'x', 'y')) . 'gID=' . $group_id . '&action=new_item') . '"><span>' . NEW_ITEM . '</span></a>';
        ?>
    </p>

    <?php
}
?>

