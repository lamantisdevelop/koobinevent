<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<?php
if (isset($_GET['pID']) && tep_not_null($_GET['pID'])) {//CAS UPDATE
    $id = $_GET['pID'];
    $item = tep_db_fetch_array(tep_get_llistat_by_id($id));
    $form_action = 'update_item';
} else {//CAS INSERT
    $item = array('id' => '',
        'autor' => '',
        'title' => '',
        'published' => '');
    $form_action = 'insert_item';
}
$itemInfo = new objectInfo($item);
?>
<div id="form">
    <?php //FORMULARI  ---------------------------------------- ?>
    <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
    <?php //BOTONS  ---------------------------------------- ?>
    <p class="botonera">
        <?php
        echo '<a href="#" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>


    <?php //camps GENERALS ---------------------------------------- ?>
    <fieldset>
        <label class="width_50_1">
            <span>Ordre</span>
            <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
            <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
        </label>

        <label class="width_50_2">
            <span>Link <em class="help">(Links externs incloure HTTP://)</em></span>
            <input type="text" name="link" value="<?php echo $itemInfo->link; ?>" />
        </label>
<!--        <p class="form_sep"></p>-->
    </fieldset>

    <?php //camp CHECKBOX ---------------------------------------- ?>
<!--    <h4><span >Checkboxes</span></h4>
    <fieldset>
        <label class="width_50_1">
            <span>Inscripcions</span>
            <input type="checkbox" name="inscripcions" value="1" <?php echo ($itemInfo->inscripcions) ? 'checked="checked"' : ''; ?> class="checkbox" />
        </label>
    </fieldset>-->

    <?php //camps LLISTATS RELACIONATS SIMPLE---------------------------------------- ?>
    


    <?php //camps LLISTATS RELACIONATS MULTIPLES----------------------------------------  ?>

    <?php //camps IDIOMES ----------------------------------------  ?>
    <?php
    if (sizeof($languages) > 1) {
        ?>
        <ul id="menuIdioma">
            <?php
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
            }
            ?>
        </ul>
        <?php
    }
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        // Busquem valors
        $aux_listing = tep_db_query("select titol,titol2,titol3, descripcio,descripcio2,descripcio3,download from " . TABLE_LLISTATS_DESCRIPTION . " where llistats_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
        $aux_numrows = tep_db_num_rows($aux_listing);
        if ($aux_numrows > 0) {
            $aux_item = tep_db_fetch_array($aux_listing);
            $auxInfo = new objectInfo($aux_item);
        }
        ?>
        <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
            <label class="width_50_1">
                <span >Titol</span>
                <input type="text" name="titol[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->titol; ?>" />
            </label>
            <p class="form_sep"></p>
            <?php
//            if (tep_not_null($auxInfo->download)) {
//                echo tep_draw_hidden_field('download_prev[' . $languages[$i]['id'] . ']', $auxInfo->download) . "\n";
//                echo '<label class="width_50_1"><span>Descàrrega Actual</span><input type="text" name="download_act[' . $languages[$i]['id'] . ']" value="' . $auxInfo->download . '" class="camp_read" readonly="readonly" />' . "\n"
//                . '<a href="javascript:void(0);" class="link_del"></a>' . "\n"
//                . '<a href="' . DIR_WS_PORTAL_DOWNLOADS . $auxInfo->download . '" title="' . $auxInfo->download . '" class="link_view" target="_blank"></a></label>' . "\n"
//                . '<label class="width_50_2"><span>Nova Descàrrega</span><input type="file" name="download_new[' . $languages[$i]['id'] . ']" value="" size="45" class="file" /></label>' . "\n";
//            } else {
//                echo '<label class="width_50_1"><span>Descàrrega</span><input type="file" name="download_new[' . $languages[$i]['id'] . ']" value="" size="45" class="file" /></label>' . "\n";
//            }
            ?>
        </fieldset>
        <?php
    }
    ?>
    <h4>Galeria d'Imatges</h4>
    <fieldset>
        <label class="width_50_1">
            <div id="continent_select_galeries">
                <?php echo tep_draw_pull_down_menu('fotos_groups_id', tep_get_fotos_groups_array(FOTOS_GROUP_CLIENTS, '--'), $itemInfo->fotos_groups_id, 'id="fotos_groups_id"'); ?>
            </div>
        </label>
        <label class="width_50_2">
            <div >
                <a class="nova_galeria" id="nova_galeria" onclick=" return novaGaleria('<?php echo FOTOS_GROUP_CLIENTS; ?>');" href="#" title="Crear una nova galeria">NOVA GALERIA <span class="container_loading"><img style="display: none;margin-top:5px;" id="loading_galeria" src="image/loading_linia.gif" alt="loading"/></span></a>
            </div>
        </label>
        <?php
        //mirem si tenim associada alguna galeria
        $src = !is_null($itemInfo->fotos_groups_id) && tep_galeria_exists($itemInfo->fotos_groups_id) ? '?modul=fotos_fotos&group_id=' . $itemInfo->fotos_groups_id . '&iframe=si' : 'about:blank';
        ?>

        <iframe scrolling="no" frameborder=0 border=0 id="childframe" name="childframe" src="<?php echo $src; ?>"></iframe>
    </fieldset>

    <?php //BOTONS  ----------------------------------------  ?>
    <p class="botonera">
        <?php
        echo '<a href="#" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>
</form>
</div>


