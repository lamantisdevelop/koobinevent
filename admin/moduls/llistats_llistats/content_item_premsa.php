<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<?php
if (isset($_GET['pID']) && tep_not_null($_GET['pID'])) {//CAS UPDATE
    $id = $_GET['pID'];
    $item = tep_db_fetch_array(tep_get_llistat_by_id($id));
    $form_action = 'update_item';
} else {//CAS INSERT
    $item = array('id' => '',
        'autor' => '',
        'title' => '',
        'published' => '');
    $form_action = 'insert_item';
}
$itemInfo = new objectInfo($item);
?>
<script type="text/javascript">
    var geocoder;
    var map;
    var infowindow = new google.maps.InfoWindow();

    function load() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(<?php echo ($itemInfo->lat) ? $itemInfo->lat : '41.96612757631878'; ?>,<?php echo ($itemInfo->lon) ? $itemInfo->lon : '2.8184395004063845'; ?>);
        var myOptions = {
            zoom: 13,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("google_map"),
                myOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo ($itemInfo->lat) ? $itemInfo->lat : '41.96612757631878'; ?>,<?php echo ($itemInfo->lon) ? $itemInfo->lon : '2.8184395004063845'; ?>),
            map: map
        });

        //onclick mostrem lat lon
        google.maps.event.addListener(map, 'click', function(event) {
            //lat lon dins input html
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lon').value = event.latLng.lng();

            geocoder.geocode({'latLng': event.latLng, 'region': 'ca'}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        //map.setZoom(11);
                        infowindow.setContent('<div>' + results[1].formatted_address + '</div><div>' + results[3].formatted_address + '</div><div>' + results[4].formatted_address + '</div>');
                        infowindow.open(map, marker);
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });


            // actualitzem posicio                
            var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
            marker.setPosition(latlng);
        });

    }
    $(document).ready(function() {
        load(); // carreguem Google Maps
    });
</script>
<div id="form">
    <?php //FORMULARI  ---------------------------------------- ?>
    <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
    <?php //BOTONS  ---------------------------------------- ?>
    <p class="botonera">
        <?php
        echo '<a href="#" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>


    <?php //camps GENERALS ---------------------------------------- ?>
    <fieldset>
        <label class="width_50_1">
            <span>Ordre</span>
            <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
            <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
        </label>
        
        <label class="width_50_2">
            <span>Link <em class="help">(Links externs incloure HTTP://)</em></span>
            <input type="text" name="link" value="<?php echo $itemInfo->link; ?>" />
        </label>
        
        <p class="form_sep"></p>
    </fieldset>
    <?php
    if (sizeof($languages) > 1) {
        ?>
        <ul id="menuIdioma">
            <?php
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
            }
            ?>
        </ul>
        <?php
    }
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        // Busquem valors
        $aux_listing = tep_db_query("select titol,titol2,titol3, descripcio,descripcio2,descripcio3,download from " . TABLE_LLISTATS_DESCRIPTION . " where llistats_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
        $aux_numrows = tep_db_num_rows($aux_listing);
        if ($aux_numrows > 0) {
            $aux_item = tep_db_fetch_array($aux_listing);
            $auxInfo = new objectInfo($aux_item);
        }
        ?>
        <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
            <label>
                <span >Titol</span>
                <input type="text" name="titol[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->titol; ?>" />
            </label>
            
            <div class="clear top_15"></div>

            <span><b>Descripció</b></span>
            <textarea name="descripcio[<?php echo $languages[$i]['id']; ?>]" id="descripcio[<?php echo $languages[$i]['id']; ?>]" >
                <?php echo $auxInfo->descripcio; ?>
            </textarea>
            <script>
                <?php 
                if (!tep_usuaris_es_admin())
                { 
                    echo("CKEDITOR.config.toolbar = 'Editor';");                     
                }
                ?>
                CKEDITOR.replace( 'descripcio[<?php echo $languages[$i]['id']; ?>]');
            </script>
            <p class="form_sep"></p>
            
        </fieldset>
        <?php
    }
    ?>


    <?php //BOTONS  ----------------------------------------  ?>
    <p class="botonera">
        <?php
        echo '<a href="#" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>
</form>
</div>


