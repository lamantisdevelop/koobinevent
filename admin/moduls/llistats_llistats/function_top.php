<?php

// Include thumbnail class
require_once(DIR_WS_CLASSES . 'ThumbLib.inc.php');

// Definim vector per selecció target
$target_array = array(array('id' => '_blank', 'text' => '_blank'),
    array('id' => '_self', 'text' => '_self'));

// Creem Array Idiomes
$languages = tep_get_languages();

// Funcions Auxiliars
function delete_item($item_id) {

    // Busquem i modifiquem ordre
    $order_now = tep_search_item_order_group($item_id, TABLE_LLISTATS, 'group_id', $_GET['gID']);

    // Eliminem entrada
    tep_db_query("delete from " . TABLE_LLISTATS . " where id = '" . (int) $item_id . "'");
    
    // Reordenem items
    tep_reorder_items_group($order_now, TABLE_LLISTATS, 'group_id', $_GET['gID']);

    // Eliminar x descripcio
    tep_db_query("delete from " . TABLE_LLISTATS_DESCRIPTION . " where llistats_id = '" . (int) $item_id . "'");
    
    //Eliminem relacionats
    tep_db_query("delete from " . TABLE_LLISTATS_RELATED . " where llistats_id = " . (int) $item_id);
}

function delete_group($groupID)
{
    delete_grup_recursiu($groupID);
}

function delete_grup_recursiu($idCat)
{
    $groups_raw = tep_db_query( " SELECT DISTINCT bg.id"
                              . " FROM " . TABLE_LLISTATS_GROUPS . " bg "
                              . " WHERE bg.parent_id = '" . $idCat ."'");
    
    while ($item = tep_db_fetch_array($groups_raw)) 
    {
        delete_grup_recursiu($item['id']);
    }
    
    //ELIMINEM LES RELACIONS
    tep_db_query(" DELETE FROM " . TABLE_LLISTATS_RELATED 
               . " WHERE id_llistats_groups = '" . $idCat . "'");

    //ELIMINEM LES DESCRIPCIONS DEL ITEMS
    tep_db_query(" DELETE ". TABLE_LLISTATS_DESCRIPTION
               . " FROM " . TABLE_LLISTATS_DESCRIPTION
               . " LEFT JOIN " . TABLE_LLISTATS . " on " . TABLE_LLISTATS_DESCRIPTION . ".llistats_id = " . TABLE_LLISTATS . ".id "
               . " WHERE " . TABLE_LLISTATS . ".group_id = '" . $idCat . "'");

    //ELIMINEM ELS ITEMS
    tep_db_query(" DELETE FROM " . TABLE_LLISTATS 
               . " WHERE group_id = '" . $idCat . "'");

    //ELIMINEM LES DESCRIPCIONS DE LA CATEGORIA
    tep_db_query(" DELETE FROM " . TABLE_LLISTATS_GROUPS_DESCRIPTION 
               . " WHERE groups_id = '" . $idCat . "'");

    //ELIMINEM LA CATEGORIA
    tep_db_query(" DELETE FROM " . TABLE_LLISTATS_GROUPS 
               . " WHERE id = '" . $idCat . "'");
}

// Busquem valors del grup
if ($_GET['gID']) {
    $query = tep_db_query("select bg.id, bg.name from " . TABLE_LLISTATS_GROUPS . " bg where bg.id = '" . (int) $_GET['gID'] . "' limit 1");
    $value = tep_db_fetch_array($query);
    $groupInfo = new objectInfo($value);
}

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case $_GET['action'] == 'new_item' || $_GET['action'] == 'new_group'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;

//------------------------------ Grups ------------------------------//

        case 'insert_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Busquem nou ordre
                $new_id = tep_search_new_id(TABLE_LLISTATS_GROUPS);
                $order_now = tep_search_new_order_group(TABLE_LLISTATS_GROUPS, 'parent_id', $_GET['parent_id']);

                // Vector entrades
                $sql_data_array = array('id' => (int) $new_id,
                    'name' => tep_db_prepare_input($_POST['name']),
                    'listorder' => $order_now,
                    'parent_id' => $_POST['parent_id'],
                    'active' => 1,
                    'entered' => 'now()',
                    'modified' => 'now()',
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']));
                // Crida base dades
                tep_db_perform(TABLE_LLISTATS_GROUPS, $sql_data_array);

                // Entrades segons idioma
                $title_array = $_POST['title'];
                $description_array = $_POST['description'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('groups_id' => (int) $new_id,
                        'language_id' => (int) $lang_aux,
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    // Crida base dades
                    tep_db_perform(TABLE_LLISTATS_GROUPS_DESCRIPTION, $sql_data_array);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'gID=' . $new_id));
            break;

        case 'update_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['gID'])) {
                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['gID']);

                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'parent_id' => $_POST['parent_id'],
                    'modified' => 'now()',
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']));

                // Crida base dades
                tep_db_perform(TABLE_LLISTATS_GROUPS, $sql_data_array, 'update', "id = '" . (int) $aux_id . "'");

                // Entrades segons idioma
                $title_array = $_POST['title'];
                $description_array = $_POST['description'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('groups_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    // Crida base dades
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT groups_id FROM " . TABLE_LLISTATS_GROUPS_DESCRIPTION . " WHERE groups_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_LLISTATS_GROUPS_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_LLISTATS_GROUPS_DESCRIPTION, $sql_data_array, 'update', 'groups_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }
                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] != $_POST['listorder_aux']) {
                    tep_update_order_group($aux_id, $_POST['listorder_aux'], $_POST['listorder'], TABLE_LLISTATS_GROUPS, 'parent_id', $_POST['parent_id']);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'active_category':
        case 'desactive_category':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['cID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['cID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive_category') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_LLISTATS_GROUPS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
                //actualitzem fills
                tep_db_perform(TABLE_LLISTATS, $sql_data_array, 'update', 'group_id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'llistat', 'x', 'y'))));
            break;

        case 'listorder_group':
            tep_usuaris_filtrar_acces(PERMIS_LECTURA);
            if ($_GET['gID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['gID']);
                $llistat_group_editant= tep_get_llistat_group_by_id($aux_id);
                // Trobar posicio actual
                $order_now = tep_search_item_order_group($aux_id, TABLE_LLISTATS_GROUPS, 'parent_id', $llistat_group_editant['parent_id']);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order_group($aux_id, $order_now, $pos_aux, TABLE_LLISTATS_GROUPS, 'parent_id', $llistat_group_editant['parent_id']);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'gID', 'llistat', 'x', 'y'))) . '&llistat=' . $_GET['llistat'] . '&gID=' . $llistat_group_editant['parent_id']);
            break;

        case 'delete_group':
            
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            
            if (isset($_GET['gID'])) 
            {
                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['gID']);
                
                // Busquem i modifiquem ordre
                $order_now = tep_search_item_order($aux_id, TABLE_LLISTATS_GROUPS);
                
                // Eliminar entrada
                delete_group($_GET['gID']);
                
                // Reordenar items
                tep_reorder_items($order_now, TABLE_LLISTATS_GROUPS);
                
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'gID', 'llistat', 'x', 'y'))) . '&llistat=' . $_GET['llistat'] . '&gID=' . $_GET['llistat']);
            break;

//------------------------------ Opcions ------------------------------//

        case 'insert_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Busquem nou ordre
                $order_now = tep_search_new_order_group(TABLE_LLISTATS, 'group_id', $_GET['gID']);


                // Vector entrades
                $sql_data_array = array('group_id' => tep_db_prepare_input($_GET['gID']),
                    'autor' => tep_db_prepare_input($_POST['autor']),
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
                    'listorder' => $order_now,
                    'published' => tep_db_prepare_input($_POST['published']),
                    'lat' => tep_db_prepare_input($_POST['lat']),
                    'lon' => tep_db_prepare_input($_POST['lon']),
                    'link' => tep_db_prepare_input($_POST['link']),
                    'generic1' => tep_db_prepare_input($_POST['generic1']),
                    'generic2' => tep_db_prepare_input($_POST['generic2']),
                    'generic3' => tep_db_prepare_input($_POST['generic3']),
                    'generic4' => tep_db_prepare_input($_POST['generic4']),
                    'generic5' => tep_db_prepare_input($_POST['generic5']),
                    'inscripcions' => tep_db_prepare_input($_POST['inscripcions']),
                    'active' => '1',
                    'entered' => 'now()',
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_LLISTATS, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Entrades segons idioma
                $titol_array = $_POST['titol'];
                $titol2_array = $_POST['titol2'];
                $titol3_array = $_POST['titol3'];
                $descripcio_array = $_POST['descripcio'];
                $descripcio2_array = $_POST['descripcio2'];
                $descripcio3_array = $_POST['descripcio3'];
                $download = $_FILES['download_new'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];
                    // Upload descàrrega
                    $download_aux = $download['name'][$lang_aux];
                    if (!empty($download_aux)) {
                        if (is_uploaded_file($download['tmp_name'][$lang_aux])) {
                            // Nom imatge no repetit
                            $download_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_aux);
                            // Copiar imatge
                            move_uploaded_file($download['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_aux);
                        }
                    }

                    // Insertem dades
                    $sql_data_array = array('llistats_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'titol' => tep_db_prepare_input($titol_array[$lang_aux]),
                        'titol2' => tep_db_prepare_input($titol2_array[$lang_aux]),
                        'titol3' => tep_db_prepare_input($titol3_array[$lang_aux]),
                        'download' => tep_db_prepare_input($download_aux),
                        'descripcio' => tep_db_prepare_input($descripcio_array[$lang_aux]),
                        'descripcio2' => tep_db_prepare_input($descripcio2_array[$lang_aux]),
                        'descripcio3' => tep_db_prepare_input($descripcio3_array[$lang_aux]));
                    // Crida base dades
                    tep_db_perform(TABLE_LLISTATS_DESCRIPTION, $sql_data_array);
                }
                // Foto portada
                if (isset($_POST['portada']) && ($_POST['portada'] == 'on')) {
                    tep_db_query("update " . TABLE_LLISTATS_GROUPS . " set foto_portada = '" . (int) $aux_id . "' where id = '" . $groupInfo->id . "'");
                }

                //LLISTATS RELACIONATS

                $groups_array = $_POST['id_llistats_groups']; //ids dels grups a relacionar
                //recorrem tots els grups de relacionats passats per post
                foreach ($groups_array as $group) {
                    $destacat_array = $_POST['related_id'][$group]; //ids dels llistats a relacionar
                    $tots_llistats = tep_get_llistats_array_by_id_group($group); //tots els llistats del grup (el grup relacionat es pot canviar manualment des de la fitxa)
                    $llistats_relacionats = tep_get_llistats_relacionats_by_group($aux_id, $group); //obtenim els llistats relacionats actuals
                    foreach ($tots_llistats as $llistat) {
                        $a_destacar = is_array($destacat_array) ? in_array($llistat['id'], $destacat_array) : $llistat['id'] == $destacat_array; //Si son relacionats multiples be donat amb array, si es simple ve donat amb string
                        if ($a_destacar) {//si desde POST es RELACIONAT amb aquest llistat
                            //si ja era relacionat no cal fer res, el deixem igual
                            print_r($llistats_relacionats);
                            if (is_null($llistats_relacionats) || !array_key_exists($llistat['id'], $llistats_relacionats)) {//si NO ERA RELACIONAT abans fem INSERT
                                // Vector entrades
                                $sql_data_array = array(
                                    'llistats_id' => $aux_id,
                                    'related_id' => $llistat['id'],
                                    'id_llistats_groups' => $group,
                                    'entered' => 'now()');
                                // Guardem a BD
                                tep_db_perform(TABLE_LLISTATS_RELATED, $sql_data_array);
                            }
                        } else {//llistat NO RELACIONAT
                            //Eliminem el relacionat (si exsitia ja com a relacionat el borra sino no fa res)
                            tep_db_query("delete from " . TABLE_LLISTATS_RELATED . " where llistats_id = " . $aux_id . " AND related_id=" . $llistat['id'] . " AND id_llistats_groups=" . $group);
                        }
                    }
                }

                // Canviem items ORDRE si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] < $order_now) {
                    tep_update_order_group($aux_id, $order_now, $_POST['listorder'], TABLE_LLISTATS, 'group_id', $_GET['group_id']);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'pID=' . $aux_id));
            break;

        case 'update_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['pID'] && $_POST) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Identificador
                $aux_id = tep_db_prepare_input($_GET['pID']);

                // Vector entrades
                $sql_data_array = array('autor' => tep_db_prepare_input($_POST['autor']),
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
                    'published' => tep_db_prepare_input($_POST['published']),
                    'lat' => tep_db_prepare_input($_POST['lat']),
                    'lon' => tep_db_prepare_input($_POST['lon']),
                    'link' => tep_db_prepare_input($_POST['link']),
                    'generic1' => tep_db_prepare_input($_POST['generic1']),
                    'generic2' => tep_db_prepare_input($_POST['generic2']),
                    'generic3' => tep_db_prepare_input($_POST['generic3']),
                    'generic4' => tep_db_prepare_input($_POST['generic4']),
                    'generic5' => tep_db_prepare_input($_POST['generic5']),
                    'inscripcions' => tep_db_prepare_input($_POST['inscripcions']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_LLISTATS, $sql_data_array, 'update', 'id = \'' . (int) $aux_id . '\'');



                // Entrades segons idioma
                $titol_array = $_POST['titol'];
                $titol2_array = $_POST['titol2'];
                $titol3_array = $_POST['titol3'];
                $descripcio_array = $_POST['descripcio'];
                $descripcio2_array = $_POST['descripcio2'];
                $descripcio3_array = $_POST['descripcio3'];
                $download_act_array = $_POST['download_act'];
                $download_prev_array = $_POST['download_prev'];
                $download = $_FILES['download_new'];

                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Upload descarrega
                    $download_aux = $download['name'][$lang_aux];
                    if (!empty($download_aux)) {
                        if (is_uploaded_file($download['tmp_name'][$lang_aux])) {
                            // Borrem imatge vella
                            $download_old = DIR_FS_PORTAL_DOWNLOADS . $download_act_array[$lang_aux];
                            if (file_exists($download_old))
                                @unlink($download_old);
                            // Nom imatge no repetit
                            $download_aux = rename_if_exists(DIR_FS_PORTAL_DOWNLOADS, $download_aux);
                            // Copiar imatge
                            move_uploaded_file($download['tmp_name'][$lang_aux], DIR_FS_PORTAL_DOWNLOADS . $download_aux);
                        }
                    } else {
                        $download_aux = $download_act_array[$lang_aux];
                        // Si �s buit borrem imatge, cas que n'hi hagi
                        if (!tep_not_null($download_act_array[$lang_aux]) && tep_not_null($download_prev_array[$lang_aux])) {
                            // Borrem imatge vella
                            $download_old = DIR_FS_PORTAL_DOWNLOADS . $download_prev_array[$lang_aux];
                            if (file_exists($download_old))
                                @unlink($download_old);
                        }
                    }

                    // Insertem dades
                    $sql_data_array = array('llistats_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'titol' => tep_db_prepare_input($titol_array[$lang_aux]),
                        'titol2' => tep_db_prepare_input($titol2_array[$lang_aux]),
                        'titol3' => tep_db_prepare_input($titol3_array[$lang_aux]),
                        'download' => tep_db_prepare_input($download_aux),
                        'descripcio' => tep_db_prepare_input($descripcio_array[$lang_aux]),
                        'descripcio2' => tep_db_prepare_input($descripcio2_array[$lang_aux]),
                        'descripcio3' => tep_db_prepare_input($descripcio3_array[$lang_aux]));
                    // Crida base dades
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT llistats_id FROM " . TABLE_LLISTATS_DESCRIPTION . " WHERE llistats_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_LLISTATS_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_LLISTATS_DESCRIPTION, $sql_data_array, 'update', 'llistats_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }

                // Foto portada
                if (isset($_POST['portada']) && ($_POST['portada'] == 'on')) {
                    tep_db_query("update " . TABLE_LLISTATS_GROUPS . " set foto_portada = '" . (int) $aux_id . "' where id = '" . $groupInfo->id . "'");
                }

                //LLISTATS RELACIONATS

                $groups_array = $_POST['id_llistats_groups']; //ids dels grups a relacionar
                //recorrem tots els grups de relacionats passats per post
                foreach ($groups_array as $group) {
                    $destacat_array = $_POST['related_id'][$group]; //ids dels llistats a relacionar
                    $grupsFillsRelacionats = tep_get_llistat_groups_by_parent_id($group); //obtenim les categories filles
                    $tots_llistats = tep_get_llistats_array_by_id_group($group); //tots els llistats del grup (el grup relacionat es pot canviar manualment des de la fitxa)
                    //Subgrups
                    if (tep_db_num_rows($grupsFillsRelacionats)) {//cas el grup te subgrups
                        while ($grupFill = tep_db_fetch_array($grupsFillsRelacionats)) {//recorrem els grups fills
                            $tots_llistats = array_merge($tots_llistats, tep_get_llistats_array_by_id_group($grupFill['id'])); //anem afegint tots els llistats de cada grup fill
                        }
                    }
                    $llistats_relacionats = tep_get_llistats_relacionats_by_group($aux_id, $group); //obtenim els llistats relacionats actuals
                    foreach ($tots_llistats as $llistat) {
                        $a_destacar = is_array($destacat_array) ? in_array($llistat['id'], $destacat_array) : $llistat['id'] == $destacat_array; //Si son relacionats multiples be donat amb array, si es simple ve donat amb string
                        if ($a_destacar) {//si desde POST es RELACIONAT amb aquest llistat
                            //si ja era relacionat no cal fer res, el deixem igual
                            print_r($llistats_relacionats);
                            if (is_null($llistats_relacionats) || !array_key_exists($llistat['id'], $llistats_relacionats)) {//si NO ERA RELACIONAT abans fem INSERT
                                // Vector entrades
                                $sql_data_array = array(
                                    'llistats_id' => $aux_id,
                                    'related_id' => $llistat['id'],
                                    'id_llistats_groups' => $group,
                                    'entered' => 'now()');
                                // Guardem a BD
                                tep_db_perform(TABLE_LLISTATS_RELATED, $sql_data_array);
                            }
                        } else {//llistat NO RELACIONAT
                            //Eliminem el relacionat (si exsitia ja com a relacionat el borra sino no fa res)
                            tep_db_query("delete from " . TABLE_LLISTATS_RELATED . " where llistats_id = " . $aux_id . " AND related_id=" . $llistat['id'] . " AND id_llistats_groups=" . $group);
                        }
                    }
                }
                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] != $_POST['listorder_aux']) {
                    tep_update_order_group($aux_id, $_POST['listorder_aux'], $_POST['listorder'], TABLE_LLISTATS, 'group_id', $_GET['gID']);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'listorder_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['pID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['pID']);
                // Trobar posicio actual
                $order_now = tep_search_item_order_group($aux_id, TABLE_LLISTATS, 'group_id', $_GET['gID']);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order_group($aux_id, $order_now, $pos_aux, TABLE_LLISTATS, 'group_id', $_GET['gID']);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'gID', 'llistat', 'x', 'y'))) . '&llistat=' . $_GET['llistat'] . '&gID=' . $_GET['gID']);
            break;

        case 'active':
        case 'desactive':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['pID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['pID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_LLISTATS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;
        case 'portada':
        case 'desportada':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['pID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['pID']);
                // Nou valor
                $value = (($_GET['action'] == 'desportada') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('portada' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_LLISTATS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            break;
        case 'delete_item':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['pID']) {
                $aux_id = tep_db_prepare_input($_GET['pID']);
                // Borrem entraeda
                delete_item($aux_id);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'x', 'y'))));
            break;
    }
}

// Comprovem si existeixen i es pot escriure en directoris per imatges i descàrregues
if (is_dir(DIR_FS_PORTAL_IMAGE_LLISTATS)) {
    if (!is_writeable(DIR_FS_PORTAL_IMAGE_LLISTATS))
        $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE . ' (' . DIR_FS_PORTAL_IMAGE_LLISTATS . ')', 'error');
} else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST . ' (' . DIR_FS_PORTAL_IMAGE_LLISTATS . ')', 'error');

// Textos
    $titular = WEB_NAME . ' | ' . PAGE_TITLE;
}
?>
