<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>
<?php
if (isset($_GET['pID']) && tep_not_null($_GET['pID'])) {//CAS UPDATE
    $id = $_GET['pID'];
    $item = tep_db_fetch_array(tep_get_llistat_by_id($id));
    $form_action = 'update_item';
} else {//CAS INSERT
    $item = array('id' => '',
        'autor' => '',
        'title' => '',
        'published' => '');
    $form_action = 'insert_item';
}
$itemInfo = new objectInfo($item);
?>
<script type="text/javascript">
    var geocoder;
    var map;
    var infowindow = new google.maps.InfoWindow();

    function load() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(<?php echo ($itemInfo->lat) ? $itemInfo->lat : '41.96612757631878'; ?>,<?php echo ($itemInfo->lon) ? $itemInfo->lon : '2.8184395004063845'; ?>);
        var myOptions = {
            zoom: 13,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("google_map"),
                myOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo ($itemInfo->lat) ? $itemInfo->lat : '41.96612757631878'; ?>,<?php echo ($itemInfo->lon) ? $itemInfo->lon : '2.8184395004063845'; ?>),
            map: map
        });

        //onclick mostrem lat lon
        google.maps.event.addListener(map, 'click', function(event) {
            //lat lon dins input html
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lon').value = event.latLng.lng();

            geocoder.geocode({'latLng': event.latLng, 'region': 'ca'}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        //map.setZoom(11);
                        infowindow.setContent('<div>' + results[1].formatted_address + '</div><div>' + results[3].formatted_address + '</div><div>' + results[4].formatted_address + '</div>');
                        infowindow.open(map, marker);
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });


            // actualitzem posicio                
            var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
            marker.setPosition(latlng);
        });

    }
    $(document).ready(function() {
        load(); // carreguem Google Maps
    });
</script>
<div id="form">
    <?php //FORMULARI  ---------------------------------------- ?>
    <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
    <?php //BOTONS  ---------------------------------------- ?>
    <p class="botonera">
        <?php
        echo '<a href="#" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>


    <?php //camps GENERALS ---------------------------------------- ?>
    <fieldset>
        <label class="width_50_1">
            <span>Ordre</span>
            <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
            <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
        </label>
        <label class="width_50_2">
            <span>Autor</span>
            <input type="text" name="autor" value="<?php echo $itemInfo->autor; ?>" />
        </label>
        <label class="width_50_1">
            <span>Link <em class="help">(Links externs incloure HTTP://)</em></span>
            <input type="text" name="link" value="<?php echo $itemInfo->link; ?>" />
        </label>
        <label class="width_50_2">
            <span>Generic1</span>
            <input type="text" name="generic1" value="<?php echo $itemInfo->generic1; ?>" />
        </label>
        <label class="width_50_1">
            <span>Generic2</span>
            <input type="text" name="generic2" value="<?php echo $itemInfo->generic2; ?>" />
        </label>
        <label class="width_50_2">
            <span>Generic3</span>
            <input type="text" name="generic3" value="<?php echo $itemInfo->generic3; ?>" />
        </label>
        <label class="width_50_1">
            <span>Generic4</span>
            <input type="text" name="generic4" value="<?php echo $itemInfo->generic4; ?>" />
        </label>
        <label class="width_50_2">
            <span>Generic5</span>
            <input type="text" name="generic5" value="<?php echo $itemInfo->generic5; ?>" />
        </label>
        <label class="width_50_1">
            <span>Data Publicació<?php echo $itemInfo->data_inici; ?></span>
            <input type="text"  name="published" value="<?php echo (($itemInfo->published) ? tep_date_db($itemInfo->published) : date("Y-m-d")); ?>" class="camp_read date_input" />
            <a class="link_cal"></a>
        </label>
        <p class="form_sep"></p>
    </fieldset>

    <?php //camp CHECKBOX ---------------------------------------- ?>
    <h4><span >Checkboxes</span></h4>
    <fieldset>
        <label class="width_50_1">
            <span>Inscripcions</span>
            <input type="checkbox" name="inscripcions" value="1" <?php echo ($itemInfo->inscripcions) ? 'checked="checked"' : ''; ?> class="checkbox" />
        </label>
    </fieldset>

    <?php //camps LLISTATS RELACIONATS SIMPLE---------------------------------------- ?>
    <h4><span >Relacionats SIMPLE</span></h4>
    <fieldset>
        <label class="width_50_1">
            <span>Llistat relacionat1</span>
            <?php $idGrupRelacionat1 = 4; //canviar per el grup que volem relacionar?>
            <input type="hidden" name="id_llistats_groups[<?php echo $idGrupRelacionat1; ?>]" value="<?php echo $idGrupRelacionat1; ?>"/>
            <?php
            $relacionats_actuals1 = tep_get_llistats_relacionats_by_group($itemInfo->id, $idGrupRelacionat1); //obtenim llistats (ID TIPUS) relacionats amb l'actual
            echo tep_draw_pull_down_menu('related_id[' . $idGrupRelacionat1 . ']', tep_get_llistats_array_by_id_group($idGrupRelacionat1, 'Seleccionar...'), current(array_keys($relacionats_actuals1)), 'id="' . $idGrupRelacionat1 . '"');
            ?>
        </label>
        <label class="width_50_2">
            <span>Llistat relacionat2</span>
            <?php $idGrupRelacionat2 = 2; //canviar per el grup que volem relacionar?>
            <input type="hidden" name="id_llistats_groups[<?php echo $idGrupRelacionat2; ?>]" value="<?php echo $idGrupRelacionat2; ?>"/>
            <?php
            $relacionats_actuals2 = tep_get_llistats_relacionats_by_group($itemInfo->id, $idGrupRelacionat2); //obtenim llistats (ID TIPUS) relacionats amb l'actual
            echo tep_draw_pull_down_menu('related_id[' . $idGrupRelacionat2 . ']', tep_get_llistats_array_by_id_group($idGrupRelacionat2, 'Seleccionar...'), current(array_keys($relacionats_actuals2)), 'id="' . $idGrupRelacionat2 . '"');
            ?>
        </label>
        <!-- SI TENIM SUBCATEGORIES 
                <label class="width_50_1">
            <span>Client</span>
            <?php $idGrupRelacionat1 = 4; //canviar per el grup que volem relacionar?>
            <input type="hidden" name="id_llistats_groups[<?php echo $idGrupRelacionat1; ?>]" value="<?php echo $idGrupRelacionat1; ?>"/>
            <?php
            $grupsFillsRelacionats = tep_get_llistat_groups_by_parent_id($idGrupRelacionat1); //obtenim les categories filles
            $relacionats_actuals1 = tep_get_llistats_relacionats_by_group($itemInfo->id, $idGrupRelacionat1); //obtenim llistats (ID TIPUS) relacionats amb l'actual

            $llistats[0] = array('id' => '0', 'text' => 'Seleccionar...');
            while ($grupFill = tep_db_fetch_array($grupsFillsRelacionats)) {//recorrem els grups fills
                $llistats = array_merge($llistats, tep_get_llistats_array_by_id_group($grupFill['id'])); //anem afegint tots els llistats de cada grup fill
            }
            ordenar_multiarray($llistats, 'text');
            echo tep_draw_pull_down_menu('related_id[' . $idGrupRelacionat1 . ']', $llistats, current(array_keys($relacionats_actuals1)), 'id="' . $idGrupRelacionat1 . '"');
            ?>
        </label>-->
    </fieldset>



    <?php //camps LLISTATS RELACIONATS MULTIPLES----------------------------------------  ?>
    <h4><span >Relacionats MULTIPLES</span></h4>
    <fieldset>

        <?php
        //-----------tipus checkboxes
        $idGrupRelacionat = 4; //canviar per el grup que volem relacionar
        $itemsRel = tep_get_llistats_array_by_id_group($idGrupRelacionat);
        ?>
        <input type="hidden" name="id_llistats_groups[<?php echo $idGrupRelacionat; ?>]" value="<?php echo $idGrupRelacionat; ?>"/>
        <?php
        foreach ($itemsRel as $itemRel) {
            $relacionats_actuals = tep_get_llistats_relacionats_by_group($itemInfo->id, $idGrupRelacionat); //obtenim llistats (ID TIPUS) relacionats amb l'actual
            //print_r($relacionats_actuals);
            ?>
            <label class="width_50_2" >
                <input type="checkbox" name="related_id[<?php echo $idGrupRelacionat; ?>][<?php echo $itemRel['id']; ?>]" value="<?php echo $itemRel['id']; ?>" <?php echo ($relacionats_actuals[$itemRel['id']]) ? 'checked="checked"' : ''; ?> class="checkbox" />
                <span class="titol_destacat"><strong><?php echo $itemRel['text']; ?></strong></span>
            </label>
        <?php } ?>

    </fieldset>

    <?php //camps GOOGLE MAPS ----------------------------------------  ?>
    <fieldset>
        <div id="google_map"></div>
        <label class="width_50_1">
            <span>Latitud</span>
            <input type="text" name="lat" id="lat" value="<?php echo $itemInfo->lat; ?>" />
        </label>
        <label class="width_50_2">
            <span>Longitud<?php echo $itemInfo->data_inici; ?></span>
            <input type="text"  name="lon" id="lon" value="<?php echo $itemInfo->lon; ?>" />
        </label>
    </fieldset>



    <?php //camps IDIOMES ----------------------------------------  ?>
    <?php
    if (sizeof($languages) > 1) {
        ?>
        <ul id="menuIdioma">
            <?php
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
            }
            ?>
        </ul>
        <?php
    }
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        // Busquem valors
        $aux_listing = tep_db_query("select titol,titol2,titol3, descripcio,descripcio2,descripcio3,download from " . TABLE_LLISTATS_DESCRIPTION . " where llistats_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
        $aux_numrows = tep_db_num_rows($aux_listing);
        if ($aux_numrows > 0) {
            $aux_item = tep_db_fetch_array($aux_listing);
            $auxInfo = new objectInfo($aux_item);
        }
        ?>
        <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
            <label>
                <span >Titol</span>
                <input type="text" name="titol[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->titol; ?>" />
            </label>
            <label class="width_50_1">
                <span >Titol2</span>
                <input type="text" name="titol2[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->titol2; ?>" />
            </label>
            <label class="width_50_2">
                <span >Titol3</span>
                <input type="text" name="titol3[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->titol3; ?>" />
            </label>
            <div class="clear top_15"></div>

            <span >Descripció</span>
            <textarea name="descripcio[<?php echo $languages[$i]['id']; ?>]" id="descripcio[<?php echo $languages[$i]['id']; ?>]" >
                <?php echo $auxInfo->descripcio; ?>
            </textarea>
            <script>
                <?php 
                if (!tep_usuaris_es_admin())
                { 
                    echo("CKEDITOR.config.toolbar = 'Editor';");                     
                }
                ?>
                CKEDITOR.replace( 'descripcio[<?php echo $languages[$i]['id']; ?>]');
            </script>
            
            <span >Descripció 2</span>
            <textarea name="descripcio2[<?php echo $languages[$i]['id']; ?>]" id="descripcio2[<?php echo $languages[$i]['id']; ?>]" >
                <?php echo $auxInfo->descripcio2; ?>
            </textarea>
            <script>
                <?php 
                if (!tep_usuaris_es_admin())
                { 
                    echo("CKEDITOR.config.toolbar = 'Editor';");                     
                }
                ?>
                CKEDITOR.replace( 'descripcio2[<?php echo $languages[$i]['id']; ?>]');
            </script>
            
            <span >Descripció 3</span>
            <textarea name="descripcio3[<?php echo $languages[$i]['id']; ?>]" id="descripcio3[<?php echo $languages[$i]['id']; ?>]" >
                <?php echo $auxInfo->descripcio3; ?>
            </textarea>
            <script>
                <?php 
                if (!tep_usuaris_es_admin())
                { 
                    echo("CKEDITOR.config.toolbar = 'Editor';");                     
                }
                ?>
                CKEDITOR.replace( 'descripcio3[<?php echo $languages[$i]['id']; ?>]');
            </script>

            <?php
//            $params_ckeditor = array('height' => '500');
//            if (tep_usuaris_es_admin()) {
//                $params_ckeditor['toolbar'] = 'Full';
//            }
//            $ckeditor->replace('descripcio[' . $languages[$i]['id'] . ']', $params_ckeditor);
            ?>
            <?php
//            $params_ckeditor = array('height' => '500');
//            if (tep_usuaris_es_admin()) {
//                $params_ckeditor['toolbar'] = 'Full';
//            }
//            $ckeditor->replace('descripcio2[' . $languages[$i]['id'] . ']', $params_ckeditor);
            ?>
            <?php
//            $params_ckeditor = array('height' => '500');
//            if (tep_usuaris_es_admin()) {
//                $params_ckeditor['toolbar'] = 'Full';
//            }
//            $ckeditor->replace('descripcio3[' . $languages[$i]['id'] . ']', $params_ckeditor);
            ?>

            <p class="form_sep"></p>
            <?php
            if (tep_not_null($auxInfo->download)) {
                echo tep_draw_hidden_field('download_prev[' . $languages[$i]['id'] . ']', $auxInfo->download) . "\n";
                echo '<label class="width_50_1"><span>Descàrrega Actual</span><input type="text" name="download_act[' . $languages[$i]['id'] . ']" value="' . $auxInfo->download . '" class="camp_read" readonly="readonly" />' . "\n"
                . '<a href="javascript:void(0);" class="link_del"></a>' . "\n"
                . '<a href="' . DIR_WS_PORTAL_DOWNLOADS . $auxInfo->download . '" title="' . $auxInfo->download . '" class="link_view" target="_blank"></a></label>' . "\n"
                . '<label class="width_50_2"><span>Nova Descàrrega</span><input type="file" name="download_new[' . $languages[$i]['id'] . ']" value="" size="45" class="file" /></label>' . "\n";
            } else {
                echo '<label class="width_50_1"><span>Descàrrega</span><input type="file" name="download_new[' . $languages[$i]['id'] . ']" value="" size="45" class="file" /></label>' . "\n";
            }
            ?>
        </fieldset>
        <?php
    }
    ?>
    <h4>Galeria d'Imatges</h4>
    <fieldset>
        <label class="width_50_1">
            <div id="continent_select_galeries">
                <?php echo tep_draw_pull_down_menu('fotos_groups_id', tep_get_fotos_groups_array(FOTOS_GROUP_PROJECTES, '--'), $itemInfo->fotos_groups_id, 'id="fotos_groups_id"'); ?>
            </div>
        </label>
        <label class="width_50_2">
            <div >
                <a class="nova_galeria" id="nova_galeria" onclick=" return novaGaleria('<?php echo FOTOS_GROUP_PROJECTES; ?>');" href="#" title="Crear una nova galeria">NOVA GALERIA <span class="container_loading"><img style="display: none;margin-top:5px;" id="loading_galeria" src="image/loading_linia.gif" alt="loading"/></span></a>
            </div>
        </label>
        <?php
        //mirem si tenim associada alguna galeria
        $src = !is_null($itemInfo->fotos_groups_id) && tep_galeria_exists($itemInfo->fotos_groups_id) ? '?modul=fotos_fotos&group_id=' . $itemInfo->fotos_groups_id . '&iframe=si' : 'about:blank';
        ?>

        <iframe scrolling="no" frameborder=0 border=0 id="childframe" name="childframe" src="<?php echo $src; ?>"></iframe>
    </fieldset>

    <?php //BOTONS  ----------------------------------------  ?>
    <p class="botonera">
        <?php
        echo '<a href="#" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>
</form>
</div>


