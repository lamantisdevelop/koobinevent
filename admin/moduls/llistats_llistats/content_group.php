<?php
/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */
?>

<?php
if (($_GET['gID'])) {
    $id = $_GET['gID'];
    $query_raw = "select bg.name, bg.listorder,bg.parent_id,bg.fotos_groups_id from " . TABLE_LLISTATS_GROUPS . " bg where bg.id = '" . (int) $id . "'";
    $item = tep_db_fetch_array(tep_db_query($query_raw));
} else {
    $item = array('name' => '',
        'listorder' => '');
}
$itemInfo = new objectInfo($item);
$form_action = ($_GET['gID']) ? 'update_group' : 'insert_group';
?>

<div id="form">
    <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
    <p class="botonera">
        <?php
        echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>
    <fieldset >
        <input type="hidden" name="parent_id" value="<?php echo isset($_GET['parent_id']) ? $_GET['parent_id'] : $itemInfo->parent_id; ?>" />

        <label class="width_50_1">
            <span>Nom</span>
            <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
        </label>
        <label class="width_50_2">
            <span>Ordre</span>
            <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
            <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
        </label>
    </fieldset>
    <!-- Idiomes -->
    <?php
    if (sizeof($languages) > 1) {
        echo '<ul id="menuIdioma">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
        }
        echo '</ul>';
    }

    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        // Busquem valors
        $aux_listing = tep_db_query("select description, target, title, class, rel, rev from " . TABLE_LLISTATS_GROUPS_DESCRIPTION . " where groups_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
        $aux_numrows = tep_db_num_rows($aux_listing);
        if ($aux_numrows > 0) {
            $aux_item = tep_db_fetch_array($aux_listing);
            $auxInfo = new objectInfo($aux_item);
        }
        ?>               
        <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
            <p class="form_sep"></p>
            <label class="width_50">
                <span>Titol</span>
                <input type="text" name="title[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->title; ?>" />
            </label>

            <div class="clear top_15"></div>
            <label class="width_50">
                <span>Descripció</span>
            </label> 
            <textarea name="description[<?php echo $languages[$i]['id']; ?>]" id="description[<?php echo $languages[$i]['id']; ?>]" <?php echo (($itemInfo->action == 'content') ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                <?php echo $auxInfo->description; ?>
            </textarea>
            <script>
                <?php 
                if (!tep_usuaris_es_admin())
                { 
                    echo("CKEDITOR.config.toolbar = 'Editor';");                     
                }
                ?>
                CKEDITOR.replace( 'description[<?php echo $languages[$i]['id']; ?>]');
            </script>

            <?php
//            $params_ckeditor = array('height' => '500');
//            if (tep_usuaris_es_admin()) {
//                $params_ckeditor['toolbar'] = 'Full';
//            }
//            $ckeditor->replace('description[' . $languages[$i]['id'] . ']', $params_ckeditor);
            ?>

            <div class="clear top_15"></div>
            <label class="width_50_1">
                <span>Target*</span>
                <?php echo tep_draw_pull_down_menu('target[' . $languages[$i]['id'] . ']', $target_array, $auxInfo->target, ''); ?>
            </label>
            <label class="width_50_2">
                <span>Class</span>
                <input type="text" name="class[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->class; ?>" />
            </label>
            <label class="width_50_1">
                <span>Rel</span>
                <input type="text" name="rel[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rel; ?>" />
            </label>
            <label class="width_50_2">
                <span>Rev</span>
                <input type="text" name="rev[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rev; ?>" />
            </label>
        </fieldset>
        <?php
    }
    ?>

    <h4>Galeria d'Imatges</h4>
    <fieldset>
        <label class="width_50_1">
            <div id="continent_select_galeries">
                <?php echo tep_draw_pull_down_menu('fotos_groups_id', tep_get_fotos_groups_array(FOTOS_GROUP_CATEGORIES, '--'), $itemInfo->fotos_groups_id, 'id="fotos_groups_id"'); ?>
            </div>
        </label>
        <label class="width_50_2">
            <div >
                <a class="nova_galeria" id="nova_galeria" onclick=" return novaGaleria('<?php echo FOTOS_GROUP_CATEGORIES; ?>');" href="#" title="Crear una nova galeria">NOVA GALERIA <span class="container_loading"><img style="display: none;margin-top:5px;" id="loading_galeria" src="image/loading_linia.gif" alt="loading"/></span></a>
            </div>
        </label>
        <?php
        //mirem si tenim associada alguna galeria
        $src = !is_null($itemInfo->fotos_groups_id) && tep_galeria_exists($itemInfo->fotos_groups_id) ? '?modul=fotos_fotos&group_id=' . $itemInfo->fotos_groups_id . '&iframe=si' : 'about:blank';
        ?>

        <iframe scrolling="no" frameborder=0 border=0 id="childframe" name="childframe" src="<?php echo $src; ?>"></iframe>
    </fieldset>

    <p class="botonera">
        <?php
        echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
        ?>
    </p>
</form>
</div>

