<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

// JQuery hide/show tags
$(document).ready(function() {
    $('label.tab').click(function () {
      $('.tab_content').hide();

      var mostra = $(this).attr('title');
      $('.'+mostra).show();
    });
});


// Validar formulari
function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}                    

//]]>

 //var codemirror_rootpath = 'js/'; 
</script>
