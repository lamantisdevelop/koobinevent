<h2 class="pages">
    <?php
    echo TITLE_PAGE;
    if ($_GET['action'] == 'new') {
        echo ': <b>'.tep_get_page_titol($_GET['pID']).'</b>';
    }
    ?></h2>
<?php
if ($_GET['action'] == 'new') {
    // Fitxa
    if (isset($_GET['pID']) && tep_not_null($_GET['pID'])) {
        $id = $_GET['pID'];
        $query_raw = "select id, parent_id,banners_id,fotos_groups_id, action, listorder from " . TABLE_PAGES . " where id = '" . (int) $id . "'";
        $item = tep_db_fetch_array(tep_db_query($query_raw));
    } else {
        $item = array('id' => '',
            'parent_id' => '',
            'action' => '');
    }
    $itemInfo = new objectInfo($item);

    $form_action = ($_GET['pID']) ? 'update' : 'insert';
    ?>

    <div id="form">
        <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data" accept-charset="UTF-8"'); ?>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a class="enrere" href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
        <fieldset>
            <label class="width_50_1">
                <span>Parent*</span>
                <?php echo tep_draw_pull_down_menu('parent_id', tep_get_pages_tree(), $itemInfo->parent_id, 'class="llarg"'); ?>
            </label>
            <label class="width_50_2">
                <span>Ordre</span>
                <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
            </label>
            <p class="form_sep"></p>
            <?php echo!tep_usuaris_es_admin() ? '<div style="display:none;">' : ''; //no mostrem per no admins?>
            <label>
                <span>Accio:</span>
            </label>
            <label class="inline tab" title="action_1">
                <input type="radio" name="action" value="no" class="radio" <?php echo (($itemInfo->action == 'no') ? 'checked="checked"' : ''); ?> /><span class="inline">Sense Acció</span>
            </label>
            <label class="inline tab" title="action_2">
                <input type="radio" name="action" value="link" class="radio" <?php echo (($itemInfo->action == 'link') ? 'checked="checked"' : ''); ?> /><span class="inline">Link</span>
            </label>
            <label class="inline tab" title="action_3">
                <input type="radio" name="action" value="modul" class="radio" <?php echo (($itemInfo->action == 'modul') ? 'checked="checked"' : ''); ?> /><span class="inline">Mòdul</span>
            </label>
            <label class="inline tab" title="action_4">
                <input type="radio" name="action" value="content" class="radio" <?php echo (!tep_not_null($itemInfo->action) || ($itemInfo->action == 'content') ? 'checked="checked"' : ''); ?> /><span class="inline">Contingut</span>
            </label>
            <p class="form_sep"></p>
            <label>
                <input type="checkbox" name="default" value="on" <?php echo (($itemInfo->id == DEFAULT_PAGE) ? 'checked="checked"' : ''); ?> class="checkbox" />
                <span class="inline">Establir com a pàgina d'inici.</span>
            </label>
            <?php echo!tep_usuaris_es_admin() ? '<div/>' : ''; ?>
        </fieldset>
        <!-- Idiomes -->
        <?php
        if (sizeof($languages) > 1) {
            ?>
            <ul id="menuIdioma">
                <?php
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
                }
                ?>
            </ul>
            <?php
        }
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            // Busquem valors
            $aux_listing = tep_db_query("select titol, page_title, link, target, modul, vars, content, title, keywords, description from " . TABLE_PAGES_DESCRIPTION . " where pages_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
            $aux_numrows = tep_db_num_rows($aux_listing);
            if ($aux_numrows > 0) {
                $aux_item = tep_db_fetch_array($aux_listing);
                $auxInfo = new objectInfo($aux_item);
            }
            ?>
            <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                <label class="width_50_1">
                    <span>Titol*</span>
                    <input type="text" name="titol[<?php echo $languages[$i]['id']; ?>]" value="<?php echo htmlentities($auxInfo->titol, ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?>" />
                </label>
                <label class="width_50_2">
                    <span>SEO Titol</span>
                    <input type="text" name="page_title[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->page_title; ?>" />
                </label>
                <div class="tab_content action_2" <?php echo (($itemInfo->action == 'link') ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                    <label class="width_50_1">
                        <span>Link</span>
                        <input type="text" name="link[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->link; ?>" />
                    </label>
                    <label class="width_50_2">
                        <span>Target</span>
                        <?php echo tep_draw_pull_down_menu('target[' . $languages[$i]['id'] . ']', $target_array, $auxInfo->target, ''); ?>
                    </label>
                </div>
                <div class="tab_content action_3" <?php echo ((tep_usuaris_es_admin() && $itemInfo->action == 'modul') ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                    <label class="width_50_1">
                        <span>Modul</span>
                        <input type="text" name="modul[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->modul; ?>" />
                    </label>
                    <label class="width_50_2">
                        <span>Variables</span>
                        <input type="text" name="vars[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->vars; ?>" />
                    </label>
                </div>
                <div class="clear"></div>
                <div >
                    <textarea name="content[<?php echo $languages[$i]['id']; ?>]" id="content[<?php echo $languages[$i]['id']; ?>]">
                        <?php echo $auxInfo->content; ?>
                    </textarea>
                    <script>
                        <?php 
                        if (!tep_usuaris_es_admin())
                        { 
                            echo("CKEDITOR.config.toolbar = 'Editor';");                     
                        }
                        ?>
                        CKEDITOR.replace( 'content[<?php echo $languages[$i]['id']; ?>]');
                    </script>

                    <?php
//                    $params_ckeditor = array('height' => '500');
//                    if (tep_usuaris_es_admin()) {
//                        $params_ckeditor['toolbar'] = 'Full';
//                    }
//                    $ckeditor->replace('content[' . $languages[$i]['id'] . ']', $params_ckeditor);
                    ?>             

                </div>

                <p class="form_sep"></p>
                <a class="mostrar" rel="<?php echo 'metas' . $i; ?>">Meta Tags</a>
                <div id="<?php echo 'metas' . $i; ?>" class="">
                    <label>
                        <span>Title</span>
                        <input type="text" name="title[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->title; ?>" />
                    </label>
                    <label>
                        <span>Keywords</span>
                        <textarea name="keywords[<?php echo $languages[$i]['id']; ?>]" class="info" rows="10" cols="10"><?php echo $auxInfo->keywords; ?></textarea>
                    </label>
                    <label>
                        <span>Description</span>
                        <textarea name="description[<?php echo $languages[$i]['id']; ?>]" class="info" rows="10" cols="10"><?php echo $auxInfo->description; ?></textarea>
                    </label>
                </div>

            </fieldset>
            <?php
        }
        ?>
        
        <?php 
        if(tep_usuaris_es_admin())
        {
            ?>
            <h4 class="desplegable collapsed"><span class="media">Galeria d'Imatges</span></h4>
            <fieldset>
                <label class="width_50_1">
                    <div id="continent_select_galeries">
                        <?php echo tep_draw_pull_down_menu('fotos_groups_id', tep_get_fotos_groups_array(FOTOS_GROUP_TOTES, 'Seleccionar...'), $itemInfo->fotos_groups_id, 'id="fotos_groups_id"'); ?>
                    </div>
                </label>
                <label class="width_50_2">
                    <div class="botonera">
                        <a class="nova_galeria" id="nova_galeria" onclick="return novaGaleria('<?php echo FOTOS_GROUP_PAGES; ?>');" href="#" title="Crear una nova galeria">NOVA GALERIA <span class="container_loading"><img style="display: none;margin-top:5px;" id="loading_galeria" src="image/loading_linia.gif" alt="loading"/></span></a>
                    </div>
                </label>
                <?php
                //mirem si tenim associada alguna galeria
                $src = !is_null($itemInfo->fotos_groups_id) && tep_galeria_exists($itemInfo->fotos_groups_id) ? '?modul=fotos_fotos&group_id=' . $itemInfo->fotos_groups_id . '&iframe=si' : 'about:blank';
                ?>

                <iframe scrolling="no" frameborder=0 border=0 id="childframe" name="childframe" src="<?php echo $src; ?>"></iframe>
            </fieldset>
            <?php 
        }
        ?>
        
        <h4 class="desplegable collapsed"><span class="media">Slide</span></h4>
        <fieldset>
            <label class="width_50_1">
                <div id="continent_select_galeries_premsa">
                    <?php echo tep_draw_pull_down_menu('banners_id', tep_get_fotos_groups_array(FOTOS_GROUP_SLIDESHOW, 'Seleccionar...'), $itemInfo->banners_id, 'id="fotos_groups_id_premsa"'); ?>
                </div>
            </label>
            <label class="width_50_2">
                <div class="botonera" >
                    <a class="nova_galeria" id="nova_galeria_premsa" onclick="return novaGaleriaPremsa('<?php echo FOTOS_GROUP_SLIDESHOW; ?>');" href="#" title="Crear una nova galeria">NOVA GALERIA <span class="container_loading"><img style="display: none;margin-top:5px;" id="loading_galeria_premsa" src="image/loading_linia.gif" alt="loading"/></span></a>
                </div>
            </label>
            <?php
            //mirem si tenim associada alguna galeria
            $src = !is_null($itemInfo->banners_id) && tep_galeria_exists($itemInfo->banners_id) ? '?modul=fotos_fotos&group_id=' . $itemInfo->banners_id . '&iframe=si&idLlistat=premsa' : 'about:blank';
            ?>
            <iframe scrolling="no" frameborder=0 border=0 id="childframe_premsa" name="childframe_premsa" src="<?php echo $src; ?>"></iframe>
        </fieldset>
        
        
        <?php
        $history_query = tep_db_query("select summary, ip, date, detail, systeminfo from " . TABLE_PAGES_HISTORY . " where pages_id = '" . (int) $id . "' order by date desc");
        if (tep_db_num_rows($history_query) > 0) {
            ?>
            <a class="mostrar" rel="historial">Historial</a>
            <table id="historial" summary="valors per opcions producte"  style="display:none;" class="taula fixed">
                <tbody>
                    <?php
                    while ($history = tep_db_fetch_array($history_query)) {
                        $historyInfo = new objectInfo($history);

                        echo '<tr>'
                        . '<td style="width:20%;">' . tep_date_long($historyInfo->date) . '</td>'
                        . '<td style="width:20%;">' . $historyInfo->ip . ' | ' . tep_get_agent_name($historyInfo->systeminfo) . '</td>'
                        . '<td style="width:60%;">' . $historyInfo->summary . '</td>'
                        . '</tr>';
                    }
                    ?>
                </tbody>
            </table>
            <?php
        }
        ?>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a class="enrere" href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
    </form>
    </div>

    <?php
} else {
    ?>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>
    <table summary="<?php echo TAULA; ?>" class="llista fixed">
        <thead>
            <tr>
                <th style="width:55%;"><?php echo COL_1; ?></th>
                <th style="width:10%;"><?php echo COL_2; ?></th>
                <th style="width:10%; text-align:center;"><?php echo COL_3; ?></th>
                <th style="width:10%; text-align:center;"><?php echo COL_4; ?></th>
                <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="5">
        <div id="split_left">
            <?php
            // Num pÃ gines
            $pages = tep_db_fetch_array(tep_db_query("select count(distinct id) as num from " . TABLE_PAGES . ""));
            echo @sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS, $pages['num'])
            ?>
        </div>
    </th>
    </tr>
    </tfoot>
    <tbody>

        <?php

// Obtenir arbre categories
        function tep_get_pages_table($parent_id = '0', $spacing = '', $exclude = '', $pages_table = '', $include_itself = false) {
            global $_GET, $language_id, $aux;

            $pages_query = tep_db_query("select p.id, pd.titol, p.parent_id, p.action, p.active,  p.listorder from " . TABLE_PAGES . " p left join  " . TABLE_PAGES_DESCRIPTION . " pd on p.id = pd.pages_id and pd.language_id = '" . $language_id . "'where p.parent_id = '" . $parent_id . "' order by p.listorder, pd.titol");
            while ($pages = tep_db_fetch_array($pages_query)) {
                $itemInfo = new objectInfo($pages);

                $aux = ($aux % 2) + 1;

                if ($itemInfo->active) {
                    $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=desactive&amp;pID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                } else {
                    $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=active&amp;pID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                }

                $pages_table .= '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=new&amp;pID=' . $itemInfo->id) . '\'" class="' . (($itemInfo->id == $_GET['pID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                        . '  <td class="nom">' . (($itemInfo->id == DEFAULT_PAGE) ? ICON_DEFAULT : '') . (($spacing != '') ? $spacing . '<span class="separador">&rarr;</span>' : '') . htmlentities($itemInfo->titol, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</td>' . "\n"
                        . '  <td>' . (($spacing != '') ? $spacing . '<span class="separador">&rarr;</span>' : '') . $itemInfo->listorder . '</td>' . "\n"
                        . '  <td style="text-align:center;">' . $itemInfo->action . '</td>' . "\n"
                        . '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
                        . '  <td style="text-align:center;" class="accions">'
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=new&amp;pID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a>';
                if ($itemInfo->action == 'content' || ($itemInfo->action == 'modul' && tep_usuaris_es_admin())) { //els mòduls només els pot borrar un admin
                    $pages_table .= ' <a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'buscar', 'x', 'y')) . 'action=delete&amp;pID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>';
                }
                $pages_table .= '</td>' . "\n"
                        . '</tr>' . "\n";

                $pages_table .= tep_get_pages_table($pages['id'], $spacing . '<span class="espai">&nbsp;</span>');
            }
            return $pages_table;
        }

        $taula = tep_get_pages_table('0');
        if (tep_not_null($taula)) {
            echo $taula;
        } else {
            // No Items
            echo '<tr>' . "\n";
            echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
            echo '</tr>' . "\n";
        }
        ?>

    </tbody>
    </table>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>

    <?php
}
?>
