<?php

// Creem Array Idiomes
$languages = tep_get_languages();

// Definim vector per selecció target
$target_array = array(array('id' => '_blank', 'text' => '_blank'),
    array('id' => '_self', 'text' => '_self'));

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
        case 'insert':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Vector entrades
                $sql_data_array = array('parent_id' => tep_db_prepare_input($_POST['parent_id']),
                    'action' => tep_db_prepare_input($_POST['action']),
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
                    'banners_id' => tep_db_prepare_input($_POST['banners_id']),
                    'active' => '1',
                    'listorder' => tep_db_prepare_input($_POST['listorder']),
                    'entered' => 'now()',
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_PAGES, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Entrades segons idioma
                $titol_array = $_POST['titol'];
                $link_array = $_POST['link'];
                $target_array = $_POST['target'];
                $modul_array = $_POST['modul'];
                $vars_array = $_POST['vars'];
                $content_array = $_POST['content'];
                $title_array = $_POST['title'];
                $page_title_array = $_POST['page_title'];
                $keywords_array = $_POST['keywords'];
                $description_array = $_POST['description'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Title, no buit
                    if (!tep_not_null($title_array[$lang_aux])) {
                        $title_array[$lang_aux] = $titol_array[$lang_aux];
                    }

                    // Page title: diferent per tots els idiomes, si és buit l'agafem a partir del títol de la pàgina
                    if (tep_not_null($page_title_array[$lang_aux])) {
                        $page_title = friendly_param($page_title_array[$lang_aux]);
                    } else {
                        $page_title = friendly_param($titol_array[$lang_aux]);
                    }
                    $page_title = rename_page_if_exists($aux_id, $page_title, $lang_aux);

                    // Insertem dades
                    $sql_data_array = array('pages_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'titol' => tep_db_prepare_input($titol_array[$lang_aux]),
                        'link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'modul' => tep_db_prepare_input($modul_array[$lang_aux]),
                        'vars' => tep_db_prepare_input($vars_array[$lang_aux]),
                        'content' => tep_db_prepare_input($content_array[$lang_aux]),
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'page_title' => tep_db_prepare_input($page_title),
                        'keywords' => tep_db_prepare_input($keywords_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]));
                    // Crida base dades
                    tep_db_perform(TABLE_PAGES_DESCRIPTION, $sql_data_array);
                }

                // Pàgina x defecte
                if (isset($_POST['default']) && ($_POST['default'] == 'on')) {
                    tep_db_query("update " . TABLE_CONFIG_VALUES . " set value = '" . (int) $aux_id . "' where item = 'DEFAULT_PAGE'");
                }

                // Historial
                tep_pages_history_insert($aux_id, $_GET['action'] . " by " . tep_get_admin_name($customer_id));
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'pID=' . $aux_id));
            break;

        case 'update':
            //print_r($_POST);die();
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['pID'] && $_POST) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['pID']);
                // Vector entrades
                $sql_data_array = array('parent_id' => tep_db_prepare_input($_POST['parent_id']),
                    'action' => tep_db_prepare_input($_POST['action']),
                    'listorder' => tep_db_prepare_input($_POST['listorder']),
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
                    'banners_id' => tep_db_prepare_input($_POST['banners_id']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_PAGES, $sql_data_array, 'update', 'id = \'' . (int) $aux_id . '\'');

                // Entrades segons idioma
                $titol_array = $_POST['titol'];
                $link_array = $_POST['link'];
                $target_array = $_POST['target'];
                $modul_array = $_POST['modul'];
                $vars_array = $_POST['vars'];
                $content_array = $_POST['content'];
                $title_array = $_POST['title'];
                $page_title_array = $_POST['page_title'];
                $keywords_array = $_POST['keywords'];
                $description_array = $_POST['description'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Title, no buit
                    if (!tep_not_null($title_array[$lang_aux])) {
                        $title_array[$lang_aux] = $titol_array[$lang_aux];
                    }

                    // Page title: diferent per tots els idiomes, si és buit l'agafem a partir del títol de la pàgina
                    if (tep_not_null($page_title_array[$lang_aux])) {
                        $page_title = friendly_param($page_title_array[$lang_aux]);
                    } else {
                        $page_title = friendly_param($titol_array[$lang_aux]);
                    }
                    $page_title = rename_page_if_exists($aux_id, $page_title, $lang_aux);

                    // Insertem dades
                    $sql_data_array = array('pages_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux, 'titol' => tep_db_prepare_input($titol_array[$lang_aux]),
                        'link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'modul' => tep_db_prepare_input($modul_array[$lang_aux]),
                        'vars' => tep_db_prepare_input($vars_array[$lang_aux]),
                        'content' => tep_db_prepare_input($content_array[$lang_aux]),
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'page_title' => tep_db_prepare_input($page_title),
                        'keywords' => tep_db_prepare_input($keywords_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]));
                    // Crida base dades
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT pages_id FROM " . TABLE_PAGES_DESCRIPTION . " WHERE pages_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_PAGES_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_PAGES_DESCRIPTION, $sql_data_array, 'update', 'pages_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }

                // Pàgina x defecte
                if (isset($_POST['default']) && ($_POST['default'] == 'on')) {
                    tep_db_query("update " . TABLE_CONFIG_VALUES . " set value = '" . (int) $aux_id . "' where item = 'DEFAULT_PAGE'");
                }

                // Historial Pàgines
                tep_pages_history_insert($aux_id, $_GET['action'] . " by " . tep_get_admin_name($customer_id));
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'active':
        case 'desactive':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['pID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['pID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_PAGES, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');

                // Historial Pàgines
                tep_pages_history_insert($aux_id, $_GET['action'] . " by " . tep_get_admin_name($customer_id));
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'delete':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['pID']) {
                $aux_id = tep_db_prepare_input($_GET['pID']);
                // Si ho està error no la deixem borrar
                if ($aux_id == DEFAULT_PAGE) {
                    $messageStack->add_session(ERROR_REMOVE_DEFAULT, 'error');
                    tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
                } else {
                    // Comprovem que no esxisteixen pàgines enllaçades
                    $pages_query = tep_db_query("select id from " . TABLE_PAGES . " where parent_id = '" . (int) $aux_id . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows > 0) {
                        // Redirecció
                        $messageStack->add_session(ERROR_PAGES_CHILDS, 'error');
                        tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
                    } else {
                        // Eliminem entrada
                        tep_db_query("delete from " . TABLE_PAGES . " where id = '" . (int) $aux_id . "'");
                        tep_db_query("delete from " . TABLE_PAGES_DESCRIPTION . " where pages_id = '" . (int) $aux_id . "'");
                    }
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'pID', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
