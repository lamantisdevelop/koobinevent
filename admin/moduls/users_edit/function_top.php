<?php

// Enviament Mails
require_once(DIR_WS_CLASSES . 'class.phpmailer.php');
require_once(DIR_WS_CLASSES . 'class.smtp.php');

// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
        case 'update':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA); 
            if ($_GET['uID'] && $_POST['name']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['uID']);

                // Comprovem que no existeixi un usuari amb el mateix mail
                $user_query = tep_db_query("select count(*) as total from " . TABLE_ADMIN_USERS . " where mail = '" . tep_db_prepare_input($_POST['mail']) . "' and id!='" . (int) $aux_id . "'");
                $user_mail = tep_db_fetch_array($user_query);
                $aux_duplicate = (int) $user_mail['total'];

                if ($aux_duplicate == 0) {
                    // Vector entrades
                    $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                        'mail' => tep_db_prepare_input($_POST['mail']),
                        'modified' => 'now()');
                    // Password si s'ha introduit
                    if (tep_not_null($_POST['password'])) {
                        $sql_data_array['password'] = tep_encrypt_password($_POST['password']);
                    }
                    // Crida base dades
                    tep_db_perform(TABLE_ADMIN_USERS, $sql_data_array, 'update', "id = '" . (int) $aux_id . "'");
                    $messageStack->add_session(sprintf(TEXT_USER_UPDATED, $_POST['name']), 'success');
                } else {
                    $messageStack->add_session(sprintf(TEXT_EXISTENT_USER, $_POST['mail']), 'error');
                    tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y'))));
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
