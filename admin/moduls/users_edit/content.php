<h2 class="perfil"><?php echo TITLE_PAGE; ?></h2>

<?php
// Fitxa
$id = $_GET['uID'];
$query_raw = "select u.id, u.name, u.mail, u.password from " . TABLE_ADMIN_USERS . " u where u.id = '" . (int) $customer_id . "'";
$item = tep_db_fetch_array(tep_db_query($query_raw));

$itemInfo = new objectInfo($item);
?>

<div id="form">
    <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=update&amp;uID=' . (int) $customer_id), 'post', 'enctype="multipart/form-data"'); ?>
    <p class="botonera">
        <?php
        echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_UPDATE . '</span></a>';
        ?>
    </p>
    <fieldset>
        <label class="width_30_1">
            <span>Nom*</span>
            <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
        </label>
        <label class="width_70_2">
            <span>Email*</span>
            <input type="text" name="mail" value="<?php echo $itemInfo->mail; ?>" />
        </label>
        <p class="form_sep"></p>
        <label class="width_50_1">
            <span>Password*</span>
            <input type="password" name="password" value="" />
        </label>
        <label class="width_50_2">
            <span>Confirmar Password*</span>
            <input type="password" name="password_confirm" value="" />
        </label>                  
    </fieldset>
    <p class="botonera">
        <?php
        echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_UPDATE . '</span></a>';
        ?>
    </p>
</form>
</div>
