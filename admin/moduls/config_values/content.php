<div id="buscador">
    <?php
    echo tep_draw_form('form_search', tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))), 'get');
    // Recuperem totes les variables $_GET
    reset($_GET);
    while (list($key, $value) = each($_GET)) {
        if (($key != 'gID') && ($key != 'action') && ($key != 'page') && ($key != 'x') && ($key != 'y') && ($key != tep_session_name())) {
            if (tep_not_null($value))
                echo tep_draw_hidden_field(rawurldecode($key), rawurldecode($value));
        }
    }
    // Variables buscador
    echo '<label><span>' . SEARCH . '</span>'
    . tep_draw_pull_down_menu('gID', tep_get_config_groups_array(), $_GET['gID'], '  onchange="send_form(\'form_search\'); return false;"')
    . '</label>';
    ?>
</form>
</div>
<h2 class="config"><?php echo TITLE_PAGE . ' ' . $groupInfo->name; ?></h2> 

<?php
if ($_GET['action'] == 'new') {
    // Fitxa
    if (isset($_GET['vItem']) && tep_not_null($_GET['vItem'])) {
        $id = $_GET['vItem'];
        $query_raw = "select c.item, c.value, c.description from " . TABLE_CONFIG_VALUES . " c where c.item='" . tep_db_input($_GET['vItem']) . "'";
        $item = tep_db_fetch_array(tep_db_query($query_raw));
    } else {
        $item = array('item' => '',
            'value' => '',
            'description' => '');
    }
    $itemInfo = new objectInfo($item);

    $form_action = ($_GET['vItem']) ? 'update' : 'insert';
    ?>

    <div id="form">
    <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
        <fieldset>
            <label>
                <span>Nom</span>
                <input type="text" name="item" value="<?php echo $itemInfo->item; ?>" />
            </label>
            <label>
                <span>Value</span>
                <textarea name="value" id="code" class="info" rows="10" cols="10"><?php echo $itemInfo->value; ?></textarea>
            </label>
            <label>
                <span>Description</span>
                <textarea name="description" class="info" rows="10" cols="10"><?php echo $itemInfo->description; ?></textarea>
            </label>
        </fieldset>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
    </form>
    </div>

    <?php
} else {
    // Llistat
    $query_raw = "select c.item, c.value, c.description from " . TABLE_CONFIG_VALUES . " c where c.group_id = '" . (int) $_GET['gID'] . "' order by c.item";
    $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
    $listing = tep_db_query($query_raw);
    ?>

    <table summary="<?php echo TAULA; ?>"  class="llista fixed">
        <thead>
            <tr>
                <th style="width:35%;"><?php echo COL_1; ?></th>
                <th style="width:55%;"><?php echo COL_2; ?></th>
                <th style="width:10%; text-align:center;"><?php echo COL_3; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="3">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'action', 'x', 'y'))); ?></div>
    </th>
    </tr>
    </tfoot>
    <tbody>

        <?php
        if ($query_numrows > 0) {
            // Llistat segons búsqueda
            $i = 0;
            while ($item = tep_db_fetch_array($listing)) {
                $i++;
                $aux = ($aux % 2) + 1;
                $itemInfo = new objectInfo($item);

                echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'vItem', 'x', 'y')) . 'vItem=' . $itemInfo->item . '&amp;action=new') . '\'" class="' . (($itemInfo->item == $_GET['vItem']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                . '  <td>' . $itemInfo->item . '</td>' . "\n"
                . '  <td>' . htmlentities($itemInfo->value, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</td>' . "\n"
                . '  <td class="accions">'
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'vItem', 'x', 'y')) . 'vItem=' . $itemInfo->item . '&amp;action=new') . '">' . ICON_EDIT . '</a> '
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'vItem', 'x', 'y')) . 'vItem=' . $itemInfo->item . '&amp;action=delete') . '" class="delete">' . ICON_DELETE . '</a>'
                . '</td>' . "\n"
                . '</tr>' . "\n";
            }
        } else {
            // No Items
            echo '<tr>' . "\n";
            echo '  <td colspan="7" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
            echo '</tr>' . "\n";
        }
        ?>

    </tbody>
    </table>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'vItem', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
        <?php echo '<a href="' . tep_href_link('?modul=config_groups&amp;gID=' . $_GET['gID']) . '"><span>' . BUTTON_BACK . '</span></a>'; ?>
    </p>



    <?php
}
?>
