<?php

// Si no hi ha definit un id de grup tornem cap a grups
if (!tep_not_null($_GET['gID']) || $_GET['gID'] == '0') {
    tep_redirect(tep_href_link('', 'modul=config_groups'));
} else {
// Si no busquem valors del grup
    $query = tep_db_query("select id, name from " . TABLE_CONFIG_GROUPS . " where id = '" . (int) $_GET['gID'] . "' limit 1");
    $value = tep_db_fetch_array($query);
    $groupInfo = new objectInfo($value);
}

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
        case 'insert':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST['item']) {
                // Vector entrades
                $sql_data_array = array('item' => tep_db_prepare_input($_POST['item']),
                    'value' => tep_db_prepare_input($_POST['value']),
                    'description' => tep_db_prepare_input(ucfirst($_POST['description'])),
                    'group_id' => (int) $_GET['gID'],
                    'editable' => '1');
                // Crida base dades
                tep_db_perform(TABLE_CONFIG_VALUES, $sql_data_array);
                $aux_id = tep_db_insert_id();
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'vItem=' . $_POST['item']));
            break;

        case 'update':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['vItem'] && $_POST['item']) {
                // Identificador Usuari
                $valueItem = tep_db_prepare_input($_GET['vItem']);
                // Vector entrades
                $sql_data_array = array('item' => tep_db_prepare_input($_POST['item']),
                    'value' => tep_db_prepare_input($_POST['value']),
                    'description' => tep_db_prepare_input(ucfirst($_POST['description'])));
                // Crida base dades
                tep_db_perform(TABLE_CONFIG_VALUES, $sql_data_array, 'update', 'item = \'' . tep_db_input($valueItem) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'delete':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['vItem']) {
                $valueItem = tep_db_prepare_input($_GET['vItem']);
                // Eliminem entrada
                tep_db_query("delete from " . TABLE_CONFIG_VALUES . " where item = '" . tep_db_input($valueItem) . "'");
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'vItem', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
