<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  var item = trim(document.form_data.item.value);

  if (!/^.[A-Za-z0-9_]{2,}$/.test(item)) {
    error_message = error_message + "<?php echo JS_NOM; ?>";
    error = 1;
  }

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}           

//]]>
</script>
