<?php

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;

        case 'update':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['lID'] && $_POST['name']) {
                // Identificador Usuari
                $aux_id = (int) $_GET['lID'];
                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']));
                // Crida base dades
                tep_db_perform(TABLE_LANGUAGES, $sql_data_array, 'update', 'id = \'' . (int) $aux_id . '\'');

                if (isset($_POST['default']) && ($_POST['default'] == 'on')) {
                    tep_db_query("update " . TABLE_CONFIG_VALUES . " set value = '" . tep_db_prepare_input($_POST['code']) . "' where item = 'DEFAULT_LANGUAGE'");
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'active':
        case 'desactive':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['lID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['lID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_LANGUAGES, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'listorder':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['lID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['lID']);
                // Trobar posicio actual
                $order_now = tep_search_item_order($aux_id, TABLE_LANGUAGES);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order($aux_id, $order_now, $pos_aux, TABLE_LANGUAGES);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
