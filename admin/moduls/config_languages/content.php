<h2 class="config"><?php echo TITLE_PAGE; ?></h2>
<?php echo (($_GET['action']== 'new')? TEXT_FORM : TEXT_LIST); ?>  

<?php
if ($_GET['action'] == 'new') {
  // Fitxa
  if (isset($_GET['lID']) && tep_not_null($_GET['lID'])) {
    $id = $_GET['lID'];
    $query_raw = "select id, name, code, directory from " . TABLE_LANGUAGES . " where id = '" . (int)$id . "'";
    $item = tep_db_fetch_array(tep_db_query($query_raw));
  } else {
    $item = array('name' => '',
	        	  'code' => '',
	        	  'directory' => '');
  }
  $itemInfo = new objectInfo($item);

  $form_action = ($_GET['lID']) ? 'update' : 'insert';
?>

<div id="form">
  <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action','pos','x','y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
  <fieldset>
    <label>
      <input type="checkbox" name="default" value="on" <?php echo (($itemInfo->code==DEFAULT_LANGUAGE) ? 'checked="checked"' : ''); ?> class="checkbox" />
      <span class="inline"><?php echo MAKE_DEFAULT; ?></span>
    </label>
    <p class="form_sep"></p>
    <label>
      <span>Nom</span>
      <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
    </label>
    <p class="form_sep"></p>
    <label class="width_50_1">
      <span>Codi</span>
      <input type="text" name="code" value="<?php echo $itemInfo->code; ?>" class="camp_read width_50" readonly="readonly" />
    </label>
    <label class="width_50_2">
      <span>Directori</span>
      <input type="text" name="directory" value="<?php echo $itemInfo->directory; ?>" class="camp_read width_50" readonly="readonly" />
    </label>
  </fieldset>
  <p class="botonera">
    <?php
    echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
    echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','lID','x','y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
    ?>
  </p>
  </form>
</div>

<?php
} else {
  // Llistat
  $query_raw = "select id, name, code, directory, listorder, active from " . TABLE_LANGUAGES . " order by listorder";
  $listing = tep_db_query($query_raw);
  $query_numrows = tep_db_num_rows($listing);
?>

<table summary="<?php echo TAULA; ?>" class="llista fixed">
  <thead>
    <tr>
      <th style="width:25%;"><?php echo COL_1; ?></th>
      <th style="width:35%;"><?php echo COL_2; ?></th>
      <th style="width:10%; text-align:center;"><?php echo COL_3; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="5">
        <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS,$query_numrows)?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if ($query_numrows>0) {
  // Llistat segons búsqueda
  $i=0;
  while ($item = tep_db_fetch_array($listing)) {
  	$i++;
  	$aux=($aux%2)+1;
    $itemInfo = new objectInfo($item);

  	echo '<tr class="' . (($itemInfo->id == $_GET['lID'])? 'current_list' : 'taula' . $aux) . '">' . "\n"
     . '  <td>' . (($itemInfo->code==DEFAULT_LANGUAGE) ? ICON_DEFAULT : '') . $itemInfo->name . '</td>' . "\n"
     . '  <td>' . $itemInfo->code . '</td>' . "\n";

    if ($itemInfo->active) {
      $aux_active = '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','lID','buscar','x','y')) . 'action=desactive&amp;lID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
    } else {
      $aux_active = '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','lID','buscar','x','y')) . 'action=active&amp;lID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
    }

    $aux_order = '';
    if ($itemInfo->listorder > 1) {
      $aux_order .= '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','lID','value','buscar','x','y')) . 'action=listorder&amp;value=down&amp;lID=' . $itemInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
    } else {
      $aux_order .= ICON_ORDER_NO;
    }
    $aux_order .= '&nbsp;&nbsp;' . (($itemInfo->listorder < 10) ? '0' : '') . $itemInfo->listorder . '&nbsp;&nbsp;';
    if ($itemInfo->listorder < $query_numrows) {
      $aux_order .= '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','lID','value','buscar','x','y')) . 'action=listorder&amp;value=up&amp;lID=' . $itemInfo->id) . '">' . ICON_ORDER_UP . '</a>';
    } else {
      $aux_order .= ICON_ORDER_NO;
    }

    echo '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
     . '  <td class="order">' . $aux_order . '</td>' . "\n"
     . '  <td style="text-align:center;">'
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','lID','buscar','x','y')) . 'action=new&amp;lID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a>'
     . '</td>' . "\n"
     . '</tr>' . "\n";
  }

} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS,mb_strtolower(NEW_ITEM,'UTF-8')) . '</td>' . "\n" ;
  echo '</tr>' . "\n";

}
?>

  </tbody>
</table>

<?php
}
?>
