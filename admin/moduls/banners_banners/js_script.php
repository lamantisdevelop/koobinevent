<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

// Validar formulari
    function validarCamps(){
        var error = 0;
        var error_message = "<?php echo JS_ERROR; ?>";

        var action = '<?php echo $_GET['action'];?>';//per tal de validar si som grup o foto
        var name = trim(document.form_data.name.value);


        if (!/^.{2,}$/.test(name)) {
            error_message = error_message + "<?php echo JS_NOM; ?>";
            error = 1;
        }

        if (error == 1) {
            alert(error_message);
            return false;
        } else {
            return true;
        }

    }

//]]>
</script>
