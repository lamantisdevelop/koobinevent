<h2 class="banners"><?php echo TITLE_PAGE . (tep_not_null($groupInfo->name) ? ': ' . $groupInfo->name : ''); ?></h2>
<?php
if (substr($_GET['action'], 0, 3) == 'new') {

    // Fitxa
    if ($_GET['action'] == 'new_item') {
        if (isset($_GET['bID']) && tep_not_null($_GET['bID'])) {
            $id = $_GET['bID'];
            $query_raw = "select id, name, listorder from " . TABLE_BANNERS . " where id = '" . (int) $id . "'";
            $item = tep_db_fetch_array(tep_db_query($query_raw));
        } else {
            $item = array('id' => '',
                'name' => '',
                'listorder' => '');
        }
        $itemInfo = new objectInfo($item);

        $form_action = ($_GET['bID']) ? 'update_item' : 'insert_item';
        ?>
        <div id="form">
            <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                ?>
            </p>
            <fieldset>
                <label class="width_50_1">
                    <span>Nom (ús intern) *</span>
                    <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
                </label>
                <label class="width_50_2">
                    <span>Ordre</span>
                    <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
                    <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
                </label>
            </fieldset>
            <!-- Idiomes -->
            <?php
            if (sizeof($languages) > 1) {
                echo '<ul id="menuIdioma">';
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
                }
                echo '</ul>';
            }

            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                // Busquem valors
                $aux_listing = tep_db_query("select titol,descripcio,image, link, target, title, class, rel, rev from " . TABLE_BANNERS_DESCRIPTION . " where banners_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
                $aux_numrows = tep_db_num_rows($aux_listing);
                if ($aux_numrows > 0) {
                    $aux_item = tep_db_fetch_array($aux_listing);
                    $auxInfo = new objectInfo($aux_item);
                }
                ?>               
                <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                    <?php
                    if (tep_not_null($auxInfo->image)) {
                        echo tep_draw_hidden_field('banner_prev[' . $languages[$i]['id'] . ']', $auxInfo->image) . "\n";
                        echo '<label class="width_50_1"><span>Imatge Actual</span><input type="text" name="banner_act[' . $languages[$i]['id'] . ']" value="' . $auxInfo->image . '" class="camp_read" readonly="readonly" />' . "\n"
                        . '<a href="javascript:void(0);" class="link_del"></a>' . "\n"
                        . '<a href="' . DIR_WS_PORTAL_BANNERS . $auxInfo->image . '" rel="colorbox" title="' . $auxInfo->image . '" class="link_view"></a></label>' . "\n"
                        . '<label class="width_50_2"><span>Nova Imatge <em>(ULL!! entrar imatge amb mida exacta)</em></span><input class="img_input" type="file" name="banner_new[' . $languages[$i]['id'] . ']" value="" size="45" class="file" /></label>' . "\n";
                    } else {
                        echo '<label class="width_50_1"><span>Imatge <em>(ULL!! entrar imatge amb mida exacta)</em></span><input class="img_input" type="file" name="banner_new[' . $languages[$i]['id'] . ']" value="" size="45" class="file" /></label>' . "\n";
                    }
                    ?>
                    <p class="form_sep"></p>
                    <label>
                        <img id="img_preview" src="<?php echo DIR_WS_PORTAL_BANNERS . $auxInfo->image; ?>" alt="Previsualització de la imatge" /> 
                    </label>
                    <p class="form_sep"></p>
                    <label>
                        <span>Titol</span>
                        <input type="text" name="titol[<?php echo $languages[$i]['id']; ?>]" value="<?php echo htmlentities($auxInfo->titol, ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?>" />
                    </label>
                    <div class="clear"></div>
                    Descripció
                    <div class="tab_content action_4">
                        <textarea name="descripcio[<?php echo $languages[$i]['id']; ?>]" id="descripcio[<?php echo $languages[$i]['id']; ?>]">
                            <?php echo $auxInfo->descripcio; ?>
                        </textarea>
                        <script>
                            <?php 
                            if (!tep_usuaris_es_admin())
                            { 
                                echo("CKEDITOR.config.toolbar = 'Editor';");                     
                            }
                            ?>
                            CKEDITOR.replace( 'descripcio[<?php echo $languages[$i]['id']; ?>]');
                        </script>
                        <?php
                        
//                        if (tep_usuaris_es_admin()) {
//                            $params_ckeditor['toolbar'] = 'Full';
//                        }
//                        $ckeditor->replace('descripcio[' . $languages[$i]['id'] . ']', $params_ckeditor);
                        ?>

                    </div>
                    <p class="form_sep"></p>
                    <label class="width_50_1">
                        <span>Enllaç</span>
                        <input type="text" name="link[<?php echo $languages[$i]['id']; ?>]" value="<?php echo (($auxInfo->link) ? $auxInfo->link : 'http://'); ?>" />
                    </label>
                    <label class="width_50_2">
                        <span>Title</span>
                        <input type="text" name="title[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->title; ?>" />
                    </label>
                    <label class="width_50_1">
                        <span>Target*</span>
                        <?php echo tep_draw_pull_down_menu('target[' . $languages[$i]['id'] . ']', $target_array, $auxInfo->target, ''); ?>
                    </label>
                    <?php if (tep_usuaris_es_admin()) { ?>
                        <label class="width_50_2">
                            <span>Class</span>
                            <input type="text" name="class[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->class; ?>" />
                        </label>
                        <label class="width_50_1">
                            <span>Rel</span>
                            <input type="text" name="rel[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rel; ?>" />
                        </label>
                        <label class="width_50_2">
                            <span>Rev</span>
                            <input type="text" name="rev[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rev; ?>" />
                        </label>
                    <?php } ?>
                </fieldset>
                <?php
            }
            ?>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                ?>
            </p>
        </form>
        </div>

        <?php
        // Formulari Categories
    } else if ($_GET['action'] == 'new_group') {

        if (($_GET['gID'])) {
            $id = $_GET['gID'];
            $query_raw = "select bg.name, bg.listorder from " . TABLE_BANNERS_GROUPS . " bg where bg.id = '" . (int) $id . "'";
            $item = tep_db_fetch_array(tep_db_query($query_raw));
        } else {
            $item = array('name' => '',
                'listorder' => '');
        }
        $itemInfo = new objectInfo($item);
        $form_action = ($_GET['gID']) ? 'update_group' : 'insert_group';
        ?>

        <div id="form">
            <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
            <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                <label class="width_50_1">
                    <span>Nom</span>
                    <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
                </label>
                <label class="width_50_2">
                    <span>Ordre</span>
                    <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
                </label>
            </fieldset>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                ?>
            </p>
        </form>
        </div>

        <?php
    }
} else {
    ?>
    <p class="botonera">
        <?php
        if (!isset($_GET['group_id']) || !tep_not_null($_GET['group_id'])) {
            if (tep_usuaris_es_admin()) {
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_group') . '"><span>' . NEW_CATEGORY . '</span></a>';
            }
        } else {
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_item') . '"><span>' . NEW_ITEM . '</span></a>';
        }
        ?>
    </p>
    <?php
// Llistat
    if (!isset($_GET['group_id']) || !tep_not_null($_GET['group_id'])) {
        // Categories
        $groups_raw = "select distinct bg.id, bg.name, bg.listorder from " . TABLE_BANNERS_GROUPS . " bg order by bg.listorder";
        $groups_query = tep_db_query($groups_raw);
        $groups_rows = tep_db_num_rows($groups_query);
        ?>

        <table summary="<?php echo TAULA; ?>"  class="llista fixed">
            <thead>
                <tr>
                    <th style="width:60%;"><?php echo COL_1; ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_2a; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="4">
            <div id="split_left"><?php echo sprintf(TEXT_DISPLAY_NUMBER_OF_GROUPS, $groups_rows); ?></div>
        </th>
        </tr>
        </tfoot>
        <tbody>

            <?php
            if ($groups_rows > 0) {
                // Categories
                while ($categories = tep_db_fetch_array($groups_query)) {
                    $i++;
                    $aux = ($aux % 2) + 1;
                    $cInfo = new objectInfo($categories);

                    $aux_order = '';
                    if ($cInfo->listorder > 1) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=down&amp;gID=' . $cInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }
                    $aux_order .= '&nbsp;&nbsp;' . (($cInfo->listorder < 10) ? '0' : '') . $cInfo->listorder . '&nbsp;&nbsp;';
                    if ($cInfo->listorder < $groups_rows) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=up&amp;gID=' . $cInfo->id) . '">' . ICON_ORDER_UP . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }

                    echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'group_id=' . $cInfo->id) . '\'" class="' . (($cInfo->id == $_GET['gID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                    . '  <td class="nom"><a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'group_id=' . $cInfo->id) . '">' . ICON_FOLDER . ($cInfo->name) . '</a></td>' . "\n"
                    . '  <td style="text-align:center;">' . $cInfo->id . '</td>' . "\n"
                    . '  <td class="order">' . $aux_order . '</td>' . "\n"
                    . '  <td style="text-align:center;" class="accions">';
                    if (tep_usuaris_es_admin()) {
                        echo'<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'gID=' . $cInfo->id . '&amp;action=new_group') . '">' . ICON_EDIT . '</a> '
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'gID=' . $cInfo->id . '&amp;action=delete_group') . '"  class="delete">' . ICON_DELETE . '</a>';
                    }
                    echo '</td>' . "\n"
                    . '</tr>' . "\n";
                }
            } else {
                // No Items
                echo '<tr>' . "\n";
                echo '  <td colspan="4" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_CATEGORY, 'UTF-8')) . '</td>' . "\n";
                echo '</tr>' . "\n";
            }
        } else {
            // Llistat
            $query_raw = "select b.id, b.name, b.active, b.data_final, b.listorder from " . TABLE_BANNERS . " b left join " . TABLE_BANNERS_DESCRIPTION . " bd on bd.banners_id = b.id and bd.language_id = '" . (int) $language_id . "' where b.group_id = '" . (int) $_GET['group_id'] . "' order by listorder";
            $listing = tep_db_query($query_raw);
            $query_numrows = tep_db_num_rows($listing);
            ?>
        <table summary="<?php echo TAULA; ?>" class="llista fixed">
            <thead>
                <tr>
                    <th style="width:55%;"><?php echo COL_1; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_3; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="5">
            <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS, $query_numrows) ?></div>
            </th>
            </tr>
            </tfoot>
            <tbody>

                <?php
                // Tornar a categories
                echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'group_id', 'gID', 'bID', 'x', 'y')) . 'gID=' . $_GET['group_id']) . '\'" class="taula2">' . "\n"
                . '  <td colspan="5"><a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'group_id', 'gID', 'bID', 'x', 'y')) . 'gID=' . $_GET['group_id']) . '">' . ICON_FOLDER . '..</a></td>' . "\n"
                . '</tr>' . "\n";

                if ($query_numrows > 0) {
                    // Llistat segons búsqueda
                    $i = 0;
                    while ($item = tep_db_fetch_array($listing)) {
                        $i++;
                        $aux = ($aux % 2) + 1;
                        $itemInfo = new objectInfo($item);

                        $aux_order = '';
                        if ($itemInfo->listorder > 1) {
                            $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'value', 'buscar', 'x', 'y')) . 'action=listorder_item&amp;value=down&amp;bID=' . $itemInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
                        } else {
                            $aux_order .= ICON_ORDER_NO;
                        }
                        $aux_order .= '&nbsp;&nbsp;' . (($itemInfo->listorder < 10) ? '0' : '') . $itemInfo->listorder . '&nbsp;&nbsp;';
                        if ($itemInfo->listorder < $query_numrows) {
                            $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'value', 'buscar', 'x', 'y')) . 'action=listorder_item&amp;value=up&amp;bID=' . $itemInfo->id) . '">' . ICON_ORDER_UP . '</a>';
                        } else {
                            $aux_order .= ICON_ORDER_NO;
                        }

                        if ($itemInfo->active) {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=desactive&amp;bID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                        } else {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=active&amp;bID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                        }

                        $aux_data_final = tep_date_short($itemInfo->data_final);

                        echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=new_item&amp;bID=' . $itemInfo->id) . '\'" class="' . (($itemInfo->id == $_GET['bID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                        . '  <td class="nom">' . $itemInfo->name . '</td>' . "\n"
                        . '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
                        . '  <td class="order">' . $aux_order . '</td>' . "\n"
                        . '  <td style="text-align:center;" class="accions">'
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=new_item&amp;bID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a> '
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=delete_item&amp;bID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>'
                        . '</td>' . "\n"
                        . '</tr>' . "\n";
                    }
                } else {
                    // No Items
                    echo '<tr>' . "\n";
                    echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
                    echo '</tr>' . "\n";
                }
            }
            ?>

        </tbody>
    </table>

    <p class="botonera">
        <?php
        if (!isset($_GET['group_id']) || !tep_not_null($_GET['group_id'])) {
            if (tep_usuaris_es_admin()) {
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_group') . '"><span>' . NEW_CATEGORY . '</span></a>';
            }
        } else {
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_item') . '"><span>' . NEW_ITEM . '</span></a>';
        }
        ?>
    </p>

    <?php
}
?>
