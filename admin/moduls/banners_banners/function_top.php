<?php

// Include thumbnail class
require_once(DIR_WS_CLASSES . 'ThumbLib.inc.php');

// Definim vector per selecció target
$target_array = array(array('id' => '_self', 'text' => 'Obrir a la mateixa finestra (_self)'),
    array('id' => '_blank', 'text' => 'Obrir en una finestra nova (_blank)'));

// Creem Array Idiomes
$languages = tep_get_languages();

// Funcions Auxiliars
function delete_item($item_id) {

    // Busquem i modifiquem ordre
    $order_now = tep_search_item_order_group($item_id, TABLE_BANNERS, 'group_id', $_GET['group_id']);
    // Eliminem entrada
    tep_db_query("delete from " . TABLE_BANNERS . " where id = '" . (int) $item_id . "'");
    // Reordenem items
    tep_reorder_items_group($order_now, TABLE_BANNERS, 'group_id', $_GET['group_id']);

    // Eliminar x descripcio
    $languages = tep_get_languages();
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        $lang_aux = $languages[$i]['id'];
        $files_query = tep_db_query("select image from " . TABLE_BANNERS_DESCRIPTION . " where banners_id = '" . (int) $item_id . "' and language_id = '" . (int) $lang_aux . "'");
        $files = tep_db_fetch_array($files_query);
        if (file_exists(DIR_FS_PORTAL_BANNERS . $files['image']))
            @unlink(DIR_FS_PORTAL_BANNERS . $files['image']);
    }
    tep_db_query("delete from " . TABLE_BANNERS_DESCRIPTION . " where banners_id = '" . (int) $item_id . "'");
}

// Busquem valors del grup
if ($_GET['group_id']) {
    $query = tep_db_query("select bg.id, bg.name from " . TABLE_BANNERS_GROUPS . " bg where bg.id = '" . (int) $_GET['group_id'] . "' limit 1");
    $value = tep_db_fetch_array($query);
    $groupInfo = new objectInfo($value);
}

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
//------------------------------ Grups ------------------------------//

        case 'insert_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Busquem nou ordre
                $new_id = tep_search_new_id(TABLE_BANNERS_GROUPS);
                $order_now = tep_search_new_order(TABLE_BANNERS_GROUPS);

                // Vector entrades
                $sql_data_array = array('id' => (int) $new_id,
                    'name' => tep_db_prepare_input($_POST['name']),
                    'listorder' => $order_now,
                    'entered' => 'now()',
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_BANNERS_GROUPS, $sql_data_array);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'gID=' . $new_id));
            break;

        case 'update_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['gID'])) {
                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['gID']);

                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_BANNERS_GROUPS, $sql_data_array, 'update', "id = '" . (int) $aux_id . "'");
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'listorder_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['gID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['gID']);
                // Trobar posicio actual
                $order_now = tep_search_item_order($aux_id, TABLE_BANNERS_GROUPS);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order($aux_id, $order_now, $pos_aux, TABLE_BANNERS_GROUPS);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'delete_group':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if (isset($_GET['gID'])) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['gID']);
                // Busquem i modifiquem ordre
                $order_now = tep_search_item_order($aux_id, TABLE_BANNERS_GROUPS);
                // Eliminar entrada
                tep_db_query("delete from " . TABLE_BANNERS_GROUPS . " where id = '" . (int) $aux_id . "'");
                // Reordenar items
                tep_reorder_items($order_now, TABLE_BANNERS_GROUPS);

                // Eliminar items
                $items_query = tep_db_query("select distinct id from " . TABLE_BANNERS . " where group_id = '" . (int) $aux_id . "' order by listorder");
                while ($item = tep_db_fetch_array($items_query)) {
                    // Borrem entraeda
                    delete_item($item['id']);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'x', 'y'))));
            break;

//------------------------------ Opcions ------------------------------//

        case 'insert_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST['name']) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Busquem nou ordre
                $order_now = tep_search_new_order_group(TABLE_BANNERS, 'group_id', $_GET['group_id']);

                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'listorder' => $order_now,
                    'active' => '1',
                    'group_id' => tep_db_prepare_input($_GET['group_id']),
                    'entered' => 'now()',
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_BANNERS, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Entrades segons idioma
                $link_array = $_POST['link'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                $titol_array = $_POST['titol'];
                $descripcio_array = $_POST['descripcio'];
                $banner = $_FILES['banner_new'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Guardar banner
                    $banner_aux = $banner['name'][$lang_aux];
                    if (!empty($banner_aux)) {
                        if (is_uploaded_file($banner['tmp_name'][$lang_aux])) {
                            // Borrem imatge vella
                            $banner_old = DIR_FS_PORTAL_BANNERS . $_POST['banner_act'][$lang_aux];
                            if (file_exists($banner_old))
                                @unlink($banner_old);
                            // Nom imatge no repetit
                            $banner_aux = rename_if_exists(DIR_FS_PORTAL_BANNERS, $banner_aux);
                            // Copiar imatge
                            move_uploaded_file($banner['tmp_name'][$lang_aux], DIR_FS_PORTAL_BANNERS . $banner_aux);
                        }
                    }

                    // Insertem dades
                    $sql_data_array = array('banners_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'titol' => tep_db_prepare_input($titol_array[$lang_aux]),
                        'descripcio' => tep_db_prepare_input($descripcio_array[$lang_aux]),
                        'image' => tep_db_prepare_input($banner_aux),
                        'link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    // Crida base dades
                    tep_db_perform(TABLE_BANNERS_DESCRIPTION, $sql_data_array);
                }

                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] < $order_now) {
                    tep_update_order_group($aux_id, $order_now, $_POST['listorder'], TABLE_BANNERS, 'group_id', $_GET['group_id']);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'bID=' . $aux_id));
            break;

        case 'update_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['bID'] && $_POST['name']) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Identificador
                $aux_id = tep_db_prepare_input($_GET['bID']);

                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_BANNERS, $sql_data_array, 'update', 'id = \'' . (int) $aux_id . '\'');

                // Entrades segons idioma
                $link_array = $_POST['link'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                $titol_array = $_POST['titol'];
                $descripcio_array = $_POST['descripcio'];
                $banner = $_FILES['banner_new'];
                $banner_act = $_POST['banner_act'];
                $banner_prev = $_POST['banner_prev'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Guardar banner
                    $banner_aux = $banner['name'][$lang_aux];
                    if (!empty($banner_aux)) {
                        if (is_uploaded_file($banner['tmp_name'][$lang_aux])) {
                            // Borrem imatge vella
                            $banner_old = DIR_FS_PORTAL_BANNERS . $_POST['banner_act'][$lang_aux];
                            if (file_exists($banner_old))
                                @unlink($banner_old);
                            // Nom imatge no repetit
                            $banner_aux = rename_if_exists(DIR_FS_PORTAL_BANNERS, $banner_aux);
                            // Copiar imatge
                            move_uploaded_file($banner['tmp_name'][$lang_aux], DIR_FS_PORTAL_BANNERS . $banner_aux);
                        }
                    } else {
                        $banner_aux = $banner_act[$lang_aux];
                        // Si és buit borrem imatge, cas que n'hi hagi
                        if (!tep_not_null($banner_act[$lang_aux]) && tep_not_null($banner_prev[$lang_aux])) {
                            // Borrem imatge vella
                            $banner_old = DIR_FS_PORTAL_BANNERS . $banner_prev[$lang_aux];
                            if (file_exists($banner_old))
                                @unlink($banner_old);
                        }
                    }

                    // Insertem dades
                    $sql_data_array = array('banners_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'titol' => tep_db_prepare_input($titol_array[$lang_aux]),
                        'descripcio' => tep_db_prepare_input($descripcio_array[$lang_aux]),
                        'image' => tep_db_prepare_input($banner_aux),
                        'link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT banners_id FROM " . TABLE_BANNERS_DESCRIPTION . " WHERE banners_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_BANNERS_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_BANNERS_DESCRIPTION, $sql_data_array, 'update', 'banners_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }

                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] != $_POST['listorder_aux']) {
                    tep_update_order_group($aux_id, $_POST['listorder_aux'], $_POST['listorder'], TABLE_BANNERS, 'group_id', $_GET['group_id']);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'listorder_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['bID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['bID']);
                // Trobar posicio actual
                $order_now = tep_search_item_order_group($aux_id, TABLE_BANNERS, 'group_id', $_GET['group_id']);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order_group($aux_id, $order_now, $pos_aux, TABLE_BANNERS, 'group_id', $_GET['group_id']);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'active':
        case 'desactive':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['bID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['bID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_BANNERS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'delete_item':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['bID']) {
                $aux_id = tep_db_prepare_input($_GET['bID']);
                // Borrem entraeda
                delete_item($aux_id);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'bID', 'x', 'y'))));
            break;
    }
}

// Comprovem si existeixen i es pot escriure en directoris per imatges i descàrregues
if (is_dir(DIR_FS_PORTAL_BANNERS)) {
    if (!is_writeable(DIR_FS_PORTAL_BANNERS))
        $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE . ' (' . DIR_FS_PORTAL_BANNERS . ')', 'error');
} else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST . ' (' . DIR_FS_PORTAL_BANNERS . ')', 'error');

// Textos
    $titular = WEB_NAME . ' | ' . PAGE_TITLE;
}
?>
