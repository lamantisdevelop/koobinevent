<h2 class="fotos">RATIOS</h2>

<?php
if ($_GET['action'] == 'new') {
    // Fitxa
    if (isset($_GET['fID']) && tep_not_null($_GET['fID'])) {
        $id = $_GET['fID'];
        $query_raw = "select id, nom,ratio, thumb1_h,thumb1_w,thumb2_h,thumb2_w,thumb3_h,thumb3_w from " . TABLE_FOTOS_RATIOS . " where id = '" . (int) $_GET['fID'] . "'";
        $item = tep_db_fetch_array(tep_db_query($query_raw));
    } else {
        $item = array('id' => '',
            'nom' => '',
            'ratio' => '',
            'thumb1_h' => '',
            'thumb1_w' => '',
            'thumb2_h' => '',
            'thumb2_w' => '',
            'thumb3_h' => '',
            'thumb3_w' => '');
    }
    $itemInfo = new objectInfo($item);

    $form_action = ($_GET['fID']) ? 'update' : 'insert';
    ?>

    <div id="form">
        <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
        <fieldset>
            <label class="width_50_1">
                <span>Nom</span>
                <input type="text" name="nom" value="<?php echo $itemInfo->nom; ?>" />
            </label>
            <label class="width_50_2">
                <span>Ratio</span>
                <input type="text" name="ratio" value="<?php echo $itemInfo->ratio; ?>" />
            </label>
            <p class="form_sep"></p>
            <label class="width_50_1">
                <span>Miniatura1 Width</span>
                <input type="text" name="thumb1_w" value="<?php echo $itemInfo->thumb1_w; ?>" />
            </label>
            <label class="width_50_2">
                <span>Miniatura1 Height</span>
                <input type="text" name="thumb1_h" value="<?php echo $itemInfo->thumb1_h; ?>" />
            </label>

            <p class="form_sep"></p>
            <label class="width_50_1">
                <span>Miniatura2 Width</span>
                <input type="text" name="thumb2_w" value="<?php echo $itemInfo->thumb2_w; ?>" />
            </label>
            <label class="width_50_2">
                <span>Miniatura2 Height</span>
                <input type="text" name="thumb2_h" value="<?php echo $itemInfo->thumb2_h; ?>" />
            </label>

            <p class="form_sep"></p>
            <label class="width_50_1">
                <span>Miniatura3 Width</span>
                <input type="text" name="thumb3_w" value="<?php echo $itemInfo->thumb3_w; ?>" />
            </label>
            <label class="width_50_2">
                <span>Miniatura3 Height</span>
                <input type="text" name="thumb3_h" value="<?php echo $itemInfo->thumb3_h; ?>" />
            </label>

        </fieldset>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
    </form>
    </div>

    <?php
} else {
    // Llistat
    if (isset($_GET['keyword']) && tep_not_null($_GET['keyword'])) {
        $query_raw = "select id, nom,ratio, thumb1_h,thumb1_w,thumb2_h,thumb2_w,thumb3_h,thumb3_w from " . TABLE_FOTOS_RATIOS . " where nom like '%" . tep_db_input($_GET['keyword']) . "%'";
    } else {
        $query_raw = "select id, nom,ratio, thumb1_h,thumb1_w,thumb2_h,thumb2_w,thumb3_h,thumb3_w from " . TABLE_FOTOS_RATIOS;
    }
    $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
    $listing = tep_db_query($query_raw);
    ?>
    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'fID', 'x', 'y')) . 'action=new') . '"><span>Nou ratio</span></a>'; ?>
    </p>
    <table summary="<?php echo TAULA; ?>" class="llista fixed">
        <thead>
            <tr>
                <th style="width:50%;">Nom</th>
                <th style="width:15%; text-align:center;">Ratio</th>
                <th style="width:20%; text-align:center;">Accions</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="3">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'action', 'x', 'y'))); ?></div>
    </th>
    </tr>
    </tfoot>
    <tbody>

        <?php
        if ($query_numrows > 0) {
            // Llistat segons búsqueda
            $i = 0;
            while ($item = tep_db_fetch_array($listing)) {
                $i++;
                $aux = ($aux % 2) + 1;
                $itemInfo = new objectInfo($item);

                echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'fID', 'x', 'y')) . 'fID=' . $itemInfo->id . '&amp;action=new') . '\'" class="' . (($itemInfo->id == $_GET['fID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                . '  <td>' . $itemInfo->nom . '</td>' . "\n"
                . '  <td style="text-align:center;">' . $itemInfo->ratio . '</td>' . "\n"
                . '  <td style="text-align:center;" class="accions">'
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'fID', 'x', 'y')) . 'fID=' . $itemInfo->id . '&amp;action=new') . '">' . ICON_EDIT . '</a> '
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'fID', 'x', 'y')) . 'fID=' . $itemInfo->id . '&amp;action=delete') . '" class="delete">' . ICON_DELETE . '</a>'
                . '</td>' . "\n"
                . '</tr>' . "\n";
            }
        } else {
            // No Items
            echo '<tr>' . "\n";
            echo '  <td colspan="3" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
            echo '</tr>' . "\n";
        }
        ?>

    </tbody>
    </table>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'fID', 'x', 'y')) . 'action=new') . '"><span>Nou ratio</span></a>'; ?>
    </p>


    <?php
}
?>


