<?php

// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'insert':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Traiem limit scripts
                tep_set_time_limit(0);
                // Insertem dades generals
                $sql_data_array = array('nom' => tep_db_prepare_input($_POST['nom']),
                    'ratio' => ($_POST['ratio']),
                    'thumb1_h' => tep_db_prepare_input($_POST['thumb1_h']),
                    'thumb1_w' => tep_db_prepare_input($_POST['thumb1_w']),
                    'thumb2_h' => tep_db_prepare_input($_POST['thumb2_h']),
                    'thumb2_w' => tep_db_prepare_input($_POST['thumb2_w']),
                    'thumb3_h' => tep_db_prepare_input($_POST['thumb3_h']),
                    'thumb3_w' => tep_db_prepare_input($_POST['thumb3_w'])
                );

                tep_db_perform(TABLE_FOTOS_RATIOS, $sql_data_array);
                $aux_id = tep_db_insert_id();
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'fID', 'x', 'y')) . 'fID=' . $aux_id));
            break;

        case 'update':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['fID'] && $_POST) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Identificador
                $aux_id = tep_db_prepare_input($_GET['fID']);

                // Insertem dades personals
                $sql_data_array = array('nom' => tep_db_prepare_input($_POST['nom']),
                    'ratio' => ($_POST['ratio']),
                    'thumb1_h' => tep_db_prepare_input($_POST['thumb1_h']),
                    'thumb1_w' => tep_db_prepare_input($_POST['thumb1_w']),
                    'thumb2_h' => tep_db_prepare_input($_POST['thumb2_h']),
                    'thumb2_w' => tep_db_prepare_input($_POST['thumb2_w']),
                    'thumb3_h' => tep_db_prepare_input($_POST['thumb3_h']),
                    'thumb3_w' => tep_db_prepare_input($_POST['thumb3_w'])
                    );

                tep_db_perform(TABLE_FOTOS_RATIOS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'delete':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['fID']) {
                $aux_id = tep_db_prepare_input($_GET['fID']);

                // Eliminem entrada

                tep_db_query("delete from " . TABLE_FOTOS_RATIOS . " where id = '" . (int) $aux_id . "'");
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'fID', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
