
<!-- Inicic Header -->
<div id="header">
    <div id="header_content">
        <div id="header_esq">
            <div onclick="window.location='<?php echo DIR_WS_ADMIN; ?>'" id="header_logo"></div>
        </div>
        <div id="header_dret">
            <?php
            if (tep_session_is_registered('customer_id')) {
                ?>
                <div id="header_options">
                    <a class="veure_web" href="../" target="_blank">Veure Web</a>
                    <a class="perfil" href="?modul=users_edit">Perfil</a>
                    <a class="estadistiques" href="?modul=estadistiques">Estadístiques</a>
                    <!--<a class="ajuda" href="ajuda_cms.pdf" target="_blank">Ajuda</a>-->
                    <a class="sortir" href="logoff.php">Log Out</a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<!-- Fi Header -->
