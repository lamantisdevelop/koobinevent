<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

// Validar formulari
function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  var name = trim(document.form_data.name.value);
  var mail = trim(document.form_data.mail.value);
  var password = trim(document.form_data.password.value);
  var password_confirm = trim(document.form_data.password_confirm.value);


  if (!/^.{2,}$/.test(name)) {
    error_message = error_message + "<?php echo JS_NOM; ?>";
    error = 1;
  }

  if (!/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(mail)) {
    error_message = error_message + "<?php echo JS_MAIL; ?>";
    error = 1;
  }

  if (password!=''){
    if (password != password_confirm) {
      error_message = error_message + "<?php echo JS_PASSWORD_CONFIRM; ?>";
      error = 1;
    }
  }
  
  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}

//]]>
</script>
