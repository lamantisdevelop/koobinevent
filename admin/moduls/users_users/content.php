<h2 class="users"><?php echo TITLE_PAGE; ?></h2> 

<?php
if ($_GET['action'] == 'new') {
    // Fitxa
    if (isset($_GET['uID']) && tep_not_null($_GET['uID'])) {
        $id = $_GET['uID'];
        $query_raw = "select u.id, u.name, u.mail, u.password, u.id_rol, tur.nom as rol from " . TABLE_ADMIN_USERS . " u LEFT JOIN " . TABLE_ADMIN_USERS_ROLS . " tur ON u.id_rol=tur.id_rol where u.id = '" . (int) $id . "'";
        $item = tep_db_fetch_array(tep_db_query($query_raw));
    } else {
        $item = array('id' => '',
            'name' => '',
            'mail' => '',
            'password' => '',
            'id_rol' => '');
    }
    $itemInfo = new objectInfo($item);

    $form_action = ($_GET['uID']) ? 'update' : 'insert';

    echo(TEXT_FORM);
    ?>

    <div id="form">
        <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
        <fieldset>
            <label class="width_30_1">
                <span>Nom*</span>
                <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
            </label>
            <label class="width_70_2">
                <span>Email*</span>
                <input type="text" name="mail" value="<?php echo $itemInfo->mail; ?>" />
            </label>
            <p class="form_sep"></p>
            <label class="width_50_1">
                <span>Password*</span>
                <input type="password" name="password" value="" />
            </label>
            <label class="width_50_2">
                <span>Confirmar Password*</span>
                <input type="password" name="password_confirm" value="" />
            </label>
            <p class="form_sep"></p>
            <label>
                <span>Rol*</span>
                <?php echo tep_draw_pull_down_menu('rol', tep_get_admin_users_rols(), $itemInfo->id_rol); ?>
            </label>
        </fieldset>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
    </form>
    </div>

    <?php
} elseif ($_GET['action'] == 'forbidden') {
    echo(TEXT_FORBIDDEN);
} else {
    // Llistat
    $query_raw = "select u.id, u.name, u.mail, u.entered, u.modified, u.logons, u.last_logon, tur.id_rol, tur.nom as rol from " . TABLE_ADMIN_USERS . " u LEFT JOIN " . TABLE_ADMIN_USERS_ROLS . " tur ON u.id_rol=tur.id_rol  order by u.id";
    $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
    $listing = tep_db_query($query_raw);
    echo(TEXT_LIST);
    ?>
    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>
    <table summary="<?php echo TAULA; ?>" class="llista fixed">
        <thead>
            <tr>
                <th style="width:5%;"><?php echo COL_1; ?></th>
                <th style="width:45%"><?php echo COL_2; ?></th>
                <th style="width:20%"><?php echo COL_3; ?></th>
                <th style="width:10%"><?php echo COL_4; ?></th>
                <th style="width:15%"><?php echo COL_5; ?></th>
                <th style="width:15%; text-align:center;"><?php echo COL_6; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="7">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'action', 'x', 'y'))); ?></div>
    </th>
    </tr>
    </tfoot>
    <tbody>

        <?php
        if ($query_numrows > 0) {
            // Llistat segons búsqueda
            $i = 0;
            while ($item = tep_db_fetch_array($listing)) {
                $i++;
                $aux = ($aux % 2) + 1;
                $itemInfo = new objectInfo($item);

                echo '<tr class="' . (($itemInfo->id == $_GET['uID']) ? 'current_list' : 'taula' . $aux) . '" onmouseover="this.className=\'sobre\';" onmouseout="this.className=\'' . (($itemInfo->id == $_GET['uID']) ? 'current_list' : 'taula' . $aux) . '\';">' . "\n"
                . '  <td>' . $itemInfo->id . '</td>' . "\n"
                . '  <td>' . $itemInfo->name . '</td>' . "\n"
                . '  <td>' . $itemInfo->rol . '</td>' . "\n"
                . '  <td>' . $itemInfo->logons . '</td>' . "\n"
                . '  <td>' . tep_date_short($itemInfo->last_logon) . '</td>' . "\n"
                . '  <td style="text-align:center;" class="accions">'
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'buscar', 'x', 'y')) . 'action=new&amp;uID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a> | '
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'buscar', 'x', 'y')) . 'action=delete&amp;uID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>'
                . '</td>' . "\n"
                . '</tr>' . "\n";
            }
        } else {
            // No Items
            echo '<tr>' . "\n";
            echo '  <td colspan="7" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
            echo '</tr>' . "\n";
        }
        ?>

    </tbody>
    </table>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>

    <?php
}
?>
