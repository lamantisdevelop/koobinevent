<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  var sql_file = trim(document.form_data.sql_file.value);

  if (!/^.+\.(sql)$/.test(sql_file)) {
    error_message = error_message + "<?php echo JS_FILE; ?>";
    error = 1;
  }

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}

$(document).ready(function() {
    $('.restore').click(function(evento) {
	  if(confirm('<?php echo addslashes(TEXT_RESTORE); ?>')) {
        $('#pagina').block({
          message: '<h1><img src="image/loading.gif"/>&nbsp;&nbsp;Processant...<\/h1>'
        });
      } else {
          evento.preventDefault();
      }
    });
});

//]]>
</script>
