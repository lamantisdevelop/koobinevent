<h2 class="tools"><?php echo TITLE_PAGE; ?></h2>
<?php echo TEXT_LIST; ?>  

<table summary="<?php echo TAULA; ?>" class="llista fixed">
  <thead>
    <tr>
      <th style="width:50%;"><?php echo COL_1; ?></th>
      <th style="width:20%;"><?php echo COL_2; ?></th>
      <th style="width:15%;"><?php echo COL_3; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="4">
        <div id="split_left"><?php echo TEXT_BACKUP_DIRECTORY . ' ' . DIR_WS_BACKUPS; ?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if ($num_items>0) {
  // Llistat segons búsqueda
  $i=0;
  for ($files = 0, $num_items; $files < $num_items; $files++) {

  	$i++;
  	$aux=($aux%2)+1;

    $entry = $contents[$files];

    echo '<tr class="' . (($entry == $_GET['file'])? 'current_list' : 'taula' . $aux) . '">' . "\n"
     . '  <td>' . $entry . '</td>' . "\n"
     . '  <td>' . date(PHP_DATE_TIME_FORMAT, filemtime(DIR_WS_BACKUPS . $entry)) . '</td>' . "\n"
     . '  <td>' . number_format(filesize(DIR_WS_BACKUPS . $entry)) . 'bytes</td>' . "\n"
     . '  <td style="text-align:center;">'
     . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','name','x','y')) . 'file=' . $entry . '&amp;action=download') . '">' . ICON_DOWNLOAD . '</a> | '
     . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','name','x','y')) . 'file=' . $entry . '&amp;action=restore') . '" class="restore">' . ICON_RESTORE . '</a> | '
     . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','name','x','y')) . 'file=' . $entry . '&amp;action=delete') . '" class="delete">' . ICON_DELETE . '</a>'
     . '</td>' . "\n"
     . '</tr>' . "\n";
  }

} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="4" class="no_item">' . sprintf(NO_ITEMS,mb_strtolower(NEW_ITEM,'UTF-8')) . '</td>' . "\n" ;
  echo '</tr>' . "\n";

}
?>

  </tbody>
</table>
<p class="botonera">
  <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','x','y')) . 'action=backup') . '" class="send_form"><span>' . NEW_ITEM . '</span></a>'; ?>
</p>

<div id="buscador">
  <?php
  echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action','x','y')) . 'action=restore_local'), 'post', 'enctype="multipart/form-data"')
   .  '<label><span>' . UPLOAD_BACKUP . '</span><input type="file" name="sql_file" value="" size="25" class="file" /></label>'
   . ' | ' . '<a href="javascript:void(0);" class="check_form">GO!</a>';
  ?>
  </form>
</div>
