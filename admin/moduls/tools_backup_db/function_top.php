<?php

// include compress class
require(DIR_WS_CLASSES . 'dUnzip.php');
require(DIR_WS_CLASSES . 'dZip.php');

// Controlem si existeix el directori per backups
$dir_ok = false;
$error = false;
if (is_dir(tep_get_local_path(DIR_WS_BACKUPS))) {
    $dir_ok = true;
    if (!is_writeable(tep_get_local_path(DIR_WS_BACKUPS))) {
        $messageStack->add(ERROR_BACKUP_DIRECTORY_NOT_WRITEABLE, 'error');
        $error = true;
    }
} else {
    $messageStack->add(ERROR_BACKUP_DIRECTORY_DOES_NOT_EXIST, 'error');
    $error = true;
}

// Funció per ordenar per data fitxer
function ordenar($a, $b) {
    $a = filemtime(DIR_WS_BACKUPS . $a);
    $b = filemtime(DIR_WS_BACKUPS . $b);
    if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? -1 : 1;
}

// Si existeix fem un array amb els fitxers
if ($dir_ok) {
    $contents = array();
    $dir = dir(DIR_WS_BACKUPS);
    while ($file = $dir->read()) {
        if (!is_dir(DIR_WS_BACKUPS . $file)) {
            $contents[] = $file;
        }
    }
    usort($contents, "ordenar");
    $num_items = sizeof($contents);
}

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'backup':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            tep_set_time_limit(0);
            // Donem nom
            $backup_file = 'db_' . DB_DATABASE . '-' . date('YmdHis') . '.sql';

            $fp = fopen(DIR_WS_BACKUPS . $backup_file, 'w');

            $schema = '# Database Backup For ' . WEB_NAME . "\n" .
                    '# Copyright (c) ' . date('Y') . ' ' . WEB_OWNER . "\n" .
                    '#' . "\n" .
                    '# Database: ' . DB_DATABASE . "\n" .
                    '# Database Server: ' . DB_SERVER . "\n" .
                    '#' . "\n" .
                    '# Backup Date: ' . date(PHP_DATE_TIME_FORMAT) . "\n\n";

            fputs($fp, $schema);

            $tables_query = tep_db_query('show tables');
            while ($tables = tep_db_fetch_array($tables_query)) {
                list(, $table) = each($tables);

                $schema = 'drop table if exists ' . $table . ';' . "\n" .
                        'create table ' . $table . ' (' . "\n";

                $table_list = array();
                $fields_query = tep_db_query("show fields from " . $table);
                while ($fields = tep_db_fetch_array($fields_query)) {
                    $table_list[] = $fields['Field'];

                    $schema .= '  ' . $fields['Field'] . ' ' . $fields['Type'];

                    if (strlen($fields['Default']) > 0)
                        $schema .= ' default \'' . $fields['Default'] . '\'';

                    if ($fields['Null'] != 'YES')
                        $schema .= ' not null';

                    if (isset($fields['Extra']))
                        $schema .= ' ' . $fields['Extra'];

                    $schema .= ',' . "\n";
                }

                $schema = preg_replace("/,\n$/", '', $schema);

                // Add the keys
                $index = array();
                $keys_query = tep_db_query("show keys from " . $table);
                while ($keys = tep_db_fetch_array($keys_query)) {
                    $kname = $keys['Key_name'];

                    if (!isset($index[$kname])) {
                        $index[$kname] = array('unique' => !$keys['Non_unique'],
                            'columns' => array());
                    }

                    $index[$kname]['columns'][] = $keys['Column_name'];
                }

                while (list($kname, $info) = each($index)) {
                    $schema .= ',' . "\n";

                    $columns = implode($info['columns'], ', ');

                    if ($kname == 'PRIMARY') {
                        $schema .= '  PRIMARY KEY (' . $columns . ')';
                    } elseif ($info['unique']) {
                        $schema .= '  UNIQUE ' . $kname . ' (' . $columns . ')';
                    } else {
                        $schema .= '  KEY ' . $kname . ' (' . $columns . ')';
                    }
                }

                $schema .= "\n" . ');' . "\n\n";
                fputs($fp, $schema);

                // Dump the data
                $rows_query = tep_db_query("select " . implode(',', $table_list) . " from " . $table);
                while ($rows = tep_db_fetch_array($rows_query)) {
                    $schema = 'insert into ' . $table . ' (' . implode(', ', $table_list) . ') values (';

                    reset($table_list);
                    while (list(, $i) = each($table_list)) {
                        if (!isset($rows[$i])) {
                            $schema .= 'NULL, ';
                        } elseif (tep_not_null($rows[$i])) {
                            $row = addslashes($rows[$i]);
                            $row = preg_replace("/\n#/", "\n" . '\#', $row);

                            $schema .= '\'' . $row . '\', ';
                        } else {
                            $schema .= '\'\', ';
                        }
                    }

                    $schema = preg_replace('/, $/', '', $schema) . ');' . "\n";
                    fputs($fp, $schema);
                }
            }

            fclose($fp);

            $messageStack->add_session(SUCCESS_DATABASE_SAVED, 'success');
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'file', 'x', 'y')) . 'file=' . $backup_file));
            break;

        case 'restore':
        case 'restore_local':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            tep_set_time_limit(0); // No considerem temps per evitar que interrompi script

            if ($_GET['action'] == 'restore') {
                $read_from = $_GET['file'];

                if (file_exists(DIR_WS_BACKUPS . $_GET['file'])) {
                    $restore_file = DIR_WS_BACKUPS . $_GET['file'];
                    $extension = substr($_GET['file'], -3);

                    if (($extension == 'sql')) {

                        $restore_from = $restore_file;
                        $remove_raw = false;

                        if (isset($restore_from) && file_exists($restore_from)) {
                            $fd = fopen($restore_from, 'rb');
                            $restore_query = fread($fd, filesize($restore_from));
                            fclose($fd);
                        }
                    }
                }
            } elseif ($_GET['action'] == 'restore_local') {
                $sql_file = tep_get_uploaded_file('sql_file');

                if (is_uploaded_file($sql_file['tmp_name'])) {
                    $restore_query = fread(fopen($sql_file['tmp_name'], 'r'), filesize($sql_file['tmp_name']));
                    $read_from = $sql_file['name'];
                }
            }

            if ($restore_query) {
                $sql_array = array();
                $sql_length = strlen($restore_query);
                $pos = strpos($restore_query, ';');
                for ($i = $pos; $i < $sql_length; $i++) {
                    if ($restore_query[0] == '#') {
                        $restore_query = ltrim(substr($restore_query, strpos($restore_query, "\n")));
                        $sql_length = strlen($restore_query);
                        $i = strpos($restore_query, ';') - 1;
                        continue;
                    }
                    if ($restore_query[($i + 1)] == "\n") {
                        for ($j = ($i + 2); $j < $sql_length; $j++) {
                            if (trim($restore_query[$j]) != '') {
                                $next = substr($restore_query, $j, 6);
                                if ($next[0] == '#') {
                                    // find out where the break position is so we can remove this line (#comment line)
                                    for ($k = $j; $k < $sql_length; $k++) {
                                        if ($restore_query[$k] == "\n")
                                            break;
                                    }
                                    $query = substr($restore_query, 0, $i + 1);
                                    $restore_query = substr($restore_query, $k);
                                    // join the query before the comment appeared, with the rest of the dump
                                    $restore_query = $query . $restore_query;
                                    $sql_length = strlen($restore_query);
                                    $i = strpos($restore_query, ';') - 1;
                                    continue 2;
                                }
                                break;
                            }
                        }
                        if ($next == '') { // get the last insert query
                            $next = 'insert';
                        }
                        if ((preg_match('/create/i', $next)) || (preg_match('/insert/i', $next)) || (preg_match('/drop t/i', $next))) {
                            $next = '';
                            $sql_array[] = substr($restore_query, 0, $i);
                            $restore_query = ltrim(substr($restore_query, $i + 1));
                            $sql_length = strlen($restore_query);
                            $i = strpos($restore_query, ';') - 1;
                        }
                    }
                }

                // Eliminem totes les taules
                $database_name = DB_DATABASE;
                $sql = "SHOW TABLES FROM $database_name";
                if ($result = tep_db_query($sql)) {
                    /* add table name to array */
                    while ($row = tep_db_fetch_row($result)) {
                        $found_tables[] = $row[0];
                    }
                }
                /* loop through and drop each table */
                foreach ($found_tables as $table_name) {
                    $sql = "DROP TABLE $database_name.$table_name";
                    tep_db_query($sql);
                }

                // Restaurem taules de la còpia de seguretat
                for ($i = 0, $n = sizeof($sql_array); $i < $n; $i++) {
                    tep_db_query($sql_array[$i]);
                }

                $messageStack->add_session(SUCCESS_DATABASE_RESTORED, 'success');
            } else {
                $messageStack->add_session(ERROR_DATABASE_NOT_RESTORED, 'error');
            }
            if (isset($remove_raw) && ($remove_raw == true)) {
                unlink($restore_from);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'delete':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if (strstr($_GET['file'], '..'))
                tep_redirect(tep_href_link(basename($PHP_SELF)));

            tep_remove(DIR_WS_BACKUPS . '/' . $_GET['file']);

            if (!$tep_remove_error) {
                $messageStack->add_session(SUCCESS_BACKUP_DELETED, 'success');
            } else {
                $messageStack->add_session(ERROR_FILE_NOT_REMOVEABLE, 'error');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'file', 'x', 'y'))));
            break;

        case 'download':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['file']) && !empty($_GET['file'])) {
                // Forcem la descàrrega si existeix l'arxiu
                $root = DIR_WS_BACKUPS;
                $file = basename($_GET['file']);
                $path = $root . $file;
                $type = '';

                if (is_file($path)) {
                    tep_force_download($path, $file);
                    exit();
                }
            } else {
                $messageStack->add_session(ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE, 'error');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'file', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
