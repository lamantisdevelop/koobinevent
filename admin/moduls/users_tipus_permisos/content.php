 <h2 class="users"><?php echo TITLE_PAGE; ?></h2> 

<?php
if ($_GET['action'] == 'new') {
    // FITXA
    //Obtenim dades del rol si fem update / cas insert dades buides
    if (isset($_GET['uID']) && tep_not_null($_GET['uID'])) {
        $id = $_GET['uID'];
        $query_raw = "select * from " . TABLE_ADMIN_USERS_TIPUS_PERMISOS . " WHERE id_tipus_permis=" . $id;
        $item = tep_db_fetch_array(tep_db_query($query_raw));
    } else {
        $item = array('id_rol' => '',
            'nom' => ''
        );
    }
    $itemInfo = new objectInfo($item);

    //Llistat de moduls
    $moduls = tep_moduls_get_all();

    //mirem si estem fent update o insert
    $form_action = (isset($_GET['uID'])) ? 'update' : 'insert';

    echo(TEXT_FORM);
    ?>
    <div id="form">
        <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
        <fieldset>
            <h3><?php echo $itemInfo->nom_permis; ?></h3>
            <label class="width_50_1">
                <span>Id*</span>
                <input type="text" name="id_tipus_permis" value="<?php echo $itemInfo->id_tipus_permis; ?>" />
            </label>
            <label class="width_50_2">
                <span>Nom*</span>
                <input type="text" name="name" value="<?php echo $itemInfo->nom_permis; ?>" />
            </label>

        </fieldset>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
    </form>
    </div>

    <?php
} else {
    // Llistat
    $query_raw = "SELECT ur.id_tipus_permis, ur.nom_permis FROM " . TABLE_ADMIN_USERS_TIPUS_PERMISOS . " ur ORDER BY ur.id_tipus_permis";
    $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
    $listing = tep_db_query($query_raw);

    echo(TEXT_LIST);
    ?>
    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>
    <table summary="<?php echo TAULA; ?>" class="llista fixed">
        <thead>
            <tr>
                <th style="width:5%;"><?php echo COL_1; ?></th>
                <th style="width:75%"><?php echo COL_2; ?></th>
                <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="7">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'action', 'x', 'y'))); ?></div>
    </th>
    </tr>
    </tfoot>
    <tbody>

        <?php
        if ($query_numrows > 0) {
            // Llistat segons búsqueda
            $i = 0;
            while ($item = tep_db_fetch_array($listing)) {
                $i++;
                $aux = ($aux % 2) + 1;
                $itemInfo = new objectInfo($item);

                echo '<tr class="' . (($itemInfo->id_tipus_permis == $_GET['uID']) ? 'current_list' : 'taula' . $aux) . '" onmouseover="this.className=\'sobre\';" onmouseout="this.className=\'' . (($itemInfo->id_tipus_permis == $_GET['uID']) ? 'current_list' : 'taula' . $aux) . '\';">' . "\n"
                . '  <td>' . $itemInfo->id_tipus_permis . '</td>' . "\n"
                . '  <td>' . $itemInfo->nom_permis . '</td>' . "\n"
                . '  <td style="text-align:center;" class="accions">'
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'buscar', 'x', 'y')) . 'action=new&amp;uID=' . $itemInfo->id_tipus_permis) . '">' . ICON_EDIT . '</a> | '
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'buscar', 'x', 'y')) . 'action=delete&amp;uID=' . $itemInfo->id_tipus_permis) . '" class="delete">' . ICON_DELETE . '</a>'
                . '</td>' . "\n"
                . '</tr>' . "\n";
            }
        } else {
            // No Items
            echo '<tr>' . "\n";
            echo '  <td colspan="7" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
            echo '</tr>' . "\n";
        }
        ?>

    </tbody>
    </table>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>

    <?php
}
?>
