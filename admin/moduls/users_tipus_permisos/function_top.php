<?php

// Enviament Mails
require_once(DIR_WS_CLASSES . 'class.phpmailer.php');
require_once(DIR_WS_CLASSES . 'class.smtp.php');

// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
        case 'insert':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_POST['name'])) {
                //mirem permisos
                tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
                // Comprovem que no existeixi un rol amb el mateix id
                $user_query = tep_db_query("select count(*) as total from " . TABLE_ADMIN_USERS_TIPUS_PERMISOS . " where id_tipus_permis = '" . tep_db_prepare_input($_POST['id_tipus_permis']) . "'");
                $user_mail = tep_db_fetch_array($user_query);
                $aux_duplicate = (int) $user_mail['total'];
                if ($aux_duplicate == 0) {
                    // Vector entrades
                    $sql_data_array = array('id_tipus_permis' => tep_db_prepare_input($_POST['id_tipus_permis']),
                        'nom_permis' => tep_db_prepare_input($_POST['name'])
                    );

                    // Crida base dades
                    tep_db_perform(TABLE_ADMIN_USERS_TIPUS_PERMISOS, $sql_data_array);
                    $aux_id = tep_db_insert_id();
                } else {
                    $messageStack->add_session(sprintf(TEXT_EXISTENT_ROL, $_POST['id_tipus_permis']), 'error');
                    tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y'))));
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'uID=' . $aux_id));
            break;


        case 'update':
            //mirem permisos
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['uID']) && isset($_POST['name'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['uID']);

                // Comprovem que no existeixi un usuari amb el mateix mail
                $user_query = tep_db_query("select count(*) as total from " . TABLE_ADMIN_USERS_TIPUS_PERMISOS . " where id_tipus_permis = '" . tep_db_prepare_input($_POST['id_tipus_permis']) . "'");
                $user_mail = tep_db_fetch_array($user_query);
                $aux_duplicate = (int) $user_mail['total'];
                //mirem si s'esta utilitzant un id nou k ja existeixi 
                if ($aux_duplicate == 0 || tep_db_prepare_input($_POST['id_tipus_permis']) == $aux_id) {
                    // Vector entrades
                    $sql_data_array = array('id_tipus_permis' => (int) $aux_id,
                        'nom_permis' => tep_db_prepare_input($_POST['name'])
                    );

                    // Crida base dades Rol
                    tep_db_perform(TABLE_ADMIN_USERS_TIPUS_PERMISOS, $sql_data_array, 'update', "id_tipus_permis = '" . (int) $aux_id . "'");
                }

                $messageStack->add_session(sprintf(TEXT_USER_UPDATED, $_POST['name']), 'success');
            } else {
                $messageStack->add_session(sprintf(TEXT_EXISTENT_ROL, $_POST['id_rol']), 'error');
                tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'uID', 'x', 'y'))));
            }

            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;


        case 'delete':
            //mirem permisos
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['uID']) {
                $aux_id = tep_db_prepare_input($_GET['uID']);
                tep_db_query("delete from " . TABLE_ADMIN_USERS_TIPUS_PERMISOS . " where id_tipus_permis = '" . (int) $aux_id . "'");
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'uID=' . $aux_id));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
