<h2 class="tools"><?php echo TITLE_PAGE . ((isset($_GET['list']) && tep_not_null($_GET['list'])) ? ': ' . tep_get_name_list($_GET['list']) : ''); ?></h2>
<?php echo (($_GET['action']== 'edit')? TEXT_FORM : TEXT_LIST); ?>

<?php
if ($_GET['action'] == 'edit') {
?>
<div id="form">
  <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action','pos','x','y')) . 'action=save'), 'post', 'enctype="multipart/form-data"'); ?>
  <fieldset>
    <?php echo tep_draw_hidden_field('filename',$_GET['filename']); ?>
    <label>
       <span>Texts web: <b> <?php echo $_GET['filename']; ?></b></span>
      <br/>
      <textarea name="contents" id="code" rows="10" cols="10"><?php echo ($contents); ?></textarea>
    </label>
  </fieldset>
  <p class="botonera">
    <?php
    echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SAVE . '</span></a>';
    echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','filename','buscar','x','y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
    ?>
  </p>
  </form>
</div>

<?php
} else {
// Construim array amb els fitxers del directori
$contents = array();

$file = $_GET['lngdir'] . '.php';
if (is_file($current_path . $file)) {
  $contents[] = array('name' => $file,
          	'size' => number_format(filesize($current_path . $file)),
                'last_modified' => filemtime($current_path . $file));
}

if (is_dir($current_path  . $_GET['lngdir'])) {
  $dir = dir($current_path  . $_GET['lngdir']);

  while ($file = $dir->read()) {
    if ( ($file != '.') && ($file != 'CVS') && ($file != '..') && !is_dir($current_path . $_GET['lngdir'] . '/' . $file)) {

      $contents[] = array('name' => $file,
        	    'size' => number_format(filesize($current_path  . $_GET['lngdir'] . '/' . $file)),
                    'last_modified' => filemtime($current_path  . $_GET['lngdir'] . '/' . $file));
    }
  }
  $dir->close();
}
$num_items  = sizeof($contents);

// Ordenem llista
switch ($_GET['sort_by']) {
    case 'name_desc':
      // Ordenem per nom desc
      function ordenar_nom_asc($a,$b){
        $a = $a['name'];
        $b = $b['name'];
        if ($a == $b) {
          return 0;
        }
        return ($a < $b) ? -1 : 1;
      }
      usort($contents, "ordenar_nom_asc");
      break;

    case 'name_asc':
      // Ordenem per nom asc
      function ordenar_nom_desc($a,$b){
        $a = $a['name'];
        $b = $b['name'];
        if ($a == $b) {
          return 0;
        }
        return ($a > $b) ? -1 : 1;
      }
      usort($contents, "ordenar_nom_desc");
      break;

    case 'size_asc':
      // Ordenem per tamany asc
      function ordenar_tamany_asc($a,$b){
   	    $a = str_replace(',','',$a['size']);
   		$b = str_replace(',','',$b['size']);
        if ($a == $b) {
          return 0;
        }
        return ($a < $b) ? -1 : 1;
      }
      usort($contents, "ordenar_tamany_asc");
      break;

    case 'size_desc':
      // Ordenem per tamany desc
      function ordenar_tamany_desc($a,$b){
   	    $a = str_replace(',','',$a['size']);
   		$b = str_replace(',','',$b['size']);
        if ($a == $b) {
          return 0;
        }
        return ($a > $b) ? -1 : 1;
      }
      usort($contents, "ordenar_tamany_desc");
      break;

    case 'date_desc':
      // Ordenem per nom desc
      function ordenar_data_asc($a,$b){
        $a = date("Ymd",$a['last_modified']);
        $b = date("Ymd",$b['last_modified']);
        if ($a == $b) {
          return 0;
        }
        return ($a > $b) ? -1 : 1;
      }
      usort($contents, "ordenar_data_asc");
      break;

    case 'date_asc':
      // Ordenem per nom asc
      function ordenar_data_desc($a,$b){
        $a = date("YmdHis",$a['last_modified']);
        $b = date("YmdHis",$b['last_modified']);
        if ($a == $b) {
          return 0;
        }
        return ($a < $b) ? -1 : 1;
      }
      usort($contents, "ordenar_data_desc");
      break;
}

?>

<table summary="<?php echo TAULA; ?>" class="llista fixed">
  <thead>
    <tr>
      <th style="width:45%;">
        <?php
        if ($_GET['sort_by']=="name_asc") {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=name_desc') . '" class="asc">' . COL_1 . '</a>';
        } else if ($_GET['sort_by']=="name_desc") {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=name_asc') . '" class="desc">' . COL_1 . '</a>';
        } else {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=name_desc') . '">' . COL_1 . '</a>';
        }
        ?>
      </th>
      <th style="width:20%;">
        <?php
        if ($_GET['sort_by']=="size_asc") {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=size_desc') . '" class="asc">' . COL_2 . '</a>';
        } else if ($_GET['sort_by']=="size_desc") {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=size_asc') . '" class="desc">' . COL_2 . '</a>';
        } else {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=size_desc') . '">' . COL_2 . '</a>';
        }
        ?>
      </th>
      <th style="width:20%;">
        <?php
        if ($_GET['sort_by']=="date_asc") {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=date_desc') . '" class="asc">' . COL_3 . '</a>';
        } else if ($_GET['sort_by']=="date_desc") {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=date_asc') . '" class="desc">' . COL_3 . '</a>';
        } else {
          echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action','sort_by','x','y')) . 'sort_by=date_desc') . '">' . COL_3 . '</a>';
        }
        ?>
      </th>
      <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="4">
        <div id="split_left"><?php echo sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS,sizeof($contents))?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if (sizeof($contents)>0) {
  // Llistat segons búsqueda
  for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
  	$aux=($i%2)+1;

  	echo '<tr class="' . (($contents[$i]['name'] == $_GET['filename'])? 'current_list' : 'taula' . $aux) . '">' . "\n"
     . '  <td>' . ICON_NEWS . $contents[$i]['name']. '</td>' . "\n"
     . '  <td>' . $contents[$i]['size'] . ' bytes' . '</td>' . "\n"
     . '  <td>' . strftime("%d/%m/%Y&nbsp;&nbsp;%H:%M:%S",$contents[$i]['last_modified']) . '</td>' . "\n"
     . '  <td style="text-align:center;">'
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','filename','buscar','x','y')) . 'action=edit&amp;filename=' . $contents[$i]['name']) . '">' . ICON_EDIT . '</a>'
     . '</td>' . "\n"
     . '</tr>' . "\n";
  }

} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="4" class="no_item">' . TEXT_NO_ITEMS . '<br/>' . $current_path . $_GET['lngdir'] . '</td>' . "\n" ;
  echo '</tr>' . "\n";

}
?>

  </tbody>
</table>

<div id="buscador">
  <?php
  echo tep_draw_form('form_search', tep_href_link('', tep_get_all_get_params(array('action','x','y'))), 'get');
  // Recuperem totes les variables $_GET
  reset($_GET);
  while (list($key, $value) = each ($_GET)) {
    if ( ($key != 'lngdir') && ($key != 'action') && ($key != 'x') && ($key != 'y') && ($key != tep_session_name()) ) {
      if (tep_not_null($value)) echo tep_draw_hidden_field(rawurldecode($key), rawurldecode($value));
    }
  }
  // Variables buscador
  echo '<label class="options"><span>' . SEARCH . '</span>' . tep_draw_pull_down_menu('lngdir',$languages_array,$_GET['lngdir'],'  class="llarg" onchange="send_form(\'form_search\'); return false;"') . '</label>';
  ?>
  </form>
</div>

<?php
}
?>
