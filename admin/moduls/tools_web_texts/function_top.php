<?php

// Creem Array Idiomes
$languages = tep_get_languages();

// Ordre per defecte
if (!isset($_GET['sort_by']))
    $_GET['sort_by'] = 'name_desc';

// Array idiomes
$languages_array = array();
$languages = tep_get_languages();
$lng_exists = false;
for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    if ($languages[$i]['directory'] == $_GET['lngdir'])
        $lng_exists = true;

    $languages_array[] = array('id' => $languages[$i]['directory'],
        'text' => $languages[$i]['name']);
}

// Definim vector per ordenar taula usuaris
$orders_array = array(array('id' => '', 'text' => ORDER_DATA),
    array('id' => 'name', 'text' => ORDER_NAME),
    array('id' => 'size', 'text' => ORDER_TAMANY));
// Array idiomes
$languages_array = array();
$languages = tep_get_languages();
$lng_exists = false;
for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    if ($languages[$i]['directory'] == $_GET['lngdir'])
        $lng_exists = true;

    $languages_array[] = array('id' => $languages[$i]['directory'],
        'text' => $languages[$i]['name']);
}

if (!$lng_exists)
    $_GET['lngdir'] = $language;

$current_path = DIR_FS_PORTAL_TEXTS;

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'edit':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['filename'] == $_GET['lngdir'] . '.php') {
                $file = DIR_FS_PORTAL_TEXTS . $_GET['filename'];
            } else {
                $file = DIR_FS_PORTAL_TEXTS . $_GET['lngdir'] . '/' . $_GET['filename'];
            }

            if (file_exists($file)) {
                $file_array = file($file);
                $contents = implode('', $file_array);
                //$contents_array = explode('define',$contents);

                if (!is_writeable($file)) {
                    // Si no podem escriure error i no la deixem editar
                    $messageStack->add_session(ERROR_FILE_NOT_WRITABLE, 'error');
                    // Redirecció
                    tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'filename', 'x', 'y'))));
                }
            } else {
                // Si no existeix error i no la deixem editar
                $messageStack->add_session(ERROR_FILE_NOT_EXISTS, 'error');
                // Redirecció
                tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'filename', 'x', 'y'))));
            }
            break;

        case 'save':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['lngdir']) && isset($_POST['filename'])) {
                if ($_GET['filename'] == $_GET['lngdir'] . '.php') {
                    $file = DIR_FS_PORTAL_TEXTS . $_POST['filename'];
                } else {
                    $file = DIR_FS_PORTAL_TEXTS . $_GET['lngdir'] . '/' . $_POST['filename'];
                }

                if (file_exists($file)) {
                    if (file_exists('bak' . $file)) {
                        @unlink('bak' . $file);
                    }

                    @rename($file, 'bak' . $file);

                    $new_file = fopen($file, 'w');
                    if (!get_magic_quotes_gpc()) {
                        $file_contents = trim($_POST['contents']);
                    } else {
                        $file_contents = stripslashes($_POST['contents']);
                    }
                    fwrite($new_file, $file_contents, strlen($file_contents));
                    fclose($new_file);

                    $messageStack->add(SUCCESS_FILE_EDIT, 'success');
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
