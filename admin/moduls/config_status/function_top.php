<?php
// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
  switch ($_GET['action']) {
    case 'insert':
      if ($_POST['name']) {
        // Busquem nou ordre
        $new_id = tep_search_new_id(TABLE_ORDERS_STATUS);
        $order_now = tep_search_new_order(TABLE_ORDERS_STATUS);

        // Entrades segons idioma
	$name_array = $_POST['name'];
	for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $lang_aux = $languages[$i]['id'];
          // Insertem dades
          $sql_data_array = array('id' => tep_db_prepare_input($new_id),
             		          'language_id' => (int)$lang_aux,
        			  'name' => tep_db_prepare_input($name_array[$lang_aux]),
                                  'listorder' => (int)$order_now);
          // Crida base dades
          tep_db_perform(TABLE_ORDERS_STATUS, $sql_data_array);
        }
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y')) . 'osID=' . $new_id));
      break;

    case 'update':
      if ($_GET['osID'] && $_POST['name']) {
        // Identificador
        $aux_id = tep_db_prepare_input($_GET['osID']);

        // Entrades segons idioma
	$name_array = $_POST['name'];
	for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $lang_aux = $languages[$i]['id'];
          // Insertem dades
          $sql_data_array = array('name' => tep_db_prepare_input($name_array[$lang_aux]));
          // Crida base dades
          tep_db_perform(TABLE_ORDERS_STATUS, $sql_data_array, 'update', 'id = \'' . (int)$aux_id . '\' and language_id=\'' . $lang_aux . '\'' );
        }
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y'))));
      break;

    case 'delete':
      if ($_GET['osID']) {
        $aux_id = tep_db_prepare_input($_GET['osID']);
        // Busquem i modifiquem ordre
        $order_now = tep_search_item_order($aux_id,TABLE_ORDERS_STATUS);
        tep_reorder_items($order_now,TABLE_ORDERS_STATUS);
        // Eliminem entrada
        tep_db_query("delete from " . TABLE_ORDERS_STATUS . " where id = '" . (int)$aux_id . "'");
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','osID','x','y'))));
      break;

    case 'listorder':
      if ($_GET['osID'] && isset($_GET['value'])) {
        // Identificador
        $aux_id = tep_db_prepare_input($_GET['osID']);
        // Trobar posicio actual
        $order_now = tep_search_item_order($aux_id,TABLE_ORDERS_STATUS);
        $pos_aux = (($_GET['value']=='up') ? $order_now + 1 : $order_now -1 );
        // Busquem id pos_aux per intercanviar posicions
        tep_change_order($aux_id,$order_now,$pos_aux,TABLE_ORDERS_STATUS);
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','value','x','y'))));
      break;
  }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
