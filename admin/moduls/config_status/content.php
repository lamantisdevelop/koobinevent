<h2 class="pages"><?php echo TITLE_PAGE; ?></h2>
<?php echo (($_GET['action']== 'new')? TEXT_FORM : TEXT_LIST); ?>   

<?php
if ($_GET['action'] == 'new') {
  // Fitxa
  if (isset($_GET['osID']) && tep_not_null($_GET['osID'])) {
    $id = $_GET['osID'];
    $query_raw = "select id from " . TABLE_ORDERS_STATUS . " where id = '" . (int)$id . "'";
    $item = tep_db_fetch_array(tep_db_query($query_raw));
  } else {
    $item = array('id' => '');
  }
  $itemInfo = new objectInfo($item);

  $form_action = ($_GET['osID']) ? 'update' : 'insert';
?>

<div id="form">
  <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action','pos','x','y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
  <!-- Idiomes -->
  <?php
  if (sizeof($languages)>1) {
  ?>
    <ul id="menuIdioma">
    <?php
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      echo '<li><a class="tab ' . (($i==0) ? 'current':'')  . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
    }
    ?>
    </ul>
  <?php
  }
  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
  // Busquem valors
    $aux_listing = tep_db_query("select os.name from " . TABLE_ORDERS_STATUS . " os where os.id = '" . (int)$id . "' and os.language_id = '" . (int)$languages[$i]['id'] . "'");
    $aux_numrows = tep_db_num_rows($aux_listing);
    if ($aux_numrows>0) {
      $aux_item = tep_db_fetch_array($aux_listing);
      $auxInfo = new objectInfo($aux_item);
    }
    ?>
    <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i==0) ? 'style="display:block;"' : 'style="display:none;"');?>>
      <label>
        <span>Texto*</span>
        <input type="name" name="name[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->name; ?>" />
      </label>
    </fieldset>
    <?php
    }
    ?>
    <p class="botonera">
      <?php
      echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
      echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','x','y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
      ?>
    </p>
  </form>
</div>

<?php
} else {
  // Llistat
  $query_raw = "select os.id, os.name, os.listorder from " . TABLE_ORDERS_STATUS . " os where os.language_id = '" . $language_id . "' order by os.listorder";
  $listing = tep_db_query($query_raw);
  $query_numrows = tep_db_num_rows($listing);
?>

<table summary="<?php echo TAULA; ?>" class="llista fixed">
  <thead>
    <tr>
      <th style="width:70%;"><?php echo COL_1; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_2; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_3; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="4">
        <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS,$query_numrows)?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if ($query_numrows>0) {
  // Llistat segons búsqueda
  $i=0;
  while ($item = tep_db_fetch_array($listing)) {
    $i++;
    $aux=($aux%2)+1;
    $itemInfo = new objectInfo($item);

    $aux_order = '';
    if ($itemInfo->listorder > 1) {
      $aux_order .= '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','osID','value','buscar','x','y')) . 'action=listorder&amp;value=down&amp;osID=' . $itemInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
    } else {
      $aux_order .= ICON_ORDER_NO;
    }
    $aux_order .= '&nbsp;|&nbsp;' . (($itemInfo->listorder < 10) ? '0' : '') . $itemInfo->listorder . '&nbsp;|&nbsp;';
    if ($itemInfo->listorder < $query_numrows) {
      $aux_order .= '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','osID','value','buscar','x','y')) . 'action=listorder&amp;value=up&amp;osID=' . $itemInfo->id) . '">' . ICON_ORDER_UP . '</a>';
    } else {
      $aux_order .= ICON_ORDER_NO;
    }

    echo '<tr class="' . (($itemInfo->id == $_GET['osID'])? 'current_list' : 'taula' . $aux) . '" onmouseover="this.className=\'sobre\';" onmouseout="this.className=\'' . (($itemInfo->id == $_GET['osID'])? 'current_list' : 'taula' . $aux) . '\';">' . "\n"
     . '  <td>' . $itemInfo->name . '</td>' . "\n"
     . '  <td style="text-align:center;">' . $aux_order . '</td>' . "\n"
     . '  <td style="text-align:center;">'
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','osID','buscar','x','y')) . 'action=new&amp;osID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a> | '
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','osID','buscar','x','y')) . 'action=delete&amp;osID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>'
     . '</td>' . "\n"
     . '</tr>' . "\n";
  }
} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="3" class="no_item">' . sprintf(NO_ITEMS,mb_strtolower(NEW_ITEM,'UTF-8')) . '</td>' . "\n" ;
  echo '</tr>' . "\n";
}
?>

  </tbody>
</table>

<p class="botonera">
  <?php echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','osID','x','y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
</p>

<?php
}
?>
