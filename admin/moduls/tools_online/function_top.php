<?php
require_once(DIR_WS_CLASSES . 'browser.php');
// x Països
require_once(DIR_WS_INCLUDES . 'geoip.inc');
$gi = geoip_open(DIR_WS_INCLUDES . 'GeoIP.dat', GEOIP_STANDARD);

// Borrem entrades que han expirat, temps: 10 Minuts
$mins_ago = (time() - 600);
tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where time_last_click < '" . $mins_ago . "'");

// Accions
if ($_GET['action']) {
  switch ($_GET['action']) {
    case 'initialize':
      // Buidem la taula
      tep_db_query("truncate table " . TABLE_WHOS_ONLINE);
      // Redireccionem
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y'))));
      break;
  }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
