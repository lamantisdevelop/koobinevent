<?php
/*
JavascriptScript específic pel mòdul
*/

?>
<script type="text/javascript" language="javascript">
//<![CDATA[

$(document).ready(function() {
    $('.init').click(function(evento) {
	  if(confirm('<?php echo addslashes(TEXT_INIT); ?>')) {
        $('#pagina').block({
          message: '<h1><img src="image/loading.gif"/>&nbsp;&nbsp;Processant...<\/h1>'
        });
      } else {
          evento.preventDefault();
      }
    });
});

// Actualitzem la pàgina cada 5 segons
//setTimeout('document.location.reload()',30000);

//]]>
</script>
</script>
