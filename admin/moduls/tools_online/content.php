<h2 class="tools"><?php echo TITLE_PAGE; ?></h2>
<?php echo TEXT_LIST; ?>   

<?php
  // Llistat
  $query_raw = "select id, name, session_id, ip_address, user_agent, time_entry, time_last_click, last_page_url from " . TABLE_WHOS_ONLINE . " order by time_last_click desc";
  $listing = tep_db_query($query_raw);
  $query_numrows = tep_db_num_rows($listing);
?>

<table summary="<?php echo TAULA; ?>" class="llista">
  <thead>
    <tr>
      <th style="width:10%;"><?php echo COL_1; ?></th>
      <th style="width:10%;"><?php echo COL_2; ?></th>
      <th style="width:8%;"><?php echo COL_3; ?></th>
      <th style="width:13%;"><?php echo COL_4; ?></th>
      <th style="width:13%;"><?php echo COL_6; ?></th>
      <th style="width:18%;"><?php echo COL_5; ?></th>
      <th style="width:28%;"><?php echo COL_7; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="7">
        <div id="split_left"><?php echo sprintf(TEXT_DISPLAY_NUMBER_OF_ONLINE,strftime("%H:%M:%S"),$query_numrows)?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if ($query_numrows>0) {
  // Eina Paisos
  require_once(DIR_WS_INCLUDES . 'geoip.inc');
  $gi = geoip_open(DIR_WS_INCLUDES . 'GeoIP.dat', GEOIP_STANDARD);

  // Llistat segons búsqueda
  $i=0;
  while ($item = tep_db_fetch_array($listing)) {
  	$i++;
  	$aux=($aux%2)+1;
    $itemInfo = new objectInfo($item);

    // Browser
    $browser = new Browser();
    $browser->setUserAgent($itemInfo->user_agent);
    $agent_aux = $browser->getBrowser() . ' ' . $browser->getVersion();

    echo '<tr class="taula' . $aux . '">' . "\n"
     . '  <td>' . date('H:i:s', $itemInfo->time_last_click)  . '</td>' . "\n"
     . '  <td>' . date('H:i:s', $itemInfo->time_entry) . '</td>' . "\n"
     . '  <td>' . (($itemInfo->id) ? $itemInfo->id : 'Guest') . '</td>' . "\n"
     . '  <td>' . $itemInfo->ip_address . '</td>' . "\n"
     . '  <td>' . geoip_country_name_by_addr($gi, $itemInfo->ip_address) . '</td>' . "\n"
     . '  <td>' . $agent_aux . '</td>' . "\n"
     . '  <td>' . htmlentities($itemInfo->last_page_url) . '</td>' . "\n"
     . '</tr>' . "\n";
  }

} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="7" class="no_item">' . NO_USERS_ONLINE . '</td>' . "\n" ;
  echo '</tr>' . "\n";

}
?>

  </tbody>
</table>
<p class="botonera">
  <?php echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','x','y')) . 'action=initialize') . '" class="init"><span>' . BUTTON_INIT . '</span></a>'; ?>
</p>