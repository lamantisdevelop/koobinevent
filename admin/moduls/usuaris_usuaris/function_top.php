<?php
// Enviament Mails
require_once(DIR_WS_CLASSES . 'class.phpmailer.php');
require_once(DIR_WS_CLASSES . 'class.smtp.php');

// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
  switch ($_GET['action']) {
    case 'insert':
      tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
      if ($_POST['firstname']) {

        // Vector entrades
        $sql_data_array = array('firstname' => tep_db_prepare_input($_POST['firstname']),
	      		        'lastname' => tep_db_prepare_input($_POST['lastname']),
    				'dob' => tep_db_prepare_input($_POST['dob']),
    				'mail' => tep_db_prepare_input($_POST['mail']),
    				'tel' => tep_db_prepare_input($_POST['tel']),
    				'fax' => tep_db_prepare_input($_POST['fax']),
    				'active' => '0',
    				'entered' => 'now()',
    				'modified' => 'now()');

        // Crida base dades
        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
        $aux_id = tep_db_insert_id();

        // Llibreta direccions
        $sql_data_array = array('address_id' => '1',
                                'customers_id' => tep_db_prepare_input($aux_id),
    				'firstname' => tep_db_prepare_input($_POST['firstname']),
    				'lastname' => tep_db_prepare_input($_POST['lastname']),
      				'company' => tep_db_prepare_input($_POST['company']),
      				'nif' => tep_db_prepare_input($_POST['nif']),
    				'address' => tep_db_prepare_input($_POST['address']),
    				'postcode' => tep_db_prepare_input($_POST['postcode']),
    				'city' => tep_db_prepare_input($_POST['city']),
    				'country_id' => tep_db_prepare_input($_POST['country_id']),
    				'zone_id' => tep_db_prepare_input($_POST['zone_id']),
    				'state' => tep_db_prepare_input($_POST['state']));

        // Crida base dades
      	tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y')) . 'uID=' . $aux_id));
      break;


    case 'update':
      tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
      if ($_GET['uID'] && $_POST['firstname']) {
        // Identificador
        $aux_id = tep_db_prepare_input($_GET['uID']);

        // Vector entrades
        $sql_data_array = array('firstname' => tep_db_prepare_input($_POST['firstname']),
	      			'lastname' => tep_db_prepare_input($_POST['lastname']),
    				'dob' => tep_db_prepare_input($_POST['dob']),
    				'mail' => tep_db_prepare_input($_POST['mail']),
    				'tel' => tep_db_prepare_input($_POST['tel']),
    				'fax' => tep_db_prepare_input($_POST['fax']),
    				'modified' => 'now()');

        // Crida base dades
        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "id = '" . (int)$aux_id . "'");

        // Llibreta direccions
        $default_address_id = tep_db_prepare_input($_POST['default_address_id']);
        $sql_data_array = array('firstname' => tep_db_prepare_input($_POST['firstname']),
    			        'lastname' => tep_db_prepare_input($_POST['lastname']),
      				'company' => tep_db_prepare_input($_POST['company']),
      				'nif' => tep_db_prepare_input($_POST['nif']),
    				'address' => tep_db_prepare_input($_POST['address']),
    				'postcode' => tep_db_prepare_input($_POST['postcode']),
    				'city' => tep_db_prepare_input($_POST['city']),
    				'country_id' => tep_db_prepare_input($_POST['country_id']),
    				'zone_id' => tep_db_prepare_input($_POST['zone_id']),
    				'state' => tep_db_prepare_input($_POST['state']));

        // Crida base dades
        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$aux_id . "' and address_id = '" . (int)$default_address_id . "'");
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','x','y'))));
      break;


    case 'active':
      tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
      if ($_GET['uID']) {
        // Identificador
        $aux_id = tep_db_prepare_input($_GET['uID']);
        // Busquem dades usuari
        $user_query = tep_db_query("select firstname, lastname, mail from " . TABLE_CUSTOMERS . " where id = '" . (int)$aux_id . "'");
        $user = tep_db_fetch_array($user_query);

        // Generem password
  	$t1 = date("mdy");
	srand ((float) microtime() * 10000000);
	$input = array ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
	$rand_keys = array_rand ($input, 3);
	$l1 = $input[$rand_keys[0]];
	$r1 = rand(0,9);
	$l2 = $input[$rand_keys[1]];
	$l3 = $input[$rand_keys[2]];
	$r2 = rand(0,9);
	$password = "v".$l1.$r1.$l2.$l3.$r2;

        // Enviar mail password
	$mail = new PHPMailer();

	$mail->Host = SMTP_SERVER; // SMTP server
	$mail->From = EMAIL_FROM; // Mail
	$mail->FromName = EMAIL_FROM_NAME; // Nom
	$mail->AddAddress($user['mail'], $user['firstname'] . ' ' . $user['lastname']); // A qui l'enviem

        $content = sprintf(TEXT_MAIL_ACTIVE, $user['mail'], $password);

	$mail->Subject =  EMAIL_SUBJECT; // Titol
	$mail->Body = $content;
	$mail->WordWrap = 100;

    	$mail->IsHTML(true);

	if ($mail->Send()) {
          // Vector entrades
          $sql_data_array = array('password' => tep_encrypt_password($password),
                                  'active' => 1);
          // Crida base dades
          tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');

	  $messageStack->add_session(sprintf(USER_ACTIVE_SUCCESS, $user['firstname'] . ' ' . $user['lastname']), 'success');
	} else {
	  $messageStack->add_session(sprintf(USER_ACTIVE_ERROR, $user['firstname'] . ' ' . $user['lastname']), 'error');
        }
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','value','x','y'))));
      break;


    case 'desactive':
      tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
      if ($_GET['uID']) {
        // Identificador
        $aux_id = tep_db_prepare_input($_GET['uID']);
        // Vector entrades
        $sql_data_array = array('active' => 0);
        // Crida base dades
        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','value','x','y'))));
      break;


    case 'delete':
      tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
      if ($_GET['uID']) {
        $aux_id = tep_db_prepare_input($_GET['uID']);
        // Eliminem entrades
        tep_db_query("delete from " . TABLE_CUSTOMERS . " where id = '" . (int)$aux_id . "'");
        tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$aux_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$aux_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$aux_id . "'");
      }
      // Redirecció
      tep_redirect(tep_href_link('', tep_get_all_get_params(array('action','uID','x','y'))));
      break;
  }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
