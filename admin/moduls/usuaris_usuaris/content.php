<h2 class="pages"><?php echo TITLE_PAGE; ?></h2>
<?php echo (($_GET['action']== 'new')? TEXT_FORM : TEXT_LIST); ?>   

<?php
if ($_GET['action'] == 'new') {
  // Fitxa
  if (isset($_GET['uID']) && tep_not_null($_GET['uID'])) {
    $id = $_GET['uID'];
    $query_raw = "select c.id, c.firstname, c.lastname, c.dob, c.mail, c.tel, c.fax, c.default_address_id, a.company, a.nif, a.address, a.postcode, a.city, a.country_id, a.zone_id, a.state  from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.id=a.customers_id and c.default_address_id = a.address_id where c.id = '" . (int)$id . "'";
    $item = tep_db_fetch_array(tep_db_query($query_raw));
  } else {
    $item = array('id' => '',
	    'firstname' => '',
            'lastname' => '',
            'dob' => '',
            'mail' => '',
            'tel' => '',
            'fax' => '',
            'default_address_id' => '',
            'company' => '',
            'nif' => '',
            'address' => '',
            'postcode' => '',
            'city' => '',
            'country_id' => '',
            'zone_id' => '',
            'state' => '');
  }
  $itemInfo = new objectInfo($item);

  $form_action = ($_GET['uID']) ? 'update' : 'insert';
?>

<div id="form">
  <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action','pos','x','y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
  <?php echo tep_draw_hidden_field('default_address_id', $itemInfo->default_address_id); ?>
    <fieldset>
      <label class="width_30_1">
        <span>Nombre*</span>
        <input type="text" name="firstname" value="<?php echo $itemInfo->firstname; ?>" />
      </label>
      <label class="width_70_2">
        <span>Apellidos*</span>
        <input type="text" name="lastname" value="<?php echo $itemInfo->lastname; ?>" />
      </label>
      <label>
        <span>Empresa</span>
        <input type="text" name="company" value="<?php echo $itemInfo->company; ?>" />
      </label>
      <label class="width_50_1">
        <span>DNI/NIF</span>
        <input type="text" name="nif" value="<?php echo $itemInfo->nif; ?>" />
      </label>
      <label class="width_50_2">
        <span>Fecha Nacimiento</span>
        <input type="text"  name="dob" value="<?php echo (($itemInfo->dob)? tep_date_db($itemInfo->dob) : ''); ?>" class="camp_read date_input" />
        <a class="link_cal"></a>
      </label>
      <p class="form_sep"></p>
      <label>
        <span>Dirección</span>
        <input type="text" name="address" value="<?php echo $itemInfo->address; ?>" />
      </label>
      <label class="width_50_1">
        <span>Población/Ciudad </span>
        <input type="text" name="city" value="<?php echo $itemInfo->city; ?>" />
      </label>
      <label class="width_50_2">
        <span>Codigo Postal</span>
        <input type="text" name="postcode" value="<?php echo $itemInfo->postcode; ?>" />
      </label>
      <label class="width_50_1">
        <span>País*</span>
        <?php echo tep_draw_pull_down_menu('country_id', tep_get_countries(), (($itemInfo->country_id) ? $itemInfo->country_id : '195'), ' onchange="update_zone(this.form);"'); ?>
      </label>
      <label class="width_50_2"  id="label_zone_id">
        <span>Província*</span>
        <?php echo tep_draw_pull_down_menu('zone_id', tep_prepare_country_zones_pull_down(($itemInfo->country_id) ? $itemInfo->country_id : '195'), $itemInfo->zone_id, ' id="zone_id"'); ?>
      </label>
      <label class="width_50_2 amagat" id="label_state">
        <span>Província*</span>
        <input type="text" name="state" value="<?php echo $itemInfo->state; ?>" />
      </label>
      <p class="form_sep"></p>
      <label>
        <span>E-mail</span>
        <input type="text" name="mail" value="<?php echo $itemInfo->mail; ?>" />
      </label>
      <label class="width_50_1">
        <span>Teléfono</span>
        <input type="text" name="tel" value="<?php echo $itemInfo->tel; ?>" />
      </label>
      <label class="width_50_2">
        <span>Fax</span>
        <input type="text" name="fax" value="<?php echo $itemInfo->fax; ?>" />
      </label>
    </fieldset>
    <p class="botonera">
      <?php
      echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
      echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','uID','x','y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
      ?>
    </p>
  </form>
</div>

<?php
} else {
  // Llistat
  if (isset($_GET['keyword']) && tep_not_null($_GET['keyword'])) {
    $query_raw = "select c.id, c.firstname, c.lastname, c.entered, c.active, c.dob, a.city, a.country_id, a.zone_id, a.state from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.id = a.customers_id and c.default_address_id = a.address_id where (c.firstname like '%" . tep_db_input($_GET['keyword']) . "%' or c.lastname like '%" . tep_db_input($_GET['keyword']) . "%') order by c.id desc";
  } else {
    $query_raw = "select c.id, c.firstname, c.lastname, c.entered, c.active, c.dob, a.city, a.country_id, a.zone_id, a.state from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.id = a.customers_id and c.default_address_id = a.address_id order by c.id desc";
  }
  $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
  $listing = tep_db_query($query_raw);
?>

<table summary="<?php echo TAULA; ?>" class="llista fixed">
  <thead>
    <tr>
      <th style="width:5%;"><?php echo COL_1; ?></th>
      <th style="width:30%"><?php echo COL_2; ?></th>
      <th style="width:22%"><?php echo COL_3; ?></th>
      <th style="width:10%; text-align:center;"><?php echo COL_4; ?></th>
      <th style="width:10%"><?php echo COL_5; ?></th>
      <th style="width:8%; text-align:center;"><?php echo COL_6; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_7; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="7">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page','action','x','y'))); ?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if ($query_numrows>0) {
  // Llistat segons búsqueda
  $i=0;
  while ($item = tep_db_fetch_array($listing)) {
  	$i++;
  	$aux=($aux%2)+1;
    $itemInfo = new objectInfo($item);

    $orders_query = tep_db_query("select count(*) as num from " . TABLE_ORDERS . " where customers_id = '" . (int)$itemInfo->id . "'");
    $orders = tep_db_fetch_array($orders_query);

    if ($itemInfo->active) {
      $aux_active = ICON_CONFIRMED;
    } else {
      $aux_active = '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','uID','buscar','x','y')) . 'action=active&amp;uID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
    }

  	echo '<tr class="' . (($itemInfo->id == $_GET['uID'])? 'current_list' : 'taula' . $aux) . '" onmouseover="this.className=\'sobre\';" onmouseout="this.className=\'' . (($itemInfo->id == $_GET['uID'])? 'current_list' : 'taula' . $aux) . '\';">' . "\n"
     . '  <td>' . $itemInfo->id . '</td>' . "\n"
     . '  <td>' . $itemInfo->firstname . ' ' . $itemInfo->lastname . '</td>' . "\n"
     . '  <td>' . $itemInfo->city . ' (' . tep_get_country_name($itemInfo->country_id) . ')</td>' . "\n"
     . '  <td style="text-align:center;"><a href="' . tep_href_link('','modul=comandes_comandes&amp;uID=' . $itemInfo->id) . '">' . $orders['num'] . '</a></td>' . "\n"
     . '  <td>' . tep_date_short($itemInfo->entered) . '</td>' . "\n"
     . '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
     . '  <td style="text-align:center;">'
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','uID','buscar','x','y')) . 'action=new&amp;uID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a> | '
     . '<a href="' . tep_href_link('','modul=usuaris_contacte&amp;uID=' . $itemInfo->id) . '">' . ICON_MAIL . '</a> | '
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','uID','buscar','x','y')) . 'action=delete&amp;uID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>'
     . '</td>' . "\n"
     . '</tr>' . "\n";
  }
} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="7" class="no_item">' . sprintf(NO_ITEMS,mb_strtolower(NEW_ITEM,'UTF-8')) . '</td>' . "\n" ;
  echo '</tr>' . "\n";
}
?>

  </tbody>
</table>

<p class="botonera">
  <?php echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','uID','x','y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
</p>

<div id="buscador">
  <?php
  echo tep_draw_form('form_search', tep_href_link('', tep_get_all_get_params(array('action','x','y'))), 'get');
  // Recuperem totes les variables $_GET
  reset($_GET);
  while (list($key, $value) = each ($_GET)) {
    if ( ($key != 'tipus') && ($key != 'keyword') && ($key != 'action') && ($key != 'page') && ($key != 'x') && ($key != 'y') && ($key != tep_session_name()) ) {
      if (tep_not_null($value)) echo tep_draw_hidden_field(rawurldecode($key), rawurldecode($value));
    }
  }
  // Variables buscador
  echo '<label><span>' . SEARCH . '</span>'
    . '<input type="text" name="keyword" value="' . $_GET['keyword'] . '" />'
    . '</label>';
  ?>
  </form>
</div>

<?php
}
?>
