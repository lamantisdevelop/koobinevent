<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

// Validar formulari
function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}

function update_zone(theForm) {
  var NumState = theForm.zone_id.options.length;
  var SelectedCountry = '';

  while(NumState > 0) {
    NumState = NumState - 1;
    theForm.zone_id.options[NumState] = null;
  }

  SelectedCountry = theForm.country_id.options[theForm.country_id.selectedIndex].value;

  <?php echo tep_js_zone_list('SelectedCountry', 'theForm', 'zone_id'); ?>

  resetStateText(theForm);
}

function resetStateText(theForm) {
  if (theForm.zone_id.options.length > 1 && theForm.zone_id.selectedIndex == 0) {
    document.getElementById('label_state').style.display= "none";
    theForm.state.value = '';
    document.getElementById('label_zone_id').style.display= "block";
  } else {
    document.getElementById('label_zone_id').style.display= "none";
    document.getElementById('label_state').style.display= "block";
  }
}

//]]>
</script>
