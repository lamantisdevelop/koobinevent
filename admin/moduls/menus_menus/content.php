<h2 class="menus"><?php echo TITLE_PAGE . (tep_not_null($groupInfo->name) ? ': ' . $groupInfo->name : ''); ?></h2>
<?php
if (substr($_GET['action'], 0, 3) == 'new') {

    // Fitxa
    if ($_GET['action'] == 'new_item') {
        if (isset($_GET['iID']) && tep_not_null($_GET['iID'])) {
            $id = $_GET['iID'];
            $query_raw = "select i.id, i.parent_id, i.action, i.listorder from " . TABLE_MENUS_PAGES . " i where i.id = '" . (int) $id . "' and i.menus_id = '" . (int) $_GET['menu_id'] . "'";
            $item = tep_db_fetch_array(tep_db_query($query_raw));
        } else {
            $item = array('id' => '',
                'parent_id' => '',
                'action' => '',
                'listorder' => '');
        }
        $itemInfo = new objectInfo($item);

        $form_action = ($_GET['iID']) ? 'update_item' : 'insert_item';
        ?>

        <div id="form">
            <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                ?>
            </p>
            <fieldset>
                <label class="width_50_1">
                    <span class="menus amb_icona">Menú PARE*</span>
                    <?php echo tep_draw_pull_down_menu('parent_id', tep_get_menu_pages_tree($_GET['menu_id']), $itemInfo->parent_id, 'class="llarg"'); ?>
                </label>
                <label class="width_50_2">
                    <span>Ordre</span>
                    <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
                    <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
                </label>
                <p class="form_sep"></p>
                <label>
                    <span>Acció:</span>
                </label>
                <label class="inline tab" title="action_1">
                    <input type="radio" name="action" value="page" class="radio" <?php echo (($itemInfo->action != 'link') ? 'checked="checked"' : ''); ?> /><span class="inline">Pàgina <span class="help">(L'element del menú enllaçarà a una pàgina del site)</span></span>
                </label>
                <label class="inline tab" title="action_2">
                    <input type="radio" name="action" value="link" class="radio" <?php echo (($itemInfo->action == 'link') ? 'checked="checked"' : ''); ?> /><span class="inline">Link <span class="help">(L'element del menú enllaçarà a un link entrat manualment)</span></span>
                </label>
            </fieldset>
            <!-- Idiomes -->
            <?php
            if (sizeof($languages) > 1) {
                ?>
                <ul id="menuIdioma">
                    <?php
                    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                        echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
                    }
                    ?>
                </ul>
                <?php
            }
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                // Busquem valors
                $aux_listing = tep_db_query("select page_id, link, target, class, rel, rev, title, alt_title from " . TABLE_MENUS_PAGES_DESCRIPTION . " where menus_pages_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
                $aux_numrows = tep_db_num_rows($aux_listing);
                if ($aux_numrows > 0) {
                    $aux_item = tep_db_fetch_array($aux_listing);
                    $auxInfo = new objectInfo($aux_item);
                }
                ?>
                <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                    <div class="tab_content action_1" <?php echo (($itemInfo->action != 'link') ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                        <label >
                            <span class="pages amb_icona">Pàgina</span>
                            <?php echo tep_draw_pull_down_menu('page_id[' . $languages[$i]['id'] . ']', tep_get_pages_tree(), $auxInfo->page_id, 'class="llarg pagina"'); ?>
                        </label>
                    </div>
                    <div class="tab_content action_2" <?php echo (($itemInfo->action == 'link') ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                        <label>
                            <span>Link </span>
                            <input type="text" name="link[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->link; ?>" />
                        </label>
                    </div>
                    <p class="form_sep"></p>
                    <label class="width_50_1">
                        <span>Titol Menú*</span>
                        <input type="text" name="title[<?php echo $languages[$i]['id']; ?>]" value="<?php echo htmlentities($auxInfo->title, ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?>"  class="titol_menu"/>
                    </label>
                    <label class="width_50_2">
                        <span>Text Alternatiu</span>
                        <input type="text" name="alt_title[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->alt_title; ?>" />
                    </label>
                    <p class="form_sep"></p>
                    <label class="width_50_1">
                        <span>Target*</span>
                        <?php echo tep_draw_pull_down_menu('target[' . $languages[$i]['id'] . ']', $target_array, $auxInfo->target, ''); ?>
                    </label>
                    <?php if (tep_usuaris_es_admin()) { //camps ocults als editors?>
                        <label class="width_50_2">
                            <span>Class*</span>
                            <input type="text" name="class[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->class; ?>" />
                        </label>
                        <label class="width_50_1">
                            <span>Rel*</span>
                            <input type="text" name="rel[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rel; ?>" />
                        </label>
                        <label class="width_50_2">
                            <span>Rev*</span>
                            <input type="text" name="rev[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rev; ?>" />
                        </label>
                    <?php } ?>
                </fieldset>
                <?php
            }
            ?>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                ?>
            </p>
        </form>
        </div>

        <?php
        // Formulari Categories
    } else if ($_GET['action'] == 'new_menu') {

        if (($_GET['mID'])) {
            $id = $_GET['mID'];
            $query_raw = "select m.name, m.listorder from " . TABLE_MENUS . " m where m.id = '" . (int) $id . "'";
            $item = tep_db_fetch_array(tep_db_query($query_raw));
        } else {
            $item = array('name' => '',
                'listorder' => '');
        }
        $itemInfo = new objectInfo($item);
        $form_action = ($_GET['mID']) ? 'update_group' : 'insert_group';
        ?>

        <div id="form">
            <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                ?>
            </p>
            <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                <label class="width_50_1">
                    <span>Nom</span>
                    <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
                </label>
                <label class="width_50_2">
                    <span>Ordre</span>
                    <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
                    <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
                </label>
            </fieldset>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                ?>
            </p>
        </form>
        </div>

        <?php
    }
} else {
// Llistat
    if (!isset($_GET['menu_id']) || !tep_not_null($_GET['menu_id'])) {
        // Categories
        $menus_raw = "select distinct m.id, m.name, m.listorder from " . TABLE_MENUS . " m order by m.listorder";
        $menus_query = tep_db_query($menus_raw);
        $menus_rows = tep_db_num_rows($menus_query);
        ?>
        <p class="botonera">
            <?php
            if (!isset($_GET['menu_id']) || !tep_not_null($_GET['menu_id'])) {
                if (tep_usuaris_es_admin()) {
                    echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'iID', 'keyword', 'x', 'y')) . 'action=new_menu') . '"><span>' . NEW_MENU . '</span></a>';
                }
            } else {
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'iID', 'keyword', 'x', 'y')) . 'action=new_item') . '"><span>' . NEW_ITEM . '</span></a>';
            }
            ?>
        </p>
        <table summary="<?php echo TAULA; ?>"  class="llista fixed">
            <thead>
                <tr>
                    <th style="width:70%;"><?php echo COL_1; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="4">
            <div id="split_left"><?php echo sprintf(TEXT_DISPLAY_NUMBER_OF_MENUS, $menus_rows); ?></div>
        </th>
        </tr>
        </tfoot>
        <tbody>

            <?php
            if ($menus_rows > 0) {
                // Menús
                while ($menus = tep_db_fetch_array($menus_query)) {
                    $i++;
                    $aux = ($aux % 2) + 1;
                    $cInfo = new objectInfo($menus);

                    $aux_order = '';
                    if ($cInfo->listorder > 1) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=down&amp;mID=' . $cInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }
                    $aux_order .= '&nbsp;&nbsp;' . (($cInfo->listorder < 10) ? '0' : '') . $cInfo->listorder . '&nbsp;&nbsp;';
                    if ($cInfo->listorder < $menus_rows) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=up&amp;mID=' . $cInfo->id) . '">' . ICON_ORDER_UP . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }

                    echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'keyword', 'x', 'y')) . 'menu_id=' . $cInfo->id) . '\'" class="' . (($cInfo->id == $_GET['mID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                    . '  <td class="nom"><a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'keyword', 'x', 'y')) . 'menu_id=' . $cInfo->id) . '">' . ICON_FOLDER . htmlentities($cInfo->name, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</a></td>' . "\n"
                    . '  <td class="order">' . $aux_order . '</td>' . "\n"
                    . '  <td style="text-align:center;" class="accions">';
                    if (tep_usuaris_es_admin()) {
                        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'keyword', 'x', 'y')) . 'mID=' . $cInfo->id . '&amp;action=new_menu') . '">' . ICON_EDIT . '</a> '
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'keyword', 'x', 'y')) . 'mID=' . $cInfo->id . '&amp;action=delete_group') . '"  class="delete">' . ICON_DELETE . '</a>';
                    }
                    echo '</td>' . "\n"
                    . '</tr>' . "\n";
                }
            } else {
                // No Items
                echo '<tr>' . "\n";
                echo '  <td colspan="4" class="no_item">' . sprintf(NO_ITEMS, strtolower(NEW_CATEGORY)) . '</td>' . "\n";
                echo '</tr>' . "\n";
            }
        } else {
            // Llistat
            ?>

        <p class="botonera">
            <?php
            if (!isset($_GET['menu_id']) || !tep_not_null($_GET['menu_id'])) {
                if (tep_usuaris_es_admin()) {
                    echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'iID', 'keyword', 'x', 'y')) . 'action=new_menu') . '"><span>' . NEW_MENU . '</span></a>';
                }
            } else {
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'iID', 'keyword', 'x', 'y')) . 'action=new_item') . '"><span>' . NEW_ITEM . '</span></a>';
            }
            ?>
        </p>
        <table summary="<?php echo TAULA; ?>" class="llista fixed">
            <thead>
                <tr>
                    <th style="width:55%;"><?php echo COL_1; ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_2; ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_3; ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="5">
            <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS, $query_numrows) ?></div>
            </th>
            </tr>
            </tfoot>
            <tbody>

                <?php
                // Tornar a categories
                echo '<tr onclick="window.location.href=\'' .tep_href_link('', tep_get_all_get_params(array('action', 'menu_id', 'mID', 'iID', 'x', 'y')) . 'mID=' . $_GET['menu_id']).'\'" class="taula2">' . "\n"
                . '  <td colspan="5"><a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'menu_id', 'mID', 'iID', 'x', 'y')) . 'mID=' . $_GET['menu_id']) . '">' . ICON_FOLDER . '..</a></td>' . "\n"
                . '</tr>' . "\n";

// Obtenir arbre categories
                function tep_get_menu_pages_table($menus_id, $parent_id = '0', $spacing = '', $exclude = '', $menu_pages_table = '') {
                    global $_GET, $language_id, $aux;

                    $pages_query = tep_db_query("select p.id, pd.title, p.parent_id, p.action, p.active,  p.listorder from " . TABLE_MENUS_PAGES . " p left join  " . TABLE_MENUS_PAGES_DESCRIPTION . " pd on p.id = pd.menus_pages_id and pd.language_id = '" . $language_id . "'where p.menus_id = '" . (int) $menus_id . "' and p.parent_id = '" . $parent_id . "' order by p.listorder, pd.title");
                    while ($pages = tep_db_fetch_array($pages_query)) {
                        $itemInfo = new objectInfo($pages);

                        $aux = ($aux % 2) + 1;

                        if ($itemInfo->active) {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'iID', 'buscar', 'x', 'y')) . 'action=desactive&amp;iID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                        } else {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'iID', 'buscar', 'x', 'y')) . 'action=active&amp;iID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                        }

                        $menu_pages_table .= '<tr onclick="window.location.href=\'' .tep_href_link('', tep_get_all_get_params(array('action', 'iID', 'buscar', 'x', 'y')) . 'action=new_item&amp;iID=' . $itemInfo->id).'\'" class="' . (($itemInfo->id == $_GET['iID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                                . '  <td class="nom">' . (($spacing != '') ? $spacing . '<span class="separador">&rarr;</span>' : '') . htmlentities($itemInfo->title, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</td>' . "\n"
                                . '  <td style="text-align:center;">' . $itemInfo->action . '</td>' . "\n"
                                . '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
                                . '  <td>' . (($spacing != '') ? $spacing . '<span class="separador">&rarr;</span>' : '') . $itemInfo->listorder . '</td>' . "\n"
                                . '  <td style="text-align:center;" class="accions">'
                                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'iID', 'buscar', 'x', 'y')) . 'action=new_item&amp;iID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a> '
                                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'iID', 'buscar', 'x', 'y')) . 'action=delete_item&amp;iID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>'
                                . '</td>' . "\n"
                                . '</tr>' . "\n";

                        $menu_pages_table .= tep_get_menu_pages_table($menus_id, $pages['id'], $spacing . '<span class="espai">&nbsp;</span>');
                    }
                    return $menu_pages_table;
                }

                $taula = tep_get_menu_pages_table($_GET['menu_id']);
                if (tep_not_null($taula)) {
                    echo $taula;
                } else {
                    // No Items
                    echo '<tr>' . "\n";
                    echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
                    echo '</tr>' . "\n";
                }
            }
            ?>

        </tbody>
    </table>

    <p class="botonera">
        <?php
        if (!isset($_GET['menu_id']) || !tep_not_null($_GET['menu_id'])) {
            if (tep_usuaris_es_admin()) {
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'iID', 'keyword', 'x', 'y')) . 'action=new_menu') . '"><span>' . NEW_MENU . '</span></a>';
            }
        } else {
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'iID', 'keyword', 'x', 'y')) . 'action=new_item') . '"><span>' . NEW_ITEM . '</span></a>';
        }
        ?>
    </p>

    <?php
}
?>
