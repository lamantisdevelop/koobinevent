<?php

// Include thumbnail class
require_once(DIR_WS_CLASSES . 'ThumbLib.inc.php');

// Definim vector per selecció target
$target_array = array(array('id' => '_self', 'text' => 'Obrir a la mateixa finestra (_self)'),
    array('id' => '_blank', 'text' => 'Obrir en una finestra nova (_blank)'));

// Creem Array Idiomes
$languages = tep_get_languages();

// Funcions Auxiliars
function delete_item($item_id) 
{
    // Eliminem entrada
    tep_db_query(" delete from " . TABLE_MENUS_PAGES . " where id = '" . (int) $item_id ."'" );

    // Eliminar x descripcio
    tep_db_query(" delete from " . TABLE_MENUS_PAGES_DESCRIPTION . " where menus_pages_id = '" . (int) $item_id . "'" );

    //Eliminem fills
    if ((int)$item_id > 0) //0 es el valor per defecte quan no te valor... hauria de ser null !
    {
        tep_db_query(" DELETE " . TABLE_MENUS_PAGES . " ," . TABLE_MENUS_PAGES_DESCRIPTION . "  "
                   . " FROM " . TABLE_MENUS_PAGES . " "
                   . " INNER JOIN " . TABLE_MENUS_PAGES_DESCRIPTION . " ON " . TABLE_MENUS_PAGES . ".id = " . TABLE_MENUS_PAGES_DESCRIPTION . ".menus_pages_id "
                   . " WHERE " . TABLE_MENUS_PAGES . ".parent_id = '" . (int) $item_id . "'");

        delete_item_recursiu($item_id);
    }
}

function delete_item_recursiu($item_id)
{
    $items_query = tep_db_query(" SELECT DISTINCT ID "
                              . " FROM ". TABLE_MENUS_PAGES
                              . " LEFT JOIN " . TABLE_MENUS_PAGES_DESCRIPTION . " ON " . TABLE_MENUS_PAGES . ".id = " . TABLE_MENUS_PAGES_DESCRIPTION . ".menus_pages_id "
                              . " WHERE " . TABLE_MENUS_PAGES . ".parent_id = '" . (int) $item_id . "'"
                              . " ORDER BY ID ASC");

    while ($item = tep_db_fetch_array($items_query)) 
    {
        delete_item_recursiu($item['ID']);

        if ((int)$item['ID'] > 0) //0 es el valor per defecte quan no te valor... hauria de ser null !
        {
            tep_db_query(" DELETE " . TABLE_MENUS_PAGES . " ," . TABLE_MENUS_PAGES_DESCRIPTION . "  "
                       . " FROM " . TABLE_MENUS_PAGES . " "
                       . " INNER JOIN " . TABLE_MENUS_PAGES_DESCRIPTION . " ON " . TABLE_MENUS_PAGES . ".id = " . TABLE_MENUS_PAGES_DESCRIPTION . ".menus_pages_id "
                       . " WHERE " . TABLE_MENUS_PAGES . ".id = '" . (int) $item['ID'] . "'");
        }
    }

}

// Busquem valors del grup
if ($_GET['group_id']) {
    $query = tep_db_query("select bg.id, bg.name from " . TABLE_IMAGES_GROUPS . " bg where bg.id = '" . (int) $_GET['group_id'] . "' limit 1");
    $value = tep_db_fetch_array($query);
    $groupInfo = new objectInfo($value);
}

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case $_GET['action'] == 'new_item' || $_GET['action'] == 'new_menu'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;

//------------------------------ Grups ------------------------------//

        case 'insert_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Busquem nou ordre
                $order_now = tep_search_new_order(TABLE_MENUS);

                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'listorder' => $order_now,
                    'entered' => 'now()',
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_MENUS, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] < $order_now) {
                    tep_update_order($aux_id, $order_now, $_POST['listorder'], TABLE_MENUS);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'mID=' . $aux_id));
            break;

        case 'update_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['mID'])) {
                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['mID']);

                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_MENUS, $sql_data_array, 'update', "id = '" . (int) $aux_id . "'");

                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] != $_POST['listorder_aux']) {
                    tep_update_order($aux_id, $_POST['listorder_aux'], $_POST['listorder'], TABLE_MENUS);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'listorder_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['mID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['mID']);
                // Trobar posicio actual
                $order_now = tep_search_item_order($aux_id, TABLE_MENUS);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order($aux_id, $order_now, $pos_aux, TABLE_MENUS);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'delete_group':

            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            
            if (isset($_GET['mID'])) 
            {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['mID']);
                
                
                // Busquem i modifiquem ordre
                $order_now = tep_search_item_order($aux_id, TABLE_MENUS);
                
                // Eliminar entrada
                tep_db_query("delete from " . TABLE_MENUS . " where id = '" . (int) $aux_id . "'");
                
                // Reordenar items
                tep_reorder_items($order_now, TABLE_MENUS);

                // Eliminar items
                $items_query = tep_db_query(" select distinct id from " . TABLE_MENUS_PAGES . " "
                                          . " where menus_id = '" . (int) $aux_id . "' "
                                          . " order by listorder");
                
                while ($item = tep_db_fetch_array($items_query)) 
                {
                    // Identificador item
                    $item_id = tep_db_prepare_input($item['id']);
                    
                    // Borrem entraeda
                    delete_item($item_id);
                }
            }
            
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'mID', 'x', 'y'))));
            
            break;

//------------------------------ Opcions ------------------------------//

        case 'insert_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Vector entrades
                $sql_data_array = array('menus_id' => tep_db_prepare_input($_GET['menu_id']),
                    'parent_id' => tep_db_prepare_input($_POST['parent_id']),
                    'action' => tep_db_prepare_input($_POST['action']),
                    'listorder' => tep_db_prepare_input($_POST['listorder']),
                    'active' => '1',
                    'entered' => 'now()',
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_MENUS_PAGES, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Entrades segons idioma
                $page_id_array = $_POST['page_id'];
                $link_array = $_POST['link'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                $title_array = $_POST['title'];
                $alt_title_array = $_POST['alt_title'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('menus_pages_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'page_id' => tep_db_prepare_input($page_id_array[$lang_aux]),
                        'link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]),
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'alt_title' => tep_db_prepare_input($alt_title_array[$lang_aux]));
                    // Crida base dades
                    tep_db_perform(TABLE_MENUS_PAGES_DESCRIPTION, $sql_data_array);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'iID=' . $aux_id));
            break;

        case 'update_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['iID'] && $_POST) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['iID']);
                // Vector entrades
                $sql_data_array = array('parent_id' => tep_db_prepare_input($_POST['parent_id']),
                    'action' => tep_db_prepare_input($_POST['action']),
                    'listorder' => tep_db_prepare_input($_POST['listorder']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_MENUS_PAGES, $sql_data_array, 'update', 'id = \'' . (int) $aux_id . '\'');

                // Entrades segons idioma
                $page_id_array = $_POST['page_id'];
                $link_array = $_POST['link'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                $title_array = $_POST['title'];
                $alt_title_array = $_POST['alt_title'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('menus_pages_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'page_id' => tep_db_prepare_input($page_id_array[$lang_aux]),
                        'link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]),
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'alt_title' => tep_db_prepare_input($alt_title_array[$lang_aux]));
                    // Crida base dades
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT menus_pages_id FROM " . TABLE_MENUS_PAGES_DESCRIPTION . " WHERE menus_pages_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_MENUS_PAGES_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_MENUS_PAGES_DESCRIPTION, $sql_data_array, 'update', 'menus_pages_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'active':
        case 'desactive':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['iID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['iID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_MENUS_PAGES, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'delete_item':
            
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            
            if ($_GET['iID']) {
                
                $aux_id = tep_db_prepare_input($_GET['iID']);
                
                // Borrem entraeda
                delete_item($aux_id);
            }
            
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'menu_id', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
