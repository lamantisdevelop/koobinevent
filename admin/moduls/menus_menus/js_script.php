<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

// JQuery hide/show tags
$(document).ready(function() {
    $('label.tab').click(function () {
      $('.tab_content').hide();

      var mostra = $(this).attr('title');
      $('.'+mostra).show();
    });
    
    //per no haver de tornar a escriure el titol de pagina
    $('.pagina').change(function(){
        $(this).parents('.tab_content').nextAll("label").find('input.titol_menu').val($.trim($(this).find(' option:selected').text()));
    });
    
});


// Validar formulari
function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}                    

//]]>
</script>
