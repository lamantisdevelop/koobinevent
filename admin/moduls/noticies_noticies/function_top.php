<?php

//include(DIR_WS_FACEBOOK . 'functions_facebook.php');

// Creem Array Idiomes
$languages = tep_get_languages();

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
        case 'facebook':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            /** ------------GESTIONEM AFEGIR FACEBOOK------------------------ */
            if (is_numeric($_GET['nID'])) {
                if ($login_fb == '') {
                    $aux_listing = tep_get_noticia_by_id((int) $_GET['nID']); //obtenim info de la noticia
                    $aux_numrows = tep_db_num_rows($aux_listing);

                    if ($aux_numrows > 0) {
                        $aux_item = tep_db_fetch_array($aux_listing);
                        $auxInfo = new objectInfo($aux_item);
                    }

                    //noticies publiques
                    $img = tep_db_fetch_array(tep_get_foto_portada_galeria($auxInfo->fotos_groups_id));
                    $img = $_SERVER['SERVER_NAME'] . "/image/fotos/thumbs2/" . $img['image'];
                    $link = URL_CONNECTION . tep_friendly_url(DEFAULT_LANGUAGE, tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $_GET['nID'] . '&item_title=' . $auxInfo->titular, true);
                    add_post_facebook($auxInfo, $link, $img);
                }
                //print_r($auxInfo);
            }
            break;
        /* -----------------------------FI FACEBOOK-------------------------------- */
        case 'insert':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Traiem limit scripts
                tep_set_time_limit(0);
                // Insertem dades generals
                $sql_data_array = array('active' => '1',
                    'published' => tep_db_prepare_input($_POST['published']),
                    'finished' => tep_db_prepare_input($_POST['finished']),
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
                    'entered' => 'now()',
                    'modified' => 'now()');

                tep_db_perform(TABLE_NOTICIES, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Entrades segons idioma
                $titular_array = $_POST['titular'];
                $destacat_array = $_POST['destacat'];
                $text_array = $_POST['text'];
                //$tags_array = $_POST['tags'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('noticies_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'titular' => $titular_array[$lang_aux],
                        'destacat' => tep_db_prepare_input($destacat_array[$lang_aux]),
                        'text' => tep_db_prepare_input($text_array[$lang_aux])
                        //'tags' => tep_db_prepare_input($tags_array[$lang_aux])
                        );
                    // Crida base dades
                    tep_db_perform(TABLE_NOTICIES_DESCRIPTION, $sql_data_array);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'x', 'y')) . 'nID=' . $aux_id));
            break;

        case 'update':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['nID'] && $_POST) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Identificador
                $aux_id = tep_db_prepare_input($_GET['nID']);

                // Insertem dades personals
                $sql_data_array = array('published' => tep_db_prepare_input($_POST['published']),
                    'finished' => tep_db_prepare_input($_POST['finished']),
                    'fotos_groups_id' => tep_db_prepare_input($_POST['fotos_groups_id']),
                    'modified' => 'now()');

                tep_db_perform(TABLE_NOTICIES, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
                // Entrades segons idioma
                $titular_array = $_POST['titular'];
                $destacat_array = $_POST['destacat'];
                $text_array = $_POST['text'];
                //$tags_array = $_POST['tags'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('noticies_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'titular' => tep_db_prepare_input($titular_array[$lang_aux]),
                        'destacat' => tep_db_prepare_input($destacat_array[$lang_aux]),
                        'text' => tep_db_prepare_input($text_array[$lang_aux]),
                        //'tags' => tep_db_prepare_input($tags_array[$lang_aux])
                        );
                    // Crida base dades
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT noticies_id FROM " . TABLE_NOTICIES_DESCRIPTION . " WHERE noticies_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_NOTICIES_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_NOTICIES_DESCRIPTION, $sql_data_array, 'update', 'noticies_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'delete':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['nID']) {
                $aux_id = tep_db_prepare_input($_GET['nID']);

                // Eliminem entrada
                tep_db_query("delete from " . TABLE_NOTICIES . " where id = '" . (int) $aux_id . "'");

                // Eliminar x descripcio        
                tep_db_query("delete from " . TABLE_NOTICIES_DESCRIPTION . " where noticies_id = '" . (int) $aux_id . "'");
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'x', 'y'))));
            break;

        case 'active':
        case 'desactive':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['nID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['nID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_NOTICIES, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'portada':
        case 'desportada':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['nID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['nID']);
                // Nou valor
                $value = (($_GET['action'] == 'desportada') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('portada' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_NOTICIES, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;
    }
}

// Comprovem si existeixen i es pot escriure en directoris per imatges i descàrregues

if (is_dir(DIR_FS_PORTAL_DOWNLOADS)) {
    if (!is_writeable(DIR_FS_PORTAL_DOWNLOADS))
        $messageStack->add(ERROR_CATALOG_DOWNLOAD_DIRECTORY_NOT_WRITEABLE . ' (' . DIR_FS_PORTAL_DOWNLOADS . ')', 'error');
} else {
    $messageStack->add(ERROR_CATALOG_DOWNLOAD_DIRECTORY_DOES_NOT_EXIST . ' (' . DIR_FS_PORTAL_DOWNLOADS . ')', 'error');
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
