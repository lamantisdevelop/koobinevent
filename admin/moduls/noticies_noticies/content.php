<div id="buscador">
    <?php
    echo tep_draw_form('form_search', tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))), 'get');
    // Recuperem totes les variables $_GET
    reset($_GET);
    while (list($key, $value) = each($_GET)) {
        if (($key != 'tipus') && ($key != 'keyword') && ($key != 'action') && ($key != 'page') && ($key != 'x') && ($key != 'y') && ($key != tep_session_name())) {
            if (tep_not_null($value))
                echo tep_draw_hidden_field(rawurldecode($key), rawurldecode($value));
        }
    }
    // Variables buscador
    echo '<input type="text" name="keyword" value="' . $_GET['keyword'] . '" placeholder="Entrar text a cercar..." />'
    . '<input type="submit" class="boto_buscador" value=""/>'
    . '</label>';
    ?>
</form>
</div>
<h2 class="noticies"><?php echo TITLE_PAGE; ?></h2>

<?php
if ($_GET['action'] == 'new') {
    // Fitxa
    if (isset($_GET['nID']) && tep_not_null($_GET['nID'])) {
        $id = $_GET['nID'];
        $query_raw = "select id, published,fotos_groups_id,facebook_send from " . TABLE_NOTICIES . " where id = '" . (int) $_GET['nID'] . "'";
        $item = tep_db_fetch_array(tep_db_query($query_raw));
    } else {
        $item = array('id' => '',
            'published' => '');
    }
    $itemInfo = new objectInfo($item);

    $form_action = ($_GET['nID']) ? 'update' : 'insert';
    ?>

    <div id="form">
        <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
        <!-- Idiomes -->
        <?php
        if (sizeof($languages) > 1) {
            ?>
            <ul id="menuIdioma">
                <?php
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
                }
                ?>
            </ul>
            <?php
        }
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            // Busquem valors
            $aux_listing = tep_db_query("select titular, text, tags from " . TABLE_NOTICIES_DESCRIPTION . " where noticies_id = '" . (int) $id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
            $aux_numrows = tep_db_num_rows($aux_listing);
            if ($aux_numrows > 0) {
                $aux_item = tep_db_fetch_array($aux_listing);
                $auxInfo = new objectInfo($aux_item);
            }
            ?>
            <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                <label>
                    <span>Titular</span>
                    <input type="text" name="titular[<?php echo $languages[$i]['id']; ?>]" value="<?php echo htmlentities($auxInfo->titular); ?>" />
                </label>
                <div class="clear top_15"></div>

                <textarea class="editor" name="text[<?php echo $languages[$i]['id']; ?>]" id="text[<?php echo $languages[$i]['id']; ?>]" <?php echo (($itemInfo->action == 'content') ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                    <?php echo $auxInfo->text; ?>
                </textarea>
                <script>
                    <?php 
                    if (!tep_usuaris_es_admin())
                    { 
                        echo("CKEDITOR.config.toolbar = 'Editor';");                     
                    }
                    ?>
                    CKEDITOR.replace( 'text[<?php echo $languages[$i]['id']; ?>]');
                </script>

                    <?php
//                    $params_ckeditor = array('height' => '500');
//                    if (tep_usuaris_es_admin()) {
//                        $params_ckeditor['toolbar'] = 'Full';
//                    }
//                    $ckeditor->replace('text[' . $languages[$i]['id'] . ']', $params_ckeditor);
//                    ?>

<!--                <label>
                    <span>Etiquetes (separades per comes)</span>
                    <input type="text" name="tags[<?php //echo $languages[$i]['id']; ?>]" value="<?php //echo $auxInfo->tags; ?>" />
                </label>-->
            </fieldset>
            <?php
        }
        ?>
        <fieldset>
            <label class="width_50_1">
                <span>Data Publicació (Dia que la notícia es publicarà automàticament)</span>
                <input type="text"  name="published" value="<?php echo (($itemInfo->published) ? tep_date_db($itemInfo->published) : date("Y-m-d")); ?>" class="camp_read date_input" />
                <a class="link_cal"></a>
            </label>
        </fieldset>
        <h4 class="desplegable collapsed"><span class="media">Galeria d'Imatges</span></h4>
        <fieldset>
            <label class="width_50_1">
                <div id="continent_select_galeries">
                    <?php echo tep_draw_pull_down_menu('fotos_groups_id', tep_get_fotos_groups_array(FOTOS_GROUP_NOTICIES, '--'), $itemInfo->fotos_groups_id, 'id="fotos_groups_id"'); ?>
                </div>
            </label>
            <label class="width_50_2">
                <div >
                    <a class="nova_galeria" id="nova_galeria" onclick="return novaGaleria('<?php echo FOTOS_GROUP_NOTICIES; ?>');" href="#" title="Crear una nova galeria">NOVA GALERIA <span class="container_loading"><img style="display: none;margin-top:5px;" id="loading_galeria" src="image/loading_linia.gif" alt="loading"/></span></a>
                </div>
            </label>
            <?php
            //mirem si tenim associada alguna galeria
            $src = !is_null($itemInfo->fotos_groups_id) && tep_galeria_exists($itemInfo->fotos_groups_id) ? '?modul=fotos_fotos&group_id=' . $itemInfo->fotos_groups_id . '&iframe=si' : 'about:blank';
            ?>

            <iframe scrolling="no" frameborder=0 border=0 id="childframe" name="childframe" src="<?php echo $src; ?>"></iframe>
        </fieldset>
        <p class="botonera">
            <?php
            echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'buscar', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
            ?>
        </p>
    </form>
    </div>

    <?php
} else {
    // Llistat
    if (isset($_GET['keyword']) && tep_not_null($_GET['keyword'])) {
        $query_raw = "select n.id, nd.titular, nd.text, n.active, n.portada, n.published,n.facebook_send from " . TABLE_NOTICIES . " n left join " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.language_id = '" . (int) $language_id . "' where nd.titular like '%" . tep_db_input($_GET['keyword']) . "%' order by n.published desc";
    } else {
        $query_raw = "select n.id, nd.titular, nd.text, n.active, n.portada, n.published,n.facebook_send from " . TABLE_NOTICIES . " n left join " . TABLE_NOTICIES_DESCRIPTION . " nd on n.id = nd.noticies_id and nd.language_id = '" . (int) $language_id . "' order by n.published desc";
    }
    $split = new splitPageResults($_GET['page'], MAX_ADMIN_RESULTS, $query_raw, $query_numrows);
    $listing = tep_db_query($query_raw);
    ?>
    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>
    <table summary="<?php echo TAULA; ?>" class="llista fixed">
        <thead>
            <tr>
                <th style="width:60%;"><?php echo COL_1; ?></th>
                <th style="width:10%;"><?php echo COL_2; ?></th>
                <th style="width:5%; text-align:center;"><?php echo COL_3; ?></th>
                <th style="width:5%; text-align:center;"><?php echo COL_4; ?></th>
                <th style="width:10%; text-align:center;">Social</th>
                <th style="width:10%; text-align:center;"><?php echo COL_5; ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="6">
        <div id="split_left"><?php echo $split->display_count($query_numrows, MAX_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></div>
        <div id="split_right"><?php echo $split->display_links($query_numrows, MAX_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'action', 'x', 'y'))); ?></div>
    </th>
    </tr>
    </tfoot>
    <tbody>

        <?php
        if ($query_numrows > 0) {
            // Llistat segons b�squeda
            $i = 0;
            while ($item = tep_db_fetch_array($listing)) {
                $i++;
                $aux = ($aux % 2) + 1;
                $itemInfo = new objectInfo($item);

                if ($itemInfo->active) {
                    $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'buscar', 'x', 'y')) . 'action=desactive&amp;nID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                } else {
                    $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'buscar', 'x', 'y')) . 'action=active&amp;nID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                }

                if ($itemInfo->portada) {
                    $aux_portada = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'buscar', 'x', 'y')) . 'action=desportada&amp;nID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                } else {
                    $aux_portada = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'buscar', 'x', 'y')) . 'action=portada&amp;nID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                }

                echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'x', 'y')) . 'nID=' . $itemInfo->id . '&amp;action=new') . '\'" class="' . (($itemInfo->id == $_GET['nID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                . '  <td>' . $itemInfo->titular . '</td>' . "\n"
                . '  <td>' . tep_date_short($itemInfo->published) . '</td>' . "\n"
                . '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
                . '  <td style="text-align:center;">' . $aux_portada . '</td>' . "\n";
                //FACEBOOK 
                               //FACEBOOK 
                echo '  <td class="social">';
                if (!$itemInfo->facebook_send) {
                    echo '<a target="_blank" rel="shadowbox" href="https://www.facebook.com/sharer/sharer.php?u=http://'.$_SERVER['HTTP_HOST']  . '/ca' . tep_friendly_url('', tep_get_page_title(DEFAULT_NOTICIES_PAGE), 'itemID=' . $itemInfo->id . '&item_title=' . $itemInfo->titular) . '" >' . ICON_FACEBOOK . '</a>';
                } else {
                    echo ICON_FACEBOOK_DISABLED;
                }
                echo '</td>' . "\n";
                echo'  <td style="text-align:center;" class="accions">'
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'x', 'y')) . 'nID=' . $itemInfo->id . '&amp;action=new') . '">' . ICON_EDIT . '</a> '
                . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'x', 'y')) . 'nID=' . $itemInfo->id . '&amp;action=delete') . '" class="delete">' . ICON_DELETE . '</a>'
                . '</td>' . "\n"
                . '</tr>' . "\n";
            }
        } else {
            // No Items
            echo '<tr>' . "\n";
            echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS, strtolower(NEW_ITEM)) . '</td>' . "\n";
            echo '</tr>' . "\n";
        }
        ?>

    </tbody>
    </table>

    <p class="botonera">
        <?php echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'nID', 'x', 'y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
    </p>

    <?php
}
?>
