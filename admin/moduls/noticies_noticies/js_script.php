<?php
/*
JavascriptScript específic pel mòdul
*/
?>
<script type="text/javascript" language="javascript">
//<![CDATA[

// Comprovar  enviar formulari
function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}

function addRow(taula) {
  var elements, templateRow, rowCount, row, className, newRow, element;
  var i, s, t;

    /* Get and count all "tr" elements with class="row".    The last one will
     * be serve as a template. */
    if (!document.getElementsByTagName)
        return false; /* DOM not supported */
    elements = document.getElementById(taula).getElementsByTagName("tr");
    templateRow = null;
    rowCount = 0;
    for (i = 0; i < elements.length; i++) {
        row = elements.item(i);

        /* Get the "class" attribute of the row. */
        className = null;
        if (row.getAttribute)
            className = row.getAttribute('class')
        if (className == null && row.attributes) {    // MSIE 5
            /* getAttribute('class') always returns null on MSIE 5, and
             * row.attributes doesn't work on Firefox 1.0.    Go figure. */
            className = row.attributes['class'];
            if (className && typeof(className) == 'object' && className.value) {
                // MSIE 6
                className = className.value;
            }
        }

        /* This is not one of the rows we're looking for.    Move along. */
        if (className != "row_to_clone")
            continue;

        /* This *is* a row we're looking for. */
        templateRow = row;
        rowCount++;
    }
    if (templateRow == null)
        return false; /* Couldn't find a template row. */

    /* Make a copy of the template row */
    newRow = templateRow.cloneNode(true);

    /* Add the newly-created row to the table */
    templateRow.parentNode.appendChild(newRow);
    newRow.style.display='';
    newRow.className='cloned_row';
    return true;
}


$(document).ready(function() {
    $('.delete_node').on("click", function(evento) {
      $(this).parent().parent().remove();
      evento.preventDefault();
    });

    $('.add_node').click(function(evento) {
      addRow($(this).prev('table').attr('id'));
      evento.preventDefault();
    });
});

//]]>
</script>
