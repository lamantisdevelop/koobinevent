<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */

/**
 * @desc Retorna el llistat de galeries actual
 */
function refreshLlistatGaleries($gID) {
    $galeria = tep_db_fetch_array(tep_get_galeria_by_id($gID));
    if ($_GET['idLlistat']== '0') {
        echo tep_draw_pull_down_menu('fotos_groups_id', tep_get_fotos_groups_array($galeria['parent_id'],'--'), $_GET['gID'], 'id="fotos_groups_id"');
    } else {
        echo tep_draw_pull_down_menu('fotos_groups_id_' . $_GET['idLlistat'], tep_get_fotos_groups_array($galeria['parent_id'],'--'), $_GET['gID'], 'id="fotos_groups_id_' . $_GET['idLlistat'] . '"');
    }
}

?>