<?php
ini_set("display_errors",1);

// Definim vector per selecció target
$target_array = array(array('id' => '_blank', 'text' => '_blank'),
    array('id' => '_self', 'text' => '_self'));

// Creem Array Idiomes
$languages = tep_get_languages();

// Funcions Auxiliars
function delete_item($item_id) {

    // Busquem i modifiquem ordre
    $order_now = tep_search_item_order_group($item_id, TABLE_FOTOS, 'group_id', $_GET['group_id']);

    //Eliminem arxiu de foto
    $files_query = tep_db_query("select image from " . TABLE_FOTOS . " where id = '" . (int) $item_id . "'");
    $files = tep_db_fetch_array($files_query);
    if (file_exists(DIR_FS_PORTAL_IMAGE_FOTOS . $files['image']))
        @unlink(DIR_FS_PORTAL_IMAGE_FOTOS . $files['image']);
    //eliminem thumbs
    if (file_exists(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $files['image']))
        @unlink(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $files['image']);
    if (file_exists(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $files['image']))
        @unlink(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs2/' . $files['image']);
    if (file_exists(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs3/' . $files['image']))
        @unlink(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs3/' . $files['image']);
    if (file_exists(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs4/' . $files['image']))
        @unlink(DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs4/' . $files['image']);

    // Eliminem entrada
    tep_db_query("delete from " . TABLE_FOTOS . " where id = '" . (int) $item_id . "'");
    // Reordenem items
    tep_reorder_items_group($order_now, TABLE_FOTOS, 'group_id', $_GET['group_id']);

    // Eliminar x descripcio
    tep_db_query("delete from " . TABLE_FOTOS_DESCRIPTION . " where fotos_id = '" . (int) $item_id . "'");
}

// Busquem valors del grup
if ($_GET['group_id']) {
    $query = tep_db_query("select bg.id, bg.name,bg.parent_id from " . TABLE_FOTOS_GROUPS . " bg where bg.id = '" . (int) $_GET['group_id'] . "' limit 1");
    $value = tep_db_fetch_array($query);
    $groupInfo = new objectInfo($value);
}

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case $_GET['action'] == 'new_item' || $_GET['action'] == 'new_group'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
//------------------------------ Grups ------------------------------//

        case 'insert_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Busquem nou ordre
                $new_id = tep_search_new_id(TABLE_FOTOS_GROUPS);
                //$order_now = tep_search_new_order(TABLE_FOTOS_GROUPS);
                $order_now = tep_search_new_order_group(TABLE_FOTOS_GROUPS, 'parent_id', $_GET['group_id']);

                // Vector entrades
                $sql_data_array = array('id' => (int) $new_id,
                    'name' => tep_db_prepare_input($_POST['name']),
                    'id_ratios' => ($_POST['id_ratios']),
                    'listorder' => $order_now,
                    'parent_id' => $_POST['parent_id'],
                    'active' => '1',
                    'entered' => 'now()',
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_FOTOS_GROUPS, $sql_data_array);

                // Entrades segons idioma
                $title_array = $_POST['title'];
                $description_array = $_POST['description'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('groups_id' => (int) $new_id,
                        'language_id' => (int) $lang_aux,
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    // Crida base dades
                    tep_db_perform(TABLE_FOTOS_GROUPS_DESCRIPTION, $sql_data_array);
                }
            }
            //mirem si estem dins d'un iframe mostrant en algun altre mòdul
            $iframe = isset($_GET['iframe']) ? '&refresh=true&group_id=' . $new_id : '';

            //Redireccio si venim d'un altre modul
            if (isset($_GET['from'])) {
                tep_redirect(tep_href_link('', 'modul=' . $_GET['from'] . '&pID=' . $_GET['from_pID'] . '&action=' . $_GET['from_action'] . $iframe));
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'gID=' . $new_id . $iframe));
            break;

        case 'update_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if (isset($_GET['gID'])) {
                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['gID']);

                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'id_ratios' => ($_POST['id_ratios']),
                    'listorder' => $_POST['listorder'],
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_FOTOS_GROUPS, $sql_data_array, 'update', "id = '" . (int) $aux_id . "'");

                // Entrades segons idioma
                $title_array = $_POST['title'];
                $description_array = $_POST['description'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('groups_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    // Crida base dades
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT groups_id FROM " . TABLE_FOTOS_GROUPS_DESCRIPTION . " WHERE groups_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_FOTOS_GROUPS_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_FOTOS_GROUPS_DESCRIPTION, $sql_data_array, 'update', 'groups_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }
            }
            // Canviem items ordre si cal (no funciona)
            /* if ($_POST['listorder'] > 0 && $_POST['listorder'] != $_POST['listorder_aux']) {
              tep_update_order_group($aux_id, $_POST['listorder_aux'], $_POST['listorder'], TABLE_FOTOS_GROUPS, 'parent_id', $aux_id);
              } */

            //mirem si estem dins d'un iframe mostrant en algun altre mòdul
            $iframe = isset($_GET['iframe']) ? '&refresh=true&group_id=' . $aux_id : '';

            //Redireccio si venim d'un altre modul
            if (isset($_GET['from'])) {
                tep_redirect(tep_href_link('', 'modul=' . $_GET['from'] . '&pID=' . $_GET['from_pID'] . '&action=' . $_GET['from_action'] . $iframe));
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . $iframe));
            break;

        case 'active_category':
        case 'desactive_category':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['cID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['cID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive_category') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_FOTOS_GROUPS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
                //actualitzem fills
                tep_db_perform(TABLE_FOTOS, $sql_data_array, 'update', 'group_id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'cID', 'x', 'y'))));
            break;

        case 'listorder_group':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['gID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['gID']);
                // Trobar posicio actual
                //$order_now = tep_search_item_order($aux_id, TABLE_FOTOS_GROUPS);
                $order_now = tep_search_item_order_group($aux_id, TABLE_FOTOS_GROUPS, 'parent_id', $_GET['group_id']);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                //tep_change_order($aux_id, $order_now, $pos_aux, TABLE_FOTOS_GROUPS);
                tep_change_order_group($aux_id, $order_now, $pos_aux, TABLE_FOTOS_GROUPS, 'parent_id', $_GET['group_id']);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'delete_group':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if (isset($_GET['gID'])) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Identificador Item
                $aux_id = tep_db_prepare_input($_GET['gID']);

                $grups_fills = tep_get_fotos_groups_array($aux_id);
                //eliminem tots els grups fills
                foreach ($grups_fills as $grup_fill) {
                    // Busquem i modifiquem ordre
                    $order_now = tep_search_item_order($aux_id, TABLE_FOTOS_GROUPS);
                    // Eliminar entrada
                    tep_db_query("delete from " . TABLE_FOTOS_GROUPS . " where id = '" . (int) $grup_fill['id'] . "'");
                    // Reordenar items
                    tep_reorder_items_fills($order_now, $aux_id, TABLE_FOTOS_GROUPS);
                    // Eliminar descripcions
                    tep_db_query("delete from " . TABLE_FOTOS_GROUPS_DESCRIPTION . " where groups_id = '" . (int) $grup_fill['id'] . "'");
                    // Eliminar items
                    $items_query = tep_db_query("select distinct id from " . TABLE_FOTOS . " where group_id = '" . (int) $grup_fill['id'] . "' order by listorder");
                    while ($item = tep_db_fetch_array($items_query)) {
                        // Borrem entraeda
                        delete_item($item['id']);
                    }
                }
                //eliminem el grup pare
                // Busquem i modifiquem ordre
                $order_now = tep_search_item_order($aux_id, TABLE_FOTOS_GROUPS);
                // Eliminar entrada
                tep_db_query("delete from " . TABLE_FOTOS_GROUPS . " where id = '" . (int) $aux_id . "'");
                // Reordenar items
                tep_reorder_items($order_now, TABLE_FOTOS_GROUPS);
                // Eliminar descripcions
                tep_db_query("delete from " . TABLE_FOTOS_GROUPS_DESCRIPTION . " where groups_id = '" . (int) $aux_id . "'");


//mirem si estem dins d'un iframe mostrant en algun altre mòdul
                $iframe = isset($_GET['iframe']) ? '&refresh=true' : '';

                // Eliminar items
                $items_query = tep_db_query("select distinct id from " . TABLE_FOTOS . " where group_id = '" . (int) $aux_id . "' order by listorder");
                while ($item = tep_db_fetch_array($items_query)) {
                    // Borrem entraeda
                    delete_item($item['id']);
                }
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'x', 'y')) . $iframe));
            break;

//------------------------------ Opcions ------------------------------//

        case 'insert_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST) {
                // Traiem limit scripts
                tep_set_time_limit(0);

                // Busquem nou ordre
                $order_now = tep_search_new_order_group(TABLE_FOTOS, 'group_id', $_GET['group_id']);

                // obtenim ratios i mides fotos
                $id = $_GET['group_id'];
                $query_raw = "select fr.id,fr.nom,fr.ratio,fr.thumb1_w,fr.thumb1_h,fr.thumb2_w,fr.thumb2_h,fr.thumb3_h,fr.thumb3_w from " . TABLE_FOTOS_GROUPS . " fg left join " . TABLE_FOTOS_RATIOS . " fr ON fg.id_ratios=fr.id  WHERE fg.id = '" . (int) $id . "'";
                $item = tep_db_fetch_array(tep_db_query($query_raw));
                $itemInfo = new objectInfo($item);

                // Imatge
                $image = $_FILES['foto_new'];
                $image_aux = $image['name'];
                if (!empty($image_aux)) {
                    if (is_uploaded_file($image['tmp_name'])) {
                        // Nom image no repetit
                        $image_aux = rename_if_exists(DIR_FS_PORTAL_IMAGE_FOTOS, $image_aux);
                        // Copiar image
                        move_uploaded_file($image['tmp_name'], DIR_FS_PORTAL_IMAGE_FOTOS . $image_aux);
                        // Generem les miniatures
                        tep_generar_miniatures($itemInfo, $image_aux);
                    }
                }
                
                //die;

                // Vector entrades
                $sql_data_array = array(//'name' => tep_db_prepare_input($_POST['name']),
                    'name' => tep_db_prepare_input($_POST['title'][1]),
                    'listorder' => $order_now,
                    'active' => '1',
                    'group_id' => tep_db_prepare_input($_GET['group_id']),
                    'entered' => 'now()',
                    'image' => tep_db_prepare_input($image_aux),
                    'efecte' => tep_db_prepare_input($_POST['efecte']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_FOTOS, $sql_data_array);
                $aux_id = tep_db_insert_id();

                // Entrades segons idioma
                $link_array = $_POST['link'];
                $title_array = $_POST['title'];
                $autor_array = $_POST['autor'];
                $description_array = $_POST['description'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                $title_slide_array = $_POST['title_slide'];
                $subtitle_array = $_POST['subtitle'];
                $subtitle2_array = $_POST['subtitle2'];
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('fotos_id' => tep_db_prepare_input($aux_id),
                        'language_id' => (int) $lang_aux,
                        'link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'autor' => tep_db_prepare_input($autor_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]),
                        'title_slide' => tep_db_prepare_input($title_slide_array[$lang_aux]),
                        'subtitle' => tep_db_prepare_input($subtitle_array[$lang_aux]),
                        'subtitle2' => tep_db_prepare_input($subtitle2_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    // Crida base dades
                    tep_db_perform(TABLE_FOTOS_DESCRIPTION, $sql_data_array);
                }
                // Foto portada
                $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($_GET['group_id'])); //mirem si ja hi ha foto de portada, si no, forcem que sigui aquesta
                if (isset($_POST['portada']) && ($_POST['portada'] == 'on') || !$portada) {
                    tep_db_query("update " . TABLE_FOTOS_GROUPS . " set foto_portada = '" . (int) $aux_id . "' where id = '" . $groupInfo->id . "'");
                }



                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] < $order_now) {
                    tep_update_order_group($aux_id, $order_now, $_POST['listorder'], TABLE_FOTOS, 'group_id', $_GET['group_id']);
                }
            }
            //Redireccio si venim d'un altre modul
            if (isset($_GET['from'])) {
                tep_redirect(tep_href_link('', 'modul=' . $_GET['from'] . '&pID=' . $_GET['from_pID'] . '&action=' . $_GET['from_action']));
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'bID=' . $aux_id));
            break;

        case 'update_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);

            if ($_GET['bID'] && $_POST) {

                // Traiem limit scripts
                tep_set_time_limit(0);


                // Identificador
                $aux_id = tep_db_prepare_input($_GET['bID']);

                // obtenim ratios i mides fotos
                $id = $_GET['group_id'];
                $query_raw = "select fr.id,fr.nom,fr.ratio,fr.thumb1_w,fr.thumb1_h,fr.thumb2_w,fr.thumb2_h,fr.thumb3_h,fr.thumb3_w from " . TABLE_FOTOS_GROUPS . " fg left join " . TABLE_FOTOS_RATIOS . " fr ON fg.id_ratios=fr.id  WHERE fg.id = '" . (int) $id . "'";
                $item = tep_db_fetch_array(tep_db_query($query_raw));
                $itemInfo = new objectInfo($item);

                // Imatge + Borrar anterior ( sempre, no repetides, noms diferents )
                $image = $_FILES['foto_new'];
                $foto_aux = $image['name'];
                if (!empty($foto_aux)) {
                    if (is_uploaded_file($image['tmp_name'])) {
                        // Borrem image vella
                        $image_old = DIR_FS_PORTAL_IMAGE_FOTOS . $_POST['foto_vella'];
                        $thumb_old = DIR_FS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $_POST['foto_vella'];
                        if (file_exists($image_old)) {
                            @unlink($image_old);
                            @unlink($thumb_old);
                        }
                        // Nom image no repetit
                        $foto_aux = rename_if_exists(DIR_FS_PORTAL_IMAGE_FOTOS, $foto_aux);
                        // Copiar image
                        move_uploaded_file($image['tmp_name'], DIR_FS_PORTAL_IMAGE_FOTOS . $foto_aux);

                        // Generem les miniatures
                        tep_generar_miniatures($itemInfo, $foto_aux);
                    }
                } else {
                    $foto_aux = $_POST['foto_act'];
                    if (isset($_POST['x']) && $_POST['x'] != '') {//en cas que s'hagi tocat el crop
                        $crop = array('x' => $_POST['x'], 'y' => $_POST['y'], 'w' => round($_POST['w']), 'h' => round($_POST['h']));
                        // Generem les miniatures
                        tep_generar_miniatures($itemInfo, $foto_aux, $crop);
                    }
                }

                // Vector entrades
                $sql_data_array = array(//'name' => tep_db_prepare_input($_POST['name']),
                    'name' => tep_db_prepare_input($_POST['title'][1]),
                    'image' => tep_db_prepare_input($foto_aux),
                    'efecte' => tep_db_prepare_input($_POST['efecte']),
                    'modified' => 'now()');
                // Crida base dades
                tep_db_perform(TABLE_FOTOS, $sql_data_array, 'update', 'id = \'' . (int) $aux_id . '\'');



                // Entrades segons idioma
                $link_array = $_POST['link'];
                $title_array = $_POST['title'];
                $autor_array = $_POST['autor'];
                $description_array = $_POST['description'];
                $target_array = $_POST['target'];
                $class_array = $_POST['class'];
                $rel_array = $_POST['rel'];
                $rev_array = $_POST['rev'];
                $title_slide_array = $_POST['title_slide'];
                $subtitle_array = $_POST['subtitle'];
                $subtitle2_array = $_POST['subtitle2'];

                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    $lang_aux = $languages[$i]['id'];

                    // Insertem dades
                    $sql_data_array = array('link' => tep_db_prepare_input($link_array[$lang_aux]),
                        'target' => tep_db_prepare_input($target_array[$lang_aux]),
                        'title' => tep_db_prepare_input($title_array[$lang_aux]),
                        'autor' => tep_db_prepare_input($autor_array[$lang_aux]),
                        'description' => tep_db_prepare_input($description_array[$lang_aux]),
                        'title_slide' => tep_db_prepare_input($title_slide_array[$lang_aux]),
                        'subtitle' => tep_db_prepare_input($subtitle_array[$lang_aux]),
                        'subtitle2' => tep_db_prepare_input($subtitle2_array[$lang_aux]),
                        'class' => tep_db_prepare_input($class_array[$lang_aux]),
                        'rel' => tep_db_prepare_input($rel_array[$lang_aux]),
                        'rev' => tep_db_prepare_input($rev_array[$lang_aux]));
                    // Crida base dades
                    //Comprovem si existeixen tots els idiomes per aquesta pagina
                    $pages_query = tep_db_query("SELECT fotos_id FROM " . TABLE_FOTOS_DESCRIPTION . " WHERE fotos_id = '" . (int) $aux_id . "' AND language_id='" . $lang_aux . "'");
                    $pages_numrows = tep_db_num_rows($pages_query);
                    if ($pages_numrows == 0) { //Cas que no existeix la pagina per idioma (INSERT)
                        tep_db_perform(TABLE_FOTOS_DESCRIPTION, $sql_data_array);
                    } else {//Cas que la pagina ja te tots els idiomes (UPDATE)
                        tep_db_perform(TABLE_FOTOS_DESCRIPTION, $sql_data_array, 'update', 'fotos_id = \'' . (int) $aux_id . '\' and language_id=\'' . $lang_aux . '\'');
                    }
                }

                // Canviem items ordre si cal
                if ($_POST['listorder'] > 0 && $_POST['listorder'] != $_POST['listorder_aux']) {
                    tep_update_order_group($aux_id, $_POST['listorder_aux'], $_POST['listorder'], TABLE_FOTOS, 'group_id', $_GET['group_id']);
                }
                // Foto portada
                if (isset($_POST['portada']) && ($_POST['portada'] == 'on')) {
                    tep_db_query("update " . TABLE_FOTOS_GROUPS . " set foto_portada = '" . (int) $aux_id . "' where id = '" . $groupInfo->id . "'");
                    //fem la portada que sigui la primera en ordre
                    tep_update_order_group($aux_id, $_POST['listorder_aux'], 1, TABLE_FOTOS, 'group_id', $_GET['group_id']);
                }
            }
            //Redireccio si venim d'un altre modul
            if (isset($_GET['from'])) {
                tep_redirect(tep_href_link('', 'modul=' . $_GET['from'] . '&pID=' . $_GET['from_pID'] . '&action=' . $_GET['from_action']));
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'listorder_item':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['bID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['bID']);
                // Trobar posicio actual
                $order_now = tep_search_item_order_group($aux_id, TABLE_FOTOS, 'group_id', $_GET['group_id']);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order_group($aux_id, $order_now, $pos_aux, TABLE_FOTOS, 'group_id', $_GET['group_id']);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'active':
        case 'desactive':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['bID']) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['bID']);
                // Nou valor
                $value = (($_GET['action'] == 'desactive') ? 0 : 1);
                // Vector entrades
                $sql_data_array = array('active' => tep_db_prepare_input($value));
                // Crida base dades
                tep_db_perform(TABLE_FOTOS, $sql_data_array, 'update', 'id = \'' . tep_db_input($aux_id) . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;

        case 'delete_item':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['bID']) {
                $aux_id = tep_db_prepare_input($_GET['bID']);
                // Borrem entraeda
                delete_item($aux_id);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'bID', 'x', 'y'))));
            break;
    }
}

// Comprovem si existeixen i es pot escriure en directoris per imatges i descàrregues
if (is_dir(DIR_FS_PORTAL_IMAGE_FOTOS)) {
    if (!is_writeable(DIR_FS_PORTAL_IMAGE_FOTOS))
        $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE . ' (' . DIR_FS_PORTAL_IMAGE_FOTOS . ')', 'error');
} else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST . ' (' . DIR_FS_PORTAL_IMAGE_FOTOS . ')', 'error');

// Textos
    $titular = WEB_NAME . ' | ' . PAGE_TITLE;
}
?>
