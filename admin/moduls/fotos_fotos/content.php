<?php
if (!$_GET['iframe']) {//en cas que no siguem dins un iframe
    ?>
    <div id="buscador">
        <input type="text" id="tfBuscar" placeholder="Entrar text per filtrar...">
        <input type="submit" class="boto_buscador" value=""/>
    </div>

    <?php
}

if (!$_GET['iframe']) {//en cas que no siguem dins un iframe
    ?>
    <h2 class="<?php echo $_GET['group_id'] == 32 ? 'slideshows' : 'fotos'; ?>"><?php echo TITLE_PAGE . (tep_not_null($groupInfo->name) ? ': ' . $groupInfo->name : ''); ?></h2>
    <?php
}

if (substr($_GET['action'], 0, 3) == 'new') {
// Fitxa
    if ($_GET['action'] == 'new_item') {
        if (isset($_GET['bID']) && tep_not_null($_GET['bID'])) {
            $id = $_GET['bID'];
            $query_raw = "select f.id, f.name,f.image,f.efecte, f.listorder,fg.foto_portada,fg.id_ratios,fg.parent_id,fr.nom,fr.ratio,fr.thumb1_w,fr.thumb1_h,fr.thumb2_w,fr.thumb2_h,fr.thumb3_h,fr.thumb3_w from " . TABLE_FOTOS . " f left join " . TABLE_FOTOS_GROUPS . " fg ON f.group_id=fg.id left join " . TABLE_FOTOS_RATIOS . " fr ON fg.id_ratios=fr.id WHERE f.id = '" . (int) $id . "'";
            $item = tep_db_fetch_array(tep_db_query($query_raw));
        } else {
            $item = array('id' => '',
                'title' => '',
                'name' => '',
                'listorder' => '');
        }
        $itemInfo = new objectInfo($item);
        $form_action = ($_GET['bID']) ? 'update_item' : 'insert_item';
        ?>
        <div id="form">
            <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=' . $form_action . '&amp;iframe=' . $_GET['iframe'] . '&amp;idLlistat=' . $_GET['idLlistat']), 'post', 'enctype="multipart/form-data"'); ?>
            <?php if (!$_GET['iframe']) { ?>
                <p class="botonera">
                    <?php
                    echo '<a href="javascript:void(0);" class="check_form"><span>Desar la Imatge</span></a>';

                    if (isset($_GET['from'])) {//Redireccio si venim d'un altre modul
                        echo '<a href="' . tep_href_link('', 'modul=' . $_GET['from']) . '&pID=' . $_GET['pID'] . '"><span>' . BUTTON_BACK . '</span></a>';
                    } else {//enrere per defecte
                        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                    }
                    ?>
                </p>
            <?php } ?>
            <h4><span class="media amb_icona">Mèdia</span></h4>
            <fieldset>
                <!--<label class="width_50_1">
                    <span>Nom d'us intern <em class="help">(admin)</em></span>
                    <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
                </label>-->
                <label class="width_50_2" style="display: none;">
                    <span>Ordre</span>
                    <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
                    <?php echo tep_draw_hidden_field('listorder_aux', $itemInfo->listorder); ?>
                </label>
                <label>
                    <input type="checkbox" name="portada" value="on" <?php echo (($itemInfo->foto_portada == $itemInfo->id && $itemInfo->id != "") ? 'checked="checked"' : ''); ?> class="checkbox" />
                    <span class="inline">Fes aquesta imatge portada de la galeria / àlbum.</span>
                </label>
                <p class="form_sep"></p>
                <?php
                if (tep_not_null($itemInfo->image)) {
                    echo tep_draw_hidden_field('foto_prev', $itemInfo->image) . "\n";
                    echo '<label class="width_50_1"><span>Imatge Actual</span><input type="text" id="foto_act" name="foto_act" value="' . $itemInfo->image . '" class="camp_read" readonly="readonly" />' . "\n"
                    . '<a href="javascript:void(0);" class="link_del"></a>' . "\n"
                    . '<a href="' . DIR_WS_PORTAL_IMAGE_FOTOS . $itemInfo->image . '" rel="colorbox" title="' . $itemInfo->image . '" class="link_view"></a></label>' . "\n"
                    . '<label class="width_50_2"><span>Nova Imatge</span><input type="file" id="foto_new" name="foto_new" value="" size="45" class="file" /></label>' . "\n"
                    . '<input type="hidden" name="foto_vella" id="foto_vella"  value="' . $itemInfo->image . '"/>' . "\n";
                    ?>
                    <p class="form_sep"></p>
                    <label id="miniatura">
                        <div class="crop">
                            <div class="nom escalar amb_icona">Modificar miniatura <em class="help">(arrossega el cursor dins la imatge)</em></div>
                            <img src="<?php echo(DIR_WS_PORTAL_IMAGE_FOTOS . $itemInfo->image); ?>" id="jcrop" />
                        </div>
                        <div id="thumb_actual">
                            <div class="nom">Miniatura ACTUAL</div>
                            <img  src="<?php echo(DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $itemInfo->image . '?' . time()); ?>"/>
                        </div>
                        <div id="thumb_nou">
                            <div class="nom">Previsualització de la NOVA miniatura</div>
                            <?php
                            //calculem la miniatura
                            $ratio = $itemInfo->ratio;
                            $width = 100 * $ratio;
                            $height = 100;
                            ?>
                            <div id="thumb" style="width: <?php echo $width; ?>px; height:<?php echo $height; ?>px; ">
                                <img src="<?php echo(DIR_WS_PORTAL_IMAGE_FOTOS . $itemInfo->image); ?>" id="preview" alt="Preview" class="jcrop-preview" />
                            </div>
                        </div>

                        <input type="hidden" id="x" name="x" />
                        <input type="hidden" id="y" name="y" />
                        <input type="hidden" id="w" name="w" />
                        <input type="hidden" id="h" name="h" />
                    </label>
                    <?php
                } else {
                    echo '<label class="width_50_1"><span>Imatge</span><input type="file" id="foto_new" name="foto_new" value="" size="45" class="file" /></label>' . "\n";
                }
                ?>
            </fieldset>
            <!-- Idiomes -->
            <h4><span class="general amb_icona">Textos</span></h4>
            <fieldset>
            <?php
            if (sizeof($languages) > 1) {
                echo '<ul id="menuIdioma">';
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                    echo '<li><a class="tab ' . (($i == 0) ? 'current' : '') . '" title="infolang' . $i . '">' . $languages[$i]['name'] . '</a></li>';
                }
                echo '</ul>';
            }

            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                // Busquem valors
                $aux_listing = tep_db_query("select link, target, title,autor,description, class, rel, rev,title_slide,subtitle,subtitle2 from " . TABLE_FOTOS_DESCRIPTION . " where fotos_id = '" . $itemInfo->id . "' and language_id = '" . (int) $languages[$i]['id'] . "'");
                $aux_numrows = tep_db_num_rows($aux_listing);
                if ($aux_numrows > 0) {
                    $aux_item = tep_db_fetch_array($aux_listing);
                    $auxInfo = new objectInfo($aux_item);
                }
                ?>
                <fieldset id="<?php echo 'infolang' . $i; ?>" class="lang_content" <?php echo (($i == 0) ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                    <label style="display:none;" class="width_50_1">
                        <span>Enllaç*</span>
                        <input type="text" name="link[<?php echo $languages[$i]['id']; ?>]" value="<?php echo (($auxInfo->link) ? $auxInfo->link : 'http://'); ?>" />
                    </label>
                    <label class="width_50_1">
                        <span>Títol de la imatge <em class="help">(El que es mostrarà al web)</em></span>
                        <input type="text" name="title[<?php echo $languages[$i]['id']; ?>]" value="<?php echo html_entity_decode($auxInfo->title, ENT_NOQUOTES); ?>" />
                    </label>
                    <label class="width_50_2">
                        <span>&COPY; Autor de la imatge <em class="help">(Copyright de la foto, si en té)</em></span>
                        <input type="text" name="autor[<?php echo $languages[$i]['id']; ?>]" value="<?php echo html_entity_decode($auxInfo->autor, ENT_NOQUOTES); ?>" />
                    </label>
                    <p class="form_sep"></p>

                    <?php if ($groupInfo->parent_id == FOTOS_GROUP_SLIDESHOW) { ?>
                        <h4><span class="general amb_icona">Textos slide</span></h4>
                        <fieldset>  
                            <label class="width_50_1">
                                <span>Text línia 1 </span>
                                <input type="text" name="title_slide[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->title_slide; ?>" />
                            </label>
                            <label class="width_50_2">
                                <span>Text línia 2  </span>
                                <input type="text" name="subtitle[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->subtitle; ?>" />
                            </label>
                            <label class="width_50_1">
                                <span>Text línia 3  </span>
                                <input type="text" name="subtitle2[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->subtitle2; ?>" />
                            </label>
                        </fieldset>
                        <?php if (tep_usuaris_es_admin()) { //cas admin pot fer la descripció a mida. (SI CAMP titol_slide ÉS PLE NO FA CAS DE CAMP description)  ?>
                            <div class="editor_content">
                                <textarea name="description[<?php echo $languages[$i]['id']; ?>]" id="description[<?php echo $languages[$i]['id']; ?>]" >
                                    <?php echo $auxInfo->description; ?>
                                </textarea>
                                <script>
                                    <?php 
                                    if (!tep_usuaris_es_admin())
                                    { 
                                        echo("CKEDITOR.config.toolbar = 'Editor';");                     
                                    }
                                    ?>
                                    CKEDITOR.replace( 'description[<?php echo $languages[$i]['id']; ?>]');
                                </script>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <label style="display:none;" class="width_50_1">
                        <span>Target*</span>
                        <?php echo tep_draw_pull_down_menu('target[' . $languages[$i]['id'] . ']', $target_array, $auxInfo->target, ''); ?>
                    </label>
                    <label style="display:none;" class="width_50_2">
                        <span>Class</span>
                        <input type="text" name="class[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->class; ?>" />
                    </label>
                    <label style="display:none;" class="width_50_1">
                        <span>Rel</span>
                        <input type="text" name="rel[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rel; ?>" />
                    </label>
                    <label style="display:none;" class="width_50_2">
                        <span>Rev</span>
                        <input type="text" name="rev[<?php echo $languages[$i]['id']; ?>]" value="<?php echo $auxInfo->rev; ?>" />
                    </label>
                </fieldset>
                <?php
                //$params_ckeditor['toolbar'] = 'Full';
                //$params_ckeditor['height'] = '250';
                //$ckeditor->replace('description[' . $languages[$i]['id'] . ']', $params_ckeditor);
            }
            ?>
            </fieldset>
            <?php if ($groupInfo->parent_id == FOTOS_GROUP_SLIDESHOW) { ?>
                <h4 class="desplegable collapsed"><span class="config amb_icona">Configuració Slide</span></h4>    
                <fieldset>
                    <label class="width_50_1">
                        <span>Efecte canvi imatge</span>
                        <select name="efecte">
                            <?php
                            $efectes_transisions = array('fade', 'boxslide', 'boxfade', 'slotzoom-horizontal', 'slotslide-horizontal', 'slotfade-horizontal', 'slotzoom-vertical', 'slotslide-vertical', 'slotfade-vertical', 'curtain-1', 'curtain-2', 'curtain-3', 'slideleft', 'slideright', 'slideup', 'slidedown', 'random', 'slidehorizontal', 'slidevertical', 'papercut', 'flyin', 'turnoff', 'cube', '3dcurtain-vertical', '3dcurtain-horizontal');
                            foreach ($efectes_transisions as $efecte) {
                                ?>
                                <option <?php echo $itemInfo->efecte == $efecte ? 'selected="selected"' : ''; ?> value="<?php echo $efecte; ?>"><?php echo $efecte; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </label>
                </fieldset>
            <?php } ?>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>Desar la Imatge</span></a>';

                if (isset($_GET['from'])) {//Redireccio si venim d'un altre modul
                    echo '<a class="enrere" href="' . tep_href_link('', 'modul=' . $_GET['from']) . '&pID=' . $_GET['pID'] . '"><span>' . BUTTON_BACK . '</span></a>';
                } else {//enrere per defecte
                    echo '<a class="enrere" href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                }
                ?>
            </p>
        </form>
        </div>

        <?php
// Formulari Categories
    } else if ($_GET['action'] == 'new_group') {

        if (($_GET['gID'])) {
            $id = $_GET['gID'];
//obtenim info de la galeria
            $item = tep_db_fetch_array(tep_get_galeria_by_id($id));
        } else {
            $item = array('name' => '',
                'listorder' => '');
        }
        $itemInfo = new objectInfo($item);
        $form_action = ($_GET['gID']) ? 'update_group' : 'insert_group';
        ?>

        <div id="form">
            <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y', 'group_id')) . 'action=' . $form_action . '&amp;iframe=' . $_GET['iframe'] . '&amp;idLlistat=' . $_GET['idLlistat']), 'post', 'enctype="multipart/form-data"'); ?>
            <?php if (!$_GET['iframe']) { ?>
                <p class="botonera">
                    <?php
                    echo '<a href="javascript:void(0);" class="check_form"><span>Desar la Galeria</span></a>';
                    if (!$_GET['iframe']) {//en cas que no siguem dins un iframe, mostrem opció de tornar a veure llistat categories
                        if (isset($_GET['from'])) {//Redireccio si venim d'un altre modul
                            echo '<a href="' . tep_href_link('', 'modul=' . $_GET['from'] . '&pID=' . $_GET['from_pID'] . '&amp;action=' . $_GET['from_action']) . '"><span>' . BUTTON_BACK . '</span></a>';
                        } else {//enrere per defecte
                            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                        }
                    }
                    ?>
                </p>
            <?php } ?>
            <input type="hidden" name="parent_id" value="<?php echo $_GET['group_id']; ?>" />

            <fieldset >
                <label class="width_50_1">
                    <span>Nom</span>
                    <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
                </label>
                <label class="width_50_2">
                    <span>Ordre</span>
                    <input type="text" name="listorder" value="<?php echo $itemInfo->listorder; ?>" />
                </label>
                <?php
                if (!isset($_GET['group_id']) || $_GET['group_id'] == 0) {//si es una categoria pare mostrem seleccionar ratio
                    ?>                
                    <label class="width_50_1">
                        <span>Mides miniatures</span>
                        <?php echo tep_draw_pull_down_menu('id_ratios', tep_get_fotos_ratios_array($itemInfo->id_ratios), $itemInfo->id_ratios, ''); ?>
                    </label>
                    <?php
                } else {//si NO es una categoria pare (es una galeria) agafem el RATIO DEL PARE
                    tep_get_ratio_final_galeria($_GET['group_id']);
                    ?>

                    <input type="hidden" name="id_ratios" value="<?php echo tep_get_ratio_final_galeria($_GET['group_id']); ?>"/>
                <?php } ?>
            </fieldset>
            <p class="botonera">
                <?php
                echo '<a href="javascript:void(0);" class="check_form"><span>Desar la Galeria</span></a>';
                if (!$_GET['iframe']) {//en cas que no siguem dins un iframe, mostrem opció de tornar a veure llistat categories
                    if (isset($_GET['from'])) {//Redireccio si venim d'un altre modul
                        echo '<a href="' . tep_href_link('', 'modul=' . $_GET['from'] . '&pID=' . $_GET['from_pID'] . '&amp;action=' . $_GET['from_action']) . '"><span>' . BUTTON_BACK . '</span></a>';
                    } else {//enrere per defecte
                        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
                    }
                }
                ?>
            </p>
        </form>
        </div>

        <?php
    }
} else {
//LLISTATS
//mirem si estem dins d'una carpeta/categoria
    $group_id = ($_GET['group_id'] != '') ? $_GET['group_id'] : 0;
//obtenim les categories filles
    $groups_query = tep_get_galeries_by_parent_id($group_id);
    $groups_rows = tep_db_num_rows($groups_query);
//Obtenim info de la galeria
    $group = tep_db_fetch_array(tep_get_galeria_by_id($group_id));
    ?>
    <?php if (!$_GET['iframe']) { ?>
        <p class="botonera">
            <?php
            if (($group['parent_id'] == 0 && $group != 0) || ($group == 0 && tep_usuaris_es_admin())) {//nova CATEGORIA  si es CATEGORIA PARE ARREL només pot crear ADMIN
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_group&amp;iframe=' . $_GET['iframe']) . '&amp;idLlistat=' . $_GET['idLlistat'] . '"><span>' . NEW_CATEGORY . '</span></a>';
            } elseif ($group['parent_id'] != 0 && $group != 0) {//nova FOTO (nomes dins de les galeries NO ARRELS)
                echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_item&amp;iframe=' . $_GET['iframe']) . '&amp;idLlistat=' . $_GET['idLlistat'] . '"><span>' . NEW_ITEM . '</span></a>';
            }
            ?>
        </p>

        <?php
    }
    if (!isset($_GET['group_id']) || !tep_not_null($_GET['group_id']) || $group['parent_id'] == 0) {
// CATEGORIES
        ?>

        <table summary="<?php echo TAULA; ?>"  class="llista fixed">
            <thead>
                <tr>
                    <th style="width:45%;"><?php echo COL_1; ?><?php echo($group != 0) ? ' <span class="help">(Nº imatges de la galeria)</span>' : ''; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo($group != 0) ? 'Foto Portada' : ''; ?></th>
                    <th style="width:10%; text-align:center;"><?php echo COL_3; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="5">
            <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_GROUPS, $groups_rows); ?></div>
        </th>
        </tr>
        </tfoot>
        <tbody>

            <?php
            if ($groups_rows > 0) {
                ?>
                <tr class="taula2" onclick="window.location.href='<?php echo tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'gID', 'pID', 'x', 'y', 'group_id')) . 'group_id=' . $group['parent_id']); ?>'">
                    <td colspan="5"><a href="<?php echo tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'gID', 'pID', 'x', 'y', 'group_id')) . 'group_id=' . $group['parent_id']); ?>"><?php echo ICON_FOLDER; ?>..</a></td>
                </tr>
                <?php
                // Categories
                while ($categories = tep_db_fetch_array($groups_query)) {
                    $i++;
                    $aux = ($aux % 2) + 1;
                    $cInfo = new objectInfo($categories);

                    $aux_order = '';
                    if ($cInfo->listorder > 1) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=down&amp;gID=' . $cInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }
                    $aux_order .= '&nbsp;&nbsp;' . (($cInfo->listorder < 10) ? '0' : '') . $cInfo->listorder . '&nbsp;&nbsp;';
                    if ($cInfo->listorder < $groups_rows) {
                        $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'value', 'keyword', 'x', 'y')) . 'action=listorder_group&amp;value=up&amp;gID=' . $cInfo->id) . '">' . ICON_ORDER_UP . '</a>';
                    } else {
                        $aux_order .= ICON_ORDER_NO;
                    }
                    if ($cInfo->active) {
                        $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=desactive_category&amp;cID=' . $cInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                    } else {
                        $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=active_category&amp;cID=' . $cInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                    }
                    if ($_GET['group_id'] != 0) {//cas llistat galeries
                        $num_fotos = '<span class="help"> (' . tep_fotos_count_by_galeria($cInfo->id) . ' imatges)<span>';
                    }
                    $portada = tep_db_fetch_array(tep_get_foto_portada_galeria($cInfo->id));
                    echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y', 'group_id')) . 'group_id=' . $cInfo->id) . '&amp;iframe=' . $_GET['iframe'] . '&amp;idLlistat=' . $_GET['idLlistat'] . '\'" class="' . (($cInfo->id == $_GET['gID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                    . '  <td class="nom"><a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y', 'group_id')) . 'group_id=' . $cInfo->id) . '&amp;iframe=' . $_GET['iframe'] . '&amp;idLlistat=' . $_GET['idLlistat'] . '">' . ICON_FOLDER . $cInfo->name . $num_fotos . '</a></td>' . "\n";
                    echo '  <td style="text-align:center;">';
                    if ($group != 0) {//si es CATEGORIA PARE ARREL no mostrem foto
                        echo '<img class="foto_llistat_peque" src="' . DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $portada['image'] . '?' . time() . '"/>'; //afegim date al nom de la imatge x forçar refresh
                    }
                    echo '</td>' . "\n";
                    echo '<td style="text-align:center;">';
                    if (($group['parent_id'] == 0 && $group != 0) || ($group == 0 && tep_usuaris_es_admin())) {//si es CATEGORIA PARE ARREL només pot borrar o editar ADMIN
                        echo $aux_active;
                    }
                    echo '</td>' . "\n"
                    . '<td class="order">' . $aux_order . '</td>' . "\n"
                    . '  <td style="text-align:center;" class="accions">';


                    if (($group['parent_id'] == 0 && $group != 0) || ($group == 0 && tep_usuaris_es_admin())) {//si es CATEGORIA PARE ARREL només pot borrar o editar ADMIN
                        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'gID=' . $cInfo->id . '&amp;action=new_group&amp;iframe=' . $_GET['iframe']) . '&amp;idLlistat=' . $_GET['idLlistat'] . '">' . ICON_EDIT . '</a> ';
                        echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'keyword', 'x', 'y')) . 'gID=' . $cInfo->id . '&amp;action=delete_group&amp;iframe=' . $_GET['iframe']) . '&amp;idLlistat=' . $_GET['idLlistat'] . '"  class="delete">' . ICON_DELETE . '</a>';
                    }
                    echo '</td>' . "\n"
                    . '</tr>' . "\n";
                }
            } else {
                // No Items
                echo '<tr >' . "\n";
                echo '  <td colspan="5" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_CATEGORY, 'UTF-8')) . '</td>' . "\n";
                echo '</tr>' . "\n";
            }
        } else {
            // Llistat de fotos
            $query_raw = "select b.id, b.name, b.active, b.listorder,b.image from " . TABLE_FOTOS . " b left join " . TABLE_FOTOS_DESCRIPTION . " bd on bd.fotos_id = b.id and bd.language_id = '" . (int) $language_id . "' where b.group_id = '" . (int) $_GET['group_id'] . "' order by listorder";
            $listing = tep_db_query($query_raw);
            $query_numrows = tep_db_num_rows($listing);
            //mirar foto portada
            $query_raw_aux = "select fg.foto_portada from " . TABLE_FOTOS_GROUPS . " fg WHERE fg.id = '" . (int) $_GET['group_id'] . "'";
            $listing_aux = tep_db_query($query_raw_aux);
            $item_aux = tep_db_fetch_array($listing_aux);
            $itemInfo_aux = new objectInfo($item_aux);
            ?>

        <table summary="<?php echo TAULA; ?>" class="llista fixed">
            <thead>
                <tr>
                    <th style="width:27%;">Imatge</th>
                    <th style="width:38%;"><?php echo COL_1; ?></th>
                    <!--<th style="width:20%; text-align:left;"><?php echo COL_2; ?></th>-->
                    <th style="width:5%; text-align:center;"><?php echo COL_3; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
                    <th style="width:15%; text-align:center;"><?php echo COL_5; ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="5">
            <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS, $query_numrows) ?></div>
            </th>
            </tr>
            </tfoot>
            <tbody>

                <?php
                // Tornar a categories
                if (!$_GET['iframe']) {//en cas que no siguem dins un iframe, mostrem opció de tornar a veure llistat categories
                    echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'group_id', 'gID', 'bID', 'x', 'y')) . 'group_id=' . $group['parent_id']) . '\'" class="taula2">' . "\n"
                    . '  <td colspan="5"><a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'group_id', 'gID', 'bID', 'x', 'y')) . 'group_id=' . $group['parent_id']) . '">' . ICON_FOLDER . '..</a></td>' . "\n"
                    . '</tr>' . "\n";
                }

                if ($query_numrows > 0) {
                    // Llistat segons búsqueda
                    $i = 0;
                    while ($item = tep_db_fetch_array($listing)) {
                        $i++;
                        $aux = ($aux % 2) + 1;
                        $itemInfo = new objectInfo($item);

                        $aux_order = '';
                        if ($itemInfo->listorder > 1) {
                            $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'value', 'buscar', 'x', 'y')) . 'action=listorder_item&amp;value=down&amp;bID=' . $itemInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
                        } else {
                            $aux_order .= ICON_ORDER_NO;
                        }
                        $aux_order .= '&nbsp;&nbsp;' . (($itemInfo->listorder < 10) ? '0' : '') . $itemInfo->listorder . '&nbsp;&nbsp;';
                        if ($itemInfo->listorder < $query_numrows) {
                            $aux_order .= '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'value', 'buscar', 'x', 'y')) . 'action=listorder_item&amp;value=up&amp;bID=' . $itemInfo->id) . '">' . ICON_ORDER_UP . '</a>';
                        } else {
                            $aux_order .= ICON_ORDER_NO;
                        }

                        if ($itemInfo->active) {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=desactive&amp;bID=' . $itemInfo->id) . '" class="desactive">' . ICON_CONFIRMED . '</a>';
                        } else {
                            $aux_active = '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=active&amp;bID=' . $itemInfo->id) . '" class="active">' . ICON_CONFIRM . '</a>';
                        }

                        echo '<tr onclick="window.location.href=\'' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=new_item&amp;bID=' . $itemInfo->id) . '&amp;iframe=' . $_GET['iframe'] . '&amp;idLlistat=' . $_GET['idLlistat'] . '\'" class="' . (($itemInfo->id == $_GET['bID']) ? 'current_list' : 'taula' . $aux) . '">' . "\n"
                        . '  <td>'
                        . '<a href="' . DIR_WS_PORTAL_IMAGE_FOTOS . $itemInfo->image . '" rel="colorbox" title="' . $itemInfo->image . '">'
                        . '<img class="foto_llistat" src="' . DIR_WS_PORTAL_IMAGE_FOTOS . 'thumbs/' . $itemInfo->image . '?' . time() . '"/>' . (($itemInfo_aux->foto_portada == $itemInfo->id) ? '<span class="foto_portada"></span>' : '')
                        . '</a>'
                        . '</td>'//afegim date al nom de la imatge x forçar refresh
                        . '  <td class="nom">' . $itemInfo->name . '</td>' . "\n"
                        //. '  <td><a href="' . DIR_WS_PORTAL_IMAGE_FOTOS . $itemInfo->image . '" rel="colorbox" title="' . $itemInfo->image . '" class="link_view">' . $itemInfo->image . '</a></td>' . "\n"
                        . '  <td style="text-align:center;">' . $aux_active . '</td>' . "\n"
                        . '  <td class="order">' . $aux_order . '</td>' . "\n"
                        . '  <td style="text-align:center;" class="accions">'
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=new_item&amp;bID=' . $itemInfo->id) . '&amp;iframe=' . $_GET['iframe'] . '&amp;idLlistat=' . $_GET['idLlistat'] . '">' . ICON_EDIT . '</a> '
                        . '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'buscar', 'x', 'y')) . 'action=delete_item&amp;bID=' . $itemInfo->id) . '&amp;iframe=' . $_GET['iframe'] . '&amp;idLlistat=' . $_GET['idLlistat'] . '" class="delete">' . ICON_DELETE . '</a>'
                        . '</td>' . "\n"
                        . '</tr>' . "\n";
                    }
                } else {
                    // No Items
                    echo '<tr>' . "\n";
                    echo '  <td colspan="6" class="no_item">' . sprintf(NO_ITEMS, mb_strtolower(NEW_ITEM, 'UTF-8')) . '</td>' . "\n";
                    echo '</tr>' . "\n";
                }
            }
            ?>

        </tbody>
    </table>

    <p class="botonera">
        <?php
        if (($group['parent_id'] == 0 && $group != 0) || ($group == 0 && tep_usuaris_es_admin())) {//nova CATEGORIA  si es CATEGORIA PARE ARREL només pot crear ADMIN
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_group&amp;iframe=' . $_GET['iframe']) . '&amp;idLlistat=' . $_GET['idLlistat'] . '"><span>' . NEW_CATEGORY . '</span></a>';
        } elseif ($group['parent_id'] != 0 && $group != 0) {//nova FOTO (nomes dins de les galeries NO ARRELS)
            echo '<a href="' . tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'bID', 'keyword', 'x', 'y')) . 'action=new_item&amp;iframe=' . $_GET['iframe']) . '&amp;idLlistat=' . $_GET['idLlistat'] . '"><span>' . NEW_ITEM . '</span></a>';
        }
        ?>
    </p>
    <?php
}
?>
