<?php
/*
  JavascriptScript espec�fic pel m�dul
 */
?>
<script type="text/javascript" language="javascript">
    //<![CDATA[
    function validarCamps(){
        var error = 0;
        var error_message = "<?php echo JS_ERROR; ?>";

        var action = '<?php echo $_GET['action']; ?>';//per tal de validar si som grup o foto
        var name = trim(document.form_data.name.value);
        
        //validem que hi hagi seleccionada una imatge
        var img_new ='';
        if($('#foto_new').length>0){
            img_new = document.form_data.foto_new.value;
        }
        var img_act = '';
        if($('#foto_act').length>0){
            img_act = document.form_data.foto_act.value;
        }

        if (!/^.{2,}$/.test(name)) {
            error_message = error_message + "<?php echo JS_NOM; ?>";
            error = 1;
        }
        if (action!='new_group' && !/^.{2,}$/.test(img_new) && !/^.{2,}$/.test(img_act)) {
            error_message = error_message + "<?php echo JS_FOTO; ?>";
            error = 1;
        }

        if (error == 1) {
            alert(error_message);
            return false;
        } else {
            return true;
        }
        
    }
    
<?php
if (substr($_GET['action'], 0, 3) == 'new') {

    // Fitxa
    if ($_GET['action'] == 'new_item') {
        $id = $_GET['group_id'];
        $query_raw = "select fr.id,fr.nom,fr.ratio,fr.thumb1_w,fr.thumb1_h,fr.thumb2_w,fr.thumb2_h,fr.thumb3_h,fr.thumb3_w from " . TABLE_FOTOS_GROUPS . " fg left join " . TABLE_FOTOS_RATIOS . " fr ON fg.id_ratios=fr.id  WHERE fg.id = '" . (int) $id . "'";
        $item = tep_db_fetch_array(tep_db_query($query_raw));
        $itemInfo = new objectInfo($item);
        ?>
                    //JCROP
                    $(document).ready(function() {
                        var jcrop_api, boundx, boundy;

                        $('#jcrop').Jcrop({
                            onChange: updatePreview,
                            onSelect: updatePreview,
                            aspectRatio: <?php echo $itemInfo->ratio; ?>,
                            bgFade:     true,
                            bgOpacity: .5,
                            addClass: 'jcrop-light',
                            boxWidth: 445
                        },function(){
                            // Use the API to get the real image size
                            var bounds = this.getBounds();
                            boundx = bounds[0];
                            boundy = bounds[1];
                            // Store the API in the jcrop_api variable
                            jcrop_api = this;
                        });

                        function updatePreview(c)
                        {
                            //Al comenar a editar mostrem previsualitzacio
                            $('#thumb_nou').show();

                            //agafem coordenades per a passar a form (multipliquem per dos ja que amb css a la imatge principal on apliquem el crop hi hem fet un with del 50%)
                            $('#x').val(c.x);
                            $('#y').val(c.y);
                            $('#w').val(c.w);
                            $('#h').val(c.h);

                            if (parseInt(c.w) > 0)
                            {
                                //var rx = 150 / c.w;
                                //var ry = 100 / c.h;
                                var rx = (100* <?php echo $itemInfo->ratio; ?>) / c.w;
                                var ry = (100) / c.h;

                                $('#preview').css({
                                    width: Math.round(rx * boundx) + 'px',
                                    height: Math.round(ry * boundy) + 'px',
                                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                                });
                            }
                        };
                    });
        <?php
    }
}
?>

    //]]>
</script>
