<h2 class="config"><?php echo TITLE_PAGE; ?></h2>


<?php
if ($_GET['action'] == 'new') {
  // Fitxa
  if (isset($_GET['gID']) && tep_not_null($_GET['gID'])) {
    $id = $_GET['gID'];
    $query_raw = "select c.id, c.name, c.description from " . TABLE_CONFIG_GROUPS . " c where c.id = '" . (int)$id . "'";
    $item = tep_db_fetch_array(tep_db_query($query_raw));
  } else {
    $item = array('name' => '',
	        	  'description' => '');
  }
  $itemInfo = new objectInfo($item);

  $form_action = ($_GET['gID']) ? 'update' : 'insert';
?>

<div id="form">
  <?php echo tep_draw_form('form_data', tep_href_link('', tep_get_all_get_params(array('action','pos','x','y')) . 'action=' . $form_action), 'post', 'enctype="multipart/form-data"'); ?>
  <fieldset>
    <label>
      <span>Nom</span>
      <input type="text" name="name" value="<?php echo $itemInfo->name; ?>" />
    </label>
    <label>
      <span>Descripció</span>
      <textarea name="description" class="info" rows="10" cols="10"><?php echo $itemInfo->description; ?></textarea>
    </label>
  </fieldset>
  <p class="botonera">
    <?php
    echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
    echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','gID','x','y'))) . '"><span>' . BUTTON_BACK . '</span></a>';
    ?>
  </p>
  </form>
</div>

<?php
} else {
  // Llistat
  $query_raw = "select c.id, c.name, c.description, c.listorder from " . TABLE_CONFIG_GROUPS . " c order by c.listorder";
  $listing = tep_db_query($query_raw);
  $query_numrows = tep_db_num_rows($listing);
?>

<table summary="<?php echo TAULA; ?>" class="llista fixed">
  <thead>
    <tr>
      <th style="width:25%;"><?php echo COL_1; ?></th>
      <th style="width:45%;"><?php echo COL_2; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_3; ?></th>
      <th style="width:15%; text-align:center;"><?php echo COL_4; ?></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="4">
        <div id="split_left"><?php echo @sprintf(TEXT_DISPLAY_NUMBER_OF_ITEMS,$query_numrows)?></div>
      </th>
    </tr>
  </tfoot>
  <tbody>

<?php
if ($query_numrows>0) {
  // Llistat segons búsqueda
  $i=0;
  while ($item = tep_db_fetch_array($listing)) {
  	$i++;
  	$aux=($aux%2)+1;
    $itemInfo = new objectInfo($item);

  	echo '<tr onclick="window.location.href=\''.tep_href_link('','modul=config_values&amp;gID=' . $itemInfo->id).'\'" class="' . (($itemInfo->id == $_GET['gID'])? 'current_list' : 'taula' . $aux) . '">' . "\n"
     . '  <td>' . $itemInfo->name . '</td>' . "\n"
     . '  <td>' . htmlentities($itemInfo->description, ENT_QUOTES | ENT_IGNORE, "UTF-8") . '</td>' . "\n";

    $aux_order = '';
    if ($itemInfo->listorder > 1) {
      $aux_order .= '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','gID','value','buscar','x','y')) . 'action=listorder&amp;value=down&amp;gID=' . $itemInfo->id) . '">' . ICON_ORDER_DOWN . '</a>';
    } else {
      $aux_order .= ICON_ORDER_NO;
    }
    $aux_order .= '&nbsp;&nbsp;' . (($itemInfo->listorder < 10) ? '0' : '') . $itemInfo->listorder . '&nbsp;&nbsp;';
    if ($itemInfo->listorder < $query_numrows) {
      $aux_order .= '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','gID','value','buscar','x','y')) . 'action=listorder&amp;value=up&amp;gID=' . $itemInfo->id) . '">' . ICON_ORDER_UP . '</a>';
    } else {
      $aux_order .= ICON_ORDER_NO;
    }

    echo '  <td class="order">' . $aux_order . '</td>' . "\n"
     . '  <td class="accions">'
     . '<a href="' . tep_href_link('','modul=config_values&amp;gID=' . $itemInfo->id) . '">' . ICON_VIEW . '</a> '
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','gID','buscar','x','y')) . 'action=new&amp;gID=' . $itemInfo->id) . '">' . ICON_EDIT . '</a> '
     . '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','gID','buscar','x','y')) . 'action=delete&amp;gID=' . $itemInfo->id) . '" class="delete">' . ICON_DELETE . '</a>'
     . '</td>' . "\n"
     . '</tr>' . "\n";
  }

} else {
  // No Items
  echo '<tr>' . "\n";
  echo '  <td colspan="4" class="no_item">' . sprintf(NO_ITEMS,mb_strtolower(NEW_ITEM,'UTF-8')) . '</td>' . "\n" ;
  echo '</tr>' . "\n";

}
?>

  </tbody>
</table>

<p class="botonera">
  <?php echo '<a href="' . tep_href_link('',tep_get_all_get_params(array('action','gID','x','y')) . 'action=new') . '"><span>' . NEW_ITEM . '</span></a>'; ?>
</p>

<?php
}
?>
