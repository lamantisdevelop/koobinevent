<?php

// Accions
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'new'://mirem permisos edicio
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            break;
        case 'insert':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_POST['name']) {
                // Busquem nou ordre
                $order_now = tep_search_new_order(TABLE_CONFIG_GROUPS);
                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input($_POST['name']),
                    'description' => tep_db_prepare_input(ucfirst($_POST['description'])),
                    'listorder' => (int) $order_now,
                    'visible' => '1');
                // Crida base dades
                tep_db_perform(TABLE_CONFIG_GROUPS, $sql_data_array);
                $aux_id = tep_db_insert_id();
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y')) . 'gID=' . $aux_id));
            break;

        case 'update':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['gID'] && $_POST['name']) {
                // Identificador Usuari
                $aux_id = tep_db_prepare_input($_GET['gID']);
                // Vector entrades
                $sql_data_array = array('name' => tep_db_prepare_input(ucfirst($_POST['name'])),
                    'description' => tep_db_prepare_input(ucfirst($_POST['description'])));
                // Crida base dades
                tep_db_perform(TABLE_CONFIG_GROUPS, $sql_data_array, 'update', 'id = \'' . (int) $aux_id . '\'');
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            break;

        case 'delete':
            tep_usuaris_filtrar_acces(PERMIS_ESBORRAR);
            if ($_GET['gID']) {
                $aux_id = tep_db_prepare_input($_GET['gID']);
                // Busquem i modifiquem ordre
                $order_now = tep_search_item_order($aux_id, TABLE_CONFIG_GROUPS);
                tep_reorder_items($order_now, TABLE_CONFIG_GROUPS);
                // Eliminem entrada
                tep_db_query("delete from " . TABLE_CONFIG_GROUPS . " where id = '" . (int) $aux_id . "'");
                // Eliminem entrades atributs a usuaris
                tep_db_query("delete from " . TABLE_CONFIG_VALUES . " where group_id = '" . (int) $aux_id . "'");
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'gID', 'x', 'y'))));
            break;

        case 'listorder':
            tep_usuaris_filtrar_acces(PERMIS_ESCRIPTURA);
            if ($_GET['gID'] && isset($_GET['value'])) {
                // Identificador
                $aux_id = tep_db_prepare_input($_GET['gID']);
                // Trobar posicio actual
                $order_now = tep_search_item_order($aux_id, TABLE_CONFIG_GROUPS);
                $pos_aux = (($_GET['value'] == 'up') ? $order_now + 1 : $order_now - 1 );
                // Busquem id pos_aux per intercanviar posicions
                tep_change_order($aux_id, $order_now, $pos_aux, TABLE_CONFIG_GROUPS);
            }
            // Redirecció
            tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'value', 'x', 'y'))));
            break;
    }
}

// Textos
$titular = WEB_NAME . ' | ' . PAGE_TITLE;
?>
