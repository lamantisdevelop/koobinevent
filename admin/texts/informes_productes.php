<?php
// Text
define('TITLE_PAGE','Informes Productos');

define('TEXT_LIST','
<p>Selecciona una de las diferentes opciones de visualización disponibles.
</p>
');

define('SEARCH','Visualizar por: ');

define('TAULA','Esta tabla contiene las noticias introducidas en la base de datos.');
define('COL_1','Id');
define('COL_2','Producto');
define('COL_3','Fecha Alta');
define('COL_4','Total');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS', 'Viendo del <b>%d</b> al <b>%d</b> (de <b>%d</b> productos)');
?>
