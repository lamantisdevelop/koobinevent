<?php
// Textos
define('TITLE_PAGE','Galeries d\'imatges');
define('NEW_CATEGORY','Nova Galeria');
define('NEW_ITEM','Nova Imatge');

define('TEXT_LIST','
<p>Aquí es mostren el llistat de banners del web
</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('SEARCH','Visualitzar: ');

define('COL_1','Nom');
define('COL_2a','Id');
define('COL_2','Data Final');
define('COL_3','Actiu');
define('COL_4','Ordre');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_GROUPS','Veient %s galeries');
define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s imatges)');

define('TEXT_ACTIVE','Prem acceptar per activar la imatge.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la imatge.');
define('TEXT_DELETE','Prem acceptar per eliminar la galeria de banners.');

define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST','Error. El directori per imatges no existeix');
define('ERROR_IMAGE_DIRECTORY_IS_NOT_WRITABLE','Error. No es pot escriure en el directori per imatges');
?>
