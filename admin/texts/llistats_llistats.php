<?php
// Textos
define('TITLE_PAGE','Llistats');
define('NEW_ITEM','Nou element');
define('NEW_CATEGORY','Nova Categoria');

define('TEXT_LIST','
<p>Actualitzi els continguts d\'aquesta secció a través del botó editar. Des d\'aquí també pot desactivar blocs de contingut, eliminar-los definitivament i crear-ne de nous.</p>
');
define('TEXT_FORM','
<p>Ha accedit a la ﬁtxa d\'una publicació. En aquesta secció vostè podrà editar la descripció
de les seves publicacions en els diferents idiomes del web. Crei un text nou o modiﬁqui un
text existent. Col·loqui una imatge relacionada i el document associat a la seva publicació.</p>
');

define('SEARCH','Tipus: ');

define('COL_1','Nom');
define('COL_2','Data'); 
define('COL_2a','ID'); 
define('COL_3','Actiu');
define('COL_4','Ordre');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s elements)');
define('TEXT_DISPLAY_NUMBER_OF_GROUPS','Veient %s categories');

define('TEXT_ACTIVE','Prem acceptar per activar l\'element.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la l\'element.');
define('TEXT_DELETE','Prem acceptar per eliminar la l\'element.');
?>
