<?php
// Text
define('TITLE_PAGE','Informes Clientes');

define('TEXT_LIST','
<p>Selecciona una de las diferentes opciones de visualización disponibles.
</p>
');

define('SEARCH','Visualizar por: ');

define('TAULA','Esta tabla contiene las noticias introducidas en la base de datos');
define('COL_1','Id');
define('COL_2','Nombre');
define('COL_3','País (Ciudad)');
define('COL_4','Fecha Alta');
define('COL_5','Total');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS', 'Viendo del <b>%d</b> al <b>%d</b> (de <b>%d</b> clientes)');
?>
