<?php
// Textos
define('TITLE_PAGE','Projecte');
define('NEW_ITEM','Nou Projecte');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('SEARCH','Buscar: ');

define('COL_1','Nom');
define('COL_2','Actiu');
define('COL_3','Ordre');
define('COL_4','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s projectes)');

define('TEXT_DELETE','Prem acceptar per eliminar el projecte.');
define('TEXT_ACTIVE','Prem acceptar per activer el projecte.');
define('TEXT_DESACTIVE','Prem acceptar per desactiver el projecte.');

define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST','Error. El directori per imatges no existeix');
define('ERROR_IMAGE_DIRECTORY_IS_NOT_WRITABLE','Error. No es pot escriure en el directori per imatges');
?>
