<?php
// Text
define('TITLE_PAGE','Informes Búsquedas');

define('TEXT_LIST','
<p>Selecciona una de las diferentes opciones de visualización disponibles.
</p>
');

define('SEARCH','Buscador: ');

define('TAULA','Esta tabla contiene las búsquedas introducidas en la base de datos');
define('COL_1','Búsqueda');
define('COL_2','Alternativa');
define('COL_3','Resultados');
define('COL_4','Total');
define('COL_5','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS', 'Viendo de la <b>%d</b> a la <b>%d</b> (de <b>%d</b> búsuqedas)');

define('TEXT_DELETE','Pulsa aceptar para eliminar la búsqueda.');
?>
