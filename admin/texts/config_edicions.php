<?php
// Textos
define('TITLE_PAGE','Edicions');
define('NEW_ITEM','Nova Edició');

define('TEXT_LIST','
<p>Gestió de les Edicions / Temporades .</p>
');
define('TEXT_FORM','
<p>Permet modificar les dades de l\'edició seleccionada.</p>
');
define('TEXT_FORBIDDEN','<div class="forbidden">
 <p ><b>NO TENS PERMISOS PER A VEURE AQUEST MÒDUL.</b> </p>
 
 <p>Posa\'t amb contacte amb l\'administrador, </p>
 
 <p>Gràcies</p>
</div>');
define('COL_1','Id');
define('COL_2','Nom');
define('COL_3','Data Inici Edició');
define('COL_4','Data Inici');
define('COL_5','Data Final');
define('COL_6','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s a la %s (de %s edicions)');

define('TEXT_DELETE','Prem acceptar per eliminar l\'edició.');
define('TEXT_ACTIVE','Prem acceptar per activar l\'edició.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar l\'edició.');

define('TEXT_CONFLICTE_DATES_EDICIONS','Error. Ja existeix una edició que conté alguna de les dates entrades');
define('TEXT_CONFLICTE_DATES','Error. Les dates que has entrat no tenen un ordre lògic');
define('TEXT_USER_UPDATED','L\'edició ha estat actualitzada correctament.');

define('TEXT_EDICIO_ACTUAL','Edició actual');


?>
