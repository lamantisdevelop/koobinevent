<?php
// Textos
define('TITLE_PAGE','Espectacles:');
define('NEW_ITEM','Nou concert / categoria');
define('NEW_PRODUCT','Nou concert');
define('NEW_CATEGORY','Nova categoria');

define('TEXT_LIST','
<p>Consulta los productos/categorias introducidos en el sitio web.<br/>Las acciones le permiten actualizar el estado, modificar el contenido, mover de categoria, eliminar un registro existente o insertar uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Actualiza o modifica los productos/categorias del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('SEARCH','Buscador: ');
define('CATEGORIES','Categorias: ');

define('TOTES_LES_EDICIONS','TOTES les edicions');

define('COL_1','Nom');
define('COL_2','Modificat');
define('COL_3','Actiu');
define('COL_4','Stat.USA');
define('COL_5','Recom.');
define('COL_6','Ordre');
define('COL_7','Accions');
define('COL_8','Tipus');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s concerts / %s categories');

define('TEXT_ACTIVE','Pulsa acceptar per a activar el concert / categoria');
define('TEXT_DESACTIVE','Pulsa acceptar per a desactivar l\'concert / categoria');
define('TEXT_DELETE','Pulsa acceptar per a eliminar el concert / categoria.');

define('TEXT_RECOMEND','Pulsa aceptar para poner el producto como recomendado.');
define('TEXT_DESRECOMEND','Pulsa aceptar para poner el producto como no recomendado.');
define('TEXT_DELETE_PRODUCT','Selecciona las categorías de las que quieres eliminar el producto:');

define('TEXT_MOVE_PRODUCT','Escull una de les següents opcions:');
define('MOVE_PRODUCT','Moure el producte a la categoria seleccionada.');
define('LINK_PRODUCT','Enllaçar el producte la categoria seleccionada (Mateix producte a diferents categories).');

define('COPY_PRODUCT','Hacer una copia del producto en la categoría seleccionada (Crear un nuevo producto a partir del seleccionado).');
define('COPY_PRODUCT_OPTIONS','Copiar opciones producto.');
define('COPY_PRODUCT_IMG_GAL','Copiar imagénes galeria.');
define('COPY_PRODUCT_DOWNLOADS','Copiar descargas adicionales.');
define('COPY_PRODUCT_RELATED','Copiar productos relacionados.');

define('TEXT_MOVE_CATEGORY','Escull una de les següents opcions:');
define('MOVE_CATEGORY','Moure a la categoria seleccionada (com a subcategoria).');
define('COPY_CATEGORY','Fer una còpia de la categoria a la categoria seleccionada (Crear una nova categoria a partir de la seleccionada).');

define('TEXT_TOP','---------- Top ----------');
define('PLEASE_SELECT_CATEGORY','---------- Selecciona una categoria ----------');
define('PLEASE_SELECT_PRODUCT','---------- Selecciona una subcategoria ------');

define('TEXT_INFO_NO_ITEM','No hay ningún elemento. Introduce algún elemento.');

define('ERROR_PORTAL_IMAGE_DIRECTORY_DOES_NOT_EXIST','Error. El directorio para imágenes no existe.');
define('ERROR_PORTAL_IMAGE_DIRECTORY_NOT_WRITEABLE','Error. No se puede escribir en el directorio para imágenes.');

define('ERROR_PORTAL_VIDEO_DIRECTORY_DOES_NOT_EXIST','Error. El directorio para videos no existe.');
define('ERROR_PORTAL_VIDEO_DIRECTORY_NOT_WRITEABLE','Error. No se puede escribir en el directorio para videos.');

define('ERROR_PORTAL_DOWNLOADS_DIRECTORY_DOES_NOT_EXIST','Error. El directorio para descargas no existe.');
define('ERROR_PORTAL_DOWNLOADS_DIRECTORY_NOT_WRITEABLE','Error. No se puede escribir en el directorio para descargas.');
?>
