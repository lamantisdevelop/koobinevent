<?php
// Textos
define('TITLE_PAGE','Menús');
define('NEW_MENU','Nou Menú');
define('NEW_ITEM','Nou Item');

define('TEXT_LIST','
<p>Consulta els menús definits en el lloc web.<br/>Les accions li permeten canviar el seu estat, modificar el contingut, eliminar un registre existent o introduir-ne un de nou.</p>
');
define('TEXT_FORM','
<p>Introdueix o edita els menús del lloc web.<br/>Realitza els canvis necessaris. Els camps marcats amb asterisc són obligatoris.</p>
');

define('COL_1','Titol');
define('COL_2','Tipus');
define('COL_3','Actiu');
define('COL_4','Ordre');
define('COL_5','Accions');

define('TIPUS','Tipus: ');

define('TEXT_DISPLAY_NUMBER_OF_MENUS','Veient %s menús');
define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s pàgines');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Introdueix o edita els camps que creguis convenients. Els camps marcats amb asterisc són obligatoris.</p>
');

define('TEXT_DELETE','Prem acceptar per eliminar la pàgina.');
define('TEXT_ACTIVE','Prem acceptar per activar la pàgina.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la pàgina.');

define('ERROR_PAGES_CHILDS','La pàgina seleccionada no es pot eliminar. Elimini o mogui primer les pàgines que depenen d\'aquesta.');
?>
