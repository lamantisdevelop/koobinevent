<?php
// Textos
define('TITLE_PAGE','Editar Pedido');

define('TEXT_FORM','
<p>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p> 
');

define('COL_COM_1','Producto');
define('COL_COM_2','Referéncia');
define('COL_COM_3','IVA');
define('COL_COM_4','Precio');
define('COL_COM_5','Total');

define('SUCCESS_ORDER_UPDATED','El pedido ha sido debidamente editado.');
define('WARNING_ORDER_NOT_UPDATED','Error. El pedido no ha podido ser editado.');

define('ERROR_ORDER_DOES_NOT_EXIST','Error. El pedido no existe.');   
?>
