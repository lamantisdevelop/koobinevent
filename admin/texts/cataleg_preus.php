<?php
// Textos
define('TITLE_PAGE','Listado Precios');

define('TEXT_LIST','
<p>Consulta y modifica los precios y disponibilidades de los productos por categorias.</p>
');

define('SEARCH','Buscador: ');

define('COL_1','ID');
define('COL_2','Producto');
define('COL_3','Última Mod.');
define('COL_4','Precio');
define('COL_5','Disponibilidad');
define('COL_6','Peso');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo %s productos');
?>
