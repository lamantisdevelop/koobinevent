<?php
// Textos
define('TITLE_PAGE','Perfil Usuari');

define('TEXT_LIST','
<p>	Permet conèixer el nombre d\’usuaris que estan visualitzant la pàgina en aquest moment.</p>
');
define('TEXT_FORM','
<p>Permet editar les dades de l\'usuari actual.</p>
');

define('TEXT_EXISTENT_USER','Error. Ja existeix un administrador amb l\'e-mail %s.');
define('TEXT_USER_UPDATED','El perfil de l\'administrador %s ha estat actualitzat correctament.');                                 
?>
