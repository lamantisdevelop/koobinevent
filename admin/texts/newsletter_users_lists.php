<?php
// Textos
define('TITLE_PAGE','Llistes Distribució Newsletter');
define('NEW_ITEM','Nova Llista');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('COL_1','Nom');
define('COL_2','Descripció');
define('COL_3','Usuaris');
define('COL_4','Ordre');
define('COL_5','Activa');
define('COL_6','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s grups de configuració');

define('TEXT_ACTIVE','Prem acceptar per activar la llista de distribució.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la llista de distribució.');
define('TEXT_DELETE','Prem acceptar per eliminar la llista de distribució.');                      
?>
