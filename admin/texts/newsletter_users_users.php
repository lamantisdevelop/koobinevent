<?php
// Textos
define('TITLE_PAGE','Usuaris Newsletter');
define('NEW_ITEM','Nou Usuari');
define('SEARCH','Buscador: ');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('COL_1','Nom');
define('COL_2','E-mail');
define('COL_3','Confirmat');
define('COL_4','Llistes');
define('COL_5','Missatges');
define('COL_6','Rebots');
define('COL_7','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s usuaris)');

define('SEARCH_ALL','--- Tots');
define('SEARCH_NAME','Nom');
define('SEARCH_EMAIL','E-mail');
define('SEARCH_DATA','Extradata');

define('ORDER','Ordenar: ');
define('ORDER_ASC','Asc');
define('ORDER_DESC','Desc');

define('ORDER_NAME','Nom');
define('ORDER_EMAIL','E-mail');
define('ORDER_ENTERED','Data Entrada');
define('ORDER_MODIFIED','Última Modificació');

define('TEXT_DELETE','Prem acceptar per eliminar l\'usuari.');
define('TEXT_ACTIVE','Prem acceptar per activar l\'usuari.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar l\'usuari.');

define('TEXT_EXISTENT_USER','Error. Ja existeix un usuari del newsletter amb l\'e-mail %s.');       
?>
