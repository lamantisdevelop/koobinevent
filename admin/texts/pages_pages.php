<?php
// Textos
define('TITLE_PAGE','Pàgines');
define('NEW_ITEM','Nova Pàgina');

define('TEXT_LIST','
<p>Aquí es mostren la totalitat de les pàgines del web.</p>

<p>En aquesta secció vostè podrà accedir a totes les pàgines del web. La informació que pot visualitzar és el nom de la pàgina, el seu ordre dins el menú de navegació, comprovar si estan actives (és a dir, si són accessibles des del menú de navegació de la pàgina). 

<p>Finalment les accions permeten accedir a editar la pàgina o eliminar-la. </p>

</p>
');
define('TEXT_FORM','
<p>Aquí es mostren la totalitat de les pàgines del web.</p>

<p>En aquesta secció vostè podrà accedir a totes les pàgines del web. La informació que pot visualitzar és el nom de la pàgina, el seu ordre dins el menú de navegació, comprovar si estan actives (és a dir, si són accessibles des del menú de navegació de la pàgina). 

<p>Finalment les accions permeten accedir a editar la pàgina o eliminar-la. </p>
');

define('COL_1','Titol');
define('COL_2','Ordre');
define('COL_3','Tipus');
define('COL_4','Actiu');
define('COL_5','Accions');

define('TIPUS','Tipus: ');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s pàgines');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Introdueix o edita els camps que creguis convenients. Els camps marcats amb asterisc són obligatoris.</p>
');

define('TEXT_DELETE','Prem acceptar per eliminar la pàgina.');
define('TEXT_ACTIVE','Prem acceptar per activar la pàgina.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la pàgina.');

define('ERROR_PAGES_CHILDS','La pàgina seleccionada no es pot eliminar. Elimini o mogui primer les pàgines que depenen d\'aquesta.');
?>
