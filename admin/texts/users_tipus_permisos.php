<?php
// Textos
define('TITLE_PAGE','Rols - Tipus Permisos');
define('NEW_ITEM','Nou Tipus de Permís');

define('TEXT_LIST','
<p>Permet veure tots els Tipus de Permisos de la pàgina.</p>
');
define('TEXT_FORM','
<p>Permet modificar les dades del Tipus de Permís seleccionat</p>
');

define('COL_1','Id'); 
define('COL_2','Nom Permis');
define('COL_4','Edició');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s Tipus de Permisos)');

define('TEXT_DELETE','Prem acceptar per eliminar el Tipus de Permís.');
define('TEXT_ACTIVE','Prem acceptar per activar el Tipus de Permís.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar el Tipus de Permís.');

define('TEXT_EXISTENT_ROL','Error. Ja existeix un Tipus de Permís amb l\'ID %s.');
define('TEXT_USER_UPDATED','El Tipus de Permís %s ha estat actualitzat correctament.');
?>
