<?php
// Textos
define('TITLE_PAGE','Productes/Categories');
define('NEW_ITEM','Nou Producte/Categoria');
define('NEW_PRODUCT','Nou Producte');
define('NEW_CATEGORY','Nova Categoria');

define('TEXT_LIST','
<p>Actualitzi els continguts d\'aquesta secció a través del botó editar. Des d\'aquí també pot
desactivar blocs de contingut, eliminar-los deﬁnitivament, ordenar-los i crear-ne de nous.</p>
');
define('TEXT_FORM','
<p>Ha accedit a la fitxa de producte. En aquesta secció vostè podrà editar la descripció dels seus productes en els diferents idiomes
del web a més de col·locar imatges, documents i vídeos entre d’altres accions.</p>
');

define('SEARCH','Buscador: ');
define('CATEGORIES','Categories: ');

define('COL_1','Nom');
define('COL_2','Modificat');
define('COL_3','Status');
define('COL_4','Ordre');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s productes / %s categories');

define('TEXT_ACTIVE','Prem acceptar per activar el producte/categoria.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar el producte/categoria.');
define('TEXT_DELETE','Prem acceptar per eliminar el producte/categoria.');

define('TEXT_RECOMEND','Prem acceptar per posar el producte com a recomanat.');
define('TEXT_DESRECOMEND','Prem acceptar per posar el producte com a no recomanat.');
define('TEXT_DELETE_PRODUCT','Selecciona les categories de les que vols eliminar el producte:');

define('TEXT_MOVE_PRODUCT','Escull una de les següents opcions:');
define('MOVE_PRODUCT','Moure el producte a la categoria seleccionada.');
define('LINK_PRODUCT','Enllaçar el producte a la categoria seleccionada (Mateix producte a diferents categories).');
define('COPY_PRODUCT','Fer una còpia del producte a la categoria seleccionada (Crear un nou producte a partir del seleccionat).');

define('TEXT_MOVE_CATEGORY','Escull una de les següents opcions:');
define('MOVE_CATEGORY','Moure la categoria a la categoria seleccionada (com a subcategoria).');
define('COPY_CATEGORY','Fer una còpia de la categoria a la categoria seleccionada (Crear una nova categoria a partir de la seleccionada).');

define('TEXT_TOP','---------- Top ----------');
define('PLEASE_SELECT_CATEGORY','---------- Selecciona una categoria ----------');
define('PLEASE_SELECT_PRODUCT','---------- Selecciona una subcategoria ------');

define('TEXT_INFO_NO_ITEM','No hi ha cap element. Introdueix algun element.');

define('ERROR_PORTAL_IMAGE_DIRECTORY_DOES_NOT_EXIST','Error. El directori per a imatges no existeix.');
define('ERROR_PORTAL_IMAGE_DIRECTORY_NOT_WRITEABLE','Error. No es pot escriure en el directori per a imatges.');

define('ERROR_PORTAL_VIDEO_DIRECTORY_DOES_NOT_EXIST','Error. El directori per a videos no existeix.');
define('ERROR_PORTAL_VIDEO_DIRECTORY_NOT_WRITEABLE','Error. No es pot escriure en el directori per a videos');

define('ERROR_PORTAL_DOWNLOADS_DIRECTORY_DOES_NOT_EXIST','Error. El directori per a descàrregues no existeix.');
define('ERROR_PORTAL_DOWNLOADS_DIRECTORY_NOT_WRITEABLE','Error. No es pot escriure en el directori per a descàrregues');
?>
