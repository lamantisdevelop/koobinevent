<?php
// Text
define('TITLE_PAGE','Distribuidores');
define('NEW_ITEM','Nuevo distribuidor');

define('TEXT_LIST','
<p>Consulta los distribuidores introducidos en el sitio web.<br/>Las acciones le permiten cambiar su estado, modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita los distribuidores del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('SEARCH','Buscador: ');

define('TAULA','Esta tabla contiene los enlaces introducidos en la base de datos');
define('COL_1','Nombre');
define('COL_2','País');
define('COL_3','Tel');
define('COL_4','Mail');
define('COL_5','Activo');
define('COL_6','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo del %s al %s (de %s distribuidores)');

define('TEXT_ACTIVE','Pulsa aceptar para activar el distribuidor.');
define('TEXT_DESACTIVE','Pulsa aceptar para desactivar el distribuidor.');
define('TEXT_DELETE','Pulsa aceptar para eliminar el distribuidor.');
?>
