<?php
// Exemples:
// RedHat -> 'es_ES'
// FreeBSD -> 'es_ES.ISO_8859-1'
// Windows -> 'sp' o 'Spanish'
@setlocale(LC_TIME, 'es_ES');
@date_default_timezone_set('Europe/Madrid');

define('DATE_FORMAT_SHORT', 'd/m/Y');  // this is used for date()
define('DATE_FORMAT_LONG', 'd/m/Y H:i:s'); // this is used for date()
define('PHP_DATE_TIME_FORMAT', 'd/m/Y H:i:s'); // this is used for date()

$day_text = array('Diumenge','Dilluns','Dimarts','Dimecres','Dijous','Divendres','Dissabte');
$month_text = array('Gener','Febrer','Març','Abril','Maig','Juny','Juliol','Agost','Setembre','Octubre','Novembre','Desembre');

// Charset per pàgines web i emails   
define('LANGUAGE_CONTENT','es');

// Titulars
define('PAGE_TITLE','Eina d\'Administració');
define('MAX_ADMIN_RESULTS', '15'); // Items per llistat admin

// Login
define('TEXT_LOGIN','Per accedir a les funcions de l\'eina d\'administració necessites validar-te introduint la teva direcció de correu electrònic i password.');
define('ERROR_LOGIN','Error. L\'usuari i/o password introduits són incorrectes.');
define('SUCCESS_LOGIN','L\'usuari %s ha accedit correctament a l\'eina d\'administració.');
define('SUCCESS_LOGOFF','La sessió s\'ha tancat satisfactòriament.');

// Icones
define('ICON_EDIT','<img src="image/icones/edit_ico.png" alt="Editar" title="Editar" />');
define('ICON_VIEW','<img src="image/icones/_veure.png" alt="Visualitzar" title="Visualitzar" />');
define('ICON_DELETE','<img src="image/icones/delete_ico.png" alt="Eliminar" title="Eliminar" />');
define('ICON_DOWNLOAD','<img src="image/icon_download.gif" alt="Descarregar" title="Descarregar" />');
define('ICON_RESTORE','<img src="image/icon_restore.gif" alt="Restaurar" title="Restaurar" />');
define('ICON_HISTORY','<img src="image/icon_history.gif" alt="Historial" title="Historial" />');
define('ICON_SEARCH','<img src="image/icon_search.gif" alt="Buscar" title="Buscar" />');
define('ICON_SORT','<img src="image/icon_sort.gif" alt="" title="" />');
define('ICON_NEWS','<img src="image/icon_news.gif" alt="" title="" />');
define('ICON_VISIBLE','<img src="image/icon_visible.gif" alt="Veure" title="Veure" />');
define('ICON_MAIL','<img src="image/icon_mail.gif" alt="" title="" />');
define('ICON_DEFAULT','<img src="image/icon_default.gif" alt="" title="" />');
define('ICON_PIXEL','<img src="image/icon_pixel.gif" alt="" title="" />');
define('ICON_MORE','<img src="image/icon_more.gif" alt="" title="" />');
define('ICON_ERROR','<img src="image/error.gif" alt="" title="" />');
define('ICON_FOLDER','<img src="image/icones/icon_folder.png" alt="" title="" />');
define('ICON_CONFIRMED','<img src="image/icones/public_icon.png" alt="Confirmat" title="Confirmat" />');
define('ICON_CONFIRM','<img src="image/icones/no_public_icon.png" alt="Confirmar" title="Confirmar" />');
define('ICON_ORDER_DOWN','<img src="image/icones/left_ico.png" alt="Baixar Ordre" title="Baixar Ordre" />');
define('ICON_ORDER_UP','<img src="image/icones/right_ico.png" alt="Pujar Ordre" title="Pujar Ordre" />');
define('ICON_ORDER_NO','<img src="image/icones/stop_ico.png" alt="" title="" />');
define('ICON_DEFAULT','<img src="image/icon_default.gif" alt="" title="" />');
define('ICON_CALENDAR','<img src="image/icon_calendar.gif" alt="" title="" />');
define('ICON_SEARCH_ALT','<img src="image/icon_search.gif" alt="" title="" />');
define('ICON_MOVE','<img src="image/icones/icon_move.png" alt="" title="" />');
define('ICON_FACEBOOK','<img src="image/socialnetworks/facebook.png" alt="Publicar a Facebook" title="Publicar a Facebook" />');
define('ICON_FACEBOOK_DISABLED','<img src="image/socialnetworks/facebook_disabled.png" alt="Ja es va publicar aquesta noticia al Facebook" title="Ja es va publicar aquesta noticia al facebook" />');
define('ICON_FACEBOOK_KEY','<img src="image/socialnetworks/facebook_key.png" alt="Loguejar a Facebook per publicar noticia" title="Loguejar a Facebook per publicar noticia" />');


// Split Pages
define('TEXT_PAGE', 'Pàgina');
define('TEXT_RESULT_PAGE', 'de %d');
define('TEXT_PAGE_NUM', 'Pàgina %s de %d');

define('PREVNEXT_BUTTON_PREV', '<img src="image/icon_left.gif" alt="" title="" />');
define('PREVNEXT_BUTTON_NEXT', '<img src="image/icon_right.gif" alt="" title="" />');

define('NO_ITEMS', 'No hi ha cap element.<br/>Introdueix un %s.');
define('NO_ITEMS_F', 'No hi ha cap element.<br/>Introdueix una %s.');

// Formularis
define('TEXT_SELECT','*** No hi ha cap valor seleccionat');
define('TYPE_BELOW', '-----  Escriu Abaix  -----');
define('TEXT_SOME_LIST','--- Qualsevol llista');
define('TEXT_TOP', '--- Top ---');
define('VIEW_ALL', '--- Veure Tots ---');

define('MAKE_DEFAULT','Posar com a predeterminat.');
define('TEXT_CONSULT','A consultar');

define('SELECT_CATEGORY', '--- Selecciona una categoria ---');
define('SELECT_PRODUCT', '--- Selecciona un producto ---');

// Text ERROR! Formularis
define('JS_ERROR', 'Hi ha errors en el seu formulari!\nPer favor, faci les següents correcions:\n\n');

define('JS_NOM', '* El nom sembla incorrecte.\n');
define('JS_MAIL', '* L\'e-mail sembla incorrecte.\n');
define('JS_ID', '* L\'id sembla incorrecte.\n');
define('JS_DESCRIPTION', '* La descripció sembla incorrecte.\n');
define('JS_FILE', '* Seleccioni un arxiu.\n');
define('JS_LIST', '* Seleccioni una llista.\n');
define('JS_TABLE', '* El nom de la taula sembla incorrecte.\n');
define('JS_FOTO', '* No s\ha seleccionat cap imatge.\n');

define('JS_TEXT_CONFIRM_PERDRE_CANVIS', 'ATENCIÓ: estàs sortint d\'aquesta pàgina, els canvis que no hagis desat es perdran. Vols continuar?.\n');

define('JS_THUMB', '* Introudeixi una imatge pel thumbnail.\n');
define('JS_AUDIO', '* Introdueixi un arxiu d\'audio.\n');

define('JS_TITLE', '* El títol sembla incorrecte.\n');
define('JS_TEMPLATE', '* La seva plantilla sembla incorrecte.\n');
define('JS_MESSAGE_SELECT', '* Seleccioni un missatge.\n');
define('JS_TEMPLATE_SELECT', '* Seleccioni una plantilla.\n');

define('JS_SUBJECT', '* L\'assumpte del seu missatge sembla incorrecte.\n');
define('JS_MESSAGE', '* El text del seu missatge sembla incorrecte.\n');

define('JS_DATA_INICI', '* La data d\'inici sembla incorrecte.\n');

define('JS_PASSWORD', '* El password sembla incorrecte.\n');
define('JS_PASSWORD_CONFIRM', '* El password i la seva confirmació han de coincidir.\n');

// Botons
define('BUTTON_SEND','Enviar');
define('BUTTON_BACK','Enrere');
define('BUTTON_EXPORT','Exportar');
define('BUTTON_INIT','Inicialitzar');
define('BUTTON_DELETE','Eliminar');
define('BUTTON_IMPORT','Importar');
define('BUTTON_CONFIRM','Confirmar');
define('BUTTON_SAVE','Guardar');
define('BUTTON_EDIT','Editar');
define('BUTTON_UPDATE','Actualitzar');
define('BUTTON_CANCELL','Cancel.lar');

// Errors
define('ERROR_IMAGE_DIRECTORY_NOT_WRITEABLE','No es pot escriure en el Directori per Imatges.');
define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST','Directori per Imatges no existeix.');

define('ERROR_DOWNLOADS_DIRECTORY_NOT_WRITEABLE','Error. No es pot escriure en el directori per descàrregues');
define('ERROR_DOWNLOADS_DIRECTORY_DOES_NOT_EXIST','Error. El directori per descàrregues no existeix');

//Permisos
define('ERROR_PERMISOS','No tens suficients permisos per a realitzar aquesta acció.');
?>
