<?php
// Textos
define('TITLE_PAGE','Rols');
define('NEW_ITEM','Nou Rol');

define('TEXT_LIST','
<p>Permet veure tots els Rols de la pàgina.</p>
');
define('TEXT_FORM','
<p>Permet modificar les dades del Rol seleccionat.</p>
');

define('TEXT_PERMISOS','
<p><b>Permisos sobre mòduls:</b></p>
');

define('COL_1','Id'); 
define('COL_2','Mòdul');
define('COL_3','Accés');
define('COL_4','Edició');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s Rols)');

define('TEXT_DELETE','Prem acceptar per eliminar el Rol.');
define('TEXT_ACTIVE','Prem acceptar per activar el Rol.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar el Rol.');

define('TEXT_EXISTENT_ROL','Error. Ja existeix un Rol amb l\'ID %s.');
define('TEXT_USER_UPDATED','El Rol %s ha estat actualitzat correctament.');
?>
