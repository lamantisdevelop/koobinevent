<?php
// Textos
define('TITLE_PAGE','Usuarios');
define('NEW_ITEM','Nuevo Usuario');
define('SEARCH','Buscador: ');

define('TEXT_LIST','
<p>Consulta los usuarios introducidos en el sitio web.<br/>Las acciones le permiten cambiar su estado, modificar el contenido, contactar, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita los usuarios del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('COL_1','ID');
define('COL_2','Nombre');
define('COL_3','Ciudad (País)');
define('COL_4','Pedidos');
define('COL_5','Fecha Alta');
define('COL_6','Activo');
define('COL_7','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo del %s al %s (de %s usuarios)');

define('TEXT_ACTIVE','Pulsa aceptar para activar el usuario.');
define('TEXT_DESACTIVE','Pulsa aceptar para desactivar el usuario.');
define('TEXT_DELETE','Pulsa aceptar para eliminar el usuario.');

define('USER_ACTIVE_SUCCESS','El usuario %s ha sido activado satisfactoriamente.');
define('USER_ACTIVE_ERROR','No se ha podido activar satisfactoriamente el usuario %s.');

define('EMAIL_SUBJECT','Aditech-uw - Cuenta de usuario');
define('TEXT_MAIL_ACTIVE','Ha sido validado como usuario de aditech-uw.com:<br/><br/>USUARIO: %s<br/>PASSWORD: %s');
?>
