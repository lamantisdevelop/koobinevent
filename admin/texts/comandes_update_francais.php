<?php
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Mise à jour de votre commande');
define('EMAIL_TEXT_ORDER_NUMBER', 'Numéro de la commande:');
define('EMAIL_TEXT_DATE_ORDERED', 'Date de votre commande:');
define('EMAIL_TEXT_STATUS_UPDATE', 'Le status de votre commande a été mis àjour comme suit.<br/>Nouveau statut: %s<br/><br/>Merci de répondre à ce courrier électronique si vous avez des questions.<br/>');
define('EMAIL_TEXT_COMMENTS_UPDATE', 'Commentaires : ' . "<br/><br/>%s<br/><br/>");
?>
