<?php
// Textos
define('TITLE_PAGE','Enviar Mail');
define('SEARCH','Buscador: ');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas malesuada. Proin ultricies purus non mauris. Nam in turpis at magna volutpat pharetra. Maecenas pretium tempus sapien. Maecenas tortor enim, volutpat quis, mollis vel, feugiat eget, pede. Morbi facilisis diam vel urna. Suspendisse laoreet vulputate nulla. Suspendisse pede. Ut lorem. Integer mollis condimentum orci.</p>
');

define('NOTICE_EMAIL_SENT_TO', 'E-mail enviat a: %s');
define('ERROR_EMAIL_SENT_TO', 'Error. L\'e-mail no ha pogut ser enviat a: %s');
?>
