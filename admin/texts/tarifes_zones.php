<?php
// Textos
define('TITLE_PAGE','Zonas Envio');
define('NEW_ITEM','Nueva Zona');

define('TEXT_LIST','
<p>Consulta las zonas de envio introducidas en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita las zonas de envio del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('SEARCH','Buscador: ');

define('COL_1','Nombre');
define('COL_2','Paises / Zonas');
define('COL_3','Orden');
define('COL_4','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo %s zonas de envio (%s/%s zonas).');

define('TEXT_DELETE','Pulsa aceptar para eliminar la zona.');
?>
