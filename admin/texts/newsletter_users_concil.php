<?php
// Textos
define('TITLE_PAGE','Depurar Usuaris');
define('NEW_ITEM','');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('TEXT_INTRO','<div>Pot netejar la base de dades d\'usuaris del newsletter seguint diversos criteris:</div>');

define('ACTION_1','Reenviar la solicitud de confirmació a aquells usuaris que s\'hagin inscrit i no hagin confirmat la inscripció.');
define('ACTION_2','Confirmar automàticament usuaris inscrits però no confirmats.');
define('ACTION_3','Eliminar usuaris inscrits però no confirmats.');
define('ACTION_4','Desplaçar tots els usuaris que no estiguin en cap llista a alguna llista.');
define('ACTION_5','Eliminar usuaris no inscrits a cap llista.');
define('ACTION_6','Intentar arreglar de forma automàtica el mail d\'usuaris amb mails no vàlids.');
define('ACTION_7','Eliminar usuaris que tinguin un mail no vàlid.');
define('ACTION_8','Eliminar usuaris amb correus rebotats.');

define('NO_RESULTS','No hi ha resultats que satisfacin els criteris de búsqueda.');

define('DATE_INFO_1','<p>Reenviar la solicitud de confirmació a aquells usuaris que s\'hagin inscrit i no hagin confirmat la inscripció.</p>');
define('DATE_INFO_1_B','<p>Per reenviar la sol.licitud de confirmació a aquells usuaris que s\'han donat d\'alta però no han confirmat la seva subscripció editi si fos necessari el text que anirà a l\'inici del correu i faci click a  «Enviar».</p>');

define('PREV_MAIL','Sentim molestar-lo: estam fent neteja de la nostra base de dades i sembla que vosté es va inscriure a alguna de les nostres llistes de distribució, prò no va confirmar la seva inscripció. Voldriem donar-li una nova oportunitat de confirmar la seva inscripció. Més avall trobarà les instruccions per fer-ho.');

define('SUBJECT_INSCRIT', 'Subscripció Newsletter Click!ArtFoto');
define('MESSAGE_INSCRIT', 'Vostè o algú que coneix la seva adreça electrònica està tramitant la seva subscripció a la newsletter de l\'IdibGi. Si està d\'acord en activar la seva subscripció a la newsletter cliqui a l\'enllaç a la pàgina de subscripció. En cas contrari no faci res. <br/><br/>Link pàgina confirmació subscripció:<br/>%s<br/><br/>Link pàgina modificació dades:<br/>%s<br/><br/>');

define('SUCCESS_EMAIL', 'Els mails de confirmació s\'han enviat correctament.');
define('ERROR_EMAIL', 'Error. L\'e-mail no ha pogut ser enviat als següents destinataris: %s');

define('DATE_INFO_2','<p>Confirmar automàticament usuaris inscrits però no confirmats.</p>');

define('DATE_INFO_3','<p>Eliminar usuaris inscrits però no confirmats.</p>');

define('DATE_INFO_4','<p>Desplaçar tots els usuaris que no estiguin en cap llista a alguna llista.</p>');
define('DATE_INFO_4_B','<p>Desplaçar tots els usuaris que no estiguin en cap llista a alguna llista.</p>');

define('DATE_INFO_5','<p>Eliminar usuaris no inscrits a cap llista.</p>');

define('DATE_INFO_6','<p>Intentar arreglar de forma automàtica el mail d\'usuaris amb mails no vàlids.</p>');

define('DATE_INFO_7','<p>Eliminar usuaris que tinguin un mail no vàlid.</p>');

define('DATE_INFO_8','<p>Eliminar usuaris amb correus rebotats.</p>');
define('DATE_INFO_8_B','<p>Eliminar usuaris amb més de ... correus rebotats.</p>');

define('ERROR_1','No confirmat');
define('ERROR_2','No confirmat');
define('ERROR_3','0 Llistes');
define('ERROR_4','0 Llistes');
define('ERROR_5','Mail no vàlid');
define('ERROR_6','Mail no vàlid');
define('ERROR_7','%s Rebots');
?>
