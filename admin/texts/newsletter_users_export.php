<?php
// Textos
define('TITLE_PAGE','Exportar Usuaris Newsletter');
define('NEW_ITEM','');

define('TEXT_FORM','  
<p>Si fa click al botó «Exportar» s\'obrirà un quadre de diàleg per descarregar un arxiu. Aquest és un fitxer de text de valors delimitats per tabulació que conté les dades que seleccioni per exportar.</p>
<p>Aquest arxiu es pot llegir amb la majoria dels programes de fulla de càlcul.</p>
');

define('ERROR_EXPORT_FILE','Hi ha hagut un error a l\'exportar l\'arxiu. No hi ha cap resultat que satisfaci els criteris d\'exportació.');
?>
