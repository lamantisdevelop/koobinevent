<?php
define('TITLE_PAGE','Estadístiques');

define('TEXT_LIST','
<p>Consulta les estadístiques de visites del lloc web:</p>
');  

define('TAULA','Aquesta taula conté els usuaris que es troben online.');
define('COL_1','Ultim');
define('COL_2','Primer');
define('COL_3','Usuari');
define('COL_4','IP');
define('COL_5','Navegador/Robot');
define('COL_6','Pais');
define('COL_7','Ultima URL');

define('NO_USERS_ONLINE','No hi ha usuaris online.');
define('TEXT_DISPLAY_NUMBER_OF_ONLINE', 'Hora: %s - Actualment hi ha %d ususaris online');

define('TEXT_INIT','Prem confirmar per inicialitzar.S\'eliminaran tots els registres de l\'historial d\'usuaris');
?>
