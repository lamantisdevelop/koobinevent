<?php
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Statusänderung Ihrer Bestellung');
define('EMAIL_TEXT_ORDER_NUMBER', 'Bestell-Nr.:');
define('EMAIL_TEXT_DATE_ORDERED', 'Bestell-Datum:');
define('EMAIL_TEXT_STATUS_UPDATE', 'Ihre Bestellung ist wie folgt aktualisiert worden:.<br/><br/>Neuer Stand: %s<br/><br/>Wenn Sie Fragen haben, bitte antworten Sie auf diesen e-mail.<br/>');
define('EMAIL_TEXT_COMMENTS_UPDATE', 'Anmerkungen und Kommentare zu Ihrer Bestellung:<br/><br/>%s<br/><br/>');
?>
