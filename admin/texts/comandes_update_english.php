<?php
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Order Update');
define('EMAIL_TEXT_ORDER_NUMBER', 'Order Number:');
define('EMAIL_TEXT_DATE_ORDERED', 'Date of order:');
define('EMAIL_TEXT_STATUS_UPDATE', 'Your order has been updated as follows.<br/><br/>New state: %s<br/><br/>Please answer to this e-mail if you have any question.<br/>');
define('EMAIL_TEXT_COMMENTS_UPDATE', 'The comments for your order are:<br/>%s<br/><br/>');
?>
