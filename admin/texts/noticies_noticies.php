<?php
// Text
define('TITLE_PAGE','Notícies');
define('NEW_ITEM','Nova notícia');

define('TEXT_LIST','
<p>En  el llistat apareix el titular de la notícia, la seva data de publicació, el seu estat (si està activa, és a dir visible, o inactiva) i les accions que permeten accedir a la fitxa de la notícia per editar-la o eliminar-la. 
</p>
');
define('TEXT_FORM','
<p>Has accedit a la fitxa de notícia. En aquesta secció es pot editar el text de les notícies en els diferents idiomes del web, col·locar imatges, documents i vídeos entre d\'altres accions. </p>
');

define('SEARCH','Buscador: ');

define('TAULA','Aquesta taula conté les notícies introduides a la base de dades');
define('COL_1','Titular');
define('COL_2','Data');
define('COL_3','Activa');
define('COL_4','Portada');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s notícies)');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Per introduir o editar una notícia tingui en compte que:</p>
<ul>
<li>Els camps marcats amb asterisc són obligatoris.</li>
<li>Es poden introduir imatges i/o enllaços a fitxers externs en el cos de la notícia utilitzant l\'administrador d\'arxius habilitat al respecte (màxim 2MB).</li>
<li>Es poden afegir tantes imatges com es cregui convenient a la galeria (màxim 2MB). La imatge s\'ajustarà al tamany màxim permès: 600x450px.</li>
</ul>
');

define('TEXT_ACTIVE','Prem acceptar per activar la notícia.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la notícia.');
define('TEXT_DELETE','Prem acceptar per eliminar la notícia.');

define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST','Error. El directori per imatges no existeix');
define('ERROR_IMAGE_DIRECTORY_IS_NOT_WRITABLE','Error. No es pot escriure en el directori per imatges');
?>
