<?php
// Text
define('TITLE_PAGE','Fabricantes');
define('NEW_ITEM','Nuevo fabricante');

define('TEXT_LIST','
<p>Consulta los fabricantes introducidos en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita los fabricantes del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('SEARCH','Buscador: ');

define('TAULA','Esta tabla contiene los fabricantes introducidos en la base de datos');
define('COL_1','Nombre');
define('COL_2','Productos');
define('COL_3','Fecha Alta');
define('COL_4','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo del %s al %s (de %s fabricantes)');

define('TEXT_USA_CONVERSION','Aplicar conversión precios USA a los productos de este fabricante.'); 
define('TEXT_EUR_CONVERSION','Aplicar conversión precios EURO a los productos de este fabricante.'); 

define('TEXT_ACTIVE','Pulsa aceptar para activar el fabricante.');
define('TEXT_DESACTIVE','Pulsa aceptar para desactivar el fabricante.');
define('TEXT_DELETE','Pulsa aceptar para eliminar el fabricante.');

define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST','Error. El directorio por imágenes no existe.');
define('ERROR_IMAGE_DIRECTORY_IS_NOT_WRITABLE','Error. No se puede escribir en el directorio por imágenes.');
?>
