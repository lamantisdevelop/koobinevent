<?php
// Textos
define('TITLE_PAGE','Grups Configuració');
define('NEW_ITEM','Nou Grup');

define('TEXT_LIST','
<p>Els valors de configuració defineixen diversos aspectes tècnics del seu web. Vostè no podrà editar cap mena de contingut des d’aquesta secció.
Per tant, li recomanem que no manipuli aquest fitxers a no ser que disposi de coneixements tècnics específics.</p>
');
define('TEXT_FORM','
<p>Els valors de configuració defineixen diversos aspectes tècnics del seu web. Vostè no podrà editar cap mena de contingut des d’aquesta secció.
Per tant, li recomanem que no manipuli aquest fitxers a no ser que disposi de coneixements tècnics específics.</p>
');

define('COL_1','Nom');
define('COL_2','Descripció');
define('COL_3','Ordre');
define('COL_4','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s grups de configuració');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Introdueix o edita els camps que creguis convenients. Els camps marcats amb asterisc són obligatoris.</p>
');

define('TEXT_DELETE','Prem acceptar per eliminar el grup de configuració.');
?>
