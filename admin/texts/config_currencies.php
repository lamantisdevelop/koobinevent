<?php
// Textos
define('TITLE_PAGE','Monedas');
define('NEW_ITEM','Nueva Moneda');

define('TEXT_LIST','
<p>Consulta las monedas introducidas en el sitio web.<br/>Las acciones le permiten cambiar su estado, modificar el contenido, eliminar un registro existente o introducir uno de nuevo.<br/>Pulsa "Actualizar Valores" para actualizar los valores de cambio respecto a la moneda por defecto de forma automática.</p>
');
define('TEXT_FORM','
<p>Introduce o edita las monedas del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('ACT_VALUE','Actualizar Valores');

define('COL_1','Nombre');
define('COL_2','Codigo');
define('COL_3','Valor');
define('COL_4','Actualizado');
define('COL_5','Activo');
define('COL_6','Orden');
define('COL_7','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo %s monedas');

define('TEXT_ACTIVE','Pulsa aceptar para activar la moneda.');
define('TEXT_DESACTIVE','Pulsa aceptar para desactivar la moneda.');
define('TEXT_DELETE','Pulsa aceptar para eliminar aquella moneda.');

define('TEXT_DEFAULT_CURRENCY', 'Establecer una moneda como predeterminada requiere actualizar los cambios de moneda.');
define('ERROR_REMOVE_DEFAULT', 'Error. No se puede eliminar la moneda predeterminada, seleccione otra moneda como predeterminada.');

define('TEXT_INFO_CURRENCY_UPDATED', 'El valor de %s (%s) se ha actualizado via %s.');
define('ERROR_CURRENCY_INVALID', 'Error: El valor de %s (%s) no ha sido actualizado via %s.');
?>
