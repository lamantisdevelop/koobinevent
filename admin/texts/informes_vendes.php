<?php
// Text
define('TITLE_PAGE','Informes Ventas');

define('TEXT_LIST','
<p>Selecciona una de las diferentes opciones de visualización disponibles.
</p>
');

define('SEARCH','Visualizar por: ');

define('TAULA','Esta tabla contiene las noticias introducidas en la base de datos');
define('COL_1','Intervalo');
define('COL_2','Pedidos');
define('COL_3','Productos');
define('COL_4','Ingresos');
define('COL_5','Envio');                                                                              
?>
