<?php
// Textos
define('TITLE_PAGE','Búsqueda alternativa');
define('NEW_ITEM','Nuevo Valor');

define('TEXT_LIST','
<p>Consulta las alternativas de búsqueda introducidas en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita las alternativas de búsqueda del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('SEARCH','Buscador: ');

define('COL_1','Item');
define('COL_2','Valor');
define('COL_3','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo del %s al %s (de %s valores de búsqueda alternativos)');

define('TEXT_DELETE','Pulsa aceptar para eliminar el valor de búsqueda alternativo.');
?>
