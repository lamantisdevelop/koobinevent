<?php
// Text
define('TITLE_PAGE','Informes Estoc');

define('TEXT_LIST','
<p>Selecciona una de las diferentes opciones de visualización disponibles.
</p>
');

define('SEARCH','Visualizar por: ');

define('COL_1','Id');
define('COL_2','Producto');
define('COL_3','Estoc');
define('COL_4','Estoc mínimo');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS', 'Viendo del <b>%d</b> al <b>%d</b> (de <b>%d</b> productos)');
?>
