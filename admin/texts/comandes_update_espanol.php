<?php
// Textos
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Actualización del Pedido');
define('EMAIL_TEXT_ORDER_NUMBER', 'Número de Pedido:');
define('EMAIL_TEXT_DATE_ORDERED', 'Fecha del Pedido:');
define('EMAIL_TEXT_STATUS_UPDATE', 'Su pedido ha sido actualizado al siguiente estado.<br/><br/>Nuevo estado: %s<br/><br/>Por favor responda a este email si tiene alguna pregunta que hacer.<br/>');
define('EMAIL_TEXT_COMMENTS_UPDATE', 'Los comentarios sobre su pedido son:<br/>%s<br/><br/>');
?>
