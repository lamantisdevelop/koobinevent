<?php
// Text
define('TITLE_PAGE','Tipo de Impuestos');
define('NEW_ITEM','Nuevo Tipo de Impuesto');

define('TEXT_LIST','
<p>Consulta los tipos de impuestos introducidos en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita los tipos de impuestos del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo %s tipos de impuestos');

define('COL_1','Nombre');
define('COL_2','Tasa');
define('COL_3','Orden');
define('COL_4','Acciones');

define('TEXT_DELETE','Pulsa aceptar para eliminar el tipo de impuesto');
?>
