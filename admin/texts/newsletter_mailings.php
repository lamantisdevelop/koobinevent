<?php
// Textos
define('TITLE_PAGE','Enviaments Newsletter');
define('NEW_ITEM','Nou Enviament');
define('VIEW','Visualitzar: ');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('COL_1','Nom');
define('COL_2','Data Enviament');
define('COL_3','Errors');
define('COL_4','Visualitzacions');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s enviaments)');

define('TEXT_DELETE','Prem acceptar per eliminar l\'enviament.');
define('TEXT_ACTIVE','Prem acceptar per activar l\'enviament.');

define('TEXT_MESSAGE','Selecciona un missatge i una plantilla per realitzar un nou enviament:');

define('TEXT_FORMAT','Selecciona un format per realitzar l\'enviament:');
define('FORMAT_HTML','HTML');
define('FORMAT_TEXT','Text Pla');
define('FORMAT_BOTH','HTML + Text Pla');

define('TEXT_LISTS','Selecciona les llistes de distribució a les que s\'ha realitzar l\'enviament ( <a href="javascript:void(0);" class="select_lists">TOTES</a> ):');

define('TEXT_CRITERIA','Selecciona si cal un criteri per realitzar l\'enviament:');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas malesuada. Proin ultricies purus non mauris. Nam in turpis at magna volutpat pharetra. Maecenas pretium tempus sapien. Maecenas tortor enim, volutpat quis, mollis vel, feugiat eget, pede. Morbi facilisis diam vel urna. Suspendisse laoreet vulputate nulla. Suspendisse pede. Ut lorem. Integer mollis condimentum orci.</p>
');

define('SUCCESS_NEWSLETTER', 'El newsletter s\'ha enviat correctament a %s usuaris.');
define('ERROR_NEWSLETTER', 'S\'han produit errors en l\'enviament. El newsletter no ha pogut ser enviat a %s usuaris.');

define('NO_CRITERIA','No s\'ha creat cap filtre.');
define('NO_MAILING_ERRORS','No hi ha errors.<br/>El newsletter s\'ha enviat correctament a tots els usuaris seleccionats.');
?>
