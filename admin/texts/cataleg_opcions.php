<?php
// Textos
define('TITLE_PAGE','Opciones');
define('NEW_CATEGORY','Nuevo Grupo');
define('NEW_PRODUCT','Nueva Opción');

define('TEXT_LIST','
<p>Consulta las opciones para productos introducidos en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita las opciones para productos del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('COL_1','Nombre');
define('COL_2','Orden');
define('COL_3','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_GROUPS','Viendo %s grupos');
define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo %s opciones');

define('TEXT_DELETE','Pulsa aceptar para eliminar la opción/grupo.');

define('TEXT_MOVE_CATEGORY','Escoge una de las siguientes opciones:');
define('MOVE_CATEGORY','Mover la opción al grupo seleccionado.');
?>
