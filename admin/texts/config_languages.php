<?php
// Textos
define('TITLE_PAGE','Idiomes');
define('NEW_ITEM','Nou Idioma');

define('TEXT_LIST','
<p>Permet activar i desactivar els idiomes del web.</p>
');
define('TEXT_FORM','
<p>Permet activar i desactivar els idiomes del web.</p>
');

define('COL_1','Item');
define('COL_2','Codi');
define('COL_3','Active');
define('COL_4','Ordre');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s idiomes');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Especifiqui els valors adequats per a la configuració de l\'idioma.</p>
');

define('TEXT_ACTIVE','Prem acceptar per activar l\'idioma.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar l\'idioma.');
define('TEXT_DELETE','Prem acceptar per eliminar l\'idioma.');

define('ERROR_REMOVE_DEFAULT','Error. No es pot eliminar l\'idioma predeterminat, seleccioni un altre idioma per defecte. ')
?>
