<?php
// Textos
define('TITLE_PAGE','Activitats');
define('NEW_ITEM','Nova Activitat');

define('TEXT_LIST','
<p>Actualitzi els continguts d\'aquesta secció a través del botó editar. Des d\'aquí també pot desactivar blocs de contingut, eliminar-los definitivament i crear-ne de nous.</p>
');
define('TEXT_FORM','
<p>Ha accedit a la ﬁtxa d\'una activitat. En aquesta secció vostè podrà editar la descripció de
les seves activitats en els diferents idiomes del web. Crei un text nou o modiﬁqui un text
existent. Introdueixi el lloc on es celebra l\'activitat, el dia i l\'hora. L\'administrador també li
permetrà afegir diverses imatges si ho considera necessari.</p>
');

define('SEARCH','Buscador: ');

define('COL_1','Dia Inici');
define('COL_2','Dia Final');
define('COL_3','Hora');
define('COL_4','Activitat');
define('COL_5','Actiu');
define('COL_6','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s activitats)');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas malesuada. Proin ultricies purus non mauris. Nam in turpis at magna volutpat pharetra. Maecenas pretium tempus sapien. Maecenas tortor enim, volutpat quis, mollis vel, feugiat eget, pede. Morbi facilisis diam vel urna. Suspendisse laoreet vulputate nulla. Suspendisse pede. Ut lorem. Integer mollis condimentum orci.</p>
');

define('TEXT_ACTIVE','Prem acceptar per activar l\'activitat.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar l\'activitat.');
define('TEXT_DELETE','Prem acceptar per eliminar l\'activitat.');
?>
