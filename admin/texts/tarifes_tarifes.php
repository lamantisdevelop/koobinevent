<?php
// Textos
define('TITLE_PAGE','Tarifas Envio');
define('NEW_ITEM','Nueva Tarifa');

define('TEXT_LIST','
<p>Consulta las tarifas de envio introducidas en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita las tarifas de envio del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('SEARCH','Buscador: ');

define('COL_1','Nombre');
define('COL_2','Intervalo Peso');
define('COL_3','Precio');
define('COL_4','Última Modificación');
define('COL_5','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_GROUPS','Viendo %s tipos de tarifas.');
define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo %s tarifas de envio.');

define('TEXT_DELETE','Pulsa aceptar para eliminar la zona.');
?>
