<?php
// Textos
define('TITLE_PAGE','Plantilles Newsletter');
define('NEW_ITEM','Nova Plantilla');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Una plantilla és una pàgina HTML amb estils CSS incrustats en la que està inclosa un marcador [CONTENT]. En aquest lloc s\'insertarà el missatge.</p>
<p>A més de [CONTENT] pot incloure altres marcadors opcionals que seràn substituits pels valors de configuració corresponents:<br/>[VISUALIZE], [HEADER], [INTRODUCE], [FOOTER], [LEGAL],...</p>
');

define('COL_1','Nom');
define('COL_2','Data');
define('COL_3','Ordre');
define('COL_4','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s administradors)');

define('TEXT_DELETE','Prem acceptar per eliminar la plantilla.');
define('TEXT_ACTIVE','Prem acceptar per activar la plantilla.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la plantilla.');

define('ERROR_REMOVE_DEFAULT_TEMPLATE','Error. No es pot eliminar la plantilla per defecte. Seleccioni una altra plantilla per defecte.');
?>
