<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */

define('TITLE_PAGE','Gestión de Estoc');
define('TEXT_LIST','Selecciona las familias a las que se actualizará el estoc de sus productos:');

/*FORMULARI*/
define('BUTTON_UPDATE_PRODUCTS','Actualizar Productos');


/*MISSATGES*/
define('ERROR_FAMILIES_BUIDES','Atenció, has de seleccionar alguna família i omplir algun dels camps d\'estoc.');
define('ERROR_CONSULTA','ERROR: no s\'ha pogut actualitzar els productes.');
define('MSG_ESTOC_ACTUALITZAT','S\'han actualitzat els productes de les categories seleccionades.');

?>

