<?php
// Textos
define('TITLE_PAGE','Rebots Newsletter');
define('NEW_ITEM','Nou Rebot');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');   

define('COL_1','Data');
define('COL_2','Header');
define('COL_3','Dades');
define('COL_4','Status');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s rebots)');

define('TEXT_DELETE','Prem acceptar per eliminar el rebot.');
define('TEXT_PROCESS','Prem acceptar per processar el rebot.');                                    
?>
