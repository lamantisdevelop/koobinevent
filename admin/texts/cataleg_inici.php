<?php

/* * ****************************
 * Created by Xavi Pujolras
 * La Mantis Disseny i Internet
 * www.mantis.cat
 * **************************** */

define('TITLE_PAGE','Gestió de la pàgina d\'Inici');
define('TEXT_LIST','Selecciona els productes que vols destacar a la pàgina d\'inici per a cada categoria i idioma:');

/*FORMULARI*/
define('BUTTON_UPDATE_PRODUCTS','Actualitzar pàgina d\'inici');


/*MISSATGES*/
define('ERROR_CONSULTA','ERROR: no s\'ha pogut actualitzar els espectacles.');
define('MSG_ESTOC_ACTUALITZAT','S\'ha actualitzat la pàgina d\'inici.');

?>

