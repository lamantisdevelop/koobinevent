<?php
// Textos
define('TITLE_PAGE','Ofertas');
define('NEW_ITEM','Nueva Oferta');

define('TEXT_LIST','
<p>Consulta los productos en oferta introducidos en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita los productos en oferta del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.<br/>Puede introducir un parcetaje de reducción del precio del producto (p.e. 20%).</p>
');

define('SEARCH','Buscador: ');

define('COL_1','Producto');
define('COL_2','Stat.EU');
define('COL_3','Precio EU');
define('COL_4','Stat.USA');
define('COL_5','Precio USA');
define('COL_6','Fecha Fin');
define('COL_7','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo de la %s a la %s (de %s ofertas)');

define('TEXT_DELETE','Pulsa aceptar para eliminar la oferta.');
define('TEXT_ACTIVE_SALE','Pulsa aceptar para activar la oferta.');
define('TEXT_DESACTIVE_SALE','Pulsa aceptar para desactivar la oferta.');

define('TEXT_INFO_NO_ITEM','No hay ningún elemento. Introduce algún elemento.');
?>
