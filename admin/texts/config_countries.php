<?php
// Textos
define('TITLE_PAGE','Países');
define('NEW_ITEM','Nuevo País');

define('TEXT_LIST','
<p>Consulta los países introducidos en el sitio web.<br/>Las acciones le permiten modificar el contenido, eliminar un registro existente o introducir uno de nuevo.</p>
');
define('TEXT_FORM','
<p>Introduce o edita los países del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('SEARCH','Buscador: ');

define('COL_1','País');
define('COL_2','ISO Code 2');
define('COL_3','ISO Code 3');
define('COL_4','Acciones');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veiendo del %s al %s (de %s países)');

define('TEXT_DELETE','Pulsa aceptar para eliminar el país.');
?>
