<?php
// Text
define('TITLE_PAGE','Definir Texts Web');
define('NEW_ITEM','Nou agent');

define('TEXT_LIST','
<p>	Permet editar la totalitat dels textos del web.</p>
');
define('TEXT_FORM','
<p>	Permet editar la totalitat dels textos del web.</p>
');

define('SEARCH','Selecciona un idioma: ');        

define('TAULA','Aquesta taula conté els agents introduits a la base de dades');
define('COL_1','Nom');
define('COL_2','Tamany');
define('COL_3','Última Modificació');
define('COL_4','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s arxius');
define('TEXT_NO_ITEMS','No existeixen els fitxers de configuració per l\'idioma.');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Introdueix o edita els camps que creguis convenients. Els camps marcats amb asterisc són obligatoris.</p>
');

define('TEXT_DELETE','Prem acceptar per eliminar l\'agent.');


define('ERROR_FILE_NOT_WRITABLE','Error. No es pot editar l\'arxiu. Canvii els permisos per poder-lo editar');
define('ERROR_FILE_NOT_EXISTS','Error. No s\'ha pogut trobar l\'arxiu.');
?>
