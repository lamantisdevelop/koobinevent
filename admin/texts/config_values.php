<?php
// Textos
define('TITLE_PAGE','Valors Configuració:');
define('NEW_ITEM','Nou Valor');

define('TEXT_LIST','
<p>Els valors de conﬁguració deﬁneixen diversos aspectes tècnics del seu web. Vostè no
podrà editar cap mena de contingut des d\'aquesta secció. Per tant, li recomanem que no
manipuli aquest ﬁtxers a no ser que disposi de coneixements tècnics especíﬁcs.</p>
');
define('TEXT_FORM','
<p>Els valors de conﬁguració deﬁneixen diversos aspectes tècnics del seu web. Vostè no
podrà editar cap mena de contingut des d\'aquesta secció. Per tant, li recomanem que no
manipuli aquest ﬁtxers a no ser que disposi de coneixements tècnics especíﬁcs.</p>
');

define('SEARCH','Grup: ');

define('COL_1','Item');
define('COL_2','Valor');
define('COL_3','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s valors de configuració)');

define('TEXT_INFO','
<h2>INFO</h2>
<p>Especifiqui el valor adequat per a la configuració del seu portal.</p>
');

define('TEXT_DELETE','Prem acceptar per eliminar el valor de configuració.');
?>
