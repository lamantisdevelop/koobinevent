<?php
// Textos
define('TITLE_PAGE','Acceso TPV Virtual');
define('NEW_ITEM','Nuevo Pago');

define('TEXT_LIST','
<p>Consulta los pagos realizados a través de la pasarela de pago (TPV Virtual) o realiza un nuevo pago.</p>
');
define('TEXT_FORM','
<p>Introduce un importe y continua hasta finalizar el pago.</p>
');

define('COL_1','Fecha');
define('COL_2','Importe');
define('COL_3','Cliente');
define('COL_4','Respuesta');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo del %s al %s (de %s accesos al tpv virtual)');
?>
