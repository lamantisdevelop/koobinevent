<?php
// Textos
define('TITLE_PAGE','Enviaments Newsletter');
define('NEW_ITEM','Nou Enviament');

define('TEXT_LIST','
<p>List. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');
define('TEXT_FORM','
<p>Form. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui. Suspendisse dictum libero in urna. Curabitur neque est, molestie ut, rutrum sed, fermentum vel, ante. Pellentesque vitae est in dui pellentesque tincidunt. Ut sodales. Quisque ut elit. Phasellus iaculis, urna a rhoncus porta, sem mi auctor eros, aliquet feugiat sapien magna at felis. Vivamus justo lacus, facilisis sit amet, semper et, volutpat non, tortor. Mauris non massa. Duis semper eros ac urna aliquam dignissim.</p>
');

define('COL_1','Id');
define('COL_2','Nom');
define('COL_3','Rol');
define('COL_4','Logons');
define('COL_5','Ultim Logon');
define('COL_6','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s administradors)');

define('TEXT_DELETE','Prem acceptar per eliminar la pàgina.');
define('TEXT_ACTIVE','Prem acceptar per activar la pàgina.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la pàgina.');

define('TEXT_EXISTENT_USER','Error. Ja existeix un administrador amb l\'e-mail %s.');
define('TEXT_USER_UPDATED','El perfil de l\'administrador %s ha estat actualitzat correctament.');
?>
