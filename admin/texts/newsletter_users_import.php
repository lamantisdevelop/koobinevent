<?php
// Textos
define('TITLE_PAGE','Importar Usuaris Newsletter');
define('NEW_ITEM','');

define('TEXT_FORM','
<p>Aquesta pàgina el permet afegir múltiples usuaris al seu newsletter. Els diferents camps i atributs han d\'estar identificats en la primera línea de l\'arxiu. No seràn creats si no existeixen. Asseguri\'s que el nom de les columnes és el correcte, per exemple la columna anomenada "email" i no alguna cosa com "e-mail" o "Email Address". Les majúscules no són importants.</p>
<p>Camps: %s</p>
<p>Atributs: %s</p>
<p><b>Avís:</b> els arxius necessiten ser en format .xls o .csv. En cas de ser .csv s\'ha d\'utilitzar ";" com a separador entre camps (Per defecte en excel).</p>
');

define('SUCCESS_FILE_IMPORTED','Arxiu importat satisfactòriament.');
define('ERROR_FILE_NOT_VALID','Error. L\'arxiu té un format no vàlid.');

define('ERROR_UPLOAD_DIRECTORY_NOT_WRITEABLE','No es pot escriure en el directori temporal per uploads');
define('ERROR_UPLOAD_DIRECTORY_DOES_NOT_EXIST','Directori temporal per uploads no existeix.');
?>
