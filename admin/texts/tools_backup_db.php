<?php
// Textos
define('TITLE_PAGE','Còpies de Seguretat de la BBDD');
define('NEW_ITEM','Nova Còpia de Seguretat');

define('TEXT_LIST','
<p>Permet realitzar còpies de seguretat de la base de dades on s\'emmagatzema tota la informació del web.</p>
');  

define('UPLOAD_BACKUP','Restaurar local: ');

define('COL_1','Nom');
define('COL_2','Data');
define('COL_3','Tamany');
define('COL_4','Accions');

define('TEXT_BACKUP_DIRECTORY','Directori per còpies de seguretat: ');

define('TEXT_DELETE','Prem acceptar per eliminar la còpia de seguretat.');
define('TEXT_RESTORE','Prem acceptar per restaurar la còpia de seguretat.');

define('SUCCESS_BACKUP_DELETED','Còpia de Seguretat borrada amb èxit.');
define('SUCCESS_DATABASE_SAVED','Còpia de Seguretat creada amb èxit.');
define('SUCCESS_DATABASE_RESTORED','Còpia de Seguretat restaurada amb èxit.');    

define('ERROR_BACKUP_DIRECTORY_NOT_WRITEABLE','No es pot escriure en el Directori per Còpies de Seguretat.');
define('ERROR_BACKUP_DIRECTORY_DOES_NOT_EXIST','Directori per Còpies de Seguretat no existeix.');
define('ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE','La descàrrega no es pot realitzar, el link incorrecte');
define('ERROR_FILE_NOT_REMOVEABLE','No es pot eliminar l\'arxiu. Canvii els permisos per poder-lo eliminar.');
define('ERROR_DATABASE_NOT_RESTORED','Error. La còpia de Seguretat no ha pogut ser restaurada amb èxit.');
?>
