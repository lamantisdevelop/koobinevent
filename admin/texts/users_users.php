<?php
// Textos
define('TITLE_PAGE','Usuaris');
define('NEW_ITEM','Nou Usuari');

define('TEXT_LIST','
<p>Permet conèixer el nombre d\'usuaris de la la pàgina.</p>
');
define('TEXT_FORM','
<p>Permet modificar les dades de l\'usuari seleccionat.</p>
');
define('TEXT_FORBIDDEN','<div class="forbidden">
 <p ><b>NO TENS PERMISOS PER A VEURE AQUEST MÒDUL.</b> </p>
 
 <p>Posa\'t amb contacte amb l\'administrador, </p>
 
 <p>Gràcies</p>
</div>');
define('COL_1','Id');
define('COL_2','Nom');
define('COL_3','Rol');
define('COL_4','Logons');
define('COL_5','Ultim Logon');
define('COL_6','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s al %s (de %s usuaris)');

define('TEXT_DELETE','Prem acceptar per eliminar la pàgina.');
define('TEXT_ACTIVE','Prem acceptar per activar la pàgina.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la pàgina.');

define('TEXT_EXISTENT_USER','Error. Ja existeix un usuari amb l\'e-mail %s.');
define('TEXT_USER_UPDATED','El perfil de l\'usuari %s ha estat actualitzat correctament.');
?>
