<?php
// Textos
define('TITLE_PAGE','Pedidos');
define('SEARCH','Buscador: ');

define('TEXT_LIST','
<p>Consulta los pedidos introducidos en el sitio web.<br/>Las acciones le permiten actualizar el status, modificar el contenido, descargar en formato pdf o eliminar un registro existente.</p>
');
define('TEXT_FORM','
<p>Actualiza o modifica los pedidos del sitio web.<br/>Realiza los cambios necesarios. Los campos marcados con asterisco son obligatorios.</p>
');

define('COL_1','ID');
define('COL_2','Cliente (ID)');
define('COL_3','Total');
define('COL_4','Status');
define('COL_5','Fecha');
define('COL_6','Envio');
define('COL_7','Acciones');

define('COL_COM_1','Producto');
define('COL_COM_2','Referéncia');
define('COL_COM_3','IVA');
define('COL_COM_4','Precio');
define('COL_COM_5','Total');

define('COL_STAT_1','Valor nuevo');
define('COL_STAT_2','Valor antiguo');
define('COL_STAT_3','Data actualización');
define('COL_STAT_4','Notificación');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Viendo del %s al %s (de %s pedidos)');

define('TEXT_DELETE','Pulsa aceptar para eliminar el pedido');
define('ERROR_ORDER_DOES_NOT_EXIST','Error. El pedido que intenta visualizar no existe o ha sido eliminada.');

define('NOTICE_EMAIL_SENT_TO','Se ha enviado un mail de notificación a %s');
define('SUCCESS_ORDER_UPDATED','El pedido ha sido correctamente actualitzado.');
define('WARNING_ORDER_NOT_UPDATED','Error. El pedido no ha podido ser actualitzado.');
?>
