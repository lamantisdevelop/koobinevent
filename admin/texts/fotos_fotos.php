<?php
// Textos
define('TITLE_PAGE','Fotos');
define('NEW_ITEM','Nova Foto');
define('NEW_CATEGORY','Nova Galeria');

define('SEARCH','Tipus: ');

define('COL_1','Nom');
define('COL_2','Imatge'); 
define('COL_2a','ID'); 
define('COL_3','Actiu');
define('COL_4','Ordre');
define('COL_5','Accions');

define('TEXT_DISPLAY_NUMBER_OF_ITEMS','Veient %s fotos');
define('TEXT_DISPLAY_NUMBER_OF_GROUPS','Veient %s albums');

define('TEXT_LIST','Aqui es mostra el llistat de galeries i fotos');
define('TEXT_FORM','Entra les dades de la foto / galeria');
define('TEXT_ACTIVE','Prem acceptar per activar la foto.');
define('TEXT_DESACTIVE','Prem acceptar per desactivar la foto.');
define('TEXT_DELETE','Prem acceptar per eliminar la foto.');        

define('ERROR_NOM_BUIT','Has d\'entrar el nom de la foto.');  
?>
