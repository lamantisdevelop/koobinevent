<?php
/* Forcem que no guardi en cach - Problema Explorer */
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT"); // Date in the past

// General
require_once('includes/application_top.php');

require_once(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

include(DIR_WS_CLASSES . 'order.php');

// Extraiem variables enviades
if (!empty($_POST)) {
  extract($_POST);
}

// Accions
if ($_GET['action']) {
  switch ($_GET['action']) {
    case 'save':
      // Get Order Info
      $oID = tep_db_prepare_input($_GET['oID']);
      $order = new order($oID);

      $AddedOptionsPrice = 0;

      // Recorrem atributs producte x calcular preu final
      if(isset($add_product_options)) {
        foreach($add_product_options as $option_id => $option_value_id) {
	  $query_raw = "select pa.price, pa.price_prefix, po.name, pov.name as values_name from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join " . TABLE_PRODUCTS_OPTIONS . " po on po.id = pa.options_id left join " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov on pa.values_id = pov.id where pa.products_id = '" . $add_product_products_id . "' and pa.options_id = '" . $option_id . "' and pa.values_id = '" . $option_value_id . "'";
          $option = tep_db_fetch_array(tep_db_query($query_raw));
          $optionInfo = new objectInfo($option);

	  $AddedOptionsPrice += $optionInfo->price;
	  $option_value_details[$option_id][$option_value_id] = array ("price" => $optionInfo->price, "price_prefix" => $optionInfo->price_prefix);
	  $option_names[$option_id] = $optionInfo->name;
	  $option_values_names[$option_value_id] = $optionInfo->values_name;
        }
      }

      // Obtenim info producte x insertar el producte a la comanda
      $query_raw = "select p.model, p.price, pd.name, p.tax_class_id from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.id = pd.products_id  where p.id='" . $add_product_products_id . "'";
      $product = tep_db_fetch_array(tep_db_query($query_raw));
      $productInfo = new objectInfo($product);

      $FinalPrice = $productInfo->price + $AddedOptionsPrice;
      $ProductsTax = tep_get_tax_rate($productInfo->tax_class_id);

      $sql_data_array = array('orders_id' => tep_db_prepare_input($oID),
                              'products_id' => tep_db_prepare_input($add_product_products_id),
                              'model' => tep_db_prepare_input($productInfo->model),
                              'name' => tep_db_prepare_input($productInfo->name),
                              'price' => tep_db_prepare_input($productInfo->price),
                              'final_price' => tep_db_prepare_input($FinalPrice),
                              'tax' => tep_db_prepare_input($ProductsTax),
                              'quantity' => '1');

      tep_db_perform(TABLE_ORDERS_PRODUCTS, $sql_data_array);
      $new_product_id = tep_db_insert_id();

      tep_db_query("update " . TABLE_PRODUCTS . " set quantity = quantity - 1, ordered = ordered + 1 where id = '" . (int)$add_product_products_id . "'");

      if(isset($add_product_options)) {
        foreach($add_product_options as $option_id => $option_value_id) {
          $sql_data_array = array('orders_id' => tep_db_prepare_input($oID),
                                  'orders_products_id' => tep_db_prepare_input($new_product_id),
                                  'options_name' => tep_db_prepare_input($option_names[$option_id]),
                                  'values_name' => tep_db_prepare_input($option_values_names[$option_value_id]),
                                  'price' => tep_db_prepare_input($option_value_details[$option_id][$option_value_id]["price"]),
                                  'price_prefix' => tep_db_prepare_input($option_value_details[$option_id][$option_value_id]["price_prefix"]));

          tep_db_perform(TABLE_ORDERS_PRODUCTS_ATTRIBUTES, $sql_data_array);
	}
      }

      // Tornem a definir comanda un cop afegit el producte x modificar totals
      $order = new order($oID);
      // Calculate Totals
      $RunningSubTotal = 0;
      $RunningTax = 0;

      // Recorrem productes i atributs
      for ($i=0; $i<sizeof($order->products); $i++) {
        // Calcular preu si hi ha atributs
        $products_price =  $order->products[$i]['price'];
        if ((isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0)) {
          $sizea = sizeof($order->products[$i]['attributes']);
          for ($j=0; $j<$sizea; $j++) {
            if ($order->products[$i]['attributes'][$j]['price_prefix']=='-') {
  	      $products_price -=  $order->products[$i]['attributes'][$j]['price'];
            } else {
  	      $products_price +=  $order->products[$i]['attributes'][$j]['price'];
            }
          }
        }

	// Update Subtotals
    	$RunningSubTotal += tep_substract_tax($products_price, $order->products[$i]['tax']) * $order->products[$i]['qty'];
	$RunningTax += tep_calculate_tax($products_price, $order->products[$i]['tax']) * $order->products[$i]['qty'];
      }

      // Sub-Total
      $sql_data_array = array('text' => tep_db_prepare_input($currencies->display_price($RunningSubTotal)),
                              'value' => tep_db_prepare_input($RunningSubTotal));

      tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array, 'update', 'class="ot_subtotal" and orders_id="' . $oID . '"');

      // Tax
      $sql_data_array = array('text' => tep_db_prepare_input($currencies->display_price($RunningTax)),
                              'value' => tep_db_prepare_input($RunningTax));

      tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array, 'update', 'class="ot_tax" and orders_id="' . $oID . '"');

      // Total
      $query_raw = "select sum(value) as total_value from " . TABLE_ORDERS_TOTAL . " where class != 'ot_total' and orders_id='" . $oID . "'";
      $result = tep_db_fetch_array(tep_db_query($query_raw));
      $Total = $result["total_value"];

      $sql_data_array = array('text' => tep_db_prepare_input($currencies->display_price($Total)),
                              'value' => tep_db_prepare_input($Total));

      tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array, 'update', 'class="ot_total" and orders_id="' . $oID . '"');
    break;
  }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='<?php echo LANGUAGE_CONTENT; ?>' xml:lang='<?php echo LANGUAGE_CONTENT; ?>' xmlns="http://www.w3.org/1999/xhtml">

<head>
<title><?php echo (tep_not_null($titular)) ? $titular : WEB_NAME; ?></title>
<meta name="author" content="Arnau Pujol Cabarrocas - analogicemotion.com"/>
<meta name="keywords" content="<?php echo (tep_not_null($keywords)) ? $keywords : WEB_KEYWORDS; ?>" />
<meta name="description" content="<?php echo (tep_not_null($description)) ? $description : WEB_DESCRIPTION; ?>" />
<meta http-equiv="content-language" content="<?php echo LANGUAGE_CONTENT; ?>" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="robots" content="index,follow" />

<link href="favicon.png" type="image/png" rel="shortcut icon" />
<link href="favicon.png" type="image/png" rel="icon" />

<link rel="stylesheet" type="text/css" href="css/structure_form.css" media="screen" title="default" />
<link rel="stylesheet" type="text/css" href="css/colorbox.css" media="screen" title="default" />

<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>

<script type="text/javascript" language="javascript">
//<![CDATA[
<?php
if ($_GET['action']=='save'){
?>

parent.location.reload();

<?php
}
?>

$(document).ready(function() {

  $('.check_form').click(function(evento) {
    if(check_form()) {
      $('#'+$(this).parents("form").attr("id")).submit();
    } else {
      evento.preventDefault();
    }
  });

  $('.close_box').click(function() {
    parent.$.fn.colorbox.close()
  });

});

function update_product(theForm) {
  var NumProducts = theForm.products_id.options.length;
  var SelectedCategory = '';

  while(NumProducts > 0) {
    NumProducts = NumProducts - 1;
    theForm.products_id.options[NumProducts] = null;
  }

  SelectedCategory = theForm.categories_id.options[theForm.categories_id.selectedIndex].value;

  <?php echo tep_js_productes_list('SelectedCategory', 'theForm', 'products_id'); ?>
}

function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}

//]]>
</script>

</head>

<body>
<div id="pagina">
  <div><?php echo TEXT_INTRO; ?></div>
  <?php
  if (!$_GET['action']) {
  ?>
  <?php echo tep_draw_form('form_data', tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=confirm'), 'post', 'enctype="multipart/form-data"'); ?>
  <fieldset>
    <label>
      <span>Categoria</span>
      <?php echo tep_draw_pull_down_menu('categories_id',  tep_get_category_tree(), $_POST['add_product_categories_id'], 'onchange="update_product(this.form);"'); ?>
    </label>
    <label>
      <span>Producto</span>
      <?php echo tep_draw_pull_down_menu('products_id', tep_prepare_productes_pull_down($_POST['add_product_categories_id']), '', ''); ?>
    </label>
  </fieldset>
  <p class="botonera">
    <?php
    echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_CONTINUE . '</span></a>';
    echo '<a href="javascript:void(0);" class="close_box"><span>' . BUTTON_CLOSE . '</span></a>';
    ?>
  </p>
  </form>
  <?php
  } else if ($_GET['action']=='confirm') {
  ?>
  <?php echo tep_draw_form('form_data', tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=save'), 'post', 'enctype="multipart/form-data"'); ?>
  <?php echo tep_draw_hidden_field('add_product_categories_id', $_POST['categories_id']); ?>
  <?php echo tep_draw_hidden_field('add_product_products_id', $_POST['products_id']); ?>
  <fieldset>
    <label>
      <span>Categoria</span>
      <input type="text" name="categories_name" value="<?php echo tep_get_category_name($_POST['categories_id'],$language_id); ?>" readonly="readonly" />
    </label>
    <label>
      <span>Producto</span>
      <input type="text" name="products_name" value="<?php echo tep_get_products_name($_POST['products_id']); ?>" readonly="readonly" />
    </label>
    <p class="form_sep"></p>

    <?php
    $product_id = $_POST['products_id'];
    // Atributs
    $options = '';
    $options_name = tep_db_query("select distinct pa.options_id, po.id, po.name from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join " .TABLE_PRODUCTS_OPTIONS . " po on po.id = pa.options_id and po.language_id = '" . (int)$language_id . "' where pa.products_id='" . (int)$product_id . "' order by po.listorder");
    if (tep_db_num_rows($options_name)>0) {
      while ($option = tep_db_fetch_array($options_name)) {
        $options .=  '<label><span>' . $option['name'] . ':</span>';
        $products_options_array = array();
        $options_values = tep_db_query("select distinct pa.values_id, pa.price, pa.price_prefix, pov.name from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov on pov.id = pa.values_id  and pov.language_id = '" . (int)$language_id . "' where pa.products_id = '" . (int)$product_id . "' and pa.options_id = '" . $option['id'] . "' order by pov.listorder, pa.price");
        while ($option_values = tep_db_fetch_array($options_values)) {
          $products_options_array[] = array('id' => $option_values['values_id'], 'text' => $option_values['name']);
        }
        $options .= tep_draw_pull_down_menu('add_product_options[' . $option['id'] . ']', $products_options_array, '', ' id="option_id_' . $option['id'] . '"') . '</label>';
      }
    } else {
      $options = TEXT_NO_OPTIONS;
    }
    echo $options;
    ?>

  </fieldset>
  <p class="botonera">
    <?php
    echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_CONTINUE . '</span></a>';
    echo '<a href="' . tep_href_link('form_product.php') . '"><span>' . BUTTON_BACK . '</span></a>';
    echo '<a href="javascript:void(0);" class="close_box"><span>' . BUTTON_CLOSE . '</span></a>';
    ?>
  </p>
  </form>
  <?php
  }
  ?>    
</div>

</body>
</html>

<?php
require('./includes/application_bottom.php');
?>
