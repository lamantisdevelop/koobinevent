<?php
/*
Web modular v.1.0 - Login
Arnau Pujol i Cabarrocas - analogicemotion.com
*/

/*
Tasques que es repeteixen a cada recàrrega de l'index.php - INICI
*/
require('includes/application_top.php');

/*
Accions
*/

// Eliminem variables de sessió
tep_session_unregister('customer_id');
tep_session_unregister('customer_id_rol');

// Guardem variables de sessió
tep_session_close();

// Missatge
$messageStack->add_session(SUCCESS_LOGOFF, 'success');
// Redirecció
tep_redirect(tep_href_link('login.php',''));


/*
Tasques que es repeteixen a cada recàrrega
*/
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
