<?php

/*
  Web modular v.1.0 - Login
  La Mantis www.mantis.cat
 */

/*
  Tasques que es repeteixen a cada recàrrega de l'index.php - INICI
 */
require('includes/application_top.php');

/*
  Comprovem login usuari
 */
if (tep_session_is_registered('customer_id')) {
    tep_redirect(tep_href_link('index.php'));
}

/*
  Accions
 */
if ($_GET['action']) {
    switch ($_GET['action']) {
        case 'process':

            $mail = tep_db_prepare_input($_POST['mail']);
            $password = tep_db_prepare_input($_POST['password']);

            // Check if email exists
            $check_admin_query = tep_db_query("select id, name, password, mail, id_rol from " . TABLE_ADMIN_USERS . " where mail = '" . tep_db_input($mail) . "'");
            if (!tep_db_num_rows($check_admin_query)) {
                $error = true;
            } else {
                $check_admin = tep_db_fetch_array($check_admin_query);
                // Check that password is good
                if (!tep_validate_password($password, $check_admin['password'])) {
                    $error = true;
                } else {
                    // Validat correctament
                    $error = false;
                }
            }

            if ($error) {
                // Missatge d'error
                $messageStack->add_session(ERROR_LOGIN, 'error');
                // Redirecció
                tep_redirect(tep_href_link('login.php', tep_get_all_get_params(array('action', 'x', 'y'))));
            } else {

                // Creem variable de sessió i li assignem els valors corresponents
                $customer_id = $check_admin['id'];
                tep_session_register('customer_id');
                $customer_id_rol = $check_admin['id_rol'];
                tep_session_register('customer_id_rol');
                // Guardem variables de sessió
                tep_session_close();

                // Modifiquem info a la base de dades
                tep_db_query("update " . TABLE_ADMIN_USERS . " set logons = logons+1, last_logon = now() where id = '" . (int) $check_admin['id'] . "'");

                // Missatge d'èxit
                $messageStack->add_session(sprintf(SUCCESS_LOGIN, $check_admin['name']), 'success');
                // Redirecció
                tep_redirect(tep_href_link('', tep_get_all_get_params(array('action', 'x', 'y'))));
            }
            break;
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title><?php echo (tep_not_null($titular)) ? $titular : WEB_NAME; ?></title>
        <meta name="author" content="La Mantis - www.mantis.cat"/>
        <meta name="keywords" content="<?php echo (tep_not_null($keywords)) ? $keywords : WEB_KEYWORDS; ?>" />
        <meta name="description" content="<?php echo (tep_not_null($description)) ? $description : WEB_DESCRIPTION; ?>" />
        <meta http-equiv="content-language" content="<?php echo LANGUAGE_CONTENT; ?>" />
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta name="robots" content="index,follow" />

        <link href="favicon.png" type="image/png" rel="shortcut icon" />
        <link href="favicon.png" type="image/png" rel="icon" />

        <link rel="stylesheet" type="text/css" href="css/structure.css" media="screen" title="default" />

        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>

        <script type="text/javascript" language="javascript">
            //<![CDATA[

            // Comprovar  enviar formulari
            function check_form() {
                var error = 0;
                var error_message = "<?php echo JS_ERROR; ?>";

                var mail = trim(document.form_data.mail.value);
                var password = trim(document.form_data.password.value);

                if (!/^.{2,}$/.test(mail)) {
                    error_message = error_message + "<?php echo JS_MAIL; ?>";
                    error = 1;
                }

                if (!/^.{2,}$/.test(password)) {
                    error_message = error_message + "<?php echo JS_PASSWORD; ?>";
                    error = 1;
                }

                if (error == 1) {
                    alert(error_message);
                    return false;
                } else {
                    return true;
                }
            }

            // Funcions JQuery
            $(document).ready(function() {

                $('.check_form').click(function(evento) {
                    if(check_form()) {
          
                        $('#pagina').block({
                            message: '<h1><img src="image/loading.gif"/>&nbsp;&nbsp;Processant...<\/h1>'
                        });
                        $('#form_data').submit();
                    } else {
                        evento.preventDefault();
                    }
                });
            });

            //]]>
        </script>

    </head>

    <body>
        <div id="pagina">
            <?php
            /* Capçalera */
            include(DIR_WS_MODULS . 'header.php');
            ?>
            <div id="estructura_400">
            <?php
            /* Errors */
            if ($messageStack->size > 0) {
                echo $messageStack->output();
            }
            ?>
                <div id="form">
                <?php echo tep_draw_form('form_data', tep_href_link('login.php', tep_get_all_get_params(array('action', 'pos', 'x', 'y')) . 'action=process'), 'post', 'enctype="multipart/form-data"'); ?>
                    <fieldset>
                        <p><strong>Accés a l'eina d'administració:</strong></p>
                        <p class="form_sep"></p>
                        <label>
                            <span>E-mail</span>
                        </label>
                        <input type="text" name="mail" value="" />
                        <label>
                            <span>Password</span>
                        </label>
                        <input type="password" name="password" value="" />
                    </fieldset>
                    <p class="botonera">
<?php
echo '<a href="javascript:void(0);" class="check_form"><span>' . BUTTON_SEND . '</span></a>';
//echo '<a href="../"><span>' . BUTTON_CANCELL . '</span></a>';
?>
                    </p>
                    </form>
                </div>
            </div>
        </div>
<?php
include(DIR_WS_MODULS . 'footer.php');
?>
    </body>
</html>

<?php
/*
  Tasques que es repeteixen a cada recàrrega
 */
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
